<?xml version="1.0" encoding="UTF-8"?>
<!-- 
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->
<project basedir="." default="build-jar" name="itk-payloads">

	<target name="all" depends="build-jar, create-javadoc">
	</target>

	<!-- Set properties and class path -->
	<target name="init">
		<property name="bld.jar.file" value="itk-payloads.jar" />
		<property name="bld.dir" value="${basedir}/target" />
		<property name="release.dir" value="${basedir}/release" />
		<property name="bld.classes.dir" value="${basedir}/bin" />
		<property name="javadoc.dir" value="${basedir}/javadoc" />
		<property name="src.dir" value="${basedir}/src/main/java" />
		<property name="lib.dir" value="${basedir}/src/main/lib" />
		<property name="config.dir" value="${basedir}/src/main/resources" />
		<property name="bld.compiler" value="modern"/>
		<property name="doclet.dir" value="${basedir}/src/test/lib"/>

		<echo message="Setting classpath." />
		<path id="local.classpath">
			<fileset dir="${config.dir}">
				<include name="**/*.*" />
			</fileset>
			<fileset dir="${lib.dir}">
				<include name="**/*.jar" />
			</fileset>
		</path>
	</target>

	<!-- Clean up directories -->
	<target depends="init" name="clean">
		<echo message="Cleaning up directories." />
		<delete dir="${bld.classes.dir}" />
		<mkdir dir="${bld.classes.dir}" />
	</target>

	<!-- Compile the code -->
	<target depends="clean" name="compile">
		<echo message="Compiling code." />
		<javac debug="true" deprecation="true" destdir="${bld.classes.dir}" srcdir="${src.dir}" target="1.5" source="1.5">
			<classpath>
				<path refid="local.classpath" />
			</classpath>
		</javac>
	</target>

	<!-- Generate JavaDoc Documentation -->
	<target depends="init" name="create-javadoc">
		<javadoc packagenames="*" destdir="${javadoc.dir}" author="true" overview="src/main/javadoc/overview.html"
			version="true" use="true" access="public" windowtitle="ITK Payloads API">
			<packageset dir="${src.dir}" defaultexcludes="no">
			</packageset>
			<classpath>
				<path refid="local.classpath" />
			</classpath>
			<doctitle>
				<![CDATA[<h1>ITK Payloads API</h1>]]></doctitle>
			<bottom>
				<![CDATA[<i>&#169; 2015 Health and Social Care Information Centre.</i>]]></bottom>
		</javadoc>
	</target>

	<!-- Build the JAR file -->
	<target depends="compile" name="build-jar">
		<delete dir="${bld.dir}" />
		<mkdir dir="${bld.dir}" />
		<jar compress="true" jarfile="${bld.dir}/${bld.jar.file}">
			<fileset dir="${bld.classes.dir}" includes="**" />
			<fileset dir="${config.dir}" includes="**/*.*"/>
		</jar>
	</target>
	
	<!-- Build a JAR file for a distribution release -->
	<target depends="compile" name="build-jar-dist">
		<delete dir="${release.dir}" />
		<mkdir dir="${release.dir}" />
		<tstamp/> <!-- Sets the time and date stamp properties -->
		<jar compress="true" jarfile="${release.dir}/${DSTAMP}-${bld.jar.file}">
			<fileset dir="${bld.classes.dir}" includes="**" />
			<fileset dir="${config.dir}" includes="**/*.*"/>
		</jar>
	</target>
	
	<!-- Run the JUnit Tests -->
    <target name="run-tests">
    	<ant antfile="./runTests.xml" target="run-junit"/>
    </target>
	
</project>
