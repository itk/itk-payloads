<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
  	<xsl:apply-templates select="fieldList"/>
  </xsl:template>
  
  <xsl:template match="fieldList">
  	<xsl:apply-templates select="class"/>
  </xsl:template>
  
  <xsl:template match="class">
h2. <xsl:value-of select="@name" />

<xsl:value-of select="description" />

|_. Field Name |_. Type |_. Description |
<xsl:apply-templates select="field[not(fixed)]"/>
  </xsl:template>
  
<xsl:template match="field[not(fixed)]">| <xsl:value-of select="name" /> | <xsl:value-of select="type" /> | <xsl:value-of select="description" /> |
</xsl:template>

</xsl:stylesheet>
