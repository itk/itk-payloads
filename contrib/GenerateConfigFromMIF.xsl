<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:mif="urn:hl7-org:v3/mif" version="1.0">
  <xsl:template match="/">
  <fieldList>
  	<xsl:apply-templates select="mif:staticModel"/>
  </fieldList>
  </xsl:template>
  
  <xsl:template match="mif:staticModel">
  	<xsl:apply-templates select="mif:ownedClass/mif:class"/>
  </xsl:template>
  
  <xsl:template match="mif:ownedClass/mif:class">
  	<class>
  		<xsl:attribute name="name">
  			<xsl:value-of select="@name" />
  		</xsl:attribute>
  		
  		<xsl:apply-templates select="mif:attribute"/>
  	
  		<xsl:variable name="className" select="@name"/>
  	  	<!-- Try to traverse the links to linked classes -->
  	  	<xsl:for-each select="/mif:staticModel/mif:ownedAssociation/mif:connections/mif:traversableConnection[mif:derivationSupplier/@className=$className]">
  	  		<xsl:call-template name="AddConnection" />
  	  	</xsl:for-each>
  	</class>
  </xsl:template>
  
  <xsl:template name="AddConnection">
  		<field>
  			
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
  			<xpath>x:<xsl:value-of select="@name"/></xpath>
  			<description><xsl:value-of select="mif:annotations/mif:otherAnnotation/mif:text/mif:div"/></description>
  			<type><xsl:value-of select="@participantClassName"/></type>
  			<xsl:text disable-output-escaping="yes">
  			&lt;!-- Ideally, where possible (i.e. where there is a 0..1 or 1..1 relationship, try to flatten the template into a single object and remove this link --&gt;</xsl:text>
  	<xsl:choose>
  		<xsl:when test="@isMandatory = 'true'">
  			<mandatory>true</mandatory>
  		</xsl:when>
  		<xsl:otherwise>
  			<mandatory>false</mandatory>
  		</xsl:otherwise>
  	</xsl:choose>
  	<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  </xsl:template>
  
  <xsl:template match="mif:attribute">
    		<xsl:call-template name="TryToFigureOutDetails"/>
  </xsl:template>
  
  <xsl:template match="@maximumMultiplicity">
  	<xsl:choose>
  		<xsl:when test=". = 1"></xsl:when>
  		<xsl:when test=". = '*'">
  			<maxOccurs>100</maxOccurs></xsl:when>
  		<xsl:when test=". &lt; 100">
  			<maxOccurs><xsl:value-of select="."/></maxOccurs></xsl:when>
  		<xsl:otherwise></xsl:otherwise>
  	</xsl:choose>
  </xsl:template>

  <!--
  Need to add logic to traverse links to other classes - e.g. from MIF:
   <ownedAssociation>
        <graphicRepresentation>...</graphicRepresentation>
        <connections>
            <traversableConnection conformance="R" isMandatory="true"
                maximumMultiplicity="1" minimumMultiplicity="1"
                name="patientPatient" participantClassName="Patient">
                <annotations>...</annotations>
                <derivationSupplier associationEndName="player"
                    className="PatientRole" staticModelDerivationId="1"/>
            </traversableConnection>
            <nonTraversableConnection ...>...</nonTraversableConnection>
        </connections>
    </ownedAssociation>
    
    and
    
    <ownedAssociation>
        <graphicRepresentation> ... </graphicRepresentation>
        <connections>
            <traversableConnection conformance="R" isMandatory="true"
                maximumMultiplicity="1" minimumMultiplicity="1"
                name="representedCustodianOrganization" participantClassName="CustodianOrganization">
                <annotations> ... </annotations>
                <derivationSupplier associationEndName="scoper"
                    className="AssignedCustodian" staticModelDerivationId="1"/>
            </traversableConnection>
            <nonTraversableConnection ...> ... </nonTraversableConnection>
        </connections>
    </ownedAssociation>
  -->
  
  <xsl:template name="TryToFigureOutDetails">
  	<xsl:choose>
  	<xsl:when test="mif:type/@name='II'">
  		<xsl:call-template name="II"/>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='CV'">
  		<xsl:call-template name="CV"/>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='CS'">
  	      	<xsl:call-template name="CS">
  	      	</xsl:call-template>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='ON'">
  	      	<xsl:call-template name="String"/>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='TEL'">
  	      	<xsl:call-template name="TEL"/>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='AD'">
  	      	<xsl:call-template name="AD"/>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='PN'">
                <xsl:call-template name="PN"/>
  	</xsl:when>
  	<xsl:when test="mif:type/@name='TS'">
                <xsl:call-template name="TS"/>
  	</xsl:when>
  	<!-- If this is a SET we need to find out what kind of set -->
  	<xsl:when test="mif:type/@name='SET'">
  		<xsl:choose>
		<!-- Set of Addresses -->
		<xsl:when test="mif:type/mif:supplierBindingArgumentDatatype/@name='AD'">
			<xsl:call-template name="AD" />
		</xsl:when>
		<!-- Set of IIs -->
		<xsl:when test="mif:type/mif:supplierBindingArgumentDatatype/@name='II'">
			<xsl:call-template name="II" />
		</xsl:when>
		<!-- Set of TELs -->
		<xsl:when test="mif:type/mif:supplierBindingArgumentDatatype/@name='TEL'">
			<xsl:call-template name="TEL" />
		</xsl:when>
		<!-- Set of PNs -->
		<xsl:when test="mif:type/mif:supplierBindingArgumentDatatype/@name='PN'">  			
			<xsl:call-template name="PN" />
		</xsl:when>
		<!-- Unknown type within the SET -->
		<xsl:otherwise>
			<xsl:call-template name="String">
			<xsl:with-param name="Comments">UNKNOWN TYPE IN SET - PLEASE SET TYPE</xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
  		</xsl:choose>
  	</xsl:when>
  	<!-- Unknown Type -->
  	<xsl:otherwise>
			<xsl:call-template name="String">
			<xsl:with-param name="Comments">UNKNOWN TYPE - PLEASE SET TYPE</xsl:with-param>
			</xsl:call-template>  		
  	</xsl:otherwise>
    	</xsl:choose>

  </xsl:template>
  
  <!-- Populate Field Definitions for specific Data Types -->
  <xsl:template name="II">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  	<xsl:choose>
  	<xsl:when test="@fixedValue">
  		<field>
  			<!-- We need to try and find a root value.. -->
  			<name><xsl:apply-templates select="@name" mode="capitalise"/>Root</name>
  			<xpath>x:<xsl:value-of select="@name"/>/@root</xpath>
  			<fixed><xsl:call-template name="TryToGuessRootFromHTML" /></fixed>
  		</field>
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
  			<xpath>x:<xsl:value-of select="@name"/>/@extension</xpath>
  			<fixed><xsl:value-of select="@fixedValue"/></fixed>
  		</field>
  	</xsl:when>
  	<xsl:otherwise>
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
  			<xpath>x:<xsl:value-of select="@name"/></xpath>
  			<xsl:text disable-output-escaping="yes">
  			&lt;!-- SET THE BELOW TYPE TO ONE OF: AuthorRoleID, ConsentID, DeviceID, OrgID, PatientID, PersonID, RoleID, SystemID or WorkgroupID --&gt;</xsl:text>
  			<type package="uk.nhs.interoperability.payloads.commontypes.">SETME</type>
  			<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
  			<xsl:value-of select="$AdditionalElements"/>
  			<xsl:if test="$Comments != ''">
				<xsl:text disable-output-escaping="yes">
				&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
			</xsl:if>
  			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  	</xsl:otherwise>
  	</xsl:choose>
  </xsl:template>
  <xsl:template name="CV">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  	<xsl:choose>
  	<xsl:when test="@fixedValue">
  		<field>
  			<!-- Fixed coded value... do something with this! -->
  			<name><xsl:apply-templates select="@name" mode="capitalise"/>FixedCodedValue</name>
  			<xpath>x:<xsl:value-of select="@name"/>/@code</xpath>
  			<fixed><xsl:call-template name="TryToGuessRootFromHTML" /></fixed>
  		</field>
  	</xsl:when>
  	<xsl:otherwise>
  		<field>
  			<name><xsl:apply-templates select="mif:supplierDomainSpecification/@domainName" mode="capitalise"/></name>
  			<xpath>x:<xsl:value-of select="@name"/></xpath>
  			<type>CodedValue</type>
  			<vocabulary><xsl:value-of select="mif:supplierDomainSpecification/@domainName"/></vocabulary>
  			<xsl:if test="mif:supplierDomainSpecification/@codingStrength = 'CWE'">
  			<localCodeOID><xsl:text disable-output-escaping="yes">&lt;!-- Add OID for local codes here --&gt;</xsl:text></localCodeOID>
  			</xsl:if>
  			<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
  			<xsl:value-of select="$AdditionalElements"/>
  			<xsl:if test="$Comments != ''">
				<xsl:text disable-output-escaping="yes">
				&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
			</xsl:if>
  			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  	</xsl:otherwise>
  	</xsl:choose>
  </xsl:template>
  <xsl:template name="CS">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  		<!-- If this is a CS we will assume it is an attribute on the parent element -->
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
  			<xpath>@<xsl:value-of select="@name"/></xpath>
  			
  			<xsl:choose>
  			<xsl:when test="@fixedValue">
  				<fixed><xsl:value-of select="@fixedValue"/></fixed>
  			</xsl:when>
  			<xsl:otherwise>
  				<type>String</type>
  				<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
    			<xsl:value-of select="$AdditionalElements"/>
    			<xsl:if test="$Comments != ''">
					<xsl:text disable-output-escaping="yes">
					&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
				</xsl:if>
    			<xsl:apply-templates select="@maximumMultiplicity"/>
  			</xsl:otherwise>
  			</xsl:choose>
  		</field>
  </xsl:template>
  <xsl:template name="String">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
  			<xpath>x:<xsl:value-of select="@name"/></xpath>
  			<type>String</type>
  			<xsl:if test="@fixedValue">
    			<fixed><xsl:value-of select="@fixedValue"/></fixed>
    			</xsl:if>
    			<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
    			<xsl:value-of select="$AdditionalElements"/>
    			<xsl:if test="$Comments != ''">
					<xsl:text disable-output-escaping="yes">
					&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
				</xsl:if>
    			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  </xsl:template>
  <xsl:template name="AD">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
  			<xpath>x:<xsl:value-of select="@name"/></xpath>
  			<xsl:text disable-output-escaping="yes">
  			&lt;!-- DECIDE WETHER YOU REALLY WANT MULTIPLE ADDRESSES HERE --&gt;</xsl:text>
  			<type package="uk.nhs.interoperability.payloads.commontypes.">Address</type>
  			<xsl:value-of select="$AdditionalElements"/>
  			<xsl:if test="$Comments != ''">
				<xsl:text disable-output-escaping="yes">
				&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
			</xsl:if>
  			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  </xsl:template>
  <xsl:template name="PN">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
			<xpath>x:<xsl:value-of select="@name"/></xpath>
			<description>Person name</description>
			<type package="uk.nhs.interoperability.payloads.commontypes.">PersonName</type>
			<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
    			<xsl:value-of select="$AdditionalElements"/>
    			<xsl:if test="$Comments != ''">
    				<xsl:text disable-output-escaping="yes">
    				&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
    			</xsl:if>
    			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  </xsl:template>
  <xsl:template name="TEL">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
			<xpath>x:<xsl:value-of select="@name"/></xpath>
			<description>Telephone Number</description>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Telecom</type>
			<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
    			<xsl:value-of select="$AdditionalElements"/>
    			<xsl:if test="$Comments != ''">
    				<xsl:text disable-output-escaping="yes">
    				&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
    			</xsl:if>
    			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  </xsl:template>
  <xsl:template name="TS">
  	<xsl:param name="AdditionalElements" />
  	<xsl:param name="Comments" />
  		<field>
  			<name><xsl:apply-templates select="@name" mode="capitalise"/></name>
			<xpath>x:<xsl:value-of select="@name"/></xpath>
			<description>Date</description>
			<type>HL7Date</type>
			<mandatory><xsl:value-of select="@isMandatory"/></mandatory>
			<format>yyyyMMddHHmm</format>
    			<xsl:value-of select="$AdditionalElements"/>
    			<xsl:if test="$Comments != ''">
    				<xsl:text disable-output-escaping="yes">
    				&lt;!-- </xsl:text><xsl:value-of select="$Comments"/><xsl:text disable-output-escaping="yes"> --&gt;</xsl:text>
    			</xsl:if>
    			<xsl:apply-templates select="@maximumMultiplicity"/>
  		</field>
  </xsl:template>
  
  
  <!-- For some reason, the MIF only includes this information in some hand-written HTML
         Warning - This template is VERY brittle! -->
  <xsl:template name="TryToGuessRootFromHTML">
  	<!-- Look for an li element that contains the text OID -->
  	<xsl:apply-templates select="mif:annotations/mif:otherAnnotation[@type='html']/mif:text"/>
  </xsl:template>
  <xsl:template match="mif:annotations/mif:otherAnnotation[@type='html']/mif:text">
  	<xsl:apply-templates select="mif:ul/mif:li[contains(., 'OID')]"/>
  	<xsl:apply-templates select="mif:div"/>
  </xsl:template>
  <xsl:template match="mif:div">
  	<xsl:apply-templates select="mif:ul/mif:li[contains(., 'OID')]"/>
  	<xsl:apply-templates select="mif:div"/>
  </xsl:template>
  <xsl:template match="mif:ul/mif:li[contains(., 'OID')]">
  	<xsl:value-of select="mif:font/mif:b"/>
  </xsl:template>
  

  <!-- Templates to capitalise values -->  
  <xsl:template match="@name" mode="capitalise">
  	<xsl:call-template name="capitalise">
  		<xsl:with-param name="value" select="."/>
  	</xsl:call-template>
  </xsl:template>
  <xsl:template match="@name"><xsl:value-of select="."/></xsl:template>
  
  <!-- XSLT 1.0 hack to capitalise a value -->
  <xsl:variable name="vLower" select="'abcdefghijklmnopqrstuvwxyz'"/>
  <xsl:variable name="vUpper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
 
  <xsl:template name="capitalise">
     <xsl:param name="value"/>
     <xsl:value-of select=
          "normalize-space(concat(translate(substring($value,1,1), $vLower, $vUpper),
          substring($value, 2), substring(' ', 1 div not(position()=last()))))"/>
  </xsl:template>
  
  </xsl:stylesheet>
