0.6 (In development)
====================

- Small changes to helper class for transfer of care:
	- Renamed medications author field - note: this may cause issues for anyone who had previously used this field in the helper.
	- Made Gender non-mandatory - if not supplied then an UNK null flavour will be used.

0.5 (2015-09-27)
====================

- Added support for Transfer of Care (eDischarge) Payload (Draft B) - currently WITHOUT coded entries
- Added support for PDS Mini Service SOAP messages
- Added limited support for Spine PDS messages (simple trace only)
- Performance improvements by using thread local caching for parsing and serialising. This could potentially cause strange issues when running inside a container that uses thread pooling - if so, it can be disabled in config.properties by setting reuseXmlFactories to false.
- Added rudimentary support for SNOMED CT compositional statements (required for latest CDA specifications that use them for document type codes).

0.4 (2014-12-04)
================

- Added support for Child Screening payload
- Added Maven build script (as an alternative to Ant)
- First release to be made available via Maven central

0.3 (2014-05-19)
================

- Added support for Non-Coded CDA payload
- JavaDoc improvements to make it easier to navigate the fields in generated payload classes

0.2 (2014-01-24)
================

- Added support for Document Retrieval payload
- Bug fixes for end of life care payload helper 

0.1 (2013-10-09)
================

- Payloads supported: End of Life Care, Notifications
- First release of the itk-payloads library
- Code generation for payload classes in place, with support for parsing and serialising payloads
