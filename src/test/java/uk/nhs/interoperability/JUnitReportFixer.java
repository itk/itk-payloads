/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability;

import java.io.File;

import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;

/**
 * This is a class to fix junit report outputs. This is a workaround
 * for an issue where junit writes an extra training character on the
 * end of it's reports. I believe this is a concurrency issue, but there
 * does not appear to be any fixes available currently.
 * @author Adam Hatherly
 */
public class JUnitReportFixer {

	public static void main(String[] args) {
		if (args.length!=1) {
			return;
		}
		// Note: no paths with spaces allowed!
		String path = args[0];
		
		File dir = new File(path);
		  File[] directoryListing = dir.listFiles();
		  if (directoryListing != null) {
		    for (File f : directoryListing) {
		      // Fix the report
		      fixFile(f);
		    }
		  }
	}
	
	private static void fixFile(File f) {
		String content = FileLoader.loadFile(f);
		String search = "</testsuite>";
		int idx = content.indexOf(search);
		if (idx > -1) {
			content = content.substring(0, idx+search.length());
			FileWriter.writeFile(f, content.getBytes());
		} else {
			// Doesn't seem to be a junit report, so leave it alone
			return;
		}
	}
}
