/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import uk.nhs.interoperability.payloads.DateValueTest;
import uk.nhs.interoperability.payloads.childscreeningv2.ChildScreeningPerformanceTest;
import uk.nhs.interoperability.payloads.documentretrieval.GetDocumentQueryTest;
import uk.nhs.interoperability.payloads.documentretrieval.GetDocumentResponseTest;
import uk.nhs.interoperability.payloads.endoflifecarev1.EndOfLifeCarePerformanceTest;
import uk.nhs.interoperability.payloads.helpers.ChildScreeningDocumentCreationHelperTest;
import uk.nhs.interoperability.payloads.helpers.EndOfLifeCareDocumentCreationHelperTest;
import uk.nhs.interoperability.payloads.helpers.TransferOfCareDraftBDocumentCreationHelperTest;
import uk.nhs.interoperability.payloads.noncodedcdav2.NonCodedCDAPerformanceTest;
import uk.nhs.interoperability.payloads.notification.EventNotificationPerformanceTest;
import uk.nhs.interoperability.payloads.notification.EventNotificationTest;
import uk.nhs.interoperability.payloads.notification.NotificationParseProfilingTest;
import uk.nhs.interoperability.payloads.notification.NotificationSerialiseProfilingTest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetNHSNumberRequestTest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetNHSNumberResponseTest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetPatientDetailsByNHSNumberRequestTest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetPatientDetailsResponseTest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.VerifyNHSNumberRequest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.VerifyNHSNumberRequestTest;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.VerifyNHSNumberResponse;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.VerifyNHSNumberResponseTest;
import uk.nhs.interoperability.payloads.spine.PdsTraceQueryTest;
import uk.nhs.interoperability.payloads.spine.QueryControlActTest;
import uk.nhs.interoperability.payloads.spine.SendMessagePayloadTest;
import uk.nhs.interoperability.payloads.spine.SimpleTraceResponseTest;
import uk.nhs.interoperability.payloads.spine.SpineSOAPTest;
import uk.nhs.interoperability.payloads.templates.TemplatesTestSuite;
import uk.nhs.interoperability.payloads.util.CompositionalGrammarHandlerTest;
import uk.nhs.interoperability.payloads.util.xml.Base64Test;
import uk.nhs.interoperability.payloads.util.xml.ParseUtilsTest;
import uk.nhs.interoperability.payloads.vocabularies.generated.GeneratedEnumTest;
import uk.nhs.interoperability.payloads.vocabularies.internal.IDTypeTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	ParseUtilsTest.class,
	Base64Test.class,
	DateValueTest.class,
	GeneratedEnumTest.class,
	IDTypeTest.class,
	EventNotificationTest.class,
	EventNotificationPerformanceTest.class,
	TemplatesTestSuite.class,
	uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocumentTest.class,
	EndOfLifeCarePerformanceTest.class,
	EndOfLifeCareDocumentCreationHelperTest.class,
	GetDocumentQueryTest.class,
	GetDocumentResponseTest.class,
	uk.nhs.interoperability.payloads.noncodedcdav2.ClinicalDocumentTest.class,
	uk.nhs.interoperability.payloads.noncodedcdav2.ClinicalDocumentTestNonXMLBody.class,
	uk.nhs.interoperability.payloads.toc_edischarge_draftB.ClinicalDocumentTest.class,
	NonCodedCDAPerformanceTest.class,
	NotificationParseProfilingTest.class,
	NotificationSerialiseProfilingTest.class,
	uk.nhs.interoperability.payloads.childscreeningv2.ClinicalDocumentTest.class,
	ChildScreeningPerformanceTest.class,
	ChildScreeningDocumentCreationHelperTest.class,
	GetPatientDetailsResponseTest.class,
	VerifyNHSNumberRequestTest.class,
	VerifyNHSNumberResponseTest.class,
	GetNHSNumberRequestTest.class,
	GetNHSNumberResponseTest.class,
	GetPatientDetailsByNHSNumberRequestTest.class,
	SimpleTraceResponseTest.class,
	PdsTraceQueryTest.class,
	QueryControlActTest.class,
	SendMessagePayloadTest.class,
	SimpleTraceResponseTest.class,
	SpineSOAPTest.class,
	CompositionalGrammarHandlerTest.class,
	TransferOfCareDraftBDocumentCreationHelperTest.class
	})
public class AllTestsTestSuite {
}
