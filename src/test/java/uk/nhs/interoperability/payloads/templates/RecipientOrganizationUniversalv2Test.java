/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class RecipientOrganizationUniversalv2Test extends AbstractTest {
	
	public static RecipientOrganizationUniversalv2 template;
	public static RecipientOrganizationUniversalv2 minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static RecipientOrganizationUniversalv2 createFull() {
		RecipientOrganizationUniversalv2 template = new RecipientOrganizationUniversalv2();
		
		template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:01634445667"));
		
		template.setOrgId(new OrgID()
								.setID("XZ901")
								.setType(OrgIDType.ODSSiteCode.code));
		
		template.setOrgName("St. Elsewhere's Hospital");
		
		template.setCDAOrganizationType(CDAOrganizationType._008);
		
		return template;
	}
	
	public static RecipientOrganizationUniversalv2 createMinimal() {
		RecipientOrganizationUniversalv2 minimal = new RecipientOrganizationUniversalv2();
		
		minimal.setOrgId(new OrgID()
								.setID("XZ901")
								.setType(OrgIDType.ODSSiteCode.code));
		
		minimal.setOrgName("St. Elsewhere's Hospital");
		
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("intendedRecipient", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145203GB02_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("intendedRecipient", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145203GB02_02.xml", result, true);
	}
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145203GB02_01.xml", true);
		RecipientOrganizationUniversalv2 template = new RecipientOrganizationUniversalv2();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("intendedRecipient", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145203GB02_01.xml", result, true);
	}
}
