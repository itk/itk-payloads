/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode;
import uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class PatientUniversalTest extends AbstractTest {
	
	public static PatientUniversal template;
	public static PatientUniversal minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static PatientUniversal createFull() {
		PatientUniversal template = new PatientUniversal();
		
		// PatientRole:
		template.addPatientID(new PatientID()
									.setPatientID("K12345")
									.setAssigningOrganisation("V396A:Medway PCT")
									.setPatientIDType(PatientIDType.LocalID.code));

		template.addPatientID(new PatientID()
									.setPatientID("993254128")
									.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));		
		
		template.addAddress(new Address()
									.addAddressLine("17, County Court")
									.addAddressLine("Woodtown")
									.addAddressLine("Medway")
									.setPostcode("ME5 FS3")
									.setAddressUse(AddressType.Home.code));
		
		template.addAddress(new Address()
									.addAddressLine("Hightown Retirement Home")
									.addAddressLine("2, Brancaster Road")
									.addAddressLine("Medway")
									.addAddressLine("Kent")
									.setPostcode("ME5 FL5")
									.setAddressUse(AddressType.PhysicalVisit.code));
		
		template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:01634772367")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		
		template.addTelephoneNumber(new Telecom()
									.setTelecom("mailto:mark.smith@emailfree.co.uk")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		
		template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:01634451628")
									.setTelecomType(TelecomUseType.EmergencyContact.code));
		
		// Patient
		template.addPatientName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Mark")
									.setFamilyName("Smith"));
		template.setSex(Sex._1);
		template.setBirthTime(new DateValue("19490101"));
		
		// Language
		LanguageCommunication language = new LanguageCommunication();
		language.setLanguage(HumanLanguage._en.code);
		language.setMode(LanguageAbilityMode._ESP);
		language.setProficiencyLevel(LanguageAbilityProficiency._G);
		language.setPreferenceIndNullFlavour(NullFlavour.NotAsked.code);
		template.addLanguages(language);
		
		// Organisation - Registered GP:
		template.setRegisteredGPOrgId(new OrgID()
									.setID("V396F")
									.setType(OrgIDType.ODSOrgID.code));
		template.setRegisteredGPOrgName("Medway Medical Practice");
		template.addRegisteredGPTelephone(new Telecom()
									.setTelecom("tel:01634111222")
									.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setRegisteredGPAddress(new Address()
									.addAddressLine("Springer Street")
									.addAddressLine("Medway")
									.setPostcode("ME5 5TY")
									.setAddressUse(AddressType.WorkPlace.code));
		return template;
	}
	
	public static PatientUniversal createMinimal() {
		PatientUniversal minimal = new PatientUniversal();
		minimal.addPatientID(new PatientID()
								.setPatientID("K12345")
								.setAssigningOrganisation("V396A:Medway PCT")
								.setPatientIDType(PatientIDType.LocalID.code));
		minimal.addPatientID(new PatientID()
								.setPatientID("993254128")
								.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));
		minimal.addPatientName(new PersonName().setNullFlavour(NullFlavour.Unknown.code));
		minimal.setSexNullFlavour(NullFlavour.Unknown.code);
		minimal.setBirthTimeNullFlavour(NullFlavour.Unknown.code);
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("patientRole", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145201GB01_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("patientRole", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145201GB01_02.xml", result, true);
	}
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145201GB01_01.xml", true);
		PatientUniversal template = new PatientUniversal();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("patientRole", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145201GB01_01.xml", result, true);
	}
}
