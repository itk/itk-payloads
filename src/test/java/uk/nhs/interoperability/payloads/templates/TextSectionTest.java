/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.templates.Section2;
import uk.nhs.interoperability.payloads.templates.TextSection;
import uk.nhs.interoperability.payloads.util.FileWriter;

public class TextSectionTest extends AbstractTest {
	
	public static TextSection section1;
	
	static {
		section1 = createSection1();
	}
	
	public static TextSection createSection1() {
		TextSection template = new TextSection();
		
		template.setSectionId("EAE2350C-0242-11E2-9FB6-5BB26188709B");
		template.setTitle("End of Life Care Artefacts");
		
		Section2 s2 = new Section2();
		s2.setSectionId("EF3FC326-0242-11E2-A44C-5FB26188709B");
		s2.setTitle("Anticipatory Medicine Box");
		s2.setText("<content ID=\"a4\">Anticipatory medicine box is issued and kept in refrigerator at patient home.</content>");
		
		template.addSection2(s2);
		
		return template;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		try {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = section1.serialise("section", parentNamespaces);
		//FileWriter.writeFile("output.xml", result.getBytes());
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146229GB01_AH_01.xml", result, true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
	
	@Test
	public void testParse() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146229GB01_AH_01.xml", true);
		TextSection template = new TextSection();
		template.parse(expectedResult, parentNamespaces);
		List<Section2> s2List = template.getSection2();
		Section2 s2 = s2List.get(0);
		System.out.println("TEXT = '" + s2.getText() + "'");
		assertEquals("<content ID=\"a4\" xmlns=\"urn:hl7-org:v3\">Anticipatory medicine box is issued and kept in refrigerator at patient home.</content>", s2.getText().trim());
	}
	
	@Test
	public void testRoundTrip1() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146229GB01_AH_01.xml", true);
		TextSection template = new TextSection();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("section", parentNamespaces);
		//FileWriter.writeFile("output.xml", result.getBytes());
		testXMLisSimilar("/TestData/Templates/COCD_TP146229GB01_AH_01.xml", result, true);
	}
	
	@Test
	public void testRoundTrip2() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146229GB01_AH_02.xml", true);
		TextSection template = new TextSection();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("section", parentNamespaces);
		//FileWriter.writeFile("output.xml", result.getBytes());
		testXMLisSimilar("/TestData/Templates/COCD_TP146229GB01_AH_02.xml", result, true);
	}
	
	@Test
	public void testRoundTrip3() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146229GB01_AH_03.xml", true);
		TextSection template = new TextSection();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("section", parentNamespaces);
		//FileWriter.writeFile("output.xml", result.getBytes());
		testXMLisSimilar("/TestData/Templates/COCD_TP146229GB01_AH_03.xml", result, true);
	}
}
