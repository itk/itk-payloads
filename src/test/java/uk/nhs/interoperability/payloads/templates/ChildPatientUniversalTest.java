/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType;
import uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class ChildPatientUniversalTest extends AbstractTest {
	
	public static ChildPatientUniversal full;
	
	static {
		full = createFull();
	}
	
	public static ChildPatientUniversal createFull() {
		ChildPatientUniversal template = new ChildPatientUniversal();

		template.addPatientID(new PatientIDWithTraceStatuses()
										.setPatientID("K12345")
										.setAssigningOrganisation("V396A:Medway PCT")
										.setPatientIDType(PatientIDType.LocalID.code));
		
		template.addPatientID(new PatientIDWithTraceStatuses()
										.setPatientID("9932541280")
										.setNHSNoTraceStatus(NHSNumberTraceStatus.TraceNeedsToBeResolved.code)
										.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));
		
		template.addAddress(new Address()
								.setAddressUse(AddressType.Home.code)
								.addAddressLine("Appleton House")
								.addAddressLine("Lanchester Road")
								.addAddressLine("Grimsby")
								.setPostcode("DN3 1UJ"));
		
		template.addName(new PersonName()
								.addGivenName("Mark")
								.setFamilyName("Smith"));
		
		template.setGender(Sex._Male);
		template.setDateOfBirth(new DateValue("20120728"));
		
		// Guardian
		ChildPatientGuardian guardian = new ChildPatientGuardian();
		
		guardian.addId(new PatientIDWithTraceStatuses()
									.setPatientID("K12344")
									.setPatientIDType(PatientIDType.LocalID.code)
									.setAssigningOrganisation("V396A:Medway PCT"));
		guardian.addId(new PatientIDWithTraceStatuses()
									.setPatientID("993254127")
									.setPatientIDType(PatientIDType.VerifiedNHSNumber.code)
									.setNHSNoTraceStatus(NHSNumberTraceStatus.Traced.code));

		guardian.setRole(GuardianRoleType._Mother);
		
		guardian.addAddress(new Address()
									.setAddressUse(AddressType.Home.code)
									.addAddressLine("Appleton House")
									.addAddressLine("Lanchester Road")
									.addAddressLine("Grimsby")
									.setPostcode("DN3 1UJ"));
		guardian.addTelephoneNumber(new Telecom()
										.setTelecom("tel:01634111678")
										.setTelecomType(TelecomUseType.HomeAddress.code));
		
		guardian.setGuardianDetails(new ChildPatientGuardianPerson()
											.setGuardianName(new PersonName("Mrs", "Shelly", "Smith")));
		
		template.addGuardian(guardian);
		
		// Provider Organisation
		template.setOrganisationId(new OrgID().setID("V396F"));
		template.setOrganisationName("Dr De Hopper and Partners");
		template.addOrganisationTelephone(new Telecom()
												.setTelecom("tel:01634111222")
												.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setOrganisationAddress(new Address()
											.addAddressLine("Freshney Green PCC")
											.addAddressLine("Grimsby")
											.setPostcode("DN34 4GB")
											.setAddressUse(AddressType.WorkPlace.code));
		template.setOrganisationType(CDAOrganizationProviderType._GPPractice);
		template.addOrganisationPartOf(new ChildPatientOrganisationPartOf()
												.addOrganisationId(new OrgID(OrgIDType.ODSOrgID.code, "V396G")));
		return template;
	}
	
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "ChildPatientUniversal-SerialiseTest", "ChildPatientUniversal: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = full.serialise("patientRole", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145230GB02_AH_01.xml", result, true);	
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
			
	@Test
	public void testRoundTrip() {
		super.init("templates/", "ChildPatientUniversal-roundTrip", "ChildPatientUniversal: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145230GB02_AH_01.xml", true);
			ChildPatientUniversal template = new ChildPatientUniversal();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("patientRole", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP145230GB02_AH_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip2() {
		super.init("templates/", "ChildPatientUniversal-roundTrip2", "ChildPatientUniversal: Round Trip Test 2", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145230GB02_AH_02.xml", true);
			ChildPatientUniversal template = new ChildPatientUniversal();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("patientRole", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP145230GB02_AH_02.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
