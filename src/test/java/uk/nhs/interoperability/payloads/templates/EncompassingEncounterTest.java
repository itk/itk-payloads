/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.DateRange;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.helpers.HelperUtils;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class EncompassingEncounterTest extends AbstractTest {
	
	public static EncompassingEncounter template;
	//public static EncompassingEncounter minimal;
	
	static {
		template = createFull();
		//minimal = createMinimal();
	}
	
	public static EncompassingEncounter createFull() {
		EncompassingEncounter template = new EncompassingEncounter();
		
		template.setId("3D3B95B5-24AA-42ED-9F77-BE7ECEB78C3E");
		template.setCode(CDAEncounterType._Consultation);
		template.setEffectiveTime(new DateRange(new DateValue("201105191945+01"),
												new DateValue("201105192015+01")));
		
		PersonWithOrganizationUniversal person = new PersonWithOrganizationUniversal();
		person.setJobRoleName(JobRoleName._MedicalDirector);
		person.addPersonId(new PersonID().setNullFlavour(NullFlavour.NI.code));
		person.setPersonName(new PersonName()
									.addGivenName("Dave")
									.setFamilyName("Cornwell"));
		person.setOrgId(new OrgID()
								.setID("VDE232323")
								.setType(OrgIDType.ODSOrgID.code));
		person.setOrgName("Medway South Walk-in Centre");
		template.setResponsibleParty(person);
		
		template.setEncounterCareSettingType(CDACareSettingType._313161000000107);
		
		return template;
	}
	
	/*public static EncompassingEncounter createMinimal() {
		EncompassingEncounter minimal = new EncompassingEncounter();
		return minimal;
	}*/
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "EncompassingEncounter-SerialiseTest", "Encompassing Encounter: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = template.serialise("encompassingEncounter", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146228GB01_01_AH.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/*
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "EncompassingEncounter-SerialiseMinimalTest", "Encompassing Encounter: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("encompassingEncounter", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146228GB01_02_AH.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}*/
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "EncompassingEncounter-roundTrip", "Encompassing Encounter: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146228GB01_01_AH.xml", true);
			EncompassingEncounter template = new EncompassingEncounter();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("encompassingEncounter", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP146228GB01_01_AH.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
