/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.helpers.HelperUtils;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;
import uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;

public class RelatedSubjectTest extends AbstractTest {
	
	public static RelatedSubject template;
	public static RelatedSubject minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static RelatedSubject createFull() {
		RelatedSubject template = new RelatedSubject();
		
		template.setRelationshipType(PersonalRelationshipRoleType._FamilyMember);
		template.addAddress(new Address()
						.addAddressLine("Appleton House")
						.addAddressLine("Lanchester Road")
						.setCity("Grimsby")
						.setPostcode("DN3 1UJ"));
		template.addTelephoneNumber(new Telecom("tel:01472354321"));
		template.addName(new PersonName("Mr", "Mark", "Smith"));
		template.setSex(Sex._Male);
		template.setBirthTime(new DateValue("19490101"));
		
		return template;
	}
	
	public static RelatedSubject createMinimal() {
		RelatedSubject minimal = new RelatedSubject();
		minimal.setRelationshipType(PersonalRelationshipRoleType._FamilyMember);
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "RelatedSubject-SerialiseTest", "Related Subject: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = template.serialise("relatedSubject", parentNamespaces);
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145213GB01_AH_01_Unverified.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "RelatedSubject-SerialiseMinimalTest", "Related Subject: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("relatedSubject", parentNamespaces);
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145213GB01_AH_02_Unverified.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "RelatedSubject-roundTrip", "Related Subject: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145213GB01_AH_01_Unverified.xml", true);
			RelatedSubject template = new RelatedSubject();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("relatedSubject", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP145213GB01_AH_01_Unverified.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}	
}
