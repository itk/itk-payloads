/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus;
import uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT;

public class NewBornHearingScreeningTest extends AbstractTest {
	
	public static NewBornHearingScreening full;
	
	static {
		full = createFull();
	}
	
	public static NewBornHearingScreening createFull() {
		NewBornHearingScreening template = new NewBornHearingScreening();

		template.setId("A1265DB0-89B1-11E3-AFED-19F46188709B");
		template.setScreeningOutcome(NewBornHearingScreeningOutcomeSnCT._Clearresponsenofollowuprequired);
		template.setAudiologyTestFinding(AudiologyTestingOutcomeStatus._No);
		template.setAudiologyReferralTime(new DateValue("201409111400+0000"));
		return template;
	}
	
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "NewBornHearingScreening-SerialiseTest", "NewBornHearingScreening: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = full.serialise("observation", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			
			
			testXMLisSimilar("/TestData/Templates/COCD_TP146003GB01_AH_01.xml", result, true);
			System.out.println(result);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "NewBornHearingScreening-roundTrip", "NewBornHearingScreening: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146003GB01_AH_01.xml", true);
			NewBornHearingScreening template = new NewBornHearingScreening();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("observation", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP146003GB01_AH_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
