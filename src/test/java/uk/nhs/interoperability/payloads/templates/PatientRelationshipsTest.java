/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class PatientRelationshipsTest extends AbstractTest {
	
	public static PatientRelationships full;
	
	static {
		full = createFull();
	}
	
	public static PatientRelationships createFull() {
		PatientRelationships template = new PatientRelationships();
		
 		template.setID("EF0C2E84-2A89-11E2-A57D-D9CB6088709B");
 		
 		PatientRelationshipParticipantRole rp = new PatientRelationshipParticipantRole();
 		rp.setPersonRole(JobRoleName._NR1980);
 		rp.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp.addTelephoneNumber(new Telecom("tel:014722823451"));
 		rp.addTelephoneNumber(new Telecom("tel:07831191333"));
 		rp.setPersonName(new PersonName()
 								.setTitle("Mrs")
 								.addGivenName("Emily")
 								.setFamilyName("Smith"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp));
		
 		PatientRelationshipParticipantRole rp2 = new PatientRelationshipParticipantRole();
 		rp2.setPersonRole(JobRoleName._NR1990);
 		rp2.setAssociatedJobRole(JobRoleName._NR2040);
 		rp2.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp2.addTelephoneNumber(new Telecom("tel:07833825701"));
 		rp2.setPersonName(new PersonName()
 								.setTitle("Ms")
 								.addGivenName("Niral")
 								.setFamilyName("Singh"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp2));
 		
 		PatientRelationshipParticipantRole rp3 = new PatientRelationshipParticipantRole();
 		rp3.setPersonRole(JobRoleName._NR2000);
 		rp3.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp3.addTelephoneNumber(new Telecom("tel:07822761939"));
 		rp3.setPersonName(new PersonName()
 								.setTitle("Ms")
 								.addGivenName("Gemma")
 								.setFamilyName("Hopkins"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp3));

 		PatientRelationshipParticipantRole rp4 = new PatientRelationshipParticipantRole();
 		rp4.setPersonRole(JobRoleName._NR2010);
 		rp4.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp4.addTelephoneNumber(new Telecom("tel:07871715735"));
 		rp4.setPersonName(new PersonName()
 								.setTitle("Mr")
 								.addGivenName("Alan")
 								.setFamilyName("Smith"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp4));

 		PatientRelationshipParticipantRole rp5 = new PatientRelationshipParticipantRole();
 		rp5.addID(new PersonID()
 						.setID("110")
 						.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
 						.setType(PersonIDType.LocalPersonID.code));
 		rp5.setPersonRole(JobRoleName._NR2020);
 		rp5.setAssociatedJobRole(JobRoleName._NR2050);
 		rp5.setAddress(new Address()
 								.addAddressLine("St James Hospital")
 								.addAddressLine("Potter Street")
 								.addAddressLine("Grimsby")
 								.setPostcode("DN3 6AA")
 								.setAddressUse(AddressType.WorkPlace.code));
 		rp5.addTelephoneNumber(new Telecom("tel:07334345612"));
 		rp5.setPersonName(new PersonName()
 								.setTitle("Mrs")
 								.addGivenName("Isabella")
 								.setFamilyName("Allen"));
 		rp5.setOrgId(new OrgID()
 							.setID("V356F")
 							.setType(OrgIDType.ODSOrgID.code));
 		rp5.setOrgDescription("St James Hospital");
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp5));

 		PatientRelationshipParticipantRole rp6 = new PatientRelationshipParticipantRole();
 		rp6.addID(new PersonID()
			.setID("112")
			.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
			.setType(PersonIDType.LocalPersonID.code));
 		rp6.setPersonRole(JobRoleName._NR2030);
 		rp6.setAssociatedJobRole(JobRoleName._NR0260);
 		rp6.setAddress(new Address()
 								.addAddressLine("Freshney Green PCC")
 								.addAddressLine("Carlton Court")
 								.addAddressLine("Martin Road")
 								.addAddressLine("Grimsby")
 								.setPostcode("DN34 4GB")
 								.setAddressUse(AddressType.WorkPlace.code));
 		rp6.addTelephoneNumber(new Telecom("tel:07621846951"));
 		rp6.setPersonName(new PersonName()
 								.setTitle("Dr")
 								.addGivenName("John")
 								.setFamilyName("Manning"));
 		rp6.setOrgId(new OrgID()
 							.setID("V365F")
 							.setType(OrgIDType.ODSOrgID.code));
 		rp6.setOrgDescription("Freshney Green PCC");
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp6));

 		
		return template;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("act", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146416GB01_AH_01.xml", result, true);
	}
			
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146416GB01_AH_01.xml", true);
		PatientRelationships template = new PatientRelationships();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("act", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP146416GB01_AH_01.xml", result, true);
	}
}
