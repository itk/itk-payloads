/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.templates.AnticipatoryMedicineBoxIssueProcedure;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT;

public class AnticipatoryMedicineBoxIssueProcedureTest extends AbstractTest {
	
	public static AnticipatoryMedicineBoxIssueProcedure full;
	
	static {
		full = createFull();
	}
	
	public static AnticipatoryMedicineBoxIssueProcedure createFull() {
		AnticipatoryMedicineBoxIssueProcedure template = new AnticipatoryMedicineBoxIssueProcedure();

		template.setID("227413CC-28FA-11E2-ADB5-72976188709B");
		
		// There is only one code in this vocab, so if the below is omitted, that code will still be used - however
		// we need to set it explicitly if we also want the code to include a cross-reference to a text section, as in this test
		template.setAnticipatoryMedicineBoxIssueCode(
					new CodedValue(EoLAnticipatoryMedicineBoxIssueSnCT._376201000000102, "#a4"));
		
		template.setTimeIssued(new DateValue("201209111400+0000"));
		template.setLocationOfBox("Bedside Table Drawer");
		
		return template;
	}
	
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("procedure", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146418GB01_AH_01.xml", result, true);
	}
			
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146418GB01_AH_01.xml", true);
		AnticipatoryMedicineBoxIssueProcedure template = new AnticipatoryMedicineBoxIssueProcedure();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("procedure", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP146418GB01_AH_01.xml", result, true);
	}
}
