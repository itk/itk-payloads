/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;

public class AttachmentTest extends AbstractTest {
	
	public static Attachment template;
	public static Attachment minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static Attachment createFull() {
		Attachment template = new Attachment();
		
		template.setId("7DAC2CE0-AE1A-11DB-98EE-B18E1E0994CD");
		template.setReferenceId("ID_DAC2CE0-AE1A-11DB-98EE-B18E1E0994CD");
		template.setAttachmentType(AttachmentType.Base64.code);
		template.setMediaType("text/xml");
		String data = FileLoader.loadFileOnClasspath("/TestData/Templates/attachment1.xml");
		// Using the standard Java 6 base64 encoder
		String base64data = Base64.encodeBase64String(data.getBytes());
		template.setAttachment(base64data);
		
		template.setTimeAuthored(new DateValue("20130101"));
		AuthorPersonUniversal author = AuthorPersonUniversalTest.createFull();
		template.setAuthor(author);
		
		RelatedEntity informant = RelatedEntityTest.createFull();
		template.setInformant(informant);

		
		AttachmentSubject subject = new AttachmentSubject();
		subject.setAwarenessCode(TargetAwareness._fullawareness);
		
		subject.setSubjectRelationshipType(PersonalRelationshipRoleType._FamilyMember);
		subject.addSubjectAddress(new Address()
						.addAddressLine("Appleton House")
						.addAddressLine("Lanchester Road")
						.setCity("Grimsby")
						.setPostcode("DN3 1UJ"));
		subject.addSubjectTelephoneNumber(new Telecom("tel:01472354321"));
		subject.addSubjectName(new PersonName("Mr", "Mark", "Smith"));
		subject.setSubjectSex(Sex._Male);
		subject.setSubjectBirthTime(new DateValue("19490101"));
		template.addAttachmentSubject(subject);
		
		return template;
	}
	
	public static Attachment createMinimal() {
		Attachment minimal = new Attachment();
		minimal.setId("7DAC2CE0-AE1A-11DB-98EE-B18E1E0994CD");
		minimal.setReferenceId("ID_DAC2CE0-AE1A-11DB-98EE-B18E1E0994CD");
		minimal.setAttachmentType(AttachmentType.Base64.code);
		minimal.setMediaType("text/xml");
		String data = FileLoader.loadFileOnClasspath("/TestData/Templates/attachment1.xml");
		// Using the standard Java 6 base64 encoder
		String base64data = Base64.encodeBase64String(data.getBytes());
		minimal.setAttachment(base64data);
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "Attachment-SerialiseTest", "Attachment: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = template.serialise("observationMedia", parentNamespaces);
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146224GB02_AH_01_Unverified.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "Attachment-SerialiseMinimalTest", "Attachment: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("observationMedia", parentNamespaces);
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146224GB02_AH_02.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "Attachment-roundTrip", "Attachment: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146224GB02_AH_01_Unverified.xml", true);
			Attachment template = new Attachment();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("observationMedia", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP146224GB02_AH_01_Unverified.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
