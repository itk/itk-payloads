/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.DateRange;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.helpers.HelperUtils;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;
import uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class ServiceEventTest extends AbstractTest {
	
	public static ServiceEvent template;
	public static ServiceEvent minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static ServiceEvent createFull() {
		ServiceEvent template = new ServiceEvent();
		
		template.setId("8371D2F1-123F-4A14-A1AC-C6C8023103CF");
		template.setClassCode(HL7ActType.Procedure.code);
		template.setEventCode(new CodedValue("73761001", "colonoscopy", "2.16.840.1.113883.2.1.3.2.4.15"));
		
		template.setEffectiveTime(
						new DateRange(new DateValue("201105192000+01"),
									  new DateValue("201105192045+01")));
		
		PersonWithOrganizationUniversal person = new PersonWithOrganizationUniversal();
		person.addPersonId(new PersonID().setNullFlavour(NullFlavour.NI.code));
		person.setPersonName(new PersonName("Joe Bloggs"));
		person.setOrgId(new OrgID()
								.setType(OrgIDType.ODSOrgID.code)
								.setID("8785675885765767"));
		person.setOrgName("xx organisation");
		
		template.addEventPerformer(
				new ServiceEventPerformer(person, HL7PerformerType.Performer.code));
		
		return template;
	}
	
	public static ServiceEvent createMinimal() {
		ServiceEvent minimal = new ServiceEvent();
		//minimal.setRelationshipType(PersonalRelationshipRoleType._FamilyMember);
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "ServiceEvent-SerialiseTest", "Service Event: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = template.serialise("serviceEvent", parentNamespaces);
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146227GB02_AH_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/*
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "ServiceEvent-SerialiseMinimalTest", "Service Event: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("serviceEvent", parentNamespaces);
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146227GB02_AH_02.xml.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	*/
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "ServiceEvent-roundTrip", "Service Event: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146227GB02_AH_01.xml", true);
			ServiceEvent template = new ServiceEvent();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("serviceEvent", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP146227GB02_AH_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}	
}
