/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class PersonWithOrganizationUniversalTest extends AbstractTest {

	private static PersonWithOrganizationUniversal full;
	private static PersonWithOrganizationUniversal minimal;
	
	static {
		full = createFull();
		minimal = createMinimal();
	}
	
	public static PersonWithOrganizationUniversal createFull() {
		PersonWithOrganizationUniversal template = new PersonWithOrganizationUniversal();

		template.addPersonId(new PersonID()
									.setID("10987641")
									.setType(PersonIDType.SDSID.code));

		template.addPersonId(new PersonID()
									.setID("20354681")
									.setType(PersonIDType.SDSRoleProfile.code));

		template.setJobRoleName(JobRoleName._NR0070);
		
		template.setAddress(new Address()
									.addAddressLine("21, County Lodge")
									.addAddressLine("Woodtown")
									.setCity("Medway")
									.setPostcode("ME5 FS1"));
		
		template.addTelephoneNumber(new Telecom("tel:01634446995"));
		
		template.setPersonName(new PersonName()
									.setTitle("Dr")
									.addGivenName("Peter")
									.setFamilyName("Thomson"));
		
		template.setOrgId(new OrgID()
								.setID("XZ901")
								.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrgName("St. Elsewhere's Hospital");
		
		return template;
	}
	
	public static PersonWithOrganizationUniversal createMinimal() {
		PersonWithOrganizationUniversal template = new PersonWithOrganizationUniversal();
		
		template.addPersonId(new PersonID().setNullFlavour(NullFlavour.NI.code));
		
		template.setPersonName(new PersonName()
				.setTitle("Dr")
				.addGivenName("Peter")
				.setFamilyName("Thomson"));
		
		template.setOrgId(new OrgID()
			.setID("XZ901")
			.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrgName("St. Elsewhere's Hospital");

		return template;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("assignedEntity", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145210GB01_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("assignedEntity", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145210GB01_02.xml", result, true);
	}
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145210GB01_01.xml", true);
		PersonWithOrganizationUniversal template = new PersonWithOrganizationUniversal();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("assignedEntity", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145210GB01_01.xml", result, true);
	}
}
