/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class RecipientPersonUniversalv1Test extends AbstractTest {
	
	public static RecipientPersonUniversalv1 template;
	public static RecipientPersonUniversalv1 minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static RecipientPersonUniversalv1 createFull() {
		RecipientPersonUniversalv1 template = new RecipientPersonUniversalv1();
		
		template.addId(new RoleID()
							.setID("13579")
							.setAssigningOrganisation("AB1:Medway NHS Foundation Trust"));
		
		template.setJobRoleName(JobRoleName._NR0050);
		
		template.addTelephoneNumber(new Telecom("tel:01634446995"));
		
		template.setName(new PersonName()
								.setTitle("Dr")
								.addGivenName("Peter")
								.setFamilyName("Thomson"));
		
		template.setOrgId(new OrgID().setID("RPA").setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("Medway NHS Foundation Trust");
		template.setCDAOrganizationType(CDAOrganizationType._008);
		
		return template;
	}
	
	public static RecipientPersonUniversalv1 createMinimal() {
		RecipientPersonUniversalv1 minimal = new RecipientPersonUniversalv1();

		minimal.setName(new PersonName()
								.setTitle("Dr")
								.addGivenName("Peter")
								.setFamilyName("Thomson"));
		
		minimal.setOrgId(new OrgID().setID("RPA").setType(OrgIDType.ODSOrgID.code));
		minimal.setOrgName("Medway NHS Foundation Trust");
		
		return minimal;
	}
	
	public static RecipientPersonUniversalv1 createEOLCDocRecipient() {
		RecipientPersonUniversalv1 eolcRecipient = new RecipientPersonUniversalv1();

		eolcRecipient.setName(new PersonName("De Hopper"));
		eolcRecipient.setOrgId(new OrgID().setID("V396F").setType(OrgIDType.ODSOrgID.code));
		eolcRecipient.setOrgName("Dr De Hopper and Partners");
		
		return eolcRecipient;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("intendedRecipient", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145202GB01_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("intendedRecipient", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145202GB01_02.xml", result, true);
	}
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145202GB01_01.xml", true);
		RecipientPersonUniversalv1 template = new RecipientPersonUniversalv1();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("intendedRecipient", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145202GB01_01.xml", result, true);
	}
}
