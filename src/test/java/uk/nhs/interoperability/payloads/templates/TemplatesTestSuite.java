/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AdvanceDecisionToRefuseTreatmentTest.class,
				AnticipatoryMedicineBoxIssueProcedureTest.class,
				AttachmentTest.class,
				AuthorDeviceUniversalTest.class,
				AuthorityToLastingPowerOfAttorneyTest.class,
				AuthorNonNamedPersonUniversalTest.class,
				AuthorPersonUniversalTest.class,
				BloodSpotScreeningTest.class,
				ChildPatientUniversalTest.class,
				ConsentTest.class,
				CustodianOrganizationUniversalTest.class,
				DocumentParticipantUniversalTest.class,
				DNACPRDecisionBySeniorResponsibleClinicianTest.class,
				EncompassingEncounterTest.class,
				EOLCarePlanTest.class,
				HealthCareFacilityUniversalTest.class,
				NewBornBirthDetailsTest.class,
				NewBornHearingScreeningTest.class,
				NewBornPhysicalExaminationTest.class,
				ParticipantOrganizationUniversalTest.class,
				PatientRelationshipsTest.class,
				PatientUniversalTest.class,
				PatientUniversalv2Test.class,
				PersonUniversalTest.class,
				PersonWithOrganizationUniversalTest.class,
				PrognosisAwarenessTest.class,
				RecipientOrganizationUniversalTest.class,
				RecipientOrganizationUniversalv2Test.class,
				RecipientPersonUniversalTest.class,
				RecipientPersonUniversalv1Test.class,
				RecipientWorkgroupUniversalTest.class,
				RecipientWorkgroupUniversalv2Test.class,
				RelatedEntityTest.class,
				RelatedSubjectTest.class,
				ServiceEventTest.class,
				TextSectionTest.class,
				WorkgroupUniversalTest.class})
public class TemplatesTestSuite {
}
