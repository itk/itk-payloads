/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class PersonUniversalTest extends AbstractTest {

	private static PersonUniversal full;
	private static PersonUniversal minimal;
	
	static {
		full = createFull();
		minimal = createMinimal();
	}
	
	public static PersonUniversal createFull() {
		PersonUniversal template = new PersonUniversal();

		template.addPersonId(new PersonID()
									.setID("ABC123")
									.setType(PersonIDType.SDSID.code));

		template.addPersonId(new PersonID()
									.setID("12345")
									.setType(PersonIDType.SDSRoleProfile.code));

		template.setJobRoleName(JobRoleName._NR0050);
				
		template.addTelephoneNumber(new Telecom("tel:01634445697"));
		
		template.setPersonName(new PersonName()
									.setTitle("Dr")
									.addGivenName("Bruce")
									.setFamilyName("Weller"));
		
		template.setOrgId(new OrgID()
								.setID("XZ901")
								.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrgName("St. Elsewhere's Hospital");
		
		return template;
	}
	
	public static PersonUniversal createMinimal() {
		PersonUniversal template = new PersonUniversal();
		
		template.addPersonId(new PersonID().setNullFlavour(NullFlavour.NI.code));
		
		template.setPersonName(new PersonName()
				.setTitle("Dr")
				.addGivenName("Bruce")
				.setFamilyName("Weller"));
		
		return template;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "PersonUniversal-SerialiseTest", "PersonUniversal: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = full.serialise("assignedEntity", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145205GB01_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "PersonUniversal-SerialiseMinimalTest", "PersonUniversal: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("assignedEntity", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145205GB01_02.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		super.init("templates/", "PersonUniversal-roundTrip", "PersonUniversal: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145205GB01_01.xml", true);
			PersonUniversal template = new PersonUniversal();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("assignedEntity", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP145205GB01_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
