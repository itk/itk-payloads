/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative;
import uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class DocumentParticipantUniversalTest extends AbstractTest {
	
	public static DocumentParticipantUniversal template;
	public static DocumentParticipantUniversal minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static DocumentParticipantUniversal createFull() {
		DocumentParticipantUniversal template = new DocumentParticipantUniversal();
		
		template.setRoleClass(RoleClassAssociative._ASSIGNED.code);
		template.addId(new PersonID()
							.setID("10987641")
							.setType(PersonIDType.SDSID.code));
		
		template.addId(new PersonID()
							.setID("20354681")
							.setType(PersonIDType.SDSRoleProfile.code));
		
		template.setRoleCode(RoleCode._Hospital);
		
		template.setAddress(new Address()
									.addAddressLine("21, County Lodge")
									.addAddressLine("Woodtown")
									.setCity("Medway")
									.setPostcode("ME5 FS1"));
		
		template.addTelephoneNumber(new Telecom("tel:01634446995"));
		
		template.setName(new PersonName()
								.setTitle("Dr")
								.addGivenName("Peter")
								.setFamilyName("Thomson"));
		
		template.setOrgId(new OrgID().setID("XZ901").setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("St. Elsewhere's Hospital");
		
		return template;
	}
	
	public static DocumentParticipantUniversal createMinimal() {
		DocumentParticipantUniversal minimal = new DocumentParticipantUniversal();
		minimal.setRoleClass(RoleClassAssociative._ASSIGNED.code);
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "DocumentParticipantUniversal-SerialiseTest", "DocumentParticipantUniversal: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = template.serialise("associatedEntity", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145214GB01_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "DocumentParticipantUniversal-SerialiseMinimalTest", "DocumentParticipantUniversal: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("associatedEntity", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145214GB01_02.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		super.init("templates/", "DocumentParticipantUniversal-roundTrip", "DocumentParticipantUniversal: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145214GB01_01.xml", true);
			DocumentParticipantUniversal template = new DocumentParticipantUniversal();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("associatedEntity", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP145214GB01_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
