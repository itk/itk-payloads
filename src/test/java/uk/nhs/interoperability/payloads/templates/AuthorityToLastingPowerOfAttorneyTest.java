/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;

public class AuthorityToLastingPowerOfAttorneyTest extends AbstractTest {
	
	public static AuthoritytoLastingPowerofAttorney full;
	
	static {
		full = createFull();
	}
	
	public static AuthoritytoLastingPowerofAttorney createFull() {
		AuthoritytoLastingPowerofAttorney template = new AuthoritytoLastingPowerofAttorney();

		template.setID("26919440-2A88-11E2-A205-1ECA6088709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setAuthoritytoLPA(new CodedValue(AuthoritytoLPASnCT._816381000000105, "#a7"));
		
		PatientRelationshipParticipantRole person = new PatientRelationshipParticipantRole(); 
		person.setPersonRole(JobRoleName._NR2010);
		person.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
		person.addTelephoneNumber(new Telecom("tel:07871715735"));
		person.setPersonName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Alan")
									.setFamilyName("Smith"));
		
		template.setLPADetails(person);
		
		return template;
	}
	
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("observation", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146417GB01_AH_01.xml", result, true);
	}
			
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146417GB01_AH_01.xml", true);
		AuthoritytoLastingPowerofAttorney template = new AuthoritytoLastingPowerofAttorney();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("observation", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP146417GB01_AH_01.xml", result, true);
	}
}
