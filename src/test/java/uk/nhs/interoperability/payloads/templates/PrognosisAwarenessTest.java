/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;

public class PrognosisAwarenessTest extends AbstractTest {
	
	public static PrognosisAwareness template;
	public static PrognosisAwareness minimal;
	
	static {
		template = createFull();
		//minimal = createMinimal();
	}
	
	public static PrognosisAwareness createFull() {
		PrognosisAwareness template = new PrognosisAwareness();
		
		template.setID("369FBB7A-2A86-11E2-B118-84C76088709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setPrognosisAwareness(PrognosisAwarenessSnCT._751941000000100);
		// Main informal carer
		PatientRelationshipParticipantRole carer = new PatientRelationshipParticipantRole();
		carer.setPersonRole(JobRoleName._NR1980);
		carer.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
		//carer.setAddressNullFlavour(NullFlavour.NI.code);
		carer.addID(new PersonID().setNullFlavour(NullFlavour.NI.code));
		carer.addTelephoneNumber(new Telecom("tel:014722823451"));
		carer.addTelephoneNumber(new Telecom("tel:07831191333"));
		carer.setPersonName(new PersonName()
									.setTitle("Mrs")
									.addGivenName("Emily")
									.setFamilyName("Smith"));
		template.setMainInformalCarer(carer);
		return template;
	}
	
	public static PrognosisAwareness createMinimal() {
		PrognosisAwareness minimal = new PrognosisAwareness();
		
		
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		try {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("observation", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146414GB01.xml", result, true);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception");
		}
	}
	/*
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146414GB01.xml", result, true);
	}*/
	
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146414GB01.xml", true);
		PrognosisAwareness template = new PrognosisAwareness();
		template.parse(expectedResult, parentNamespaces);
		
		PatientRelationshipParticipantRole carer = template.getMainInformalCarer();
		
		System.out.println("\n\n\n    Carer Address: " + carer.getAddress() + "\n\n");
		
		String result = template.serialise("observation", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP146414GB01.xml", result, true);
	}
}
