/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.helpers.HelperUtils;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;

public class RelatedEntityTest extends AbstractTest {
	
	public static RelatedEntity template;
	public static RelatedEntity minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static RelatedEntity createFull() {
		RelatedEntity template = new RelatedEntity();
		
		template.setCDAPersonRelationshipType(CDAPersonRelationshipType._Spouse);
		template.setAddress(new Address()
						.addAddressLine("Appleton House")
						.addAddressLine("Lanchester Road")
						.setCity("Grimsby")
						.setPostcode("DN3 1UJ"));
		template.setTelephone(new Telecom("tel:01472354321"));
		template.setName(new PersonName("Mr", "Mark", "Smith"));
		return template;
	}
	
	public static RelatedEntity createMinimal() {
		RelatedEntity minimal = new RelatedEntity();
		minimal.setCDAPersonRelationshipType(CDAPersonRelationshipType._Spouse);
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "RelatedEntity-SerialiseTest", "Related Entity: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = template.serialise("relatedEntity", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145007UK03_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		super.init("templates/", "RelatedEntity-SerialiseMinimalTest", "Related Entity: Serialise Minimal Test", "This uses the itk-payloads library to generate a serialisation of a minimal version of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = minimal.serialise("relatedEntity", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP145007UK03_02.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testParse() {
		super.init("templates/", "RelatedEntity-ParseTest", "Related Entity: Parse Test", "This uses the itk-payloads library to parse an example of the template. The values parsed are then compared with what we expect to get.");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145007UK03_01.xml", true);
			RelatedEntity template = new RelatedEntity();
			template.parse(expectedResult, parentNamespaces);
			
			PersonName name = template.getName();
			assertEquals("Smith", name.getFamilyName());
			
			/*CDAPersonRelationshipType relationshipType =
						(CDAPersonRelationshipType)	
						HelperUtils.safelyMapCodedValueToVocabEntry(template.getCDAPersonRelationshipType(),
																	CDAPersonRelationshipType._01);
			*/
			CDAPersonRelationshipType relationshipType = template.getCDAPersonRelationshipTypeEnum();
			
			assertTrue(relationshipType.sameAs(CDAPersonRelationshipType._Spouse));
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "RelatedEntity-roundTrip", "Related Entity: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145007UK03_01.xml", true);
			RelatedEntity template = new RelatedEntity();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("relatedEntity", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP145007UK03_01.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
