/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity;
import uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode;
import uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class PatientUniversalv2Test extends AbstractTest {
	
	public static PatientUniversalv2 template;
	public static PatientUniversalv2 minimal;
	
	static {
		template = createFull();
		//minimal = createMinimal();
		
		//TODO: Implement tests for PatientOrganisationPartOf elements
		
	}
	
	public static PatientUniversalv2 createFull() {
		PatientUniversalv2 template = new PatientUniversalv2();
		
		// PatientRole:
		template.addPatientID(new PatientIDWithTraceStatuses()
									.setPatientID("1234567899")
									.setPatientIDType(PatientIDType.VerifiedNHSNumber.code));
		
		template.addAddress(new Address()
									.addAddressLine("21, Grove Street")
									.addAddressLine("Overtown")
									.addAddressLine("Leeds")
									.addAddressLine("West Yorkshire")
									.setPostcode("LS21 1PF")
									.setAddressUse(AddressType.Home.code));
		
		// Patient
		template.addPatientName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Richard")
									.setFamilyName("Smith"));
		template.setSex(Sex._Male);
		template.setBirthTime(new DateValue("19570101"));
		template.setEthnicGroup(Ethnicity._British);
		
		// Guardian
		PatientGuardian guardian = new PatientGuardian();
		guardian.setRole(GuardianRoleType._Relative);
		guardian.addTelephoneNumber(new Telecom("tel:0712345678")
										.setTelecomType(TelecomUseType.MobileContact.code));
		guardian.addTelephoneNumber(new Telecom("mailto:mrssmith@mymail.com"));
		guardian.setGuardianDetails(new PatientGuardianPerson(
											new PersonName("Mrs", "Joy", "Smith")
														.setNameType(PersonNameType.Preferred.code)));
		template.setGuardian(guardian);
		
		// Organisation
		template.setOrganisationId(new OrgID()
										.setID("GP123456")
										.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrganisationName("MGP Medical Centre");
		template.addOrganisationTelephone(new Telecom("tel:01634111222")
											.setTelecomType(TelecomUseType.WorkPlace.code));
		template.addOrganisationTelephone(new Telecom("mailto:jlorenzo@MMC.co.uk"));
		
		template.setOrganisationAddress(new Address()
											.addAddressLine("1 MGP House")
											.addAddressLine("Overtown")
											.addAddressLine("Leeds")
											.addAddressLine("West Yorkshire")
											.setPostcode("LS21 7PA")
											.setAddressUse(AddressType.WorkPlace.code));
		template.setOrganisationType(CDAOrganizationProviderType._GPPractice);
		
		return template;
	}
	
	/*public static PatientUniversalv2 createMinimal() {
		PatientUniversalv2 minimal = new PatientUniversalv2();
		minimal.addPatientID(new PatientID()
								.setPatientID("K12345")
								.setAssigningOrganisation("V396A:Medway PCT")
								.setPatientIDType(PatientIDType.LocalID.code));
		minimal.addPatientID(new PatientID()
								.setPatientID("993254128")
								.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));
		minimal.addPatientName(new PersonName().setNullFlavour(NullFlavour.Unknown.code));
		minimal.setSexNullFlavour(NullFlavour.Unknown.code);
		minimal.setBirthTimeNullFlavour(NullFlavour.Unknown.code);
		return minimal;
	}*/
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("patientRole", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145201GB02_AH_01.xml", result, true);
	}
/*	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("patientRole", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145201GB01_02.xml", result, true);
	}
*/
	@Test
	public void testRoundTrip() throws SAXException, IOException {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145201GB02_AH_01.xml", true);
		PatientUniversalv2 template = new PatientUniversalv2();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("patientRole", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145201GB02_AH_01.xml", result, true);
	}
}
