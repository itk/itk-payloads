/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.commontypes.WorkgroupID;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.WorkgroupIDType;

public class WorkgroupUniversalTest extends AbstractTest {
	
	public static WorkgroupUniversal full;
	public static WorkgroupUniversal minimal;
	
	static {
		full = createFull();
		minimal = createMinimal();
	}
	
	public static WorkgroupUniversal createFull() {
		WorkgroupUniversal template = new WorkgroupUniversal();
		
 		template.setID(new WorkgroupID()
 								.setID("1000998765123")
 								.setType(WorkgroupIDType.SDSWorkgroupID.code));
 		
 		template.setWorkgroupName(new CodedValue("01","MEDWAYSPECIALISTTEAM"));
		
 		template.addTelephoneNumber(new Telecom("tel:01634445667"));
 		
 		template.setPersonName(new PersonName()
 										.setTitle("Dr")
 										.addGivenName("Nick")
 										.setFamilyName("Smith"));
 		
		template.setOrgId(new OrgID()
								.setID("XZ901")
								.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrgName("St. Elsewhere's Hospital");
		
		return template;
	}
	
	public static WorkgroupUniversal createMinimal() {
		WorkgroupUniversal template = new WorkgroupUniversal();

		template.setWorkgroupName(new CodedValue("01","MEDWAYSPECIALISTTEAM"));
		 		
		template.setOrgId(new OrgID()
								.setID("XZ901")
								.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrgName("St. Elsewhere's Hospital");
		
		return template;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145212GB02_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145212GB02_02.xml", result, true);
	}
		
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145212GB02_01.xml", true);
		WorkgroupUniversal template = new WorkgroupUniversal();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("assignedAuthor", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145212GB02_01.xml", result, true);
	}
}
