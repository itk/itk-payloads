/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.commontypes.ConsentID;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT;

public class ConsentTest extends AbstractTest {
	
	public static Consent full;
	
	static {
		full = createFull();
	}
	
	public static Consent createFull() {
		Consent template = new Consent();
		
		template.addID(new ConsentID().setRoot("7AFDCAD6-1CE3-45A6-A176-DDAC27F46C87"));
 		template.setConsentCode(DocumentConsentSnCT._319951000000105);
		
		return template;
	}
		
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("consent", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146226GB02_AH_01.xml", result, true);
	}
			
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146226GB02_AH_01.xml", true);
		Consent template = new Consent();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("consent", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP146226GB02_AH_01.xml", result, true);
	}
}
