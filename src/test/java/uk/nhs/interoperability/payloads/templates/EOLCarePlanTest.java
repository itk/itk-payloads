/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class EOLCarePlanTest extends AbstractTest {
	
	public static EoLCarePlan full;
	
	static {
		full = createFull();
	}
	
	public static EoLCarePlan createFull() {
		EoLCarePlan template = new EoLCarePlan();

		template.setID("EBDF8AE8-28F5-11E2-9460-80936188709B");
		template.setEffectiveFrom(new DateValue("201209111400+0000"));
		
		template.setOriginalTimeAuthored(new DateValue("201209111400+0000"));
		AuthorPersonUniversal originalAuthor = new AuthorPersonUniversal();
		originalAuthor.addId(new PersonID()
									.setID("201")
									.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
									.setType(PersonIDType.LocalPersonID.code));
		originalAuthor.setJobRoleName(JobRoleName._NR2040);
		originalAuthor.setName(new PersonName()
									.setTitle("Ms")
									.addGivenName("Niral")
									.setFamilyName("Singh"));
		originalAuthor.setOrganisationId(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		originalAuthor.setOrganisationName("St James Hospital");
		template.setOriginalAuthor(originalAuthor);
		
		template.setLastAmendedTimeAuthored(new DateValue("201209111415+0000"));
		template.setLastAmendedAuthor(originalAuthor);

		template.setEoLToolSnCT(new CodedValue(EoLToolSnCT._818221000000103, "#a1"));
		template.setEOLToolStage("first stage");
		template.setPlannedReviewDate(new DateValue("20121011"));
		
		EOLPatientRelationships relationships = new EOLPatientRelationships();
		relationships.setRelationships(PatientRelationshipsTest.createFull());
		
		template.addPatientRelationships(new EOLPatientRelationships(PatientRelationshipsTest.createFull()));

		return template;
	}
	
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = full.serialise("act", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP146419GB01_AH_01.xml", result, true);
	}
			
	/*@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146419GB01_AH_01.xml", true);
		EoLCarePlan template = new EoLCarePlan();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("act", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP146419GB01_AH_01.xml", result, true);
	}*/
}
