/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.commontypes.DeviceID;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class AuthorDeviceUniversalTest extends AbstractTest {
	
	public static AuthorDeviceUniversal template;
	public static AuthorDeviceUniversal minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static AuthorDeviceUniversal createFull() {
		AuthorDeviceUniversal template = new AuthorDeviceUniversal();
		
 		template.addDeviceId(new DeviceID().setID("123456"));
		template.setManufacturerModelName(new CodedValue("OMRON-M2", "BASIC BP DEVICE"));
		template.setSoftwareName(new CodedValue("B003CYK6FA", "OMRON M2 BP MONITOR"));
		template.setOrgId(new OrgID()
								.setID("SITE1234")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("MEDWAY HEATH CENTRE");
		
		return template;
	}
	
	public static AuthorDeviceUniversal createMinimal() {
		AuthorDeviceUniversal minimal = new AuthorDeviceUniversal();
				
 		minimal.setManufacturerModelName(new CodedValue("OMRON-M2", "BASIC BP DEVICE"));
 		minimal.setSoftwareName(new CodedValue("B003CYK6FA", "OMRON M2 BP MONITOR"));
 		minimal.setOrgId(new OrgID()
								.setID("SITE1234")
								.setType(OrgIDType.ODSOrgID.code));
 		minimal.setOrgName("MEDWAY HEATH CENTRE");
		
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145207GB01_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145207GB01_02.xml", result, true);
	}
		
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145207GB01_01.xml", true);
		AuthorDeviceUniversal template = new AuthorDeviceUniversal();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("assignedAuthor", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145207GB01_01.xml", result, true);
	}
}
