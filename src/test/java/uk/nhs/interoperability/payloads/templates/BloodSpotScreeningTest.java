/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes;
import uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class BloodSpotScreeningTest extends AbstractTest {
	public static BloodSpotScreening full;
	
	static {
		full = createFull();
	}
	
	public static BloodSpotScreening createFull() {
		BloodSpotScreening template = new BloodSpotScreening();

		template.setId("2B6D3E28-3B0A-11E2-B7AF-B4F66183783C");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		
		// Performer
		template.setSampleCollectedTime(new DateValue("201209111300"));
		template.addPerformerPersonId(new PersonID().setNullFlavour(NullFlavour.Unknown.code));
		template.setPerformerPersonName(new PersonName("Mr", "Brian", "Weberly"));
		template.setPerformerOrgId(new OrgID()
											.setID("A0991")
											.setType(OrgIDType.ODSOrgID.code));
		template.setPerformerOrgName("St James Hospital");
		
		// Lab
		template.setTimeReceivedAtLab(new DateValue("201209111310"));
		template.setLabOrganisationId(new OrgID()
											.setID("A0078")
											.setType(OrgIDType.ODSOrgID.code));
		template.setLabOrganisationDescription("St James Pathology Laboratory");
		
		// Screening results
		template.setPKUScreeningValue(PKUScreeningResult._Conditionnotsuspected);
		template.setPKUBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
		template.setPKUScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setPKUReasonText("PKU Reason Text");
		template.setPKUSupplementaryText("PKU supplementary text");
		
		template.setSCDScreeningValue(SCDScreeningResult._Conditionnotsuspected);
		template.setSCDScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setSCDReasonText("SCD Reason Text");
		template.setSCDSupplementaryText("SCD supplementary text");
		
		template.setCFScreeningValue(CFScreeningResult._Conditionnotsuspected);
		template.setCFScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setCFReasonText("CFS Reason Text");
		template.setCFSupplementaryText("CFS supplementary text");
		
		template.setCHTScreeningValue(CHTScreeningResult._Conditionnotsuspected);
		template.setCHTScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setCHTReasonText("CHT Reason Text");
		template.setCHTSupplementaryText("CHT supplementary text");
		
		template.setMCADDScreeningValue(MCADDScreeningResult._Conditionnotsuspected);
		template.setMCADDScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setMCADDReasonText("MCADD Reason Text");
		template.setMCADDSupplementaryText("MCADD supplementary text");
		
		// Other observations
		template.setScreeningLocationStatus(BSScreeningLocStatus._NotDischargedOtherspecialistunit);
		template.setLaboratoryCardSerialNumber("0902652801");
		template.setPreviousLaboratoryCardSerialNumber("0902652800");
		
		return template;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		super.init("templates/", "BloodSpotScreening-SerialiseTest", "BloodSpotScreening: Serialise Test", "This uses the itk-payloads library to generate a serialisation of the template. This is then serialised and compared against the example developed by the messaging team to ensure it matches.");
		try {
			// Normally this template would inherit namespaces from the overall
			// document, but as we are calling it directly we need to provide them
			// when we serialise. We will also override the root element name to match
			// the one used in the sample XML.
			String result = full.serialise("organizer", parentNamespaces);
			
			// The sample XML does not include any namespace declarations, so we will
			// add some as our configuration expects all the elements to be in the 
			// correct default namespace for HL7v3 (urn:hl7-org:v3).
			testXMLisSimilar("/TestData/Templates/COCD_TP146005GB02_AH_1.xml", result, true);	
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testRoundTrip() {
		super.init("templates/", "BloodSpotScreening-roundTrip", "BloodSpotScreening: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP146005GB02_AH_1.xml", true);
			BloodSpotScreening template = new BloodSpotScreening();
			template.parse(expectedResult, parentNamespaces);
			String result = template.serialise("organizer", parentNamespaces);
			testXMLisSimilar("/TestData/Templates/COCD_TP146005GB02_AH_1.xml", result, true);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

}
