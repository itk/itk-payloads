/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class AuthorPersonUniversalTest extends AbstractTest {
	
	public static AuthorPersonUniversal template;
	public static AuthorPersonUniversal minimal;
	
	static {
		template = createFull();
		minimal = createMinimal();
	}
	
	public static AuthorPersonUniversal createFull() {
		AuthorPersonUniversal template = new AuthorPersonUniversal();
		
 		template.addId(new PersonID()
 						.setType(PersonIDType.SDSRoleProfile.code)
						.setID("200025166218"));
 		template.addId(new PersonID()
						.setType(PersonIDType.SDSID.code)
						.setID("100099876439"));
 		template.addId(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("123456")
 						.setAssigningOrganisation("AB1:MEDWAY NHS TRUST"));		
		template.setName(new PersonName()
						.setTitle("Dr")
						.addGivenName("Nick")
						.setFamilyName("Smith"));
		template.addAddress(new Address()
						.addAddressLine("21, County Lodge")
						.addAddressLine("Woodtown")
						.setCity("Medway")
						.setPostcode("ME5 FS1"));
		template.setJobRoleName(JobRoleName._NR0050);
		template.setOrganisationName("St. Elsewhere's Hospital");
		template.setOrganisationId(new OrgID()
									.setID("XZ901")
									.setType(OrgIDType.ODSOrgID.code));
		template.addTelephoneNumber(new Telecom("tel:01634445667"));
		return template;
	}
	
	public static AuthorPersonUniversal createMinimal() {
		AuthorPersonUniversal minimal = new AuthorPersonUniversal();
		minimal.addId(new PersonID().setNullFlavour(NullFlavour.NI.code));
		minimal.setJobRoleNameNullFlavour(NullFlavour.NI.code);
		minimal.setName(new PersonName()
						.setTitle("Dr")
						.addGivenName("Nick")
						.setFamilyName("Smith"));
		minimal.setOrganisationName("St. Elsewhere's Hospital");
		minimal.setOrganisationId(new OrgID()
									.setID("XZ901")
									.setType(OrgIDType.ODSOrgID.code));
		return minimal;
	}
	
	@Test
	public void testSerialise() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = template.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145200GB01_01.xml", result, true);
	}
	
	@Test
	public void testSerialiseMinimal() throws SAXException, IOException {
		// Normally this template would inherit namespaces from the overall
		// document, but as we are calling it directly we need to provide them
		// when we serialise. We will also override the root element name to match
		// the one used in the sample XML.
		String result = minimal.serialise("assignedAuthor", parentNamespaces);
		
		// The sample XML does not include any namespace declarations, so we will
		// add some as our configuration expects all the elements to be in the 
		// correct default namespace for HL7v3 (urn:hl7-org:v3).
		testXMLisSimilar("/TestData/Templates/COCD_TP145200GB01_02.xml", result, true);
	}
	
	@Test
	public void testParse() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145200GB01_01.xml", true);
		AuthorPersonUniversal template = new AuthorPersonUniversal();
		template.parse(expectedResult, parentNamespaces);
		
		List<PersonID> ids = template.getId();
		assertEquals("200025166218", (ids.get(0)).getID());
		assertEquals("100099876439", (ids.get(1)).getID());
		
		CodedValue job = template.getJobRoleName();
		assertEquals("NR0050", job.getCode());
	}
	
	@Test
	public void testRoundTrip() {
		String expectedResult = loadExpectedResult("/TestData/Templates/COCD_TP145200GB01_01.xml", true);
		AuthorPersonUniversal template = new AuthorPersonUniversal();
		template.parse(expectedResult, parentNamespaces);
		String result = template.serialise("assignedAuthor", parentNamespaces);
		testXMLisSimilar("/TestData/Templates/COCD_TP145200GB01_01.xml", result, true);
	}
}
