/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.toc_edischarge_draftB;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.CompositionalStatement;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.AuthorRoleID;
import uk.nhs.interoperability.payloads.commontypes.DateRange;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.prsbsections.AdmissionDetailsSection;
import uk.nhs.interoperability.payloads.prsbsections.AllergiesAndAdverseReactionsSection;
import uk.nhs.interoperability.payloads.prsbsections.AssessmentScalesSection;
import uk.nhs.interoperability.payloads.prsbsections.ClinicalSummarySection;
import uk.nhs.interoperability.payloads.prsbsections.DiagnosesSection;
import uk.nhs.interoperability.payloads.prsbsections.DischargeDetailsSection;
import uk.nhs.interoperability.payloads.prsbsections.InformationGivenSection;
import uk.nhs.interoperability.payloads.prsbsections.InvestigationsAndProceduresRequestedSection;
import uk.nhs.interoperability.payloads.prsbsections.LegalInformationSection;
import uk.nhs.interoperability.payloads.prsbsections.MedicationsAndMedicalDevicesSection;
import uk.nhs.interoperability.payloads.prsbsections.ParticipationInResearchSection;
import uk.nhs.interoperability.payloads.prsbsections.PatientAndCarerConcernsSection;
import uk.nhs.interoperability.payloads.prsbsections.PersonCompletingRecordSection;
import uk.nhs.interoperability.payloads.prsbsections.PlanAndRequestedActionsSection;
import uk.nhs.interoperability.payloads.prsbsections.ProceduresSection;
import uk.nhs.interoperability.payloads.prsbsections.SafetyAlertsSection;
import uk.nhs.interoperability.payloads.prsbsections.SocialContextSection;
import uk.nhs.interoperability.payloads.templates.AuthorNonNamedPersonUniversal;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.DocumentParticipantUniversal;
import uk.nhs.interoperability.payloads.templates.EncompassingEncounter;
import uk.nhs.interoperability.payloads.templates.PatientGuardian;
import uk.nhs.interoperability.payloads.templates.PatientGuardianPerson;
import uk.nhs.interoperability.payloads.templates.PatientUniversalv2;
import uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal;
import uk.nhs.interoperability.payloads.templates.RelatedEntityParticipant;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceCaresettingtype;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceDocumenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition;
import uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity;
import uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DocumentTypes;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class ClinicalDocumentTest extends AbstractTest {

	public static ClinicalDocument eDischargeDoc;
	
	static {
		eDischargeDoc = createDocument();
	}
	
	public static ClinicalDocument createDocument() {
		ClinicalDocument template = createCommonFields();
		template.addDocumentSections(new DocumentSection(createAdmissionDetails()));
		template.addDocumentSections(new DocumentSection(createAllergies()));
		template.addDocumentSections(new DocumentSection(createAssessments()));
		template.addDocumentSections(new DocumentSection(createClinicalSummary()));
		template.addDocumentSections(new DocumentSection(createDiagnoses()));
		template.addDocumentSections(new DocumentSection(createDischargeDetails()));
		template.addDocumentSections(new DocumentSection(createInformationGiven()));
		template.addDocumentSections(new DocumentSection(createInvestigations()));
		template.addDocumentSections(new DocumentSection(createLegal()));
		template.addDocumentSections(new DocumentSection(createMedications()));
		template.addDocumentSections(new DocumentSection(createResearch()));
		template.addDocumentSections(new DocumentSection(createConcerns()));
		template.addDocumentSections(new DocumentSection(createPersonCompletingRecord()));
		template.addDocumentSections(new DocumentSection(createPlan()));
		template.addDocumentSections(new DocumentSection(createProcedures()));
		template.addDocumentSections(new DocumentSection(createAlerts()));
		template.addDocumentSections(new DocumentSection(createSocialContext()));
		return template;
	}
	
	public static AdmissionDetailsSection createAdmissionDetails() {
		AdmissionDetailsSection template = new AdmissionDetailsSection();
		template.setSectionId("C3A0F8FB-35FA-4358-B46E-6F9EE6AC6CF2");
		template.setTimeAuthored(new DateValue("200708012055+01"));
		template.setAuthor(createAuthorMaryFlemming());
		template.setText("<table width=\"100%\"><tbody><tr><th>Admission method</th><td>Emergency</td></tr><tr><th>Date of admission</th><td>12-Feb-2015</td></tr><tr><th>Source of admission</th><td>Usual place of residence</td></tr></tbody></table>");
		return template;
	}
	
	public static AllergiesAndAdverseReactionsSection createAllergies() {
		AllergiesAndAdverseReactionsSection template = new AllergiesAndAdverseReactionsSection();
		template.setSectionId("82BF32F4-297F-4768-8D13-E832B08CC8F9");
		template.setText("<table width=\"100%\"><tbody><tr><th>Causative agent</th><td>Penicillin</td></tr><tr><th>Description of the reaction</th><td>Rash. No swelling/anaphylaxis</td></tr></tbody></table>");
		return template;
	}

	public static AssessmentScalesSection createAssessments() {
		AssessmentScalesSection template = new AssessmentScalesSection();
		template.setSectionId("C0EA8568-2F11-4EC1-910D-B4A5793D0487");
		template.setText("<table width=\"100%\"><tbody><tr><th>Assessment scales</th><td>TIMI score=2</td></tr></tbody></table>");
		return template;
	}

	public static ClinicalSummarySection createClinicalSummary() {
		ClinicalSummarySection template = new ClinicalSummarySection();
		template.setSectionId("FFC35837-1801-4CA1-A2B2-AC514A250BD4");
		template.setText("<table width=\"100%\"><tbody><tr><th>Clinical Summary</th></tr><tr><td>58 year old man with an acute history of cardiac sounding chest pain lasting around 2 hours. </td></tr><tr><td>Examination unremarkable.</td></tr><tr><td>Inferior ischaemic changes on ECGs and raised Troponin T. </td></tr><tr><td>Coronary angiogram demonstrated diseased RCA, drug eluting stent successfully placed. </td></tr><tr><td>Appropriate secondary prevention medications prescribed and for follow up with Cardiac Rehabilitation team. </td></tr></tbody></table>");
		return template;
	}

	public static DiagnosesSection createDiagnoses() {
		DiagnosesSection template = new DiagnosesSection();
		template.setSectionId("B1A24C38-C952-4231-8A7C-3D4BB3022468");
		template.setText("<table width=\"100%\"><tbody><tr><th>Diagnosis</th></tr><tr><td>Cardiac chest pain/ACS</td></tr></tbody></table>");
		return template;
	}

	public static DischargeDetailsSection createDischargeDetails() {
		DischargeDetailsSection template = new DischargeDetailsSection();
		template.setSectionId("939FAE99-0E5B-44DD-8EF6-DBBFCE9089C1");
		template.setText("<table width=\"100%\"><tbody><tr><th>Discharging consultant</th><td>Mr Abacus</td></tr><tr><th>Date of discharge</th><td>16-Feb-2015</td></tr><tr><th>Discharge method</th><td>Patient discharged on clinical advice</td></tr><tr><th>Discharge destination</th><td>Usual place of residence</td></tr></tbody></table>");
		return template;
	}

	public static InformationGivenSection createInformationGiven() {
		InformationGivenSection template = new InformationGivenSection();
		template.setSectionId("A68BCC94-C887-4B89-B6EF-B972789A4DEE");
		template.setText("<table width=\"100%\"><tbody><tr><th>Information Given</th></tr><tr><td>Patient seen by Cardiac Rehab and information given regarding diagnosis, lifestyle change and benefits of rehab programme.</td></tr><tr><td>Community follow up planned.</td></tr></tbody></table>");
		return template;
	}

	public static InvestigationsAndProceduresRequestedSection createInvestigations() {
		InvestigationsAndProceduresRequestedSection template = new InvestigationsAndProceduresRequestedSection();
		template.setSectionId("5998CA3D-A3A5-4C24-A7A6-49B51823062A");
		template.setText("<table width=\"100%\"><tbody><tr><th>FBC</th><td> 5 x 10<content styleCode=\"super\"><sup>12</sup></content>/L, WCC: 11.1 x 10<content styleCode=\"super\"><sup>9</sup></content>/L, Hb: 150 g/dl, neutrophils: 7.0 x 10<content styleCode=\"super\"><sup>9</sup></content>/L, eosinophils: 0.40 x 10<content styleCode=\"super\"><sup>9</sup></content>/L, lymphocytes: 3.5x10<content styleCode=\"super\"><sup>9</sup></content>/L, monocytes: 0.3 x 10<content styleCode=\"super\"><sup>9</sup></content>/L</td></tr><tr><th>Arterial Blood Gases</th><td>PH: 7.41, pO<content styleCode=\"sub\"><sub>2</sub></content>: 13 kPa, pCO<content styleCode=\"sub\"><sub>2</sub></content>: 6.0kPa, HCO3: 24 mEq/L, B.E.: +2 mmol/L</td></tr><tr><th>ECG</th><td>Inferior ischaemic changes</td></tr><tr><th>Chest Xray</th><td>Normal</td></tr></tbody></table>");
		return template;
	}

	public static LegalInformationSection createLegal() {
		LegalInformationSection template = new LegalInformationSection();
		template.setSectionId("D6429937-7490-4C30-88B2-5C20BD913CA2");
		template.setText("<table width=\"100%\"><tbody><tr><th>Advance decisions about treatment</th></tr><tr><td>Declines blood products - patient is a Jehovah's witness </td></tr></tbody></table>");
		return template;
	}

	public static MedicationsAndMedicalDevicesSection createMedications() {
		MedicationsAndMedicalDevicesSection template = new MedicationsAndMedicalDevicesSection();
		template.setSectionId("581BD7AB-80D0-4052-AF58-521C80FF9B8D");
		template.setText("<table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Ramipril</td></tr><tr><th>Dose</th><td>2.5mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Omeprazole</td></tr><tr><th>Dose</th><td>20mg</td></tr><tr><th>Medication Frequency</th><td>OD/PRN</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Aspirin</td></tr><tr><th>Dose</th><td>75mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Clopidogrel</td></tr><tr><th>Dose</th><td>75mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>1 year then stop</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Simvastatin</td></tr><tr><th>Dose</th><td>40mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Bisoprolol</td></tr><tr><th>Dose</th><td>5mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>Uptitrate according to BP and HR</td></tr></tbody></table>");
		return template;
	}

	public static ParticipationInResearchSection createResearch() {
		ParticipationInResearchSection template = new ParticipationInResearchSection();
		template.setSectionId("9FAD3E38-9589-4C6E-A686-4C2B0E06E22F");
		template.setText("<table width=\"100%\"><tbody><tr><th>Participation in research</th></tr><tr><td>None</td></tr></tbody></table>");
		return template;
	}

	public static PatientAndCarerConcernsSection createConcerns() {
		PatientAndCarerConcernsSection template = new PatientAndCarerConcernsSection();
		template.setSectionId("40085D49-A7AE-4654-97E5-09EDD4F9E9C3");
		template.setText("<table width=\"100%\"><tbody><tr><th>Patient and carer concerns</th></tr><tr><td>Concern re possibility of heart attack/similarity with mothers illness.</td></tr><tr><td>Worried about ability to drive and work/financial implications.</td></tr></tbody></table>");
		return template;
	}

	public static PersonCompletingRecordSection createPersonCompletingRecord() {
		PersonCompletingRecordSection template = new PersonCompletingRecordSection();
		template.setSectionId("3FC2BA99-C93E-44B2-A926-4B4FE04C0E79");
		template.setText("<table width=\"100%\"><tbody><tr><th>Name</th><td>Dr Paul Rastall</td></tr><tr><th>Designation or role</th><td>On call Dr</td></tr><tr><th>Grade</th><td>SpR</td></tr><tr><th>Specialty</th><td>Medicine</td></tr></tbody></table>");
		return template;
	}
	
	public static PlanAndRequestedActionsSection createPlan() {
		PlanAndRequestedActionsSection template = new PlanAndRequestedActionsSection();
		template.setSectionId("F1775E17-A7B5-4668-AB2F-1262FBF16BEA");
		template.setText("<table width=\"100%\"><tbody><tr><th>Actions</th></tr><tr><td>GP please continue secondary preventative medication.</td></tr><tr><td>Doses will be uptitrated by Cardiac Rehab team.</td></tr></tbody></table>");
		return template;
	}
	
	public static ProceduresSection createProcedures() {
		ProceduresSection template = new ProceduresSection();
		template.setSectionId("DBA09523-E29A-4D05-AA88-8530988FB61F");
		template.setText("<table width=\"100%\"><tbody><tr><th>Procedures</th></tr><tr><td>Coronary Angiogram with stent to right coronary artery (RCA)</td></tr></tbody></table>");
		return template;
	}
	
	public static SafetyAlertsSection createAlerts() {
		SafetyAlertsSection template = new SafetyAlertsSection();
		template.setSectionId("1D350DC7-A2B2-4509-948C-0B8E125F8A68");
		template.setText("<table width=\"100%\"><tbody><tr><th>Safety Alerts</th></tr><tr><td>None</td></tr></tbody></table>");
		return template;
	}
	
	public static SocialContextSection createSocialContext() {
		SocialContextSection template = new SocialContextSection();
		template.setSectionId("046F1E8C-C842-47FD-8292-CC5AF87E4C50");
		template.setText("<table width=\"100%\"><tbody><tr><th>Household composition</th><td>Lives alone</td></tr><tr><th>Lives alone</th><td>No</td></tr><tr><th>Occupational history</th><td>Self employed electrician</td></tr></tbody></table>");
		return template;
	}

	public static AuthorPersonUniversal createAuthorMaryFlemming() {
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		template.addId(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("102")
 						.setAssigningOrganisation("5L399:Medway South Out of Hours Centre"));		
 		// In the sample XML the SDSJobRoleName vocab is used (which is an empty vocab). We will use it's OID here:
 		template.setJobRoleName(new CodedValue("OOH02","Nurse Practitioner","2.16.840.1.113883.2.1.3.2.4.17.196"));
 		template.setName(new PersonName()
						.addGivenName("Mary")
						.setFamilyName("Flemming"));
		template.setOrganisationId(new OrgID()
									.setID("5L399")
									.setType(OrgIDType.ODSOrgID.code));
		template.setOrganisationName("Medway South Out of Hours Centre");
		return template;
	}
	
	public static ClinicalDocument createCommonFields() {
		ClinicalDocument template = new ClinicalDocument();
		
		// ==== Set the basic document information ====
		template.setDocumentId("CB0AABF7-E7F9-4D76-A6E1-40C440C79634");
		template.setDocumentTitle("Discharge summary");
		
		CompositionalStatement cs = new CompositionalStatement(DocumentTypes.ClinicalDocumentDescriptor);
		cs.addAttributeCodeAndValueCode(DocumentTypes.DocumentType,
										CorrespondenceDocumenttype._Dischargesummary);
		cs.addAttributeCodeAndValueCode(DocumentTypes.CareSetting,
										CorrespondenceCaresettingtype._Accidentemergency);
		template.setDocumentType(cs);
		
		template.setEffectiveTime(new DateValue("201502171000+00"));
		template.setConfidentialityCode(x_BasicConfidentialityKind._V);
		template.setDocumentSetId("262BF05B-74B9-4487-88FE-8B72C71E9D76");
		template.setDocumentVersionNumber("1");
		
		// ==== Now, set the "left hand side" values in the document ====
		// Patient
		template.setPatient(createPatient());
		// Author
		template.setTimeAuthored(new DateValue("201502140900+00"));
		template.setAuthor(createAuthorXYZHospital());
		// Custodian
		template.setCustodianOrganisation(createCustodian());
		// Recipients
		template.addPrimaryRecipients(new PrimaryRecipient(createPrimaryRecipient()));
		template.addInformationOnlyRecipients(new InformationOnlyRecipient(createCopyRecipient()));
		// Participant (GP)
		DocumentParticipant gp = new DocumentParticipant();
		gp.setParticipant(createParticipantGP());
		gp.setParticipantTypeCode(ParticipationType._IND.code);
		gp.setFunctionCode(ParticipationFunction._primarycarephysician);
		template.addParticipant(gp);
		// Participant (Patient - self referral)
		DocumentParticipant self = new DocumentParticipant();
		self.setParticipant(createParticipantSelf());
		self.setParticipantTypeCode(ParticipationType._Referredby.code);
		template.addParticipant(self);
		// Encompassing Encounter
		template.setEncompassingEncounter(createEncounter());
		return template;
	}
	
	public static PatientUniversalv2 createPatient() {
		PatientUniversalv2 template = new PatientUniversalv2();
		
		// PatientRole:
		template.addPatientID(new PatientIDWithTraceStatuses()
									.setPatientID("1352465790")
									.setPatientIDType(PatientIDType.VerifiedNHSNumber.code));
		
		template.addAddress(new Address()
									.addAddressLine("21, Grove Street")
									.addAddressLine("Overtown")
									.addAddressLine("Leeds")
									.addAddressLine("West Yorkshire")
									.setPostcode("LS21 1PF")
									.setAddressUse(AddressType.Home.code));
		
		// Patient
		template.addPatientName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Richard")
									.setFamilyName("Smith"));
		template.setSex(Sex._Male);
		template.setBirthTime(new DateValue("19570101"));
		template.setEthnicGroup(Ethnicity._British);
		
		// Guardian
		PatientGuardian guardian = new PatientGuardian();
		guardian.setRole(GuardianRoleType._Relative);
		guardian.addTelephoneNumber(new Telecom("tel:0712345678")
										.setTelecomType(TelecomUseType.MobileContact.code));
		guardian.addTelephoneNumber(new Telecom("mailto:mrssmith@mymail.com"));
		guardian.setGuardianDetails(new PatientGuardianPerson(
											new PersonName("Mrs", "Joy", "Smith")
														.setNameType(PersonNameType.Preferred.code)));
		template.setGuardian(guardian);
		
		// Organisation
		template.setOrganisationId(new OrgID()
										.setID("GP123456")
										.setType(OrgIDType.ODSOrgID.code));
		
		template.setOrganisationName("MGP Medical Centre");
		template.addOrganisationTelephone(new Telecom("tel:01634111222")
											.setTelecomType(TelecomUseType.WorkPlace.code));
		template.addOrganisationTelephone(new Telecom("mailto:jlorenzo@MMC.co.uk"));
		
		template.setOrganisationAddress(new Address()
											.addAddressLine("1 MGP House")
											.addAddressLine("Overtown")
											.addAddressLine("Leeds")
											.addAddressLine("West Yorkshire")
											.setPostcode("LS21 7PA")
											.setAddressUse(AddressType.WorkPlace.code));
		template.setOrganisationType(CDAOrganizationProviderType._GPPractice);
		
		return template;
	}
	
	public static AuthorNonNamedPersonUniversal createAuthorXYZHospital() {
		AuthorNonNamedPersonUniversal template = new AuthorNonNamedPersonUniversal();
		template.setID(new AuthorRoleID()
 						.setID("SR1701")
 						.setAssigningOrganisation("A1A:XYZ Hospital"));		
		template.setJobRoleName(JobRoleName._SpecialistRegistrar);
		template.setOrgId(new OrgID()
								.setID("A1A")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("XYZ Hospital");
		return template;
	}
	
	public static CustodianOrganizationUniversal createCustodian() {
		CustodianOrganizationUniversal template = new CustodianOrganizationUniversal();
		template.setId(new OrgID(OrgIDType.ODSOrgID.code, "GP135791"));
		template.setName("XYZ NHS trust");
		return template;
	}
	
	public static RecipientOrganizationUniversal createPrimaryRecipient() {
		RecipientOrganizationUniversal template = new RecipientOrganizationUniversal();
		
		template.setOrgId(new OrgID()
								.setID("V396A")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("MGP Medical Centre");
		return template;
	}
	
	public static RecipientPersonUniversal createCopyRecipient() {
		RecipientPersonUniversal template = new RecipientPersonUniversal();
		template.addId(new RoleID().setID("NR0700")
								   .setAssigningOrganisation("RY6:Leeds Community Healthcare NHS Trust"));
		template.setJobRoleName(JobRoleName._NR0700);
		template.setAddress(new Address()
									.addAddressLine("Leeds Community Healthcare NHS Trust")
									.addAddressLine("First Floor, Stockdale House")
									.addAddressLine("Headingley Office Park")
									.addAddressLine("Victoria Road")
									.addAddressLine("Leeds")
									.setPostcode("LS6 1PF")
									.setAddressUse(AddressType.WorkPlace.code));
		template.addTelephoneNumber(new Telecom("tel:01136323200")
											.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setName(new PersonName("Mrs","A","Smith"));
		template.setOrgId(new OrgID()
								.setID("RAE")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("RY6:Leeds Community Healthcare NHS Trust");
		return template;
	}
	
	public static DocumentParticipantUniversal createParticipantGP() {
		DocumentParticipantUniversal template = new DocumentParticipantUniversal();
		
		template.addId(new PersonID()
								.setID("GP123456")
								.setType(PersonIDType.SDSID.code));
		template.setRoleClass(RoleClassAssociative._PROV.code);
		template.setAddress(new Address()
								.addAddressLine("1 MGP House")
								.addAddressLine("Overtown")
								.addAddressLine("Leeds")
								.addAddressLine("West Yorkshire")
								.setPostcode("LS21 7PA")
								.setAddressUse(AddressType.WorkPlace.code));
		template.addTelephoneNumber(new Telecom("tel:01634111222")
								.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setName(new PersonName("Dr", "John", "Lorenzo")
								.setNameType(PersonNameType.Preferred.code));
		return template;
	}
	
	public static RelatedEntityParticipant createParticipantSelf() {
		RelatedEntityParticipant template = new RelatedEntityParticipant();
		template.setCDAPersonRelationshipType(CDAPersonRelationshipType._Self);
		template.setTelephone(new Telecom("tel:0712345678")
									.setTelecomType(TelecomUseType.MobileContact.code));
		template.setName(new PersonName("Mr", "Richard", "Smith")
								.setNameType(PersonNameType.Preferred.code));
		return template;
	}
	
	
	public static EncompassingEncounter createEncounter() {
		EncompassingEncounter template = new EncompassingEncounter();
		
		template.setId("133C6C34-1399-4342-B72D-F0011FF91661");
		template.setCode(CDAEncounterType._Emergencydepartmentpatientvisit);
		template.setEffectiveTime(
				new DateRange(new DateValue("201502120900+00"),
							  new DateValue("201502160900+00")));
		template.setDischargeDispositionCode(Encounterdisposition._Dischargetohome);

		// Healthcare Facility
		template.setEncounterCareSettingType(CDACareSettingType._AccidentandEmergencydepartment);
		
		return template;
	}
	
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("edischarge-draftB/", "eDischarge-roundTrip", "eDischarge: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/POCD_EX000026GB01_AH_03.xml");
			ClinicalDocument document = new ClinicalDocument();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/POCD_EX000026GB01_AH_03.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("edischarge-draftB/", "eDischarge-SerialiseTest", "eDischarge: Serialise Test", "This uses the itk-payloads library to generate a full non-coded CDA document using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = eDischargeDoc.serialise();
			testXMLisSimilar("/TestData/POCD_EX000026GB01_AH_03.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("edischarge-draftB/", "eDischarge-schemaCheck", "eDischarge: Schema Validation Test", "This uses the itk-payloads library to generate a full non-coded CDA document, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = eDischargeDoc.serialise();
			testAgainstSchema(PropertyReader.getProperty("TOCDraftBSchemaPath")+"POCD_MT000002UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testTemplatedSchemaValidation() {
		super.init("edischarge-draftB/", "eDischarge-templatedSchemaCheck", "eDischarge: Templated Schema Test", "This uses the itk-payloads library to generate a full non-coded CDA document, uses the transforms provided by the messaging team to transform ths XML into templated (aka conformance) format, and then validates it against the published templated schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = eDischargeDoc.serialise();
			//FileWriter.writeFile("output.xml", result.getBytes());
			testAgainstTemplatedSchema(PropertyReader.getProperty("TOCDraftBSchemaPath")+"POCD_MT000026GB01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
