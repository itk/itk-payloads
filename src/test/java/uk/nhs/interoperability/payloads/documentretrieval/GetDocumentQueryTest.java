/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.documentretrieval;

import java.io.IOException;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;

public class GetDocumentQueryTest extends AbstractTest {

	public static GetDocumentQuery requestBySetID;
	public static GetDocumentQuery requestByDocID;
	
	static {
		requestBySetID = createRequestBySetID();
		requestByDocID = createRequestByDocumentID();
	}
	
	public static GetDocumentQuery createRequestWithoutDocument() {
		GetDocumentQuery template = new GetDocumentQuery();
		
		// ==== Request information ====
		template.setRequestID("E71ADBF0-3A09-11E2-81C1-0800200C9A66");
		template.setTime(new DateValue("20120101150000+0000"));
		
		// Author
 		template.addAuthorID(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("101")
 						.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST"));		
 		template.setAuthorJobRoleName(JobRoleName._NR2040);
 		template.setAuthorPersonAddress(new Address()
						.addAddressLine("Potter Street")
						.addAddressLine("Grimsby")
						.setPostcode("DN3 6AA")
						.setAddressUse(AddressType.WorkPlace.code));
 		template.setAuthorTelephone("tel:01472312345");
 		template.setAuthorName(new PersonName()
						.setTitle("Ms")
						.addGivenName("Niral")
						.setFamilyName("Singh"));
		template.setAuthorOrgID(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		template.setAuthorOrgName("St James Hospital");

		return template; 
	}
	
	public static GetDocumentQuery createRequestBySetID() {
		GetDocumentQuery template = createRequestWithoutDocument();
		// ==== Request information ====
		template.setDocumentSetID("2E0A4036-28F4-11E2-A045-B3916188709B");
		return template; 
	}
	
	public static GetDocumentQuery createRequestByDocumentID() {
		GetDocumentQuery template = createRequestWithoutDocument();
		// ==== Request information ====
		template.setDocumentID("28BEE6CC-28F4-11E2-B493-B2916188709B");
		return template; 
	}
	
	
	/**
	 * This is a test using an example XML file, to round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("documentretrieval/", "GetDocumentRequest-roundTrip", "GetDocumentRequest: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/QUPA_EX000002GB01_01_AHFixed.xml");
			GetDocumentQuery document = new GetDocumentQuery();
			document.parse(expectedXML);
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			testXMLisSimilar("/TestData/QUPA_EX000002GB01_01_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("documentretrieval/", "GetDocumentRequest-SerialiseTest", "GetDocumentRequest: Serialise Test", "This uses the itk-payloads library to generate a GetDocumentRequest. This is then serialised and compared against an example to ensure it matches.");
		try {
			// Serialise the objects
			String result1 = requestBySetID.serialise();
			String result2 = requestByDocID.serialise();
			//FileWriter.writeFile("output1.xml", result1.getBytes());
			//FileWriter.writeFile("output2.xml", result2.getBytes());
			testXMLisSimilar("/TestData/QUPA_EX000002GB01_AH_01.xml", result1, false);
			testXMLisSimilar("/TestData/QUPA_EX000002GB01_AH_02.xml", result2, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("documentretrieval/", "GetDocumentRequest-schemaCheck", "GetDocumentRequest: Schema Validation Test", "This uses the itk-payloads library to generate a GetDocumentRequest. This is then serialised and validated against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result1 = requestBySetID.serialise();
			testAgainstSchema(PropertyReader.getProperty("documentrequestSchemaPath")+"QUPA_MT000002GB01.xsd", result1);
			
			String result2 = requestByDocID.serialise();
			testAgainstSchema(PropertyReader.getProperty("documentrequestSchemaPath")+"QUPA_MT000002GB01.xsd", result2);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
