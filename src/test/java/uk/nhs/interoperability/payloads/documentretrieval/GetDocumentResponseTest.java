/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.documentretrieval;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.SystemID;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType;

public class GetDocumentResponseTest extends AbstractTest {

	public static GetDocumentResponse simpleResponse;
	public static GetDocumentResponse b64DocResponse;
	
	static {
		simpleResponse = createSimpleResponse();
		b64DocResponse = createBase64DocResponse();
	}
	
	public static GetDocumentResponse createBase64DocResponse() {
		GetDocumentResponse template = createBasicResponse();
		// Response payload
		template.setResponseDocumentMediaType("text/xml");
		template.setResponseDocumentType(AttachmentType.Base64.code);
		
		// Load the EOLC Clinical Document
		String b64Doc = "<ClinicalDocument>Dummy Document</ClinicalDocument>";
		// Base64 encode it using the Apache Commons base64 encoder
		String base64data = Base64.encodeBase64String(b64Doc.getBytes());
		template.setResponseDocument(base64data);
		return template;
	}
	
	public static GetDocumentResponse createSimpleResponse() {
		GetDocumentResponse template = createBasicResponse();
		// Response payload
		template.setResponseDocumentMediaType("text/plain");
		template.setResponseDocumentType(AttachmentType.Text.code);
		template.setResponseDocument("DummyDocument");
		return template;
	}
	
	public static GetDocumentResponse createBasicResponse() {
		GetDocumentResponse template = new GetDocumentResponse();
		
		// ==== Response information ====
		template.setTime(new DateValue("20120101150100+0000"));
		template.setResponseID("E71ADBF1-3A09-11E2-81C1-0800200C9A66");
		template.setResponseDocumentMessageType(DocumentResponseType._EndofLifeCareCoordinationSummary);
		
		// Request
		template.setRequestID("E71ADBF0-3A09-11E2-81C1-0800200C9A66");
		
		// System Author
		template.setOriginatingSystemSystemID(new SystemID()
				.setType(SystemIDType.LocalSystemID.code)
				.setAssigningOrganisation("RA8:St Elsewhere's Hospital")
				.setID("112YT"));
		template.setOriginatingSystemOrgID(new OrgID()
				.setType(OrgIDType.ODSOrgID.code)
				.setID("RA8"));
		template.setOriginatingSystemOrgName("St Elsewhere's Hospital");
		
		return template;
	}
	
	/**
	 * This is a test using an example XML file, to round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("documentretrieval/", "GetDocumentResponse-roundTrip", "GetDocumentResponse: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/PRPA_EX000002GB01_AH_01.xml");
			GetDocumentResponse document = new GetDocumentResponse();
			document.parse(expectedXML);
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			testXMLisSimilar("/TestData/PRPA_EX000002GB01_AH_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with a sample XML file
	 */
	@Test
	public void testSerialiseSimple() {
		super.init("documentretrieval/", "GetDocumentResponse-SerialiseSimpleTest", "GetDocumentResponse: Serialise Simple Test", "This uses the itk-payloads library to generate a GetDocumentResponse. This is then serialised and compared against an example to ensure it matches.");
		try {
			// Serialise the objects
			String result = simpleResponse.serialise();
			//FileWriter.writeFile("output.xml", result.getBytes());
			testXMLisSimilar("/TestData/PRPA_EX000002GB01_AH_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with a sample XML file
	 */
	@Test
	public void testSerialise() {
		super.init("documentretrieval/", "GetDocumentResponse-SerialiseTest", "GetDocumentResponse: Serialise Test", "This uses the itk-payloads library to generate a GetDocumentResponse. This is then serialised and compared against an example to ensure it matches.");
		try {
			// Serialise the objects
			String result = b64DocResponse.serialise();
			//FileWriter.writeFile("output.xml", result.getBytes());
			testXMLisSimilar("/TestData/PRPA_EX000002GB01_SimpleB64.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("documentretrieval/", "GetDocumentResponse-schemaCheck", "GetDocumentResponse: Schema Validation Test", "This uses the itk-payloads library to generate a GetDocumentResponse. This is then serialised and validated against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = simpleResponse.serialise();
			testAgainstSchema(PropertyReader.getProperty("documentrequestSchemaPath")+"PRPA_MT000002GB01.xsd", result);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
