/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.childscreeningv2;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.BloodSpotScreening;
import uk.nhs.interoperability.payloads.templates.ChildPatientGuardian;
import uk.nhs.interoperability.payloads.templates.ChildPatientGuardianPerson;
import uk.nhs.interoperability.payloads.templates.ChildPatientOrganisationPartOf;
import uk.nhs.interoperability.payloads.templates.ChildPatientUniversal;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.NewBornBirthDetails;
import uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal;
import uk.nhs.interoperability.payloads.templates.Section2;
import uk.nhs.interoperability.payloads.templates.Section3;
import uk.nhs.interoperability.payloads.templates.TextSection;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class ClinicalDocumentTest extends AbstractTest {

	public static ClinicalDocument childScreeningExample;
	
	static {
		childScreeningExample = createChildScreeningExample();
	}
	
	public static ClinicalDocument createChildScreeningExample() {
		ClinicalDocument template = new ClinicalDocument();
		
		// ==== Set the basic document information ====
		template.setDocumentId("A570ED3C-3D67-11E2-9389-A28C6188709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setConfidentialityCode(x_BasicConfidentialityKind._N);
		template.setDocumentSetId("B8275470-3D67-11E2-B9D6-A68C6188709B");
		template.setDocumentVersionNumber("1");
		
		// ==== Now, set the "left hand side" values in the document ====
		
		// Patient
		ChildPatientUniversal patient = createPatient();
		template.setChildPatient(patient);
		// Author
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		AuthorPersonUniversal author = createAuthorNiralSingh();
		template.setAuthor(author);
		// Custodian
		CustodianOrganizationUniversal custodian = createCustodian();
		template.setCustodianOrganisation(custodian);
		// Recipients
		RecipientPersonUniversal recipient = createRecipient(); 
		template.addPrimaryRecipients(new PrimaryRecipient(recipient));
		
		// ==== Now create the coded sections ====
		
		template.setMainDocumentSectionID("5476CF02-89C2-11E3-A295-9E296288709B");
		
		// BloodSpotScreening
		BloodSpotScreening bloodSpot = createBloodSpotScreening();
		template.addCodedSections(new CodedSections(bloodSpot));

		//NewBornBirthDetails
		NewBornBirthDetails details = createNewBornBirthDetails();
		template.addCodedSections(new CodedSections(details));
		
		// ==== Now create the text sections ====
		template.addTextSections(new TextSections(createTextSection1()));
		template.addTextSections(new TextSections(createTextSection2()));
		template.addTextSections(new TextSections(createTextSection3()));
		return template; 
	}
	
	public static ChildPatientUniversal createPatient() {
		ChildPatientUniversal template = new ChildPatientUniversal();
		template.addPatientID(new PatientIDWithTraceStatuses()
								.setPatientID("K12345")
								.setAssigningOrganisation("V396A:Medway PCT")
								.setPatientIDType(PatientIDType.LocalID.code));
		template.addPatientID(new PatientIDWithTraceStatuses()
								.setPatientID("9932541280")
								.setNHSNoTraceStatus(NHSNumberTraceStatus.TraceNeedsToBeResolved.code)
								.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));
		template.addAddress(new Address()
								.setAddressUse(AddressType.Home.code)
								.addAddressLine("Appleton House")
								.addAddressLine("Lanchester Road")
								.addAddressLine("Grimsby")
								.setPostcode("DN3 1UJ"));
		template.addName(new PersonName()
								.addGivenName("Mark")
								.setFamilyName("Smith"));
		template.setGender(Sex._Male);
		template.setDateOfBirth(new DateValue("20120728"));
		
		// Guardian
		ChildPatientGuardian guardian = new ChildPatientGuardian();
		guardian.addId(new PatientIDWithTraceStatuses()
									.setPatientID("K12344")
									.setPatientIDType(PatientIDType.LocalID.code)
									.setAssigningOrganisation("V396A:Medway PCT"));
		guardian.addId(new PatientIDWithTraceStatuses()
									.setPatientID("993254127")
									.setPatientIDType(PatientIDType.VerifiedNHSNumber.code)
									.setNHSNoTraceStatus(NHSNumberTraceStatus.Traced.code));
		guardian.setRole(GuardianRoleType._Mother);
		guardian.addAddress(new Address()
									.setAddressUse(AddressType.Home.code)
									.addAddressLine("Appleton House")
									.addAddressLine("Lanchester Road")
									.addAddressLine("Grimsby")
									.setPostcode("DN3 1UJ"));
		guardian.addTelephoneNumber(new Telecom()
										.setTelecom("tel:01634111678")
										.setTelecomType(TelecomUseType.HomeAddress.code));		
		guardian.setGuardianDetails(new ChildPatientGuardianPerson()
											.setGuardianName(new PersonName("Mrs", "Shelly", "Smith")));
		template.addGuardian(guardian);
		
		// Provider Organisation
		template.setOrganisationId(new OrgID().setID("V396F"));
		template.setOrganisationName("Dr De Hopper and Partners");
		template.addOrganisationTelephone(new Telecom()
												.setTelecom("tel:01634111222")
												.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setOrganisationAddress(new Address()
											.addAddressLine("Freshney Green PCC")
											.addAddressLine("Grimsby")
											.setPostcode("DN34 4GB")
											.setAddressUse(AddressType.WorkPlace.code));
		template.setOrganisationType(CDAOrganizationProviderType._GPPractice);
		template.addOrganisationPartOf(new ChildPatientOrganisationPartOf()
												.addOrganisationId(new OrgID(OrgIDType.ODSOrgID.code, "V396G")));
		
		return template;
	}
	
	public static AuthorPersonUniversal createAuthorNiralSingh() {
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		template.addId(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("101")
 						.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST"));		
 		template.setJobRoleName(JobRoleName._MidwifeSpecialistPractitioner);
 		template.addAddress(new Address()
						.addAddressLine("Potter Street")
						.addAddressLine("Grimsby")
						.setPostcode("DN3 6AA")
						.setAddressUse(AddressType.WorkPlace.code));
 		template.addTelephoneNumber(new Telecom("tel:01472312345"));
 		template.setName(new PersonName()
						.setTitle("Ms")
						.addGivenName("Niral")
						.setFamilyName("Singh"));
		template.setOrganisationId(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		template.setOrganisationName("St James Hospital");
		return template;
	}
	
	public static AuthorPersonUniversal createAuthorIsabellaHopkins() {
		AuthorPersonUniversal author = new AuthorPersonUniversal();
		author.addId(new PersonID()
									.setID("101")
									.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
									.setType(PersonIDType.LocalPersonID.code));
		author.setJobRoleName(JobRoleName._NR2050);
		author.addAddress(new Address()
									.addAddressLine("Potter Street")
									.addAddressLine("Grimsby")
									.setPostcode("DN3 6AA")
									.setAddressUse(AddressType.WorkPlace.code));
		author.addTelephoneNumber(new Telecom("tel:01472312345"));
		author.setName(new PersonName()
									.setTitle("Mrs")
									.addGivenName("Isabella")
									.setFamilyName("Hopkins"));
		author.setOrganisationId(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		author.setOrganisationName("St James Hospital");
		return author;
	}
	
	public static AuthorPersonUniversal createAuthorJohnManning() {
		AuthorPersonUniversal author = new AuthorPersonUniversal();
		author.addId(new PersonID()
									.setID("112")
									.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
									.setType(PersonIDType.LocalPersonID.code));
		author.setJobRoleName(JobRoleName._NR0260);
		author.addAddress(new Address()
									.addAddressLine("Freshney Green PCC")
									.addAddressLine("Carlton Court")
									.addAddressLine("Martin Road")
									.addAddressLine("Grimsby")
									.setPostcode("DN34 4GB")
									.setAddressUse(AddressType.WorkPlace.code));
		author.addTelephoneNumber(new Telecom("tel:07621846951"));
		author.setName(new PersonName()
									.setTitle("Dr")
									.addGivenName("John")
									.setFamilyName("Manning"));
		author.setOrganisationId(new OrgID()
									.setID("V365F")
									.setType(OrgIDType.ODSOrgID.code));
		author.setOrganisationName("Freshney Green PCC");
		return author;
	}
	
	public static CustodianOrganizationUniversal createCustodian() {
		CustodianOrganizationUniversal template = new CustodianOrganizationUniversal();
		template.setId(new OrgID(OrgIDType.ODSOrgID.code, "TAN01"));
		template.setName("NORTH EAST LINCOLNSHIRE CARE TRUST");
		return template;
	}
	
	public static RecipientPersonUniversal createRecipient() {
		RecipientPersonUniversal template = new RecipientPersonUniversal();
		template.addId(new RoleID().setNullFlavour(NullFlavour.NI.code)); // Non-standard null flavour used in example XML..
		template.setJobRoleName(JobRoleName._GeneralMedicalPractitioner); // The example XML included a job role from a different vocab...
		template.setName(new PersonName("De Hopper"));
		template.setOrgId(new OrgID()
								.setID("V396F")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("Dr De Hopper and Partners");
		return template;
	}
	
	public static BloodSpotScreening createBloodSpotScreening() {
		BloodSpotScreening template = new BloodSpotScreening();
		template.setId("2B6D3E28-3B0A-11E2-B7AF-B4F66183783C");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		// Performer
		template.setSampleCollectedTime(new DateValue("201209111300"));
		template.addPerformerPersonId(new PersonID().setNullFlavour(NullFlavour.Unknown.code));
		template.setPerformerPersonName(new PersonName("Mr", "Brian", "Weberly"));
		template.setPerformerOrgId(new OrgID()
											.setID("A0991")
											.setType(OrgIDType.ODSOrgID.code));
		template.setPerformerOrgName("St James Hospital");
		// Lab
		template.setTimeReceivedAtLab(new DateValue("201209111310"));
		template.setLabOrganisationId(new OrgID()
											.setID("A0078")
											.setType(OrgIDType.ODSOrgID.code));
		template.setLabOrganisationDescription("St James Pathology Laboratory");
		
		// Screening results
		template.setPKUScreeningValue(PKUScreeningResult._Conditionnotsuspected);
		template.setPKUBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
		template.setPKUScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setPKUReasonText("PKU Reason Text");
		template.setPKUSupplementaryText("PKU supplementary text");
		
		template.setSCDScreeningValue(SCDScreeningResult._Conditionnotsuspected);
		template.setSCDScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setSCDReasonText("SCD Reason Text");
		template.setSCDSupplementaryText("SCD supplementary text");
		
		template.setCFScreeningValue(CFScreeningResult._Conditionnotsuspected);
		template.setCFScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setCFReasonText("CFS Reason Text");
		template.setCFSupplementaryText("CFS supplementary text");
		
		template.setCHTScreeningValue(CHTScreeningResult._Conditionnotsuspected);
		template.setCHTScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setCHTReasonText("CHT Reason Text");
		template.setCHTSupplementaryText("CHT supplementary text");
		
		template.setMCADDScreeningValue(MCADDScreeningResult._Conditionnotsuspected);
		template.setMCADDScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		template.setMCADDReasonText("MCADD Reason Text");
		template.setMCADDSupplementaryText("MCADD supplementary text");
		
		// Other observations
		template.setScreeningLocationStatus(BSScreeningLocStatus._NotDischargedOtherspecialistunit);
		template.setLaboratoryCardSerialNumber("0902652801");
		template.setPreviousLaboratoryCardSerialNumber("0902652800");
		return template;
	}
	
	public static NewBornBirthDetails createNewBornBirthDetails() {
		NewBornBirthDetails template = new NewBornBirthDetails();
		template.setId("A1265DB0-89B1-11E3-AFED-19F46188709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setGestationalAgeInWeeks("40");
		template.setBirthOrder("01");
		template.setNoOfFoetusInConfinement("1");
		template.setBirthWeightInGrams("2887");
		return template;
	}
	
	
	
	
	public static TextSection createTextSection1() {
		TextSection template = new TextSection();
		template.setSectionId("1362DCBE-89CB-11E3-A750-1F326288709B");
		template.setTitle("Mother Details");
		template.setText("<table width=\"100%\"><tbody>"
				+ "<tr align=\"left\" valign=\"top\"><td>NHS Number</td><td>993254127</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Name</td><td>Mrs Shelly Smith</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Address</td><td>Appleton House<br/>Lanchester Road<br/>Grimsby<br/>DN3 1UJ</td></tr>"
				+ "</tbody></table>");
		return template;
	}
	
	public static TextSection createTextSection2() {
		TextSection template = new TextSection();
		template.setSectionId("5B49EC7E-89C2-11E3-9C8E-A2296288709B");
		template.setTitle("New Born Birth Details");
		template.setText("<table width=\"100%\"><tbody>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a1\">Gestational age</content></td><td>40 Weeks</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a2\">Birth order</content></td><td>01</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a3\">Number of births in confinement</content></td><td>1</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a4\">Birth weight</content></td><td>2887 grams</td></tr>"
				+ "</tbody></table>");
		return template;
	}
	
	public static TextSection createTextSection3() {
		TextSection template = new TextSection();
		template.setSectionId("AD8D0E24-0243-11E2-A989-D6B26188709B");
		template.setTitle("Blood Spot Screening");
		template.setText("<table width=\"100%\"><tbody>"
				+ "<tr align=\"left\" valign=\"top\"><th>Blood spot screening tests</th><th>Results</th><th>Re-evaluated Result: Laboratory Confirm as Correct</th><th>Status Sub Code</th><th>Reason Text</th><th>Supplementary Text</th></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Phenylketonuria screening test</td><td>Condition not suspected</td><td>Yes</td><td>Not contactable, reasonable efforts made</td><td>PKU Reason Text</td><td>PKU supplementary text</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Screening for sickle cell disease</td><td>Condition not suspected</td><td/><td>Not contactable, reasonable efforts made</td><td>SCD Reason Text</td><td>SCD supplementary text</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Cystic fibrosis screening test</td><td>Condition not suspected</td><td/><td>Not contactable, reasonable efforts made</td><td>CFS Reason Text</td><td>CFS supplementary text</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Congenital hypothyroidism screening test</td><td>Condition not suspected</td><td/><td>Not contactable, reasonable efforts made</td><td>CHT Reason Text</td><td>CHT supplementary text</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Medium-chain acyl-coenzyme A dehydrogenase deficiency screening test</td><td>Condition not suspected</td><td/><td>Not contactable, reasonable efforts made</td><td>MCADD Reason Text</td><td>MCADD supplementary text</td></tr>"
				+ "</tbody></table>");
		
		Section2 s2 = new Section2("97069BCA-3D35-11E2-9A71-5C3D6188709B", "Laboratory Details", "St James Pathology Laboratory");
		template.addSection2(s2);
		
		s2.addSection3(new Section3("4FFF101C-3D36-11E2-B4B7-9C3E6188709D","Date of Receipt at Lab","11-Sep-2012, 13:10"));
		s2.addSection3(new Section3("4FFF101C-3D36-11E2-B4B7-9C3E6188709B","Lab Card Serial Number","0902652801"));
		s2.addSection3(new Section3("9415CD54-3D36-11E2-9337-C93F6188709B","Previous Lab Card Serial Number","0902652800"));
		
		Section2 s2b = new Section2("97069BCA-3D35-11E2-9A71-5C3D6188709B","Specimen Collection Details","Blood specimen collected at St. James Hospital");
		template.addSection2(s2b);
		
		s2b.addSection3(new Section3("4FFF101C-3D36-11E2-B4B7-9C3E6188709D","Specimen Collection Time","11-Sep-2012, 13:00"));
		s2b.addSection3(new Section3("4FFF101C-3D36-11E2-B4B7-9C3E6188709B","Specimen Collected by","Mr Brian Weberly"));
		
		return template;
	}
	
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("childscreening/", "ChildScreening-roundTrip", "ChildScreening: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/POCD_MT010000GB01_01_AHFixed.xml");
			ClinicalDocument document = new ClinicalDocument();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/POCD_MT010000GB01_01_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("childscreening/", "ChildScreening-SerialiseTest", "ChildScreening: Serialise Test", "This uses the itk-payloads library to generate a full CDA document using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = childScreeningExample.serialise();
			testXMLisSimilar("/TestData/POCD_MT010000GB01_01_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("childscreening/", "ChildScreening-schemaCheck", "ChildScreening: Schema Validation Test", "This uses the itk-payloads library to generate a full CDA document, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = childScreeningExample.serialise();
			testAgainstSchema(PropertyReader.getProperty("childscreeningSchemaPath")+"POCD_MT000002UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testTemplatedSchemaValidation() {
		super.init("childscreening/", "ChildScreening-templatedSchemaCheck", "ChildScreening: Templated Schema Test", "This uses the itk-payloads library to generate a full CDA document, uses the transforms provided by the messaging team to transform ths XML into templated (aka conformance) format, and then validates it against the published templated schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = childScreeningExample.serialise();
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("childscreeningSchemaPath")+"POCD_MT010000GB01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
