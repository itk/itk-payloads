/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.noncodedcdav2;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.ConsentID;
import uk.nhs.interoperability.payloads.commontypes.DateRange;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.Consent;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.DocumentParticipantUniversal;
import uk.nhs.interoperability.payloads.templates.EncompassingEncounter;
import uk.nhs.interoperability.payloads.templates.LanguageCommunication;
import uk.nhs.interoperability.payloads.templates.PatientUniversal;
import uk.nhs.interoperability.payloads.templates.PersonUniversal;
import uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.RecipientPersonUniversalv1;
import uk.nhs.interoperability.payloads.templates.RelatedEntity;
import uk.nhs.interoperability.payloads.templates.RelatedEntityParticipant;
import uk.nhs.interoperability.payloads.templates.ServiceEvent;
import uk.nhs.interoperability.payloads.templates.ServiceEventPerformer;
import uk.nhs.interoperability.payloads.templates.TextSection;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class ClinicalDocumentTest extends AbstractTest {

	public static ClinicalDocument nonCodedCDADoc;
	
	static {
		nonCodedCDADoc = createNonCodedCDAStructuredBody();
	}
	
	public static ClinicalDocument createNonCodedCDAStructuredBody() {
		ClinicalDocument template = createCommonFields();
		// Structured Body
		template.setMainStructuredBodyID("7AFDCAD6-1CE3-45A6-A176-DDAC27F46C87");
		template.addTextSections(new TextSections()
									.setTextSection(createTextSection1()));
		template.addTextSections(new TextSections()
									.setTextSection(createTextSection2()));
		template.addTextSections(new TextSections()
									.setTextSection(createTextSection3()));
		return template;
	}
	
	public static ClinicalDocument createCommonFields() {
		ClinicalDocument template = new ClinicalDocument();
		
		// ==== Set the basic document information ====
		template.setDocumentId("A709A442-3CF4-476E-8377-376500E829C9");
		template.setDocumentTitle("Report");
		template.setDocumentType(Documenttype._Report);
		template.setEffectiveTime(new DateValue("201105192000+01"));
		template.setConfidentialityCode(x_BasicConfidentialityKind._V);
		template.setDocumentSetId("411910CF-1A76-4330-98FE-C345DDEE5553");
		template.setDocumentVersionNumber("1");
		
		// ==== Now, set the "left hand side" values in the document ====
		
		// Patient
		template.setPatient(createPatient());
		// Author
		template.setTimeAuthored(new DateValue("201105192000+01"));
		template.setAuthor(createAuthorMaryJones());
		// Data Enterer
		template.setDataEnterer(createDataEnterer());
		// Informant
		template.addInformant(new DocumentInformant(createInformant()));
		// Custodian
		template.setCustodianOrganisation(createCustodian());
		// Recipients
		template.addPrimaryRecipients(new PrimaryRecipient(createRecipient()));
		template.addInformationOnlyRecipients(new InformationOnlyRecipient(createCopyRecipient()));
		// Authenticator
		template.setAuthenticator(createAuthenticator());
		template.setTimeAuthenticated(new DateValue("201105192015+01"));

		// Participant (Consultant)
		DocumentParticipant consultant = new DocumentParticipant();
		consultant.setParticipant(createParticipantConsultant());
		consultant.setParticipantTypeCode(ParticipationType._Consultant.code);
		template.addParticipant(consultant);
		
		// Participant (Call back contact)
		DocumentParticipant callback = new DocumentParticipant();
		callback.setParticipant(createParticipantCallBack());
		callback.setParticipantTypeCode("CALLBCK"); // Wierdly this value is recommended in the tabular view but is not actually in the ParticipationType vocab...
		template.addParticipant(callback);
		
		// DocumentationOf (ServiceEvent)
		template.addDocumentationOf(new DocumentationOf(createServiceEvent()));
		
		// Authorisation
		template.setAuthorizingConsent(createConsent());
		
		// Encompassing Encounter
		template.setEncompassingEncounter(createEncounter());
		return template;
	}
	
	public static PatientUniversal createPatient() {
		PatientUniversal template = new PatientUniversal();
		template.addPatientID(new PatientID()
									.setPatientID("K12345")
									.setAssigningOrganisation("V396A:Medway PCT")
									.setPatientIDType(PatientIDType.LocalID.code));
		template.addPatientID(new PatientID()
									.setPatientID("993254128")
									.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));		
		template.addAddress(new Address()
									.addAddressLine("17, County Court")
									.addAddressLine("Woodtown")
									.addAddressLine("Medway")
									.setPostcode("ME5 FS3")
									.setAddressUse(AddressType.Home.code));
		template.addAddress(new Address()
									.addAddressLine("Hightown Retirement Home")
									.addAddressLine("2, Brancaster Road")
									.addAddressLine("Medway")
									.addAddressLine("Kent")
									.setPostcode("ME5 FL5")
									.setAddressUse(AddressType.PhysicalVisit.code));
		template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:01634775667")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:01634451628")
									.setTelecomType(TelecomUseType.VacationHome.code));
		template.addTelephoneNumber(new Telecom()
									.setTelecom("mailto:mark.smith@emailfree.co.uk")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		template.addPatientName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Mark")
									.setFamilyName("Smith"));
		template.setSex(Sex._Male);
		template.setBirthTime(new DateValue("19490101"));
		// Language
		LanguageCommunication language = new LanguageCommunication();
		language.setLanguage(HumanLanguage._en.code);
		template.addLanguages(language);
		// Organisation - Registered GP:
		template.setRegisteredGPOrgId(new OrgID()
									.setID("V396F")
									.setType(OrgIDType.ODSOrgID.code));
		template.setRegisteredGPOrgName("Medway Medical Practice");
		template.addRegisteredGPTelephone(new Telecom()
									.setTelecom("tel:01634111222")
									.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setRegisteredGPAddress(new Address()
									.addAddressLine("Springer Street")
									.addAddressLine("Medway")
									.setPostcode("ME5 5TY")
									.setAddressUse(AddressType.WorkPlace.code));
		return template;
	}
	
	public static AuthorPersonUniversal createAuthorMaryJones() {
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		template.addId(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("101")
 						.setAssigningOrganisation("5L399:Medway NHS Foundation Trust"));		
 		// In the sample XML the SDSJobRoleName vocab is used (which is an empty vocab). We will use it's OID here:
 		template.setJobRoleName(new CodedValue("OOH02","Nurse Practitioner","2.16.840.1.113883.2.1.3.2.4.17.196"));
 		template.setName(new PersonName()
						.addGivenName("Mary")
						.setFamilyName("Jones"));
		template.setOrganisationId(new OrgID()
									.setID("5L399")
									.setType(OrgIDType.ODSOrgID.code));
		template.setOrganisationName("Medway NHS Foundation Trust");
		return template;
	}
	
	public static PersonUniversal createDataEnterer() {
		PersonUniversal template = new PersonUniversal();
		template.addPersonId(new PersonID()
									.setID("103")
									.setAssigningOrganisation("5L399:Medway NHS Foundation Trust"));
		template.setPersonName(new PersonName()
									.addGivenName("Steve")
									.setFamilyName("Cook"));
		return template;
	}
	
	public static RelatedEntity createInformant() {
		RelatedEntity template = new RelatedEntity();
		template.setCDAPersonRelationshipType(CDAPersonRelationshipType._Spouse);
		return template;
	}
	
	public static CustodianOrganizationUniversal createCustodian() {
		CustodianOrganizationUniversal template = new CustodianOrganizationUniversal();
		template.setId(new OrgID(OrgIDType.ODSOrgID.code, "5L3"));
		template.setName("Medway NHS Foundation Trust");
		return template;
	}
	
	public static RecipientPersonUniversalv1 createRecipient() {
		RecipientPersonUniversalv1 template = new RecipientPersonUniversalv1();
		template.addId(new RoleID()
							.setID("1234512345")
							.setAssigningOrganisation("V396A:Medway PCT"));
		template.addTelephoneNumber(new Telecom()
											.setTelecom("mailto:t.hall@emailfree.co.uk"));
		template.setJobRoleName(JobRoleName._SpecialistNursePractitioner);
		template.setName(new PersonName()
								.setTitle("Mr")
								.setFamilyName("Hall")
								.addGivenName("Terence"));
		template.setOrgId(new OrgID()
								.setID("V396A")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("Medway PCT");
		return template;
	}
	
	public static RecipientPersonUniversalv1 createCopyRecipient() {
		RecipientPersonUniversalv1 template = new RecipientPersonUniversalv1();
		template.addId(new RoleID().setNullFlavour(NullFlavour.NI.code));
		template.setJobRoleName(JobRoleName._GeneralMedicalPractitioner);
		template.setName(new PersonName()
								.setFamilyName("Shelley")
								.addGivenName("Pauline"));
		template.setOrgId(new OrgID()
								.setID("W123")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("Medway Medical Practice");
		return template;
	}
	
	public static PersonUniversal createAuthenticator() {
		PersonUniversal template = new PersonUniversal();
		
		template.addPersonId(new PersonID()
								.setID("100")
								.setAssigningOrganisation("5L399:Medway NHS Foundation Trust"));
		template.setPersonName(new PersonName()
										.addGivenName("Bruce")
										.setFamilyName("Weller"));
		return template;
	}
	
	public static DocumentParticipantUniversal createParticipantConsultant() {
		DocumentParticipantUniversal template = new DocumentParticipantUniversal();
		
		template.setRoleClass(RoleClassAssociative._ASSIGNED.code);
		template.addId(new PersonID()
								.setType(PersonIDType.LocalPersonID.code)
								.setID("000000000")
								.setAssigningOrganisation("V396AA:Medway PCT"));
		template.setName(new PersonName("Bill Lydon")); // Using unstructured name - not advisable!
		template.setOrgId(new OrgID()
								.setID("V396AA")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("Medway PCT");
		return template;
	}
	
	public static RelatedEntityParticipant createParticipantCallBack() {
		RelatedEntityParticipant template = new RelatedEntityParticipant();
		template.setName(new PersonName("Nora Lydon")); // Using unstructured name - not advisable!
		template.setCDAPersonRelationshipType(CDAPersonRelationshipType._Spouse);
		return template;
	}
	
	public static ServiceEvent createServiceEvent() {
		ServiceEvent template = new ServiceEvent();
		
		template.setId("8371D2F1-123F-4A14-A1AC-C6C8023103CF");
		template.setClassCode(HL7ActType.Procedure.code);
		template.setEventCode(new CodedValue("73761001","colonoscopy","2.16.840.1.113883.2.1.3.2.4.15"));
		template.setEffectiveTime(
							new DateRange(new DateValue("201105192000+01"),
										  new DateValue("201105192045+01")));
		
		// Performer
		PersonWithOrganizationUniversal performer = new PersonWithOrganizationUniversal();
		performer.addPersonId(new PersonID()
									.setNullFlavour(NullFlavour.NI.code));
		performer.setPersonName(new PersonName("Joe Bloggs"));
		performer.setOrgId(new OrgID()
									.setID("8785675885765767")
									.setType(OrgIDType.ODSOrgID.code));
		performer.setOrgName("xx organisation");
		
		template.addEventPerformer(new ServiceEventPerformer()
											.setPerformer(performer)
											.setPerformerType(HL7PerformerType.Performer.code));
		return template;
	}
	
	public static Consent createConsent() {
		Consent template = new Consent();
		template.addID(new ConsentID("7AFDCAD6-1CE3-45A6-A176-DDAC27F46C87"));
		template.setConsentCode(DocumentConsentSnCT._Consentgiventosharepatientdatawithspecifiedthirdparty);
		return template;
	}
	
	public static EncompassingEncounter createEncounter() {
		EncompassingEncounter template = new EncompassingEncounter();
		
		template.setId("3D3B95B5-24AA-42ED-9F77-BE7ECEB78C3E");
		template.setCode(new CodedValue("11429006", "Consultation", "2.16.840.1.113883.2.1.3.2.4.15"));
		template.setEffectiveTime(
				new DateRange(new DateValue("201105191945+01"),
							  new DateValue("201105192015+01")));
		
		// Responsible Party
		PersonWithOrganizationUniversal responsible = new PersonWithOrganizationUniversal();
		responsible.addPersonId(new PersonID().setNullFlavour(NullFlavour.NI.code));
		responsible.setJobRoleName(JobRoleName._MedicalDirector);
		responsible.setPersonName(new PersonName()
										.addGivenName("Dave")
										.setFamilyName("Cornwell"));
		responsible.setOrgId(new OrgID()
									.setID("VDE232323")
									.setType(OrgIDType.ODSOrgID.code));
		responsible.setOrgName("Medway South Walk-in Centre");
		template.setResponsibleParty(responsible);
		
		// Healthcare Facility
		template.setEncounterCareSettingType(new CodedValue("313161000000107", "Example Care Setting", "2.16.840.1.113883.2.1.3.2.4.15"));
		
		return template;
	}
	
	public static TextSection createTextSection1() {
		TextSection template = new TextSection();
		template.setSectionId("E27F4264-C005-4BC3-BFA1-57C3E64B30B7");
		template.setTitle("Document Section 1");
		template.setText("<content>Some text associated with Document Section 1</content>");
		
		template.setTimeAuthored(new DateValue("200708012055+01"));
		template.setAuthor(createAuthorMaryFlemming());
		
		return template;
	}
	
	public static AuthorPersonUniversal createAuthorMaryFlemming() {
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		template.addId(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("102")
 						.setAssigningOrganisation("5L399:Medway South Out of Hours Centre"));		
 		// In the sample XML the SDSJobRoleName vocab is used (which is an empty vocab). We will use it's OID here:
 		template.setJobRoleName(new CodedValue("OOH02","Nurse Practitioner","2.16.840.1.113883.2.1.3.2.4.17.196"));
 		template.setName(new PersonName()
						.addGivenName("Mary")
						.setFamilyName("Flemming"));
		template.setOrganisationId(new OrgID()
									.setID("5L399")
									.setType(OrgIDType.ODSOrgID.code));
		template.setOrganisationName("Medway South Out of Hours Centre");
		return template;
	}

	public static TextSection createTextSection2() {
		TextSection template = new TextSection();
		template.setSectionId("773110DB-288F-4B32-8DE1-362646A65E9A");
		template.setTitle("Document Section 2");
		template.setText("<content>Some text associated with Document Section 2</content>");
		return template;
	}

	public static TextSection createTextSection3() {
		TextSection template = new TextSection();
		template.setSectionId("8271D2F1-123F-4A14-A1AC-C6C8023203CF");
		template.setTitle("Document Section 3");
		template.setText("<content>Some text associated with Document Section 3</content>");
		return template;
	}
	
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("noncodedcda/", "NonCodedCDA-roundTrip", "NonCodedCDA: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/POCD_EX010011GB02_01_AHFixed.xml");
			ClinicalDocument document = new ClinicalDocument();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/POCD_EX010011GB02_01_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("noncodedcda/", "NonCodedCDA-SerialiseTest", "NonCodedCDA: Serialise Test", "This uses the itk-payloads library to generate a full non-coded CDA document using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = nonCodedCDADoc.serialise();
			testXMLisSimilar("/TestData/POCD_EX010011GB02_01_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("noncodedcda/", "NonCodedCDA-schemaCheck", "NonCodedCDA: Schema Validation Test", "This uses the itk-payloads library to generate a full non-coded CDA document, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = nonCodedCDADoc.serialise();
			testAgainstSchema(PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT000002UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testTemplatedSchemaValidation() {
		super.init("noncodedcda/", "NonCodedCDA-templatedSchemaCheck", "NonCodedCDA: Templated Schema Test", "This uses the itk-payloads library to generate a full non-coded CDA document, uses the transforms provided by the messaging team to transform ths XML into templated (aka conformance) format, and then validates it against the published templated schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = nonCodedCDADoc.serialise();
			FileWriter.writeFile("output.xml", result.getBytes());
			testAgainstTemplatedSchema(PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT010011GB02.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
