/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.noncodedcdav2;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;

public class ClinicalDocumentTestNonXMLBody extends AbstractTest {

	public static ClinicalDocument nonCodedCDADoc;
	
	static {
		nonCodedCDADoc = createNonCodedCDANonXMLBody();
	}
	
	public static ClinicalDocument createNonCodedCDANonXMLBody() {
		ClinicalDocument template = ClinicalDocumentTest.createCommonFields();
		// Non XML Body
		template.setNonXMLBodyType(AttachmentType.Base64.code);
		template.setNonXMLBodyMediaType("text/xml");
		String data = FileLoader.loadFileOnClasspath("/TestData/Templates/attachment1.xml");
		// Using the standard Java 6 base64 encoder
		String base64data = Base64.encodeBase64String(data.getBytes());
		template.setNonXMLBodyText(base64data);
		return template;
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("noncodedcda/", "NonCodedCDANonXML-roundTrip", "NonCodedCDA: Non XML Body Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/POCD_EX010011GB02_02_AHFixed.xml");
			ClinicalDocument document = new ClinicalDocument();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/POCD_EX010011GB02_02_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("noncodedcda/", "NonCodedCDANonXML-SerialiseTest", "NonCodedCDA: Non XML Body Serialise Test", "This uses the itk-payloads library to generate a full non-coded CDA document using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = nonCodedCDADoc.serialise();
			testXMLisSimilar("/TestData/POCD_EX010011GB02_02_AHFixed.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("noncodedcda/", "NonCodedCDANonXML-schemaCheck", "NonCodedCDA: Non XML Body Schema Validation Test", "This uses the itk-payloads library to generate a full non-coded CDA document, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = nonCodedCDADoc.serialise();
			testAgainstSchema(PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT000002UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testTemplatedSchemaValidation() {
		super.init("noncodedcda/", "NonCodedCDANonXML-templatedSchemaCheck", "NonCodedCDA: Non XML Body Templated Schema Test", "This uses the itk-payloads library to generate a full non-coded CDA document, uses the transforms provided by the messaging team to transform ths XML into templated (aka conformance) format, and then validates it against the published templated schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = nonCodedCDADoc.serialise();
			FileWriter.writeFile("output.xml", result.getBytes());
			testAgainstTemplatedSchema(PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT010011GB02.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
