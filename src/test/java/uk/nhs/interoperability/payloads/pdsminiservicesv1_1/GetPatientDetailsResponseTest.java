/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import java.util.Arrays;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.OrgName;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.SMSPAddress;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;

public class GetPatientDetailsResponseTest extends AbstractTest {

	public static GetPatientDetailsResponse full;
	
	static {
		full = createResponseFull();
	}
	
	public static GetPatientDetailsResponse createResponseFull() {
		GetPatientDetailsResponse template = new GetPatientDetailsResponse();
		
		template.setMessageID("4E25ACE2-23F8-4A37-B446-6A37F31BF76C");
		template.setResponseCode(PDSMiniServiceResponseCode._Success);
		
		template.addNHSNumber(new PatientID(PatientIDType.VerifiedNHSNumber.code, "9999345201"));
		template.setPatientName(new PersonName()
										.addGivenName("John")
										.setFamilyName("Smith"));
		template.addAddress(new SMSPAddress()
									.addAddressLine("41 Manor Grove")
									.addAddressLine("Little Manorton")
									.addAddressLine("Leeds")
									.addAddressLine("")
									.addAddressLine("")
									.setPostcode("LS17 7TR")
									.setAddressUse(AddressType.Home.code));
		template.setGender(Sex._Male);
		template.setDateOfBirth(new DateValue("19890101"));
		
		template.setGPAddress(new SMSPAddress()
									.addAddressLine("2 High Street")
									.addAddressLine("Little Manorton")
									.addAddressLine("Leeds")
									.addAddressLine("")
									.addAddressLine("")
									.setPostcode("LS17 8TY"));
		template.setGPOrganisationID(new OrgID()
									.setID("YT4")
									.setType(OrgIDType.ODSSiteCode.code));
		template.setGPPracticeName(new OrgName("Grove GP Surgery"));
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("pdsminiservice/", "GetPatientDetailsResponse-roundTrip", "GetPatientDetailsResponse: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/MiniServices/COMT_EX000016GB01_01.xml");
			GetPatientDetailsResponse document = new GetPatientDetailsResponse();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/MiniServices/COMT_EX000016GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("pdsminiservice/", "GetPatientDetailsResponse-SerialiseTest", "GetPatientDetailsResponse: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testXMLisSimilar("/TestData/MiniServices/COMT_EX000016GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("pdsminiservice/", "GetPatientDetailsResponse-schemaCheck", "GetPatientDetailsResponse: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testAgainstSchema(PropertyReader.getProperty("pdsminiSchemaPath")+"COMT_MT000016GB01.xsd", result);
		} catch (Exception e) {
			e.printStackTrace();
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
