/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.SMSPPersonName;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;

public class GetPatientDetailsByNHSNumberRequestTest extends AbstractTest {

	public static GetPatientDetailsByNHSNumberRequest full;
	
	static {
		full = createResponseFull();
	}
	
	public static GetPatientDetailsByNHSNumberRequest createResponseFull() {
		GetPatientDetailsByNHSNumberRequest template = new GetPatientDetailsByNHSNumberRequest();
		
		template.setMessageID("16C2662F-1C6E-4F38-9B3F-5084F46CE3E2");
		template.setDateOfBirth(new DateValue("19890101"));
		template.setNHSNumber("9999345201");
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("pdsminiservice/", "GetPatientDetailsByNHSNumberRequest-roundTrip", "GetPatientDetailsByNHSNumberRequest: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/MiniServices/QUPA_EX000003GB01_01.xml");
			GetPatientDetailsByNHSNumberRequest document = new GetPatientDetailsByNHSNumberRequest();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/MiniServices/QUPA_EX000003GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("pdsminiservice/", "GetPatientDetailsByNHSNumberRequest-SerialiseTest", "GetPatientDetailsByNHSNumberRequest: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testXMLisSimilar("/TestData/MiniServices/QUPA_EX000003GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("pdsminiservice/", "GetPatientDetailsByNHSNumberRequest-schemaCheck", "GetPatientDetailsByNHSNumberRequest: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testAgainstSchema(PropertyReader.getProperty("pdsminiSchemaPath")+"QUPA_MT000003GB01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
