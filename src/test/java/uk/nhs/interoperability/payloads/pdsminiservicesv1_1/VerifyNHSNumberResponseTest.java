/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.SMSPPersonName;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode;

public class VerifyNHSNumberResponseTest extends AbstractTest {

	public static VerifyNHSNumberResponse full;
	
	static {
		full = createResponseFull();
	}
	
	public static VerifyNHSNumberResponse createResponseFull() {
		VerifyNHSNumberResponse template = new VerifyNHSNumberResponse();
		
		template.setMessageID("3E25ACE2-23F8-4A37-B446-6A37F31BF77B");
		template.setResponseCode(PDSMiniServiceResponseCode._Success);
		template.setValidIdentifier("true");
		template.setNHSNumber("9999345201");
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("pdsminiservice/", "VerifyNHSNumberResponse-roundTrip", "VerifyNHSNumberResponse: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/MiniServices/COMT_EX000013GB01_01.xml");
			VerifyNHSNumberResponse document = new VerifyNHSNumberResponse();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/MiniServices/COMT_EX000013GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("pdsminiservice/", "VerifyNHSNumberResponse-SerialiseTest", "VerifyNHSNumberResponse: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testXMLisSimilar("/TestData/MiniServices/COMT_EX000013GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("pdsminiservice/", "VerifyNHSNumberResponse-schemaCheck", "VerifyNHSNumberResponse: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testAgainstSchema(PropertyReader.getProperty("pdsminiSchemaPath")+"COMT_MT000013GB01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
