/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;

public class NotificationParseProfilingTest extends AbstractTest {

	/**
	 * Test performance of serialisation and parsing of the Payload
	 */
	@Test
	public void testParseProfile() {
		super.init("performance/", "Notification-ParseProfilerTest", "Notification: Parse Profiler Test", "This uses the itk-payloads library to generate a full payload and parse it several times. A code profiler is used to capture stastics from the JVM throughout.");
		super.setExpected("");
		try {
			// We will run this as a new separate java process with hprof arguments to start the profiler
			exec(NotificationParseProfilingTest.class, "cpu", super.getDirectory(), super.getFilename());
			String filename = PropertyReader.getProperty("testReportPath") + super.getDirectory() + super.getFilename() + ".hprof"; 
			String hprofOutput = FileLoader.loadFile(filename);
			hprofOutput = hprofOutput.substring(hprofOutput.indexOf("CPU TIME (ms)"));
			super.setActual("<a href='" + super.getFilename() + ".hprof'>Full results</a><br/><br/>" + super.encodeHprofOutputsForHTML(hprofOutput));
			super.setResultPass();
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Measure HEAP use for serialisation and parsing of the Payload
	 */
	@Test
	public void testParseHeapProfile() {
		super.init("performance/", "Notification-ParseHEAPTest", "Notification: Parse HEAP Test", "This uses the itk-payloads library to generate a full payload and parse it several times. A code profiler is used to capture stastics from the JVM throughout.");
		super.setExpected("");
		try {
			// We will run this as a new separate java process with hprof arguments to start the profiler
			exec(NotificationParseProfilingTest.class, "heap", super.getDirectory(), super.getFilename());
			String filename = PropertyReader.getProperty("testReportPath") + super.getDirectory() + super.getFilename() + ".hprof"; 
			String hprofOutput = FileLoader.loadFile(filename);
			hprofOutput = hprofOutput.substring(hprofOutput.indexOf("SITES BEGIN"));
			super.setActual("<a href='" + super.getFilename() + ".hprof'>Full results</a><br/><br/>" + super.encodeHprofOutputsForHTML(hprofOutput));
			super.setResultPass();
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	public static void main(String[] args) throws Exception {
		String documentXML = FileLoader.loadFileOnClasspath("/TestData/REPC_EX000001GB01_01.xml");
		String testOutput = runPerfTest(EventNotificationTest.createNotification(), false, true, documentXML, 5);
		System.out.println(testOutput);
	}
}
