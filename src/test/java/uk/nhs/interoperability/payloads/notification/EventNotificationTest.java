/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.SystemID;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentEvent;
import uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType;
import uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType;

public class EventNotificationTest extends AbstractTest {
	private static final Logger logger = LoggerFactory.getLogger(EventNotificationTest.class);

	private static EventNotification notification;
	
	static {
		try {
			notification = createNotification();
		} catch (Exception e) {
			logger.error("Error creating notification", e);
		}
	}
	
	public static EventNotification createNotification() throws Exception {
		EventNotification document;
		document = new EventNotification();
		document.setTime(new DateValue("20121127151500+0000"));
		document.setNotificationID("1A97FCE0-389F-11E2-81C1-0800200C9A66");
		
		// ********** ORIGINATING SYSTEM ********************************
		OriginatingSystem originator = new OriginatingSystem();
		originator.setSystemID(new SystemID()
								.setType(SystemIDType.LocalSystemID.code)
								.setAssigningOrganisation("RA8:St Elsewhere's Hospital")
								.setID("112YT"));
		originator.setOrgID(new OrgID()
								.setType(OrgIDType.ODSOrgID.code)
								.setID("RA8"));
		originator.setOrgName("St Elsewhere's Hospital");
		document.setOriginatingSystem(originator);

		// ********** EVENT *********************************************
		Event event = new Event();
		event.setEventID("1A97FCE1-389F-11E2-81C1-0800200C9A66");
		event.setEventTime(new DateValue("20121127130000+0000"));
		event.setEventType(NotificationEventType._01);
		event.setDocumentType(NotificationDocumentType._861421000000109);
		event.setDocumentFormat("text/xml");
		event.setDocumentProfileID("urn:nhs-en:profile:EndofLifeRecordCDADocument-v1-0");
		event.setEventSubtype(NotificationDocumentEvent._01);
		document.setEvent(event);		

		// ********** CONTACT PERSON ************************************
		ContactPerson contact = new ContactPerson();
		contact.setPersonAddress(new Address("St. Elsewhere's Hostpital,Leeds, LS13 6YP"));
		contact.setJobRoleName(JobRoleName._NR0050);
		contact.setTelephone("tel:01132111111");
		contact.setName(new PersonName("Dr. Smith"));
		contact.setOrgID(new OrgID(OrgIDType.ODSOrgID.code, "RA8"));
		contact.setOrgName("St. Elsewhere's Hospital");
		event.setContactPerson(contact);
		
		// ********** RECIPIENT *****************************************
		Recipient recipient = new Recipient();
		recipient.setRecipientAddress(new Address("St. Elsewhere's Practice, Leeds, LS1 4HY"));
		recipient.setJobRoleName(JobRoleName._NR0260);
		recipient.setTelephone("tel:01132111112");
		recipient.setOrgName("St. Elsewhere's Practice");
		document.addRecipient(recipient);
		
		// ********** PATIENT *********************************************
		Patient patient = new Patient();
		patient.setPatientAddress(new Address("111 St. Elsewhere's Street, Leeds, LS13 7TF"));
		patient.setPatientNhsNumber(new PatientID(PatientIDType.VerifiedNHSNumber.code, "1111111111"));
		patient.setPatientDOB(new DateValue("20010101"));
		patient.addPatientName(new PersonName("John Smith"));
		document.setPatient(patient);
		return document;
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("notification/", "Notification-roundTrip", "Notification: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/REPC_EX000001GB01_01.xml");
			EventNotification notification = new EventNotification();
			notification.parse(expectedXML);
			// Now re-serialise
			String result = notification.serialise();
			testXMLisSimilar("/TestData/REPC_EX000001GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("notification/", "Notification-SerialiseTest", "Notification: Serialise Test", "This uses the itk-payloads library to generate a full notification payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = notification.serialise();
			//FileWriter.writeFile("output.xml", result.getBytes());
			testXMLisSimilar("/TestData/REPC_EX000001GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("notification/", "Notification-schemaCheck", "Notification: Schema Validation Test", "This uses the itk-payloads library to generate a full notification payload, and then validates it against the published notification schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = notification.serialise();
			testAgainstSchema(PropertyReader.getProperty("notificationSchemaPath")+"REPC_MT000001GB01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
}
