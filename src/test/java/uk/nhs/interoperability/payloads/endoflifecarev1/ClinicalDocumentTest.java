/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.endoflifecarev1;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.templates.AdvanceDecisionToRefuseTreatment;
import uk.nhs.interoperability.payloads.templates.AnticipatoryMedicineBoxIssueProcedure;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.AuthoritytoLastingPowerofAttorney;
import uk.nhs.interoperability.payloads.templates.CodedSection;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.DNACPRDecisionbySeniorResponsibleClinician;
import uk.nhs.interoperability.payloads.templates.EOLPatientRelationships;
import uk.nhs.interoperability.payloads.templates.EoLCarePlan;
import uk.nhs.interoperability.payloads.templates.LanguageCommunication;
import uk.nhs.interoperability.payloads.templates.PatientRelationshipParticipantRole;
import uk.nhs.interoperability.payloads.templates.PatientRelationships;
import uk.nhs.interoperability.payloads.templates.PatientUniversal;
import uk.nhs.interoperability.payloads.templates.PrognosisAwareness;
import uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal;
import uk.nhs.interoperability.payloads.templates.RelatedPersonDetails;
import uk.nhs.interoperability.payloads.templates.Section2;
import uk.nhs.interoperability.payloads.templates.Section3;
import uk.nhs.interoperability.payloads.templates.TextSection;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency;
import uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class ClinicalDocumentTest extends AbstractTest {

	public static ClinicalDocument eolcExample;
	
	static {
		eolcExample = createEOLCExample();
	}
	
	public static ClinicalDocument createEOLCExample() {
		ClinicalDocument template = new ClinicalDocument();
		
		// ==== Set the basic document information ====
		template.setDocumentId("28BEE6CC-28F4-11E2-B493-B2916188709B");
		template.setDocumentTitle("End of Life Care Coordination Summary");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setConfidentialityCode(x_BasicConfidentialityKind._N);
		template.setDocumentSetId("2E0A4036-28F4-11E2-A045-B3916188709B");
		template.setDocumentVersionNumber("1");
		
		// ==== Now, set the "left hand side" values in the document ====
		
		// Patient
		PatientUniversal patient = createPatient();
		template.setPatient(patient);
		// Author
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		AuthorPersonUniversal author = createAuthorNiralSingh();
		template.setAuthor(author);
		// Custodian (Organisation hosting the EPaCCS)
		CustodianOrganizationUniversal custodian = createCustodian();
		template.setCustodianOrganisation(custodian);
		// Recipients
		RecipientPersonUniversal recipient = createRecipient(); 
		template.addPrimaryRecipients(new PrimaryRecipient(recipient));
		
		// ==== Now create the coded sections ====
		
		// End of Life Care Plan
		EoLCarePlan carePlan = createEoLCarePlan();
		template.addCodedSections(new CodedSections(carePlan));

		// Advance decision to refuse treatment
		AdvanceDecisionToRefuseTreatment adrt = createADRT();
		template.addCodedSections(new CodedSections(adrt));

		// Anticipatory medicine box
		AnticipatoryMedicineBoxIssueProcedure ambox = createAMBox();
		template.addCodedSections(new CodedSections(ambox));
		
		// DNACPR Decision
		DNACPRDecisionbySeniorResponsibleClinician dnacpr = createDNACPR();
		template.addCodedSections(new CodedSections(dnacpr));
		
		// Patient awareness of prognosis
		PrognosisAwareness prognosis = createPrognosisAwareness();
		template.addCodedSections(new CodedSections(prognosis));
		
		// Authority to Lasting Power of Attorney
		AuthoritytoLastingPowerofAttorney lpa = createLPA();
		template.addCodedSections(new CodedSections(lpa));
		
		// ==== Now create the text sections ====
		template.setMainDocumentSectionID("71F8271A-349B-11E2-980A-A5D46088709B");
		template.addTextSections(new TextSections(createTextSection1()));
		template.addTextSections(new TextSections(createTextSection2()));
		template.addTextSections(new TextSections(createTextSection3()));
		template.addTextSections(new TextSections(createTextSection4()));
		template.addTextSections(new TextSections(createTextSection5()));
		template.addTextSections(new TextSections(createTextSection6()));
		template.addTextSections(new TextSections(createTextSection7()));
		template.addTextSections(new TextSections(createTextSection8()));
		template.addTextSections(new TextSections(createTextSection9()));
		
		return template; 
	}
	
	public static PatientUniversal createPatient() {
		PatientUniversal template = new PatientUniversal();
		template.addPatientID(new PatientID()
									.setPatientID("K12345")
									.setAssigningOrganisation("V396A:Medway PCT")
									.setPatientIDType(PatientIDType.LocalID.code));
		template.addPatientID(new PatientID()
									.setPatientID("993254128")
									.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));		
		template.addAddress(new Address()
									.addAddressLine("Appleton House")
									.addAddressLine("Lanchester Road")
									.addAddressLine("Grimsby")
									.setPostcode("DN3 1UJ")
									.setAddressUse(AddressType.Home.code));
		template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:01472354321")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		template.addTelephoneNumber(new Telecom()
									.setTelecom("mailto:mark.smith@emailfree.co.uk")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		template.addPatientName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Mark")
									.setFamilyName("Smith"));
		template.setSex(Sex._1);
		template.setBirthTime(new DateValue("19210101"));
		// Language
		LanguageCommunication language = new LanguageCommunication();
		language.setLanguage(HumanLanguage._en.code);
		//language.setMode(LanguageAbilityMode._ESP);
		language.setProficiencyLevel(LanguageAbilityProficiency._0);
		//language.setPreferenceIndNullFlavour(NullFlavour.NotAsked.code);
		template.addLanguages(language);
		// Organisation - Registered GP:
		template.setRegisteredGPOrgId(new OrgID()
									.setID("V396F")
									.setType(OrgIDType.ODSOrgID.code));
		template.setRegisteredGPOrgName("Dr De Hopper and Partners");
		template.addRegisteredGPTelephone(new Telecom()
									.setTelecom("tel:01634111222")
									.setTelecomType(TelecomUseType.WorkPlace.code));
		template.setRegisteredGPAddress(new Address()
									.addAddressLine("Freshney Green PCC")
									.addAddressLine("Grimsby")
									.setPostcode("DN34 4GB")
									.setAddressUse(AddressType.WorkPlace.code));
		return template;
	}
	
	public static AuthorPersonUniversal createAuthorNiralSingh() {
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		template.addId(new PersonID()
 						.setType(PersonIDType.LocalPersonID.code)
 						.setID("101")
 						.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST"));		
 		template.setJobRoleName(JobRoleName._NR2040);
 		template.addAddress(new Address()
						.addAddressLine("Potter Street")
						.addAddressLine("Grimsby")
						.setPostcode("DN3 6AA")
						.setAddressUse(AddressType.WorkPlace.code));
 		template.addTelephoneNumber(new Telecom("tel:01472312345"));
 		template.setName(new PersonName()
						.setTitle("Ms")
						.addGivenName("Niral")
						.setFamilyName("Singh"));
		template.setOrganisationId(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		template.setOrganisationName("St James Hospital");
		return template;
	}
	
	public static AuthorPersonUniversal createAuthorIsabellaHopkins() {
		AuthorPersonUniversal author = new AuthorPersonUniversal();
		author.addId(new PersonID()
									.setID("101")
									.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
									.setType(PersonIDType.LocalPersonID.code));
		author.setJobRoleName(JobRoleName._NR2050);
		author.addAddress(new Address()
									.addAddressLine("Potter Street")
									.addAddressLine("Grimsby")
									.setPostcode("DN3 6AA")
									.setAddressUse(AddressType.WorkPlace.code));
		author.addTelephoneNumber(new Telecom("tel:01472312345"));
		author.setName(new PersonName()
									.setTitle("Mrs")
									.addGivenName("Isabella")
									.setFamilyName("Hopkins"));
		author.setOrganisationId(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		author.setOrganisationName("St James Hospital");
		return author;
	}
	
	public static AuthorPersonUniversal createAuthorJohnManning() {
		AuthorPersonUniversal author = new AuthorPersonUniversal();
		author.addId(new PersonID()
									.setID("112")
									.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
									.setType(PersonIDType.LocalPersonID.code));
		author.setJobRoleName(JobRoleName._NR0260);
		author.addAddress(new Address()
									.addAddressLine("Freshney Green PCC")
									.addAddressLine("Carlton Court")
									.addAddressLine("Martin Road")
									.addAddressLine("Grimsby")
									.setPostcode("DN34 4GB")
									.setAddressUse(AddressType.WorkPlace.code));
		author.addTelephoneNumber(new Telecom("tel:07621846951"));
		author.setName(new PersonName()
									.setTitle("Dr")
									.addGivenName("John")
									.setFamilyName("Manning"));
		author.setOrganisationId(new OrgID()
									.setID("V365F")
									.setType(OrgIDType.ODSOrgID.code));
		author.setOrganisationName("Freshney Green PCC");
		return author;
	}
	
	public static CustodianOrganizationUniversal createCustodian() {
		CustodianOrganizationUniversal template = new CustodianOrganizationUniversal();
		template.setId(new OrgID(OrgIDType.ODSOrgID.code, "TAN01"));
		template.setName("NORTH EAST LINCOLNSHIRE CARE TRUST");
		return template;
	}
	
	public static RecipientPersonUniversal createRecipient() {
		RecipientPersonUniversal template = new RecipientPersonUniversal();
		template.addId(new RoleID().setNullFlavour(NullFlavour.NI.code)); // Non-standard null flavour used in example XML..
		template.setJobRoleName(JobRoleName._NR0260);
		template.setName(new PersonName("De Hopper"));
		template.setOrgId(new OrgID()
								.setID("V396F")
								.setType(OrgIDType.ODSOrgID.code));
		template.setOrgName("Dr De Hopper and Partners");
		return template;
	}
	
	public static EoLCarePlan createEoLCarePlan() {
		EoLCarePlan template = new EoLCarePlan();

		template.setID("EBDF8AE8-28F5-11E2-9460-80936188709B");
		template.setEffectiveFrom(new DateValue("201209111400+0000"));
		
		template.setOriginalTimeAuthored(new DateValue("201209111400+0000"));
		
		// Note: this author section for Niral Singh differs slightly from the author sections for
		// Niral Singh used elsewhere in the document, hence the repetition here. This could potentially
		// be a valid use-case if the author information is captured at different times, or even in
		// different systems.
		AuthorPersonUniversal originalAuthor = new AuthorPersonUniversal();
		originalAuthor.addId(new PersonID()
									.setID("201")
									.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
									.setType(PersonIDType.LocalPersonID.code));
		originalAuthor.setJobRoleName(JobRoleName._NR2040);
		originalAuthor.setName(new PersonName()
									.setTitle("Ms")
									.addGivenName("Niral")
									.setFamilyName("Singh"));
		originalAuthor.setOrganisationId(new OrgID()
									.setID("V356F")
									.setType(OrgIDType.ODSOrgID.code));
		originalAuthor.setOrganisationName("St James Hospital");
		template.setOriginalAuthor(originalAuthor);
		
		template.setLastAmendedTimeAuthored(new DateValue("201209111415+0000"));
		template.setLastAmendedAuthor(originalAuthor);

		template.setEoLToolSnCT(new CodedValue(EoLToolSnCT._818221000000103, "#a1"));
		template.setEOLToolStage("first stage");
		template.setPlannedReviewDate(new DateValue("20121011"));
		
		template.addPatientRelationships(new EOLPatientRelationships(createRelatedPersons()));

		return template;
	}
	
	public static PatientRelationships createRelatedPersons() {
		PatientRelationships template = new PatientRelationships();
		
 		template.setID("EF0C2E84-2A89-11E2-A57D-D9CB6088709B");
 		
 		PatientRelationshipParticipantRole rp = new PatientRelationshipParticipantRole();
 		rp.setPersonRole(JobRoleName._NR1980);
 		rp.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp.addTelephoneNumber(new Telecom("tel:014722823451"));
 		rp.addTelephoneNumber(new Telecom("tel:07831191333"));
 		rp.setPersonName(new PersonName()
 								.setTitle("Mrs")
 								.addGivenName("Emily")
 								.setFamilyName("Smith"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp));
		
 		PatientRelationshipParticipantRole rp2 = new PatientRelationshipParticipantRole();
 		rp2.setPersonRole(JobRoleName._NR1990);
 		rp2.setAssociatedJobRole(JobRoleName._NR2040);
 		rp2.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp2.addTelephoneNumber(new Telecom("tel:07833825701"));
 		rp2.setPersonName(new PersonName()
 								.setTitle("Ms")
 								.addGivenName("Niral")
 								.setFamilyName("Singh"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp2));
 		
 		PatientRelationshipParticipantRole rp3 = new PatientRelationshipParticipantRole();
 		rp3.setPersonRole(JobRoleName._NR2000);
 		rp3.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp3.addTelephoneNumber(new Telecom("tel:07822761939"));
 		rp3.setPersonName(new PersonName()
 								.setTitle("Ms")
 								.addGivenName("Gemma")
 								.setFamilyName("Hopkins"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp3));

 		PatientRelationshipParticipantRole rp4 = new PatientRelationshipParticipantRole();
 		rp4.setPersonRole(JobRoleName._NR2010);
 		rp4.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
 		rp4.addTelephoneNumber(new Telecom("tel:07871715735"));
 		rp4.setPersonName(new PersonName()
 								.setTitle("Mr")
 								.addGivenName("Alan")
 								.setFamilyName("Smith"));
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp4));

 		PatientRelationshipParticipantRole rp5 = new PatientRelationshipParticipantRole();
 		rp5.addID(new PersonID()
 						.setID("110")
 						.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
 						.setType(PersonIDType.LocalPersonID.code));
 		rp5.setPersonRole(JobRoleName._NR2020);
 		rp5.setAssociatedJobRole(JobRoleName._NR2050);
 		rp5.setAddress(new Address()
 								.addAddressLine("St James Hospital")
 								.addAddressLine("Potter Street")
 								.addAddressLine("Grimsby")
 								.setPostcode("DN3 6AA")
 								.setAddressUse(AddressType.WorkPlace.code));
 		rp5.addTelephoneNumber(new Telecom("tel:07334345612"));
 		rp5.setPersonName(new PersonName()
 								.setTitle("Mrs")
 								.addGivenName("Isabella")
 								.setFamilyName("Hopkins"));
 		rp5.setOrgId(new OrgID()
 							.setID("V356F")
 							.setType(OrgIDType.ODSOrgID.code));
 		rp5.setOrgDescription("St James Hospital");
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp5));

 		PatientRelationshipParticipantRole rp6 = new PatientRelationshipParticipantRole();
 		rp6.addID(new PersonID()
			.setID("112")
			.setAssigningOrganisation("TAN01:NORTH EAST LINCOLNSHIRE CARE TRUST")
			.setType(PersonIDType.LocalPersonID.code));
 		rp6.setPersonRole(JobRoleName._NR2030);
 		rp6.setAssociatedJobRole(JobRoleName._NR0260);
 		rp6.setAddress(new Address()
 								.addAddressLine("Freshney Green PCC")
 								.addAddressLine("Carlton Court")
 								.addAddressLine("Martin Road")
 								.addAddressLine("Grimsby")
 								.setPostcode("DN34 4GB")
 								.setAddressUse(AddressType.WorkPlace.code));
 		rp6.addTelephoneNumber(new Telecom("tel:07621846951"));
 		rp6.setPersonName(new PersonName()
 								.setTitle("Dr")
 								.addGivenName("John")
 								.setFamilyName("Manning"));
 		rp6.setOrgId(new OrgID()
 							.setID("V365F")
 							.setType(OrgIDType.ODSOrgID.code));
 		rp6.setOrgDescription("Freshney Green PCC");
 		template.addRelatedPerson(new RelatedPersonDetails().setParticipant(rp6));
		return template;
	}
	
	public static PrognosisAwareness createPrognosisAwareness() {
		PrognosisAwareness template = new PrognosisAwareness();
		
		template.setID("369FBB7A-2A86-11E2-B118-84C76088709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setPrognosisAwareness(new CodedValue(PrognosisAwarenessSnCT._751941000000100, "#a6"));
		// Main informal carer
		PatientRelationshipParticipantRole carer = new PatientRelationshipParticipantRole();
		carer.setPersonRole(JobRoleName._NR1980);
		carer.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
		// Note: The DMS specifies that if the person's ID is not known, an "NA" null flavour should
		// be used. In the example XML however, a null flavour of "NI" is used, which is why we need
		// to explicitly set that here.
		carer.addID(new PersonID().setNullFlavour(NullFlavour.NI.code));
		carer.addTelephoneNumber(new Telecom("tel:014722823451"));
		carer.addTelephoneNumber(new Telecom("tel:07831191333"));
		carer.setPersonName(new PersonName()
									.setTitle("Mrs")
									.addGivenName("Emily")
									.setFamilyName("Smith"));
		template.setMainInformalCarer(carer);
		return template;
	}

	public static AdvanceDecisionToRefuseTreatment createADRT() {
		AdvanceDecisionToRefuseTreatment template = new AdvanceDecisionToRefuseTreatment();
		template.setID("C7E93500-28F8-11E2-B6B7-30966188709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setADRTPreference(new CodedValue(EoLADRTprefSnCT._816301000000100, "#a2"));
		template.setADRTDocumentLocation("The signed copy of the documentation is available in patient bedside cabinet.");
		template.setADRTDiscussed(new CodedValue(ADRTDiscussedSnCT._820621000000107, "#a3"));	
		return template;
	}
	
	public static AnticipatoryMedicineBoxIssueProcedure createAMBox() {
		AnticipatoryMedicineBoxIssueProcedure template = new AnticipatoryMedicineBoxIssueProcedure();
		template.setID("227413CC-28FA-11E2-ADB5-72976188709B");
		// There is only one code in this vocab, so if the below is omitted, that code will still be used - however
		// we need to set it explicitly if we also want the code to include a cross-reference to a text section, as in this test
		template.setAnticipatoryMedicineBoxIssueCode(
					new CodedValue(EoLAnticipatoryMedicineBoxIssueSnCT._376201000000102, "#a4"));
		template.setTimeIssued(new DateValue("201209111400+0000"));
		template.setLocationOfBox("Bedside Table Drawer");
		return template;
	}
	
	public static DNACPRDecisionbySeniorResponsibleClinician createDNACPR() {
		DNACPRDecisionbySeniorResponsibleClinician template = new DNACPRDecisionbySeniorResponsibleClinician();

		template.setID("F3F709E0-298B-11E2-ACBD-87EE6088709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setDNACPRPreference(new CodedValue(DNACPRprefSnCT._450476008, "#a5"));

		template.setSeniorResponsibleClinicianTimeAuthored(new DateValue("201209111400+0000"));
		template.setSeniorResponsibleClinicianAuthor(createAuthorJohnManning());
		template.setDNACPRDocsLocation("The signed copy is placed at the GP surgery in locker number 12345. The scanned copy is scanned and uploaded to GP system on 11/09/2012.");
		
		return template;
	}

	public static AuthoritytoLastingPowerofAttorney createLPA() {
		AuthoritytoLastingPowerofAttorney template = new AuthoritytoLastingPowerofAttorney();
		template.setID("26919440-2A88-11E2-A205-1ECA6088709B");
		template.setEffectiveTime(new DateValue("201209111400+0000"));
		template.setAuthoritytoLPA(new CodedValue(AuthoritytoLPASnCT._816381000000105, "#a7"));
		
		PatientRelationshipParticipantRole person = new PatientRelationshipParticipantRole(); 
		person.setPersonRole(JobRoleName._NR2010);
		person.setAddress(new Address().setNullFlavour(NullFlavour.NI.code));
		person.addTelephoneNumber(new Telecom("tel:07871715735"));
		person.setPersonName(new PersonName()
									.setTitle("Mr")
									.addGivenName("Alan")
									.setFamilyName("Smith"));
		template.setLPADetails(person);
		return template;
	}
	
	public static TextSection createTextSection1() {
		TextSection template = new TextSection();
		template.setSectionId("6D5C15F4-349B-11E2-9DA6-85D46088709B");
		template.setTitle("End of Life Record Details");
		
		Section2 s2 = new Section2();
		s2.setSectionId("7612D502-349B-11E2-9477-A6D46088709B");
		s2.setTitle("Record Created By");
		s2.setText("Ms Niral Singh, Palliative Care Physician at St James Hospital");
		template.addSection2(s2);
		
		Section3 s3 = new Section3();
		s3.setSectionId("E4B5FD04-349B-11E2-96DB-DCD46088709B");
		s3.setTitle("Record Creation Date");
		s3.setText("11-Sep-2012, 14:00");
		s2.addSection3(s3);
		
		Section3 s3b = new Section3();
		s3b.setSectionId("FA885E2E-349B-11E2-8876-E0D46088709B");
		s3b.setTitle("Record Amended Date");
		s3b.setText("11-Sep-2012, 14:25");
		s2.addSection3(s3b);
		
		Section3 s3c = new Section3();
		s3c.setSectionId("0555B342-349C-11E2-A2C8-E7D46088709B");
		s3c.setTitle("Record Review Date");
		s3c.setText("11-Oct-2012");
		s2.addSection3(s3c);
		
		return template;
	}
	
	public static TextSection createTextSection2() {
		TextSection template = new TextSection();
		template.setSectionId("AD8D0E24-0243-11E2-A989-D6B26188709B");
		template.setTitle("Patient Relationships");
		template.setText("<table width=\"100%\"><tbody><tr align=\"left\" valign=\"top\"><th>Relationship with patient</th><th>Carer Details</th></tr><tr align=\"left\" valign=\"top\"><td>Main Informal Carer</td><td>Mrs Emilly Smith<br/>014722823451<br/>07831191333</td></tr><tr align=\"left\" valign=\"top\"><td>Formal Carer</td><td>Ms Niral Singh<br/>Palliative Care  Physician<br/>07333825701</td></tr><tr align=\"left\" valign=\"top\"><td>Additional Person to be Involved in Decisions</td><td>Ms Gemma Hopkins<br/>07822761939</td></tr><tr align=\"left\" valign=\"top\"><td>Lasting Power of Attorney</td><td>Mr Alan Smith<br/>07871715735</td></tr><tr align=\"left\" valign=\"top\"><td>Key Worker</td><td>Mrs Isabella Hopkins<br/>Specialist Palliative Care Nurse<br/>St James Hospital<br/>Potter Street<br/>GRIMSBY<br/>DN3 6AA<br/>07334345612</td></tr><tr align=\"left\" valign=\"top\"><td>Senior Responsible Clinician</td><td>Mr John Manning<br/>General Practitioner<br/>Freshny Green PCC<br/>GRIMSBY<br/>DN34 4GB<br/>07621846951</td></tr></tbody></table>");
		
		Section2 s2 = new Section2();
		s2.setSectionId("3D3C6C6E-3254-11E2-A6F5-78B16188709B");
		s2.setTitle("Authority to LPA");
		s2.setText("<content ID=\"a7\">Patient has appointed Mr Alan Smith with personal welfare lasting power of attorney with authority for life sustaining decisions</content>");
		template.addSection2(s2);
		
		Section2 s2b = new Section2();
		s2b.setSectionId("6A0F3668-3254-11E2-B4E1-97B16188709B");
		s2b.setTitle("Prognosis Awareness");
		s2b.setText("<content ID=\"a6\">Main informal carer Mrs Emily Smith is aware of prognosis</content>");
		template.addSection2(s2b);
		
		return template;
	}
	
	public static TextSection createTextSection3() {
		TextSection template = new TextSection();
		template.setSectionId("91D3C9A4-3248-11E2-AF26-EBA76188709B");
		template.setTitle("End of Life Tool Used");
		template.setText("<content ID=\"a1\">On Liverpool care pathway for the dying</content>");
		
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		template.setAuthor(createAuthorIsabellaHopkins());
		
		Section2 s2 = new Section2();
		s2.setSectionId("D434C50A-3248-11E2-9648-00A86188709B");
		s2.setTitle("Current Stage");
		s2.setText("first stage in liverpool pathway");
		template.addSection2(s2);
		
		return template;
	}

	public static TextSection createTextSection4() {
		TextSection template = new TextSection();
		template.setSectionId("AC0C9FD8-00D6-11E2-AC78-B0726188709B");
		template.setTitle("Patient Choices");
		
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		template.setAuthor(createAuthorIsabellaHopkins());
		
		Section2 s2 = new Section2();
		s2.setSectionId("116ACB88-0191-11E2-B40B-57D36088709B");
		s2.setTitle("First preferred place of death");
		s2.setText("Home");
		template.addSection2(s2);
		
		Section2 s2b = new Section2();
		s2b.setSectionId("68CD9444-0193-11E2-9B1D-46D56088709B");
		s2b.setTitle("Second preferred place of death");
		s2b.setText("Hospice");
		template.addSection2(s2b);
		
		Section2 s2c = new Section2();
		s2c.setSectionId("6E0226C8-0193-11E2-8A9A-4DD56088709B");
		s2c.setTitle("Other patient preferences");
		s2c.setText("None currently");
		template.addSection2(s2c);
		
		return template;
	}

	public static TextSection createTextSection5() {
		TextSection template = new TextSection();
		template.setSectionId("2D56EB0C-3246-11E2-BEE0-C6A56188709B");
		template.setTitle("Advance Statements");
		template.setText("In case of emergency, Patient Informal Carer, Mrs Emily Smith should be first point of contact.");
		
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		template.setAuthor(createAuthorNiralSingh());
		
		Section2 s2 = new Section2();
		s2.setSectionId("956E6B3E-3246-11E2-840D-14A66188709B");
		s2.setTitle("Advance Decision to Refuse Treatment (ADRT)");
		s2.setText("<content ID=\"a2\">Patient has advance decision to refuse treatment</content>");
		template.addSection2(s2);
		
		Section3 s3 = new Section3();
		s3.setSectionId("E36C402C-3246-11E2-A181-7FA66188709B");
		s3.setTitle("Discussion about ADRT");
		s3.setText("<content ID=\"a3\">Patient has involved healthcare professional in advance decision to refuse treatment.</content>");
		s2.addSection3(s3);
		
		Section3 s3b = new Section3();
		s3b.setSectionId("E36C402C-3246-11E2-A181-7FA66188709B");
		s3b.setTitle("Location of ADRT documentation");
		s3b.setText("Original ADRT documentation is scanned and uploaded to EMIS on 11/09/2012. The signed copy of the documentation is available at patient bedside table.");
		s2.addSection3(s3b);
		
		return template;
	}
	
	public static TextSection createTextSection6() {
		TextSection template = new TextSection();
		template.setSectionId("2E472558-3247-11E2-BBCA-C1A66188709B");
		template.setTitle("DNACPR Decision");
		template.setText("<content ID=\"a5\">Senior responsible clinician has made DNACPR decision: Not for attempted cardiopulmonary resuscitation<br/>This record is created on 11/09/2012 and will be reviewed on 11/10/2012</content>");
		
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		template.setAuthor(createAuthorJohnManning());
		
		Section2 s2 = new Section2();
		s2.setSectionId("E9F3B2E4-3247-11E2-857F-16A76188709B");
		template.addSection2(s2);
		
		Section3 s3 = new Section3();
		s3.setSectionId("C80A6B56-023F-11E2-8ACA-08AD6188709B");
		s3.setTitle("Location of DNACPR documentation");
		s3.setText("Original DNACPR documentation is scanned and uploaded to EMIS on 11/09/2012. The signed copy of the documentation is available at GP surgery in locker number 12345.");
		s2.addSection3(s3);
		
		return template;
	}
	
	public static TextSection createTextSection7() {
		TextSection template = new TextSection();
		template.setSectionId("32895A34-023E-11E2-A13A-EFAA6188709B");
		template.setTitle("End of Life Care Observations");
		
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		template.setAuthor(createAuthorJohnManning());
		
		Section2 s2 = new Section2();
		s2.setSectionId("39E95522-023E-11E2-B794-F3AA6188709B");
		s2.setTitle("Primary EoLC Diagnosis");
		s2.setText("Carcinoma of greater curve of stomach. This condition was diagnosed on 08/Mar/2012 and is ongoing.");
		template.addSection2(s2);
		
		Section2 s2b = new Section2();
		s2b.setSectionId("3E3791D4-023E-11E2-81DE-F4AA6188709B");
		s2b.setTitle("Other EoLC Diagnosis");
		s2b.setText("Chest Infection NOS. The Patient is suffering from chest infection from 02/July/2012 and is ongoing.");
		template.addSection2(s2b);
		
		Section2 s2c = new Section2();
		s2c.setSectionId("4531036C-023E-11E2-B2E3-FCAA6188709B");
		s2c.setTitle("Disabilities");
		s2c.setText("Mild learning disablity<br/>Walking disability");
		template.addSection2(s2c);
		
		Section2 s2d = new Section2();
		s2d.setSectionId("ABCCEF6E-023E-11E2-ABAD-90AB6188709B");
		s2d.setTitle("Allergies and Adverse Reaction  Summary");
		s2d.setText("Lithium carbonate allergy<br/>No drug adverse reaction is noted on records");
		template.addSection2(s2d);
		
		return template;
	}
	
	public static TextSection createTextSection8() {
		TextSection template = new TextSection();
		template.setSectionId("EAE2350C-0242-11E2-9FB6-5BB26188709B");
		template.setTitle("End of Life Care Artefacts");
		
		template.setTimeAuthored(new DateValue("201209111400+0000"));
		template.setAuthor(createAuthorNiralSingh());
		
		Section2 s2 = new Section2();
		s2.setSectionId("EF3FC326-0242-11E2-A44C-5FB26188709B");
		s2.setTitle("Anticipatory Medicine Box");
		s2.setText("<content ID=\"a4\">Anticipatory medicine box is issued and kept in refrigerator at patient home.</content>");
		template.addSection2(s2);
		
		return template;
	}
	
	public static TextSection createTextSection9() {
		TextSection template = new TextSection();
		template.setSectionId("7B4B9094-34B0-11E2-AA20-99E66088709B");
		template.setTitle("EPaCCS Record Location");
		template.setText("URL of record: <linkHtml>http://endoflifeerecords.nhs.uk/patid?=993254128/</linkHtml>");
		return template;
	}
	
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("endoflifecare/", "EndOfLifeCare-roundTrip", "EoLC: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/POCD_EX021001GB01_01.xml");
			ClinicalDocument document = new ClinicalDocument();
			document.parse(expectedXML);
			
			String documentID = document.getDocumentId();
			Date dateOfBirth = document.getPatient().getBirthTime().getDate();
			String gpPractice = document.getPatient().getRegisteredGPOrgName();
			
			List<CodedSections> sections = document.getCodedSections();
			for(CodedSections s : sections) {
				CodedSection cs = s.getCodedEntry();
				
			}
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/POCD_EX021001GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("endoflifecare/", "EndOfLifeCare-SerialiseTest", "EoLC: Serialise Test", "This uses the itk-payloads library to generate a full end of life care CDA document using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = eolcExample.serialise();
			testXMLisSimilar("/TestData/POCD_EX021001GB01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("endoflifecare/", "EndOfLifeCare-schemaCheck", "EoLC: Schema Validation Test", "This uses the itk-payloads library to generate a full end of life care CDA document, and then validates it against the published EOLC schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = eolcExample.serialise();
			testAgainstSchema(PropertyReader.getProperty("endoflifecareSchemaPath")+"POCD_MT000002UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testTemplatedSchemaValidation() {
		super.init("endoflifecare/", "EndOfLifeCare-templatedSchemaCheck", "EoLC: Templated Schema Test", "This uses the itk-payloads library to generate a full end of life care CDA document, uses the transforms provided by the messaging team to transform ths XML into templated (aka conformance) format, and then validates it against the published EOLC templated schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = eolcExample.serialise();
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("endoflifecareSchemaPath")+"POCD_MT021001GB01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
