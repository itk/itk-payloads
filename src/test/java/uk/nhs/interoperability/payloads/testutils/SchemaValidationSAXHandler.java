/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.testutils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class SchemaValidationSAXHandler extends DefaultHandler {
	public void startDocument() {
	    //System.out.println("Start document: ");
	  }    
	    public void endDocument()  {
	    //System.out.println("End document: ");
	  }
	  
	  public void startElement(String uri, String localName, String qname, 
	                                                               Attributes attr)
	  {
	    //System.out.println("Start element: local name: " + localName + " qname: " + qname + " uri: "+uri);
	    int attrCount = attr.getLength();
	    if(attrCount>0) {
	      //System.out.println("Attributes:"); 
	      for(int i = 0 ; i<attrCount ; i++) {
	        //System.out.println("  Name : " + attr.getQName(i)); 
	        //System.out.println("  Type : " + attr.getType(i)); 
	        //System.out.println("  Value: " + attr.getValue(i)); 
	      }
	    } 
	  }
	  
	  public void endElement(String uri, String localName, String qname) {
	    //System.out.println("End element: local name: " + localName + " qname: " + qname + " uri: "+uri);
	  }
	  
	  public void characters(char[] ch, int start, int length) {
	    //System.out.println("Characters: " + new String(ch, start, length));
	  }

	  public void ignorableWhitespace(char[] ch, int start, int length) {
	    //System.out.println("Ignorable whitespace: " + new String(ch, start, length));
	  }

	  public void startPrefixMapping(String prefix, String uri) {
	    //System.out.println("Start \"" + prefix + "\" namespace scope. URI: " + uri); 
	  }

	  public void endPrefixMapping(String prefix) {
	    //System.out.println("End \"" + prefix + "\" namespace scope."); 
	  }

	  public void warning(SAXParseException spe) {
	    System.out.println("Warning at line "+spe.getLineNumber());
	    System.out.println(spe.getMessage());
	  }

	  public void fatalError(SAXParseException spe) throws SAXException {
	    System.out.println("Fatal error at line "+spe.getLineNumber());
	    System.out.println(spe.getMessage());
	    throw spe;
	  }
}
