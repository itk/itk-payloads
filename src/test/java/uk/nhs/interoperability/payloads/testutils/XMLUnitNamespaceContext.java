/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.testutils;

import java.util.Iterator;

import org.custommonkey.xmlunit.NamespaceContext;

import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public class XMLUnitNamespaceContext implements NamespaceContext {

	private static final XMLNamespaceContext NS_CONTEXT = new XMLNamespaceContext();
	
	@Override
	public String getNamespaceURI(String arg0) {
		return NS_CONTEXT.getNamespaceURI(arg0);
	}

	@Override
	public Iterator getPrefixes() {
		return NS_CONTEXT.getPrefixes();
	}

}
