package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

public class XPathConditions {
	private String xpath;
	private HashMap<String, Boolean> expandedConditionalXPaths = new HashMap<String, Boolean>();
	
	public XPathConditions(String xpath) {
		this.xpath = xpath;
		
		// Now, expand each conditional part into an XPath we can check for
		int index = 0;
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		boolean inCondition = false;
		for (int n=0; n<xpath.length(); n++) {
			char c = xpath.charAt(n);
			if (c == '[') {
				inCondition = true;
			} else if (c == ']') {
				inCondition = false;
				index++;
			} else {
				if (inCondition) {
					
				}
				if (!inCondition) {
					bos.write(c);
				}
			}
		}
	}
}
