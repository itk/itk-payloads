/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

public class ConditionalXPathTest {

	
	/*
	  Example xpath with multiple conditions: x:root/x:person[x:name='adam']/x:address[x:houseNumber='10']/x:town
	  This should be split into the following conditions:
	    x:root/x:person/x:name='adam'
	    x:root/x:person/x:address/x:houseNumber='10'
	  And the actual xpath to match:
	    x:root/x:person/x:address/x:town
	*/
	
	private static final String xpath1 = "x:root/x:person[x:name='adam']/x:address[x:houseNumber='10']/x:town";
	private static final String xpath2 = "x:root/x:person[x:name='adam']/x:address";
	/*
	@Test
	public void testSplittingConditions() {
		ConditionalXPath x = new ConditionalXPath(xpath1);
		
		assertEquals("x:root/x:person/x:address/x:town", x.unconditionalXPath);
		Set<String> conditionalXPaths = x.expandedConditionalXPaths.keySet();
		Iterator<String> i = conditionalXPaths.iterator();
		
		String xpath = i.next();
		assertEquals("x:root/x:person/x:name", xpath);
		assertEquals("adam", x.expandedConditionalXPaths.get(xpath));
		
		xpath = i.next();
		assertEquals("x:root/x:person/x:address/x:houseNumber", xpath);
		assertEquals("10", x.expandedConditionalXPaths.get(xpath));
	}
	
	@Test
	public void testSingleCondition() {
		ConditionalXPath x = new ConditionalXPath(xpath2);
		
		assertEquals("x:root/x:person/x:address", x.unconditionalXPath);
		Set<String> conditionalXPaths = x.expandedConditionalXPaths.keySet();
		Iterator<String> i = conditionalXPaths.iterator();
		
		String xpath = i.next();
		assertEquals("x:root/x:person/x:name", xpath);
		assertEquals("adam", x.expandedConditionalXPaths.get(xpath));
	}*/
}
