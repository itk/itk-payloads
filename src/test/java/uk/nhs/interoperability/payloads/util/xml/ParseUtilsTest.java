/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class ParseUtilsTest {

	private static final String xml1 = "<include file=\"DataTypes/ID.xml\"/>";
	private static final String xml2 = "<include file=\"notificationOrganisationFields.xml\" " +
										 "search1=\"#XPATH#\" replace1=\"x:COCD_TP145218GB01.IntendedRecipient/\" " +
										 "search2=\"#FIELDPREFIX#\" replace2=\"Recipient\"/>";
	private static final String xml3 = "<document><include file=\"notificationOrganisationFields.xml\" " +
										 "search1=\"#XPATH#\" replace1=\"x:COCD_TP145218GB01.IntendedRecipient/\" " +
										 "search2=\"#FIELDPREFIX#\" replace2=\"Recipient\"/>" +
									   "</document>";
	
	@Test
	public void testGetAttribute() {
		assertEquals(ParseUtils.getAttribute("file", xml1), "DataTypes/ID.xml");
		assertNull(ParseUtils.getAttribute("search", xml1));
		assertEquals(ParseUtils.getAttribute("file", xml2), "notificationOrganisationFields.xml");
	}

	@Test
	public void testGetAttributeMultiple() {
		assertEquals(ParseUtils.getAttributeMultiple("search", xml1), new ArrayList());
		assertEquals(ParseUtils.getAttributeMultiple("search", xml2), Arrays.asList("#XPATH#", "#FIELDPREFIX#"));
	}
	
	@Test
	public void testAddIncludes() {
		//TODO Fix this so it uses a special unit test classpath with pre-prepared config files
		System.out.println(ParseUtils.addIncludes(xml3));
	}
	
}
