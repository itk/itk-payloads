/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class XPathsTest {

	private static final String xpath1 = "x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:code/@displayName";

	@Test
	public void test() {
		String xpath = xpath1;
		String firstElement = XPaths.getFirstElementOnXPath(xpath);
		String elementName = XPaths.getElementNameWithoutCondition(firstElement);
		String remainder = xpath.substring(firstElement.length()+1);
		
		assertEquals("x:entryRelationship", elementName);
		assertEquals("x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']", firstElement);
		assertEquals("x:observation/x:code/@displayName", remainder);
	}
	
	@Test
	public void testGetXPathUpToFinalConditionalStatement() {
		String xpath = "element[child/@name='fred']/anotherElement[secondChild/@age='12']/finalElement";
		assertEquals("element/anotherElement", XPaths.getXPathUpToFinalConditionalStatement(xpath));
	}

}
