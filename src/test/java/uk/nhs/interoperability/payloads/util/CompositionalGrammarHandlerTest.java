package uk.nhs.interoperability.payloads.util;

import static org.junit.Assert.*;

import org.junit.Test;

import uk.nhs.interoperability.payloads.Code;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.CompositionalStatement;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceCaresettingtype;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceDocumenttype;
import uk.nhs.interoperability.payloads.vocabularies.internal.DocumentTypes;

public class CompositionalGrammarHandlerTest {

	@Test
	public void compositionalStatementParseTest() {
		String code = "810301000000103:810311000000101=373942005,810321000000107=394586005";
		String displayName = "810301000000103|Clinical document descriptor|:810311000000101|Type of clinical document|=373942005|Discharge summary|,810321000000107|Care setting of clinical document|=394586005|Gynaecology|";
		String oid = "2.16.840.1.113883.2.1.3.2.4.15.1";
		
		Code codedVal = new CodedValue(code, displayName, oid);
		
		CompositionalStatement cs = CompositionalGrammarHandler.parse(codedVal);
		
		//System.out.println(cs.displayString());
		
		assertEquals("810301000000103",cs.getFocusExpression().getCode());
		
		Code docType = cs.getValueForAttributeCode(DocumentTypes.DocumentType);
		Code careSetting = cs.getValueForAttributeCode(DocumentTypes.CareSetting);
		
		assertEquals("373942005",docType.getCode());
		assertEquals("Discharge summary",docType.getDisplayName());
		
		assertEquals("394586005",careSetting.getCode());
		assertEquals("Gynaecology",careSetting.getDisplayName());
	}

	@Test
	public void compositionalStatementSanityCheckTest() {
		String code = "810301000000103:810311000000101=373942005,810321000000107=394586005";
		String displayName = "810301000000103|Clinical document descriptor|:810311000000101|Type of clinical document|=373942005|Discharge summary|,810321000000107|Care setting of clinical document|=394586005|Gynaecology|";
		String oid = "2.16.840.1.113883.2.1.3.2.4.15.1";
		Code codedVal = new CodedValue(code, displayName, oid);
		boolean result = CompositionalGrammarHandler.sanityCheck(codedVal, "810301000000103");
		assertTrue(result);
	}
	
	@Test
	public void compositionalStatementSerialiseTest() {
		String code = "810301000000103:810311000000101=373942005,810321000000107=394586005";
		String displayName = "810301000000103|Clinical document descriptor|:810311000000101|Type of clinical document|=373942005|Discharge summary|,810321000000107|Care setting of clinical document|=394586005|Gynaecology|";
		String oid = "2.16.840.1.113883.2.1.3.2.4.15.1";
		
		CompositionalStatement cs = new CompositionalStatement(DocumentTypes.ClinicalDocumentDescriptor);
		cs.addAttributeCodeAndValueCode(DocumentTypes.DocumentType,
										CorrespondenceDocumenttype._Dischargesummary);
		cs.addAttributeCodeAndValueCode(DocumentTypes.CareSetting,
										CorrespondenceCaresettingtype._Gynaecology);
		Code serialised = CompositionalGrammarHandler.serialise(cs);
		
		assertEquals(code, serialised.getCode());
		assertEquals(displayName, serialised.getDisplayName());
		assertEquals(oid, serialised.getOID());
	}
	
	@Test
	public void compositionalStatementRoundTripTest() {
		String code = "810301000000103:810311000000101=373942005,810321000000107=394586005";
		String displayName = "810301000000103|Clinical document descriptor|:810311000000101|Type of clinical document|=373942005|Discharge summary|,810321000000107|Care setting of clinical document|=394586005|Gynaecology|";
		String oid = "2.16.840.1.113883.2.1.3.2.4.15.1";
		
		Code codedVal = new CodedValue(code, displayName, oid);
		CompositionalStatement cs = CompositionalGrammarHandler.parse(codedVal);
		Code serialised = CompositionalGrammarHandler.serialise(cs);
		
		assertEquals(code, serialised.getCode());
		assertEquals(displayName, serialised.getDisplayName());
		assertEquals(oid, serialised.getOID());
	}
	
	@Test
	public void compositionalStatementDisplayNameParseTest() {
		String displayName = "373942005|Discharge summary|";
		assertEquals("Discharge summary",CompositionalGrammarHandler.getDisplayName(displayName));
	}

}
