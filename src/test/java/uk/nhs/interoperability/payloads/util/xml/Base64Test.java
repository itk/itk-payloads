/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import static org.junit.Assert.assertEquals;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;

public class Base64Test {

	
	@Test
	public void testBase64EncoderDecoder() {
		// Load the EOLC Clinical Document
		String eolcDoc = FileLoader.loadFileOnClasspath("/TestData/POCD_EX021001GB01_01.xml");
		// Base64 encode it using the standard Java 6 base64 encoder
		String base64data = Base64.encodeBase64String(eolcDoc.getBytes());
		// Now decode it again
		String decoded = new String(Base64.decodeBase64(base64data));
		assertEquals(eolcDoc, decoded);
	}
}
