/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.templates.DNACPRDecisionbySeniorResponsibleClinician;

public class StreamXPathsTest {
	/*
	private static final String[] pathSequence = new String[] {
				"x:observation",
				"x:observation/x:templateId",
				"x:observation/x:templateId-",
				"x:observation/x:id",
				"x:observation/x:id-",
				"x:observation/x:code#1234",
				"x:observation/x:code-",
				"x:observation/x:statusCode",
				"x:observation/x:statusCode-",
				"x:observation/x:effectiveTime#19800101",
				"x:observation/x:effectiveTime-",
				"x:observation/x:value#1234",
				"x:observation/x:value-",
				"x:observation/x:author",
				"x:observation/x:author/x:templateId#template123",
				"x:observation/x:author/x:templateId-",
				"x:observation/x:author/npfitlc:contentId#template123",
				"x:observation/x:author/npfitlc:contentId-",
				"x:observation/x:author/x:functionCode",
				"x:observation/x:author/x:functionCode/@code#OA",
				"x:observation/x:author/x:functionCode/@code-",
				"x:observation/x:author/x:functionCode-",
				"x:observation/x:author/x:time",
				"x:observation/x:author/x:time-",
				"x:observation/x:author/x:templateId",
				"x:observation/x:author/x:templateId/@root",
				"x:observation/x:author/x:templateId/@root-",
				"x:observation/x:author/x:templateId-",
				"x:observation/x:author-",
				"x:observation/x:author",
				"x:observation/x:author/x:templateId#template123",
				"x:observation/x:author/x:templateId-",
				"x:observation/x:author/npfitlc:contentId#template123",
				"x:observation/x:author/npfitlc:contentId-",
				"x:observation/x:author/x:functionCode",
				"x:observation/x:author/x:functionCode/@code#DA",
				"x:observation/x:author/x:functionCode/@code-",
				"x:observation/x:author/x:functionCode-",
				"x:observation/x:author/x:time",
				"x:observation/x:author/x:time-",
				"x:observation/x:author/x:templateId",
				"x:observation/x:author/x:templateId/@root",
				"x:observation/x:author/x:templateId/@root-",
				"x:observation/x:author/x:templateId-",
				"x:observation/x:author-",
				"x:observation-"
				};
	*/
	
	/**
	 * Apply a sequence of XPaths and element/attribute values against the fields defined
	 * in the DNACPRDecisionbySeniorResponsibleClinician template to see how many fields
	 * we can match. There should be 5 matches, which includes some simple xpaths and some
	 * more complex conditional xpaths
	 */
	/*
	@Test
	public void testMatchingComplexXPaths() {
		ArrayList<String> matchedFields = new ArrayList<String>();
		
		DNACPRDecisionbySeniorResponsibleClinician template = new DNACPRDecisionbySeniorResponsibleClinician();
		StreamXPaths streamXPaths = new StreamXPaths(template);
		for (String entry : pathSequence) {
			if (entry.endsWith("-")) {
				// Simulate an end tag
				String element = entry;
				if (entry.contains("/")) {
					element = entry.substring(entry.lastIndexOf("/")+1, entry.length()-1);
				}
				//System.out.println("End: " + element);
				streamXPaths.pop();
				
			} else {
				if (entry.contains("#")) {
					String val = entry.substring(entry.indexOf("#")+1);
					String element = entry;
					if (entry.contains("/")) {
						element = entry.substring(entry.lastIndexOf("/")+1, entry.indexOf("#"));
					}
					//System.out.println("Start: " + element + " - value: " + val);
					streamXPaths.push(element);
					
					// Feed the element or attribute value in so we can check if any conditions have been
					// met in any conditional xpaths within the payload.
					streamXPaths.checkConditionalXPaths(val);
					
					// Output the xpath and any payload fields that are matched
					List<Field> fieldsMatched = streamXPaths.xpathMatches();
					String matchedField = "None";
					if (fieldsMatched != null) {
						matchedField = fieldsMatched.get(0).getName();
						matchedFields.add(matchedField);
					}
					System.out.println(streamXPaths.toString(true) + " - Field matched: " + matchedField);
					
				} else {
					String element = entry;
					if (entry.contains("/")) {
						element = entry.substring(entry.lastIndexOf("/")+1);
					}
					//System.out.println("Start: " + element);
					streamXPaths.push(element);
					
					
					// Output the xpath and any payload fields that are matched
					List<Field> fieldsMatched = streamXPaths.xpathMatches();
					String matchedField = "None";
					if (fieldsMatched != null) {
						matchedField = fieldsMatched.get(0).getName();
						matchedFields.add(matchedField);
					}
					System.out.println(streamXPaths.toString(true) + " - Field matched: " + matchedField);
				}
			}
		}
		
		assertEquals(5, matchedFields.size());
	}
	*/
}
