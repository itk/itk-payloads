/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

public class DateValueTest {

	@Test
	public void testDateValueDateDatePrecision() {
		DateValue dv;
		dv = new DateValue("2013");
		assertEquals(DatePrecision.Years, dv.getPrecision());
		dv = new DateValue("201301");
		assertEquals(DatePrecision.Months, dv.getPrecision());
		dv = new DateValue("20130101");
		assertEquals(DatePrecision.Days, dv.getPrecision());
		dv = new DateValue("2013010112+0");
		assertEquals(DatePrecision.HoursWithTimezone, dv.getPrecision());
		dv = new DateValue("2013010112");
		assertEquals(DatePrecision.Hours, dv.getPrecision());
		dv = new DateValue("201301011201+0");
		assertEquals(DatePrecision.MinutesWithTimezone, dv.getPrecision());
		dv = new DateValue("201301011201");
		assertEquals(DatePrecision.Minutes, dv.getPrecision());
		dv = new DateValue("20130101120101+0");
		assertEquals(DatePrecision.SecondsWithTimezone, dv.getPrecision());
		dv = new DateValue("20130101120101");
		assertEquals(DatePrecision.Seconds, dv.getPrecision());
		dv = new DateValue("20130101120101.01+0");
		assertEquals(DatePrecision.MillisecondsWithTimezone, dv.getPrecision());
		dv = new DateValue("20130101120101.01");
		assertEquals(DatePrecision.Milliseconds, dv.getPrecision());
	}

	@Test
	public void testDateValueString() {
		DateValue dv;
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Years);
		assertEquals("2013", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Months);
		assertEquals("201301", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Days);
		assertEquals("20130101", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Hours);
		assertEquals("2013010112", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.HoursWithTimezone);
		assertEquals("2013010112+0000", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Minutes);
		assertEquals("201301011200", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.MinutesWithTimezone);
		assertEquals("201301011200+0000", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Seconds);
		assertEquals("20130101120000", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.SecondsWithTimezone);
		assertEquals("20130101120000+0000", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.Milliseconds);
		assertEquals("20130101120000.0", dv.asString());
		dv = new DateValue(makeDate(1,1,2013,12,0,0,0), DatePrecision.MillisecondsWithTimezone);
		assertEquals("20130101120000.0+0000", dv.asString());
	}

	private static Date makeDate(int day, int month, int year, int hour, int minute, int seconds, int milliseconds) {
		Calendar date = Calendar.getInstance();
	    date.set(Calendar.YEAR, year);
	    date.set(Calendar.MONTH, month-1);
	    date.set(Calendar.DAY_OF_MONTH, day);
	    date.set(Calendar.HOUR_OF_DAY, hour);
	    date.set(Calendar.MINUTE, minute);
	    date.set(Calendar.SECOND, seconds);
	    date.set(Calendar.MILLISECOND, milliseconds);
	    return date.getTime();
	}
}
