/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.noncodedcdav2.ClinicalDocument;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.*;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType;

public class NonCodedCDAv2DocumentCreationHelperTest extends AbstractTest {

	private static NonCodedCDACommonFields minimal;
	private static NonCodedCDACommonFields full;
	
	static {
		minimal = createMinimal();
		full = createFull();
	}
	
	public static NonCodedCDACommonFields createMinimal() {
		NonCodedCDACommonFields fields = new NonCodedCDACommonFields();
		/*
		 Field name: recipients, Message: At least one recipient must be provided
		 */
		
		fields.setPatientName(new PersonName("Mr", "Mark", "Smith"));
		fields.setPatientBirthDate(new DateValue("19490101"));
		fields.setPatientGender(Sex._Male);
		fields.setPatientNHSNo("993254128");
		fields.setPatientNHSNoIsTraced(false);
		fields.setPatientAddress(new Address()
										.addAddressLine("17, County Court")
										.addAddressLine("Woodtown")
										.setPostcode("Medway"));
		fields.setUsualGPODSCode("V396F");
		fields.setUsualGPOrgName("Dr Jones and Partners");
		fields.setDocumentAuthorRole(JobRoleName._GeneralMedicalPractitioner);
		fields.setDocumentAuthorSDSID("200025166218");
		fields.setDocumentAuthorName(new PersonName ("Dr", "Simon", "Jones"));
		fields.setDocumentAuthorOrganisationODSID("V396F");
		fields.setDocumentAuthorOrganisationName("Dr Jones and Partners");
		fields.setDocumentType(Documenttype._Report);
		
		DocumentRecipient recipient = new DocumentRecipient();
		recipient.setRecipientName(new PersonName("Mr", "John", "Smith"));
		recipient.setRecipientODSCode("V396A");
		recipient.setRecipientOrganisationName("Medway PCT");
		fields.addRecipient(recipient);
		
		fields.setDocumentTitle("Report");
		fields.setCustodianOrganisationName("Medway NHS Foundation Trust");
		fields.setCustodianODSCode("5L3");
		return fields;
	}
	
	public static NonCodedCDACommonFields createFull() {
		NonCodedCDACommonFields fields = createMinimal();
		DateValue currentDate = new DateValue(new Date(), DatePrecision.Minutes);
		
		// Now add all the other fields...
		fields.setDocumentEffectiveTime(currentDate);
		fields.setDocumentSetID(CDAUUID.generateUUIDString());
		fields.setDocumentVersionNumber(1);
		
		// Patient
		fields.setPatientTelephone("0123456789");
		fields.setPatientMobile("0723456789");
		fields.setUsualGPTelephone("01111111122");
		fields.setUsualGPFax("02221111122");
		fields.setUsualGPAddress(new Address()
										.addAddressLine("Springer Street")
										.addAddressLine("Medway")
										.setPostcode("ME5 5TY")
										.setAddressUse(AddressType.WorkPlace.code));
		// Author
		fields.setTimeAuthored(currentDate);
		fields.setDocumentAuthorAddress(new Address()
										.addAddressLine("Springer Street")
										.addAddressLine("Medway")
										.setPostcode("ME5 5TY")
										.setAddressUse(AddressType.WorkPlace.code));
		fields.setDocumentAuthorSDSID("200025166218");
		fields.setDocumentAuthorTelephone("01133339999");
		// Data Enterer
		fields.setDataEntererName(new PersonName("Mr", "Simon", "Smith"));
		fields.setDataEntererSDSID("200025166218");
		fields.setDataEntererSDSRoleID("260045146218");
		// Recipient
		DocumentRecipient recipient = fields.getRecipients().get(0);
		recipient.setRecipientAddress(new Address()
											.addAddressLine("Springer Street")
											.addAddressLine("Medway")
											.setPostcode("ME5 5TY")
											.setAddressUse(AddressType.WorkPlace.code));
		recipient.setRecipientTelephone("02078884343");
		recipient.setRecipientJobRole(JobRoleName._Architect);
		// Copy Recipients
		DocumentRecipient copyRecipient = new DocumentRecipient();
		copyRecipient.setRecipientName(new PersonName("Mrs", "Jane", "Jameson"));
		copyRecipient.setRecipientODSCode("V396A");
		copyRecipient.setRecipientOrganisationName("Medway PCT");
		copyRecipient.setRecipientAddress(new Address()
											.addAddressLine("Springer Street")
											.addAddressLine("Medway")
											.setPostcode("ME5 5TY")
											.setAddressUse(AddressType.WorkPlace.code));
		copyRecipient.setRecipientJobRole(JobRoleName._ArtTherapist);
		copyRecipient.setRecipientSDSID("298745463789");
		copyRecipient.setRecipientSDSRoleID("298745111111");
		copyRecipient.setRecipientTelephone("01127756435");
		fields.addCopyRecipient(copyRecipient);
		// Second Copy Recipient
		DocumentRecipient copyRecipient2 = copyRecipient.clone();
		copyRecipient2.setRecipientName(new PersonName("Ms", "Janet", "Jameson"));
		fields.addCopyRecipient(copyRecipient2);
		// Authenticator
		fields.setAuthenticatorName(new PersonName("Mr", "Bob", "Bobson"));
		fields.setAuthenticatorSDSID("233332243124");
		fields.setAuthenticatorSDSRoleID("222222243124");
		fields.setAuthenticatedTime(currentDate);
		// Participant
		CDADocumentParticipant participant = new CDADocumentParticipant();
		participant.setParticipantName(new PersonName("Mr", "Peter", "Peterson"));
		participant.setParticipantAddress(new Address()
											.addAddressLine("Springer Street")
											.addAddressLine("Medway")
											.setPostcode("ME5 5TY")
											.setAddressUse(AddressType.WorkPlace.code));
		participant.setParticipantODSCode("V396A");
		participant.setParticipantOrganisationName("Medway PCT");
		participant.setParticipantSDSID("222222888855");
		participant.setParticipantSDSRoleID("222211100496");
		participant.setParticipantTelephone("01145589403");
		participant.setParticipantType(ParticipationType._Consultant);
		participant.setParticipantRoleClass(RoleClassAssociative._ASSIGNED);
		fields.addParticipant(participant);
		// Documentation Of
		fields.setEventCode(new CodedValue("73761001","colonoscopy","2.16.840.1.113883.2.1.3.2.4.15"));
		fields.setEventType(HL7ActType.Procedure);
		fields.setEventEffectiveFromTime(new DateValue("201105192000+01"));
		fields.setEventEffectiveToTime(new DateValue("201105192045+01"));
		fields.setEventPerformerName(new PersonName("Mr","Adam","Adamson"));
		fields.setEventODSCode("V396A");
		fields.setEventOrganisatioName("Medway PCT");
		// Consent
		fields.setConsent(DocumentConsentSnCT._Consentgivenforelectronicrecordsharing);
		// Encounter
		fields.setEncounterFromTime(new DateValue("201105192000+01"));
		fields.setEncounterToTime(new DateValue("201105192045+01"));
		fields.setEncounterType(new CodedValue("11429006", "Consultation", "2.16.840.1.113883.2.1.3.2.4.15"));
		fields.setEncounterLocationType(new CodedValue("313161000000107", "Example Care Setting", "2.16.840.1.113883.2.1.3.2.4.15"));
		fields.setEncounterLocationName("Springer Street Surgery");
		fields.setEncounterLocationAddress(new Address()
													.addAddressLine("Springer Street")
													.addAddressLine("Medway")
													.setPostcode("ME5 5TY")
													.setAddressUse(AddressType.WorkPlace.code));
		return fields;
	}
	
	
	@Test
	public void testCreateDocumentMissingMandatoryFields() {
		NonCodedCDACommonFields fields = new NonCodedCDACommonFields();
		try {
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(fields);
			fail("Expected exception was not thrown to list missing mandatory fields");
		} catch (MissingMandatoryFieldException e) {
			System.out.println(e.toString());
			// We expect 18 missing mandatory fields to be reported
			assertEquals(18, e.getMissingFields().size());
		}
	}

	@Test
	public void testCreateDocumentMinimalNonXMLBody() {
		super.init("noncodedcda/", "NonCodedCDAv2Helper-serialiseMinimalNonXMLBody", "NonCodedCDA Helper: Minimal Non-XML Body Serialise", "This uses the helper class to generate a full non-coded CDA document with a minimal set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(minimal);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = NonCodedCDADocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			//FileWriter.writeFile("output.xml", xml.getBytes());
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created minimal CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testCreateDocumentFullNonXMLBody() {
		super.init("noncodedcda/", "NonCodedCDAv2Helper-serialiseFullNonXMLBody", "NonCodedCDA Helper: Full Non-XML Body Serialise", "This uses the helper class to generate a full non coded CDA document with a full set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created full CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testMinimalNonXMLBodyDocumentSchemaCheck() {
		super.init("noncodedcda/", "NonCodedCDAv2Helper-MinimalNonXMLBodySchemaCheck", "NonCodedCDA Helper: Minimal Non-XML Body Schema Check", "This uses the helper class to generate a full non coded CDA document with a minimal set of fields, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(minimal);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = NonCodedCDADocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testMinimalNonXMLBodyDocumentTemplatedSchemaCheck() {
		super.init("noncodedcda/", "NonCodedCDAv2Helper-MinimalNonXMLBodyTemplatedCheck", "NonCodedCDA Helper: Minimal Non-XML Body Templated Check", "This uses the helper class to generate a full non coded CDA document with a minimal set of fields, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(minimal);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = NonCodedCDADocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT010011GB02.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testFullNonXMLBodyDocumentSchemaCheck() {
		super.init("noncodedcda/", "NonCodedCDAv2Helper-FullNonXMLBodySchemaCheck", "NonCodedCDA Helper: Full Non-XML Body Schema Check", "This uses the helper class to generate a full non coded CDA document with a minimal set of fields, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(full);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = NonCodedCDADocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testFullNonXMLBodyDocumentTemplatedSchemaCheck() {
		super.init("noncodedcda/", "NonCodedCDAv2Helper-FullNonXMLBodyTemplatedCheck", "NonCodedCDA Helper: Full Non-XML Body Templated Check", "This uses the helper class to generate a full non coded CDA document with a minimal set of fields, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = NonCodedCDADocumentCreationHelper.createDocument(full);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = NonCodedCDADocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("nonCodedCDASchemaPath")+"POCD_MT010011GB02.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

}
