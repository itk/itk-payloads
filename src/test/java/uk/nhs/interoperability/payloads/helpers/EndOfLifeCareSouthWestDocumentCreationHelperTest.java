/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

public class EndOfLifeCareSouthWestDocumentCreationHelperTest extends AbstractTest {

	private static EndOfLifeCareISBFields proposed;
	
	static {
		proposed = createProposed();
	}
	
	public static EndOfLifeCareISBFields createProposed() {
		EndOfLifeCareISBFields fields = new EndOfLifeCareISBFields();
		DateValue currentDate = new DateValue(new Date(), DatePrecision.Minutes);
		
		fields.setPatientName(new PersonName("Mr", "Joe", "Bloggs"));
		fields.setPatientBirthDate(new DateValue("19800101"));
		fields.setPatientGender(Sex._Male);
		fields.setPatientNHSNo("993254128");
		fields.setPatientNHSNoIsTraced(true);
		fields.setPatientAddress(new Address()
										.addAddressLine("123 The Street")
										.addAddressLine("The Town")
										.setPostcode("AB1 2CD"));
		fields.setUsualGPName(new PersonName("Dr", "Simon", "Jones"));
		fields.setUsualGPODSCode("V396F");
		fields.setUsualGPOrgName("Dr Jones and Partners");
		
		fields.setDocumentAuthoredTime(currentDate);
		fields.setDocumentAuthorRole(JobRoleName._GeneralMedicalPractitioner);
		fields.setDocumentAuthorSDSID("200025166218");
		fields.setDocumentAuthorName(new PersonName ("Dr", "Simon", "Jones"));
		fields.setDocumentAuthorOrganisationODSID("V396F");
		fields.setDocumentAuthorOrganisationName("Dr Jones and Partners");
		
		fields.setPrimaryEOLCDiagnosis("Carcinoma of greater curve of stomach. This condition was diagnosed on 08/Mar/2012 and is ongoing.");
		
		fields.setEpaccsRecordCreationDate(new DateValue("20130101"));
		
		// Should not be mandatory
		fields.setEolcTool(EoLToolSnCT._Ongoldstandardspalliativecareframeworkfinding);
		
		
		
		fields.setEPACCSOrganisationODSID("V396F");
		fields.setEPACCSOrganisation("Dr Jones and Partners");
		
		// Fields requested for the South West
		
		// Carer
		fields.setMainInformalCarerName(new PersonName("Mrs", "Susan", "Bloggs"));
		fields.setMainInformalCarerTel("02120111112");
		// Key Worker
		fields.setKeyWorkerName(new PersonName("Mrs", "Amy", "VanDerVeen"));
		fields.setKeyWorkerTelephone("01222222222");
		// Anticipatory medicines
		fields.setAnticipatoryMedicinesIssued(true);
		fields.setAnticipatoryMedicinesLocation("In fridge at home");
		fields.setAnticipatoryMedicinesDateIssued(new DateValue("20120402"));
		// DNACPR
		fields.setDNACPR(DNACPRprefSnCT._Forattemptedcardiopulmonaryresuscitationfinding);
		fields.setDNACPRDate(new DateValue("20120912"));
		fields.setDNACPRReviewDate(new DateValue("20130205"));
		fields.setDNACPRCreatedDate(new DateValue("20120910"));
		fields.setDNACPRLocation("In filing cabinet at home (top drawer)");
		// Free text box
		fields.setOtherRelevantInformation("Discussion held with wife about care plan");
		
		// Senior responsible clinician
		fields.setSeniorResponsibleClinicianName(new PersonName("Mr", "Senior", "Clinician"));
		fields.setSeniorResponsibleClinicianSDSID("9328789743214");
		fields.setSeniorResponsibleClinicianORGID("V444");
		fields.setSeniorResponsibleClinicianORGName("The Medical Centre");
		fields.setSeniorResponsibleClinicianJobRole(JobRoleName._PalliativeCarePhysician);
		fields.setSeniorResponsibleClinicianTelephone("01137766858");
		fields.setSeniorResponsibleClinicianAddress(new Address()
														.addAddressLine("432 Street")
														.addAddressLine("Town")
														.setPostcode("XX1 1XX"));

		
		return fields;
	}
	
	@Test
	public void testCreateDocumentProposed() {
		super.init("endoflifecare/", "EOLCHelper-serialiseSWProposed", "EoLC Helper: SW Proposed Serialise", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(proposed);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			// Re-serialised (for documentation purposes)
			ClinicalDocument doc2 = new ClinicalDocument();
			doc2.parse(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testProposedDocumentSchemaCheck() {
		super.init("endoflifecare/", "EOLCHelper-ProposdSchemaCheck", "EoLC Helper: Proposed Schema Check", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(proposed);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("endoflifecareSchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testProposedDocumentTemplatedSchemaCheck() {
		super.init("endoflifecare/", "EOLCHelper-ProposedTemplatedCheck", "EoLC Helper: Proposed Templated Check", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(proposed);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("endoflifecareSchemaPath")+"POCD_MT021001GB01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
