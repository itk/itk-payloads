/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

public class EndOfLifeCareDocumentCreationHelperTest extends AbstractTest {

	private static EndOfLifeCareISBFields minimal;
	private static EndOfLifeCareISBFields full;
	
	static {
		minimal = createMinimal();
		full = createFull();
	}
	
	public static EndOfLifeCareISBFields createMinimal() {
		EndOfLifeCareISBFields fields = new EndOfLifeCareISBFields();
		DateValue currentDate = new DateValue(new Date(), DatePrecision.Minutes);
		fields.setEpaccsRecordCreationDate(new DateValue("20130101"));
		fields.setPatientName(new PersonName("Mr", "Joe", "Bloggs"));
		fields.setPatientBirthDate(new DateValue("19800101"));
		fields.setPatientGender(Sex._Male);
		fields.setPatientNHSNo("993254128");
		fields.setPatientNHSNoIsTraced(true);
		fields.setPatientAddress(new Address()
										.addAddressLine("123 The Street")
										.addAddressLine("The Town")
										.setPostcode("AB1 2CD"));
		fields.setUsualGPName(new PersonName("Dr", "Simon", "Jones"));
		fields.setUsualGPODSCode("V396F");
		fields.setUsualGPOrgName("Dr Jones and Partners");
		fields.setPrimaryEOLCDiagnosis("Carcinoma of greater curve of stomach. This condition was diagnosed on 08/Mar/2012 and is ongoing.");
		fields.setEolcTool(EoLToolSnCT._Ongoldstandardspalliativecareframeworkfinding);
		fields.setDocumentAuthorRole(JobRoleName._GeneralMedicalPractitioner);
		fields.setDocumentAuthorSDSID("200025166218");
		fields.setDocumentAuthorName(new PersonName ("Dr", "Simon", "Jones"));
		fields.setDocumentAuthorOrganisationODSID("V396F");
		fields.setDocumentAuthorOrganisationName("Dr Jones and Partners");
		fields.setDocumentAuthoredTime(currentDate);
		fields.setEPACCSOrganisationODSID("V396F");
		fields.setEPACCSOrganisation("Dr Jones and Partners");
		return fields;
	}
	
	public static EndOfLifeCareISBFields createFull() {
		EndOfLifeCareISBFields fields = createMinimal();
		
		fields.setEpaccsRecordAuthoredDate(new DateValue("201201121201+0000"));
		fields.setEpaccsRecordAuthorAddress(new Address()
													.addAddressLine("432 Street")
													.addAddressLine("Town")
													.setPostcode("XX1 1XX"));
		fields.setEpaccsRecordAuthorRole(JobRoleName._PalliativeCarePhysician);
		fields.setEpaccsRecordAuthorSDSID("123456789");
		fields.setEpaccsRecordAuthorTelephone("0123456789");
		fields.setEpaccsRecordAuthorName(new PersonName("Dr", "Fred", "Andersen"));
		fields.setEpaccsRecordAuthorOrganisationODSID("V123G");
		fields.setEpaccsRecordAuthorOrganisationName("St Nowhere Hospital");
		fields.setEpaccsRecordReviewDate(new DateValue("20130601"));
		fields.setEpaccsRecordUpdatedDate(new DateValue("20130101"));
		fields.setEpaccsRecordUpdateAuthorAddress(new Address()
													.addAddressLine("6 Place")
													.addAddressLine("Area")
													.setPostcode("XY1 1ZA"));
		fields.setEpaccsRecordUpdateAuthorRole(JobRoleName._NurseConsultant);
		fields.setEpaccsRecordUpdateAuthorSDSID("87346283476");
		fields.setEpaccsRecordUpdateAuthorTelephone("01133333333");
		fields.setEpaccsRecordUpdateAuthorName(new PersonName("Mr", "James", "Smithson"));
		fields.setEpaccsRecordUpdateAuthorOrganisationODSID("AA11");
		fields.setEpaccsRecordUpdateAuthorOrganisationName("Global Community Health");
		fields.setPatientPreferredName(new PersonName("Joey"));
		fields.setPatientInterpreterNeeded(true);
		fields.setPatientPreferredSpokenLanguage(HumanLanguage._German);
		fields.setPatientDisability("Slight Limp");
		fields.setPatientTelephone("02120111111");
		fields.setPatientMobile("07920111111");
		fields.setMainInformalCarerName(new PersonName("Mrs", "Susan", "Bloggs"));
		fields.setMainInformalCarerTel("02120111112");
		fields.setMainInformalCarerAwareOfPrognosis(PrognosisAwarenessSnCT._Carerawareofprognosissituation);
		fields.setPrognosisAwarenessRecordedDate(new DateValue("20121201"));
		fields.setUsualGPTelephone("01132872624");
		fields.setUsualGPFax("01132872625");
		fields.setUsualGPAddress(new Address()
										.addAddressLine("Dr Jones Medical Practice")
										.addAddressLine("Somewhere Over the Rainbow")
										.addAddressLine("Way up High")
										.setPostcode("FT1 YBR"));
		fields.setKeyWorkerName(new PersonName("Mrs", "Amy", "VanDerVeen"));
		fields.setKeyWorkerTelephone("01222222222");
		fields.setKeyWorkerAddress(new Address()
										.addAddressLine("Key Worker Centre")
										.addAddressLine("Citysville")
										.setPostcode("KW1 1KW"));
		fields.setKeyWorkerJobRole(JobRoleName._CommunityNurse);
		fields.setKeyWorkerSDSID("2938798732864");
		fields.setKeyWorkerOrgID("V111");
		fields.setKeyWorkerOrgName("Community Nursing, Citysville");
		
		fields.addFormalCarer(new EndOfLifeCareDocumentFormalCarer(new PersonName("Mr", "Simon", "Jones"),
																	"01348876587",
																	JobRoleName._Dietitian));
		
		fields.addFormalCarer(new EndOfLifeCareDocumentFormalCarer(new PersonName("Miss", "Eileen", "Johnson"),
																	"01348446533",
																	JobRoleName._Counsellor));
		
		fields.setOtherRelevantDiagnoses("Severe Asthma");
		fields.setAllergiesAndAdverseReactions("Allergic to Penicillin");
		fields.setAnticipatoryMedicinesIssued(true);
		fields.setAnticipatoryMedicinesLocation("In fridge at home");
		fields.setAnticipatoryMedicinesDateIssued(new DateValue("20120402"));
		fields.setEolcPathwayStageNONISB("Amber");
		fields.setAdvanceStatements("None Recorded");
		fields.setPreferredPlaceOfDeath("Usual place of residence");
		fields.setPreferredPlaceOfDeathOrganisation("Not applicable");
		fields.setPreferredPlaceOfDeathAddress(new Address()
													.addAddressLine("123 The Street")
													.addAddressLine("The Town")
													.setPostcode("AB1 2CD"));
		fields.setPreferredPlaceOfDeathIsUPR("Yes");
		fields.setPreferredPlaceOfDeath2("Hospital");
		fields.setPreferredPlaceOfDeath2Organisation("St Nowhere Hospital");
		fields.setPreferredPlaceOfDeath2Address(new Address()
													.addAddressLine("432 Street")
													.addAddressLine("Town")
													.setPostcode("XX1 1XX"));
		fields.setPreferredPlaceOfDeath2IsUPR("No");
		fields.setDNACPR(DNACPRprefSnCT._Forattemptedcardiopulmonaryresuscitationfinding);
		fields.setDNACPRDate(new DateValue("20120912"));
		fields.setDNACPRReviewDate(new DateValue("20130205"));
		fields.setDNACPRCreatedDate(new DateValue("20120910"));
		fields.setDNACPRLocation("In filing cabinet at home (top drawer)");
		fields.setADRT(EoLADRTprefSnCT._HasadvancedecisiontorefuselifesustainingtreatmentMentalCapacityAct2005finding);
		fields.setADRTDiscussedWithClinicianNONISB(true);
		fields.setADRTDocumentLocation("In filing cabinet at home (bottom drawer)");
		fields.setADRTRecordedDate(new DateValue("20120923"));
		fields.setLPAName(new PersonName("Mr", "David", "Bloggs"));
		fields.setLPAAuthority(AuthoritytoLPASnCT._HasappointedpersonwithpersonalwelfarelastingpowerofattorneyMentalCapacityAct2005finding);
		fields.setLPADate(new DateValue("20110113"));
		fields.setLPATelephone("02120111112");
		fields.setAdditionalPersonToInvolve(new PersonName("Miss", "Sue", "Bloggs"));
		fields.setAdditionalPersonToInvolveTel("02198846627");
		fields.setAdditionalPersonToInvolve2(new PersonName("Mr", "Bob", "Bloggs"));
		fields.setAdditionalPersonToInvolve2Tel("02889999999");
		fields.setOtherRelevantInformation("Favourite colour is red");
		fields.setDocumentCreationDate(new DateValue("201308021234+0000"));
		fields.setDocumentSetId("D8F79E4F-4AA9-4662-9CCE-B13B5F955685");
		fields.setDocumentVersionNumber(1);
		fields.setDocumentAuthorAddress(new Address()
											.addAddressLine("432 Street")
											.addAddressLine("Town")
											.setPostcode("XX1 1XX"));
		fields.setDocumentAuthorTelephone("01135577744");
		fields.setSeniorResponsibleClinicianName(new PersonName("Mr", "Senior", "Clinician"));
		fields.setSeniorResponsibleClinicianSDSID("9328789743214");
		fields.setSeniorResponsibleClinicianORGID("V444");
		fields.setSeniorResponsibleClinicianORGName("The Medical Centre");
		fields.setSeniorResponsibleClinicianJobRole(JobRoleName._PalliativeCarePhysician);
		fields.setSeniorResponsibleClinicianTelephone("01137766858");
		fields.setSeniorResponsibleClinicianAddress(new Address()
														.addAddressLine("432 Street")
														.addAddressLine("Town")
														.setPostcode("XX1 1XX"));
		fields.setEPaCCSURL("https://www.epaccs.com");
		
		
		return fields;
	}
	
	
	@Test
	public void testCreateDocumentMissingMandatoryFields() {
		EndOfLifeCareISBFields fields = new EndOfLifeCareISBFields();
		try {
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(fields);
			fail("Expected exception was not thrown to list missing mandatory fields");
		} catch (MissingMandatoryFieldException e) {
			System.out.println(e.toString());
			// We expect 18 missing mandatory fields to be reported
			assertEquals(19, e.getMissingFields().size());
		}
	}

	@Test
	public void testCreateDocumentMinimal() {
		super.init("endoflifecare/", "EOLCHelper-serialiseMinimal", "EoLC Helper: Minimal Serialise", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(minimal);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created minimal CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testCreateDocumentFull() {
		super.init("endoflifecare/", "EOLCHelper-serialiseFull", "EoLC Helper: Full Serialise", "This uses the EOLC Helper class to generate a full end of life care CDA document with a full set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			System.out.println(xml);
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created full CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testMinimalDocumentSchemaCheck() {
		super.init("endoflifecare/", "EOLCHelper-MinimalSchemaCheck", "EoLC Helper: Minimal Schema Check", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(minimal);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("endoflifecareSchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testMinimalDocumentTemplatedSchemaCheck() {
		super.init("endoflifecare/", "EOLCHelper-MinimalTemplatedCheck", "EoLC Helper: Minimal Templated Check", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(minimal);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("endoflifecareSchemaPath")+"POCD_MT021001GB01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

}
