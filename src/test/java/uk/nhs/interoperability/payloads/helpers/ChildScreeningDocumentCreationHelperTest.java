/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Arrays;
import java.util.Date;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.childscreeningv2.ClinicalDocument;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType;
import uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType;
import uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType;
import uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;
import uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus;

public class ChildScreeningDocumentCreationHelperTest extends AbstractTest {

	private static ChildScreeningFields minimal;
	private static ChildScreeningFields typical;
	private static ChildScreeningFields full;
	
	static {
		minimal = createMinimal();
		typical = createTypical();
		full = createFull();
	}
	
	public static ChildScreeningFields createMinimal() {
		ChildScreeningFields fields = new ChildScreeningFields();
		DateValue currentDate = new DateValue(new Date(), DatePrecision.Minutes);
		
		// Author
		fields.setDocumentAuthorRole(JobRoleName._GeneralMedicalPractitioner);
		fields.setDocumentAuthorSDSID("200025166218");
		fields.setDocumentAuthorName(new PersonName ("Dr", "Simon", "Jones"));
		fields.setDocumentAuthorOrganisationODSID("V396F");
		fields.setDocumentAuthorOrganisationName("Dr Jones and Partners");
		fields.setDocumentAuthoredTime(currentDate);
		
		// Custodian
		fields.setCustodianOrganisationODSID("V396F");
		fields.setCustodianOrganisation("Dr Jones and Partners");
		
		// Patient
		fields.setPatientName(new PersonName()
									.setFamilyName("Smith")
									.addGivenName("Mark"));
		fields.setPatientBirthDate(new DateValue("20120728"));
		fields.setPatientNHSNo("9932541280");
		fields.setPatientNHSNoTraceStatus(NHSNumberTraceStatus.TraceNeedsToBeResolved);
		fields.setPatientGender(Sex._Male);
		
		// Recipients
		DocumentRecipient recipient = new DocumentRecipient();
		recipient.setRecipientSDSID("207055162234");
		recipient.setRecipientJobRole(JobRoleName._GeneralMedicalPractitioner);
		recipient.setRecipientName(new PersonName("De Hopper"));
		recipient.setRecipientODSCode("V396F");
		recipient.setRecipientOrganisationName("Dr De Hopper and Partners");
		fields.addRecipient(recipient);
		
		return fields;
	}
	
	public static ChildScreeningFields createTypical() {
		ChildScreeningFields fields = createMinimal();

		// Author
		fields.setDocumentAuthorAddress(new Address()
											.addAddressLine("Potter Street")
											.addAddressLine("Grimsby")
											.setPostcode("DN3 6AA")
											.setAddressUse(AddressType.WorkPlace.code));
		
		// Patient
		fields.setPatientAddress(new Address()
										.setAddressUse(AddressType.Home.code)
										.addAddressLine("Appleton House")
										.addAddressLine("Lanchester Road")
										.addAddressLine("Grimsby")
										.setPostcode("DN3 1UJ"));
		
		// Guardian
		fields.setGuardianName(new PersonName("Mrs", "Shelly", "Smith"));
		fields.setGuardianNHSNo("993254127");
		fields.setGuardianNHSNoTraceStatus(NHSNumberTraceStatus.Traced);
		fields.setGuardianAddress(new Address()
										.setAddressUse(AddressType.Home.code)
										.addAddressLine("Appleton House")
										.addAddressLine("Lanchester Road")
										.addAddressLine("Grimsby")
										.setPostcode("DN3 1UJ"));
		fields.setGuardianTelephone("01634111678");
		fields.setGuardianRole(GuardianRoleType._Mother);
		
		// Provider Organisation
		fields.setProviderOrganisationODSID("V396F");
		fields.setProviderOrganisation("Dr De Hopper and Partners");
		fields.setProviderOrganisationAddress(new Address()
													.addAddressLine("Freshney Green PCC")
													.addAddressLine("Grimsby")
													.setPostcode("DN34 4GB")
													.setAddressUse(AddressType.WorkPlace.code));
		fields.setProviderOrganisationTelephone("01634111222");
		fields.setProviderOrganisationType(CDAOrganizationProviderType._GPPractice);
		fields.setProviderParentOrganisationODSID("V396G");
		
		// Coded Sections
		
		// Blood Spot Screening
		fields.setDateOfBloodSpotScreening(new DateValue("201209111400+0000"));
		
		fields.setBloodSpotSampleCollectedTime(new DateValue("201209111300"));
		fields.setBloodSpotPerformerPersonSDSID("207958166432");
		fields.setBloodSpotPerformerPersonName(new PersonName("Mr", "Brian", "Weberly"));
		fields.setBloodSpotPerformerOrganisationODSID("A0991");
		fields.setBloodSpotPerformerOrganisationName("St James Hospital");
		
		fields.setBloodSpotTimeReceivedAtLab(new DateValue("201209111310"));
		fields.setBloodSpotLabOrganisationODSID("A0078");
		fields.setBloodSpotLabOrganisationName("St James Hospital");
		
		fields.setPKUScreeningValue(PKUScreeningResult._Conditionnotsuspected);
		fields.setLaboratoryConfirmPKUasCorrect(true);
		fields.setPKUScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		fields.setPKUReasonText("PKU Reason Text");
		fields.setPKUSupplementaryText("PKU supplementary text");
		
		fields.setSCDScreeningValue(SCDScreeningResult._Conditionnotsuspected);
		fields.setSCDScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		fields.setSCDReasonText("SCD Reason Text");
		fields.setSCDSupplementaryText("SCD supplementary text");
		
		fields.setCFScreeningValue(CFScreeningResult._Conditionnotsuspected);
		fields.setCFScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		fields.setCFReasonText("CFS Reason Text");
		fields.setCFSupplementaryText("CFS supplementary text");
		
		fields.setCHTScreeningValue(CHTScreeningResult._Conditionnotsuspected);
		fields.setCHTScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		fields.setCHTReasonText("CHT Reason Text");
		fields.setCHTSupplementaryText("CHT supplementary text");
		
		fields.setMCADDScreeningValue(MCADDScreeningResult._Conditionnotsuspected);
		fields.setMCADDScreeningSubStatus(BSScreeningStatusSubCodes._Notcontactablereasonableeffortsmade);
		fields.setMCADDReasonText("MCADD Reason Text");
		fields.setMCADDSupplementaryText("MCADD supplementary text");
		
		fields.setScreeningLocationStatus(BSScreeningLocStatus._NotDischargedOtherspecialistunit);
		fields.setLaboratoryCardSerialNumber("0902652801");
		fields.setPreviousLaboratoryCardSerialNumber("0902652800");
		
		// NewBornBirthDetails
		fields.setDateBirthDetailsRecorded(new DateValue("201209111400+0000"));
		fields.setGestationalAgeInWeeks("40");
		fields.setBirthOrder(1);
		fields.setNoOfFoetusInConfinement(1);
		fields.setBirthWeightInGrams("2887");
		
		return fields;
	}
	
	public static ChildScreeningFields createFull() {
		ChildScreeningFields fields = createTypical();
		
		// Hearing Screening
		fields.setHearingScreeningOutcome(NewBornHearingScreeningOutcomeSnCT._Clearresponsenofollowuprequired);
		fields.setAudiologyTestFinding(AudiologyTestingOutcomeStatus._No);
		fields.setAudiologyTestFindingEffectiveTime(new DateValue("201409111200+0000"));
		fields.setAudiologyReferralTime(new DateValue("201409111400+0000"));
		
		// Physical Examination
		fields.setDateOfPhysicalExamination(new DateValue("201410111500+0000"));
		fields.setGestationalAgeInDays(12);
		fields.setHipsExamination(HipsExaminationResult._Noabnormalitieswithriskfactors);
		fields.setUltraSoundDecision(HipsUltraSoundOutcomeDecision._Arrangeforfollowup4weekslaterwithin8weeks);
		fields.setDateOfHipsUltrasound(new DateValue("201410111800+0000"));
		fields.setExpertManagementPlan(HipsExpertManagementPlanType._Watchfulwaiting);
		fields.setDateHipsExpertManagementPlanCreated(new DateValue("201410112500+0000"));
		fields.setHeartExamination(HeartExaminationResult._Noabnormalitieswithriskfactors);
		fields.setEyesExamination(EyesExaminationResult._Noabnormalitieswithriskfactors);
		fields.setTestesExamination(TestesExaminationResult._Noabnormalities);
		
		return fields;
	}
	
	
	@Test
	public void testCreateDocumentMissingMandatoryFields() {
		ChildScreeningFields fields = new ChildScreeningFields();
		try {
			ClinicalDocument doc = ChildScreeningDocumentCreationHelper.createDocument(fields);
			fail("Expected exception was not thrown to list missing mandatory fields");
		} catch (MissingMandatoryFieldException e) {
			System.out.println(e.toString());
			// We expect 13 missing mandatory fields to be reported
			assertEquals(13, e.getMissingFields().size());
		}
	}

	@Test
	public void testCreateDocumentMinimal() {
		super.init("childscreening/", "ChildScreeningHelper-serialiseMinimal", "Child Screening Helper: Minimal Serialise", "This uses the Child Screening Helper class to generate a full CDA document with a minimal set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = ChildScreeningDocumentCreationHelper.createDocument(minimal);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created minimal CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testCreateDocumentTypical() {
		super.init("childscreening/", "ChildScreeningHelper-serialiseTypical", "Child Screening Helper: Typical Serialise", "This uses the Child Screening Helper class to generate a full CDA document with a typical set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = ChildScreeningDocumentCreationHelper.createDocument(typical);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created typical CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testCreateDocumentFull() {
		super.init("childscreening/", "ChildScreeningHelper-serialiseFull", "Child Screening Helper: Full Serialise", "This uses the Child Screening Helper class to generate a full CDA document with a full set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = ChildScreeningDocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created full CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testFullDocumentSchemaCheck() {
		super.init("childscreening/", "ChildScreeningHelper-FullSchemaCheck", "Child Screening Helper: Full Schema Check", "This uses the Child Screening Helper class to generate a full CDA document, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = ChildScreeningDocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("childscreeningSchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testFullDocumentTemplatedSchemaCheck() {
		super.init("childscreening/", "ChildScreeningHelper-FullTemplatedCheck", "Child Screening Helper: Full Templated Check", "This uses the Child Screening Helper class to generate a full CDA document, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = ChildScreeningDocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("childscreeningSchemaPath")+"POCD_MT010000GB01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

}
