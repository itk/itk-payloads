/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Date;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.ClinicalDocument;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceCaresettingtype;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

public class TransferOfCareDraftBDocumentCreationHelperTest extends AbstractTest {

	private static TransferOfCareFields minimal;
	private static TransferOfCareFields full;
	
	static {
		minimal = createMinimal();
		full = createFull();
	}
	
	public static TransferOfCareFields createMinimal() {
		TransferOfCareFields fields = new TransferOfCareFields();
		
		fields.setPatientName(new PersonName("Mr", "Mark", "Smith"));
		fields.setPatientBirthDate(new DateValue("19490101"));
		fields.setPatientGender(Sex._Male);
		fields.setPatientNHSNo("993254128");
		fields.setPatientNHSNoIsTraced(false);
		fields.setPatientAddress(new Address()
										.addAddressLine("17, County Court")
										.addAddressLine("Woodtown")
										.setPostcode("Medway"));
		fields.setUsualGPODSCode("V396F");
		fields.setUsualGPOrgName("Dr Jones and Partners");
		fields.setDocumentAuthorRole(JobRoleName._GeneralMedicalPractitioner);
		
		fields.setDocumentAuthorLocalID("SR1701");
		fields.setDocumentAuthorLocalIDAssigningAuthority("A1A:XYZ Hospital");
		fields.setDocumentAuthorName(new PersonName ("Dr", "Simon", "Jones"));
		fields.setDocumentAuthorOrganisationODSID("V396F");
		fields.setDocumentAuthorOrganisationName("Dr Jones and Partners");
		fields.setCareSetting(CorrespondenceCaresettingtype._Accidentemergency);
		
		DocumentRecipient recipient = new DocumentRecipient();
		recipient.setRecipientName(new PersonName("Mr", "John", "Smith"));
		recipient.setRecipientODSCode("V396A");
		recipient.setRecipientOrganisationName("Medway PCT");
		fields.addRecipient(recipient);
		
		fields.setDocumentTitle("Report");
		fields.setCustodianOrganisationName("Medway NHS Foundation Trust");
		fields.setCustodianODSCode("5L3");
		return fields;
	}
	
	public static TransferOfCareFields createFull() {
		TransferOfCareFields fields = createMinimal();
		DateValue currentDate = new DateValue(new Date(), DatePrecision.Minutes);
		
		// Now add all the other fields...
		fields.setDocumentEffectiveTime(currentDate);
		fields.setDocumentSetID(CDAUUID.generateUUIDString());
		fields.setDocumentVersionNumber(1);
		
		// Patient
		fields.setPatientTelephone("0123456789");
		fields.setPatientMobile("0723456789");
		fields.setUsualGPTelephone("01111111122");
		fields.setUsualGPFax("02221111122");
		fields.setUsualGPAddress(new Address()
										.addAddressLine("Springer Street")
										.addAddressLine("Medway")
										.setPostcode("ME5 5TY")
										.setAddressUse(AddressType.WorkPlace.code));
		// Author
		fields.setTimeAuthored(currentDate);
		fields.setDocumentAuthorAddress(new Address()
										.addAddressLine("Springer Street")
										.addAddressLine("Medway")
										.setPostcode("ME5 5TY")
										.setAddressUse(AddressType.WorkPlace.code));
		fields.setDocumentAuthorLocalID("SR1701");
		fields.setDocumentAuthorLocalIDAssigningAuthority("A1A:XYZ Hospital");
		fields.setDocumentAuthorTelephone("01133339999");
		// Data Enterer
		fields.setDataEntererName(new PersonName("Mr", "Simon", "Smith"));
		fields.setDataEntererSDSID("200025166218");
		fields.setDataEntererSDSRoleID("260045146218");
		// Recipient
		DocumentRecipient recipient = fields.getRecipients().get(0);
		recipient.setRecipientAddress(new Address()
											.addAddressLine("Springer Street")
											.addAddressLine("Medway")
											.setPostcode("ME5 5TY")
											.setAddressUse(AddressType.WorkPlace.code));
		recipient.setRecipientTelephone("02078884343");
		recipient.setRecipientJobRole(JobRoleName._Architect);
		// Copy Recipients
		DocumentRecipient copyRecipient = new DocumentRecipient();
		copyRecipient.setRecipientName(new PersonName("Mrs", "Jane", "Jameson"));
		copyRecipient.setRecipientODSCode("V396A");
		copyRecipient.setRecipientOrganisationName("Medway PCT");
		copyRecipient.setRecipientAddress(new Address()
											.addAddressLine("Springer Street")
											.addAddressLine("Medway")
											.setPostcode("ME5 5TY")
											.setAddressUse(AddressType.WorkPlace.code));
		copyRecipient.setRecipientJobRole(JobRoleName._ArtTherapist);
		copyRecipient.setRecipientSDSID("298745463789");
		copyRecipient.setRecipientSDSRoleID("298745111111");
		copyRecipient.setRecipientTelephone("01127756435");
		fields.addCopyRecipient(copyRecipient);
		// Second Copy Recipient
		DocumentRecipient copyRecipient2 = copyRecipient.clone();
		copyRecipient2.setRecipientName(new PersonName("Ms", "Janet", "Jameson"));
		fields.addCopyRecipient(copyRecipient2);
		// Authenticator
		fields.setAuthenticatorName(new PersonName("Mr", "Bob", "Bobson"));
		fields.setAuthenticatorSDSID("233332243124");
		fields.setAuthenticatorSDSRoleID("222222243124");
		fields.setAuthenticatedTime(currentDate);
		// Participant
		CDADocumentParticipant participant = new CDADocumentParticipant();
		participant.setParticipantName(new PersonName("Mr", "Peter", "Peterson"));
		participant.setParticipantAddress(new Address()
											.addAddressLine("Springer Street")
											.addAddressLine("Medway")
											.setPostcode("ME5 5TY")
											.setAddressUse(AddressType.WorkPlace.code));
		participant.setParticipantODSCode("V396A");
		participant.setParticipantOrganisationName("Medway PCT");
		participant.setParticipantSDSID("222222888855");
		participant.setParticipantSDSRoleID("222211100496");
		participant.setParticipantTelephone("01145589403");
		participant.setParticipantType(ParticipationType._Consultant);
		participant.setParticipantRoleClass(RoleClassAssociative._ASSIGNED);
		fields.addParticipant(participant);
		// Consent
		fields.setConsent(DocumentConsentSnCT._Consentgivenforelectronicrecordsharing);
		// Encounter
		fields.setEncounterFromTime(new DateValue("201105192000+01"));
		fields.setEncounterToTime(new DateValue("201105192045+01"));
		fields.setEncounterType(new CodedValue("11429006", "Consultation", "2.16.840.1.113883.2.1.3.2.4.15"));
		fields.setEncounterLocationType(new CodedValue("313161000000107", "Example Care Setting", "2.16.840.1.113883.2.1.3.2.4.15"));
		fields.setEncounterLocationName("Springer Street Surgery");
		fields.setEncounterLocationAddress(new Address()
													.addAddressLine("Springer Street")
													.addAddressLine("Medway")
													.setPostcode("ME5 5TY")
													.setAddressUse(AddressType.WorkPlace.code));
		// Add the PRSB sections and content
		addPRSBSections(fields);
		return fields;
	}
	
	private static void addPRSBSections(TransferOfCareFields fields) {
		// PRSB Sections
		fields.setAdmissionDetails("<table width=\"100%\"><tbody><tr><th>Admission method</th><td>Emergency</td></tr><tr><th>Date of admission</th><td>12-Feb-2015</td></tr><tr><th>Source of admission</th><td>Usual place of residence</td></tr></tbody></table>");
		fields.setAllergies("<table width=\"100%\"><tbody><tr><th>Causative agent</th><td>Penicillin</td></tr><tr><th>Description of the reaction</th><td>Rash. No swelling/anaphylaxis</td></tr></tbody></table>");
		fields.setAssessments("<table width=\"100%\"><tbody><tr><th>Assessment scales</th><td>TIMI score=2</td></tr></tbody></table>");
		fields.setClinicalSummary("<table width=\"100%\"><tbody><tr><th>Clinical Summary</th></tr><tr><td>58 year old man with an acute history of cardiac sounding chest pain lasting around 2 hours. </td></tr><tr><td>Examination unremarkable.</td></tr><tr><td>Inferior ischaemic changes on ECGs and raised Troponin T. </td></tr><tr><td>Coronary angiogram demonstrated diseased RCA, drug eluting stent successfully placed. </td></tr><tr><td>Appropriate secondary prevention medications prescribed and for follow up with Cardiac Rehabilitation team. </td></tr></tbody></table>");
		fields.setDiagnoses("<table width=\"100%\"><tbody><tr><th>Diagnosis</th></tr><tr><td>Cardiac chest pain/ACS</td></tr></tbody></table>");
		fields.setDischargeDetails("<table width=\"100%\"><tbody><tr><th>Discharging consultant</th><td>Mr Abacus</td></tr><tr><th>Date of discharge</th><td>16-Feb-2015</td></tr><tr><th>Discharge method</th><td>Patient discharged on clinical advice</td></tr><tr><th>Discharge destination</th><td>Usual place of residence</td></tr></tbody></table>");
		fields.setInformationGiven("<table width=\"100%\"><tbody><tr><th>Information Given</th></tr><tr><td>Patient seen by Cardiac Rehab and information given regarding diagnosis, lifestyle change and benefits of rehab programme.</td></tr><tr><td>Community follow up planned.</td></tr></tbody></table>");
		fields.setInvestigations("<table width=\"100%\"><tbody><tr><th>FBC</th><td> 5 x 10<content styleCode=\"super\"><sup>12</sup></content>/L, WCC: 11.1 x 10<content styleCode=\"super\"><sup>9</sup></content>/L, Hb: 150 g/dl, neutrophils: 7.0 x 10<content styleCode=\"super\"><sup>9</sup></content>/L, eosinophils: 0.40 x 10<content styleCode=\"super\"><sup>9</sup></content>/L, lymphocytes: 3.5x10<content styleCode=\"super\"><sup>9</sup></content>/L, monocytes: 0.3 x 10<content styleCode=\"super\"><sup>9</sup></content>/L</td></tr><tr><th>Arterial Blood Gases</th><td>PH: 7.41, pO<content styleCode=\"sub\"><sub>2</sub></content>: 13 kPa, pCO<content styleCode=\"sub\"><sub>2</sub></content>: 6.0kPa, HCO3: 24 mEq/L, B.E.: +2 mmol/L</td></tr><tr><th>ECG</th><td>Inferior ischaemic changes</td></tr><tr><th>Chest Xray</th><td>Normal</td></tr></tbody></table>");
		fields.setLegal("<table width=\"100%\"><tbody><tr><th>Advance decisions about treatment</th></tr><tr><td>Declines blood products - patient is a Jehovah's witness </td></tr></tbody></table>");
		fields.setMedications("<table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Ramipril</td></tr><tr><th>Dose</th><td>2.5mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Omeprazole</td></tr><tr><th>Dose</th><td>20mg</td></tr><tr><th>Medication Frequency</th><td>OD/PRN</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Aspirin</td></tr><tr><th>Dose</th><td>75mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Clopidogrel</td></tr><tr><th>Dose</th><td>75mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>1 year then stop</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Simvastatin</td></tr><tr><th>Dose</th><td>40mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>continue</td></tr></tbody></table><paragraph/><table width=\"100%\"><tbody><tr><th>Medication Name</th><td>Bisoprolol</td></tr><tr><th>Dose</th><td>5mg</td></tr><tr><th>Medication Frequency</th><td>OD</td></tr><tr><th>Route</th><td>oral</td></tr><tr><th>Medication Recommendations</th><td>Uptitrate according to BP and HR</td></tr></tbody></table>");
		fields.setMedicationsAuthorName(new PersonName("Mr", "Simon", "Smith"));
		fields.setMedicationsAuthorTelephone("0123456789");
		fields.setMedicationsAuthorDate(new DateValue("201105202000+01"));
		fields.setMedicationsAuthorOrgName("Medway NHS Foundation Trust");
		fields.setMedicationsAuthorODSCode("5L3");
		fields.setResearch("<table width=\"100%\"><tbody><tr><th>Participation in research</th></tr><tr><td>None</td></tr></tbody></table>");
		fields.setConcerns("<table width=\"100%\"><tbody><tr><th>Patient and carer concerns</th></tr><tr><td>Concern re possibility of heart attack/similarity with mothers illness.</td></tr><tr><td>Worried about ability to drive and work/financial implications.</td></tr></tbody></table>");
		fields.setPersonCompletingRecord("<table width=\"100%\"><tbody><tr><th>Name</th><td>Dr Paul Rastall</td></tr><tr><th>Designation or role</th><td>On call Dr</td></tr><tr><th>Grade</th><td>SpR</td></tr><tr><th>Specialty</th><td>Medicine</td></tr></tbody></table>");
		fields.setPlan("<table width=\"100%\"><tbody><tr><th>Actions</th></tr><tr><td>GP please continue secondary preventative medication.</td></tr><tr><td>Doses will be uptitrated by Cardiac Rehab team.</td></tr></tbody></table>");
		fields.setProcedures("<table width=\"100%\"><tbody><tr><th>Procedures</th></tr><tr><td>Coronary Angiogram with stent to right coronary artery (RCA)</td></tr></tbody></table>");
		fields.setAlerts("<table width=\"100%\"><tbody><tr><th>Safety Alerts</th></tr><tr><td>None</td></tr></tbody></table>");
		fields.setSocialContext("<table width=\"100%\"><tbody><tr><th>Household composition</th><td>Lives alone</td></tr><tr><th>Lives alone</th><td>No</td></tr><tr><th>Occupational history</th><td>Self employed electrician</td></tr></tbody></table>");	
	}
	
	
	@Test
	public void testCreateDocumentMissingMandatoryFields() {
		TransferOfCareFields fields = new TransferOfCareFields();
		try {
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(fields);
			fail("Expected exception was not thrown to list missing mandatory fields");
		} catch (MissingMandatoryFieldException e) {
			System.out.println(e.toString());
			// We expect 10 missing mandatory fields to be reported
			assertEquals(10, e.getMissingFields().size());
		}
	}

	@Test
	public void testCreateDocumentMinimalNonXMLBody() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-serialiseMinimalNonXMLBody", "TOCeDischargeDraftB Helper: Minimal Non-XML Body Serialise", "This uses the helper class to generate a full eDischarge document with a minimal set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(minimal);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = TransferOfCareDraftBDocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			//FileWriter.writeFile("output.xml", xml.getBytes());
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created minimal CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testCreateDocumentFullWorkgroupStructuredBody() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-serialiseFullWorkgroupStructuredBody", "TOCeDischargeDraftB Helper: Full Structured Body With Workgroup Author Serialise", "This uses the helper class to generate a full eDischarge document with a full set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			TransferOfCareFields fields = createFull();
			fields.setDocumentAuthorLocalID(null);
			fields.setDocumentAuthorLocalIDAssigningAuthority(null);
			fields.setDocumentAuthorAddress(null);
			fields.setDocumentAuthorWorkgroupName("Medical Ward 1");
			fields.setPatientLocalID("12345");
			fields.setPatientLocalIDAssigningAuthority("A1A:XYZ Hospital");
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(fields);
			// Add structured sections
			TransferOfCareDraftBDocumentCreationHelper.addPRSBSections(fields, doc);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			//FileWriter.writeFile("output.xml", xml.getBytes());
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created minimal CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testCreateDocumentFullNonXMLBody() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-serialiseFullNonXMLBody", "TOCeDischargeDraftB Helper: Full Non-XML Body Serialise", "This uses the helper class to generate a full eDischarge document with a full set of fields, and serialises to a CDA document, which is compared with an expected document example.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			
			// Generate the rendered version
			super.render(xml);
			
			content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created full CDA document</div>");
			content = content.replaceAll("#EXPECTED#", "");
			addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testMinimalNonXMLBodyDocumentSchemaCheck() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-MinimalNonXMLBodySchemaCheck", "TOCeDischargeDraftB Helper: Minimal Non-XML Body Schema Check", "This uses the helper class to generate a full eDischarge document with a minimal set of fields, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(minimal);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = TransferOfCareDraftBDocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("TOCDraftBSchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testMinimalNonXMLBodyDocumentTemplatedSchemaCheck() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-MinimalNonXMLBodyTemplatedCheck", "TOCeDischargeDraftB Helper: Minimal Non-XML Body Templated Check", "This uses the helper class to generate a full eDischarge document with a minimal set of fields, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(minimal);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = TransferOfCareDraftBDocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("TOCDraftBSchemaPath")+"POCD_MT000026GB01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

	@Test
	public void testFullNonXMLBodyDocumentSchemaCheck() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-FullNonXMLBodySchemaCheck", "TOCeDischargeDraftB Helper: Full Non-XML Body Schema Check", "This uses the helper class to generate a full eDischarge CDA document with a minimal set of fields, and serialises to a CDA document, which is validated against the on-the-wire schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(full);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = TransferOfCareDraftBDocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// Now check it is valid according to the schema
			testAgainstSchema(
					PropertyReader.getProperty("TOCDraftBSchemaPath")+"POCD_MT000002UK01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	@Test
	public void testFullNonXMLBodyDocumentTemplatedSchemaCheck() {
		super.init("edischarge-draftB/", "TOCeDischargeDraftBHelper-FullNonXMLBodyTemplatedCheck", "TOCeDischargeDraftB Helper: Full Non-XML Body Templated Check", "This uses the helper class to generate a full eDischarge document with a minimal set of fields, and serialises to a CDA document, transforms it to templated format, which is validated against the templated schema.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = TransferOfCareDraftBDocumentCreationHelper.createDocument(full);
			String data = "PFRlc3RYTUw+PC9UZXN0WE1MPg==";
			doc = TransferOfCareDraftBDocumentCreationHelper.addNonXMLBody(doc, AttachmentType.Base64, "text/xml", data);
			// Serialise to a CDA XML Document
			String xml = doc.serialise();
			// And then transform and re-test against templated schema
			testAgainstTemplatedSchema(
					PropertyReader.getProperty("TOCDraftBSchemaPath")+"POCD_MT000026GB01.xsd",
					xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}

}
