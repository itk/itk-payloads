/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;

public class EndOfLifeCareDocumentParsingHelperTest extends AbstractTest {

	private static EndOfLifeCareISBFields full;
	
	static {
		full = EndOfLifeCareDocumentCreationHelperTest.createFull();
	}
	
	@Test
	public void testDocumentMinimalRoundTrip() {
		super.init("endoflifecare/", "EOLCParsingHelper-roundTripMinimal", "NOT YET FULLY WORKING: EoLC Parsing Helper: Round-Trip Minimal", "This uses the EOLC Helper class to generate a full end of life care CDA document with a minimal set of fields, then uses the EOLC Parsing helper to re-extract the values into the associate ISB fields, and checks they match the original values.");
		try {
			// Use the helper to create the document
			ClinicalDocument doc = EndOfLifeCareDocumentCreationHelper.createDocument(full);
			// Serialise to a CDA XML Document
			//String xml = doc.serialise();
			
			// Generate the rendered version
			//super.render(xml);
			
			EndOfLifeCareISBFields output = EndOfLifeCareDocumentParsingHelper.getISBFields(doc);
			
			//assertEquals(full.toString(), output.toString());
			
			//content = content.replaceAll("#TESTRESULT#", "<div class='pass'>PASS: Successfully created minimal CDA document</div>");
			//content = content.replaceAll("#EXPECTED#", "");
			//addActualResultWithXML(xml);
			
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
