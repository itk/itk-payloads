package uk.nhs.interoperability.payloads;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public class DummyPayload extends AbstractPayload implements Payload {
	@Override
	public String serialise() {
		return super.serialise(this);
	}

	@Override
	public Map<String, Field> getFieldDefinitions() {
		return new LinkedHashMap<String, Field>();
	}

	@Override
	public String getClassName() {
		return null;
	}

	@Override
	public String getPackage() {
		return null;
	}

	@Override
	public String getVersionedName() {
		return null;
	}

	@Override
	public XMLNamespaceContext getNamespaceContext() {
		return new XMLNamespaceContext();
	}

	@Override
	public String getRootNode() {
		return "";
	}
}