/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.DummyPayload;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType;
import uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType;
import uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID;
import uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode;

public class ApplicationAcknowledgementTest extends AbstractTest {

	public static ApplicationAcknowledgement full;
	
	static {
		full = createFull();
		
	}
	
	public static ApplicationAcknowledgement createFull() {
		ApplicationAcknowledgement template = new ApplicationAcknowledgement();
		
		template.setID("BBBBP10A-A9D1-A411-F824-9F7A00A33757");
		
		template.setCreationTime(new DateValue("200401061130"));
		template.setHL7VersionCode(HL7StandardVersionCode._V3NPfIT30);
		template.setInteractionID("PRPA_IN020000UK02");
		template.setProcessingCode(ProcessingID._Production);
		template.setProcessingModeCode(ProcessingMode._Currentprocessing);
		
		template.setReceiverASID("ZZZ999-100000000900001");
		template.setSenderASID("ZZZ000-100000000800001");
		
		template.setAcknowledgementType(AcknowledgementType._ApplicationAcknowledgementReject.code);
		template.setAcknowledgementDetailType(AcknowledgementDetailType._Error.code);
		template.setErrorCode(new CodedValue("1", "Access denied", "2.16.840.1.113883.2.1.3.2.4.17.32"));
		template.setMessageRef("11111111-65D3-EC42-BC31-62522532BC5D");
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("spine-common/", "ApplicationAcknowledgement-roundTrip", "ApplicationAcknowledgement: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/MCCI_EX020101UK12_01.xml");
			ApplicationAcknowledgement document = new ApplicationAcknowledgement();
			document.parse(expectedXML, parentNamespaces);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/MCCI_EX020101UK12_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("spine-common/", "ApplicationAcknowledgement-SerialiseTest", "ApplicationAcknowledgement: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise("Message", parentNamespaces);
			testXMLisSimilar("/TestData/Spine/MCCI_EX020101UK12_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	/*@Test
	public void testSchemaValidation() {
		super.init("spine-common/", "ApplicationAcknowledgement-schemaCheck", "ApplicationAcknowledgement: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			ApplicationAcknowledgement template = createFull();
			
			// The schema requires a payload, so we will add an empty one
			//class EmptySpinePayload extends DummyPayload implements ControlActWrapper { }
			//template.setControlActWrapper(new EmptySpinePayload());
			
			// Serialise the objects
			String result = template.serialise("Message", parentNamespaces);
			testAgainstSchema(PropertyReader.getProperty("spineSchemaPath")+"MCCI_MT020101UK12.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}*/
}
