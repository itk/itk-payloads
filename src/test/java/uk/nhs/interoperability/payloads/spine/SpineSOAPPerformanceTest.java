/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;

public class SpineSOAPPerformanceTest extends AbstractTest {

	/**
	 * Test performance of serialisation and parsing of the Notification Payload
	 */
	@Test
	public void testPerformance() {
		super.init("performance/", "SpineSOAP_SimpleTraceQuery-PerformanceTest", "SpineSOAP_SimpleTraceQuery: Performance Test", "This uses the itk-payloads library to generate a full payload, serialise it several times measuring the time it takes each time. It then re-parses it several times measuring the time that takes also.");
		super.setExpected("");
		try {
			// We will run this as a new separate java process to include the initialisation time in the timings
			String testOutput = exec(SpineSOAPPerformanceTest.class, null, super.getDirectory(), super.getFilename());
			super.setActual(testOutput);
			super.setResultPass();
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	public static void main(String[] args) throws Exception {
		String testOutput = runPerfTest(SpineSOAPTest.createSimpleTraceQuery());
		System.out.println(testOutput);
	}
}
