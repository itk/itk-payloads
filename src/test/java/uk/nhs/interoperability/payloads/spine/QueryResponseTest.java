/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DummyPayload;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier;

public class QueryResponseTest extends AbstractTest {

	public static QueryResponse full;
	
	static {
		full = createFull();
	}
	
	public static QueryResponse createFull() {
		QueryResponse template = new QueryResponse();
		
		template.setAuthorUUID("5452322");
		template.setAuthorRoleID("ABCD");
		template.setAuthorUserRoleProfileID("7643324387");
		template.setSenderASID("accredited system id");
		template.setWorkstationID("workstationid");
		template.setWorkstationIDOID("9.99.999.9.999999.9.9.9.9.9.99");
		
		// QueryAck
		template.setQueryResponse(uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse._Issuedetected.code);
		
		// Error codes
		template.addErrorCodes(new DetectedIssueEvent()
									.setErrorCode(new CodedValue("1", "No match to a service user record", "2.16.840.1.113883.2.1.3.2.4.17.42"))
									.setErrorCodeQualifier(DetectedIssueQualifier._ER));
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("spine-common/", "QueryResponse-roundTrip", "QueryResponse: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/QUQI_EX030101UK04.xml");
			QueryResponse document = new QueryResponse();
			document.parse(expectedXML, parentNamespaces);
			
			// Now re-serialise
			String result = document.serialise();
			FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/QUQI_EX030101UK04.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("spine-common/", "QueryResponse-SerialiseTest", "QueryResponse: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise("ControlActEvent", parentNamespaces);
			testXMLisSimilar("/TestData/Spine/QUQI_EX030101UK04.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	/*@Test
	public void testSchemaValidation() {
		super.init("spine-common/", "QueryResponse-schemaCheck", "QueryResponse: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			QueryResponse template = createFull();

			// The schema requires a payload, so we will add an empty one
			class EmptySpinePayload extends DummyPayload implements SpinePayload { }
			template.setQueryPayload(new EmptySpinePayload());
			
			String result = template.serialise("ControlActEvent", parentNamespaces);
			testAgainstSchema(PropertyReader.getProperty("spineSchemaPath")+"QUQI_MT050000UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}*/
}
