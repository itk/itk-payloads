/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetNHSNumberRequest;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class SimpleTraceResponseTest extends AbstractTest {

	public static SimpleTraceResponse full;
	
	static {
		full = createResponseFull();
	}
	
	public static SimpleTraceResponse createResponseFull() {
		SimpleTraceResponse template = new SimpleTraceResponse();
		
		template.addAddress(new Address()
									.addAddressLine("PRINCES EXCHANGE")
									.addAddressLine("PRINCES SQUARE")
									.setPostcode("LS1 4HY")
									.setAddressKey("12345678")
									.setAddressUse(AddressType.Home.code));
		
		template.setNHSNumber(new PDSPatientID(PDSPatientIDType.NHSNo.code, "1234567"));
		template.addTelecom(new Telecom()
									.setTelecom("tel:01132806007")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		template.addTelecom(new Telecom()
									.setTelecom("tel:07737758370")
									.setTelecomType(TelecomUseType.MobileContact.code));
		template.addTelecom(new Telecom()
									.setTelecom("mailto:mayjune@months.co.uk")
									.setTelecomType(TelecomUseType.HomeAddress.code));
		
		// Person
		template.setAdministrativeGender(Sex._Female.code);
		template.setDateOfBirth(new DateValue("19800727"));
		template.setDeceasedTime(new DateValue("2006090206"));
		template.setMultipleBirthOrderNumber("2");
		template.addPatientName(new PersonName()
										.setTitle("Ms")
										.addGivenName("Three")
										.addGivenName("Zoe")
										.setFamilyName("Editestpatient")
										.setNameType(PersonNameType.Legal.code));
		
		// Registered GP
		template.setRegisteredGPID("Z99993");
		
		// Death information
		template.setDeathNotification(DeathNotification._InformaldeathnoticereceivedviaanupdatefromalocalNHSOrganisationsuchasGPorTrust);
		template.setDeceasedTime(new DateValue("2006090206"));
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("spine-pds/", "SimpleTraceResponse-roundTrip", "SimpleTraceResponse: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/PRPA_EX000200UK01_01.xml");
			SimpleTraceResponse document = new SimpleTraceResponse();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/PRPA_EX000200UK01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("spine-pds/", "SimpleTraceResponse-SerialiseTest", "SimpleTraceResponse: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testXMLisSimilar("/TestData/Spine/PRPA_EX000200UK01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("spine-pds/", "SimpleTraceResponse-schemaCheck", "SimpleTraceResponse: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testAgainstSchema(PropertyReader.getProperty("spineSchemaPath")+"PRPA_MT000200UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
