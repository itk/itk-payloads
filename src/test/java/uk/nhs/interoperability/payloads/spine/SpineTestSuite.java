package uk.nhs.interoperability.payloads.spine;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ApplicationAcknowledgementTest.class,
				PdsTraceQueryTest.class,
				QueryControlActTest.class,
				QueryResponseTest.class,
				SendMessagePayloadTest.class,
				SimpleTraceResponseTest.class,
				SpineSOAPPerformanceTest.class,
				SpineSOAPResponseTest.class,
				SpineSOAPTest.class
			  })
public class SpineTestSuite {
}
