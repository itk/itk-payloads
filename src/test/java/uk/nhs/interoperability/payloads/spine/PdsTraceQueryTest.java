/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.io.ByteArrayInputStream;

import org.junit.Test;
import org.w3c.dom.Document;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.SMSPPersonName;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.util.xml.PayloadParser;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;

public class PdsTraceQueryTest extends AbstractTest {

	public static PdsTraceQuery full;
	
	static {
		full = createFull();
	}
	
	public static PdsTraceQuery createFull() {
		PdsTraceQuery template = new PdsTraceQuery();
		
		template.setPostcode("EX2 5SE");
		template.setGender(Sex._Male.code);
		template.setDateOfBirth(new DateValue("19780719"));
		template.setDateOfDeath(new DateValue("20031202"));
		template.setName(new SMSPPersonName("John","Parkinson"));
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("spine-pds/", "PdsTraceQuery-roundTrip", "PdsTraceQuery: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/QUPA_EX000001UK01_01.xml");
			PdsTraceQuery document = new PdsTraceQuery();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/QUPA_EX000001UK01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTripDOM() {
		super.init("spine-pds/", "PdsTraceQuery-roundTripDOM", "PdsTraceQuery: Round Trip Test DOM", "This test loads a sample XML file, converts is to a DOM and then uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXMLstring = FileLoader.loadFileOnClasspath("/TestData/Spine/QUPA_EX000001UK01_01.xml");
			ByteArrayInputStream is = new ByteArrayInputStream(expectedXMLstring.getBytes());
			Document expectedDOM = PayloadParser.generateDOM(is, true);
			
			PdsTraceQuery document = new PdsTraceQuery();
			document.parse(expectedDOM);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/QUPA_EX000001UK01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("spine-pds/", "PdsTraceQuery-SerialiseTest", "PdsTraceQuery: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testXMLisSimilar("/TestData/Spine/QUPA_EX000001UK01_01.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and validating using the schema in the DMS
	 */
	@Test
	public void testSchemaValidation() {
		super.init("spine-pds/", "PdsTraceQuery-schemaCheck", "PdsTraceQuery: Schema Validation Test", "This uses the itk-payloads library to generate a full payload, and then validates it against the published schema provided with the domain message specification.");
		try {
			// Serialise the objects
			String result = full.serialise();
			testAgainstSchema(PropertyReader.getProperty("spineSchemaPath")+"QUPA_MT000001UK01.xsd", result);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
