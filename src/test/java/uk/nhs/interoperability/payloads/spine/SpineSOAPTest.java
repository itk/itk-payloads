/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.SMSPPersonName;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID;
import uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType;

public class SpineSOAPTest extends AbstractTest {

	public static SpineSOAP simpleTraceQuery;
	
	static {
		simpleTraceQuery = createSimpleTraceQuery();
	}
	
	public static SpineSOAP createSimpleTraceQuery() {
		/*
		// Create Payload
		PdsTraceQuery payload = new PdsTraceQuery();
		payload.setPostcode("LS11AB");
		payload.setAddressUse(AddressType.Home.code);
		payload.setGender(Sex._Male.code);
		payload.setDateOfBirth(new DateValue("19800101"));
		payload.setName(new SMSPPersonName("Adam","Hatherly"));
		payload.setNameType(PersonNameType.Legal.code);
		
		// Wrap it in a control act
		QueryControlAct controlAct = new QueryControlAct();
		controlAct.setSenderASID("SENDERASID");
		controlAct.setQueryPayload(payload);
		
		// Wrap it in a transmission wrapper
		SendMessagePayload transmission = new SendMessagePayload();
		transmission.setID("1E2F020C-9EF9-11E4-B608-01335BA1706F");
		transmission.setCreationTime(new DateValue("20150118100231"));
		transmission.setHL7VersionCode(HL7StandardVersionCode._V3NPfIT30);
		transmission.setInteractionID("QUPA_IN000005UK01");
		transmission.setProcessingCode(ProcessingID._Production);
		transmission.setProcessingModeCode(ProcessingMode._Currentprocessing);
		transmission.setReceiverASID("RECEIVERASID");
		transmission.setSenderASID("SENDERASID");
		transmission.setControlActEvent(controlAct);*/

		// And finally, wrap it in the SOAP mesage
		SpineSOAP template = new SpineSOAP();
		template.setMessageID("uuid:1E31730D-9EF9-11E4-B608-01335BA1706F");
		//template.setMessageID(CDAUUID.generateUUIDString());
		template.setAction("urn:nhs:names:services:pdsquery/QUPA_IN000005UK01");
		template.setTo("https://pds-sync.nis1.national.ncrs.nhs.uk/syncservice-pds/pds");
		template.setFrom("FROMADDRESS");
		template.setReceiverASID("RECEIVERASID");
		template.setSenderASID("SENDERASID");
		template.setReplyAddress("FROMADDRESS");
		//template.setPayload(new SpineSOAPSimpleTraceBody(transmission));
		
		SpineSOAPSimpleTraceBody body = new SpineSOAPSimpleTraceBody();
		// Transmission Wrapper Fields (Send Message Payload)
		body.setTransmissionID("1E2F020C-9EF9-11E4-B608-01335BA1706F");
		//body.setTransmissionID(CDAUUID.generateUUIDString());
		body.setTransmissionCreationTime(new DateValue("20150118100231"));
		body.setTransmissionHL7VersionCode(HL7StandardVersionCode._V3NPfIT30);
		body.setTransmissionInteractionID("QUPA_IN000005UK01");
		body.setTransmissionProcessingCode(ProcessingID._Production);
		body.setTransmissionProcessingModeCode(ProcessingMode._Currentprocessing);
		body.setTransmissionReceiverASID("RECEIVERASID");
		body.setTransmissionSenderASID("SENDERASID");
		
		// Control Act Wrapper Fields
		body.setControlActSenderASID("SENDERASID");
		
		// Actual Query Payload
		body.setPostcode("LS11AB");
		body.setAddressUse(AddressType.Home.code);
		body.setGender(Sex._Male.code);
		body.setDateOfBirth(new DateValue("19800101"));
		body.setName(new SMSPPersonName("Adam","Hatherly"));
		body.setNameType(PersonNameType.Legal.code);
		
		template.setPayload(body);
		
		return template; 
	}
	
	/**
	 * This is a test using the example XML included with the DMS, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("spine-pds/", "SpineSOAP_SimpleTraceQuery-roundTrip", "SpineSOAP_SimpleTraceQuery: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/SpineSOAP_SimpleTraceQuery.xml");
			SpineSOAP document = new SpineSOAP();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/SpineSOAP_SimpleTraceQuery.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * Test serialising some objects and comparing them with the sample XML in the DMS
	 */
	@Test
	public void testSerialise() {
		super.init("spine-pds/", "SpineSOAP_SimpleTraceQuery-SerialiseTest", "SpineSOAP_SimpleTraceQuery: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = simpleTraceQuery.serialise();
			testXMLisSimilar("/TestData/Spine/SpineSOAP_SimpleTraceQuery.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
