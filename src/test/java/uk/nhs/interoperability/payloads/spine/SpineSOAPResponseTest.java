/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import org.junit.Test;

import uk.nhs.interoperability.payloads.AbstractTest;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType;
import uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID;
import uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode;
import uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

public class SpineSOAPResponseTest extends AbstractTest {

	public static SpineSOAPResponse simpleTraceResponse;
	
	static {
		simpleTraceResponse = createSimpleTraceResponse();
	}
	
	public static SpineSOAPResponse createSimpleTraceResponse() {
		SpineSOAPResponse template = new SpineSOAPResponse();
		template.setMessageID("uuid:DB2D2F0F-AC50-11E4-A415-CFAAA3AA5D7E");
		//template.setMessageID(CDAUUID.generateUUIDString());
		template.setAction("urn:nhs:names:services:pdsquery/QUPA_IN000007UK01");
		template.setFrom("https://pds-sync.nis1.national.ncrs.nhs.uk/syncservice-pds/pds");
		template.setTo("FROMADDRESS");
		template.setReceiverASID("SENDERASID");
		template.setSenderASID("RECEIVERASID");
		template.setReplyAddress("uuid:B8CC4890-4C81-4825-B16F-73713425A508");
		//template.setPayload(new SpineSOAPSimpleTraceBody(transmission));
		
		SpineSOAPSimpleTraceResponseBody body = new SpineSOAPSimpleTraceResponseBody();
		
		// Transmission Wrapper Fields (Application Acknowledgement)
		body.setTransmissionID("DB24CA9B-AC50-11E4-A415-CFAAA3AA5D7E");
		body.setTransmissionCreationTime(new DateValue("20150204093320"));
		body.setTransmissionHL7VersionCode(HL7StandardVersionCode._V3NPfIT30);
		body.setTransmissionInteractionID("QUPA_IN000007UK01");
		body.setTransmissionProcessingCode(ProcessingID._Production);
		body.setTransmissionProcessingModeCode(ProcessingMode._Currentprocessing);
		body.setTransmissionReceiverASID("SENDERASID");
		body.setTransmissionSenderASID("RECEIVERASID");
		
		body.setTransmissionAcknowledgementType(AcknowledgementType._ApplicationAcknowledgementAccept.code);
		body.setTransmissionMessageRef("B8CC4890-4C81-4825-B16F-73713425A508");
		
		// Control Act Wrapper Fields
		body.setControlActSenderASID("234345434565");
		body.setControlActQueryResponse(QueryResponse._OK.code);
		
		// Actual Response Payload
		body.addAddress(new Address()
							.setAddressUse(AddressType.Home.code)
							.addAddressLine("")
							.addAddressLine("PRINCES EXCHANGE")
							.addAddressLine("")
							.addAddressLine("PRINCES SQUARE")
							.addAddressLine("")
							.setPostcode("LS1 4HY")
							.setAddressKey("12345678"));
		body.setNHSNumber(new PDSPatientID(PDSPatientIDType.NHSNo.code,
											"942111111111"));
		body.addTelecom(new Telecom("tel:01132806007")
								.setTelecomType(TelecomUseType.HomeAddress.code));
		body.addTelecom(new Telecom("tel:07737758370")
								.setTelecomType(TelecomUseType.MobileContact.code));
		body.addTelecom(new Telecom("mailto:mayjune@months.co.uk")
								.setTelecomType(TelecomUseType.HomeAddress.code));
		body.setAdministrativeGender(Sex._Female.code);
		body.setDateOfBirth(new DateValue("19661111"));
		body.addPatientName(new PersonName()
									.setTitle("Ms")
									.addGivenName("SADIA")
									.addGivenName("DANA")
									.setFamilyName("PURVES")
									.setNameType(PersonNameType.Legal.code));
		body.setRegisteredGPID("Z99993");
		
		template.setPayload(body);
		
		return template; 
	}
	
	/**
	 * This is a test using an example XML, to a round-trip and compare..
	 */
	@Test
	public void testRoundTrip() {
		super.init("spine-pds/", "SpineSOAP_SimpleTraceResponse-roundTrip", "SpineSOAP_SimpleTraceResponse: Round Trip Test", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/SpineSOAP_SimpleTraceResponse.xml");
			SpineSOAPResponse document = new SpineSOAPResponse();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/SpineSOAP_SimpleTraceResponse.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
	
	/**
	 * This is a test using an example XML, to a round-trip and compare..
	 * This was an example of a response from TKW, but one field appears to be invalid, so will need further investigation
	 */
	/*@Test
	public void testRoundTrip2() {
		super.init("spine-pds/", "SpineSOAP_SimpleTraceResponse-roundTrip2", "SpineSOAP_SimpleTraceResponse: Round Trip Test 2", "This test loads a sample XML file, uses the itk-payloads library to parse it into a set of Java objects, and then re-serialises it back to an XML file. The resulting XML file should match the original text file (i.e. from an XML perspective they should be logically the same).");
		try {
			// First parse
			String expectedXML = FileLoader.loadFileOnClasspath("/TestData/Spine/SpineSOAP_SimpleTraceResponse_02.xml");
			SpineSOAPResponse document = new SpineSOAPResponse();
			document.parse(expectedXML);
			
			// Now re-serialise
			String result = document.serialise();
			//FileWriter.writeFile("roundTripOutput.xml", result.getBytes());
			
			testXMLisSimilar("/TestData/Spine/SpineSOAP_SimpleTraceResponse_02.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}*/
	
	/**
	 * Test serialising some objects and comparing them with a sample XML
	 */
	@Test
	public void testSerialise() {
		super.init("spine-pds/", "SpineSOAP_SimpleTraceResponse-SerialiseTest", "SpineSOAP_SimpleTraceResponse: Serialise Test", "This uses the itk-payloads library to generate a full payload using the same values as are used in the XML example provided with the DMS. This is then serialised and compared against the example to ensure it matches.");
		try {
			// Serialise the objects
			String result = simpleTraceResponse.serialise();
			testXMLisSimilar("/TestData/Spine/SpineSOAP_SimpleTraceResponse.xml", result, false);
		} catch (Exception e) {
			super.exception(e);
		} finally {
			super.writeResults();
		}
	}
}
