<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
<xsl:output method="text" encoding="UTF-8"/>
<xsl:param name="package"></xsl:param>
<xsl:param name="configFileKey"></xsl:param>

<!--

TODO: Loop through all <type> elements with a package attribute so the relevant imports can be added to the payload class

-->

<xsl:template match="/fieldList/class">@@@@<xsl:value-of select="@name"/>.java@@@@<xsl:call-template name="MakeVersionedName"/>@@@@<xsl:value-of select="@rootNode"/>@@@@<xsl:apply-templates select="interfaces"/>@@@@/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package <xsl:value-of select="$package"/>;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;

<xsl:call-template name="JavaDocComment"/>
public class <xsl:value-of select="@name"/> extends AbstractPayload implements Payload <xsl:apply-templates select="interfaces"/> {
    	
		protected static final String configFileKey = "<xsl:value-of select="$configFileKey"/>";
		protected static final String name = "<xsl:value-of select="@name"/>";
		protected static final String shortName = "<xsl:value-of select="@shortName"/>";
		protected static final String rootNode = "<xsl:value-of select="@rootNode"/>";
		protected static final String version = "<xsl:value-of select="@version"/>";
		private static final String packg = "<xsl:value-of select="$package"/>";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		<xsl:call-template name="MakeConstructors"/>		
		
	<xsl:apply-templates select="field" mode="gettersAndSetters"/>

			protected static Map&lt;String, Field&gt; fieldDefinitions = new LinkedHashMap&lt;String, Field&gt;() {{
			<xsl:apply-templates select="field" mode="definitions"/>
    	}};
	
	<xsl:value-of select="customMethod"/>

		static {<xsl:apply-templates select="namespaces/namespace"/>
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map&lt;String, Field&gt; getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public <xsl:value-of select="@name"/>() {
			fields = new LinkedHashMap&lt;String, Object&gt;();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public <xsl:value-of select="@name"/>(InputStream xml) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}
</xsl:template>

<xsl:template match="interfaces">
	<xsl:for-each select="interface">, <xsl:value-of select="."/></xsl:for-each>
</xsl:template>

<xsl:template match="deprecated">@deprecated</xsl:template>

<xsl:template match="namespaces/namespace">
			namespaces.addNamespace("<xsl:value-of select="@prefix"/>", "<xsl:value-of select="."/>", <xsl:choose><xsl:when test="@default='true'">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>);</xsl:template>

<!-- Constructors -->
<xsl:template name="MakeConstructors">
	<xsl:choose>
	<!-- If specific constructor parameters are specified in the config file, use those -->
	<xsl:when test="constructor">
		<xsl:apply-templates select="constructor"/>
	</xsl:when>
	<!-- If not, generate a contstructor with all the fields in it -->
	<xsl:otherwise>
		/**
		 * Constructor for payload object<xsl:call-template name="defaultConstructorJavadoc"/>
		 */
	    public <xsl:value-of select="@name"/>(<xsl:call-template name="defaultConstructorParams"/>) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			<xsl:apply-templates select="field" mode="constructorSetters"/>
		}
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="constructor">
		/**<xsl:apply-templates select="param" mode="constructorJavaDoc"/>
		 */
		public <xsl:value-of select="../@name"/>(<xsl:call-template name="constructorUserSpecifiedParams"/>) {
			fields = new LinkedHashMap&lt;String, Object&gt;();
			<xsl:apply-templates select="param" mode="constructorSetters"/>
		}
</xsl:template>

<xsl:template name="constructorUserSpecifiedParams">
	<xsl:for-each select="param">
	<xsl:choose>
	<xsl:when test="../../field[name=current()]/maxOccurs &gt; 1">List&lt;<xsl:apply-templates select="../../field[name=current()]/type" mode="withPackage"/>&gt;<xsl:text> </xsl:text><xsl:value-of select="."/><xsl:choose><xsl:when test="position() = last()"></xsl:when><xsl:otherwise>, </xsl:otherwise></xsl:choose>
	</xsl:when>
	<xsl:otherwise>
		<xsl:apply-templates select="../../field[name=current()]/type" mode="withPackage"/><xsl:text> </xsl:text><xsl:value-of select="."/><xsl:choose><xsl:when test="position() = last()"></xsl:when><xsl:otherwise>, </xsl:otherwise></xsl:choose>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>
</xsl:template>

<xsl:template match="param" mode="constructorSetters">
			set<xsl:value-of select="."/>(<xsl:value-of select="."/>);</xsl:template>

<xsl:template match="param" mode="constructorJavaDoc">
		 * @param <xsl:value-of select="."/><xsl:text> </xsl:text><xsl:apply-templates select="../../field[name=current()]/type"/></xsl:template>


<xsl:template name="defaultConstructorParams">
	<xsl:for-each select="field[not(fixed)]">
	<xsl:choose>
	<xsl:when test="maxOccurs &gt; 1">
		List&lt;<xsl:apply-templates select="type" mode="withPackage"/>&gt;<xsl:text> </xsl:text><xsl:value-of select="name"/><xsl:choose><xsl:when test="position() = last()"></xsl:when><xsl:otherwise>, </xsl:otherwise></xsl:choose>
	</xsl:when>
	<xsl:otherwise>
		<xsl:apply-templates select="type" mode="withPackage"/><xsl:text> </xsl:text><xsl:value-of select="name"/><xsl:choose><xsl:when test="position() = last()"></xsl:when><xsl:otherwise>, </xsl:otherwise></xsl:choose>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>
</xsl:template>

<xsl:template name="defaultConstructorJavadoc">
	<xsl:for-each select="field[not(fixed)]">
		 * @param <xsl:value-of select="name"/><xsl:text> </xsl:text><xsl:value-of select="name"/>
	</xsl:for-each>
</xsl:template>


<xsl:template match="field" mode="constructorSetters">
	<xsl:if test="not(fixed)">
			set<xsl:value-of select="name"/>(<xsl:value-of select="name"/>);</xsl:if>
</xsl:template>

<!-- JavaDoc Content -->
<xsl:template name="JavaDocComment">
/**
 * This is the <xsl:value-of select="@name"/> object
 * &lt;br&gt;This class is generated using the message config in resources/config
 * &lt;br&gt;
 * &lt;br&gt;<xsl:apply-templates select="description"/>
 * &lt;br&gt;The fields that can be set for this payload are:
 * &lt;ul&gt;
 	<xsl:for-each select="field[not(fixed)]">
		<xsl:choose>
		<xsl:when test="maxOccurs &gt; 1">
 * &lt;li&gt;List&amp;lt;<xsl:apply-templates select="type" mode="withUrl"/>&amp;gt;<xsl:text> </xsl:text><xsl:value-of select="name"/>&lt;/li&gt;</xsl:when>
		<xsl:otherwise>
 * &lt;li&gt;<xsl:apply-templates select="type" mode="withUrl"/><xsl:text> </xsl:text><xsl:value-of select="name"/>&lt;/li&gt;</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
 * &lt;/ul&gt;
 * @author Adam Hatherly
 * <xsl:apply-templates select="deprecated"/>
 */
</xsl:template>
<xsl:template match="mandatory">
	<xsl:choose>
	<xsl:when test="contains(.,'true')">
		 * &lt;br&gt;&lt;br&gt;This field is MANDATORY</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="description">
	<xsl:apply-templates/></xsl:template>

<xsl:template match="br">&lt;br&gt;</xsl:template>
<xsl:template match="li">&lt;li&gt;<xsl:apply-templates/>&lt;/li&gt;</xsl:template>
<xsl:template match="ul">&lt;ul&gt;<xsl:apply-templates/>&lt;/ul&gt;</xsl:template>

<xsl:template match="vocabulary">
  		 * &lt;br&gt;NOTE: This field should be populated using the "<xsl:value-of select="."/>" vocabulary.
  <xsl:choose>
  	<xsl:when test="@type = 'internal'">       * @see uk.nhs.interoperability.payloads.vocabularies.internal.<xsl:value-of select="."/>
  	</xsl:when>
  	<xsl:otherwise>       * @see uk.nhs.interoperability.payloads.vocabularies.generated.<xsl:value-of select="."/>
  	</xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="vocabulary" mode="withPackage">
  <xsl:choose>
  	<xsl:when test="@type = 'internal'">uk.nhs.interoperability.payloads.vocabularies.internal.<xsl:value-of select="."/></xsl:when>
  	<xsl:otherwise>uk.nhs.interoperability.payloads.vocabularies.generated.<xsl:value-of select="."/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="vocabulary" mode="nameOnly"><xsl:value-of select="."/></xsl:template>

<!-- Types -->
<xsl:template match="type" mode="withPackage">
	<xsl:choose>
  		<xsl:when test=". = 'XML'">String</xsl:when>
  		<xsl:when test=". = 'FHIRCodedValue'">Code</xsl:when>
  		<xsl:otherwise><xsl:value-of select="./@package"/><xsl:value-of select="."/></xsl:otherwise>
  	</xsl:choose>
</xsl:template>

<xsl:template match="type" mode="withoutPackage">
	<xsl:choose>
  		<xsl:when test=". = 'XML'">String</xsl:when>
  		<xsl:when test=". = 'FHIRCodedValue'">Code</xsl:when>
  		<xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
  	</xsl:choose>
</xsl:template>

<xsl:template match="type" mode="withUrl">
	<xsl:choose>
  		<xsl:when test="@package">{@link <xsl:value-of select="@package"/><xsl:value-of select="."/><xsl:text> </xsl:text><xsl:value-of select="."/>}</xsl:when>
  		<xsl:when test=". = 'String'">String</xsl:when>
  		<xsl:when test=". = 'CodedValue'">{@link uk.nhs.interoperability.payloads.CodedValue CodedValue}</xsl:when>
  		<xsl:when test=". = 'CompositionalStatement'">{@link uk.nhs.interoperability.payloads.CompositionalStatement CompositionalStatement}</xsl:when>
  		<xsl:when test=". = 'HL7Date'">{@link uk.nhs.interoperability.payloads.HL7Date HL7Date}</xsl:when>
  		<xsl:when test=". = 'XML'">String</xsl:when>
  		<xsl:when test=". = 'FHIRCodedValue'">{@link uk.nhs.interoperability.payloads.FHIRCodedValue FHIRCodedValue}</xsl:when>
  		<xsl:otherwise>{@link <xsl:value-of select="."/><xsl:text> </xsl:text><xsl:value-of select="."/>}</xsl:otherwise>
  	</xsl:choose>
</xsl:template>


<!-- Getters and Setters -->
<xsl:template match="field" mode="gettersAndSetters">

	<xsl:if test="not(fixed)">
	<xsl:choose>
	<xsl:when test="maxOccurs &gt; 1">
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * @return List of <xsl:apply-templates select="type" mode="withPackage"/> objects
		 */
		public List&lt;<xsl:apply-templates select="type" mode="withPackage"/>&gt; get<xsl:value-of select="name"/>() {
			return (List&lt;<xsl:apply-templates select="type" mode="withPackage"/>&gt;)getValue("<xsl:value-of select="name"/>");
		}
		
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * @param <xsl:value-of select="name"/> value to set
		 * @return this To allow the use of the builder pattern
		 */
		public <xsl:value-of select="../@name"/> set<xsl:value-of select="name"/>(List<xsl:text> </xsl:text><xsl:value-of select="name"/>) {
			setValue("<xsl:value-of select="name"/>", <xsl:value-of select="name"/>);
			return this;
		}
		
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * &lt;br&gt;Note: This adds a <xsl:apply-templates select="type" mode="withPackage"/> object, but this method can be called
		 * multiple times to add additional <xsl:apply-templates select="type" mode="withPackage"/> objects.
		 * @param <xsl:value-of select="name"/> value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public <xsl:value-of select="../@name"/> add<xsl:value-of select="name"/>(<xsl:apply-templates select="type" mode="withPackage"/><xsl:text> </xsl:text><xsl:value-of select="name"/>) {
			addMultivalue("<xsl:value-of select="name"/>", <xsl:value-of select="name"/>);
			return this;
		}
		
		<xsl:if test="(type = 'CodedValue') or (type = 'FHIRCodedValue')">
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * &lt;br&gt;Note: This adds a coded entry from a specific vocabulary, but this method can be called
		 * multiple times to add additional coded entries objects.
		 * @param <xsl:value-of select="name"/> value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public <xsl:value-of select="../@name"/> add<xsl:value-of select="name"/>(VocabularyEntry <xsl:value-of select="name"/>) {
			Code c = new CodedValue(<xsl:value-of select="name"/>);
			addMultivalue("<xsl:value-of select="name"/>", c);
			return this;
		}
		</xsl:if>
		
		<xsl:if test="(type = 'CompositionalStatement')">
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * &lt;br&gt;Note: This adds a SNOMED CT compositional statement , but this method can be called
		 * multiple times to add additional compositional statement objects.
		 * @param <xsl:value-of select="name"/> value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public <xsl:value-of select="../@name"/> add<xsl:value-of select="name"/>(CompositionalStatement <xsl:value-of select="name"/>) {
			addMultivalue("<xsl:value-of select="name"/>", <xsl:value-of select="name"/>);
			return this;
		}
		</xsl:if>
	</xsl:when>
	<xsl:otherwise>
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * @return <xsl:apply-templates select="type" mode="withPackage"/> object
		 */	
		public <xsl:apply-templates select="type" mode="withPackage"/> get<xsl:value-of select="name"/>() {
			return (<xsl:apply-templates select="type" mode="withPackage"/>)getValue("<xsl:value-of select="name"/>");
		}
		
		<!-- If this is a CodedEntry with one vocab associated with it, add another getter to get the relevant enum entry -->
		<xsl:if test="(type = 'CodedValue') and (count(vocabulary) = 1)">
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * @return <xsl:value-of select="vocabulary" mode="withPackage"/> enumeration entry
		 */	
		public <xsl:apply-templates select="vocabulary" mode="withPackage"/> get<xsl:value-of select="name"/>Enum() {
			CodedValue cv = (CodedValue)getValue("<xsl:value-of select="name"/>");
			VocabularyEntry entry = VocabularyFactory.getVocab("<xsl:apply-templates select="vocabulary" mode="nameOnly"/>", null, cv.getCode());
			return (<xsl:apply-templates select="vocabulary" mode="withPackage"/>)entry;
		}
		</xsl:if>
		
		<!-- If this is a String with one vocab associated with it, add another getter to get the relevant enum entry -->
		<xsl:if test="(type = 'String') and (count(vocabulary) = 1)">
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * @return <xsl:value-of select="vocabulary" mode="withPackage"/> enumeration entry
		 */	
		public <xsl:apply-templates select="vocabulary" mode="withPackage"/> get<xsl:value-of select="name"/>Enum() {
			return <xsl:apply-templates select="vocabulary" mode="withPackage"/>.getByCode((String)getValue("<xsl:value-of select="name"/>"));
		}
		</xsl:if>
		
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * @param <xsl:value-of select="name"/> value to set
		 * @return this To allow the use of the builder pattern
		 */
		public <xsl:value-of select="../@name"/> set<xsl:value-of select="name"/>(<xsl:apply-templates select="type" mode="withPackage"/><xsl:text> </xsl:text><xsl:value-of select="name"/>) {
			setValue("<xsl:value-of select="name"/>", <xsl:value-of select="name"/>);
			return this;
		}
		
		<xsl:if test="(type = 'CodedValue') or (type = 'FHIRCodedValue')">
		/**
		 * <xsl:value-of select="description"/><xsl:apply-templates select="vocabulary"/><xsl:apply-templates select="mandatory"/>
		 * &lt;br&gt;Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param <xsl:value-of select="name"/> value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public <xsl:value-of select="../@name"/> set<xsl:value-of select="name"/>(VocabularyEntry <xsl:value-of select="name"/>) {
			Code c = new CodedValue(<xsl:value-of select="name"/>);
			setValue("<xsl:value-of select="name"/>", c);
			return this;
		}
		</xsl:if>
		
		<xsl:if test="xpath='.'">
		/**
		 * For internal use within the parser
		 * @param val value to set
		 */
		public void setTextValue(String val) {
			setValue("<xsl:value-of select="name"/>", val);
		}
		</xsl:if>
		
	</xsl:otherwise>
	</xsl:choose>
	</xsl:if>
</xsl:template>

<!-- XPath Lookup -->
<xsl:template match="field" mode="xpathLookups">
			addXpathLookup("<xsl:value-of select="xpath"/>", "<xsl:value-of select="name"/>");</xsl:template>

<!-- Field definitions -->
<xsl:template match="field" mode="definitions">
	<xsl:choose>	
	<xsl:when test="fixed">
		put("<xsl:value-of select="name"/>", new Field(
												"<xsl:value-of select="name"/>",
												"<xsl:value-of select="xpath"/>",
												"<xsl:value-of select="fixed"/>",
												"<xsl:value-of select="ifExists"/>",
												"<xsl:value-of select="ifNotExists"/>",
												"<xsl:value-of select="ifValue/@field"/>",
												"<xsl:value-of select="ifValue"/>",
												"<xsl:value-of select="deriveValueFromTemplateNameUsedInField"/>",
												"<xsl:value-of select="addAtStart"/>"
												));
	</xsl:when>
	<xsl:otherwise>
		put("<xsl:value-of select="name"/>", new Field(
												"<xsl:value-of select="name"/>",
												"<xsl:value-of select="xpath"/>",
												"<xsl:value-of select="description"/>",
												"<xsl:value-of select="mandatory"/>",
												"<xsl:value-of select="addAtStart"/>",
												"<xsl:value-of select="vocabulary"/>",
												"<xsl:value-of select="type" mode="withPackage"/>",
												"<xsl:value-of select="type/@package"/>",
												"<xsl:value-of select="maxOccurs"/>",
												"<xsl:value-of select="localCodeOID"/>",
												"<xsl:value-of select="deriveOIDFrom"/>",
												"<xsl:value-of select="versionedIdentifierXPath"/>",
												"<xsl:value-of select="versionedIdentifierField"/>",
												"<xsl:value-of select="suppressCodeSystem"/>",
												"<xsl:value-of select="suppressDisplayName"/>",
												"<xsl:value-of select="expectedExpressionCode"/>",
												"<xsl:value-of select="expectedExpressionDisplayName"/>"
												));
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="MakeVersionedName">
<xsl:value-of select="@version"/><xsl:if test="@version and @shortName">#</xsl:if><xsl:value-of select="@shortName"/>
</xsl:template>

</xsl:stylesheet>
