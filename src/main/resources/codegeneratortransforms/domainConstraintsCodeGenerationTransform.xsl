<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
<xsl:output method="text" encoding="UTF-8"/>
<xsl:param name="package"></xsl:param>

<xsl:template match="/">
	<xsl:apply-templates select="/fieldList/class/interfaces/interface"/>
</xsl:template>

<xsl:template match="/fieldList/class/interfaces/interface">@@@@<xsl:value-of select="."/>.java@@@@#@@@@#@@@@#@@@@/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package <xsl:value-of select="$package"/>;

/**
 * This is an interface to represent a constrained set of templates that can be used to populate a field in the message
 * @author Adam Hatherly
 */
public interface <xsl:value-of select="."/> {
}
</xsl:template>

</xsl:stylesheet>
