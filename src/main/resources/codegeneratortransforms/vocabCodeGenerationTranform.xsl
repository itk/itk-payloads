<?xml version="1.0" encoding="UTF-8"?>
<!--
XSLT to transform simple format for Notifications to the full message.

Support Notes:
- Currently only supports Patient NHS Numbers (not unverified).
- Currently only supports unstructured addresses.
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="text" encoding="UTF-8"/>
	
	<xsl:template match="/vocabulary">
<xsl:call-template name="VocabName"/>@@@@<xsl:value-of select="@id"/>@@@@/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the <xsl:call-template name="VocabName"/> vocabulary:
 * &lt;ul&gt;
<xsl:for-each select="concept">
	<xsl:variable name="DisplayNameWithoutSpecialCharacters" select="translate(
			(displayName[@type='PT'] | displayName[not(@type)])[1],
			translate(
			  (displayName[@type='PT'] | displayName[not(@type)])[1], 
			  ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-;,',
			  ''
			),
			''
		  )"/> *   &lt;li&gt;_<xsl:call-template name="CodeWithoutSpecialCharacters"/> : <xsl:value-of select="$DisplayNameWithoutSpecialCharacters"/>&lt;/li&gt;
</xsl:for-each>
 * &lt;/ul&gt;
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from &lt;a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases"&gt;TRUD&lt;/a&gt;.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum <xsl:call-template name="VocabName"/> implements VocabularyEntry {
	<!-- Output all entries with the code as the field name -->
	<xsl:for-each select="concept">
	/**
	 * <xsl:call-template name="DisplayNameWithoutSpecialCharacters"/>
	 */
	_<xsl:call-template name="CodeWithoutSpecialCharacters"/> ("<xsl:call-template name="CodeWithoutHyphens"/>", "<xsl:call-template name="DisplayName"/>"),
	</xsl:for-each>
	
	<!-- Now do them all again with the displayName as the field name -->
	<xsl:for-each select="concept">
	<xsl:variable name="DisplayNameWithoutSpaces" select="translate(
								(displayName[@type='PT'] | displayName[not(@type)])[1],
								translate(
								  (displayName[@type='PT'] | displayName[not(@type)])[1], 
								  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
								  ''
								),
								''
							  )"/>
	<xsl:variable name="PreviousDisplayNameWithoutSpaces" select="translate(
								(preceding::concept/displayName[@type='PT'] | preceding::concept/displayName[not(@type)])[1],
								translate(
								  (preceding::concept/displayName[@type='PT'] | preceding::concept/displayName[not(@type)])[1], 
								  'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
								  ''
								),
								''
							  )"/>
	<!-- Exclude if the code and display name are the same -->
	<xsl:if test="not($DisplayNameWithoutSpaces = @code)">
	<!-- Exclude if the display name is a duplicate of the previous displayName
	     (workaround for some Spine vocabs that have the same displayName for every entry) -->
	<xsl:choose><xsl:when test="$DisplayNameWithoutSpaces=$PreviousDisplayNameWithoutSpaces">
	// Duplicate display name omitted: <xsl:call-template name="DisplayName"/>
	</xsl:when><xsl:otherwise>
	/**
	 * <xsl:call-template name="DisplayNameWithoutSpecialCharacters"/>
	 */
	_<xsl:value-of select="$DisplayNameWithoutSpaces"/> ("<xsl:value-of select="@code"/>", "<xsl:call-template name="DisplayName"/>"),
	</xsl:otherwise></xsl:choose>
	</xsl:if>
	</xsl:for-each>
	;
	
	public final String code;
	public final String displayName;
	public final String oid="<xsl:value-of select="@id"/>";

	<xsl:call-template name="VocabName"/>(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static <xsl:call-template name="VocabName"/> getByCode(String code) {
		<xsl:call-template name="VocabName"/>[] vals = values();
		for (<xsl:call-template name="VocabName"/> val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(<xsl:call-template name="VocabName"/> other) {
		return (other.getCode().equals(code) &amp;&amp; other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	</xsl:template>
	
	<xsl:template name="VocabName">
	  <xsl:value-of select="translate(@name, ' -.:', '')"/>
	</xsl:template>
	
	<xsl:template name="CodeWithoutHyphens">
	  <xsl:value-of select="translate(@code, ' -', '')"/>
	</xsl:template>
	
	<xsl:template name="CodeWithoutSpecialCharacters">
	  <xsl:value-of select="translate(@code, ' -.:', '')"/>
	</xsl:template>
	
	<xsl:template name="DisplayName">
	  <xsl:value-of select="normalize-space((displayName[@type='PT'] | displayName[not(@type)])[1])"/>
	</xsl:template>
	
	<xsl:template name="DisplayNameWithoutSpecialCharacters">
	  <xsl:value-of select="translate(
					(displayName[@type='PT'] | displayName[not(@type)])[1],
					translate(
					  (displayName[@type='PT'] | displayName[not(@type)])[1], 
					  ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789;-,',
					  ''
					),
					''
				  )"/>
	</xsl:template>
</xsl:stylesheet>