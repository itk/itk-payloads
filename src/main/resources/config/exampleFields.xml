<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<fieldList>
	<class name="Example">
		<namespaces>
			<namespace prefix="x" default="true">urn:hl7-org:v3</namespace>
			<namespace prefix="xsi">http://www.w3.org/2001/XMLSchema-instance</namespace>
		</namespaces>
		<field>
			<name>Text</name>
			<xpath>x:text</xpath>
			<description>An example of a simple text field</description>
			<mandatory>true</mandatory>
			<type>String</type>
		</field>
		<field>
			<name>Attribute</name>
			<xpath>x:element/@attribute</xpath>
			<description>An example of a simple text attribute</description>
			<mandatory>true</mandatory>
			<type>String</type>
		</field>
		<field>
			<name>CodedValue</name>
			<description>This is an test of a coded value</description>
			<xpath>x:code</xpath>
			<type>CodedValue</type>
			<vocabulary>JobRoleName</vocabulary>
			<localCodeOID>2.16.840.1.113883.2.1.3.2.4.17.339</localCodeOID>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>Time</name>
			<xpath>x:time</xpath>
			<description>An example of a sime Date/Time field</description>
			<mandatory>true</mandatory>
			<type>HL7Date</type>
		</field>
		<field>
			<name>ListItem</name>
			<xpath>x:list/x:item</xpath>
			<description>An example of list of items</description>
			<mandatory>true</mandatory>
			<type>String</type>
			<maxOccurs>100</maxOccurs>
		</field>
		<field>
			<name>Fixed1</name>
			<xpath>x:fixed</xpath>
			<fixed>Example of a fixed value</fixed>
		</field>
		<include file="exampleFieldInclude.xml" search1="#ELEMENTNAME#" replace1="incl" />-->
		<field>
			<name>ID</name>
			<xpath>x:id</xpath>
			<description>Test of included IDs</description>
			<mandatory>true</mandatory>
			<type>PatientID</type>
			<maxOccurs>100</maxOccurs>
		</field>
		<field>
			<name>Child</name>
			<xpath>x:subclass</xpath>
			<description>An example of a subclass</description>
			<mandatory>true</mandatory>
			<type>Child</type>
		</field>
		<!-- Templated fields -->
		<field>
			<name>TemplatedField</name>
			<xpath>{childRootNodeName}</xpath>
			<versionedIdentifierXPath>(x:typeone/x:templateId/@extension|x:typetwo/x:templateId/@extension)[1]</versionedIdentifierXPath>
			<description>An example of a templated field which could have different element named depending on which implementation is used</description>
			<mandatory>true</mandatory>
			<type>Template</type>
		</field>
		<!-- Fixed fields with xpath conditions -->
		<field>
			<name>Fixed2</name>
			<xpath>x:title/x:subtitle[x:paragraph/@name='Introduction']/x:paragraph/@name</xpath>
			<fixed>Introduction</fixed>
		</field>
		<field>
			<name>LongXPathItem</name>
			<xpath>x:title/x:subtitle[x:paragraph/@name='Introduction']/x:paragraph/x:sentance</xpath>
			<description>An example of an XPath nesting the field several layers deep in the XML</description>
			<mandatory>true</mandatory>
			<type>String</type>
		</field>
		<field>
			<name>Fixed3</name>
			<xpath>x:title/x:subtitle[x:paragraph/@name='Introduction']/x:paragraph/@number</xpath>
			<fixed>1</fixed>
		</field>
		<field>
			<name>Fixed4</name>
			<xpath>x:title/x:subtitle[x:paragraph/@name='Background']/x:paragraph/@name</xpath>
			<fixed>Background</fixed>
		</field>
		<field>
			<name>Fixed5</name>
			<xpath>x:title/x:subtitle[x:paragraph/@name='Background']/x:paragraph/@number</xpath>
			<fixed>2</fixed>
		</field>
	</class>
	<class name="Child">
		<namespaces>
			<namespace prefix="npfitlc">NPFIT:HL7:Localisation</namespace>
		</namespaces>
		<field>
			<name>Text</name>
			<xpath>x:text</xpath>
			<description>An example of a simple text field</description>
			<mandatory>true</mandatory>
			<type>String</type>
		</field>
		<field>
			<name>FieldWithNamespace</name>
			<xpath>npfitlc:text</xpath>
			<description>An example of a simple text field with an XML namespace</description>
			<mandatory>true</mandatory>
			<type>String</type>
		</field>
	</class>
	<class name="Template1" shortName="Template1" version="Template1" rootNode="x:typeone">
		<interfaces>
			<interface>Template</interface>
		</interfaces>
		<field>
			<name>TemplateID</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>Template1#Template1</fixed>
		</field>
		<field>
			<name>Value</name>
			<xpath>x:value</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
	</class>
	<class name="Template2" shortName="Template2" version="Template2" rootNode="x:typetwo">
		<interfaces>
			<interface>Template</interface>
		</interfaces>
		<field>
			<name>TemplateID</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>Template2#Template2</fixed>
		</field>
		<field>
			<name>Value</name>
			<xpath>x:value</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
	</class>
	<class name="Template3" shortName="Template3" version="Template3">
		<interfaces>
			<interface>SimpleTemplate</interface>
		</interfaces>
		<field>
			<name>TemplateID</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>Template3#Template3</fixed>
		</field>
		<field>
			<name>Value</name>
			<xpath>x:value</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
	</class>
	<class name="Template4" shortName="Template4" version="Template4">
		<interfaces>
			<interface>SimpleTemplate</interface>
		</interfaces>
		<field>
			<name>TemplateID</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>Template4#Template4</fixed>
		</field>
		<field>
			<name>Value</name>
			<xpath>x:value</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
	</class>
	<include file="DataTypes/IDFlavours.xml"/>
</fieldList>
