<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
		<!-- Values to replace when including this file:
			#XPATH2#  - First part of path
			#FIELDPREFIX2# - Prefix for field name
			#TEMPLATEID# - Template ID
			#PARTOFCLASSNAME# - Class name to use for the "part of" relationship
		-->
		<!-- Note: The only difference between this version and the previous
			version of this template, it that this version allows multiple
			person names for the GuardianPerson -->
		<!-- Organization -->
		<field>
			<name>#FIELDPREFIX2#OrganisationFieldsClassCode</name>
			<xpath>#XPATH2#@classCode</xpath>
			<fixed>ORG</fixed>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationFieldsDeterminerCode</name>
			<xpath>#XPATH2#@determinerCode</xpath>
			<fixed>INSTANCE</fixed>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationFieldsTemplateIdRoot</name>
			<xpath>#XPATH2#x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationFieldsTemplateId</name>
			<xpath>#XPATH2#x:templateId/@extension</xpath>
			<fixed>#TEMPLATEID##providerOrganization</fixed>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationId</name>
			<description>An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient</description>
			<xpath>#XPATH2#x:id</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">OrgID</type>
			<mandatory>true</mandatory>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationName</name>
			<description>The description of the organisation associated with the ODS code</description>
			<xpath>#XPATH2#x:name</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationTelephone</name>
			<xpath>#XPATH2#x:telecom</xpath>
			<description>Telephone Number of the organisation</description>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Telecom</type>
			<mandatory>false</mandatory>
			<maxOccurs>4</maxOccurs>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationAddress</name>
			<description>A contact address for the organisation</description>
			<xpath>#XPATH2#x:addr</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Address</type>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationType</name>
			<description>A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider</description>
			<xpath>#XPATH2#x:standardIndustryClassCode</xpath>
			<type>CodedValue</type>
			<vocabulary>CDAOrganizationProviderType</vocabulary>
			<mandatory>true</mandatory>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX2#OrganisationPartOf</name>
			<xpath>#XPATH2#x:asOrganizationPartOf</xpath>
			<description>Information about the organisation the provider organisation is part of</description>
			<type>#PARTOFCLASSNAME#</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
			<ifExists>#FIELDPREFIX2#OrganisationName</ifExists>
		</field>
