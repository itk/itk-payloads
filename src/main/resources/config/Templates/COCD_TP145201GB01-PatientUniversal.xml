<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
	<class name="PatientUniversal" shortName="PatientRole" version="COCD_TP145201GB01" rootNode="x:patientRole">
		<field>
			<name>ClassCode</name>
			<xpath>@classCode</xpath>
			<fixed>PAT</fixed>
		</field>
		<field>
			<name>TemplateIdRoot</name>
			<xpath>x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>TemplateId</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>COCD_TP145201GB01#PatientRole</fixed>
		</field>
		<field>
			<name>PatientID</name>
			<description>Identifier for the patient (NHS number or a local ID)</description>
			<xpath>x:id</xpath>
			<mandatory>true</mandatory>
			<type package="uk.nhs.interoperability.payloads.commontypes.">PatientID</type>
			<maxOccurs>5</maxOccurs>
		</field>
		<field>
			<name>Address</name>
			<xpath>x:addr</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Address</type>
			<mandatory>false</mandatory>
			<maxOccurs>4</maxOccurs>
		</field>
		<field>
			<name>TelephoneNumber</name>
			<xpath>x:telecom</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Telecom</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<!-- Patient -->
		<field>
			<name>PatientClassCode</name>
			<xpath>x:patient/@classCode</xpath>
			<fixed>PSN</fixed>
		</field>
		<field>
			<name>PatientDeterminerCode</name>
			<xpath>x:patient/@determinerCode</xpath>
			<fixed>INSTANCE</fixed>
		</field>
		<field>
			<name>PatientTemplateIdRoot</name>
			<xpath>x:patient/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>PatientTemplateId</name>
			<xpath>x:patient/x:templateId/@extension</xpath>
			<fixed>COCD_TP145201GB01#patientPatient</fixed>
		</field>
		<field>
			<name>PatientName</name>
			<xpath>x:patient/x:name</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">PersonName</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<field>
			<name>Sex</name>
			<xpath>x:patient/x:administrativeGenderCode</xpath>
			<type>CodedValue</type>
			<vocabulary>Sex</vocabulary>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>SexNullFlavour</name>
			<xpath>x:patient/x:administrativeGenderCode/@nullFlavor</xpath>
			<description>A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null</description>
			<mandatory>false</mandatory>
			<type>String</type>
			<vocabulary type="internal">NullFlavour</vocabulary>
		</field>
		<field>
			<name>BirthTime</name>
			<xpath>x:patient/x:birthTime/@value</xpath>
			<type>HL7Date</type>
			<mandatory>false</mandatory>
			<format>yyyyMMddHHmm</format>
		</field>
		<field>
			<name>BirthTimeNullFlavour</name>
			<xpath>x:patient/x:birthTime/@nullFlavor</xpath>
			<description>A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null</description>
			<mandatory>false</mandatory>
			<type>String</type>
			<vocabulary type="internal">NullFlavour</vocabulary>
		</field>
		<field>
			<name>Languages</name>
			<description>Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language</description>
			<xpath>x:patient/x:languageCommunication</xpath>
			<type>LanguageCommunication</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<!-- Organisation: This is actually the registered GP -->
		<field>
			<name>OrganisationClassCode</name>
			<xpath>x:providerOrganization/@classCode</xpath>
			<fixed>ORG</fixed>
			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
		<field>
			<name>OrganisationDeterminerCode</name>
			<xpath>x:providerOrganization/@determinerCode</xpath>
			<fixed>INSTANCE</fixed>
			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
		<field>
			<name>OrganisationTemplateIdRoot</name>
			<xpath>x:providerOrganization/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
		<field>
			<name>OrganisationTemplateId</name>
			<xpath>x:providerOrganization/x:templateId/@extension</xpath>
			<fixed>COCD_TP145201GB01#providerOrganization</fixed>
			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
		<field>
			<name>RegisteredGPOrgId</name>
			<xpath>x:providerOrganization/x:id</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">OrgID</type>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>RegisteredGPOrgName</name>
			<xpath>x:providerOrganization/x:name</xpath>
			<type>String</type>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>RegisteredGPTelephone</name>
			<xpath>x:providerOrganization/x:telecom</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Telecom</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<field>
			<name>RegisteredGPAddress</name>
			<xpath>x:providerOrganization/x:addr</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Address</type>
			<mandatory>false</mandatory>
		</field>
		<!-- This is a coded value but is limited to a single code, so we will treat it as fixed -->
		<field>
			<name>CDAOrganizationProviderTypeCode</name>
			<xpath>x:providerOrganization/x:standardIndustryClassCode/@code</xpath>
  			<fixed>001</fixed>
  			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
		<field>
			<name>CDAOrganizationProviderTypeCodeSystem</name>
			<xpath>x:providerOrganization/x:standardIndustryClassCode/@codeSystem</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.17.289</fixed>
  			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
		<field>
			<name>CDAOrganizationProviderTypeDisplayName</name>
			<xpath>x:providerOrganization/x:standardIndustryClassCode/@displayName</xpath>
  			<fixed>GP Practice</fixed>
  			<ifExists>RegisteredGPOrgId</ifExists>
		</field>
	</class>
	<!-- LanguageCommunication -->
	<include file="Templates/LanguageCommunication.xml"
		search1="#TEMPLATEID#" replace1="COCD_TP145201GB01"
		search2="#NAMEPREFIX#" replace2=""/>
