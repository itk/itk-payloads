<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
		<!-- Values to replace when including this file:
			#XPATH# - First part of path
			#FIELDPREFIX# - Prefix for field name
		-->
		<field>
			<name>#FIELDPREFIX#ClassCode</name>
			<xpath>#XPATH#@classCode</xpath>
			<fixed>OBS</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#MoodCode</name>
			<xpath>#XPATH#@moodCode</xpath>
			<fixed>EVN</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#TemplateIdRoot</name>
			<xpath>#XPATH#x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#TemplateId</name>
			<xpath>#XPATH#x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#DNACPRbySRClinician</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#ID</name>
			<description>A DCE UUID to identify each instance of the DNACPR decision by the senior responsible clinician.</description>
			<xpath>#XPATH#x:id/@root</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>	
		<field>
			<name>#FIELDPREFIX#CodeValue</name>
			<xpath>#XPATH#x:code/@code</xpath>
			<fixed>EOLDCR</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#CodeSystem</name>
			<xpath>#XPATH#x:code/@codeSystem</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.17.435</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#CodeDisplayName</name>
			<xpath>#XPATH#x:code/@displayName</xpath>
			<fixed>EOL DNACPR by Senior Responsible Clinician</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#StatusCode</name>
			<xpath>#XPATH#x:statusCode/@code</xpath>
			<fixed>completed</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#EffectiveTime</name>
			<description>The time when DNACPR decision by the senior responsible clinician is recorded.</description>
			<xpath>#XPATH#x:effectiveTime/@value</xpath>
			<type>HL7Date</type>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRPreference</name>
			<xpath>#XPATH#x:value</xpath>
			<type>CodedValue</type>
			<vocabulary>DNACPRprefSnCT</vocabulary>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRPreferenceType</name>
			<xpath>#XPATH#x:value/@xsi:type</xpath>
			<fixed>CV.NPfIT.CDA.Url</fixed>
			<ifExists>DNACPRPreference</ifExists>
		</field>
		<!-- Author -->
		<include file="TemplateWrappers/Author_wrapper.xml"
				search1="#AUTHORXPATH#" replace1="x:author[x:functionCode/@code='OA']/"
				search2="#AUTHORFIELDPREFIX#" replace2="SeniorResponsibleClinician"
				search3="#AUTHORFUNCTIONCODE#" replace3="OA"
				search4="#TEMPLATEID#" replace4="COCD_TP146422GB01#author"
				search5="#FUNCTIONCODEDISPLAYNAME#" replace5="Originating Author"
				search6="#AUTHORINTERFACENAME#" replace6="Author"
				/>
		<!-- Author1 - Delegated Author -->
		<include file="TemplateWrappers/Author_wrapper.xml"
				search1="#AUTHORXPATH#" replace1="x:author[x:functionCode/@code='DA']/"
				search2="#AUTHORFIELDPREFIX#" replace2="Delegated"
				search3="#AUTHORFUNCTIONCODE#" replace3="DA"
				search4="#TEMPLATEID#" replace4="COCD_TP146422GB01#author1"
				search5="#FUNCTIONCODEDISPLAYNAME#" replace5="Delegated Author"
				search6="#AUTHORINTERFACENAME#" replace6="Author"
				/>
		<!-- EntryRelationship1 - DNACPR Docs Location -->
		<field>
			<name>#FIELDPREFIX#ER1TemplateId</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#entryRelationship1</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#ER1TemplateIdRoot</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#ER1TypeCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/@typeCode</xpath>
			<fixed>COMP</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#ER1ContextConductionInd</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/@contextConductionInd</xpath>
			<fixed>true</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#ER1SeperatableInd</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:seperatableInd/@value</xpath>
			<fixed>false</fixed>
		</field>
		<!-- DNACPRDocsLocation -->
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocClassCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/@classCode</xpath>
			<fixed>OBS</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocMoodCode </name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/@moodCode</xpath>
			<fixed>EVN</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocTemplateIdRoot</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocTemplateId</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#dNACPRDocsLocation</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocCodeValue</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:code/@code</xpath>
			<fixed>EOLDDL</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocCodeSystem</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:code/@codeSystem</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.17.435</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocCodeDisplayName</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:code/@displayName</xpath>
			<fixed>EOL DNACPR Docs Location</fixed>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocation</name>
			<description>Location of DNACPR documentation.</description>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:value</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>#FIELDPREFIX#DNACPRDocsLocationDataType</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:value/@xsi:type</xpath>
			<fixed>ST</fixed>
		</field>
		<!-- EntryRelationship2 - ReviewDateDNACPR -->
		<field>
			<name>#FIELDPREFIX#ER2TemplateId</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#entryRelationship2</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER2TemplateIdRoot</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER2TypeCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/@typeCode</xpath>
			<fixed>COMP</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER2ContextConductionInd</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/@contextConductionInd</xpath>
			<fixed>true</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER2SeperatableInd</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:seperatableInd/@value</xpath>
			<fixed>false</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<!-- ReviewDateDNACPR -->
		<field>
			<name>#FIELDPREFIX#ReviewDateTemplateIdRoot</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDateTemplateId</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#reviewDateDNACPR</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDateClassCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/@classCode</xpath>
			<fixed>OBS</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDateMoodCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/@moodCode</xpath>
			<fixed>EVN</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDateCodeValue</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:code/@code</xpath>
			<fixed>EOLDRD</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDateCodeSystem</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:code/@codeSystem</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.17.435</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDateDisplayName</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:code/@displayName</xpath>
			<fixed>EOL DNACPR Review Date</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ReviewDate</name>
			<!--<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:value</xpath>-->
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:value/@value</xpath>
			<type>HL7Date</type>
			<mandatory>false</mandatory>
		</field>
		<!-- Added to pass schema validation - to be verified by messaging team -->
		<field>
			<name>#FIELDPREFIX#ReviewDateDataType</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:value/@xsi:type</xpath>
			<fixed>TS</fixed>
			<ifExists>ReviewDate</ifExists>
		</field>
		<!-- EntryRelationship4 - ResuscitationStatus -->
		<field>
			<name>#FIELDPREFIX#ER4TemplateId</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#entryRelationship4</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER4TemplateIdRoot</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER4TypeCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/@typeCode</xpath>
			<fixed>COMP</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER4ContextConductionInd</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/@contextConductionInd</xpath>
			<fixed>true</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#ER4SeperatableInd</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:seperatableInd/@value</xpath>
			<fixed>false</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<!-- ResuscitationStatus -->
		<field>
			<name>#FIELDPREFIX#ResuscitationStatus</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:value</xpath>
			<type>CodedValue</type>
			<vocabulary>ResuscitationStatusSnCT</vocabulary>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusTemplateIdRoot</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusTemplateId</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:templateId/@extension</xpath>
			<fixed>COCD_TP146422GB01#resuscitationStatus</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusClassCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/@classCode</xpath>
			<fixed>OBS</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusMoodCode</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/@moodCode</xpath>
			<fixed>EVN</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusCodeValue</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:code/@code</xpath>
			<fixed>EOLRST</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusCodeSystem</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:code/@codeSystem</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.17.435</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
		<field>
			<name>#FIELDPREFIX#RStatusCodeDisplayName</name>
			<xpath>#XPATH#x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:code/@displayName</xpath>
			<fixed>EOL Resuscitation Status</fixed>
			<ifExists>ResuscitationStatus</ifExists>
		</field>
