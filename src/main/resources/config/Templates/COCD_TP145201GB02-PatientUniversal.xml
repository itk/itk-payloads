<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
	<class name="PatientUniversalv2" shortName="PatientRole" version="COCD_TP145201GB02" rootNode="x:patientRole">
		<interfaces>
			<interface>TOCPatient</interface>
		</interfaces>
		
		<!-- PatientRole -->
		<field>
			<name>ClassCode</name>
			<xpath>@classCode</xpath>
			<fixed>PAT</fixed>
		</field>
		<field>
			<name>TemplateIdRoot</name>
			<xpath>x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>TemplateId</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>COCD_TP145201GB02#PatientRole</fixed>
		</field>
		<field>
			<name>PatientID</name>
			<description>Identifier for the patient (NHS number or a local ID)</description>
			<xpath>x:id</xpath>
			<mandatory>true</mandatory>
			<type package="uk.nhs.interoperability.payloads.commontypes.">PatientIDWithTraceStatuses</type>
			<maxOccurs>5</maxOccurs>
		</field>
		<field>
			<name>Address</name>
			<xpath>x:addr</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Address</type>
			<mandatory>false</mandatory>
			<maxOccurs>4</maxOccurs>
		</field>
		<field>
			<name>TelephoneNumber</name>
			<xpath>x:telecom</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">Telecom</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<!-- Patient -->
		<field>
			<name>PatientClassCode</name>
			<xpath>x:patient/@classCode</xpath>
			<fixed>PSN</fixed>
		</field>
		<field>
			<name>PatientDeterminerCode</name>
			<xpath>x:patient/@determinerCode</xpath>
			<fixed>INSTANCE</fixed>
		</field>
		<field>
			<name>PatientTemplateIdRoot</name>
			<xpath>x:patient/x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>PatientTemplateId</name>
			<xpath>x:patient/x:templateId/@extension</xpath>
			<fixed>COCD_TP145201GB02#patientPatient</fixed>
		</field>
		<field>
			<name>PatientName</name>
			<xpath>x:patient/x:name</xpath>
			<type package="uk.nhs.interoperability.payloads.commontypes.">PersonName</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<field>
			<name>Sex</name>
			<xpath>x:patient/x:administrativeGenderCode</xpath>
			<type>CodedValue</type>
			<vocabulary>Sex</vocabulary>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>SexNullFlavour</name>
			<xpath>x:patient/x:administrativeGenderCode/@nullFlavor</xpath>
			<description>A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null</description>
			<mandatory>false</mandatory>
			<type>String</type>
			<vocabulary type="internal">NullFlavour</vocabulary>
		</field>
		<field>
			<name>BirthTime</name>
			<xpath>x:patient/x:birthTime/@value</xpath>
			<type>HL7Date</type>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>BirthTimeNullFlavour</name>
			<xpath>x:patient/x:birthTime/@nullFlavor</xpath>
			<description>A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null</description>
			<mandatory>false</mandatory>
			<type>String</type>
			<vocabulary type="internal">NullFlavour</vocabulary>
		</field>
		<field>
			<name>EthnicGroup</name>
			<xpath>x:patient/x:ethnicGroupCode</xpath>
			<description>The ethnic grouping the patient affiliates themselves to.</description>
			<mandatory>false</mandatory>
			<type>CodedValue</type>
			<vocabulary>Ethnicity</vocabulary>
		</field>
		<!-- Guardian -->
		<field>
			<name>Guardian</name>
			<xpath>x:patient/x:guardian</xpath>
			<description>The person or organisation who/which is or acts as the patient's guardian.</description>
			<type>PatientGuardian</type>
			<mandatory>false</mandatory>
		</field>
		<!-- LanguageCommunication -->
		<field>
			<name>Languages</name>
			<description>Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language</description>
			<xpath>x:patient/x:languageCommunication</xpath>
			<type>Patientv2LanguageCommunication</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<!-- Organization -->
		<include file="Templates/Patient_Organisation_fields.xml"
				search1="#XPATH2#" replace1="x:providerOrganization/"
				search2="#FIELDPREFIX2#" replace2=""
				search3="#TEMPLATEID#" replace3="COCD_TP145201GB02"
				search4="#PARTOFCLASSNAME#" replace4="PatientOrganisationPartOf"/>
	</class>
	<!-- LanguageCommunication -->
	<include file="Templates/LanguageCommunication.xml"
		search1="#TEMPLATEID#" replace1="COCD_TP145201GB02"
		search2="#NAMEPREFIX#" replace2="Patientv2"/>
	<!-- Guardian -->
	<include file="Templates/Guardian.xml"
		search1="#TEMPLATEID#" replace1="COCD_TP145201GB02"
		search2="#NAMEPREFIX#" replace2="Patient"/>
	<!-- Additional ORG class to allow parent-child chains -->
	<class name="PatientOrganisation">
		<include file="Templates/Patient_Organisation_fields.xml"
				search1="#XPATH2#" replace1=""
				search2="#FIELDPREFIX2#" replace2=""
				search3="#TEMPLATEID#" replace3="COCD_TP145201GB02"
				search4="#PARTOFCLASSNAME#" replace4="PatientOrganisationPartOf"/>
	</class>
	<class name="PatientOrganisationPartOf">
		<field>
			<name>ClassCode</name>
			<xpath>@classCode</xpath>
			<fixed>PART</fixed>
		</field>
		<field>
			<name>TemplateIdRoot</name>
			<xpath>x:templateId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.2</fixed>
		</field>
		<field>
			<name>TemplateId</name>
			<xpath>x:templateId/@extension</xpath>
			<fixed>COCD_TP145230GB02#playedOrganisationPartOf</fixed>
		</field>
		<field>
			<name>OrganisationId</name>
			<xpath>x:id</xpath>
			<description>An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient</description>
			<type package="uk.nhs.interoperability.payloads.commontypes.">OrgID</type>
			<mandatory>false</mandatory>
			<maxOccurs>10</maxOccurs>
		</field>
		
		
		<!-- MORE FIELDS TO ADD HERE! -->
		
		
		<!-- Allows for many levels of organisational hierarchy to be included -->
		<field>
			<name>WholeOrganization</name>
			<xpath>x:wholeOrganization</xpath>
			<description>A link to the organization that this organisation is part of</description>
			<type>PatientOrganisation</type>
			<mandatory>false</mandatory>
		</field>
	</class>
