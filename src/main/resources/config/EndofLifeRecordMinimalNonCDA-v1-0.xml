<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->
<fieldList>
	<class name="ClinicalDocument">
		<namespaces>
			<namespace prefix="x" default="true">urn:hl7-org:v3</namespace>
			<namespace prefix="">urn:hl7-org:v3</namespace>
			<namespace prefix="xsi">http://www.w3.org/2001/XMLSchema-instance</namespace>
			<namespace prefix="npfitlc">NPFIT:HL7:Localisation</namespace>
		</namespaces>
		<field>
			<name>DocumentId</name>
			<description>A DCE UUID to identify this specific document and version</description>
			<xpath>x:id/@root</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>DocumentTitle</name>
			<description>A string which is rendered as a human readable title</description>
			<xpath>x:title</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>EffectiveTime</name>
			<description>The creation time of the CDA document</description>
			<xpath>x:effectiveTime/@value</xpath>
			<type>HL7Date</type>
			<mandatory>true</mandatory>
    		<format>yyyyMMddHHmmZ</format>
		</field>
		<field>
			<name>ConfidentialityCode</name>
			<xpath>x:confidentialityCode</xpath>
			<type>CodedValue</type>
			<vocabulary>x_BasicConfidentialityKind</vocabulary>
			<localCodeOID>2.16.840.1.113883.2.1.3.2.4.17.415</localCodeOID>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>DocumentSetId</name>
			<description>A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)</description>
			<xpath>x:setId/@root</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		<field>
			<name>DocumentVersionNumber</name>
			<description>The version number of the document as an integer value</description>
			<xpath>x:versionNumber/@value</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		<!-- Record Target (Patient) -->
		<!--
		<field>
			<name>PatientTypeCode</name>
			<xpath>x:recordTarget/@typeCode</xpath>
			<fixed>RCT</fixed>
		</field>
		<field>
			<name>PatientContextControlCode</name>
			<xpath>x:recordTarget/@contextControlCode</xpath>
			<fixed>OP</fixed>
		</field>
		<field>
			<name>PatientContentId</name>
			<xpath>x:recordTarget/npfitlc:contentId/@extension</xpath>
  			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>Patient</deriveValueFromTemplateNameUsedInField>
		</field>
		<field>
			<name>PatientContentIdRoot</name>
			<xpath>x:recordTarget/npfitlc:contentId/@root</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
		</field>
		<field>
			<name>Patient</name>
			<xpath>x:recordTarget/x:patientRole</xpath>
			<description>Patient Details</description>
			<type>PatientUniversal</type>
			<mandatory>true</mandatory>
		</field>
		-->
		<!-- Author -->
		<!--
		<nclude file="TemplateWrappers/Author_wrapper_without_templateID.xml"
				search1="#AUTHORXPATH#" replace1="x:author/"
				search2="#AUTHORFIELDPREFIX#" replace2=""
				search3="#AUTHORFUNCTIONCODE#" replace3="OA"
				search4="#FUNCTIONCODEDISPLAYNAME#" replace4="Originating Author"
				search5="#AUTHORINTERFACENAME#" replace5="Author"
				/>
		-->
		<!-- Custodian (EPaCCS Hosting Owning Organisation) -->
		<!--
		<field>
			<name>CustodianTypeCode</name>
			<xpath>x:custodian/@typeCode</xpath>
			<fixed>CST</fixed>
		</field>
		<field>
			<name>CustodianContentId</name>
			<xpath>x:custodian/npfitlc:contentId/@extension</xpath>
  			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>CustodianOrganisation</deriveValueFromTemplateNameUsedInField>
		</field>
		<field>
			<name>CustodianContentIdRoot</name>
			<xpath>x:custodian/npfitlc:contentId/@root</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
		</field>
		<field>
			<name>CustodianOrganisation</name>
			<xpath>x:custodian/x:assignedCustodian</xpath>
			<description>The organisation responsible for maintaining the information in the CDA document (i.e. The EPaCCS system)</description>
			<type>CustodianOrganizationUniversal</type>
			<mandatory>true</mandatory>
		</field>
		-->
		<!-- Recipients -->
		<!--
		<field>
			<name>PrimaryRecipients</name>
			<xpath>x:informationRecipient</xpath>
			<description>Primary recipients</description>
			<type>PrimaryRecipient</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		<field>
			<name>InformationOnlyRecipients</name>
			<description>Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.</description>
			<xpath>x:tracker</xpath>
			<type>InformationOnlyRecipient</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		-->
		<!-- Authorization (Consent) -->
		<!--
		<field>
			<name>ConsentTypeCode</name>
			<xpath>x:authorization/@typeCode</xpath>
			<fixed>AUTH</fixed>
			<ifExists>AuthorizingConsent</ifExists>
		</field>
		<field>
			<name>ConsentContentId</name>
			<xpath>x:authorization/npfitlc:contentId/@extension</xpath>
			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>AuthorizingConsent</deriveValueFromTemplateNameUsedInField>
  			<ifExists>AuthorizingConsent</ifExists>
		</field>
		<field>
			<name>ConsentContentIdRoot</name>
			<xpath>x:authorization/npfitlc:contentId/@root</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
  			<ifExists>AuthorizingConsent</ifExists>
		</field>
		<field>
			<name>AuthorizingConsent</name>
			<xpath>x:authorization/x:authorizingConsent</xpath>
			<description>Details of consent/authorisation for the creation/sharing of the document</description>
			<type>Consent</type>
			<versionedIdentifierXPath>x:authorization/x:authorizingConsent/x:templateId/@extension</versionedIdentifierXPath>
			<mandatory>false</mandatory>
		</field>
		-->		
		<!-- Related Document: Parent Document - Previous document version -->
		<!--
		<field>
			<name>PreviousDocumentClassCode</name>
			<xpath>x:relatedDocument/x:priorParentDocument/@classCode</xpath>
			<fixed>DOCCLIN</fixed>
			<ifExists>PreviousDocumentVersionID</ifExists>
		</field>
		<field>
			<name>PreviousDocumentMoodCode</name>
			<xpath>x:relatedDocument/x:priorParentDocument/@moodCode</xpath>
			<fixed>EVN</fixed>
			<ifExists>PreviousDocumentVersionID</ifExists>
		</field>
		<field>
			<name>RDTypeCode</name>
			<xpath>x:relatedDocument/@typeCode</xpath>
			<fixed>RPLC</fixed>
			<ifExists>PreviousDocumentVersionID</ifExists>
		</field>
		-->
		<!-- The document type is a vocab with only one item at present, so we will treat it as a fixed value -->
		<!--
		<field>
			<name>PreviousDocumentEoLDocumentTypeValue</name>
			<xpath>x:relatedDocument/x:priorParentDocument/x:code/@code</xpath>
			<fixed>861421000000109</fixed>
			<ifExists>PreviousDocumentVersionID</ifExists>
		</field>
		<field>
			<name>PreviousDocumentEoLDocumentTypeCodeSystem</name>
			<xpath>x:relatedDocument/x:priorParentDocument/x:code/@codeSystem</xpath>
			<fixed>2.16.840.1.113883.2.1.3.2.4.15</fixed>
			<ifExists>PreviousDocumentVersionID</ifExists>
		</field>
		<field>
			<name>PreviousDocumentEoLDocumentTypeDisplayName</name>
			<xpath>x:relatedDocument/x:priorParentDocument/x:code/@displayName</xpath>
			<fixed>End of Life Care Coordination Summary (record artifact)</fixed>
			<ifExists>PreviousDocumentVersionID</ifExists>
		</field>
		<field>
			<name>PreviousDocumentVersionID</name>
			<description>A DCE UUID that identifies the specific document and version that this document replaces</description>
			<xpath>x:relatedDocument/x:priorParentDocument/x:id</xpath>
			<type>String</type>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>PreviousDocumentVersionSetId</name>
			<description>A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)</description>
			<xpath>x:relatedDocument/x:priorParentDocument/x:setId</xpath>
			<type>String</type>
			<mandatory>false</mandatory>
		</field>
		<field>
			<name>PreviousDocumentVersionNumber</name>
			<description>An integer value that identifies the version number of the document that this document replaces</description>
			<xpath>x:relatedDocument/x:priorParentDocument/x:versionNumber</xpath>
			<type>String</type>
			<mandatory>false</mandatory>
		</field>
		-->
		<!-- Component - Body sections -->
		<!--
		<field>
			<name>ComponentTypeCode</name>
			<xpath>x:component/@typeCode</xpath>
			<fixed>COMP</fixed>
		</field>
		<field>
			<name>ComponentContextConductionInd </name>
			<xpath>x:component/@contextConductionInd</xpath>
			<fixed>true</fixed>
		</field>
		-->
		<!-- Structured Body -->
		<!--
		<field>
			<name>BodyClassCode</name>
			<xpath>x:component/x:structuredBody/@classCode</xpath>
			<fixed>DOCBODY</fixed>
		</field>
		<field>
			<name>BodyMoodCode</name>
			<xpath>x:component/x:structuredBody/@moodCode</xpath>
			<fixed>EVN</fixed>
		</field>
		-->
		<!-- Component4 - BodyComponent -->
		<!--
		<field>
			<name>BodyComponentTypeCode</name>
			<xpath>x:component/x:structuredBody/x:component/@typeCode</xpath>
			<fixed>COMP</fixed>
		</field>
		<field>
			<name>BodyComponentContextConductionInd</name>
			<xpath>x:component/x:structuredBody/x:component/@contextConductionInd</xpath>
			<fixed>true</fixed>
		</field>
		-->
		<!-- ClassificationSection -->
		<!--
		<field>
			<name>CSClassCode</name>
			<xpath>x:component/x:structuredBody/x:component/x:section/@classCode</xpath>
			<fixed>DOCSECT</fixed>
		</field>
		<field>
			<name>CSMoodCode</name>
			<xpath>x:component/x:structuredBody/x:component/x:section/@moodCode</xpath>
			<fixed>EVN</fixed>
		</field>
		<field>
			<name>MainDocumentSectionID</name>
			<description>A DCE UUID to identify the main document section</description>
			<xpath>x:component/x:structuredBody/x:component/x:section/x:id/@root</xpath>
			<type>String</type>
			<mandatory>true</mandatory>
		</field>
		-->
		<!-- Component: Coded Sections -->
		<!--
		<field>
			<name>CodedSections</name>
			<xpath>x:component/x:structuredBody/x:component/x:section/x:entry</xpath>
			<description>A list of the coded sections making up the document</description>
			<type>CodedSections</type>
			<mandatory>false</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
		-->
		<!-- Entry: Text Sections -->
		<!--
		<field>
			<name>TextSections</name>
			<xpath>x:component/x:structuredBody/x:component/x:section/x:component</xpath>
			<description>A list of the text sections making up the document</description>
			<type>TextSections</type>
			<mandatory>true</mandatory>
			<maxOccurs>100</maxOccurs>
		</field>
	</class>
	<class name="PrimaryRecipient">
		<constructor>
			<param>Recipient</param>
		</constructor>
		<field>
			<name>TypeCode</name>
			<xpath>@typeCode</xpath>
			<fixed>PRCP</fixed>
		</field>
		<field>
			<name>ContentId</name>
			<xpath>npfitlc:contentId/@extension</xpath>
  			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>Recipient</deriveValueFromTemplateNameUsedInField>
		</field>
		<field>
			<name>ContentIdRoot</name>
			<xpath>npfitlc:contentId/@root</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
		</field>
		<field>
			<name>Recipient</name>
			<xpath>x:intendedRecipient</xpath>
			<description>Details of the recipient</description>
			<type>Recipient</type>
			<versionedIdentifierXPath>x:intendedRecipient/x:templateId/@extension</versionedIdentifierXPath>
			<mandatory>false</mandatory>
		</field>
	</class>
	<class name="InformationOnlyRecipient">
		<constructor>
			<param>Recipient</param>
		</constructor>
		<field>
			<name>TypeCode</name>
			<xpath>@typeCode</xpath>
			<fixed>TRC</fixed>
		</field>
		<field>
			<name>ContentId</name>
			<xpath>npfitlc:contentId/@extension</xpath>
  			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>Recipient</deriveValueFromTemplateNameUsedInField>
		</field>
		<field>
			<name>ContentIdRoot</name>
			<xpath>npfitlc:contentId/@root</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
		</field>
		<field>
			<name>Recipient</name>
			<xpath>x:intendedRecipient</xpath>
			<description>Details of the recipient</description>
			<type>Recipient</type>
			<versionedIdentifierXPath>x:intendedRecipient/x:templateId/@extension</versionedIdentifierXPath>
			<mandatory>false</mandatory>
		</field>
	</class>
	<class name="TextSections">
		<constructor>
			<param>TextSection</param>
		</constructor>
		<field>
			<name>TypeCode</name>
			<xpath>@typeCode</xpath>
			<fixed>COMP</fixed>
		</field>
		<field>
			<name>ContextConductionInd</name>
			<xpath>@contextConductionInd</xpath>
			<fixed>true</fixed>
		</field>
		<field>
			<name>ContentId</name>
			<xpath>npfitlc:contentId/@extension</xpath>
  			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>TextSection</deriveValueFromTemplateNameUsedInField>
		</field>
		<field>
			<name>ContentIdRoot</name>
			<xpath>npfitlc:contentId/@root</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
		</field>
		<field>
			<name>TextSection</name>
			<xpath>x:section</xpath>
			<description>The text section</description>
			<type>TextSection</type>
			<mandatory>true</mandatory>
		</field>
	</class>
	<class name="CodedSections">
		<constructor>
			<param>CodedEntry</param>
		</constructor>
		<field>
			<name>TypeCode</name>
			<xpath>@typeCode</xpath>
			<fixed>COMP</fixed>
		</field>
		<field>
			<name>ContextConductionInd</name>
			<xpath>@contextConductionInd</xpath>
			<fixed>true</fixed>
		</field>
		<field>
			<name>ContentId</name>
			<xpath>npfitlc:contentId/@extension</xpath>
  			<fixed>...</fixed>
			<deriveValueFromTemplateNameUsedInField>CodedEntry</deriveValueFromTemplateNameUsedInField>
			<ifExists>CodedEntry</ifExists>
		</field>
		<field>
			<name>ContentIdRoot</name>
			<xpath>npfitlc:contentId/@root</xpath>
  			<fixed>2.16.840.1.113883.2.1.3.2.4.18.16</fixed>
		</field>
		<field>
			<name>CodedEntry</name>
			<xpath>{childRootNodeName}</xpath>
			<description>The coded section</description>
			<type>CodedSection</type>
			<versionedIdentifierXPath>(x:observation/x:templateId/@extension|x:procedure/x:templateId/@extension|x:act/x:templateId/@extension)[1]</versionedIdentifierXPath>
			<mandatory>false</mandatory>
		</field>
		-->
	</class>
	<!-- Left-hand-side templates -->
	<!--
	<nclude file="Templates/COCD_TP145018UK03-CustodianOrganizationUniversal.xml"/>
	<nclude file="Templates/COCD_TP145200GB01-AuthorPersonUniversal.xml"/>
	<nclude file="Templates/COCD_TP145201GB01-PatientUniversal.xml"/>
	<nclude file="Templates/COCD_TP145202GB02-RecipientPersonUniversal.xml"/>
	<nclude file="Templates/COCD_TP145203GB03-RecipientOrganizationUniversal.xml"/>
	<nclude file="Templates/COCD_TP145204GB03-RecipientWorkgroupUniversal.xml"/>
	<nclude file="Templates/COCD_TP145207GB01-AuthorDeviceUniversal.xml"/>
	<nclude file="Templates/COCD_TP145208GB01-AuthorNonNamedPersonUniversal.xml"/>
	<nclude file="Templates/COCD_TP145210GB01-PersonWithOrganizationUniversal.xml"/>
	<nclude file="Templates/COCD_TP145212GB02-WorkgroupUniversal.xml"/>
	<nclude file="Templates/COCD_TP145226GB01-PatientRelationshipParticipantRole.xml"/>
	<nclude file="Templates/COCD_TP146226GB02-Consent.xml"/>
	<nclude file="Templates/COCD_TP146229GB01-TextSection.xml"/>
	<nclude file="Templates/COCD_TP146416GB01-PatientRelationships.xml"/>
	-->
	<!-- Coded section templates -->
	<!--
	<nclude file="Templates/COCD_TP146248GB01-ReferenceURL.xml"/>
	<nclude file="Templates/COCD_TP146414GB01-PrognosisAwareness.xml"/>
	<nclude file="Templates/COCD_TP146417GB01-AuthoritytoLastingPowerofAttorney.xml"/>
	<nclude file="Templates/COCD_TP146418GB01-AnticipatoryMedicineBoxIssueProcedure.xml"/>
	<nclude file="Templates/COCD_TP146419GB01-EoLCarePlan.xml"/>
	<nclude file="Templates/COCD_TP146420GB01-AdvanceDecisionToRefuseTreatment.xml"/>
	<nclude file="Templates/COCD_TP146422GB01-DNACPRDecisionbySeniorResponsibleClinician.xml"/>
	-->
	<!-- Basic types -->
	<!--
	<nclude file="DataTypes/Address.xml"/>
	<nclude file="DataTypes/IDFlavours.xml"/>
	<nclude file="DataTypes/PersonName.xml"/>
	<nclude file="DataTypes/Telecom.xml"/>
	-->
</fieldList>
