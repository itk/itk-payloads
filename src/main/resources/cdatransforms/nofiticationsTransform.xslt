<?xml version="1.0" encoding="UTF-8"?>
<!--
XSLT to transform simple format for Notifications to the full message.

Support Notes:
- Currently only supports Patient NHS Numbers (not unverified).
- Currently only supports unstructured addresses.
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<xsl:variable name="notificationId" select="notification/@id"/>
		<xsl:variable name="notificationTime" select="notification/@time"/>
		<xsl:variable name="authorId" select="notification/author/@id"/>
		<xsl:variable name="authorOID">
			<xsl:choose>
				<xsl:when test="notification/author/isLocal='true'">2.16.840.1.113883.2.1.3.2.4.18.36</xsl:when>
				<xsl:otherwise>1.2.826.0.1285.0.2.0.107</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="patientId" select="notification/patient/@id"/>
		<xsl:variable name="patientOID">2.16.840.1.113883.2.1.4.1</xsl:variable>
		<xsl:variable name="patientAddress" select="notification/patientAddress"/>
		<xsl:variable name="patientDOB" select="notification/patientDOB/@value"/>
		<xsl:variable name="patientGender" select="notification/patientGender/@value"/>
		<xsl:variable name="patientGenderDesc">
			<xsl:choose>
				<xsl:when test="$patientGender='0'">Not known</xsl:when>
				<xsl:when test="$patientGender='1'">Male</xsl:when>
				<xsl:when test="$patientGender='2'">Female</xsl:when>
				<xsl:when test="$patientGender='9'">Not specified</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="recipientAddress" select="notification/recipientAddress"/>
		<xsl:variable name="recipientLocalId" select="notification/recipient/@localId"/>
		<xsl:variable name="recipientSdsId" select="notification/recipient/@sdsId"/>
		<xsl:variable name="recipientRPC" select="notification/recipient/@rpc"/>
		<xsl:variable name="recipientCode" select="notification/recipient/@code"/>
		<xsl:variable name="recipientCodeDesc" select="notification/recipient/@desc"/>
		<xsl:variable name="recipientName" select="notification/recipientName/@value"/>
		<xsl:variable name="recipientOrgId" select="notification/recipientOrg/@id"/>
		<xsl:variable name="recipientOrgName" select="notification/recipientOrg/@desc"/>
		<xsl:variable name="performerAddress" select="notification/performerAddress"/>
		<xsl:variable name="performerLocalId" select="notification/performer/@localId"/>
		<xsl:variable name="performerSdsId" select="notification/performer/@sdsId"/>
		<xsl:variable name="performerRPC" select="notification/performer/@rpc"/>
		<xsl:variable name="performerCode" select="notification/performer/@code"/>
		<xsl:variable name="performerCodeDesc" select="notification/performer/@desc"/>
		<xsl:variable name="performerName" select="notification/performerName/@value"/>
		<xsl:variable name="performerOrgId" select="notification/performerOrg/@id"/>
		<xsl:variable name="performerOrgName" select="notification/performerOrg/@desc"/>
		<xsl:variable name="authorOrgId" select="notification/authorOrg/@id"/>
		<xsl:variable name="authorOrgDesc" select="notification/authorOrg/@desc"/>
		<xsl:variable name="authorOrgOID">2.16.840.1.113883.2.1.3.2.4.19.1</xsl:variable>
		<xsl:variable name="eventId" select="notification/event/@id"/>
		<xsl:variable name="eventCode" select="notification/event/@code"/>
		<xsl:variable name="eventDesc" select="notification/event/@desc"/>
		<xsl:variable name="eventTime" select="notification/event/@time"/>
		<xsl:variable name="eventOID">
			<xsl:choose>
				<xsl:when test="notification/event/isLocal='true'">2.16.840.1.113883.2.1.3.2.4.17.455</xsl:when>
				<xsl:otherwise>2.16.840.1.113883.2.1.3.2.4.17.437</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="documentCode" select="notification/documentDetails/@code"/>
		<xsl:variable name="documentOID">
			<xsl:choose>
				<xsl:when test="notification/document/isLocal='true'">2.16.840.1.113883.2.1.3.2.4.17.450</xsl:when>
				<xsl:otherwise>2.16.840.1.113883.2.1.3.2.4.17.438</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="documentDesc" select="notification/documentDetails/@desc"/>
		<xsl:variable name="documentId" select="notification/documentDetails/@id"/>
		<xsl:variable name="documentSetId" select="notification/documentDetails/@setId"/>
		<xsl:variable name="documentFormatCode" select="notification/documentFormat/@code"/>
		<xsl:variable name="profileId" select="notification/profileId/@id"/>
		<xsl:variable name="profileOID">
			<xsl:choose>
				<xsl:when test="notification/profileId/isLocal='true'">2.16.840.1.113883.2.1.3.2.4.18.44</xsl:when>
				<xsl:otherwise>2.16.840.1.113883.2.1.3.2.4.18.28</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="eventSubtypeCode" select="notification/eventSubtype/@code"/>
		<xsl:variable name="eventSubtypeDesc" select="notification/eventSubtype/@desc"/>
		<xsl:variable name="eventSubtypeOID">
			<xsl:choose>
				<xsl:when test="notification/eventSubtype/isLocal='true'">2.16.840.1.113883.2.1.3.2.4.17.456</xsl:when>
				<xsl:otherwise>2.16.840.1.113883.2.1.3.2.4.17.454</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<EventNotification classCode="INFRM" moodCode="EVN" xmlns:npfitlc="NPFIT:HL7:Localisation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:hl7-org:v3">
			<code code="01" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.436" displayName="Event Notification"/>
			<effectiveTime value="{$notificationTime}"/>
			<id root="{$notificationId}"/>
			<!--CREATE THE AUTHOR SECTION-->
			<author typeCode="AUT">
				<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145231GB01#OriginatingSystem"/>
				<COCD_TP145231GB01.OriginatingSystem classCode="ASSIGNED">
					<code code="OSY" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.205" displayName="Originating System"/>
					<id root="{$authorOID}" extension="{$authorId}"/>
					<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145231GB01#OriginatingSystem"/>
					<representedOrganization classCode="ORG" determinerCode="INSTANCE">
						<id root="{$authorOrgOID}" extension="{$authorOrgId}"/>
						<name>
							<xsl:value-of select="$authorOrgDesc"/>
						</name>
						<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145231GB01#representedOrganization"/>
					</representedOrganization>
				</COCD_TP145231GB01.OriginatingSystem>
			</author>
			<!--FINISH THE AUTHOR SECTION-->
			<component typeCode="COMP">
				<!--CREATE THE EVENT INFORMATION SECTION-->
				<eventInformation classCode="INFRM" moodCode="EVN">
					<code code="{$eventCode}" codeSystem="{$eventOID}" displayName="{$eventDesc}"/>
					<effectiveTime value="{$eventTime}"/>
					<id root="{$eventId}"/>
					<!--CREATE THE DOCUMENT INFORMATION SECTION-->
					<xsl:if test="$documentCode">
						<component typeCode="COMP">
							<documentDetails classCode="DOCCLIN" moodCode="EVN">
								<code code="{$documentCode}" codeSystem="{$documentOID}" displayName="{$documentDesc}"/>
								<xsl:if test="$documentId">
									<id root="2.16.840.1.113883.2.1.3.2.4.18.21" extension="{$documentId}"/>
								</xsl:if>
								<xsl:if test="$documentSetId">
									<setId root="2.16.840.1.113883.2.1.3.2.4.18.45" extension="{$documentSetId}"/>
								</xsl:if>
							</documentDetails>
						</component>
					</xsl:if>
					<!--END THE DOCUMENT INFORMATION SECTION-->
					<!--CREATE THE URL INFORMATION SECTION-->
					<xsl:if test="count(notification/url)>=1">
						<xsl:for-each select="notification/url">
							<xsl:variable name="urlType" select="@code"/>
							<xsl:variable name="urlDesc">
								<xsl:choose>
									<xsl:when test="$urlType='01'">URL for remote system access</xsl:when>
									<xsl:when test="$urlType='02'">URL for document access (latest version)</xsl:when>
									<xsl:when test="$urlType='03'">URL for document access (version at date notified)</xsl:when>
								</xsl:choose>
							</xsl:variable>
							<xsl:variable name="urlValue" select="@value"/>
							<component1 typeCode="COMP">
								<uRL classCode="OBS" moodCode="EVN">
									<code code="{$urlType}" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.439" displayName="{$urlDesc}"/>
									<value>
										<xsl:value-of select="$urlValue"/>
									</value>
								</uRL>
							</component1>
						</xsl:for-each>
					</xsl:if>
					<!--END THE URL INFORMATION SECTION-->
					<!--CREATE THE DOCUMENT FORMAT INFORMATION SECTION-->
					<xsl:if test="$documentFormatCode">
						<component2 typeCode="COMP">
							<documentFormat classCode="OBS" moodCode="EVN">
								<code code="01" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.440" displayName="Document Format"/>
								<value code="{$documentFormatCode}" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.441"/>
							</documentFormat>
						</component2>
					</xsl:if>
					<!--END THE DOCUMENT FORMAT INFORMATION SECTION-->
					<!--CREATE THE PROFILE INFORMATION SECTION-->
					<xsl:if test="$profileId">
						<component3 typeCode="COMP">
							<profileID classCode="OBS" moodCode="EVN">
								<code code="02" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.440" displayName="Profile ID"/>
								<value root="{$profileOID}" extension="{$profileId}"/>
							</profileID>
						</component3>
					</xsl:if>
					<!--END THE PROFILE INFORMATION SECTION-->
					<!--CREATE THE DOCUMENT SUBTYPE INFORMATION SECTION-->
					<xsl:if test="$eventSubtypeCode">
						<component4 typeCode="COMP">
							<eventSubtype classCode="OBS" moodCode="EVN">
								<code code="03" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.440" displayName="Document Subtype"/>
								<value code="{$eventSubtypeCode}" codeSystem="{$eventSubtypeOID}" displayName="{$eventSubtypeDesc}"/>
							</eventSubtype>
						</component4>
					</xsl:if>
					<!--END THE EVENT SUBTYPE INFORMATION SECTION-->
					<!--CREATE THE PERFORMER INFORMATION SECTION-->
					<performer typeCode="PRF">
						<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145217GB01#ContactPerson"/>
						<COCD_TP145217GB01.ContactPerson classCode="ASSIGNED">
							<addr>
								<xsl:value-of select="$performerAddress"/>
							</addr>
							<code code="{$performerCode}" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.124" displayName="{$performerCodeDesc}"/>
							<xsl:if test="$performerSdsId">
								<xsl:choose>
									<xsl:when test="$performerSdsId and not($performerRPC)">
										<id root="1.2.826.0.1285.0.2.0.65" extension="{$performerSdsId}"/>
										<id nullFlavor="NA"/>
									</xsl:when>
									<xsl:when test="$performerSdsId and $performerRPC">
										<id root="1.2.826.0.1285.0.2.0.65" extension="{$performerSdsId}"/>
										<id root="1.2.826.0.1285.0.2.0.67" extension="{$performerRPC}"/>
									</xsl:when>
								</xsl:choose>
							</xsl:if>
							<xsl:if test="$performerLocalId">
								<id root="2.16.840.1.113883.2.1.3.2.4.18.24" extension="{$performerLocalId}"/>
							</xsl:if>
							<xsl:for-each select="notification/performerTelecom">
								<xsl:variable name="performerTelecom" select="@value"/>
								<telecom value="{$performerTelecom}"/>
							</xsl:for-each>
							<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#ContactPerson"/>
							<assignedPerson classCode="PSN" determinerCode="INSTANCE">
								<name>
									<xsl:value-of select="$performerName"/>
								</name>
								<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#assignedPerson"/>
							</assignedPerson>
							<representedOrganization classCode="ORG" determinerCode="INSTANCE">
								<xsl:if test="$recipientOrgId">
									<id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="{$recipientOrgId}"/>
								</xsl:if>
								<name>
									<xsl:value-of select="$recipientOrgName"/>
								</name>
								<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145217GB01#representedOrganization"/>
							</representedOrganization>
						</COCD_TP145217GB01.ContactPerson>
					</performer>
					<!--END THE PERFORMER INFORMATION SECTION-->
				</eventInformation>
				<!--FINISH THE EVENT INFORMATION SECTION-->
			</component>
			<!--START THE INFORMATION RECIPIENT INFORMATION SECTION-->
			<informationRecipient typeCode="IRCP">
				<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145218GB01#IntendedRecipient"/>
				<COCD_TP145218GB01.IntendedRecipient classCode="ASSIGNED">
					<addr>
						<xsl:value-of select="$recipientAddress"/>
					</addr>
					<code code="{$recipientCode}" codeSystem="2.16.840.1.113883.2.1.3.2.4.17.124" displayName="{$recipientCodeDesc}"/>
					<xsl:if test="$recipientSdsId">
						<xsl:choose>
							<xsl:when test="$recipientSdsId and not($recipientRPC)">
								<id root="1.2.826.0.1285.0.2.0.65" extension="{$recipientSdsId}"/>
								<id nullFlavor="NA"/>
							</xsl:when>
							<xsl:when test="$recipientSdsId and $recipientRPC">
								<id root="1.2.826.0.1285.0.2.0.65" extension="{$recipientSdsId}"/>
								<id root="1.2.826.0.1285.0.2.0.67" extension="{$recipientRPC}"/>
							</xsl:when>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$recipientLocalId">
						<id root="2.16.840.1.113883.2.1.3.2.4.18.24" extension="{$recipientLocalId}"/>
					</xsl:if>
					<xsl:for-each select="notification/recipientTelecom">
						<xsl:variable name="recipientTelecom" select="@value"/>
						<telecom value="{$recipientTelecom}"/>
					</xsl:for-each>
					<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145218GB01#IntendedRecipient"/>
					<xsl:if test="$recipientName">
						<assignedPerson classCode="PSN" determinerCode="INSTANCE">
							<name>
								<xsl:value-of select="$recipientName"/>
							</name>
							<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145218GB01#assignedPerson"/>
						</assignedPerson>
					</xsl:if>
					<representedOrganization classCode="ORG" determinerCode="INSTANCE">
						<xsl:if test="$recipientOrgId">
							<id root="2.16.840.1.113883.2.1.3.2.4.19.1" extension="{$recipientOrgId}"/>
						</xsl:if>
						<name>
							<xsl:value-of select="$recipientOrgName"/>
						</name>
						<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145218GB01#representedOrganization"/>
					</representedOrganization>
				</COCD_TP145218GB01.IntendedRecipient>
			</informationRecipient>
			<!--END THE INFORMATION RECIPIENT INFORMATION SECTION-->
			<!--START THE RECORD TARGET INFORMATION SECTION-->
			<recordTarget typeCode="RCT">
				<npfitlc:contentId root="2.16.840.1.113883.2.1.3.2.4.18.16" extension="COCD_TP145216GB01#PatientRole"/>
				<COCD_TP145216GB01.PatientRole classCode="PAT">
					<addr>
						<xsl:value-of select="$patientAddress"/>
					</addr>
					<id root="{$patientOID}" extension="{$patientId}"/>
					<xsl:if test="count(notification/patientTelecom)>0">
						<xsl:for-each select="notification/patientTelecom">
							<xsl:variable name="patientTelecom" select="@value"/>
							<telecom value="{$patientTelecom}"/>
						</xsl:for-each>
					</xsl:if>
					<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145216GB01#PatientRole"/>
					<patientPatient classCode="PSN" determinerCode="INSTANCE">
						<xsl:if test="$patientGender">
							<administrativeGenderCode code="{$patientGender}" codeSystem="2.16.840.1.113883.2.1.3.2.4.16.25" displayName="{$patientGenderDesc}"/>
						</xsl:if>
						<birthTime value="{$patientDOB}"/>
						<xsl:if test="count(notification/patientName)>0">
							<xsl:for-each select="notification/patientName">
								<xsl:variable name="patientName" select="@value"/>
								<name>
									<xsl:value-of select="$patientName"/>
								</name>
							</xsl:for-each>
						</xsl:if>
						<templateId root="2.16.840.1.113883.2.1.3.2.4.18.2" extension="COCD_TP145216GB01#patientPatient"/>
					</patientPatient>
				</COCD_TP145216GB01.PatientRole>
			</recordTarget>
			<!--END THE RECORD TARGET INFORMATION SECTION-->
		</EventNotification>
	</xsl:template>
</xsl:stylesheet>
