<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:param name="package"></xsl:param>
  <xsl:param name="configFileKey"></xsl:param>

  <xsl:template match="/">
  	<html>
  	<head>
  	<link rel="stylesheet" type="text/css" media="all" href="css/core.css" />
  	<link rel="stylesheet" type="text/css" media="all" href="css/docs.css" />
  	</head>
  	<body>
  	<h1>Payload: <xsl:value-of select="$configFileKey"/></h1>
  	<h2>Java Package: <xsl:value-of select="$package"/></h2>
  	<xsl:apply-templates select="fieldList"/>
  	</body>
  	</html>
  </xsl:template>
  
  <xsl:template match="fieldList">
  	<xsl:apply-templates select="class"/>
  </xsl:template>
  
  <xsl:template match="class">
    <table>
    	<tr><th colspan="4" class="clsHdr"><xsl:value-of select="@name" /></th></tr>
    	<tr><td colspan="4"><xsl:value-of select="description" /></td></tr>
    	<tr><th class="fieldName">Field Name</th>
    		<th class="fieldType">Type</th>
    		<th class="fieldType">MaxOccurs</th>
    		<th class="fieldDesc">Description</th></tr>
    	<xsl:apply-templates select="field[not(fixed)]"/>
	</table>
  </xsl:template>
  
<xsl:template match="field[not(fixed)]">
		<tr><td><div class="fieldName"><xsl:value-of select="name" /></div></td>
			<td><div class="fieldType"><xsl:value-of select="type" /></div></td>
			<td><div class="fieldMaxOccurs"><xsl:value-of select="maxOccurs" /></div></td>
			<td><div class="fieldDesc"><xsl:value-of select="description" /></div></td></tr>
</xsl:template>

</xsl:stylesheet>
