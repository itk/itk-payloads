/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PatientRelationships object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>List&lt;{@link RelatedPersonDetails RelatedPersonDetails}&gt; RelatedPerson</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} RecordedTime</li>
 * <li>{@link Author Author} Author</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PatientRelationships extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "PatientRelationships";
		protected static final String shortName = "PatientRelationships";
		protected static final String rootNode = "";
		protected static final String version = "COCD_TP146416GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param RelatedPerson RelatedPerson
		 * @param RecordedTime RecordedTime
		 * @param Author Author
		 */
	    public PatientRelationships(String ID, 
		List<RelatedPersonDetails> RelatedPerson, HL7Date RecordedTime, Author Author) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setRelatedPerson(RelatedPerson);
			setRecordedTime(RecordedTime);
			setAuthor(Author);
		}
	
		/**
		 * A DCE UUID to identify each instance of a patient relationship
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of a patient relationship
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationships setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * The participant of the end of life care record of patient
		 * <br><br>This field is MANDATORY
		 * @return List of RelatedPersonDetails objects
		 */
		public List<RelatedPersonDetails> getRelatedPerson() {
			return (List<RelatedPersonDetails>)getValue("RelatedPerson");
		}
		
		/**
		 * The participant of the end of life care record of patient
		 * <br><br>This field is MANDATORY
		 * @param RelatedPerson value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationships setRelatedPerson(List RelatedPerson) {
			setValue("RelatedPerson", RelatedPerson);
			return this;
		}
		
		/**
		 * The participant of the end of life care record of patient
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a RelatedPersonDetails object, but this method can be called
		 * multiple times to add additional RelatedPersonDetails objects.
		 * @param RelatedPerson value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationships addRelatedPerson(RelatedPersonDetails RelatedPerson) {
			addMultivalue("RelatedPerson", RelatedPerson);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the patient relationship
		 * @return HL7Date object
		 */	
		public HL7Date getRecordedTime() {
			return (HL7Date)getValue("RecordedTime");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the patient relationship
		 * @param RecordedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationships setRecordedTime(HL7Date RecordedTime) {
			setValue("RecordedTime", RecordedTime);
			return this;
		}
		
		
		/**
		 * A template representing the author of these patient relationships
		 * @return Author object
		 */	
		public Author getAuthor() {
			return (Author)getValue("Author");
		}
		
		
		
		
		/**
		 * A template representing the author of these patient relationships
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationships setAuthor(Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PCPR",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146416GB01#PatientRelationships",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of a patient relationship",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeValue", new Field(
												"CodeValue",
												"x:code/@code",
												"EOLPR",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeSystem", new Field(
												"CodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeDisplayName", new Field(
												"CodeDisplayName",
												"x:code/@displayName",
												"EOL Patient Relationships",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelatedPerson", new Field(
												"RelatedPerson",
												"x:participant",
												"The participant of the end of life care record of patient",
												"true",
												"",
												"",
												"RelatedPersonDetails",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146416GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/x:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												""
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/x:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecordedTime", new Field(
												"RecordedTime",
												"x:author/x:time",
												"The time the author originally recorded the patient relationship",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"A template representing the author of these patient relationships",
												"false",
												"",
												"",
												"Author",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PatientRelationships() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PatientRelationships(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	