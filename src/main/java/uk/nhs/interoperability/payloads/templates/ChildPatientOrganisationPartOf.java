/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ChildPatientOrganisationPartOf object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID}&gt; OrganisationId</li>
 * <li>{@link ChildPatientOrganisation ChildPatientOrganisation} WholeOrganization</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ChildPatientOrganisationPartOf extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ChildPatientOrganisationPartOf";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param OrganisationId OrganisationId
		 * @param WholeOrganization WholeOrganization
		 */
	    public ChildPatientOrganisationPartOf(
		List<uk.nhs.interoperability.payloads.commontypes.OrgID> OrganisationId, ChildPatientOrganisation WholeOrganization) {
			fields = new LinkedHashMap<String, Object>();
			
			setOrganisationId(OrganisationId);
			setWholeOrganization(WholeOrganization);
		}
	
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * @return List of uk.nhs.interoperability.payloads.commontypes.OrgID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.OrgID> getOrganisationId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.OrgID>)getValue("OrganisationId");
		}
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * @param OrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisationPartOf setOrganisationId(List OrganisationId) {
			setValue("OrganisationId", OrganisationId);
			return this;
		}
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.OrgID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.OrgID objects.
		 * @param OrganisationId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisationPartOf addOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId) {
			addMultivalue("OrganisationId", OrganisationId);
			return this;
		}
		
		
		/**
		 * A link to the organization that this organisation is part of
		 * @return ChildPatientOrganisation object
		 */	
		public ChildPatientOrganisation getWholeOrganization() {
			return (ChildPatientOrganisation)getValue("WholeOrganization");
		}
		
		
		
		
		/**
		 * A link to the organization that this organisation is part of
		 * @param WholeOrganization value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisationPartOf setWholeOrganization(ChildPatientOrganisation WholeOrganization) {
			setValue("WholeOrganization", WholeOrganization);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PART",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145230GB02#playedOrganisationPartOf",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationId", new Field(
												"OrganisationId",
												"x:id",
												"An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"2",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WholeOrganization", new Field(
												"WholeOrganization",
												"x:wholeOrganization",
												"A link to the organization that this organisation is part of",
												"false",
												"",
												"",
												"ChildPatientOrganisation",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ChildPatientOrganisationPartOf() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ChildPatientOrganisationPartOf(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	
	
	