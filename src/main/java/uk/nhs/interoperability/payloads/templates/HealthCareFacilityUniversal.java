/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the HealthCareFacilityUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.CodedValue CodedValue}&gt; ServiceDeliveryLocationId</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CareSettingType</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * <li>String PlaceName</li>
 * <li>String PlaceNameNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} PlaceAddress</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class HealthCareFacilityUniversal extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "HealthCareFacilityUniversal";
		protected static final String shortName = "HealthCareFacility";
		protected static final String rootNode = "x:healthCareFacility";
		protected static final String version = "COCD_TP145211GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ServiceDeliveryLocationId ServiceDeliveryLocationId
		 * @param CareSettingType CareSettingType
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 * @param PlaceName PlaceName
		 * @param PlaceNameNullFlavour PlaceNameNullFlavour
		 * @param PlaceAddress PlaceAddress
		 */
	    public HealthCareFacilityUniversal(
		List<CodedValue> ServiceDeliveryLocationId, CodedValue CareSettingType, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName, String PlaceName, String PlaceNameNullFlavour, uk.nhs.interoperability.payloads.commontypes.Address PlaceAddress) {
			fields = new LinkedHashMap<String, Object>();
			
			setServiceDeliveryLocationId(ServiceDeliveryLocationId);
			setCareSettingType(CareSettingType);
			setOrgId(OrgId);
			setOrgName(OrgName);
			setPlaceName(PlaceName);
			setPlaceNameNullFlavour(PlaceNameNullFlavour);
			setPlaceAddress(PlaceAddress);
		}
	
		/**
		 * Local identifiers to identify the service delivery location
		 * @return List of CodedValue objects
		 */
		public List<CodedValue> getServiceDeliveryLocationId() {
			return (List<CodedValue>)getValue("ServiceDeliveryLocationId");
		}
		
		/**
		 * Local identifiers to identify the service delivery location
		 * @param ServiceDeliveryLocationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setServiceDeliveryLocationId(List ServiceDeliveryLocationId) {
			setValue("ServiceDeliveryLocationId", ServiceDeliveryLocationId);
			return this;
		}
		
		/**
		 * Local identifiers to identify the service delivery location
		 * <br>Note: This adds a CodedValue object, but this method can be called
		 * multiple times to add additional CodedValue objects.
		 * @param ServiceDeliveryLocationId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal addServiceDeliveryLocationId(CodedValue ServiceDeliveryLocationId) {
			addMultivalue("ServiceDeliveryLocationId", ServiceDeliveryLocationId);
			return this;
		}
		
		
		/**
		 * Local identifiers to identify the service delivery location
		 * <br>Note: This adds a coded entry from a specific vocabulary, but this method can be called
		 * multiple times to add additional coded entries objects.
		 * @param ServiceDeliveryLocationId value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal addServiceDeliveryLocationId(VocabularyEntry ServiceDeliveryLocationId) {
			Code c = new CodedValue(ServiceDeliveryLocationId);
			addMultivalue("ServiceDeliveryLocationId", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getCareSettingType() {
			return (CodedValue)getValue("CareSettingType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * @return CDACareSettingType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType getCareSettingTypeEnum() {
			CodedValue cv = (CodedValue)getValue("CareSettingType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDACareSettingType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * @param CareSettingType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setCareSettingType(CodedValue CareSettingType) {
			setValue("CareSettingType", CareSettingType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CareSettingType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setCareSettingType(VocabularyEntry CareSettingType) {
			Code c = new CodedValue(CareSettingType);
			setValue("CareSettingType", c);
			return this;
		}
		
		/**
		 * Identifier that uniquely identifies the organisation which is responsible for the service delivery location.
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * Identifier that uniquely identifies the organisation which is responsible for the service delivery location.
		 * <br><br>This field is MANDATORY
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getPlaceName() {
			return (String)getValue("PlaceName");
		}
		
		
		
		
		/**
		 * 
		 * @param PlaceName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setPlaceName(String PlaceName) {
			setValue("PlaceName", PlaceName);
			return this;
		}
		
		
		/**
		 * A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getPlaceNameNullFlavour() {
			return (String)getValue("PlaceNameNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getPlaceNameNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("PlaceNameNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param PlaceNameNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setPlaceNameNullFlavour(String PlaceNameNullFlavour) {
			setValue("PlaceNameNullFlavour", PlaceNameNullFlavour);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getPlaceAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("PlaceAddress");
		}
		
		
		
		
		/**
		 * 
		 * @param PlaceAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public HealthCareFacilityUniversal setPlaceAddress(uk.nhs.interoperability.payloads.commontypes.Address PlaceAddress) {
			setValue("PlaceAddress", PlaceAddress);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145211GB01#HealthCareFacility",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"SDLOC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ServiceDeliveryLocationId", new Field(
												"ServiceDeliveryLocationId",
												"x:id",
												"Local identifiers to identify the service delivery location",
												"false",
												"",
												"",
												"CodedValue",
												"",
												"100",
												"2.16.840.1.113883.2.1.3.2.4.18.38",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CareSettingType", new Field(
												"CareSettingType",
												"x:code",
												"",
												"true",
												"",
												"CDACareSettingType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:serviceProviderOrganization/@classCode",
												"ORG",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:serviceProviderOrganization/@determinerCode",
												"INSTANCE",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:serviceProviderOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:serviceProviderOrganization/x:templateId/@extension",
												"COCD_TP145211GB01#serviceProviderOrganization",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:serviceProviderOrganization/x:id",
												"Identifier that uniquely identifies the organisation which is responsible for the service delivery location.",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:serviceProviderOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceClassCode", new Field(
												"PlaceClassCode",
												"x:location/@classCode",
												"PLC",
												"PlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceDeterminerCode", new Field(
												"PlaceDeterminerCode",
												"x:location/@determinerCode",
												"INSTANCE",
												"PlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceTemplateIdRoot", new Field(
												"PlaceTemplateIdRoot",
												"x:location/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceTemplateId", new Field(
												"PlaceTemplateId",
												"x:location/x:templateId/@extension",
												"COCD_TP145211GB01#location",
												"PlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceName", new Field(
												"PlaceName",
												"x:location/x:name",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceNameNullFlavour", new Field(
												"PlaceNameNullFlavour",
												"x:location/x:name/@nullFlavor",
												"A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlaceNameNullFlavourWithAddress", new Field(
												"PlaceNameNullFlavourWithAddress",
												"x:location/x:name/@nullFlavor",
												"NA",
												"PlaceAddress",
												"PlaceName",
												"",
												"",
												"",
												""
												));
	
		put("PlaceAddress", new Field(
												"PlaceAddress",
												"x:location/x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public HealthCareFacilityUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public HealthCareFacilityUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	