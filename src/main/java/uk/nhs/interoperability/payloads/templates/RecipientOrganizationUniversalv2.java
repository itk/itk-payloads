/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the RecipientOrganizationUniversalv2 object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CDAOrganizationType</li>
 * </ul>
 * @author Adam Hatherly
 * @deprecated
 */

public class RecipientOrganizationUniversalv2 extends AbstractPayload implements Payload , Recipient {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "RecipientOrganizationUniversalv2";
		protected static final String shortName = "IntendedRecipient";
		protected static final String rootNode = "x:intendedRecipient";
		protected static final String version = "COCD_TP145203GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param TelephoneNumber TelephoneNumber
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 * @param CDAOrganizationType CDAOrganizationType
		 */
	    public RecipientOrganizationUniversalv2(
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName, CodedValue CDAOrganizationType) {
			fields = new LinkedHashMap<String, Object>();
			
			setTelephoneNumber(TelephoneNumber);
			setOrgId(OrgId);
			setOrgName(OrgName);
			setCDAOrganizationType(CDAOrganizationType);
		}
	
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientOrganizationUniversalv2 setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientOrganizationUniversalv2 addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which is the recipient
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which is the recipient
		 * <br><br>This field is MANDATORY
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientOrganizationUniversalv2 setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientOrganizationUniversalv2 setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @return CodedValue object
		 */	
		public CodedValue getCDAOrganizationType() {
			return (CodedValue)getValue("CDAOrganizationType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @return CDAOrganizationType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType getCDAOrganizationTypeEnum() {
			CodedValue cv = (CodedValue)getValue("CDAOrganizationType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAOrganizationType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @param CDAOrganizationType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientOrganizationUniversalv2 setCDAOrganizationType(CodedValue CDAOrganizationType) {
			setValue("CDAOrganizationType", CDAOrganizationType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CDAOrganizationType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientOrganizationUniversalv2 setCDAOrganizationType(VocabularyEntry CDAOrganizationType) {
			Code c = new CodedValue(CDAOrganizationType);
			setValue("CDAOrganizationType", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145203GB02#IntendedRecipient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:receivedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:receivedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:receivedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:receivedOrganization/x:templateId/@extension",
												"COCD_TP145203GB02#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:receivedOrganization/x:id",
												"An ODS code as an identifier that uniquely identifies the organisation which is the recipient",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:receivedOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAOrganizationType", new Field(
												"CDAOrganizationType",
												"x:receivedOrganization/x:standardIndustryClassCode",
												"",
												"false",
												"",
												"CDAOrganizationType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public RecipientOrganizationUniversalv2() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public RecipientOrganizationUniversalv2(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	