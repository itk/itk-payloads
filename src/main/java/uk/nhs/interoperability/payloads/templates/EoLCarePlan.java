/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the EoLCarePlan object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveFrom</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTo</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} OriginalTimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} OriginalAuthor</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} LastAmendedTimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} LastAmendedAuthor</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EoLToolSnCT</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} PlannedReviewDate</li>
 * <li>List&lt;{@link EOLPatientRelationships EOLPatientRelationships}&gt; PatientRelationships</li>
 * <li>String EOLToolStage</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class EoLCarePlan extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "EoLCarePlan";
		protected static final String shortName = "EoLCarePlan";
		protected static final String rootNode = "x:act";
		protected static final String version = "COCD_TP146419GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param EffectiveFrom EffectiveFrom
		 * @param EffectiveTo EffectiveTo
		 * @param OriginalTimeAuthored OriginalTimeAuthored
		 * @param OriginalAuthor OriginalAuthor
		 * @param LastAmendedTimeAuthored LastAmendedTimeAuthored
		 * @param LastAmendedAuthor LastAmendedAuthor
		 * @param EoLToolSnCT EoLToolSnCT
		 * @param PlannedReviewDate PlannedReviewDate
		 * @param PatientRelationships PatientRelationships
		 * @param EOLToolStage EOLToolStage
		 */
	    public EoLCarePlan(String ID, HL7Date EffectiveFrom, HL7Date EffectiveTo, HL7Date OriginalTimeAuthored, uk.nhs.interoperability.payloads.templates.Author OriginalAuthor, HL7Date LastAmendedTimeAuthored, uk.nhs.interoperability.payloads.templates.Author LastAmendedAuthor, CodedValue EoLToolSnCT, HL7Date PlannedReviewDate, 
		List<EOLPatientRelationships> PatientRelationships, String EOLToolStage) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setEffectiveFrom(EffectiveFrom);
			setEffectiveTo(EffectiveTo);
			setOriginalTimeAuthored(OriginalTimeAuthored);
			setOriginalAuthor(OriginalAuthor);
			setLastAmendedTimeAuthored(LastAmendedTimeAuthored);
			setLastAmendedAuthor(LastAmendedAuthor);
			setEoLToolSnCT(EoLToolSnCT);
			setPlannedReviewDate(PlannedReviewDate);
			setPatientRelationships(PatientRelationships);
			setEOLToolStage(EOLToolStage);
		}
	
		/**
		 * A DCE UUID to identify each instance of an End of Life care plan
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of an End of Life care plan
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * A 'start date' time stamp (equivalent to the start of the End of Life care plan).
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveFrom() {
			return (HL7Date)getValue("EffectiveFrom");
		}
		
		
		
		
		/**
		 * A 'start date' time stamp (equivalent to the start of the End of Life care plan).
		 * <br><br>This field is MANDATORY
		 * @param EffectiveFrom value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setEffectiveFrom(HL7Date EffectiveFrom) {
			setValue("EffectiveFrom", EffectiveFrom);
			return this;
		}
		
		
		/**
		 * An 'end date' time stamp when the End of Life care plan has ended (optional).
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTo() {
			return (HL7Date)getValue("EffectiveTo");
		}
		
		
		
		
		/**
		 * An 'end date' time stamp when the End of Life care plan has ended (optional).
		 * @param EffectiveTo value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setEffectiveTo(HL7Date EffectiveTo) {
			setValue("EffectiveTo", EffectiveTo);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getOriginalTimeAuthored() {
			return (HL7Date)getValue("OriginalTimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param OriginalTimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setOriginalTimeAuthored(HL7Date OriginalTimeAuthored) {
			setValue("OriginalTimeAuthored", OriginalTimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getOriginalAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("OriginalAuthor");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param OriginalAuthor value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setOriginalAuthor(uk.nhs.interoperability.payloads.templates.Author OriginalAuthor) {
			setValue("OriginalAuthor", OriginalAuthor);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getLastAmendedTimeAuthored() {
			return (HL7Date)getValue("LastAmendedTimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param LastAmendedTimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setLastAmendedTimeAuthored(HL7Date LastAmendedTimeAuthored) {
			setValue("LastAmendedTimeAuthored", LastAmendedTimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getLastAmendedAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("LastAmendedAuthor");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param LastAmendedAuthor value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setLastAmendedAuthor(uk.nhs.interoperability.payloads.templates.Author LastAmendedAuthor) {
			setValue("LastAmendedAuthor", LastAmendedAuthor);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLToolSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getEoLToolSnCT() {
			return (CodedValue)getValue("EoLToolSnCT");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLToolSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT
		 * <br><br>This field is MANDATORY
		 * @return EoLToolSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT getEoLToolSnCTEnum() {
			CodedValue cv = (CodedValue)getValue("EoLToolSnCT");
			VocabularyEntry entry = VocabularyFactory.getVocab("EoLToolSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLToolSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT
		 * <br><br>This field is MANDATORY
		 * @param EoLToolSnCT value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setEoLToolSnCT(CodedValue EoLToolSnCT) {
			setValue("EoLToolSnCT", EoLToolSnCT);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLToolSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EoLToolSnCT value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setEoLToolSnCT(VocabularyEntry EoLToolSnCT) {
			Code c = new CodedValue(EoLToolSnCT);
			setValue("EoLToolSnCT", c);
			return this;
		}
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getPlannedReviewDate() {
			return (HL7Date)getValue("PlannedReviewDate");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param PlannedReviewDate value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setPlannedReviewDate(HL7Date PlannedReviewDate) {
			setValue("PlannedReviewDate", PlannedReviewDate);
			return this;
		}
		
		
		/**
		 * The patient relationships associated with the End of Life care plan
		 * <br><br>This field is MANDATORY
		 * @return List of EOLPatientRelationships objects
		 */
		public List<EOLPatientRelationships> getPatientRelationships() {
			return (List<EOLPatientRelationships>)getValue("PatientRelationships");
		}
		
		/**
		 * The patient relationships associated with the End of Life care plan
		 * <br><br>This field is MANDATORY
		 * @param PatientRelationships value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setPatientRelationships(List PatientRelationships) {
			setValue("PatientRelationships", PatientRelationships);
			return this;
		}
		
		/**
		 * The patient relationships associated with the End of Life care plan
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a EOLPatientRelationships object, but this method can be called
		 * multiple times to add additional EOLPatientRelationships objects.
		 * @param PatientRelationships value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan addPatientRelationships(EOLPatientRelationships PatientRelationships) {
			addMultivalue("PatientRelationships", PatientRelationships);
			return this;
		}
		
		
		/**
		 * The current stage reached within the End of Life tool.
		 * @return String object
		 */	
		public String getEOLToolStage() {
			return (String)getValue("EOLToolStage");
		}
		
		
		
		
		/**
		 * The current stage reached within the End of Life tool.
		 * @param EOLToolStage value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EoLCarePlan setEOLToolStage(String EOLToolStage) {
			setValue("EOLToolStage", EOLToolStage);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PCPR",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"INT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146419GB01#EoLCarePlan",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of an End of Life care plan",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeValue", new Field(
												"CodeValue",
												"x:code/@code",
												"EOLCP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeSystem", new Field(
												"CodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeDisplayName", new Field(
												"CodeDisplayName",
												"x:code/@displayName",
												"EOL Care Plan",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"active",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveFrom", new Field(
												"EffectiveFrom",
												"x:effectiveTime/x:low/@value",
												"A 'start date' time stamp (equivalent to the start of the End of Life care plan).",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTo", new Field(
												"EffectiveTo",
												"x:effectiveTime/x:high/@value",
												"An 'end date' time stamp when the End of Life care plan has ended (optional).",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthorFunctionCodeValue", new Field(
												"OriginalAuthorFunctionCodeValue",
												"x:author[x:functionCode/@code='OA']/x:functionCode/@code",
												"OA",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthorTypeCode", new Field(
												"OriginalAuthorTypeCode",
												"x:author[x:functionCode/@code='OA']/@typeCode",
												"AUT",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthorContextControlCode", new Field(
												"OriginalAuthorContextControlCode",
												"x:author[x:functionCode/@code='OA']/@contextControlCode",
												"OP",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthorContentId", new Field(
												"OriginalAuthorContentId",
												"x:author[x:functionCode/@code='OA']/npfitlc:contentId/@extension",
												"...",
												"OriginalAuthor",
												"",
												"",
												"",
												"OriginalAuthor",
												"true"
												));
	
		put("OriginalAuthorContentIdRoot", new Field(
												"OriginalAuthorContentIdRoot",
												"x:author[x:functionCode/@code='OA']/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("OriginalAuthorTemplateIdRoot", new Field(
												"OriginalAuthorTemplateIdRoot",
												"x:author[x:functionCode/@code='OA']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("OriginalAuthorTemplateId", new Field(
												"OriginalAuthorTemplateId",
												"x:author[x:functionCode/@code='OA']/x:templateId/@extension",
												"COCD_TP146419GB01#author",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("OriginalTimeAuthored", new Field(
												"OriginalTimeAuthored",
												"x:author[x:functionCode/@code='OA']/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthor", new Field(
												"OriginalAuthor",
												"x:author[x:functionCode/@code='OA']/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author[x:functionCode/@code='OA']/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthorFunctionCodeSystem", new Field(
												"OriginalAuthorFunctionCodeSystem",
												"x:author[x:functionCode/@code='OA']/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginalAuthorFunctionCodeDisplayName", new Field(
												"OriginalAuthorFunctionCodeDisplayName",
												"x:author[x:functionCode/@code='OA']/x:functionCode/@displayName",
												"Originating Author",
												"OriginalAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthorFunctionCodeValue", new Field(
												"LastAmendedAuthorFunctionCodeValue",
												"x:author[x:functionCode/@code='LA']/x:functionCode/@code",
												"LA",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthorTypeCode", new Field(
												"LastAmendedAuthorTypeCode",
												"x:author[x:functionCode/@code='LA']/@typeCode",
												"AUT",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthorContextControlCode", new Field(
												"LastAmendedAuthorContextControlCode",
												"x:author[x:functionCode/@code='LA']/@contextControlCode",
												"OP",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthorContentId", new Field(
												"LastAmendedAuthorContentId",
												"x:author[x:functionCode/@code='LA']/npfitlc:contentId/@extension",
												"...",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"LastAmendedAuthor",
												"true"
												));
	
		put("LastAmendedAuthorContentIdRoot", new Field(
												"LastAmendedAuthorContentIdRoot",
												"x:author[x:functionCode/@code='LA']/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("LastAmendedAuthorTemplateIdRoot", new Field(
												"LastAmendedAuthorTemplateIdRoot",
												"x:author[x:functionCode/@code='LA']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("LastAmendedAuthorTemplateId", new Field(
												"LastAmendedAuthorTemplateId",
												"x:author[x:functionCode/@code='LA']/x:templateId/@extension",
												"COCD_TP146419GB01#author1",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("LastAmendedTimeAuthored", new Field(
												"LastAmendedTimeAuthored",
												"x:author[x:functionCode/@code='LA']/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthor", new Field(
												"LastAmendedAuthor",
												"x:author[x:functionCode/@code='LA']/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author[x:functionCode/@code='LA']/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthorFunctionCodeSystem", new Field(
												"LastAmendedAuthorFunctionCodeSystem",
												"x:author[x:functionCode/@code='LA']/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LastAmendedAuthorFunctionCodeDisplayName", new Field(
												"LastAmendedAuthorFunctionCodeDisplayName",
												"x:author[x:functionCode/@code='LA']/x:functionCode/@displayName",
												"Last Amending Author",
												"LastAmendedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateId", new Field(
												"ER1TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:templateId/@extension",
												"COCD_TP146419GB01#entryRelationship1",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TypeCode", new Field(
												"ER1TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1ContextConductionInd", new Field(
												"ER1ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateIdRoot", new Field(
												"ER1TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1SeperatableInd", new Field(
												"ER1SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:seperatableInd/@value",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedClassCode", new Field(
												"EOLToolUsedClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedMoodCode", new Field(
												"EOLToolUsedMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedTemplateIdRoot", new Field(
												"EOLToolUsedTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedTemplateId", new Field(
												"EOLToolUsedTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:templateId/@extension",
												"COCD_TP146419GB01#eOLToolUsed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedCodeValue", new Field(
												"EOLToolUsedCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:code/@code",
												"EOLTLU",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedCodeSystem", new Field(
												"EOLToolUsedCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolUsedCodeDisplayName", new Field(
												"EOLToolUsedCodeDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:code/@displayName",
												"EOL Tool Used",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EoLToolSnCT", new Field(
												"EoLToolSnCT",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:value",
												"",
												"true",
												"",
												"EoLToolSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EoLToolSnCTType", new Field(
												"EoLToolSnCTType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"EoLToolSnCT",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2TemplateId", new Field(
												"ER2TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:templateId/@extension",
												"COCD_TP146419GB01#entryRelationship2",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2TypeCode", new Field(
												"ER2TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/@typeCode",
												"COMP",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2ContextConductionInd", new Field(
												"ER2ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/@contextConductionInd",
												"true",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2TemplateIdRoot", new Field(
												"ER2TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2SeperatableInd", new Field(
												"ER2SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:seperatableInd/@value",
												"false",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateClassCode", new Field(
												"ReviewDateClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/@classCode",
												"OBS",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateMoodCode", new Field(
												"ReviewDateMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/@moodCode",
												"EVN",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateTemplateIdRoot", new Field(
												"ReviewDateTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateTemplateId", new Field(
												"ReviewDateTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:templateId/@extension",
												"COCD_TP146419GB01#plannedReviewDate",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateCodeValue", new Field(
												"ReviewDateCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:code/@code",
												"EOLPRD",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateDisplayName", new Field(
												"ReviewDateDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:code/@displayName",
												"EOL Planned Review Date",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateCodeSystem", new Field(
												"ReviewDateCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlannedReviewDate", new Field(
												"PlannedReviewDate",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:value/@value",
												"",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PlannedReviewDateType", new Field(
												"PlannedReviewDateType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship2']/x:observation/x:value/@xsi:type",
												"TS",
												"PlannedReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientRelationships", new Field(
												"PatientRelationships",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship3']",
												"The patient relationships associated with the End of Life care plan",
												"true",
												"",
												"",
												"EOLPatientRelationships",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TemplateId", new Field(
												"ER4TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:templateId/@extension",
												"COCD_TP146419GB01#entryRelationship4",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TypeCode", new Field(
												"ER4TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/@typeCode",
												"COMP",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4ContextConductionInd", new Field(
												"ER4ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/@contextConductionInd",
												"true",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TemplateIdRoot", new Field(
												"ER4TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4SeperatableInd", new Field(
												"ER4SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:seperatableInd/@value",
												"false",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageClassCode", new Field(
												"EOLStageClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/@classCode",
												"OBS",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageMoodCode", new Field(
												"EOLStageMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/@moodCode",
												"EVN",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageTemplateIdRoot", new Field(
												"EOLStageTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageTemplateId", new Field(
												"EOLStageTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:templateId/@extension",
												"COCD_TP146419GB01#eOLCurrentStage",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageCodeValue", new Field(
												"EOLStageCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:code/@code",
												"EOLTST",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageCodeSystem", new Field(
												"EOLStageCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLStageDisplayName", new Field(
												"EOLStageDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:code/@displayName",
												"EOL Current Stage",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolStage", new Field(
												"EOLToolStage",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:value",
												"The current stage reached within the End of Life tool.",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EOLToolStageDataType", new Field(
												"EOLToolStageDataType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146419GB01#entryRelationship1']/x:observation/x:entryRelationship/x:observation/x:value/@xsi:type",
												"ST",
												"EOLToolStage",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public EoLCarePlan() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public EoLCarePlan(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	