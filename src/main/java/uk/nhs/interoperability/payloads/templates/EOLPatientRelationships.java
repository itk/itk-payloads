/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the EOLPatientRelationships object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link PatientRelationships PatientRelationships} Relationships</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class EOLPatientRelationships extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "EOLPatientRelationships";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param Relationships PatientRelationships
		 */
		public EOLPatientRelationships(PatientRelationships Relationships) {
			fields = new LinkedHashMap<String, Object>();
			
			setRelationships(Relationships);
		}

		/**
		 * List of relationships the patient has (grouped by the date they were added to the record)
		 * <br><br>This field is MANDATORY
		 * @return PatientRelationships object
		 */	
		public PatientRelationships getRelationships() {
			return (PatientRelationships)getValue("Relationships");
		}
		
		
		
		
		/**
		 * List of relationships the patient has (grouped by the date they were added to the record)
		 * <br><br>This field is MANDATORY
		 * @param Relationships value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EOLPatientRelationships setRelationships(PatientRelationships Relationships) {
			setValue("Relationships", Relationships);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("RelationshipsTypeCode", new Field(
												"RelationshipsTypeCode",
												"@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelationshipsContextConductionInd", new Field(
												"RelationshipsContextConductionInd",
												"@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelationshipsTemplateId", new Field(
												"RelationshipsTemplateId",
												"x:templateId/@extension",
												"COCD_TP146419GB01#entryRelationship3",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelationshipsTemplateIdRoot", new Field(
												"RelationshipsTemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelationshipsContentId", new Field(
												"RelationshipsContentId",
												"npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"Relationships",
												""
												));
	
		put("RelationshipsContentIdRoot", new Field(
												"RelationshipsContentIdRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelationshipsSeperatableInd", new Field(
												"RelationshipsSeperatableInd",
												"x:seperatableInd/@value",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Relationships", new Field(
												"Relationships",
												"x:act",
												"List of relationships the patient has (grouped by the date they were added to the record)",
												"true",
												"",
												"",
												"PatientRelationships",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public EOLPatientRelationships() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public EOLPatientRelationships(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	