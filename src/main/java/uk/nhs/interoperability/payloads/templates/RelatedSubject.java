/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the RelatedSubject object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} RelationshipType</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; Name</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Sex</li>
 * <li>String SexNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} BirthTime</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class RelatedSubject extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "RelatedSubject";
		protected static final String shortName = "RelatedSubject";
		protected static final String rootNode = "x:relatedSubject";
		protected static final String version = "COCD_TP145213GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param RelationshipType RelationshipType
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param Name Name
		 * @param Sex Sex
		 * @param SexNullFlavour SexNullFlavour
		 * @param BirthTime BirthTime
		 */
	    public RelatedSubject(CodedValue RelationshipType, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> Name, CodedValue Sex, String SexNullFlavour, HL7Date BirthTime) {
			fields = new LinkedHashMap<String, Object>();
			
			setRelationshipType(RelationshipType);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setName(Name);
			setSex(Sex);
			setSexNullFlavour(SexNullFlavour);
			setBirthTime(BirthTime);
		}
	
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * @return CodedValue object
		 */	
		public CodedValue getRelationshipType() {
			return (CodedValue)getValue("RelationshipType");
		}
		
		
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * @return PersonalRelationshipRoleType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType getRelationshipTypeEnum() {
			CodedValue cv = (CodedValue)getValue("RelationshipType");
			VocabularyEntry entry = VocabularyFactory.getVocab("PersonalRelationshipRoleType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType)entry;
		}
		
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * @param RelationshipType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setRelationshipType(CodedValue RelationshipType) {
			setValue("RelationshipType", RelationshipType);
			return this;
		}
		
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param RelationshipType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setRelationshipType(VocabularyEntry RelationshipType) {
			Code c = new CodedValue(RelationshipType);
			setValue("RelationshipType", c);
			return this;
		}
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * 
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * Telephone Number
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("Name");
		}
		
		/**
		 * Person name
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setName(List Name) {
			setValue("Name", Name);
			return this;
		}
		
		/**
		 * Person name
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param Name value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject addName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			addMultivalue("Name", Name);
			return this;
		}
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return CodedValue object
		 */	
		public CodedValue getSex() {
			return (CodedValue)getValue("Sex");
		}
		
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getSexEnum() {
			CodedValue cv = (CodedValue)getValue("Sex");
			VocabularyEntry entry = VocabularyFactory.getVocab("Sex", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Sex)entry;
		}
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Sex value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setSex(CodedValue Sex) {
			setValue("Sex", Sex);
			return this;
		}
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Sex value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setSex(VocabularyEntry Sex) {
			Code c = new CodedValue(Sex);
			setValue("Sex", c);
			return this;
		}
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getSexNullFlavour() {
			return (String)getValue("SexNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getSexNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("SexNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param SexNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setSexNullFlavour(String SexNullFlavour) {
			setValue("SexNullFlavour", SexNullFlavour);
			return this;
		}
		
		
		/**
		 * Date
		 * @return HL7Date object
		 */	
		public HL7Date getBirthTime() {
			return (HL7Date)getValue("BirthTime");
		}
		
		
		
		
		/**
		 * Date
		 * @param BirthTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedSubject setBirthTime(HL7Date BirthTime) {
			setValue("BirthTime", BirthTime);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PRS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145213GB01#RelatedSubject",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RelationshipType", new Field(
												"RelationshipType",
												"x:code",
												"A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person",
												"false",
												"",
												"PersonalRelationshipRoleType",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.407",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectClassCode", new Field(
												"SubjectClassCode",
												"x:subject/@classCode",
												"PSN",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectDeterminerCode", new Field(
												"SubjectDeterminerCode",
												"x:subject/@determinerCode",
												"INSTANCE",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateIdRoot", new Field(
												"SubjectTemplateIdRoot",
												"x:subject/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateId", new Field(
												"SubjectTemplateId",
												"x:subject/x:templateId/@extension",
												"COCD_TP145213GB01#subject",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:subject/x:name",
												"Person name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Sex", new Field(
												"Sex",
												"x:subject/x:administrativeGenderCode",
												"A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person",
												"false",
												"",
												"Sex",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.408",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SexNullFlavour", new Field(
												"SexNullFlavour",
												"x:subject/x:administrativeGenderCode/@nullFlavor",
												"A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthTime", new Field(
												"BirthTime",
												"x:subject/x:birthTime/@value",
												"Date",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public RelatedSubject() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public RelatedSubject(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	