/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the NewBornPhysicalExamination object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor ChildScreeningAuthor} Author</li>
 * <li>String GestationalAgeInDays</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} HipsExaminationEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} HipsExamination</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} HipsUltraSoundEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} UltraSoundDecisionEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} UltraSoundDecision</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} ExpertManagementPlanEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ExpertManagementPlan</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} HeartExaminationEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} HeartExamination</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EyesExaminationEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EyesExamination</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TestesExaminationEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TestesExamination</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class NewBornPhysicalExamination extends AbstractPayload implements Payload , ChildScreeningCodedSections {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "NewBornPhysicalExamination";
		protected static final String shortName = "NewBornPhysicalExamination";
		protected static final String rootNode = "x:procedure";
		protected static final String version = "COCD_TP146004GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param EffectiveTime EffectiveTime
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param GestationalAgeInDays GestationalAgeInDays
		 * @param HipsExaminationEffectiveTime HipsExaminationEffectiveTime
		 * @param HipsExamination HipsExamination
		 * @param HipsUltraSoundEffectiveTime HipsUltraSoundEffectiveTime
		 * @param UltraSoundDecisionEffectiveTime UltraSoundDecisionEffectiveTime
		 * @param UltraSoundDecision UltraSoundDecision
		 * @param ExpertManagementPlanEffectiveTime ExpertManagementPlanEffectiveTime
		 * @param ExpertManagementPlan ExpertManagementPlan
		 * @param HeartExaminationEffectiveTime HeartExaminationEffectiveTime
		 * @param HeartExamination HeartExamination
		 * @param EyesExaminationEffectiveTime EyesExaminationEffectiveTime
		 * @param EyesExamination EyesExamination
		 * @param TestesExaminationEffectiveTime TestesExaminationEffectiveTime
		 * @param TestesExamination TestesExamination
		 */
	    public NewBornPhysicalExamination(String Id, HL7Date EffectiveTime, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author, String GestationalAgeInDays, HL7Date HipsExaminationEffectiveTime, CodedValue HipsExamination, HL7Date HipsUltraSoundEffectiveTime, HL7Date UltraSoundDecisionEffectiveTime, CodedValue UltraSoundDecision, HL7Date ExpertManagementPlanEffectiveTime, CodedValue ExpertManagementPlan, HL7Date HeartExaminationEffectiveTime, CodedValue HeartExamination, HL7Date EyesExaminationEffectiveTime, CodedValue EyesExamination, HL7Date TestesExaminationEffectiveTime, CodedValue TestesExamination) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setEffectiveTime(EffectiveTime);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setGestationalAgeInDays(GestationalAgeInDays);
			setHipsExaminationEffectiveTime(HipsExaminationEffectiveTime);
			setHipsExamination(HipsExamination);
			setHipsUltraSoundEffectiveTime(HipsUltraSoundEffectiveTime);
			setUltraSoundDecisionEffectiveTime(UltraSoundDecisionEffectiveTime);
			setUltraSoundDecision(UltraSoundDecision);
			setExpertManagementPlanEffectiveTime(ExpertManagementPlanEffectiveTime);
			setExpertManagementPlan(ExpertManagementPlan);
			setHeartExaminationEffectiveTime(HeartExaminationEffectiveTime);
			setHeartExamination(HeartExamination);
			setEyesExaminationEffectiveTime(EyesExaminationEffectiveTime);
			setEyesExamination(EyesExamination);
			setTestesExaminationEffectiveTime(TestesExaminationEffectiveTime);
			setTestesExamination(TestesExamination);
		}
	
		/**
		 * A DCE UUID to identify each instance of a newborn physical examination.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of a newborn physical examination.
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * Time when the child physical examination was done.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * Time when the child physical examination was done.
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor object
		 */	
		public uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setAuthor(uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The value of the GestationalAgeInDays observation
		 * @return String object
		 */	
		public String getGestationalAgeInDays() {
			return (String)getValue("GestationalAgeInDays");
		}
		
		
		
		
		/**
		 * The value of the GestationalAgeInDays observation
		 * @param GestationalAgeInDays value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setGestationalAgeInDays(String GestationalAgeInDays) {
			setValue("GestationalAgeInDays", GestationalAgeInDays);
			return this;
		}
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getHipsExaminationEffectiveTime() {
			return (HL7Date)getValue("HipsExaminationEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param HipsExaminationEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHipsExaminationEffectiveTime(HL7Date HipsExaminationEffectiveTime) {
			setValue("HipsExaminationEffectiveTime", HipsExaminationEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult
		 * @return CodedValue object
		 */	
		public CodedValue getHipsExamination() {
			return (CodedValue)getValue("HipsExamination");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult
		 * @return HipsExaminationResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult getHipsExaminationEnum() {
			CodedValue cv = (CodedValue)getValue("HipsExamination");
			VocabularyEntry entry = VocabularyFactory.getVocab("HipsExaminationResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult
		 * @param HipsExamination value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHipsExamination(CodedValue HipsExamination) {
			setValue("HipsExamination", HipsExamination);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param HipsExamination value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHipsExamination(VocabularyEntry HipsExamination) {
			Code c = new CodedValue(HipsExamination);
			setValue("HipsExamination", c);
			return this;
		}
		
		/**
		 * Time when the procedure was done.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getHipsUltraSoundEffectiveTime() {
			return (HL7Date)getValue("HipsUltraSoundEffectiveTime");
		}
		
		
		
		
		/**
		 * Time when the procedure was done.
		 * <br><br>This field is MANDATORY
		 * @param HipsUltraSoundEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHipsUltraSoundEffectiveTime(HL7Date HipsUltraSoundEffectiveTime) {
			setValue("HipsUltraSoundEffectiveTime", HipsUltraSoundEffectiveTime);
			return this;
		}
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getUltraSoundDecisionEffectiveTime() {
			return (HL7Date)getValue("UltraSoundDecisionEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param UltraSoundDecisionEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setUltraSoundDecisionEffectiveTime(HL7Date UltraSoundDecisionEffectiveTime) {
			setValue("UltraSoundDecisionEffectiveTime", UltraSoundDecisionEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsUltraSoundOutcomeDecision" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision
		 * @return CodedValue object
		 */	
		public CodedValue getUltraSoundDecision() {
			return (CodedValue)getValue("UltraSoundDecision");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsUltraSoundOutcomeDecision" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision
		 * @return HipsUltraSoundOutcomeDecision enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision getUltraSoundDecisionEnum() {
			CodedValue cv = (CodedValue)getValue("UltraSoundDecision");
			VocabularyEntry entry = VocabularyFactory.getVocab("HipsUltraSoundOutcomeDecision", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsUltraSoundOutcomeDecision" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision
		 * @param UltraSoundDecision value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setUltraSoundDecision(CodedValue UltraSoundDecision) {
			setValue("UltraSoundDecision", UltraSoundDecision);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsUltraSoundOutcomeDecision" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param UltraSoundDecision value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setUltraSoundDecision(VocabularyEntry UltraSoundDecision) {
			Code c = new CodedValue(UltraSoundDecision);
			setValue("UltraSoundDecision", c);
			return this;
		}
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getExpertManagementPlanEffectiveTime() {
			return (HL7Date)getValue("ExpertManagementPlanEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param ExpertManagementPlanEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setExpertManagementPlanEffectiveTime(HL7Date ExpertManagementPlanEffectiveTime) {
			setValue("ExpertManagementPlanEffectiveTime", ExpertManagementPlanEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExpertManagementPlanType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType
		 * @return CodedValue object
		 */	
		public CodedValue getExpertManagementPlan() {
			return (CodedValue)getValue("ExpertManagementPlan");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExpertManagementPlanType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType
		 * @return HipsExpertManagementPlanType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType getExpertManagementPlanEnum() {
			CodedValue cv = (CodedValue)getValue("ExpertManagementPlan");
			VocabularyEntry entry = VocabularyFactory.getVocab("HipsExpertManagementPlanType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExpertManagementPlanType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType
		 * @param ExpertManagementPlan value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setExpertManagementPlan(CodedValue ExpertManagementPlan) {
			setValue("ExpertManagementPlan", ExpertManagementPlan);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HipsExpertManagementPlanType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ExpertManagementPlan value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setExpertManagementPlan(VocabularyEntry ExpertManagementPlan) {
			Code c = new CodedValue(ExpertManagementPlan);
			setValue("ExpertManagementPlan", c);
			return this;
		}
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getHeartExaminationEffectiveTime() {
			return (HL7Date)getValue("HeartExaminationEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param HeartExaminationEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHeartExaminationEffectiveTime(HL7Date HeartExaminationEffectiveTime) {
			setValue("HeartExaminationEffectiveTime", HeartExaminationEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HeartExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult
		 * @return CodedValue object
		 */	
		public CodedValue getHeartExamination() {
			return (CodedValue)getValue("HeartExamination");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HeartExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult
		 * @return HeartExaminationResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult getHeartExaminationEnum() {
			CodedValue cv = (CodedValue)getValue("HeartExamination");
			VocabularyEntry entry = VocabularyFactory.getVocab("HeartExaminationResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HeartExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult
		 * @param HeartExamination value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHeartExamination(CodedValue HeartExamination) {
			setValue("HeartExamination", HeartExamination);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "HeartExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param HeartExamination value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setHeartExamination(VocabularyEntry HeartExamination) {
			Code c = new CodedValue(HeartExamination);
			setValue("HeartExamination", c);
			return this;
		}
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEyesExaminationEffectiveTime() {
			return (HL7Date)getValue("EyesExaminationEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param EyesExaminationEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setEyesExaminationEffectiveTime(HL7Date EyesExaminationEffectiveTime) {
			setValue("EyesExaminationEffectiveTime", EyesExaminationEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "EyesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult
		 * @return CodedValue object
		 */	
		public CodedValue getEyesExamination() {
			return (CodedValue)getValue("EyesExamination");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "EyesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult
		 * @return EyesExaminationResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult getEyesExaminationEnum() {
			CodedValue cv = (CodedValue)getValue("EyesExamination");
			VocabularyEntry entry = VocabularyFactory.getVocab("EyesExaminationResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "EyesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult
		 * @param EyesExamination value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setEyesExamination(CodedValue EyesExamination) {
			setValue("EyesExamination", EyesExamination);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "EyesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EyesExamination value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setEyesExamination(VocabularyEntry EyesExamination) {
			Code c = new CodedValue(EyesExamination);
			setValue("EyesExamination", c);
			return this;
		}
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTestesExaminationEffectiveTime() {
			return (HL7Date)getValue("TestesExaminationEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param TestesExaminationEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setTestesExaminationEffectiveTime(HL7Date TestesExaminationEffectiveTime) {
			setValue("TestesExaminationEffectiveTime", TestesExaminationEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "TestesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult
		 * @return CodedValue object
		 */	
		public CodedValue getTestesExamination() {
			return (CodedValue)getValue("TestesExamination");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "TestesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult
		 * @return TestesExaminationResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult getTestesExaminationEnum() {
			CodedValue cv = (CodedValue)getValue("TestesExamination");
			VocabularyEntry entry = VocabularyFactory.getVocab("TestesExaminationResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "TestesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult
		 * @param TestesExamination value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setTestesExamination(CodedValue TestesExamination) {
			setValue("TestesExamination", TestesExamination);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "TestesExaminationResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TestesExamination value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornPhysicalExamination setTestesExamination(VocabularyEntry TestesExamination) {
			Code c = new CodedValue(TestesExamination);
			setValue("TestesExamination", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PROC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146004GB01#NewBornPhysicalExamination",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify each instance of a newborn physical examination.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValue", new Field(
												"CodeFixedCodedValue",
												"x:code/@code",
												"243788004",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueCodeSystem", new Field(
												"CodeFixedCodedValueCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueDisplayName", new Field(
												"CodeFixedCodedValueDisplayName",
												"x:code/@displayName",
												"Child examination",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"Time when the child physical examination was done.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146004GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"ChildScreeningAuthor",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentTemplateId", new Field(
												"GestationalAgeInDaysComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentTypeCode", new Field(
												"GestationalAgeInDaysComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/@typeCode",
												"COMP",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentContextConductionInd", new Field(
												"GestationalAgeInDaysComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/@contextConductionInd",
												"true",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentTemplateIdRoot", new Field(
												"GestationalAgeInDaysComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentSeperatableInd", new Field(
												"GestationalAgeInDaysComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:seperatableInd/@value",
												"false",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationClassCode", new Field(
												"GestationalAgeInDaysComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/@classCode",
												"OBS",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationMoodCode", new Field(
												"GestationalAgeInDaysComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/@moodCode",
												"EVN",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationTemplateIdRoot", new Field(
												"GestationalAgeInDaysComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationTemplateId", new Field(
												"GestationalAgeInDaysComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#gestationalAge",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationCodeFixedCodedValue", new Field(
												"GestationalAgeInDaysComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:code/@code",
												"GA",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"GestationalAgeInDaysComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"GestationalAgeInDaysComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:code/@displayName",
												"Gestational age",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDays", new Field(
												"GestationalAgeInDays",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:value/@value",
												"The value of the GestationalAgeInDays observation",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysPQ", new Field(
												"GestationalAgeInDaysPQ",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:value/@xsi:type",
												"PQ",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInDaysType", new Field(
												"GestationalAgeInDaysType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship']/x:observation/x:value/@unit",
												"d",
												"GestationalAgeInDays",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentTemplateId", new Field(
												"HipsExaminationComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship1",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentTypeCode", new Field(
												"HipsExaminationComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/@typeCode",
												"COMP",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentContextConductionInd", new Field(
												"HipsExaminationComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/@contextConductionInd",
												"true",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentTemplateIdRoot", new Field(
												"HipsExaminationComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentSeperatableInd", new Field(
												"HipsExaminationComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:seperatableInd/@value",
												"false",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationClassCode", new Field(
												"HipsExaminationComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/@classCode",
												"OBS",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationMoodCode", new Field(
												"HipsExaminationComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/@moodCode",
												"EVN",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationTemplateIdRoot", new Field(
												"HipsExaminationComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationTemplateId", new Field(
												"HipsExaminationComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#hipsExamination",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationCodeFixedCodedValue", new Field(
												"HipsExaminationComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:code/@code",
												"170205006",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"HipsExaminationComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"HipsExaminationComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:code/@displayName",
												"Hips examination",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationEffectiveTime", new Field(
												"HipsExaminationEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExamination", new Field(
												"HipsExamination",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"HipsExaminationResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsExaminationType", new Field(
												"HipsExaminationType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"HipsExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentTemplateId", new Field(
												"HipsUltraSoundComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship5",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentTypeCode", new Field(
												"HipsUltraSoundComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/@typeCode",
												"COMP",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentContextConductionInd", new Field(
												"HipsUltraSoundComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/@contextConductionInd",
												"true",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentTemplateIdRoot", new Field(
												"HipsUltraSoundComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentSeperatableInd", new Field(
												"HipsUltraSoundComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:seperatableInd/@value",
												"false",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureClassCode", new Field(
												"HipsUltraSoundComponentProcedureClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/@classCode",
												"PROC",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureMoodCode", new Field(
												"HipsUltraSoundComponentProcedureMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/@moodCode",
												"EVN",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureTemplateIdRoot", new Field(
												"HipsUltraSoundComponentProcedureTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureTemplateId", new Field(
												"HipsUltraSoundComponentProcedureTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/x:templateId/@extension",
												"COCD_TP146004GB01#hipsUltraSound",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureCodeFixedCodedValue", new Field(
												"HipsUltraSoundComponentProcedureCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/x:code/@code",
												"426903000",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureCodeFixedCodedValueCodeSystem", new Field(
												"HipsUltraSoundComponentProcedureCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundComponentProcedureCodeFixedCodedValueDisplayName", new Field(
												"HipsUltraSoundComponentProcedureCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/x:code/@displayName",
												"Hips ultra sound procedure",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HipsUltraSoundEffectiveTime", new Field(
												"HipsUltraSoundEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship5']/x:procedure/x:effectiveTime/@value",
												"Time when the procedure was done.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentTemplateId", new Field(
												"UltraSoundDecisionComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship6",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentTypeCode", new Field(
												"UltraSoundDecisionComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/@typeCode",
												"COMP",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentContextConductionInd", new Field(
												"UltraSoundDecisionComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/@contextConductionInd",
												"true",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentTemplateIdRoot", new Field(
												"UltraSoundDecisionComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentSeperatableInd", new Field(
												"UltraSoundDecisionComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:seperatableInd/@value",
												"false",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationClassCode", new Field(
												"UltraSoundDecisionComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/@classCode",
												"OBS",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationMoodCode", new Field(
												"UltraSoundDecisionComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/@moodCode",
												"EVN",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationTemplateIdRoot", new Field(
												"UltraSoundDecisionComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationTemplateId", new Field(
												"UltraSoundDecisionComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#ultraSoundDecision",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationCodeFixedCodedValue", new Field(
												"UltraSoundDecisionComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:code/@code",
												"HUO",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"UltraSoundDecisionComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"UltraSoundDecisionComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:code/@displayName",
												"Hips ultra sound outcome decision",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionEffectiveTime", new Field(
												"UltraSoundDecisionEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecision", new Field(
												"UltraSoundDecision",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"HipsUltraSoundOutcomeDecision",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UltraSoundDecisionType", new Field(
												"UltraSoundDecisionType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship6']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"UltraSoundDecision",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentTemplateId", new Field(
												"ExpertManagementPlanComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship7",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentTypeCode", new Field(
												"ExpertManagementPlanComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/@typeCode",
												"COMP",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentContextConductionInd", new Field(
												"ExpertManagementPlanComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/@contextConductionInd",
												"true",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentTemplateIdRoot", new Field(
												"ExpertManagementPlanComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentSeperatableInd", new Field(
												"ExpertManagementPlanComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:seperatableInd/@value",
												"false",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationClassCode", new Field(
												"ExpertManagementPlanComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/@classCode",
												"OBS",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationMoodCode", new Field(
												"ExpertManagementPlanComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/@moodCode",
												"EVN",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationTemplateIdRoot", new Field(
												"ExpertManagementPlanComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationTemplateId", new Field(
												"ExpertManagementPlanComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#expertManagementPlan",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationCodeFixedCodedValue", new Field(
												"ExpertManagementPlanComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:code/@code",
												"EMP",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"ExpertManagementPlanComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"ExpertManagementPlanComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:code/@displayName",
												"Expert management plan",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanEffectiveTime", new Field(
												"ExpertManagementPlanEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlan", new Field(
												"ExpertManagementPlan",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"HipsExpertManagementPlanType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ExpertManagementPlanType", new Field(
												"ExpertManagementPlanType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship1']/x:observation/x:entryRelationship/x:procedure/x:entryRelationship/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship7']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"ExpertManagementPlan",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentTemplateId", new Field(
												"HeartExaminationComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship2",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentTypeCode", new Field(
												"HeartExaminationComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/@typeCode",
												"COMP",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentContextConductionInd", new Field(
												"HeartExaminationComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/@contextConductionInd",
												"true",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentTemplateIdRoot", new Field(
												"HeartExaminationComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentSeperatableInd", new Field(
												"HeartExaminationComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:seperatableInd/@value",
												"false",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationClassCode", new Field(
												"HeartExaminationComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/@classCode",
												"OBS",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationMoodCode", new Field(
												"HeartExaminationComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/@moodCode",
												"EVN",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationTemplateIdRoot", new Field(
												"HeartExaminationComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationTemplateId", new Field(
												"HeartExaminationComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#heartExamination",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationCodeFixedCodedValue", new Field(
												"HeartExaminationComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:code/@code",
												"170203004",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"HeartExaminationComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"HeartExaminationComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:code/@displayName",
												"Heart examination",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationEffectiveTime", new Field(
												"HeartExaminationEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExamination", new Field(
												"HeartExamination",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"HeartExaminationResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HeartExaminationType", new Field(
												"HeartExaminationType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship2']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"HeartExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentTemplateId", new Field(
												"EyesExaminationComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship3",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentTypeCode", new Field(
												"EyesExaminationComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/@typeCode",
												"COMP",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentContextConductionInd", new Field(
												"EyesExaminationComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/@contextConductionInd",
												"true",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentTemplateIdRoot", new Field(
												"EyesExaminationComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentSeperatableInd", new Field(
												"EyesExaminationComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:seperatableInd/@value",
												"false",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationClassCode", new Field(
												"EyesExaminationComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/@classCode",
												"OBS",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationMoodCode", new Field(
												"EyesExaminationComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/@moodCode",
												"EVN",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationTemplateIdRoot", new Field(
												"EyesExaminationComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationTemplateId", new Field(
												"EyesExaminationComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#eyesExamination",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationCodeFixedCodedValue", new Field(
												"EyesExaminationComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:code/@code",
												"170195005",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"EyesExaminationComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"EyesExaminationComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:code/@displayName",
												"Eyes examination",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationEffectiveTime", new Field(
												"EyesExaminationEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExamination", new Field(
												"EyesExamination",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"EyesExaminationResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EyesExaminationType", new Field(
												"EyesExaminationType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship3']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"EyesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentTemplateId", new Field(
												"TestesExaminationComponentTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:templateId/@extension",
												"COCD_TP146004GB01#entryRelationship4",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentTypeCode", new Field(
												"TestesExaminationComponentTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/@typeCode",
												"COMP",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentContextConductionInd", new Field(
												"TestesExaminationComponentContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/@contextConductionInd",
												"true",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentTemplateIdRoot", new Field(
												"TestesExaminationComponentTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentSeperatableInd", new Field(
												"TestesExaminationComponentSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:seperatableInd/@value",
												"false",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationClassCode", new Field(
												"TestesExaminationComponentObservationClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/@classCode",
												"OBS",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationMoodCode", new Field(
												"TestesExaminationComponentObservationMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/@moodCode",
												"EVN",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationTemplateIdRoot", new Field(
												"TestesExaminationComponentObservationTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationTemplateId", new Field(
												"TestesExaminationComponentObservationTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:templateId/@extension",
												"COCD_TP146004GB01#testesExamination",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationCodeFixedCodedValue", new Field(
												"TestesExaminationComponentObservationCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:code/@code",
												"170209000",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"TestesExaminationComponentObservationCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"TestesExaminationComponentObservationCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:code/@displayName",
												"Testes examination",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationEffectiveTime", new Field(
												"TestesExaminationEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExamination", new Field(
												"TestesExamination",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"TestesExaminationResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TestesExaminationType", new Field(
												"TestesExaminationType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146004GB01#entryRelationship4']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"TestesExamination",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public NewBornPhysicalExamination() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public NewBornPhysicalExamination(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	
	