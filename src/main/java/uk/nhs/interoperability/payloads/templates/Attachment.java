/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Attachment object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ReferenceId</li>
 * <li>String Id</li>
 * <li>String Attachment</li>
 * <li>String AttachmentType</li>
 * <li>String MediaType</li>
 * <li>List&lt;{@link AttachmentSubject AttachmentSubject}&gt; AttachmentSubject</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Informant Informant} Informant</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} Author</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Attachment extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "Attachment";
		protected static final String shortName = "ObservationMedia";
		protected static final String rootNode = "x:observationMedia";
		protected static final String version = "COCD_TP146224GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ReferenceId ReferenceId
		 * @param Id Id
		 * @param Attachment Attachment
		 * @param AttachmentType AttachmentType
		 * @param MediaType MediaType
		 * @param AttachmentSubject AttachmentSubject
		 * @param Informant Informant
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 */
	    public Attachment(String ReferenceId, String Id, String Attachment, String AttachmentType, String MediaType, 
		List<AttachmentSubject> AttachmentSubject, uk.nhs.interoperability.payloads.templates.Informant Informant, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.Author Author) {
			fields = new LinkedHashMap<String, Object>();
			
			setReferenceId(ReferenceId);
			setId(Id);
			setAttachment(Attachment);
			setAttachmentType(AttachmentType);
			setMediaType(MediaType);
			setAttachmentSubject(AttachmentSubject);
			setInformant(Informant);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
		}
	
		/**
		 * A unique ID to identify this attachment so it can be referenced from the text section. Note: The ID cannot start with a number.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getReferenceId() {
			return (String)getValue("ReferenceId");
		}
		
		
		
		
		/**
		 * A unique ID to identify this attachment so it can be referenced from the text section. Note: The ID cannot start with a number.
		 * <br><br>This field is MANDATORY
		 * @param ReferenceId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setReferenceId(String ReferenceId) {
			setValue("ReferenceId", ReferenceId);
			return this;
		}
		
		
		/**
		 * A DCE UUID to identify this instance of an attachment
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this instance of an attachment
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * Actual attachment content. Can be plain text or base64 encoded. Please set valueType accordingly.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getAttachment() {
			return (String)getValue("Attachment");
		}
		
		
		
		
		/**
		 * Actual attachment content. Can be plain text or base64 encoded. Please set valueType accordingly.
		 * <br><br>This field is MANDATORY
		 * @param Attachment value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setAttachment(String Attachment) {
			setValue("Attachment", Attachment);
			return this;
		}
		
		
		/**
		 * Type of attachment - must be either TXT or B64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getAttachmentType() {
			return (String)getValue("AttachmentType");
		}
		
		
		
		/**
		 * Type of attachment - must be either TXT or B64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * <br><br>This field is MANDATORY
		 * @return AttachmentType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType getAttachmentTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType.getByCode((String)getValue("AttachmentType"));
		}
		
		
		/**
		 * Type of attachment - must be either TXT or B64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * <br><br>This field is MANDATORY
		 * @param AttachmentType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setAttachmentType(String AttachmentType) {
			setValue("AttachmentType", AttachmentType);
			return this;
		}
		
		
		/**
		 * Media type of attachment (e.g. text/xml or application/pdf)
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getMediaType() {
			return (String)getValue("MediaType");
		}
		
		
		
		
		/**
		 * Media type of attachment (e.g. text/xml or application/pdf)
		 * <br><br>This field is MANDATORY
		 * @param MediaType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setMediaType(String MediaType) {
			setValue("MediaType", MediaType);
			return this;
		}
		
		
		/**
		 * Subject(s) of the attachment
		 * @return List of AttachmentSubject objects
		 */
		public List<AttachmentSubject> getAttachmentSubject() {
			return (List<AttachmentSubject>)getValue("AttachmentSubject");
		}
		
		/**
		 * Subject(s) of the attachment
		 * @param AttachmentSubject value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setAttachmentSubject(List AttachmentSubject) {
			setValue("AttachmentSubject", AttachmentSubject);
			return this;
		}
		
		/**
		 * Subject(s) of the attachment
		 * <br>Note: This adds a AttachmentSubject object, but this method can be called
		 * multiple times to add additional AttachmentSubject objects.
		 * @param AttachmentSubject value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment addAttachmentSubject(AttachmentSubject AttachmentSubject) {
			addMultivalue("AttachmentSubject", AttachmentSubject);
			return this;
		}
		
		
		/**
		 * An informant is someone who informed about the information in the attachment
		 * @return uk.nhs.interoperability.payloads.templates.Informant object
		 */	
		public uk.nhs.interoperability.payloads.templates.Informant getInformant() {
			return (uk.nhs.interoperability.payloads.templates.Informant)getValue("Informant");
		}
		
		
		
		
		/**
		 * An informant is someone who informed about the information in the attachment
		 * @param Informant value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setInformant(uk.nhs.interoperability.payloads.templates.Informant Informant) {
			setValue("Informant", Informant);
			return this;
		}
		
		
		/**
		 * Time and Date that the attachment was authored
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * Time and Date that the attachment was authored
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The person or device who/which authored the information in the attachment
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("Author");
		}
		
		
		
		
		/**
		 * The person or device who/which authored the information in the attachment
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Attachment setAuthor(uk.nhs.interoperability.payloads.templates.Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferenceId", new Field(
												"ReferenceId",
												"@ID",
												"A unique ID to identify this attachment so it can be referenced from the text section. Note: The ID cannot start with a number.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146224GB02#ObservationMedia",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify this instance of an attachment",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Attachment", new Field(
												"Attachment",
												"x:value",
												"Actual attachment content. Can be plain text or base64 encoded. Please set valueType accordingly.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AttachmentXSIType", new Field(
												"AttachmentXSIType",
												"x:value/@xsi:type",
												"ED.NHS.ObservationMedia",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AttachmentType", new Field(
												"AttachmentType",
												"",
												"Type of attachment - must be either TXT or B64. Please use the AttachmentType internal vocab.",
												"true",
												"",
												"AttachmentType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AttachmentTypeTXT", new Field(
												"AttachmentTypeTXT",
												"x:value/@representation",
												"TXT",
												"",
												"",
												"AttachmentType",
												"Text",
												"",
												""
												));
	
		put("AttachmentTypeB64", new Field(
												"AttachmentTypeB64",
												"x:value/@representation",
												"B64",
												"",
												"",
												"AttachmentType",
												"Base64",
												"",
												""
												));
	
		put("MediaType", new Field(
												"MediaType",
												"x:value/@mediaType",
												"Media type of attachment (e.g. text/xml or application/pdf)",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AttachmentSubject", new Field(
												"AttachmentSubject",
												"x:subject",
												"Subject(s) of the attachment",
												"false",
												"",
												"",
												"AttachmentSubject",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformantTypeCode", new Field(
												"InformantTypeCode",
												"x:informant/@typeCode",
												"INF",
												"Informant",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformantContextControlCode", new Field(
												"InformantContextControlCode",
												"x:informant/@contextControlCode",
												"OP",
												"Informant",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformantTemplateIdRoot", new Field(
												"InformantTemplateIdRoot",
												"x:informant/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Informant",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformantTemplateId", new Field(
												"InformantTemplateId",
												"x:informant/x:templateId/@extension",
												"COCD_TP146224GB02#informant",
												"Informant",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Informant", new Field(
												"Informant",
												"x:informant/x:participant",
												"An informant is someone who informed about the information in the attachment",
												"false",
												"",
												"",
												"Informant",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:informant/x:participant/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformantContentIdRoot", new Field(
												"InformantContentIdRoot",
												"x:informant/x:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Informant",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformantContentIdExtension", new Field(
												"InformantContentIdExtension",
												"x:informant/x:contentId/@extension",
												"...",
												"Informant",
												"",
												"",
												"",
												"Informant",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146224GB02#author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/x:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentIdExtension", new Field(
												"AuthorContentIdExtension",
												"x:author/x:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												""
												));
	
		put("AuthorFunctionCode", new Field(
												"AuthorFunctionCode",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayname",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time",
												"Time and Date that the attachment was authored",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The person or device who/which authored the information in the attachment",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Attachment() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Attachment(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	