/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the NewBornHearingScreening object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ScreeningOutcome</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor ChildScreeningAuthor} Author</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} AudiologyTestFindingEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} AudiologyTestFinding</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} AudiologyReferralTime</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class NewBornHearingScreening extends AbstractPayload implements Payload , ChildScreeningCodedSections {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "NewBornHearingScreening";
		protected static final String shortName = "NewBornHearingScreening";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP146003GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param ScreeningOutcome ScreeningOutcome
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param AudiologyTestFindingEffectiveTime AudiologyTestFindingEffectiveTime
		 * @param AudiologyTestFinding AudiologyTestFinding
		 * @param AudiologyReferralTime AudiologyReferralTime
		 */
	    public NewBornHearingScreening(String Id, CodedValue ScreeningOutcome, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author, HL7Date AudiologyTestFindingEffectiveTime, CodedValue AudiologyTestFinding, HL7Date AudiologyReferralTime) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setScreeningOutcome(ScreeningOutcome);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setAudiologyTestFindingEffectiveTime(AudiologyTestFindingEffectiveTime);
			setAudiologyTestFinding(AudiologyTestFinding);
			setAudiologyReferralTime(AudiologyReferralTime);
		}
	
		/**
		 * A DCE UUID to identify each instance of a newborn hearing screening event
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of a newborn hearing screening event
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * A code from NewBornHearingScreeningOutcomeSnCT vocabulary to describe the newborn hearing screening outcome
  		 * <br>NOTE: This field should be populated using the "NewBornHearingScreeningOutcomeSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getScreeningOutcome() {
			return (CodedValue)getValue("ScreeningOutcome");
		}
		
		
		
		/**
		 * A code from NewBornHearingScreeningOutcomeSnCT vocabulary to describe the newborn hearing screening outcome
  		 * <br>NOTE: This field should be populated using the "NewBornHearingScreeningOutcomeSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT
		 * <br><br>This field is MANDATORY
		 * @return NewBornHearingScreeningOutcomeSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT getScreeningOutcomeEnum() {
			CodedValue cv = (CodedValue)getValue("ScreeningOutcome");
			VocabularyEntry entry = VocabularyFactory.getVocab("NewBornHearingScreeningOutcomeSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT)entry;
		}
		
		
		/**
		 * A code from NewBornHearingScreeningOutcomeSnCT vocabulary to describe the newborn hearing screening outcome
  		 * <br>NOTE: This field should be populated using the "NewBornHearingScreeningOutcomeSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT
		 * <br><br>This field is MANDATORY
		 * @param ScreeningOutcome value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setScreeningOutcome(CodedValue ScreeningOutcome) {
			setValue("ScreeningOutcome", ScreeningOutcome);
			return this;
		}
		
		
		/**
		 * A code from NewBornHearingScreeningOutcomeSnCT vocabulary to describe the newborn hearing screening outcome
  		 * <br>NOTE: This field should be populated using the "NewBornHearingScreeningOutcomeSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ScreeningOutcome value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setScreeningOutcome(VocabularyEntry ScreeningOutcome) {
			Code c = new CodedValue(ScreeningOutcome);
			setValue("ScreeningOutcome", c);
			return this;
		}
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor object
		 */	
		public uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setAuthor(uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The time the audiology finding was made
		 * @return HL7Date object
		 */	
		public HL7Date getAudiologyTestFindingEffectiveTime() {
			return (HL7Date)getValue("AudiologyTestFindingEffectiveTime");
		}
		
		
		
		
		/**
		 * The time the audiology finding was made
		 * @param AudiologyTestFindingEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setAudiologyTestFindingEffectiveTime(HL7Date AudiologyTestFindingEffectiveTime) {
			setValue("AudiologyTestFindingEffectiveTime", AudiologyTestFindingEffectiveTime);
			return this;
		}
		
		
		/**
		 * A code from the AudiologyTestingOutcomeStatus vocabulary to describe status of the audiology testing outcome status
  		 * <br>NOTE: This field should be populated using the "AudiologyTestingOutcomeStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getAudiologyTestFinding() {
			return (CodedValue)getValue("AudiologyTestFinding");
		}
		
		
		
		/**
		 * A code from the AudiologyTestingOutcomeStatus vocabulary to describe status of the audiology testing outcome status
  		 * <br>NOTE: This field should be populated using the "AudiologyTestingOutcomeStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus
		 * <br><br>This field is MANDATORY
		 * @return AudiologyTestingOutcomeStatus enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus getAudiologyTestFindingEnum() {
			CodedValue cv = (CodedValue)getValue("AudiologyTestFinding");
			VocabularyEntry entry = VocabularyFactory.getVocab("AudiologyTestingOutcomeStatus", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus)entry;
		}
		
		
		/**
		 * A code from the AudiologyTestingOutcomeStatus vocabulary to describe status of the audiology testing outcome status
  		 * <br>NOTE: This field should be populated using the "AudiologyTestingOutcomeStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus
		 * <br><br>This field is MANDATORY
		 * @param AudiologyTestFinding value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setAudiologyTestFinding(CodedValue AudiologyTestFinding) {
			setValue("AudiologyTestFinding", AudiologyTestFinding);
			return this;
		}
		
		
		/**
		 * A code from the AudiologyTestingOutcomeStatus vocabulary to describe status of the audiology testing outcome status
  		 * <br>NOTE: This field should be populated using the "AudiologyTestingOutcomeStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param AudiologyTestFinding value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setAudiologyTestFinding(VocabularyEntry AudiologyTestFinding) {
			Code c = new CodedValue(AudiologyTestFinding);
			setValue("AudiologyTestFinding", c);
			return this;
		}
		
		/**
		 * The time the referral for an audiology test was made
		 * @return HL7Date object
		 */	
		public HL7Date getAudiologyReferralTime() {
			return (HL7Date)getValue("AudiologyReferralTime");
		}
		
		
		
		
		/**
		 * The time the referral for an audiology test was made
		 * @param AudiologyReferralTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornHearingScreening setAudiologyReferralTime(HL7Date AudiologyReferralTime) {
			setValue("AudiologyReferralTime", AudiologyReferralTime);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146003GB01#NewBornHearingScreening",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify each instance of a newborn hearing screening event",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValue", new Field(
												"CodeFixedCodedValue",
												"x:code/@code",
												"252957005",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueCodeSystem", new Field(
												"CodeFixedCodedValueCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueDisplayName", new Field(
												"CodeFixedCodedValueDisplayName",
												"x:code/@displayName",
												"Newborn hearing screening details",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningOutcome", new Field(
												"ScreeningOutcome",
												"x:value",
												"A code from NewBornHearingScreeningOutcomeSnCT vocabulary to describe the newborn hearing screening outcome",
												"true",
												"",
												"NewBornHearingScreeningOutcomeSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningOutcomeType", new Field(
												"ScreeningOutcomeType",
												"x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146003GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"ChildScreeningAuthor",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ATFTemplateId", new Field(
												"ATFTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:templateId/@extension",
												"COCD_TP146003GB01#entryRelationship",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ATFTypeCode", new Field(
												"ATFTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/@typeCode",
												"COMP",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ATFContextConductionInd", new Field(
												"ATFContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/@contextConductionInd",
												"true",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ATFTemplateIdRoot", new Field(
												"ATFTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ATFSeperatableInd", new Field(
												"ATFSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:seperatableInd/@value",
												"false",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingClassCode", new Field(
												"AudiologyTestFindingClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/@classCode",
												"OBS",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingMoodCode", new Field(
												"AudiologyTestFindingMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/@moodCode",
												"EVN",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingTemplateIdRoot", new Field(
												"AudiologyTestFindingTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingTemplateId", new Field(
												"AudiologyTestFindingTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:templateId/@extension",
												"COCD_TP146003GB01#audiologyTestFinding",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingCodeFixedCodedValue", new Field(
												"AudiologyTestFindingCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:code/@code",
												"405180000",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingCodeFixedCodedValueCodeSystem", new Field(
												"AudiologyTestFindingCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingCodeFixedCodedValueDisplayName", new Field(
												"AudiologyTestFindingCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:code/@displayName",
												"Audiology test finding",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingStatusCode", new Field(
												"AudiologyTestFindingStatusCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:statusCode/@code",
												"completed",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingEffectiveTime", new Field(
												"AudiologyTestFindingEffectiveTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:effectiveTime/@value",
												"The time the audiology finding was made",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFinding", new Field(
												"AudiologyTestFinding",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:value",
												"A code from the AudiologyTestingOutcomeStatus vocabulary to describe status of the audiology testing outcome status",
												"true",
												"",
												"AudiologyTestingOutcomeStatus",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyTestFindingType", new Field(
												"AudiologyTestFindingType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"AudiologyTestFinding",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ARTemplateId", new Field(
												"ARTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:templateId/@extension",
												"COCD_TP146003GB01#entryRelationship2",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ARTypeCode", new Field(
												"ARTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/@typeCode",
												"COMP",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ARContextConductionInd", new Field(
												"ARContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/@contextConductionInd",
												"true",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ARTemplateIdRoot", new Field(
												"ARTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ARSeperatableInd", new Field(
												"ARSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:seperatableInd/@value",
												"false",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralClassCode", new Field(
												"AudiologyReferralClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/@classCode",
												"PROC",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralMoodCode", new Field(
												"AudiologyReferralMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/@moodCode",
												"EVN",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralTemplateIdRoot", new Field(
												"AudiologyReferralTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralTemplateId", new Field(
												"AudiologyReferralTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/x:templateId/@extension",
												"COCD_TP146003GB01#audiologyReferral",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralCodeFixedCodedValue", new Field(
												"AudiologyReferralCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/x:code/@code",
												"309619003",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralCodeFixedCodedValueCodeSystem", new Field(
												"AudiologyReferralCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralCodeFixedCodedValueDisplayName", new Field(
												"AudiologyReferralCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/x:code/@displayName",
												"Audiology referral",
												"AudiologyReferralTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AudiologyReferralTime", new Field(
												"AudiologyReferralTime",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146003GB01#entryRelationship2']/x:procedure/x:effectiveTime/@value",
												"The time the referral for an audiology test was made",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public NewBornHearingScreening() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public NewBornHearingScreening(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	