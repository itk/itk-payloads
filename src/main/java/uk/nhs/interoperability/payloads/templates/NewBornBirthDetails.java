/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the NewBornBirthDetails object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor ChildScreeningAuthor} Author</li>
 * <li>String GestationalAgeInWeeks</li>
 * <li>String BirthOrder</li>
 * <li>String NoOfFoetusInConfinement</li>
 * <li>String BirthWeightInGrams</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class NewBornBirthDetails extends AbstractPayload implements Payload , ChildScreeningCodedSections {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "NewBornBirthDetails";
		protected static final String shortName = "NewBornBirthDetails";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP000028GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param EffectiveTime EffectiveTime
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param GestationalAgeInWeeks GestationalAgeInWeeks
		 * @param BirthOrder BirthOrder
		 * @param NoOfFoetusInConfinement NoOfFoetusInConfinement
		 * @param BirthWeightInGrams BirthWeightInGrams
		 */
	    public NewBornBirthDetails(String Id, HL7Date EffectiveTime, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author, String GestationalAgeInWeeks, String BirthOrder, String NoOfFoetusInConfinement, String BirthWeightInGrams) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setEffectiveTime(EffectiveTime);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setGestationalAgeInWeeks(GestationalAgeInWeeks);
			setBirthOrder(BirthOrder);
			setNoOfFoetusInConfinement(NoOfFoetusInConfinement);
			setBirthWeightInGrams(BirthWeightInGrams);
		}
	
		/**
		 * A DCE UUID to identify each instance of the new born birth details.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of the new born birth details.
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * This field shall be populated with the time that author recorded the information
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * This field shall be populated with the time that author recorded the information
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor object
		 */	
		public uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setAuthor(uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getGestationalAgeInWeeks() {
			return (String)getValue("GestationalAgeInWeeks");
		}
		
		
		
		
		/**
		 * 
		 * @param GestationalAgeInWeeks value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setGestationalAgeInWeeks(String GestationalAgeInWeeks) {
			setValue("GestationalAgeInWeeks", GestationalAgeInWeeks);
			return this;
		}
		
		
		/**
		 * Information about birth order
		 * @return String object
		 */	
		public String getBirthOrder() {
			return (String)getValue("BirthOrder");
		}
		
		
		
		
		/**
		 * Information about birth order
		 * @param BirthOrder value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setBirthOrder(String BirthOrder) {
			setValue("BirthOrder", BirthOrder);
			return this;
		}
		
		
		/**
		 * An integer representing the number of foetuses in confinement
		 * @return String object
		 */	
		public String getNoOfFoetusInConfinement() {
			return (String)getValue("NoOfFoetusInConfinement");
		}
		
		
		
		
		/**
		 * An integer representing the number of foetuses in confinement
		 * @param NoOfFoetusInConfinement value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setNoOfFoetusInConfinement(String NoOfFoetusInConfinement) {
			setValue("NoOfFoetusInConfinement", NoOfFoetusInConfinement);
			return this;
		}
		
		
		/**
		 * Birth weight (in grams) of the baby
		 * @return String object
		 */	
		public String getBirthWeightInGrams() {
			return (String)getValue("BirthWeightInGrams");
		}
		
		
		
		
		/**
		 * Birth weight (in grams) of the baby
		 * @param BirthWeightInGrams value to set
		 * @return this To allow the use of the builder pattern
		 */
		public NewBornBirthDetails setBirthWeightInGrams(String BirthWeightInGrams) {
			setValue("BirthWeightInGrams", BirthWeightInGrams);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP000028GB01#NewBornBirthDetails",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify each instance of the new born birth details.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValue", new Field(
												"CodeFixedCodedValue",
												"x:code/@code",
												"NBBBD",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueCodeSystem", new Field(
												"CodeFixedCodedValueCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueDisplayName", new Field(
												"CodeFixedCodedValueDisplayName",
												"x:code/@displayName",
												"New Born Baby Birth Details",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"This field shall be populated with the time that author recorded the information",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP000028GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"ChildScreeningAuthor",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GATemplateId", new Field(
												"GATemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:templateId/@extension",
												"COCD_TP000028GB01#entryRelationship",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GATypeCode", new Field(
												"GATypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/@typeCode",
												"COMP",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GAContextConductionInd", new Field(
												"GAContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/@contextConductionInd",
												"true",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GATemplateIdRoot", new Field(
												"GATemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GASeperatableInd", new Field(
												"GASeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:seperatableInd/@value",
												"false",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeClassCode", new Field(
												"GestationalAgeClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/@classCode",
												"OBS",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeMoodCode", new Field(
												"GestationalAgeMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/@moodCode",
												"EVN",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeTemplateIdRoot", new Field(
												"GestationalAgeTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeTemplateId", new Field(
												"GestationalAgeTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:templateId/@extension",
												"COCD_TP000028GB01#gestationalAge",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeCodeFixedCodedValue", new Field(
												"GestationalAgeCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:code/@code",
												"GA",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeCodeFixedCodedValueCodeSystem", new Field(
												"GestationalAgeCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeCodeFixedCodedValueDisplayName", new Field(
												"GestationalAgeCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:code/@displayName",
												"Gestational age",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeCodeFixedCodedValueRef", new Field(
												"GestationalAgeCodeFixedCodedValueRef",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:code/x:originalText/x:reference/@value",
												"#a1",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInWeeksType", new Field(
												"GestationalAgeInWeeksType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:value/@xsi:type",
												"PQ",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInWeeksUnit", new Field(
												"GestationalAgeInWeeksUnit",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:value/@unit",
												"w",
												"GestationalAgeInWeeks",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GestationalAgeInWeeks", new Field(
												"GestationalAgeInWeeks",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship']/x:observation/x:value/@value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BOTemplateId", new Field(
												"BOTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:templateId/@extension",
												"COCD_TP000028GB01#entryRelationship1",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BOTypeCode", new Field(
												"BOTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/@typeCode",
												"COMP",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BOContextConductionInd", new Field(
												"BOContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/@contextConductionInd",
												"true",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BOTemplateIdRoot", new Field(
												"BOTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BOSeperatableInd", new Field(
												"BOSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:seperatableInd/@value",
												"false",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderClassCode", new Field(
												"BirthOrderClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/@classCode",
												"OBS",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderMoodCode", new Field(
												"BirthOrderMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/@moodCode",
												"EVN",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderTemplateIdRoot", new Field(
												"BirthOrderTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderTemplateId", new Field(
												"BirthOrderTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:templateId/@extension",
												"COCD_TP000028GB01#birthOrder",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderCodeFixedCodedValue", new Field(
												"BirthOrderCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:code/@code",
												"382391000000106",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderCodeFixedCodedValueCodeSystem", new Field(
												"BirthOrderCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderCodeFixedCodedValueDisplayName", new Field(
												"BirthOrderCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:code/@displayName",
												"birth order",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderCodeFixedCodedValueRef", new Field(
												"BirthOrderCodeFixedCodedValueRef",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:code/x:originalText/x:reference/@value",
												"#a2",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrder", new Field(
												"BirthOrder",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:value",
												"Information about birth order",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthOrderType", new Field(
												"BirthOrderType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship1']/x:observation/x:value/@xsi:type",
												"ST",
												"BirthOrder",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NFCTemplateId", new Field(
												"NFCTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:templateId/@extension",
												"COCD_TP000028GB01#entryRelationship2",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NFCTypeCode", new Field(
												"NFCTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/@typeCode",
												"COMP",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NFCContextConductionInd", new Field(
												"NFCContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/@contextConductionInd",
												"true",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NFCTemplateIdRoot", new Field(
												"NFCTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NFCSeperatableInd", new Field(
												"NFCSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:seperatableInd/@value",
												"false",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementClassCode", new Field(
												"NoOfFoetusInConfinementClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/@classCode",
												"OBS",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementMoodCode", new Field(
												"NoOfFoetusInConfinementMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/@moodCode",
												"EVN",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementTemplateIdRoot", new Field(
												"NoOfFoetusInConfinementTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementTemplateId", new Field(
												"NoOfFoetusInConfinementTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:templateId/@extension",
												"COCD_TP000028GB01#noOfFoetusInConfinement",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementCodeFixedCodedValue", new Field(
												"NoOfFoetusInConfinementCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:code/@code",
												"246435002",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementCodeFixedCodedValueCodeSystem", new Field(
												"NoOfFoetusInConfinementCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementCodeFixedCodedValueDisplayName", new Field(
												"NoOfFoetusInConfinementCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:code/@displayName",
												"number of foetus in confinement",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementCodeFixedCodedValueRef", new Field(
												"NoOfFoetusInConfinementCodeFixedCodedValueRef",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:code/x:originalText/x:reference/@value",
												"#a3",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinementType", new Field(
												"NoOfFoetusInConfinementType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:value/@xsi:type",
												"INT",
												"NoOfFoetusInConfinement",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NoOfFoetusInConfinement", new Field(
												"NoOfFoetusInConfinement",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship2']/x:observation/x:value/@value",
												"An integer representing the number of foetuses in confinement",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BWTemplateId", new Field(
												"BWTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:templateId/@extension",
												"COCD_TP000028GB01#entryRelationship3",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BWTypeCode", new Field(
												"BWTypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/@typeCode",
												"COMP",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BWContextConductionInd", new Field(
												"BWContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/@contextConductionInd",
												"true",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BWTemplateIdRoot", new Field(
												"BWTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BWSeperatableInd", new Field(
												"BWSeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:seperatableInd/@value",
												"false",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightClassCode", new Field(
												"BirthWeightClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/@classCode",
												"OBS",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightMoodCode", new Field(
												"BirthWeightMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/@moodCode",
												"EVN",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightTemplateIdRoot", new Field(
												"BirthWeightTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightTemplateId", new Field(
												"BirthWeightTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:templateId/@extension",
												"COCD_TP000028GB01#birthWeight",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightCodeFixedCodedValue", new Field(
												"BirthWeightCodeFixedCodedValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:code/@code",
												"364589006",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightCodeFixedCodedValueCodeSystem", new Field(
												"BirthWeightCodeFixedCodedValueCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightCodeFixedCodedValueDisplayName", new Field(
												"BirthWeightCodeFixedCodedValueDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:code/@displayName",
												"birth weight",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightCodeFixedCodedValueRef", new Field(
												"BirthWeightCodeFixedCodedValueRef",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:code/x:originalText/x:reference/@value",
												"#a4",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightInGramsType", new Field(
												"BirthWeightInGramsType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:value/@xsi:type",
												"PQ",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightInGramsUnits", new Field(
												"BirthWeightInGramsUnits",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:value/@unit",
												"g",
												"BirthWeightInGrams",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthWeightInGrams", new Field(
												"BirthWeightInGrams",
												"x:entryRelationship[x:templateId/@extension='COCD_TP000028GB01#entryRelationship3']/x:observation/x:value/@value",
												"Birth weight (in grams) of the baby",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public NewBornBirthDetails() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public NewBornBirthDetails(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	