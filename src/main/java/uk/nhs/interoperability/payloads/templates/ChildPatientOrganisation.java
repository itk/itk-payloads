/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ChildPatientOrganisation object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrganisationId</li>
 * <li>String OrganisationName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; OrganisationTelephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} OrganisationAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} OrganisationType</li>
 * <li>List&lt;{@link ChildPatientOrganisationPartOf ChildPatientOrganisationPartOf}&gt; OrganisationPartOf</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ChildPatientOrganisation extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ChildPatientOrganisation";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param OrganisationId OrganisationId
		 * @param OrganisationName OrganisationName
		 * @param OrganisationTelephone OrganisationTelephone
		 * @param OrganisationAddress OrganisationAddress
		 * @param OrganisationType OrganisationType
		 * @param OrganisationPartOf OrganisationPartOf
		 */
	    public ChildPatientOrganisation(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId, String OrganisationName, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> OrganisationTelephone, uk.nhs.interoperability.payloads.commontypes.Address OrganisationAddress, CodedValue OrganisationType, 
		List<ChildPatientOrganisationPartOf> OrganisationPartOf) {
			fields = new LinkedHashMap<String, Object>();
			
			setOrganisationId(OrganisationId);
			setOrganisationName(OrganisationName);
			setOrganisationTelephone(OrganisationTelephone);
			setOrganisationAddress(OrganisationAddress);
			setOrganisationType(OrganisationType);
			setOrganisationPartOf(OrganisationPartOf);
		}
	
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrganisationId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrganisationId");
		}
		
		
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * <br><br>This field is MANDATORY
		 * @param OrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId) {
			setValue("OrganisationId", OrganisationId);
			return this;
		}
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrganisationName() {
			return (String)getValue("OrganisationName");
		}
		
		
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @param OrganisationName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationName(String OrganisationName) {
			setValue("OrganisationName", OrganisationName);
			return this;
		}
		
		
		/**
		 * Telephone Number of the organisation
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getOrganisationTelephone() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("OrganisationTelephone");
		}
		
		/**
		 * Telephone Number of the organisation
		 * @param OrganisationTelephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationTelephone(List OrganisationTelephone) {
			setValue("OrganisationTelephone", OrganisationTelephone);
			return this;
		}
		
		/**
		 * Telephone Number of the organisation
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param OrganisationTelephone value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation addOrganisationTelephone(uk.nhs.interoperability.payloads.commontypes.Telecom OrganisationTelephone) {
			addMultivalue("OrganisationTelephone", OrganisationTelephone);
			return this;
		}
		
		
		/**
		 * A contact address for the organisation
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getOrganisationAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("OrganisationAddress");
		}
		
		
		
		
		/**
		 * A contact address for the organisation
		 * @param OrganisationAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationAddress(uk.nhs.interoperability.payloads.commontypes.Address OrganisationAddress) {
			setValue("OrganisationAddress", OrganisationAddress);
			return this;
		}
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getOrganisationType() {
			return (CodedValue)getValue("OrganisationType");
		}
		
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * @return CDAOrganizationProviderType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType getOrganisationTypeEnum() {
			CodedValue cv = (CodedValue)getValue("OrganisationType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAOrganizationProviderType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType)entry;
		}
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * @param OrganisationType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationType(CodedValue OrganisationType) {
			setValue("OrganisationType", OrganisationType);
			return this;
		}
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param OrganisationType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationType(VocabularyEntry OrganisationType) {
			Code c = new CodedValue(OrganisationType);
			setValue("OrganisationType", c);
			return this;
		}
		
		/**
		 * Information about the organisation the provider organisation is part of
		 * @return List of ChildPatientOrganisationPartOf objects
		 */
		public List<ChildPatientOrganisationPartOf> getOrganisationPartOf() {
			return (List<ChildPatientOrganisationPartOf>)getValue("OrganisationPartOf");
		}
		
		/**
		 * Information about the organisation the provider organisation is part of
		 * @param OrganisationPartOf value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation setOrganisationPartOf(List OrganisationPartOf) {
			setValue("OrganisationPartOf", OrganisationPartOf);
			return this;
		}
		
		/**
		 * Information about the organisation the provider organisation is part of
		 * <br>Note: This adds a ChildPatientOrganisationPartOf object, but this method can be called
		 * multiple times to add additional ChildPatientOrganisationPartOf objects.
		 * @param OrganisationPartOf value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientOrganisation addOrganisationPartOf(ChildPatientOrganisationPartOf OrganisationPartOf) {
			addMultivalue("OrganisationPartOf", OrganisationPartOf);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("OrganisationFieldsClassCode", new Field(
												"OrganisationFieldsClassCode",
												"@classCode",
												"ORG",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsDeterminerCode", new Field(
												"OrganisationFieldsDeterminerCode",
												"@determinerCode",
												"INSTANCE",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsTemplateIdRoot", new Field(
												"OrganisationFieldsTemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsTemplateId", new Field(
												"OrganisationFieldsTemplateId",
												"x:templateId/@extension",
												"COCD_TP145230GB02#providerOrganization",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationId", new Field(
												"OrganisationId",
												"x:id",
												"An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationName", new Field(
												"OrganisationName",
												"x:name",
												"The description of the organisation associated with the ODS code",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTelephone", new Field(
												"OrganisationTelephone",
												"x:telecom",
												"Telephone Number of the organisation",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationAddress", new Field(
												"OrganisationAddress",
												"x:addr",
												"A contact address for the organisation",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationType", new Field(
												"OrganisationType",
												"x:standardIndustryClassCode",
												"A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider",
												"true",
												"",
												"CDAOrganizationProviderType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationPartOf", new Field(
												"OrganisationPartOf",
												"x:asOrganizationPartOf",
												"Information about the organisation the provider organisation is part of",
												"false",
												"",
												"",
												"ChildPatientOrganisationPartOf",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ChildPatientOrganisation() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ChildPatientOrganisation(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	