/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PatientUniversalv2 object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses PatientIDWithTraceStatuses}&gt; PatientID</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; PatientName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Sex</li>
 * <li>String SexNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} BirthTime</li>
 * <li>String BirthTimeNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EthnicGroup</li>
 * <li>{@link PatientGuardian PatientGuardian} Guardian</li>
 * <li>List&lt;{@link Patientv2LanguageCommunication Patientv2LanguageCommunication}&gt; Languages</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrganisationId</li>
 * <li>String OrganisationName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; OrganisationTelephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} OrganisationAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} OrganisationType</li>
 * <li>List&lt;{@link PatientOrganisationPartOf PatientOrganisationPartOf}&gt; OrganisationPartOf</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PatientUniversalv2 extends AbstractPayload implements Payload , TOCPatient {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "PatientUniversalv2";
		protected static final String shortName = "PatientRole";
		protected static final String rootNode = "x:patientRole";
		protected static final String version = "COCD_TP145201GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param PatientID PatientID
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param PatientName PatientName
		 * @param Sex Sex
		 * @param SexNullFlavour SexNullFlavour
		 * @param BirthTime BirthTime
		 * @param BirthTimeNullFlavour BirthTimeNullFlavour
		 * @param EthnicGroup EthnicGroup
		 * @param Guardian Guardian
		 * @param Languages Languages
		 * @param OrganisationId OrganisationId
		 * @param OrganisationName OrganisationName
		 * @param OrganisationTelephone OrganisationTelephone
		 * @param OrganisationAddress OrganisationAddress
		 * @param OrganisationType OrganisationType
		 * @param OrganisationPartOf OrganisationPartOf
		 */
	    public PatientUniversalv2(
		List<uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses> PatientID, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> PatientName, CodedValue Sex, String SexNullFlavour, HL7Date BirthTime, String BirthTimeNullFlavour, CodedValue EthnicGroup, PatientGuardian Guardian, 
		List<Patientv2LanguageCommunication> Languages, uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId, String OrganisationName, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> OrganisationTelephone, uk.nhs.interoperability.payloads.commontypes.Address OrganisationAddress, CodedValue OrganisationType, 
		List<PatientOrganisationPartOf> OrganisationPartOf) {
			fields = new LinkedHashMap<String, Object>();
			
			setPatientID(PatientID);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setPatientName(PatientName);
			setSex(Sex);
			setSexNullFlavour(SexNullFlavour);
			setBirthTime(BirthTime);
			setBirthTimeNullFlavour(BirthTimeNullFlavour);
			setEthnicGroup(EthnicGroup);
			setGuardian(Guardian);
			setLanguages(Languages);
			setOrganisationId(OrganisationId);
			setOrganisationName(OrganisationName);
			setOrganisationTelephone(OrganisationTelephone);
			setOrganisationAddress(OrganisationAddress);
			setOrganisationType(OrganisationType);
			setOrganisationPartOf(OrganisationPartOf);
		}
	
		/**
		 * Identifier for the patient (NHS number or a local ID)
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses> getPatientID() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses>)getValue("PatientID");
		}
		
		/**
		 * Identifier for the patient (NHS number or a local ID)
		 * <br><br>This field is MANDATORY
		 * @param PatientID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setPatientID(List PatientID) {
			setValue("PatientID", PatientID);
			return this;
		}
		
		/**
		 * Identifier for the patient (NHS number or a local ID)
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses objects.
		 * @param PatientID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addPatientID(uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses PatientID) {
			addMultivalue("PatientID", PatientID);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * 
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getPatientName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("PatientName");
		}
		
		/**
		 * 
		 * @param PatientName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setPatientName(List PatientName) {
			setValue("PatientName", PatientName);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param PatientName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addPatientName(uk.nhs.interoperability.payloads.commontypes.PersonName PatientName) {
			addMultivalue("PatientName", PatientName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return CodedValue object
		 */	
		public CodedValue getSex() {
			return (CodedValue)getValue("Sex");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getSexEnum() {
			CodedValue cv = (CodedValue)getValue("Sex");
			VocabularyEntry entry = VocabularyFactory.getVocab("Sex", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Sex)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Sex value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setSex(CodedValue Sex) {
			setValue("Sex", Sex);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Sex value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setSex(VocabularyEntry Sex) {
			Code c = new CodedValue(Sex);
			setValue("Sex", c);
			return this;
		}
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getSexNullFlavour() {
			return (String)getValue("SexNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getSexNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("SexNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param SexNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setSexNullFlavour(String SexNullFlavour) {
			setValue("SexNullFlavour", SexNullFlavour);
			return this;
		}
		
		
		/**
		 * 
		 * @return HL7Date object
		 */	
		public HL7Date getBirthTime() {
			return (HL7Date)getValue("BirthTime");
		}
		
		
		
		
		/**
		 * 
		 * @param BirthTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setBirthTime(HL7Date BirthTime) {
			setValue("BirthTime", BirthTime);
			return this;
		}
		
		
		/**
		 * A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getBirthTimeNullFlavour() {
			return (String)getValue("BirthTimeNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getBirthTimeNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("BirthTimeNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param BirthTimeNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setBirthTimeNullFlavour(String BirthTimeNullFlavour) {
			setValue("BirthTimeNullFlavour", BirthTimeNullFlavour);
			return this;
		}
		
		
		/**
		 * The ethnic grouping the patient affiliates themselves to.
  		 * <br>NOTE: This field should be populated using the "Ethnicity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity
		 * @return CodedValue object
		 */	
		public CodedValue getEthnicGroup() {
			return (CodedValue)getValue("EthnicGroup");
		}
		
		
		
		/**
		 * The ethnic grouping the patient affiliates themselves to.
  		 * <br>NOTE: This field should be populated using the "Ethnicity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity
		 * @return Ethnicity enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity getEthnicGroupEnum() {
			CodedValue cv = (CodedValue)getValue("EthnicGroup");
			VocabularyEntry entry = VocabularyFactory.getVocab("Ethnicity", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity)entry;
		}
		
		
		/**
		 * The ethnic grouping the patient affiliates themselves to.
  		 * <br>NOTE: This field should be populated using the "Ethnicity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity
		 * @param EthnicGroup value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setEthnicGroup(CodedValue EthnicGroup) {
			setValue("EthnicGroup", EthnicGroup);
			return this;
		}
		
		
		/**
		 * The ethnic grouping the patient affiliates themselves to.
  		 * <br>NOTE: This field should be populated using the "Ethnicity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EthnicGroup value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setEthnicGroup(VocabularyEntry EthnicGroup) {
			Code c = new CodedValue(EthnicGroup);
			setValue("EthnicGroup", c);
			return this;
		}
		
		/**
		 * The person or organisation who/which is or acts as the patient's guardian.
		 * @return PatientGuardian object
		 */	
		public PatientGuardian getGuardian() {
			return (PatientGuardian)getValue("Guardian");
		}
		
		
		
		
		/**
		 * The person or organisation who/which is or acts as the patient's guardian.
		 * @param Guardian value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setGuardian(PatientGuardian Guardian) {
			setValue("Guardian", Guardian);
			return this;
		}
		
		
		/**
		 * Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language
		 * @return List of Patientv2LanguageCommunication objects
		 */
		public List<Patientv2LanguageCommunication> getLanguages() {
			return (List<Patientv2LanguageCommunication>)getValue("Languages");
		}
		
		/**
		 * Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language
		 * @param Languages value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setLanguages(List Languages) {
			setValue("Languages", Languages);
			return this;
		}
		
		/**
		 * Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language
		 * <br>Note: This adds a Patientv2LanguageCommunication object, but this method can be called
		 * multiple times to add additional Patientv2LanguageCommunication objects.
		 * @param Languages value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addLanguages(Patientv2LanguageCommunication Languages) {
			addMultivalue("Languages", Languages);
			return this;
		}
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrganisationId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrganisationId");
		}
		
		
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient
		 * <br><br>This field is MANDATORY
		 * @param OrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId) {
			setValue("OrganisationId", OrganisationId);
			return this;
		}
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrganisationName() {
			return (String)getValue("OrganisationName");
		}
		
		
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @param OrganisationName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationName(String OrganisationName) {
			setValue("OrganisationName", OrganisationName);
			return this;
		}
		
		
		/**
		 * Telephone Number of the organisation
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getOrganisationTelephone() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("OrganisationTelephone");
		}
		
		/**
		 * Telephone Number of the organisation
		 * @param OrganisationTelephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationTelephone(List OrganisationTelephone) {
			setValue("OrganisationTelephone", OrganisationTelephone);
			return this;
		}
		
		/**
		 * Telephone Number of the organisation
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param OrganisationTelephone value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addOrganisationTelephone(uk.nhs.interoperability.payloads.commontypes.Telecom OrganisationTelephone) {
			addMultivalue("OrganisationTelephone", OrganisationTelephone);
			return this;
		}
		
		
		/**
		 * A contact address for the organisation
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getOrganisationAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("OrganisationAddress");
		}
		
		
		
		
		/**
		 * A contact address for the organisation
		 * @param OrganisationAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationAddress(uk.nhs.interoperability.payloads.commontypes.Address OrganisationAddress) {
			setValue("OrganisationAddress", OrganisationAddress);
			return this;
		}
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getOrganisationType() {
			return (CodedValue)getValue("OrganisationType");
		}
		
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * @return CDAOrganizationProviderType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType getOrganisationTypeEnum() {
			CodedValue cv = (CodedValue)getValue("OrganisationType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAOrganizationProviderType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType)entry;
		}
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * @param OrganisationType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationType(CodedValue OrganisationType) {
			setValue("OrganisationType", OrganisationType);
			return this;
		}
		
		
		/**
		 * A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationProviderType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param OrganisationType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationType(VocabularyEntry OrganisationType) {
			Code c = new CodedValue(OrganisationType);
			setValue("OrganisationType", c);
			return this;
		}
		
		/**
		 * Information about the organisation the provider organisation is part of
		 * @return List of PatientOrganisationPartOf objects
		 */
		public List<PatientOrganisationPartOf> getOrganisationPartOf() {
			return (List<PatientOrganisationPartOf>)getValue("OrganisationPartOf");
		}
		
		/**
		 * Information about the organisation the provider organisation is part of
		 * @param OrganisationPartOf value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 setOrganisationPartOf(List OrganisationPartOf) {
			setValue("OrganisationPartOf", OrganisationPartOf);
			return this;
		}
		
		/**
		 * Information about the organisation the provider organisation is part of
		 * <br>Note: This adds a PatientOrganisationPartOf object, but this method can be called
		 * multiple times to add additional PatientOrganisationPartOf objects.
		 * @param OrganisationPartOf value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversalv2 addOrganisationPartOf(PatientOrganisationPartOf OrganisationPartOf) {
			addMultivalue("OrganisationPartOf", OrganisationPartOf);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PAT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145201GB02#PatientRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientID", new Field(
												"PatientID",
												"x:id",
												"Identifier for the patient (NHS number or a local ID)",
												"true",
												"",
												"",
												"PatientIDWithTraceStatuses",
												"uk.nhs.interoperability.payloads.commontypes.",
												"5",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientClassCode", new Field(
												"PatientClassCode",
												"x:patient/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientDeterminerCode", new Field(
												"PatientDeterminerCode",
												"x:patient/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTemplateIdRoot", new Field(
												"PatientTemplateIdRoot",
												"x:patient/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTemplateId", new Field(
												"PatientTemplateId",
												"x:patient/x:templateId/@extension",
												"COCD_TP145201GB02#patientPatient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientName", new Field(
												"PatientName",
												"x:patient/x:name",
												"",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Sex", new Field(
												"Sex",
												"x:patient/x:administrativeGenderCode",
												"",
												"false",
												"",
												"Sex",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SexNullFlavour", new Field(
												"SexNullFlavour",
												"x:patient/x:administrativeGenderCode/@nullFlavor",
												"A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthTime", new Field(
												"BirthTime",
												"x:patient/x:birthTime/@value",
												"",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthTimeNullFlavour", new Field(
												"BirthTimeNullFlavour",
												"x:patient/x:birthTime/@nullFlavor",
												"A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EthnicGroup", new Field(
												"EthnicGroup",
												"x:patient/x:ethnicGroupCode",
												"The ethnic grouping the patient affiliates themselves to.",
												"false",
												"",
												"Ethnicity",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Guardian", new Field(
												"Guardian",
												"x:patient/x:guardian",
												"The person or organisation who/which is or acts as the patient's guardian.",
												"false",
												"",
												"",
												"PatientGuardian",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Languages", new Field(
												"Languages",
												"x:patient/x:languageCommunication",
												"Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language",
												"false",
												"",
												"",
												"Patientv2LanguageCommunication",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsClassCode", new Field(
												"OrganisationFieldsClassCode",
												"x:providerOrganization/@classCode",
												"ORG",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsDeterminerCode", new Field(
												"OrganisationFieldsDeterminerCode",
												"x:providerOrganization/@determinerCode",
												"INSTANCE",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsTemplateIdRoot", new Field(
												"OrganisationFieldsTemplateIdRoot",
												"x:providerOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationFieldsTemplateId", new Field(
												"OrganisationFieldsTemplateId",
												"x:providerOrganization/x:templateId/@extension",
												"COCD_TP145201GB02#providerOrganization",
												"OrganisationName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationId", new Field(
												"OrganisationId",
												"x:providerOrganization/x:id",
												"An ODS code as an identifier that uniquely identifies the organisation which provides/provided care to the patient",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationName", new Field(
												"OrganisationName",
												"x:providerOrganization/x:name",
												"The description of the organisation associated with the ODS code",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTelephone", new Field(
												"OrganisationTelephone",
												"x:providerOrganization/x:telecom",
												"Telephone Number of the organisation",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationAddress", new Field(
												"OrganisationAddress",
												"x:providerOrganization/x:addr",
												"A contact address for the organisation",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationType", new Field(
												"OrganisationType",
												"x:providerOrganization/x:standardIndustryClassCode",
												"A code taken from the CDAOrganizationProviderType vocabulary to indicate the type of care provider",
												"true",
												"",
												"CDAOrganizationProviderType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationPartOf", new Field(
												"OrganisationPartOf",
												"x:providerOrganization/x:asOrganizationPartOf",
												"Information about the organisation the provider organisation is part of",
												"false",
												"",
												"",
												"PatientOrganisationPartOf",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PatientUniversalv2() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PatientUniversalv2(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	
	
	