/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Section5 object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String SectionId</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CDADocumentSectionType</li>
 * <li>String Title</li>
 * <li>String Text</li>
 * <li>List&lt;{@link Section6 Section6}&gt; Section6</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Section5 extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "Section5";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param SectionId String
		 * @param Title String
		 * @param Text XML
		 */
		public Section5(String SectionId, String Title, String Text) {
			fields = new LinkedHashMap<String, Object>();
			
			setSectionId(SectionId);
			setTitle(Title);
			setText(Text);
		}

		/**
		 * A DCE UUID to identify each unique instance of a section
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getSectionId() {
			return (String)getValue("SectionId");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each unique instance of a section
		 * <br><br>This field is MANDATORY
		 * @param SectionId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 setSectionId(String SectionId) {
			setValue("SectionId", SectionId);
			return this;
		}
		
		
		/**
		 * 
		 * @return CodedValue object
		 */	
		public CodedValue getCDADocumentSectionType() {
			return (CodedValue)getValue("CDADocumentSectionType");
		}
		
		
		
		
		/**
		 * 
		 * @param CDADocumentSectionType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 setCDADocumentSectionType(CodedValue CDADocumentSectionType) {
			setValue("CDADocumentSectionType", CDADocumentSectionType);
			return this;
		}
		
		
		/**
		 * 
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CDADocumentSectionType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 setCDADocumentSectionType(VocabularyEntry CDADocumentSectionType) {
			Code c = new CodedValue(CDADocumentSectionType);
			setValue("CDADocumentSectionType", c);
			return this;
		}
		
		/**
		 * Heading for the section
		 * @return String object
		 */	
		public String getTitle() {
			return (String)getValue("Title");
		}
		
		
		
		
		/**
		 * Heading for the section
		 * @param Title value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 setTitle(String Title) {
			setValue("Title", Title);
			return this;
		}
		
		
		/**
		 * Text content of section
		 * @return String object
		 */	
		public String getText() {
			return (String)getValue("Text");
		}
		
		
		
		
		/**
		 * Text content of section
		 * @param Text value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 setText(String Text) {
			setValue("Text", Text);
			return this;
		}
		
		
		/**
		 * Subsection (optional)
		 * @return List of Section6 objects
		 */
		public List<Section6> getSection6() {
			return (List<Section6>)getValue("Section6");
		}
		
		/**
		 * Subsection (optional)
		 * @param Section6 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 setSection6(List Section6) {
			setValue("Section6", Section6);
			return this;
		}
		
		/**
		 * Subsection (optional)
		 * <br>Note: This adds a Section6 object, but this method can be called
		 * multiple times to add additional Section6 objects.
		 * @param Section6 value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Section5 addSection6(Section6 Section6) {
			addMultivalue("Section6", Section6);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ComponentTypeCode", new Field(
												"ComponentTypeCode",
												"@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContextConductionInd", new Field(
												"ContextConductionInd",
												"@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ComponentTemplateIdRoot", new Field(
												"ComponentTemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ComponentTemplateId", new Field(
												"ComponentTemplateId",
												"x:templateId/@extension",
												"COCD_TP146229GB01#component4",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassCode", new Field(
												"ClassCode",
												"x:section/@classCode",
												"DOCSECT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"x:section/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:section/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:section/x:templateId/@extension",
												"COCD_TP146229GB01#section5",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SectionId", new Field(
												"SectionId",
												"x:section/x:id/@root",
												"A DCE UUID to identify each unique instance of a section",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDADocumentSectionType", new Field(
												"CDADocumentSectionType",
												"x:section/x:code",
												"",
												"false",
												"",
												"",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.330",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Title", new Field(
												"Title",
												"x:section/x:title",
												"Heading for the section",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Text", new Field(
												"Text",
												"x:section/x:text",
												"Text content of section",
												"false",
												"",
												"",
												"XML",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Section6", new Field(
												"Section6",
												"x:section/x:component",
												"Subsection (optional)",
												"false",
												"",
												"",
												"Section6",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Section5() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Section5(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	