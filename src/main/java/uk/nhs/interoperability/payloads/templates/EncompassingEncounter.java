/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the EncompassingEncounter object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Code</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.DateRange DateRange} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} DischargeDispositionCode</li>
 * <li>List&lt;{@link EncounterParticipant EncounterParticipant}&gt; Participant</li>
 * <li>{@link ResponsibleParty ResponsibleParty} ResponsibleParty</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.CodedValue CodedValue}&gt; EncounterServiceDeliveryLocationId</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EncounterCareSettingType</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} EncounterOrgId</li>
 * <li>String EncounterOrgName</li>
 * <li>String EncounterPlaceName</li>
 * <li>String EncounterPlaceNameNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} EncounterPlaceAddress</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class EncompassingEncounter extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "EncompassingEncounter";
		protected static final String shortName = "EncompassingEncounter";
		protected static final String rootNode = "x:encompassingEncounter";
		protected static final String version = "COCD_TP146228GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param Code Code
		 * @param EffectiveTime EffectiveTime
		 * @param DischargeDispositionCode DischargeDispositionCode
		 * @param Participant Participant
		 * @param ResponsibleParty ResponsibleParty
		 * @param EncounterServiceDeliveryLocationId EncounterServiceDeliveryLocationId
		 * @param EncounterCareSettingType EncounterCareSettingType
		 * @param EncounterOrgId EncounterOrgId
		 * @param EncounterOrgName EncounterOrgName
		 * @param EncounterPlaceName EncounterPlaceName
		 * @param EncounterPlaceNameNullFlavour EncounterPlaceNameNullFlavour
		 * @param EncounterPlaceAddress EncounterPlaceAddress
		 */
	    public EncompassingEncounter(String Id, CodedValue Code, uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime, CodedValue DischargeDispositionCode, 
		List<EncounterParticipant> Participant, ResponsibleParty ResponsibleParty, 
		List<CodedValue> EncounterServiceDeliveryLocationId, CodedValue EncounterCareSettingType, uk.nhs.interoperability.payloads.commontypes.OrgID EncounterOrgId, String EncounterOrgName, String EncounterPlaceName, String EncounterPlaceNameNullFlavour, uk.nhs.interoperability.payloads.commontypes.Address EncounterPlaceAddress) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setCode(Code);
			setEffectiveTime(EffectiveTime);
			setDischargeDispositionCode(DischargeDispositionCode);
			setParticipant(Participant);
			setResponsibleParty(ResponsibleParty);
			setEncounterServiceDeliveryLocationId(EncounterServiceDeliveryLocationId);
			setEncounterCareSettingType(EncounterCareSettingType);
			setEncounterOrgId(EncounterOrgId);
			setEncounterOrgName(EncounterOrgName);
			setEncounterPlaceName(EncounterPlaceName);
			setEncounterPlaceNameNullFlavour(EncounterPlaceNameNullFlavour);
			setEncounterPlaceAddress(EncounterPlaceAddress);
		}
	
		/**
		 * A UUID to identify each unique instance of an encompassing encounter.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A UUID to identify each unique instance of an encompassing encounter.
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * Uses any vocabulary to describe the type of encompassing encounter. If using a SNOMED CT code, then it should come from the CDA Enounter Type SNOMED CT subset (1341000000130).
  		 * <br>NOTE: This field should be populated using the "CDAEncounterType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getCode() {
			return (CodedValue)getValue("Code");
		}
		
		
		
		/**
		 * Uses any vocabulary to describe the type of encompassing encounter. If using a SNOMED CT code, then it should come from the CDA Enounter Type SNOMED CT subset (1341000000130).
  		 * <br>NOTE: This field should be populated using the "CDAEncounterType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType
		 * <br><br>This field is MANDATORY
		 * @return CDAEncounterType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType getCodeEnum() {
			CodedValue cv = (CodedValue)getValue("Code");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAEncounterType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType)entry;
		}
		
		
		/**
		 * Uses any vocabulary to describe the type of encompassing encounter. If using a SNOMED CT code, then it should come from the CDA Enounter Type SNOMED CT subset (1341000000130).
  		 * <br>NOTE: This field should be populated using the "CDAEncounterType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType
		 * <br><br>This field is MANDATORY
		 * @param Code value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setCode(CodedValue Code) {
			setValue("Code", Code);
			return this;
		}
		
		
		/**
		 * Uses any vocabulary to describe the type of encompassing encounter. If using a SNOMED CT code, then it should come from the CDA Enounter Type SNOMED CT subset (1341000000130).
  		 * <br>NOTE: This field should be populated using the "CDAEncounterType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Code value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setCode(VocabularyEntry Code) {
			Code c = new CodedValue(Code);
			setValue("Code", c);
			return this;
		}
		
		/**
		 * The time associated with / relevant to the encounter
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.DateRange object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.DateRange getEffectiveTime() {
			return (uk.nhs.interoperability.payloads.commontypes.DateRange)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * The time associated with / relevant to the encounter
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEffectiveTime(uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * a code from any vocabulary to describe how the encompassing encounter ended. In the absence of any local vocabulary then a SNOMED CT code from the EncounterDispositionSnCT vocabulary can be used to describe how the encompassing encounter ended. This attribute should only be used when there is a business concept of discharge.
  		 * <br>NOTE: This field should be populated using the "Encounterdisposition" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition
		 * @return CodedValue object
		 */	
		public CodedValue getDischargeDispositionCode() {
			return (CodedValue)getValue("DischargeDispositionCode");
		}
		
		
		
		/**
		 * a code from any vocabulary to describe how the encompassing encounter ended. In the absence of any local vocabulary then a SNOMED CT code from the EncounterDispositionSnCT vocabulary can be used to describe how the encompassing encounter ended. This attribute should only be used when there is a business concept of discharge.
  		 * <br>NOTE: This field should be populated using the "Encounterdisposition" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition
		 * @return Encounterdisposition enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition getDischargeDispositionCodeEnum() {
			CodedValue cv = (CodedValue)getValue("DischargeDispositionCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("Encounterdisposition", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition)entry;
		}
		
		
		/**
		 * a code from any vocabulary to describe how the encompassing encounter ended. In the absence of any local vocabulary then a SNOMED CT code from the EncounterDispositionSnCT vocabulary can be used to describe how the encompassing encounter ended. This attribute should only be used when there is a business concept of discharge.
  		 * <br>NOTE: This field should be populated using the "Encounterdisposition" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition
		 * @param DischargeDispositionCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setDischargeDispositionCode(CodedValue DischargeDispositionCode) {
			setValue("DischargeDispositionCode", DischargeDispositionCode);
			return this;
		}
		
		
		/**
		 * a code from any vocabulary to describe how the encompassing encounter ended. In the absence of any local vocabulary then a SNOMED CT code from the EncounterDispositionSnCT vocabulary can be used to describe how the encompassing encounter ended. This attribute should only be used when there is a business concept of discharge.
  		 * <br>NOTE: This field should be populated using the "Encounterdisposition" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param DischargeDispositionCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setDischargeDispositionCode(VocabularyEntry DischargeDispositionCode) {
			Code c = new CodedValue(DischargeDispositionCode);
			setValue("DischargeDispositionCode", c);
			return this;
		}
		
		/**
		 * Details of how the various people and organisations were involved in the encompassing encounter
		 * @return List of EncounterParticipant objects
		 */
		public List<EncounterParticipant> getParticipant() {
			return (List<EncounterParticipant>)getValue("Participant");
		}
		
		/**
		 * Details of how the various people and organisations were involved in the encompassing encounter
		 * @param Participant value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setParticipant(List Participant) {
			setValue("Participant", Participant);
			return this;
		}
		
		/**
		 * Details of how the various people and organisations were involved in the encompassing encounter
		 * <br>Note: This adds a EncounterParticipant object, but this method can be called
		 * multiple times to add additional EncounterParticipant objects.
		 * @param Participant value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter addParticipant(EncounterParticipant Participant) {
			addMultivalue("Participant", Participant);
			return this;
		}
		
		
		/**
		 * A template describing the responsible party
		 * @return ResponsibleParty object
		 */	
		public ResponsibleParty getResponsibleParty() {
			return (ResponsibleParty)getValue("ResponsibleParty");
		}
		
		
		
		
		/**
		 * A template describing the responsible party
		 * @param ResponsibleParty value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setResponsibleParty(ResponsibleParty ResponsibleParty) {
			setValue("ResponsibleParty", ResponsibleParty);
			return this;
		}
		
		
		/**
		 * Local identifiers to identify the service delivery location
		 * @return List of CodedValue objects
		 */
		public List<CodedValue> getEncounterServiceDeliveryLocationId() {
			return (List<CodedValue>)getValue("EncounterServiceDeliveryLocationId");
		}
		
		/**
		 * Local identifiers to identify the service delivery location
		 * @param EncounterServiceDeliveryLocationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterServiceDeliveryLocationId(List EncounterServiceDeliveryLocationId) {
			setValue("EncounterServiceDeliveryLocationId", EncounterServiceDeliveryLocationId);
			return this;
		}
		
		/**
		 * Local identifiers to identify the service delivery location
		 * <br>Note: This adds a CodedValue object, but this method can be called
		 * multiple times to add additional CodedValue objects.
		 * @param EncounterServiceDeliveryLocationId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter addEncounterServiceDeliveryLocationId(CodedValue EncounterServiceDeliveryLocationId) {
			addMultivalue("EncounterServiceDeliveryLocationId", EncounterServiceDeliveryLocationId);
			return this;
		}
		
		
		/**
		 * Local identifiers to identify the service delivery location
		 * <br>Note: This adds a coded entry from a specific vocabulary, but this method can be called
		 * multiple times to add additional coded entries objects.
		 * @param EncounterServiceDeliveryLocationId value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter addEncounterServiceDeliveryLocationId(VocabularyEntry EncounterServiceDeliveryLocationId) {
			Code c = new CodedValue(EncounterServiceDeliveryLocationId);
			addMultivalue("EncounterServiceDeliveryLocationId", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getEncounterCareSettingType() {
			return (CodedValue)getValue("EncounterCareSettingType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * @return CDACareSettingType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType getEncounterCareSettingTypeEnum() {
			CodedValue cv = (CodedValue)getValue("EncounterCareSettingType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDACareSettingType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * @param EncounterCareSettingType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterCareSettingType(CodedValue EncounterCareSettingType) {
			setValue("EncounterCareSettingType", EncounterCareSettingType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDACareSettingType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EncounterCareSettingType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterCareSettingType(VocabularyEntry EncounterCareSettingType) {
			Code c = new CodedValue(EncounterCareSettingType);
			setValue("EncounterCareSettingType", c);
			return this;
		}
		
		/**
		 * Identifier that uniquely identifies the organisation which is responsible for the service delivery location.
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getEncounterOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("EncounterOrgId");
		}
		
		
		
		
		/**
		 * Identifier that uniquely identifies the organisation which is responsible for the service delivery location.
		 * <br><br>This field is MANDATORY
		 * @param EncounterOrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID EncounterOrgId) {
			setValue("EncounterOrgId", EncounterOrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getEncounterOrgName() {
			return (String)getValue("EncounterOrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param EncounterOrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterOrgName(String EncounterOrgName) {
			setValue("EncounterOrgName", EncounterOrgName);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getEncounterPlaceName() {
			return (String)getValue("EncounterPlaceName");
		}
		
		
		
		
		/**
		 * 
		 * @param EncounterPlaceName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterPlaceName(String EncounterPlaceName) {
			setValue("EncounterPlaceName", EncounterPlaceName);
			return this;
		}
		
		
		/**
		 * A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getEncounterPlaceNameNullFlavour() {
			return (String)getValue("EncounterPlaceNameNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getEncounterPlaceNameNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("EncounterPlaceNameNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param EncounterPlaceNameNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterPlaceNameNullFlavour(String EncounterPlaceNameNullFlavour) {
			setValue("EncounterPlaceNameNullFlavour", EncounterPlaceNameNullFlavour);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getEncounterPlaceAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("EncounterPlaceAddress");
		}
		
		
		
		
		/**
		 * 
		 * @param EncounterPlaceAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EncompassingEncounter setEncounterPlaceAddress(uk.nhs.interoperability.payloads.commontypes.Address EncounterPlaceAddress) {
			setValue("EncounterPlaceAddress", EncounterPlaceAddress);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ENC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146228GB01#EncompassingEncounter",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A UUID to identify each unique instance of an encompassing encounter.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Code", new Field(
												"Code",
												"x:code",
												"Uses any vocabulary to describe the type of encompassing encounter. If using a SNOMED CT code, then it should come from the CDA Enounter Type SNOMED CT subset (1341000000130).",
												"true",
												"",
												"CDAEncounterType",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.413",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime",
												"The time associated with / relevant to the encounter",
												"true",
												"",
												"",
												"DateRange",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DischargeDispositionCode", new Field(
												"DischargeDispositionCode",
												"x:dischargeDispositionCode",
												"a code from any vocabulary to describe how the encompassing encounter ended. In the absence of any local vocabulary then a SNOMED CT code from the EncounterDispositionSnCT vocabulary can be used to describe how the encompassing encounter ended. This attribute should only be used when there is a business concept of discharge.",
												"false",
												"",
												"Encounterdisposition",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DischargeDispositionCodeNullFlavour", new Field(
												"DischargeDispositionCodeNullFlavour",
												"x:dischargeDispositionCode/@nullFlavor",
												"NA",
												"",
												"DischargeDispositionCode",
												"",
												"",
												"",
												""
												));
	
		put("Participant", new Field(
												"Participant",
												"x:encounterParticipant",
												"Details of how the various people and organisations were involved in the encompassing encounter",
												"false",
												"",
												"",
												"EncounterParticipant",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsiblePartyTypeCode", new Field(
												"ResponsiblePartyTypeCode",
												"x:responsibleParty/@typeCode",
												"RESP",
												"ResponsibleParty",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsiblePartyTemplateIdRoot", new Field(
												"ResponsiblePartyTemplateIdRoot",
												"x:responsibleParty/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ResponsibleParty",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsiblePartyTemplateId", new Field(
												"ResponsiblePartyTemplateId",
												"x:responsibleParty/x:templateId/@extension",
												"COCD_TP146228GB01#responsibleParty",
												"ResponsibleParty",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsiblePartyContentIdRoot", new Field(
												"ResponsiblePartyContentIdRoot",
												"x:responsibleParty/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"ResponsibleParty",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsiblePartyContentId", new Field(
												"ResponsiblePartyContentId",
												"x:responsibleParty/npfitlc:contentId/@extension",
												"...",
												"ResponsibleParty",
												"",
												"",
												"",
												"ResponsibleParty",
												""
												));
	
		put("ResponsibleParty", new Field(
												"ResponsibleParty",
												"x:responsibleParty/x:assignedEntity",
												"A template describing the responsible party",
												"false",
												"",
												"",
												"ResponsibleParty",
												"",
												"",
												"",
												"",
												"x:responsibleParty/x:assignedEntity/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterLocationTypeCode", new Field(
												"EncounterLocationTypeCode",
												"x:location/@typeCode",
												"LOC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterLocationTemplateIdRoot", new Field(
												"EncounterLocationTemplateIdRoot",
												"x:location/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterLocationTemplateId", new Field(
												"EncounterLocationTemplateId",
												"x:location/x:templateId/@extension",
												"COCD_TP146228GB01#location",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterLocationContentIdRoot", new Field(
												"EncounterLocationContentIdRoot",
												"x:location/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterLocationContentId", new Field(
												"EncounterLocationContentId",
												"x:location/npfitlc:contentId/@extension",
												"COCD_TP145211GB01#HealthCareFacility",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterTemplateIdRoot", new Field(
												"EncounterTemplateIdRoot",
												"x:location/x:healthCareFacility/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterTemplateId", new Field(
												"EncounterTemplateId",
												"x:location/x:healthCareFacility/x:templateId/@extension",
												"COCD_TP145211GB01#HealthCareFacility",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterClassCode", new Field(
												"EncounterClassCode",
												"x:location/x:healthCareFacility/@classCode",
												"SDLOC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterServiceDeliveryLocationId", new Field(
												"EncounterServiceDeliveryLocationId",
												"x:location/x:healthCareFacility/x:id",
												"Local identifiers to identify the service delivery location",
												"false",
												"",
												"",
												"CodedValue",
												"",
												"100",
												"2.16.840.1.113883.2.1.3.2.4.18.38",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterCareSettingType", new Field(
												"EncounterCareSettingType",
												"x:location/x:healthCareFacility/x:code",
												"",
												"true",
												"",
												"CDACareSettingType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterOrgClassCode", new Field(
												"EncounterOrgClassCode",
												"x:location/x:healthCareFacility/x:serviceProviderOrganization/@classCode",
												"ORG",
												"EncounterOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterOrgDeterminerCode", new Field(
												"EncounterOrgDeterminerCode",
												"x:location/x:healthCareFacility/x:serviceProviderOrganization/@determinerCode",
												"INSTANCE",
												"EncounterOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterOrgTemplateIdRoot", new Field(
												"EncounterOrgTemplateIdRoot",
												"x:location/x:healthCareFacility/x:serviceProviderOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"EncounterOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterOrgTemplateId", new Field(
												"EncounterOrgTemplateId",
												"x:location/x:healthCareFacility/x:serviceProviderOrganization/x:templateId/@extension",
												"COCD_TP145211GB01#serviceProviderOrganization",
												"EncounterOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterOrgId", new Field(
												"EncounterOrgId",
												"x:location/x:healthCareFacility/x:serviceProviderOrganization/x:id",
												"Identifier that uniquely identifies the organisation which is responsible for the service delivery location.",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterOrgName", new Field(
												"EncounterOrgName",
												"x:location/x:healthCareFacility/x:serviceProviderOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceClassCode", new Field(
												"EncounterPlaceClassCode",
												"x:location/x:healthCareFacility/x:location/@classCode",
												"PLC",
												"EncounterPlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceDeterminerCode", new Field(
												"EncounterPlaceDeterminerCode",
												"x:location/x:healthCareFacility/x:location/@determinerCode",
												"INSTANCE",
												"EncounterPlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceTemplateIdRoot", new Field(
												"EncounterPlaceTemplateIdRoot",
												"x:location/x:healthCareFacility/x:location/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"EncounterPlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceTemplateId", new Field(
												"EncounterPlaceTemplateId",
												"x:location/x:healthCareFacility/x:location/x:templateId/@extension",
												"COCD_TP145211GB01#location",
												"EncounterPlaceAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceName", new Field(
												"EncounterPlaceName",
												"x:location/x:healthCareFacility/x:location/x:name",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceNameNullFlavour", new Field(
												"EncounterPlaceNameNullFlavour",
												"x:location/x:healthCareFacility/x:location/x:name/@nullFlavor",
												"A null flavour can be used when the place name cannot be provided. If an address is provided a null flavour of NA will be used automatically where there is no place name.",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceNameNullFlavourWithAddress", new Field(
												"EncounterPlaceNameNullFlavourWithAddress",
												"x:location/x:healthCareFacility/x:location/x:name/@nullFlavor",
												"NA",
												"EncounterPlaceAddress",
												"EncounterPlaceName",
												"",
												"",
												"",
												""
												));
	
		put("EncounterPlaceAddress", new Field(
												"EncounterPlaceAddress",
												"x:location/x:healthCareFacility/x:location/x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public EncompassingEncounter() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public EncompassingEncounter(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	