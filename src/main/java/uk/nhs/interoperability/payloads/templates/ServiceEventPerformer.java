/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ServiceEventPerformer object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String PerformerType</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} FunctionCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.DateRange DateRange} Time</li>
 * <li>{@link Performer Performer} Performer</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ServiceEventPerformer extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ServiceEventPerformer";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param Performer Performer
		 * @param PerformerType String
		 */
		public ServiceEventPerformer(Performer Performer, String PerformerType) {
			fields = new LinkedHashMap<String, Object>();
			
			setPerformer(Performer);
			setPerformerType(PerformerType);
		}

		/**
		 * @param Performer Performer
		 * @param PerformerType String
		 * @param Time DateRange
		 */
		public ServiceEventPerformer(Performer Performer, String PerformerType, uk.nhs.interoperability.payloads.commontypes.DateRange Time) {
			fields = new LinkedHashMap<String, Object>();
			
			setPerformer(Performer);
			setPerformerType(PerformerType);
			setTime(Time);
		}

		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7PerformerType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getPerformerType() {
			return (String)getValue("PerformerType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7PerformerType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType
		 * <br><br>This field is MANDATORY
		 * @return HL7PerformerType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType getPerformerTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType.getByCode((String)getValue("PerformerType"));
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7PerformerType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType
		 * <br><br>This field is MANDATORY
		 * @param PerformerType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEventPerformer setPerformerType(String PerformerType) {
			setValue("PerformerType", PerformerType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * @return CodedValue object
		 */	
		public CodedValue getFunctionCode() {
			return (CodedValue)getValue("FunctionCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * @return ParticipationFunction enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction getFunctionCodeEnum() {
			CodedValue cv = (CodedValue)getValue("FunctionCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ParticipationFunction", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * @param FunctionCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEventPerformer setFunctionCode(CodedValue FunctionCode) {
			setValue("FunctionCode", FunctionCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param FunctionCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEventPerformer setFunctionCode(VocabularyEntry FunctionCode) {
			Code c = new CodedValue(FunctionCode);
			setValue("FunctionCode", c);
			return this;
		}
		
		/**
		 * The time during which the performer performed (was involved) in the service event.
		 * @return uk.nhs.interoperability.payloads.commontypes.DateRange object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.DateRange getTime() {
			return (uk.nhs.interoperability.payloads.commontypes.DateRange)getValue("Time");
		}
		
		
		
		
		/**
		 * The time during which the performer performed (was involved) in the service event.
		 * @param Time value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEventPerformer setTime(uk.nhs.interoperability.payloads.commontypes.DateRange Time) {
			setValue("Time", Time);
			return this;
		}
		
		
		/**
		 * A care professional involved with carrying out the service.
		 * @return Performer object
		 */	
		public Performer getPerformer() {
			return (Performer)getValue("Performer");
		}
		
		
		
		
		/**
		 * A care professional involved with carrying out the service.
		 * @param Performer value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEventPerformer setPerformer(Performer Performer) {
			setValue("Performer", Performer);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("PerformerType", new Field(
												"PerformerType",
												"@typeCode",
												"",
												"true",
												"",
												"HL7PerformerType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146227GB02#performer",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdRoot", new Field(
												"ContentIdRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentId", new Field(
												"ContentId",
												"npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"Performer",
												""
												));
	
		put("FunctionCode", new Field(
												"FunctionCode",
												"x:functionCode",
												"",
												"false",
												"",
												"ParticipationFunction",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.412",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Time", new Field(
												"Time",
												"x:time",
												"The time during which the performer performed (was involved) in the service event.",
												"false",
												"",
												"",
												"DateRange",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Performer", new Field(
												"Performer",
												"x:assignedEntity",
												"A care professional involved with carrying out the service.",
												"false",
												"",
												"",
												"Performer",
												"",
												"",
												"",
												"",
												"x:assignedEntity/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ServiceEventPerformer() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ServiceEventPerformer(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	