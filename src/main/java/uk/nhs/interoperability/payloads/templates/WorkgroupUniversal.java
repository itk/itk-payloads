/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the WorkgroupUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.WorkgroupID WorkgroupID} ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} WorkgroupName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} PersonName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class WorkgroupUniversal extends AbstractPayload implements Payload , Author, Performer, EncounterParticipant, ResponsibleParty {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "WorkgroupUniversal";
		protected static final String shortName = "Workgroup";
		protected static final String rootNode = "";
		protected static final String version = "COCD_TP145212GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param WorkgroupName WorkgroupName
		 * @param TelephoneNumber TelephoneNumber
		 * @param PersonName PersonName
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 */
	    public WorkgroupUniversal(uk.nhs.interoperability.payloads.commontypes.WorkgroupID ID, CodedValue WorkgroupName, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName PersonName, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setWorkgroupName(WorkgroupName);
			setTelephoneNumber(TelephoneNumber);
			setPersonName(PersonName);
			setOrgId(OrgId);
			setOrgName(OrgName);
		}
	
		/**
		 * An identifier to identify the workgroup or team
		 * @return uk.nhs.interoperability.payloads.commontypes.WorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.WorkgroupID getID() {
			return (uk.nhs.interoperability.payloads.commontypes.WorkgroupID)getValue("ID");
		}
		
		
		
		
		/**
		 * An identifier to identify the workgroup or team
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setID(uk.nhs.interoperability.payloads.commontypes.WorkgroupID ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * Workgroup name to identify a workgroup or team by name. If you have a local vocabulary with an OID you can use that to populate this field, otherwise use code '01' and add the team or workgroup name as the displayName
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getWorkgroupName() {
			return (CodedValue)getValue("WorkgroupName");
		}
		
		
		
		
		/**
		 * Workgroup name to identify a workgroup or team by name. If you have a local vocabulary with an OID you can use that to populate this field, otherwise use code '01' and add the team or workgroup name as the displayName
		 * <br><br>This field is MANDATORY
		 * @param WorkgroupName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setWorkgroupName(CodedValue WorkgroupName) {
			setValue("WorkgroupName", WorkgroupName);
			return this;
		}
		
		
		/**
		 * Workgroup name to identify a workgroup or team by name. If you have a local vocabulary with an OID you can use that to populate this field, otherwise use code '01' and add the team or workgroup name as the displayName
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param WorkgroupName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setWorkgroupName(VocabularyEntry WorkgroupName) {
			Code c = new CodedValue(WorkgroupName);
			setValue("WorkgroupName", c);
			return this;
		}
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getPersonName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("PersonName");
		}
		
		
		
		
		/**
		 * Person name
		 * @param PersonName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setPersonName(uk.nhs.interoperability.payloads.commontypes.PersonName PersonName) {
			setValue("PersonName", PersonName);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public WorkgroupUniversal setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145212GB02#Workgroup",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id",
												"An identifier to identify the workgroup or team",
												"false",
												"",
												"",
												"WorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("IDNullFlavour", new Field(
												"IDNullFlavour",
												"x:id/@nullFlavor",
												"NI",
												"",
												"ID",
												"",
												"",
												"",
												""
												));
	
		put("WorkgroupName", new Field(
												"WorkgroupName",
												"x:code",
												"Workgroup name to identify a workgroup or team by name. If you have a local vocabulary with an OID you can use that to populate this field, otherwise use code '01' and add the team or workgroup name as the displayName",
												"true",
												"",
												"",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.266",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:assignedPerson/@classCode",
												"PSN",
												"PersonName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:assignedPerson/@determinerCode",
												"INSTANCE",
												"PersonName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateIdRoot", new Field(
												"PersonTemplateIdRoot",
												"x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PersonName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateId", new Field(
												"PersonTemplateId",
												"x:assignedPerson/x:templateId/@extension",
												"COCD_TP145212GB02#assignedPerson",
												"PersonName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonName", new Field(
												"PersonName",
												"x:assignedPerson/x:name",
												"Person name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:representedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:representedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:representedOrganization/x:templateId/@extension",
												"COCD_TP145212GB02#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:representedOrganization/x:id",
												"",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:representedOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public WorkgroupUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public WorkgroupUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	