/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the RelatedEntityParticipant object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CDAPersonRelationshipType</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} Address</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom} Telephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} Name</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class RelatedEntityParticipant extends AbstractPayload implements Payload , Participant {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "RelatedEntityParticipant";
		protected static final String shortName = "RelatedEntity";
		protected static final String rootNode = "x:relatedEntity";
		protected static final String version = "COCD_TP145007UK03";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param CDAPersonRelationshipType CDAPersonRelationshipType
		 * @param Address Address
		 * @param Telephone Telephone
		 * @param Name Name
		 */
	    public RelatedEntityParticipant(CodedValue CDAPersonRelationshipType, uk.nhs.interoperability.payloads.commontypes.Address Address, uk.nhs.interoperability.payloads.commontypes.Telecom Telephone, uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			fields = new LinkedHashMap<String, Object>();
			
			setCDAPersonRelationshipType(CDAPersonRelationshipType);
			setAddress(Address);
			setTelephone(Telephone);
			setName(Name);
		}
	
		/**
		 * Describes the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "CDAPersonRelationshipType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getCDAPersonRelationshipType() {
			return (CodedValue)getValue("CDAPersonRelationshipType");
		}
		
		
		
		/**
		 * Describes the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "CDAPersonRelationshipType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType
		 * <br><br>This field is MANDATORY
		 * @return CDAPersonRelationshipType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType getCDAPersonRelationshipTypeEnum() {
			CodedValue cv = (CodedValue)getValue("CDAPersonRelationshipType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAPersonRelationshipType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType)entry;
		}
		
		
		/**
		 * Describes the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "CDAPersonRelationshipType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType
		 * <br><br>This field is MANDATORY
		 * @param CDAPersonRelationshipType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedEntityParticipant setCDAPersonRelationshipType(CodedValue CDAPersonRelationshipType) {
			setValue("CDAPersonRelationshipType", CDAPersonRelationshipType);
			return this;
		}
		
		
		/**
		 * Describes the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "CDAPersonRelationshipType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CDAPersonRelationshipType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedEntityParticipant setCDAPersonRelationshipType(VocabularyEntry CDAPersonRelationshipType) {
			Code c = new CodedValue(CDAPersonRelationshipType);
			setValue("CDAPersonRelationshipType", c);
			return this;
		}
		
		/**
		 * Address for the person who has a relationship with the patient
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("Address");
		}
		
		
		
		
		/**
		 * Address for the person who has a relationship with the patient
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedEntityParticipant setAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			setValue("Address", Address);
			return this;
		}
		
		
		/**
		 * Telephone number for the person who has a relationship with the patient
		 * @return uk.nhs.interoperability.payloads.commontypes.Telecom object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Telecom getTelephone() {
			return (uk.nhs.interoperability.payloads.commontypes.Telecom)getValue("Telephone");
		}
		
		
		
		
		/**
		 * Telephone number for the person who has a relationship with the patient
		 * @param Telephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedEntityParticipant setTelephone(uk.nhs.interoperability.payloads.commontypes.Telecom Telephone) {
			setValue("Telephone", Telephone);
			return this;
		}
		
		
		/**
		 * Name of the person who has a relationship with the patient
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Name of the person who has a relationship with the patient
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedEntityParticipant setName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145007UK03#RelatedEntity",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PRS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAPersonRelationshipType", new Field(
												"CDAPersonRelationshipType",
												"x:code",
												"Describes the relationship between the patient and the named person",
												"true",
												"",
												"CDAPersonRelationshipType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"Address for the person who has a relationship with the patient",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Telephone", new Field(
												"Telephone",
												"x:telecom",
												"Telephone number for the person who has a relationship with the patient",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:associatedPerson/@classCode",
												"PSN",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:associatedPerson/@determinerCode",
												"INSTANCE",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateIdRoot", new Field(
												"PersonTemplateIdRoot",
												"x:associatedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateId", new Field(
												"PersonTemplateId",
												"x:associatedPerson/x:templateId/@extension",
												"COCD_TP145007UK03#relationshipHolder",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:associatedPerson/x:name",
												"Name of the person who has a relationship with the patient",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public RelatedEntityParticipant() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public RelatedEntityParticipant(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	