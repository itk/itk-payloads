/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the RecipientPersonUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} JobRoleName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.RoleID RoleID}&gt; Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} Name</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CDAOrganizationType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class RecipientPersonUniversal extends AbstractPayload implements Payload , Recipient {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "RecipientPersonUniversal";
		protected static final String shortName = "IntendedRecipient";
		protected static final String rootNode = "x:intendedRecipient";
		protected static final String version = "COCD_TP145202GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param JobRoleName JobRoleName
		 * @param Id Id
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param Name Name
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 * @param CDAOrganizationType CDAOrganizationType
		 */
	    public RecipientPersonUniversal(CodedValue JobRoleName, 
		List<uk.nhs.interoperability.payloads.commontypes.RoleID> Id, uk.nhs.interoperability.payloads.commontypes.Address Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName Name, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName, CodedValue CDAOrganizationType) {
			fields = new LinkedHashMap<String, Object>();
			
			setJobRoleName(JobRoleName);
			setId(Id);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setName(Name);
			setOrgId(OrgId);
			setOrgName(OrgName);
			setCDAOrganizationType(CDAOrganizationType);
		}
	
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return CodedValue object
		 */	
		public CodedValue getJobRoleName() {
			return (CodedValue)getValue("JobRoleName");
		}
		
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("JobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @param JobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setJobRoleName(CodedValue JobRoleName) {
			setValue("JobRoleName", JobRoleName);
			return this;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param JobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setJobRoleName(VocabularyEntry JobRoleName) {
			Code c = new CodedValue(JobRoleName);
			setValue("JobRoleName", c);
			return this;
		}
		
		/**
		 * Local code to represent recipient's job role where there is a need to include a local code.
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.RoleID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.RoleID> getId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.RoleID>)getValue("Id");
		}
		
		/**
		 * Local code to represent recipient's job role where there is a need to include a local code.
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setId(List Id) {
			setValue("Id", Id);
			return this;
		}
		
		/**
		 * Local code to represent recipient's job role where there is a need to include a local code.
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.RoleID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.RoleID objects.
		 * @param Id value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal addId(uk.nhs.interoperability.payloads.commontypes.RoleID Id) {
			addMultivalue("Id", Id);
			return this;
		}
		
		
		/**
		 * This address is used to identify the address associated with the recipient; for example the GP practice address of the patient's GP.
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("Address");
		}
		
		
		
		
		/**
		 * This address is used to identify the address associated with the recipient; for example the GP practice address of the patient's GP.
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			setValue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Recipient name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Recipient name
		 * <br><br>This field is MANDATORY
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * Identifier that uniquely identifies the organisation which employs the recipient
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * Identifier that uniquely identifies the organisation which employs the recipient
		 * <br><br>This field is MANDATORY
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @return CodedValue object
		 */	
		public CodedValue getCDAOrganizationType() {
			return (CodedValue)getValue("CDAOrganizationType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @return CDAOrganizationType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType getCDAOrganizationTypeEnum() {
			CodedValue cv = (CodedValue)getValue("CDAOrganizationType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAOrganizationType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @param CDAOrganizationType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setCDAOrganizationType(CodedValue CDAOrganizationType) {
			setValue("CDAOrganizationType", CDAOrganizationType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CDAOrganizationType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public RecipientPersonUniversal setCDAOrganizationType(VocabularyEntry CDAOrganizationType) {
			Code c = new CodedValue(CDAOrganizationType);
			setValue("CDAOrganizationType", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145202GB02#IntendedRecipient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("JobRoleName", new Field(
												"JobRoleName",
												"npfitlc:recipientRoleCode",
												"This is an identifier for the Job Role of the Recipient",
												"false",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id",
												"Local code to represent recipient's job role where there is a need to include a local code.",
												"true",
												"",
												"",
												"RoleID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("IdNullFlavour", new Field(
												"IdNullFlavour",
												"x:id/@nullFlavor",
												"NA",
												"",
												"Id",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"This address is used to identify the address associated with the recipient; for example the GP practice address of the patient's GP.",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:informationRecipient/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:informationRecipient/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateIdRoot", new Field(
												"PersonTemplateIdRoot",
												"x:informationRecipient/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateId", new Field(
												"PersonTemplateId",
												"x:informationRecipient/x:templateId/@extension",
												"COCD_TP145202GB02#assignedPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:informationRecipient/x:name",
												"Recipient name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:receivedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:receivedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:receivedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:receivedOrganization/x:templateId/@extension",
												"COCD_TP145202GB02#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:receivedOrganization/x:id",
												"Identifier that uniquely identifies the organisation which employs the recipient",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:receivedOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAOrganizationType", new Field(
												"CDAOrganizationType",
												"x:receivedOrganization/x:standardIndustryClassCode",
												"",
												"false",
												"",
												"CDAOrganizationType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public RecipientPersonUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public RecipientPersonUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	