/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the AuthorDeviceUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.DeviceID DeviceID}&gt; DeviceId</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ManufacturerModelName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SoftwareName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class AuthorDeviceUniversal extends AbstractPayload implements Payload , Author {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "AuthorDeviceUniversal";
		protected static final String shortName = "AssignedAuthorDevice";
		protected static final String rootNode = "x:assignedAuthor";
		protected static final String version = "COCD_TP145207GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param DeviceId DeviceId
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 * @param ManufacturerModelName ManufacturerModelName
		 * @param SoftwareName SoftwareName
		 */
	    public AuthorDeviceUniversal(
		List<uk.nhs.interoperability.payloads.commontypes.DeviceID> DeviceId, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName, CodedValue ManufacturerModelName, CodedValue SoftwareName) {
			fields = new LinkedHashMap<String, Object>();
			
			setDeviceId(DeviceId);
			setOrgId(OrgId);
			setOrgName(OrgName);
			setManufacturerModelName(ManufacturerModelName);
			setSoftwareName(SoftwareName);
		}
	
		/**
		 * One or more identifier(s) including local identifiers to identify the device
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.DeviceID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.DeviceID> getDeviceId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.DeviceID>)getValue("DeviceId");
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the device
		 * <br><br>This field is MANDATORY
		 * @param DeviceId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setDeviceId(List DeviceId) {
			setValue("DeviceId", DeviceId);
			return this;
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the device
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.DeviceID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.DeviceID objects.
		 * @param DeviceId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal addDeviceId(uk.nhs.interoperability.payloads.commontypes.DeviceID DeviceId) {
			addMultivalue("DeviceId", DeviceId);
			return this;
		}
		
		
		/**
		 * An identifier that uniquely identifies the organisation which owns or is responsible for the device
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * An identifier that uniquely identifies the organisation which owns or is responsible for the device
		 * <br><br>This field is MANDATORY
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		
		/**
		 * Used to describe the manufacture's name and / or model for the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getManufacturerModelName() {
			return (CodedValue)getValue("ManufacturerModelName");
		}
		
		
		
		
		/**
		 * Used to describe the manufacture's name and / or model for the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.
		 * <br><br>This field is MANDATORY
		 * @param ManufacturerModelName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setManufacturerModelName(CodedValue ManufacturerModelName) {
			setValue("ManufacturerModelName", ManufacturerModelName);
			return this;
		}
		
		
		/**
		 * Used to describe the manufacture's name and / or model for the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ManufacturerModelName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setManufacturerModelName(VocabularyEntry ManufacturerModelName) {
			Code c = new CodedValue(ManufacturerModelName);
			setValue("ManufacturerModelName", c);
			return this;
		}
		
		/**
		 * String used to describe the software installed / running on the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getSoftwareName() {
			return (CodedValue)getValue("SoftwareName");
		}
		
		
		
		
		/**
		 * String used to describe the software installed / running on the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.
		 * <br><br>This field is MANDATORY
		 * @param SoftwareName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setSoftwareName(CodedValue SoftwareName) {
			setValue("SoftwareName", SoftwareName);
			return this;
		}
		
		
		/**
		 * String used to describe the software installed / running on the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SoftwareName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorDeviceUniversal setSoftwareName(VocabularyEntry SoftwareName) {
			Code c = new CodedValue(SoftwareName);
			setValue("SoftwareName", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145207GB01#AssignedAuthorDevice",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeviceId", new Field(
												"DeviceId",
												"x:id",
												"One or more identifier(s) including local identifiers to identify the device",
												"true",
												"",
												"",
												"DeviceID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeviceIDNullFlavour", new Field(
												"DeviceIDNullFlavour",
												"x:id/@nullFlavor",
												"NI",
												"",
												"DeviceId",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:representedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:representedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:representedOrganization/x:templateId/@extension",
												"COCD_TP145207GB01#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:representedOrganization/x:id",
												"An identifier that uniquely identifies the organisation which owns or is responsible for the device",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:representedOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DevClassCode", new Field(
												"DevClassCode",
												"x:assignedAuthoringDevice/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DevDeterminerCode", new Field(
												"DevDeterminerCode",
												"x:assignedAuthoringDevice/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DevTemplateIdRoot", new Field(
												"DevTemplateIdRoot",
												"x:assignedAuthoringDevice/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DevTemplateId", new Field(
												"DevTemplateId",
												"x:assignedAuthoringDevice/x:templateId/@extension",
												"COCD_TP145207GB01#assignedAuthoringDevice",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ManufacturerModelName", new Field(
												"ManufacturerModelName",
												"x:assignedAuthoringDevice/x:manufacturerModelName",
												"Used to describe the manufacture's name and / or model for the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.",
												"true",
												"",
												"",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.405",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SoftwareName", new Field(
												"SoftwareName",
												"x:assignedAuthoringDevice/x:softwareName",
												"String used to describe the software installed / running on the device or system. NOTE: There is no national vocabulary for this field, so a CodedValue object should be used populated with a local code and description.",
												"true",
												"",
												"",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.406",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public AuthorDeviceUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public AuthorDeviceUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	