/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the BloodSpotScreening object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor ChildScreeningAuthor} Author</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} SampleCollectedTime</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; PerformerPersonId</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PerformerJobRoleName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} PerformerAddress</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; PerformerTelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} PerformerPersonName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} PerformerOrgId</li>
 * <li>String PerformerOrgName</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeReceivedAtLab</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} LabRoleCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} LabAddr</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; LabTelecom</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} LabOrganisationId</li>
 * <li>String LabOrganisationDescription</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PKUScreeningValue</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PKUBSLaboratoryInterpretationCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PKUScreeningSubStatus</li>
 * <li>String PKUReasonText</li>
 * <li>String PKUSupplementaryText</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SCDScreeningValue</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SCDBSLaboratoryInterpretationCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SCDScreeningSubStatus</li>
 * <li>String SCDReasonText</li>
 * <li>String SCDSupplementaryText</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CFScreeningValue</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CFBSLaboratoryInterpretationCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CFScreeningSubStatus</li>
 * <li>String CFReasonText</li>
 * <li>String CFSupplementaryText</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CHTScreeningValue</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CHTBSLaboratoryInterpretationCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CHTScreeningSubStatus</li>
 * <li>String CHTReasonText</li>
 * <li>String CHTSupplementaryText</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} MCADDScreeningValue</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} MCADDBSLaboratoryInterpretationCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} MCADDScreeningSubStatus</li>
 * <li>String MCADDReasonText</li>
 * <li>String MCADDSupplementaryText</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} ScreeningLocationStatusEffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ScreeningLocationStatus</li>
 * <li>String LaboratoryCardSerialNumber</li>
 * <li>String PreviousLaboratoryCardSerialNumber</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class BloodSpotScreening extends AbstractPayload implements Payload , ChildScreeningCodedSections {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "BloodSpotScreening";
		protected static final String shortName = "BloodSpotScreening";
		protected static final String rootNode = "x:organizer";
		protected static final String version = "COCD_TP146005GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param EffectiveTime EffectiveTime
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param SampleCollectedTime SampleCollectedTime
		 * @param PerformerPersonId PerformerPersonId
		 * @param PerformerJobRoleName PerformerJobRoleName
		 * @param PerformerAddress PerformerAddress
		 * @param PerformerTelephoneNumber PerformerTelephoneNumber
		 * @param PerformerPersonName PerformerPersonName
		 * @param PerformerOrgId PerformerOrgId
		 * @param PerformerOrgName PerformerOrgName
		 * @param TimeReceivedAtLab TimeReceivedAtLab
		 * @param LabRoleCode LabRoleCode
		 * @param LabAddr LabAddr
		 * @param LabTelecom LabTelecom
		 * @param LabOrganisationId LabOrganisationId
		 * @param LabOrganisationDescription LabOrganisationDescription
		 * @param PKUScreeningValue PKUScreeningValue
		 * @param PKUBSLaboratoryInterpretationCode PKUBSLaboratoryInterpretationCode
		 * @param PKUScreeningSubStatus PKUScreeningSubStatus
		 * @param PKUReasonText PKUReasonText
		 * @param PKUSupplementaryText PKUSupplementaryText
		 * @param SCDScreeningValue SCDScreeningValue
		 * @param SCDBSLaboratoryInterpretationCode SCDBSLaboratoryInterpretationCode
		 * @param SCDScreeningSubStatus SCDScreeningSubStatus
		 * @param SCDReasonText SCDReasonText
		 * @param SCDSupplementaryText SCDSupplementaryText
		 * @param CFScreeningValue CFScreeningValue
		 * @param CFBSLaboratoryInterpretationCode CFBSLaboratoryInterpretationCode
		 * @param CFScreeningSubStatus CFScreeningSubStatus
		 * @param CFReasonText CFReasonText
		 * @param CFSupplementaryText CFSupplementaryText
		 * @param CHTScreeningValue CHTScreeningValue
		 * @param CHTBSLaboratoryInterpretationCode CHTBSLaboratoryInterpretationCode
		 * @param CHTScreeningSubStatus CHTScreeningSubStatus
		 * @param CHTReasonText CHTReasonText
		 * @param CHTSupplementaryText CHTSupplementaryText
		 * @param MCADDScreeningValue MCADDScreeningValue
		 * @param MCADDBSLaboratoryInterpretationCode MCADDBSLaboratoryInterpretationCode
		 * @param MCADDScreeningSubStatus MCADDScreeningSubStatus
		 * @param MCADDReasonText MCADDReasonText
		 * @param MCADDSupplementaryText MCADDSupplementaryText
		 * @param ScreeningLocationStatusEffectiveTime ScreeningLocationStatusEffectiveTime
		 * @param ScreeningLocationStatus ScreeningLocationStatus
		 * @param LaboratoryCardSerialNumber LaboratoryCardSerialNumber
		 * @param PreviousLaboratoryCardSerialNumber PreviousLaboratoryCardSerialNumber
		 */
	    public BloodSpotScreening(String Id, HL7Date EffectiveTime, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author, HL7Date SampleCollectedTime, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> PerformerPersonId, CodedValue PerformerJobRoleName, uk.nhs.interoperability.payloads.commontypes.Address PerformerAddress, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> PerformerTelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName PerformerPersonName, uk.nhs.interoperability.payloads.commontypes.OrgID PerformerOrgId, String PerformerOrgName, HL7Date TimeReceivedAtLab, CodedValue LabRoleCode, uk.nhs.interoperability.payloads.commontypes.Address LabAddr, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> LabTelecom, uk.nhs.interoperability.payloads.commontypes.OrgID LabOrganisationId, String LabOrganisationDescription, CodedValue PKUScreeningValue, CodedValue PKUBSLaboratoryInterpretationCode, CodedValue PKUScreeningSubStatus, String PKUReasonText, String PKUSupplementaryText, CodedValue SCDScreeningValue, CodedValue SCDBSLaboratoryInterpretationCode, CodedValue SCDScreeningSubStatus, String SCDReasonText, String SCDSupplementaryText, CodedValue CFScreeningValue, CodedValue CFBSLaboratoryInterpretationCode, CodedValue CFScreeningSubStatus, String CFReasonText, String CFSupplementaryText, CodedValue CHTScreeningValue, CodedValue CHTBSLaboratoryInterpretationCode, CodedValue CHTScreeningSubStatus, String CHTReasonText, String CHTSupplementaryText, CodedValue MCADDScreeningValue, CodedValue MCADDBSLaboratoryInterpretationCode, CodedValue MCADDScreeningSubStatus, String MCADDReasonText, String MCADDSupplementaryText, HL7Date ScreeningLocationStatusEffectiveTime, CodedValue ScreeningLocationStatus, String LaboratoryCardSerialNumber, String PreviousLaboratoryCardSerialNumber) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setEffectiveTime(EffectiveTime);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setSampleCollectedTime(SampleCollectedTime);
			setPerformerPersonId(PerformerPersonId);
			setPerformerJobRoleName(PerformerJobRoleName);
			setPerformerAddress(PerformerAddress);
			setPerformerTelephoneNumber(PerformerTelephoneNumber);
			setPerformerPersonName(PerformerPersonName);
			setPerformerOrgId(PerformerOrgId);
			setPerformerOrgName(PerformerOrgName);
			setTimeReceivedAtLab(TimeReceivedAtLab);
			setLabRoleCode(LabRoleCode);
			setLabAddr(LabAddr);
			setLabTelecom(LabTelecom);
			setLabOrganisationId(LabOrganisationId);
			setLabOrganisationDescription(LabOrganisationDescription);
			setPKUScreeningValue(PKUScreeningValue);
			setPKUBSLaboratoryInterpretationCode(PKUBSLaboratoryInterpretationCode);
			setPKUScreeningSubStatus(PKUScreeningSubStatus);
			setPKUReasonText(PKUReasonText);
			setPKUSupplementaryText(PKUSupplementaryText);
			setSCDScreeningValue(SCDScreeningValue);
			setSCDBSLaboratoryInterpretationCode(SCDBSLaboratoryInterpretationCode);
			setSCDScreeningSubStatus(SCDScreeningSubStatus);
			setSCDReasonText(SCDReasonText);
			setSCDSupplementaryText(SCDSupplementaryText);
			setCFScreeningValue(CFScreeningValue);
			setCFBSLaboratoryInterpretationCode(CFBSLaboratoryInterpretationCode);
			setCFScreeningSubStatus(CFScreeningSubStatus);
			setCFReasonText(CFReasonText);
			setCFSupplementaryText(CFSupplementaryText);
			setCHTScreeningValue(CHTScreeningValue);
			setCHTBSLaboratoryInterpretationCode(CHTBSLaboratoryInterpretationCode);
			setCHTScreeningSubStatus(CHTScreeningSubStatus);
			setCHTReasonText(CHTReasonText);
			setCHTSupplementaryText(CHTSupplementaryText);
			setMCADDScreeningValue(MCADDScreeningValue);
			setMCADDBSLaboratoryInterpretationCode(MCADDBSLaboratoryInterpretationCode);
			setMCADDScreeningSubStatus(MCADDScreeningSubStatus);
			setMCADDReasonText(MCADDReasonText);
			setMCADDSupplementaryText(MCADDSupplementaryText);
			setScreeningLocationStatusEffectiveTime(ScreeningLocationStatusEffectiveTime);
			setScreeningLocationStatus(ScreeningLocationStatus);
			setLaboratoryCardSerialNumber(LaboratoryCardSerialNumber);
			setPreviousLaboratoryCardSerialNumber(PreviousLaboratoryCardSerialNumber);
		}
	
		/**
		 * A DCE UUID to identify each instance of a blood spot battery
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of a blood spot battery
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * The time when the blood spot screening was done
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * The time when the blood spot screening was done
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor object
		 */	
		public uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setAuthor(uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The time the performer originally collected the blood spot screening sample
		 * @return HL7Date object
		 */	
		public HL7Date getSampleCollectedTime() {
			return (HL7Date)getValue("SampleCollectedTime");
		}
		
		
		
		
		/**
		 * The time the performer originally collected the blood spot screening sample
		 * @param SampleCollectedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSampleCollectedTime(HL7Date SampleCollectedTime) {
			setValue("SampleCollectedTime", SampleCollectedTime);
			return this;
		}
		
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getPerformerPersonId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("PerformerPersonId");
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br><br>This field is MANDATORY
		 * @param PerformerPersonId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerPersonId(List PerformerPersonId) {
			setValue("PerformerPersonId", PerformerPersonId);
			return this;
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param PerformerPersonId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening addPerformerPersonId(uk.nhs.interoperability.payloads.commontypes.PersonID PerformerPersonId) {
			addMultivalue("PerformerPersonId", PerformerPersonId);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return CodedValue object
		 */	
		public CodedValue getPerformerJobRoleName() {
			return (CodedValue)getValue("PerformerJobRoleName");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getPerformerJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("PerformerJobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @param PerformerJobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerJobRoleName(CodedValue PerformerJobRoleName) {
			setValue("PerformerJobRoleName", PerformerJobRoleName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PerformerJobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerJobRoleName(VocabularyEntry PerformerJobRoleName) {
			Code c = new CodedValue(PerformerJobRoleName);
			setValue("PerformerJobRoleName", c);
			return this;
		}
		
		/**
		 * Contact address for the organisation which is the site where the role is based
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getPerformerAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("PerformerAddress");
		}
		
		
		
		
		/**
		 * Contact address for the organisation which is the site where the role is based
		 * @param PerformerAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerAddress(uk.nhs.interoperability.payloads.commontypes.Address PerformerAddress) {
			setValue("PerformerAddress", PerformerAddress);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getPerformerTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("PerformerTelephoneNumber");
		}
		
		/**
		 * 
		 * @param PerformerTelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerTelephoneNumber(List PerformerTelephoneNumber) {
			setValue("PerformerTelephoneNumber", PerformerTelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param PerformerTelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening addPerformerTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom PerformerTelephoneNumber) {
			addMultivalue("PerformerTelephoneNumber", PerformerTelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getPerformerPersonName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("PerformerPersonName");
		}
		
		
		
		
		/**
		 * Person name
		 * <br><br>This field is MANDATORY
		 * @param PerformerPersonName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerPersonName(uk.nhs.interoperability.payloads.commontypes.PersonName PerformerPersonName) {
			setValue("PerformerPersonName", PerformerPersonName);
			return this;
		}
		
		
		/**
		 * An identifier that uniquely identifies the organisation which employs the person in the role
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getPerformerOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("PerformerOrgId");
		}
		
		
		
		
		/**
		 * An identifier that uniquely identifies the organisation which employs the person in the role
		 * @param PerformerOrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID PerformerOrgId) {
			setValue("PerformerOrgId", PerformerOrgId);
			return this;
		}
		
		
		/**
		 * Name of the organisation associated with the ODS code
		 * @return String object
		 */	
		public String getPerformerOrgName() {
			return (String)getValue("PerformerOrgName");
		}
		
		
		
		
		/**
		 * Name of the organisation associated with the ODS code
		 * @param PerformerOrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPerformerOrgName(String PerformerOrgName) {
			setValue("PerformerOrgName", PerformerOrgName);
			return this;
		}
		
		
		/**
		 * The time the secondary performer (lab) originally received the blood spot screening sample
		 * @return HL7Date object
		 */	
		public HL7Date getTimeReceivedAtLab() {
			return (HL7Date)getValue("TimeReceivedAtLab");
		}
		
		
		
		
		/**
		 * The time the secondary performer (lab) originally received the blood spot screening sample
		 * @param TimeReceivedAtLab value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setTimeReceivedAtLab(HL7Date TimeReceivedAtLab) {
			setValue("TimeReceivedAtLab", TimeReceivedAtLab);
			return this;
		}
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getLabRoleCode() {
			return (CodedValue)getValue("LabRoleCode");
		}
		
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getLabRoleCodeEnum() {
			CodedValue cv = (CodedValue)getValue("LabRoleCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param LabRoleCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLabRoleCode(CodedValue LabRoleCode) {
			setValue("LabRoleCode", LabRoleCode);
			return this;
		}
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param LabRoleCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLabRoleCode(VocabularyEntry LabRoleCode) {
			Code c = new CodedValue(LabRoleCode);
			setValue("LabRoleCode", c);
			return this;
		}
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getLabAddr() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("LabAddr");
		}
		
		
		
		
		/**
		 * 
		 * @param LabAddr value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLabAddr(uk.nhs.interoperability.payloads.commontypes.Address LabAddr) {
			setValue("LabAddr", LabAddr);
			return this;
		}
		
		
		/**
		 * Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getLabTelecom() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("LabTelecom");
		}
		
		/**
		 * Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.
		 * @param LabTelecom value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLabTelecom(List LabTelecom) {
			setValue("LabTelecom", LabTelecom);
			return this;
		}
		
		/**
		 * Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param LabTelecom value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening addLabTelecom(uk.nhs.interoperability.payloads.commontypes.Telecom LabTelecom) {
			addMultivalue("LabTelecom", LabTelecom);
			return this;
		}
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation in the role.
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getLabOrganisationId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("LabOrganisationId");
		}
		
		
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation in the role.
		 * <br><br>This field is MANDATORY
		 * @param LabOrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLabOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID LabOrganisationId) {
			setValue("LabOrganisationId", LabOrganisationId);
			return this;
		}
		
		
		/**
		 * Description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getLabOrganisationDescription() {
			return (String)getValue("LabOrganisationDescription");
		}
		
		
		
		
		/**
		 * Description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @param LabOrganisationDescription value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLabOrganisationDescription(String LabOrganisationDescription) {
			setValue("LabOrganisationDescription", LabOrganisationDescription);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "PKUScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult
		 * @return CodedValue object
		 */	
		public CodedValue getPKUScreeningValue() {
			return (CodedValue)getValue("PKUScreeningValue");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "PKUScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult
		 * @return PKUScreeningResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult getPKUScreeningValueEnum() {
			CodedValue cv = (CodedValue)getValue("PKUScreeningValue");
			VocabularyEntry entry = VocabularyFactory.getVocab("PKUScreeningResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "PKUScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult
		 * @param PKUScreeningValue value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUScreeningValue(CodedValue PKUScreeningValue) {
			setValue("PKUScreeningValue", PKUScreeningValue);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "PKUScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PKUScreeningValue value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUScreeningValue(VocabularyEntry PKUScreeningValue) {
			Code c = new CodedValue(PKUScreeningValue);
			setValue("PKUScreeningValue", c);
			return this;
		}
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return CodedValue object
		 */	
		public CodedValue getPKUBSLaboratoryInterpretationCode() {
			return (CodedValue)getValue("PKUBSLaboratoryInterpretationCode");
		}
		
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return BSLaboratoryInterpretationCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode getPKUBSLaboratoryInterpretationCodeEnum() {
			CodedValue cv = (CodedValue)getValue("PKUBSLaboratoryInterpretationCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSLaboratoryInterpretationCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode)entry;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @param PKUBSLaboratoryInterpretationCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUBSLaboratoryInterpretationCode(CodedValue PKUBSLaboratoryInterpretationCode) {
			setValue("PKUBSLaboratoryInterpretationCode", PKUBSLaboratoryInterpretationCode);
			return this;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PKUBSLaboratoryInterpretationCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUBSLaboratoryInterpretationCode(VocabularyEntry PKUBSLaboratoryInterpretationCode) {
			Code c = new CodedValue(PKUBSLaboratoryInterpretationCode);
			setValue("PKUBSLaboratoryInterpretationCode", c);
			return this;
		}
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return CodedValue object
		 */	
		public CodedValue getPKUScreeningSubStatus() {
			return (CodedValue)getValue("PKUScreeningSubStatus");
		}
		
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return BSScreeningStatusSubCodes enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes getPKUScreeningSubStatusEnum() {
			CodedValue cv = (CodedValue)getValue("PKUScreeningSubStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSScreeningStatusSubCodes", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes)entry;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @param PKUScreeningSubStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUScreeningSubStatus(CodedValue PKUScreeningSubStatus) {
			setValue("PKUScreeningSubStatus", PKUScreeningSubStatus);
			return this;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PKUScreeningSubStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUScreeningSubStatus(VocabularyEntry PKUScreeningSubStatus) {
			Code c = new CodedValue(PKUScreeningSubStatus);
			setValue("PKUScreeningSubStatus", c);
			return this;
		}
		
		/**
		 * Detail about the PKU reason
		 * @return String object
		 */	
		public String getPKUReasonText() {
			return (String)getValue("PKUReasonText");
		}
		
		
		
		
		/**
		 * Detail about the PKU reason
		 * @param PKUReasonText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUReasonText(String PKUReasonText) {
			setValue("PKUReasonText", PKUReasonText);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getPKUSupplementaryText() {
			return (String)getValue("PKUSupplementaryText");
		}
		
		
		
		
		/**
		 * 
		 * @param PKUSupplementaryText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPKUSupplementaryText(String PKUSupplementaryText) {
			setValue("PKUSupplementaryText", PKUSupplementaryText);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "SCDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult
		 * @return CodedValue object
		 */	
		public CodedValue getSCDScreeningValue() {
			return (CodedValue)getValue("SCDScreeningValue");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "SCDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult
		 * @return SCDScreeningResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult getSCDScreeningValueEnum() {
			CodedValue cv = (CodedValue)getValue("SCDScreeningValue");
			VocabularyEntry entry = VocabularyFactory.getVocab("SCDScreeningResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "SCDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult
		 * @param SCDScreeningValue value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDScreeningValue(CodedValue SCDScreeningValue) {
			setValue("SCDScreeningValue", SCDScreeningValue);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "SCDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SCDScreeningValue value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDScreeningValue(VocabularyEntry SCDScreeningValue) {
			Code c = new CodedValue(SCDScreeningValue);
			setValue("SCDScreeningValue", c);
			return this;
		}
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return CodedValue object
		 */	
		public CodedValue getSCDBSLaboratoryInterpretationCode() {
			return (CodedValue)getValue("SCDBSLaboratoryInterpretationCode");
		}
		
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return BSLaboratoryInterpretationCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode getSCDBSLaboratoryInterpretationCodeEnum() {
			CodedValue cv = (CodedValue)getValue("SCDBSLaboratoryInterpretationCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSLaboratoryInterpretationCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode)entry;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @param SCDBSLaboratoryInterpretationCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDBSLaboratoryInterpretationCode(CodedValue SCDBSLaboratoryInterpretationCode) {
			setValue("SCDBSLaboratoryInterpretationCode", SCDBSLaboratoryInterpretationCode);
			return this;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SCDBSLaboratoryInterpretationCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDBSLaboratoryInterpretationCode(VocabularyEntry SCDBSLaboratoryInterpretationCode) {
			Code c = new CodedValue(SCDBSLaboratoryInterpretationCode);
			setValue("SCDBSLaboratoryInterpretationCode", c);
			return this;
		}
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return CodedValue object
		 */	
		public CodedValue getSCDScreeningSubStatus() {
			return (CodedValue)getValue("SCDScreeningSubStatus");
		}
		
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return BSScreeningStatusSubCodes enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes getSCDScreeningSubStatusEnum() {
			CodedValue cv = (CodedValue)getValue("SCDScreeningSubStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSScreeningStatusSubCodes", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes)entry;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @param SCDScreeningSubStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDScreeningSubStatus(CodedValue SCDScreeningSubStatus) {
			setValue("SCDScreeningSubStatus", SCDScreeningSubStatus);
			return this;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SCDScreeningSubStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDScreeningSubStatus(VocabularyEntry SCDScreeningSubStatus) {
			Code c = new CodedValue(SCDScreeningSubStatus);
			setValue("SCDScreeningSubStatus", c);
			return this;
		}
		
		/**
		 * Detail about the SCD reason
		 * @return String object
		 */	
		public String getSCDReasonText() {
			return (String)getValue("SCDReasonText");
		}
		
		
		
		
		/**
		 * Detail about the SCD reason
		 * @param SCDReasonText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDReasonText(String SCDReasonText) {
			setValue("SCDReasonText", SCDReasonText);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getSCDSupplementaryText() {
			return (String)getValue("SCDSupplementaryText");
		}
		
		
		
		
		/**
		 * 
		 * @param SCDSupplementaryText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setSCDSupplementaryText(String SCDSupplementaryText) {
			setValue("SCDSupplementaryText", SCDSupplementaryText);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CFScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult
		 * @return CodedValue object
		 */	
		public CodedValue getCFScreeningValue() {
			return (CodedValue)getValue("CFScreeningValue");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CFScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult
		 * @return CFScreeningResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult getCFScreeningValueEnum() {
			CodedValue cv = (CodedValue)getValue("CFScreeningValue");
			VocabularyEntry entry = VocabularyFactory.getVocab("CFScreeningResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CFScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult
		 * @param CFScreeningValue value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFScreeningValue(CodedValue CFScreeningValue) {
			setValue("CFScreeningValue", CFScreeningValue);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CFScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CFScreeningValue value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFScreeningValue(VocabularyEntry CFScreeningValue) {
			Code c = new CodedValue(CFScreeningValue);
			setValue("CFScreeningValue", c);
			return this;
		}
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return CodedValue object
		 */	
		public CodedValue getCFBSLaboratoryInterpretationCode() {
			return (CodedValue)getValue("CFBSLaboratoryInterpretationCode");
		}
		
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return BSLaboratoryInterpretationCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode getCFBSLaboratoryInterpretationCodeEnum() {
			CodedValue cv = (CodedValue)getValue("CFBSLaboratoryInterpretationCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSLaboratoryInterpretationCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode)entry;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @param CFBSLaboratoryInterpretationCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFBSLaboratoryInterpretationCode(CodedValue CFBSLaboratoryInterpretationCode) {
			setValue("CFBSLaboratoryInterpretationCode", CFBSLaboratoryInterpretationCode);
			return this;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CFBSLaboratoryInterpretationCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFBSLaboratoryInterpretationCode(VocabularyEntry CFBSLaboratoryInterpretationCode) {
			Code c = new CodedValue(CFBSLaboratoryInterpretationCode);
			setValue("CFBSLaboratoryInterpretationCode", c);
			return this;
		}
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return CodedValue object
		 */	
		public CodedValue getCFScreeningSubStatus() {
			return (CodedValue)getValue("CFScreeningSubStatus");
		}
		
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return BSScreeningStatusSubCodes enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes getCFScreeningSubStatusEnum() {
			CodedValue cv = (CodedValue)getValue("CFScreeningSubStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSScreeningStatusSubCodes", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes)entry;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @param CFScreeningSubStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFScreeningSubStatus(CodedValue CFScreeningSubStatus) {
			setValue("CFScreeningSubStatus", CFScreeningSubStatus);
			return this;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CFScreeningSubStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFScreeningSubStatus(VocabularyEntry CFScreeningSubStatus) {
			Code c = new CodedValue(CFScreeningSubStatus);
			setValue("CFScreeningSubStatus", c);
			return this;
		}
		
		/**
		 * Detail about the CF reason
		 * @return String object
		 */	
		public String getCFReasonText() {
			return (String)getValue("CFReasonText");
		}
		
		
		
		
		/**
		 * Detail about the CF reason
		 * @param CFReasonText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFReasonText(String CFReasonText) {
			setValue("CFReasonText", CFReasonText);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getCFSupplementaryText() {
			return (String)getValue("CFSupplementaryText");
		}
		
		
		
		
		/**
		 * 
		 * @param CFSupplementaryText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCFSupplementaryText(String CFSupplementaryText) {
			setValue("CFSupplementaryText", CFSupplementaryText);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CHTScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult
		 * @return CodedValue object
		 */	
		public CodedValue getCHTScreeningValue() {
			return (CodedValue)getValue("CHTScreeningValue");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CHTScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult
		 * @return CHTScreeningResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult getCHTScreeningValueEnum() {
			CodedValue cv = (CodedValue)getValue("CHTScreeningValue");
			VocabularyEntry entry = VocabularyFactory.getVocab("CHTScreeningResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CHTScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult
		 * @param CHTScreeningValue value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTScreeningValue(CodedValue CHTScreeningValue) {
			setValue("CHTScreeningValue", CHTScreeningValue);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "CHTScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CHTScreeningValue value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTScreeningValue(VocabularyEntry CHTScreeningValue) {
			Code c = new CodedValue(CHTScreeningValue);
			setValue("CHTScreeningValue", c);
			return this;
		}
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return CodedValue object
		 */	
		public CodedValue getCHTBSLaboratoryInterpretationCode() {
			return (CodedValue)getValue("CHTBSLaboratoryInterpretationCode");
		}
		
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return BSLaboratoryInterpretationCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode getCHTBSLaboratoryInterpretationCodeEnum() {
			CodedValue cv = (CodedValue)getValue("CHTBSLaboratoryInterpretationCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSLaboratoryInterpretationCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode)entry;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @param CHTBSLaboratoryInterpretationCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTBSLaboratoryInterpretationCode(CodedValue CHTBSLaboratoryInterpretationCode) {
			setValue("CHTBSLaboratoryInterpretationCode", CHTBSLaboratoryInterpretationCode);
			return this;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CHTBSLaboratoryInterpretationCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTBSLaboratoryInterpretationCode(VocabularyEntry CHTBSLaboratoryInterpretationCode) {
			Code c = new CodedValue(CHTBSLaboratoryInterpretationCode);
			setValue("CHTBSLaboratoryInterpretationCode", c);
			return this;
		}
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return CodedValue object
		 */	
		public CodedValue getCHTScreeningSubStatus() {
			return (CodedValue)getValue("CHTScreeningSubStatus");
		}
		
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return BSScreeningStatusSubCodes enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes getCHTScreeningSubStatusEnum() {
			CodedValue cv = (CodedValue)getValue("CHTScreeningSubStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSScreeningStatusSubCodes", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes)entry;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @param CHTScreeningSubStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTScreeningSubStatus(CodedValue CHTScreeningSubStatus) {
			setValue("CHTScreeningSubStatus", CHTScreeningSubStatus);
			return this;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CHTScreeningSubStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTScreeningSubStatus(VocabularyEntry CHTScreeningSubStatus) {
			Code c = new CodedValue(CHTScreeningSubStatus);
			setValue("CHTScreeningSubStatus", c);
			return this;
		}
		
		/**
		 * Detail about the CHT reason
		 * @return String object
		 */	
		public String getCHTReasonText() {
			return (String)getValue("CHTReasonText");
		}
		
		
		
		
		/**
		 * Detail about the CHT reason
		 * @param CHTReasonText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTReasonText(String CHTReasonText) {
			setValue("CHTReasonText", CHTReasonText);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getCHTSupplementaryText() {
			return (String)getValue("CHTSupplementaryText");
		}
		
		
		
		
		/**
		 * 
		 * @param CHTSupplementaryText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setCHTSupplementaryText(String CHTSupplementaryText) {
			setValue("CHTSupplementaryText", CHTSupplementaryText);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "MCADDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult
		 * @return CodedValue object
		 */	
		public CodedValue getMCADDScreeningValue() {
			return (CodedValue)getValue("MCADDScreeningValue");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "MCADDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult
		 * @return MCADDScreeningResult enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult getMCADDScreeningValueEnum() {
			CodedValue cv = (CodedValue)getValue("MCADDScreeningValue");
			VocabularyEntry entry = VocabularyFactory.getVocab("MCADDScreeningResult", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "MCADDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult
		 * @param MCADDScreeningValue value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDScreeningValue(CodedValue MCADDScreeningValue) {
			setValue("MCADDScreeningValue", MCADDScreeningValue);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "MCADDScreeningResult" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param MCADDScreeningValue value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDScreeningValue(VocabularyEntry MCADDScreeningValue) {
			Code c = new CodedValue(MCADDScreeningValue);
			setValue("MCADDScreeningValue", c);
			return this;
		}
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return CodedValue object
		 */	
		public CodedValue getMCADDBSLaboratoryInterpretationCode() {
			return (CodedValue)getValue("MCADDBSLaboratoryInterpretationCode");
		}
		
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @return BSLaboratoryInterpretationCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode getMCADDBSLaboratoryInterpretationCodeEnum() {
			CodedValue cv = (CodedValue)getValue("MCADDBSLaboratoryInterpretationCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSLaboratoryInterpretationCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode)entry;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * @param MCADDBSLaboratoryInterpretationCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDBSLaboratoryInterpretationCode(CodedValue MCADDBSLaboratoryInterpretationCode) {
			setValue("MCADDBSLaboratoryInterpretationCode", MCADDBSLaboratoryInterpretationCode);
			return this;
		}
		
		
		/**
		 * A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result
  		 * <br>NOTE: This field should be populated using the "BSLaboratoryInterpretationCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param MCADDBSLaboratoryInterpretationCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDBSLaboratoryInterpretationCode(VocabularyEntry MCADDBSLaboratoryInterpretationCode) {
			Code c = new CodedValue(MCADDBSLaboratoryInterpretationCode);
			setValue("MCADDBSLaboratoryInterpretationCode", c);
			return this;
		}
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return CodedValue object
		 */	
		public CodedValue getMCADDScreeningSubStatus() {
			return (CodedValue)getValue("MCADDScreeningSubStatus");
		}
		
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @return BSScreeningStatusSubCodes enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes getMCADDScreeningSubStatusEnum() {
			CodedValue cv = (CodedValue)getValue("MCADDScreeningSubStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSScreeningStatusSubCodes", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes)entry;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * @param MCADDScreeningSubStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDScreeningSubStatus(CodedValue MCADDScreeningSubStatus) {
			setValue("MCADDScreeningSubStatus", MCADDScreeningSubStatus);
			return this;
		}
		
		
		/**
		 * A code from the BSScreeningStatusSubCodes vocabulary
  		 * <br>NOTE: This field should be populated using the "BSScreeningStatusSubCodes" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param MCADDScreeningSubStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDScreeningSubStatus(VocabularyEntry MCADDScreeningSubStatus) {
			Code c = new CodedValue(MCADDScreeningSubStatus);
			setValue("MCADDScreeningSubStatus", c);
			return this;
		}
		
		/**
		 * Detail about the MCADD reason
		 * @return String object
		 */	
		public String getMCADDReasonText() {
			return (String)getValue("MCADDReasonText");
		}
		
		
		
		
		/**
		 * Detail about the MCADD reason
		 * @param MCADDReasonText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDReasonText(String MCADDReasonText) {
			setValue("MCADDReasonText", MCADDReasonText);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getMCADDSupplementaryText() {
			return (String)getValue("MCADDSupplementaryText");
		}
		
		
		
		
		/**
		 * 
		 * @param MCADDSupplementaryText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setMCADDSupplementaryText(String MCADDSupplementaryText) {
			setValue("MCADDSupplementaryText", MCADDSupplementaryText);
			return this;
		}
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getScreeningLocationStatusEffectiveTime() {
			return (HL7Date)getValue("ScreeningLocationStatusEffectiveTime");
		}
		
		
		
		
		/**
		 * The effective time of the observation.
		 * <br><br>This field is MANDATORY
		 * @param ScreeningLocationStatusEffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setScreeningLocationStatusEffectiveTime(HL7Date ScreeningLocationStatusEffectiveTime) {
			setValue("ScreeningLocationStatusEffectiveTime", ScreeningLocationStatusEffectiveTime);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "BSScreeningLocStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus
		 * @return CodedValue object
		 */	
		public CodedValue getScreeningLocationStatus() {
			return (CodedValue)getValue("ScreeningLocationStatus");
		}
		
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "BSScreeningLocStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus
		 * @return BSScreeningLocStatus enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus getScreeningLocationStatusEnum() {
			CodedValue cv = (CodedValue)getValue("ScreeningLocationStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("BSScreeningLocStatus", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus)entry;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "BSScreeningLocStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus
		 * @param ScreeningLocationStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setScreeningLocationStatus(CodedValue ScreeningLocationStatus) {
			setValue("ScreeningLocationStatus", ScreeningLocationStatus);
			return this;
		}
		
		
		/**
		 * The value of the observation
  		 * <br>NOTE: This field should be populated using the "BSScreeningLocStatus" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ScreeningLocationStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setScreeningLocationStatus(VocabularyEntry ScreeningLocationStatus) {
			Code c = new CodedValue(ScreeningLocationStatus);
			setValue("ScreeningLocationStatus", c);
			return this;
		}
		
		/**
		 * The value of the LaboratoryCardSerialNumber observation
		 * @return String object
		 */	
		public String getLaboratoryCardSerialNumber() {
			return (String)getValue("LaboratoryCardSerialNumber");
		}
		
		
		
		
		/**
		 * The value of the LaboratoryCardSerialNumber observation
		 * @param LaboratoryCardSerialNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setLaboratoryCardSerialNumber(String LaboratoryCardSerialNumber) {
			setValue("LaboratoryCardSerialNumber", LaboratoryCardSerialNumber);
			return this;
		}
		
		
		/**
		 * The value of the PreviousLaboratoryCardSerialNumber observation
		 * @return String object
		 */	
		public String getPreviousLaboratoryCardSerialNumber() {
			return (String)getValue("PreviousLaboratoryCardSerialNumber");
		}
		
		
		
		
		/**
		 * The value of the PreviousLaboratoryCardSerialNumber observation
		 * @param PreviousLaboratoryCardSerialNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public BloodSpotScreening setPreviousLaboratoryCardSerialNumber(String PreviousLaboratoryCardSerialNumber) {
			setValue("PreviousLaboratoryCardSerialNumber", PreviousLaboratoryCardSerialNumber);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"BATTERY",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146005GB02#BloodSpotScreening",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify each instance of a blood spot battery",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValue", new Field(
												"CodeFixedCodedValue",
												"x:code/@code",
												"428447008",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueCodeSystem", new Field(
												"CodeFixedCodedValueCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeFixedCodedValueDisplayName", new Field(
												"CodeFixedCodedValueDisplayName",
												"x:code/@displayName",
												"newborn blood spot screening",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"The time when the blood spot screening was done",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146005GB02#author",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"ChildScreeningAuthor",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerfTypeCode", new Field(
												"PerfTypeCode",
												"x:performer/@typeCode",
												"PRF",
												"SampleCollectedTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerfTemplateIdRoot", new Field(
												"PerfTemplateIdRoot",
												"x:performer/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SampleCollectedTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerfTemplateId", new Field(
												"PerfTemplateId",
												"x:performer/x:templateId/@extension",
												"COCD_TP146005GB02#performer",
												"SampleCollectedTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerfContentId", new Field(
												"PerfContentId",
												"x:performer/npfitlc:contentId/@extension",
												"COCD_TP145210GB01#AssignedEntity",
												"SampleCollectedTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerfContentIdRoot", new Field(
												"PerfContentIdRoot",
												"x:performer/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"SampleCollectedTime",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SampleCollectedTime", new Field(
												"SampleCollectedTime",
												"x:performer/x:time/@value",
												"The time the performer originally collected the blood spot screening sample",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerClassCode", new Field(
												"PerformerClassCode",
												"x:performer/x:assignedEntity/@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTemplateIdRoot", new Field(
												"PerformerTemplateIdRoot",
												"x:performer/x:assignedEntity/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTemplateId", new Field(
												"PerformerTemplateId",
												"x:performer/x:assignedEntity/x:templateId/@extension",
												"COCD_TP145210GB01#AssignedEntity",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerPersonId", new Field(
												"PerformerPersonId",
												"x:performer/x:assignedEntity/x:id",
												"One or more identifier(s) including local identifiers to identify the person",
												"true",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerJobRoleName", new Field(
												"PerformerJobRoleName",
												"x:performer/x:assignedEntity/x:code",
												"",
												"false",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerAddress", new Field(
												"PerformerAddress",
												"x:performer/x:assignedEntity/x:addr",
												"Contact address for the organisation which is the site where the role is based",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTelephoneNumber", new Field(
												"PerformerTelephoneNumber",
												"x:performer/x:assignedEntity/x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerPersonClassCode", new Field(
												"PerformerPersonClassCode",
												"x:performer/x:assignedEntity/x:assignedPerson/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerPersonDeterminerCode", new Field(
												"PerformerPersonDeterminerCode",
												"x:performer/x:assignedEntity/x:assignedPerson/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerPersonTemplateIdRoot", new Field(
												"PerformerPersonTemplateIdRoot",
												"x:performer/x:assignedEntity/x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerPersonTemplateId", new Field(
												"PerformerPersonTemplateId",
												"x:performer/x:assignedEntity/x:assignedPerson/x:templateId/@extension",
												"COCD_TP145210GB01#assignedPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerPersonName", new Field(
												"PerformerPersonName",
												"x:performer/x:assignedEntity/x:assignedPerson/x:name",
												"Person name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerOrgClassCode", new Field(
												"PerformerOrgClassCode",
												"x:performer/x:assignedEntity/x:representedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerOrgDeterminerCode", new Field(
												"PerformerOrgDeterminerCode",
												"x:performer/x:assignedEntity/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerOrgTemplateIdRoot", new Field(
												"PerformerOrgTemplateIdRoot",
												"x:performer/x:assignedEntity/x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerOrgTemplateId", new Field(
												"PerformerOrgTemplateId",
												"x:performer/x:assignedEntity/x:representedOrganization/x:templateId/@extension",
												"COCD_TP145210GB01#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerOrgId", new Field(
												"PerformerOrgId",
												"x:performer/x:assignedEntity/x:representedOrganization/x:id",
												"An identifier that uniquely identifies the organisation which employs the person in the role",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerOrgName", new Field(
												"PerformerOrgName",
												"x:performer/x:assignedEntity/x:representedOrganization/x:name",
												"Name of the organisation associated with the ODS code",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SecondaryPerformerTypeCode", new Field(
												"SecondaryPerformerTypeCode",
												"x:participant/@typeCode",
												"SPRF",
												"TimeReceivedAtLab",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SecondaryPerformerTemplateIdRoot", new Field(
												"SecondaryPerformerTemplateIdRoot",
												"x:participant/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"TimeReceivedAtLab",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SecondaryPerformerTemplateId", new Field(
												"SecondaryPerformerTemplateId",
												"x:participant/x:templateId/@extension",
												"COCD_TP146005GB02#secondaryPerformer",
												"TimeReceivedAtLab",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SecondaryPerformerContentId", new Field(
												"SecondaryPerformerContentId",
												"x:participant/npfitlc:contentId/@extension",
												"COCD_TP000029GB01#ParticipantRole",
												"TimeReceivedAtLab",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SecondaryPerformerContentIdRoot", new Field(
												"SecondaryPerformerContentIdRoot",
												"x:participant/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"TimeReceivedAtLab",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeReceivedAtLab", new Field(
												"TimeReceivedAtLab",
												"x:participant/x:time/@value",
												"The time the secondary performer (lab) originally received the blood spot screening sample",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabClassCode", new Field(
												"LabClassCode",
												"x:participant/x:participantRole/@classCode",
												"ROL",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabTemplateIdRoot", new Field(
												"LabTemplateIdRoot",
												"x:participant/x:participantRole/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabTemplateId", new Field(
												"LabTemplateId",
												"x:participant/x:participantRole/x:templateId/@extension",
												"COCD_TP000029GB01#ParticipantRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabRoleCode", new Field(
												"LabRoleCode",
												"x:participant/x:participantRole/x:code",
												"A code to describe the organisation's role",
												"false",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabAddr", new Field(
												"LabAddr",
												"x:participant/x:participantRole/x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabTelecom", new Field(
												"LabTelecom",
												"x:participant/x:participantRole/x:telecom",
												"Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabOrgClassCode", new Field(
												"LabOrgClassCode",
												"x:participant/x:participantRole/x:scopingEntity/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabOrgDeterminerCode", new Field(
												"LabOrgDeterminerCode",
												"x:participant/x:participantRole/x:scopingEntity/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabOrgTemplateIdRoot", new Field(
												"LabOrgTemplateIdRoot",
												"x:participant/x:participantRole/x:scopingEntity/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabOrgTemplateId", new Field(
												"LabOrgTemplateId",
												"x:participant/x:participantRole/x:scopingEntity/x:templateId/@extension",
												"COCD_TP000029GB01#scopingOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabOrganisationId", new Field(
												"LabOrganisationId",
												"x:participant/x:participantRole/x:scopingEntity/x:id",
												"An ODS code as an identifier that uniquely identifies the organisation in the role.",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LabOrganisationDescription", new Field(
												"LabOrganisationDescription",
												"x:participant/x:participantRole/x:scopingEntity/x:desc",
												"Description of the organisation associated with the ODS code",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentTemplateId", new Field(
												"PKUComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:templateId/@extension",
												"COCD_TP146005GB02#component",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentTypeCode", new Field(
												"PKUComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/@typeCode",
												"COMP",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentContextConductionInd", new Field(
												"PKUComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/@contextConductionInd",
												"true",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentTemplateIdRoot", new Field(
												"PKUComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentSeperatableInd", new Field(
												"PKUComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:seperatableInd/@value",
												"false",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationClassCode", new Field(
												"PKUComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/@classCode",
												"OBS",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationMoodCode", new Field(
												"PKUComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/@moodCode",
												"EVN",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationTemplateIdRoot", new Field(
												"PKUComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationTemplateId", new Field(
												"PKUComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#pKUScreening",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationCodeFixedCodedValue", new Field(
												"PKUComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:code/@code",
												"314081000",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"PKUComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"PKUComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:code/@displayName",
												"phenylketonuria screening test",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUScreeningValue", new Field(
												"PKUScreeningValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"PKUScreeningResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUScreeningValueType", new Field(
												"PKUScreeningValueType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"PKUScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUBSLaboratoryInterpretationCode", new Field(
												"PKUBSLaboratoryInterpretationCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:interpretationCode",
												"A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result",
												"false",
												"",
												"BSLaboratoryInterpretationCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1TemplateId", new Field(
												"PKUer1TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship1",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1TypeCode", new Field(
												"PKUer1TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/@typeCode",
												"COMP",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ContextConductionInd", new Field(
												"PKUer1ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/@contextConductionInd",
												"true",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1TemplateIdRoot", new Field(
												"PKUer1TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1SeperatableInd", new Field(
												"PKUer1SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:seperatableInd/@value",
												"false",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusTemplateIdRoot", new Field(
												"PKUer1ObservationStatusTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusTemplateId", new Field(
												"PKUer1ObservationStatusTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#pKUScreeningSubStatus",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusClassCode", new Field(
												"PKUer1ObservationStatusClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/@classCode",
												"OBS",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusMoodCode", new Field(
												"PKUer1ObservationStatusMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/@moodCode",
												"EVN",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusCodeFixedCodedValue", new Field(
												"PKUer1ObservationStatusCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:code/@code",
												"PKUSSS",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusCodeFixedCodedValueCodeSystem", new Field(
												"PKUer1ObservationStatusCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer1ObservationStatusCodeFixedCodedValueDisplayName", new Field(
												"PKUer1ObservationStatusCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:code/@displayName",
												"Phenylketonuria (PKU) Screening Status Sub-Code",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUScreeningSubStatus", new Field(
												"PKUScreeningSubStatus",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:value",
												"A code from the BSScreeningStatusSubCodes vocabulary",
												"false",
												"",
												"BSScreeningStatusSubCodes",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUScreeningSubStatusType", new Field(
												"PKUScreeningSubStatusType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship1']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"PKUScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer2TemplateId", new Field(
												"PKUer2TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship2",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer2TypeCode", new Field(
												"PKUer2TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/@typeCode",
												"COMP",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer2ContextConductionInd", new Field(
												"PKUer2ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/@contextConductionInd",
												"true",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer2TemplateIdRoot", new Field(
												"PKUer2TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer2SeperatableInd", new Field(
												"PKUer2SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:seperatableInd/@value",
												"false",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextClassCode", new Field(
												"PKUReasonTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/@classCode",
												"OBS",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextMoodCode", new Field(
												"PKUReasonTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/@moodCode",
												"EVN",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextTemplateId", new Field(
												"PKUReasonTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#pKUReasonText",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextTemplateIdRoot", new Field(
												"PKUReasonTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextCodeFixedCodedValue", new Field(
												"PKUReasonTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:code/@code",
												"PKURTX",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextCodeFixedCodedValueCodeSystem", new Field(
												"PKUReasonTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextCodeFixedCodedValueDisplayName", new Field(
												"PKUReasonTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:code/@displayName",
												"Phenylketonuria (PKU) Reason Text",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonText", new Field(
												"PKUReasonText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:value",
												"Detail about the PKU reason",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUReasonTextType", new Field(
												"PKUReasonTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship2']/x:observation/x:value/@xsi:type",
												"ST",
												"PKUReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer3TemplateId", new Field(
												"PKUer3TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship3",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer3TypeCode", new Field(
												"PKUer3TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/@typeCode",
												"COMP",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer3ContextConductionInd", new Field(
												"PKUer3ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/@contextConductionInd",
												"true",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer3TemplateIdRoot", new Field(
												"PKUer3TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUer3SeperatableInd", new Field(
												"PKUer3SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:seperatableInd/@value",
												"false",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextTemplateIdRoot", new Field(
												"PKUSupplementaryTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextTemplateId", new Field(
												"PKUSupplementaryTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#pKUSupplementaryText",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextClassCode", new Field(
												"PKUSupplementaryTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/@classCode",
												"OBS",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextMoodCode", new Field(
												"PKUSupplementaryTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/@moodCode",
												"EVN",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextCodeFixedCodedValue", new Field(
												"PKUSupplementaryTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:code/@code",
												"PKUSTX",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextCodeFixedCodedValueCodeSystem", new Field(
												"PKUSupplementaryTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextCodeFixedCodedValueDisplayName", new Field(
												"PKUSupplementaryTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:code/@displayName",
												"Phenylketonuria (PKU) Supplementary Text",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryText", new Field(
												"PKUSupplementaryText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PKUSupplementaryTextType", new Field(
												"PKUSupplementaryTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship3']/x:observation/x:value/@xsi:type",
												"ST",
												"PKUSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentTemplateId", new Field(
												"SCDComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:templateId/@extension",
												"COCD_TP146005GB02#component1",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentTypeCode", new Field(
												"SCDComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/@typeCode",
												"COMP",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentContextConductionInd", new Field(
												"SCDComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/@contextConductionInd",
												"true",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentTemplateIdRoot", new Field(
												"SCDComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentSeperatableInd", new Field(
												"SCDComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:seperatableInd/@value",
												"false",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationClassCode", new Field(
												"SCDComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/@classCode",
												"OBS",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationMoodCode", new Field(
												"SCDComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/@moodCode",
												"EVN",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationTemplateIdRoot", new Field(
												"SCDComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationTemplateId", new Field(
												"SCDComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#sCDScreening",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationCodeFixedCodedValue", new Field(
												"SCDComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:code/@code",
												"442186003",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"SCDComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"SCDComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:code/@displayName",
												"screening for sickle cell disease",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDScreeningValue", new Field(
												"SCDScreeningValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"SCDScreeningResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDScreeningValueType", new Field(
												"SCDScreeningValueType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"SCDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDBSLaboratoryInterpretationCode", new Field(
												"SCDBSLaboratoryInterpretationCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:interpretationCode",
												"A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result",
												"false",
												"",
												"BSLaboratoryInterpretationCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1TemplateId", new Field(
												"SCDer1TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship4",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1TypeCode", new Field(
												"SCDer1TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/@typeCode",
												"COMP",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ContextConductionInd", new Field(
												"SCDer1ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/@contextConductionInd",
												"true",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1TemplateIdRoot", new Field(
												"SCDer1TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1SeperatableInd", new Field(
												"SCDer1SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:seperatableInd/@value",
												"false",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusTemplateIdRoot", new Field(
												"SCDer1ObservationStatusTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusTemplateId", new Field(
												"SCDer1ObservationStatusTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#sCDScreeningSubStatus",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusClassCode", new Field(
												"SCDer1ObservationStatusClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/@classCode",
												"OBS",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusMoodCode", new Field(
												"SCDer1ObservationStatusMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/@moodCode",
												"EVN",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusCodeFixedCodedValue", new Field(
												"SCDer1ObservationStatusCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:code/@code",
												"SCDSSS",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusCodeFixedCodedValueCodeSystem", new Field(
												"SCDer1ObservationStatusCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer1ObservationStatusCodeFixedCodedValueDisplayName", new Field(
												"SCDer1ObservationStatusCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:code/@displayName",
												"Sickle-cell disease (SCD) Screening Status Sub-Code",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDScreeningSubStatus", new Field(
												"SCDScreeningSubStatus",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:value",
												"A code from the BSScreeningStatusSubCodes vocabulary",
												"false",
												"",
												"BSScreeningStatusSubCodes",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDScreeningSubStatusType", new Field(
												"SCDScreeningSubStatusType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship4']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"SCDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer2TemplateId", new Field(
												"SCDer2TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship5",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer2TypeCode", new Field(
												"SCDer2TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/@typeCode",
												"COMP",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer2ContextConductionInd", new Field(
												"SCDer2ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/@contextConductionInd",
												"true",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer2TemplateIdRoot", new Field(
												"SCDer2TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer2SeperatableInd", new Field(
												"SCDer2SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:seperatableInd/@value",
												"false",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextClassCode", new Field(
												"SCDReasonTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/@classCode",
												"OBS",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextMoodCode", new Field(
												"SCDReasonTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/@moodCode",
												"EVN",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextTemplateId", new Field(
												"SCDReasonTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#sCDReasonText",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextTemplateIdRoot", new Field(
												"SCDReasonTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextCodeFixedCodedValue", new Field(
												"SCDReasonTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:code/@code",
												"SCDRTX",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextCodeFixedCodedValueCodeSystem", new Field(
												"SCDReasonTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextCodeFixedCodedValueDisplayName", new Field(
												"SCDReasonTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:code/@displayName",
												"Sickle-cell disease (SCD) Reason Text",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonText", new Field(
												"SCDReasonText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:value",
												"Detail about the SCD reason",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDReasonTextType", new Field(
												"SCDReasonTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship5']/x:observation/x:value/@xsi:type",
												"ST",
												"SCDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer3TemplateId", new Field(
												"SCDer3TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship6",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer3TypeCode", new Field(
												"SCDer3TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/@typeCode",
												"COMP",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer3ContextConductionInd", new Field(
												"SCDer3ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/@contextConductionInd",
												"true",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer3TemplateIdRoot", new Field(
												"SCDer3TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDer3SeperatableInd", new Field(
												"SCDer3SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:seperatableInd/@value",
												"false",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextTemplateIdRoot", new Field(
												"SCDSupplementaryTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextTemplateId", new Field(
												"SCDSupplementaryTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#sCDSupplementaryText",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextClassCode", new Field(
												"SCDSupplementaryTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/@classCode",
												"OBS",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextMoodCode", new Field(
												"SCDSupplementaryTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/@moodCode",
												"EVN",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextCodeFixedCodedValue", new Field(
												"SCDSupplementaryTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:code/@code",
												"SCDSTX",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextCodeFixedCodedValueCodeSystem", new Field(
												"SCDSupplementaryTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextCodeFixedCodedValueDisplayName", new Field(
												"SCDSupplementaryTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:code/@displayName",
												"Sickle-cell disease (SCD) Supplementary Text",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryText", new Field(
												"SCDSupplementaryText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SCDSupplementaryTextType", new Field(
												"SCDSupplementaryTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component1']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship6']/x:observation/x:value/@xsi:type",
												"ST",
												"SCDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentTemplateId", new Field(
												"CFComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:templateId/@extension",
												"COCD_TP146005GB02#component2",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentTypeCode", new Field(
												"CFComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/@typeCode",
												"COMP",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentContextConductionInd", new Field(
												"CFComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/@contextConductionInd",
												"true",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentTemplateIdRoot", new Field(
												"CFComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentSeperatableInd", new Field(
												"CFComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:seperatableInd/@value",
												"false",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationClassCode", new Field(
												"CFComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/@classCode",
												"OBS",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationMoodCode", new Field(
												"CFComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/@moodCode",
												"EVN",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationTemplateIdRoot", new Field(
												"CFComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationTemplateId", new Field(
												"CFComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cFScreening",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationCodeFixedCodedValue", new Field(
												"CFComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:code/@code",
												"314080004",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"CFComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"CFComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:code/@displayName",
												"cystic fibrosis screening test",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFScreeningValue", new Field(
												"CFScreeningValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"CFScreeningResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFScreeningValueType", new Field(
												"CFScreeningValueType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"CFScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFBSLaboratoryInterpretationCode", new Field(
												"CFBSLaboratoryInterpretationCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:interpretationCode",
												"A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result",
												"false",
												"",
												"BSLaboratoryInterpretationCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1TemplateId", new Field(
												"CFer1TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship7",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1TypeCode", new Field(
												"CFer1TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/@typeCode",
												"COMP",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ContextConductionInd", new Field(
												"CFer1ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/@contextConductionInd",
												"true",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1TemplateIdRoot", new Field(
												"CFer1TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1SeperatableInd", new Field(
												"CFer1SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:seperatableInd/@value",
												"false",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusTemplateIdRoot", new Field(
												"CFer1ObservationStatusTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusTemplateId", new Field(
												"CFer1ObservationStatusTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cFScreeningSubStatus",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusClassCode", new Field(
												"CFer1ObservationStatusClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/@classCode",
												"OBS",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusMoodCode", new Field(
												"CFer1ObservationStatusMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/@moodCode",
												"EVN",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusCodeFixedCodedValue", new Field(
												"CFer1ObservationStatusCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:code/@code",
												"CFSSS",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusCodeFixedCodedValueCodeSystem", new Field(
												"CFer1ObservationStatusCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer1ObservationStatusCodeFixedCodedValueDisplayName", new Field(
												"CFer1ObservationStatusCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:code/@displayName",
												"Cystic fibrosis (CF) Screening Status Sub-Code",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFScreeningSubStatus", new Field(
												"CFScreeningSubStatus",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:value",
												"A code from the BSScreeningStatusSubCodes vocabulary",
												"false",
												"",
												"BSScreeningStatusSubCodes",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFScreeningSubStatusType", new Field(
												"CFScreeningSubStatusType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship7']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"CFScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer2TemplateId", new Field(
												"CFer2TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship8",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer2TypeCode", new Field(
												"CFer2TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/@typeCode",
												"COMP",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer2ContextConductionInd", new Field(
												"CFer2ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/@contextConductionInd",
												"true",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer2TemplateIdRoot", new Field(
												"CFer2TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer2SeperatableInd", new Field(
												"CFer2SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:seperatableInd/@value",
												"false",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextClassCode", new Field(
												"CFReasonTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/@classCode",
												"OBS",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextMoodCode", new Field(
												"CFReasonTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/@moodCode",
												"EVN",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextTemplateId", new Field(
												"CFReasonTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cFReasonText",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextTemplateIdRoot", new Field(
												"CFReasonTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextCodeFixedCodedValue", new Field(
												"CFReasonTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:code/@code",
												"CFRTX",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextCodeFixedCodedValueCodeSystem", new Field(
												"CFReasonTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextCodeFixedCodedValueDisplayName", new Field(
												"CFReasonTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:code/@displayName",
												"Cystic fibrosis (CF) Reason Text",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonText", new Field(
												"CFReasonText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:value",
												"Detail about the CF reason",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFReasonTextType", new Field(
												"CFReasonTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship8']/x:observation/x:value/@xsi:type",
												"ST",
												"CFReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer3TemplateId", new Field(
												"CFer3TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship9",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer3TypeCode", new Field(
												"CFer3TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/@typeCode",
												"COMP",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer3ContextConductionInd", new Field(
												"CFer3ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/@contextConductionInd",
												"true",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer3TemplateIdRoot", new Field(
												"CFer3TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFer3SeperatableInd", new Field(
												"CFer3SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:seperatableInd/@value",
												"false",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextTemplateIdRoot", new Field(
												"CFSupplementaryTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextTemplateId", new Field(
												"CFSupplementaryTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cFSupplementaryText",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextClassCode", new Field(
												"CFSupplementaryTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/@classCode",
												"OBS",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextMoodCode", new Field(
												"CFSupplementaryTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/@moodCode",
												"EVN",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextCodeFixedCodedValue", new Field(
												"CFSupplementaryTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:code/@code",
												"CFSTX",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextCodeFixedCodedValueCodeSystem", new Field(
												"CFSupplementaryTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextCodeFixedCodedValueDisplayName", new Field(
												"CFSupplementaryTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:code/@displayName",
												"Cystic fibrosis (CF) Supplementary Text",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryText", new Field(
												"CFSupplementaryText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CFSupplementaryTextType", new Field(
												"CFSupplementaryTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component2']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship9']/x:observation/x:value/@xsi:type",
												"ST",
												"CFSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentTemplateId", new Field(
												"CHTComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:templateId/@extension",
												"COCD_TP146005GB02#component3",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentTypeCode", new Field(
												"CHTComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/@typeCode",
												"COMP",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentContextConductionInd", new Field(
												"CHTComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/@contextConductionInd",
												"true",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentTemplateIdRoot", new Field(
												"CHTComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentSeperatableInd", new Field(
												"CHTComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:seperatableInd/@value",
												"false",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationClassCode", new Field(
												"CHTComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/@classCode",
												"OBS",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationMoodCode", new Field(
												"CHTComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/@moodCode",
												"EVN",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationTemplateIdRoot", new Field(
												"CHTComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationTemplateId", new Field(
												"CHTComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cHTScreening",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationCodeFixedCodedValue", new Field(
												"CHTComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:code/@code",
												"400984005",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"CHTComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"CHTComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:code/@displayName",
												"congenital hypothyroidism screening test",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTScreeningValue", new Field(
												"CHTScreeningValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"CHTScreeningResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTScreeningValueType", new Field(
												"CHTScreeningValueType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"CHTScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTBSLaboratoryInterpretationCode", new Field(
												"CHTBSLaboratoryInterpretationCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:interpretationCode",
												"A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result",
												"false",
												"",
												"BSLaboratoryInterpretationCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1TemplateId", new Field(
												"CHTer1TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship10",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1TypeCode", new Field(
												"CHTer1TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/@typeCode",
												"COMP",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ContextConductionInd", new Field(
												"CHTer1ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/@contextConductionInd",
												"true",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1TemplateIdRoot", new Field(
												"CHTer1TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1SeperatableInd", new Field(
												"CHTer1SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:seperatableInd/@value",
												"false",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusTemplateIdRoot", new Field(
												"CHTer1ObservationStatusTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusTemplateId", new Field(
												"CHTer1ObservationStatusTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cHTScreeningSubStatus",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusClassCode", new Field(
												"CHTer1ObservationStatusClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/@classCode",
												"OBS",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusMoodCode", new Field(
												"CHTer1ObservationStatusMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/@moodCode",
												"EVN",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusCodeFixedCodedValue", new Field(
												"CHTer1ObservationStatusCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:code/@code",
												"CHTSSS",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusCodeFixedCodedValueCodeSystem", new Field(
												"CHTer1ObservationStatusCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer1ObservationStatusCodeFixedCodedValueDisplayName", new Field(
												"CHTer1ObservationStatusCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:code/@displayName",
												"Congenital hypothyroidism (CHT) Screening Status Sub-Code",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTScreeningSubStatus", new Field(
												"CHTScreeningSubStatus",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:value",
												"A code from the BSScreeningStatusSubCodes vocabulary",
												"false",
												"",
												"BSScreeningStatusSubCodes",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTScreeningSubStatusType", new Field(
												"CHTScreeningSubStatusType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship10']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"CHTScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer2TemplateId", new Field(
												"CHTer2TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship11",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer2TypeCode", new Field(
												"CHTer2TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/@typeCode",
												"COMP",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer2ContextConductionInd", new Field(
												"CHTer2ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/@contextConductionInd",
												"true",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer2TemplateIdRoot", new Field(
												"CHTer2TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer2SeperatableInd", new Field(
												"CHTer2SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:seperatableInd/@value",
												"false",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextClassCode", new Field(
												"CHTReasonTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/@classCode",
												"OBS",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextMoodCode", new Field(
												"CHTReasonTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/@moodCode",
												"EVN",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextTemplateId", new Field(
												"CHTReasonTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cHTReasonText",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextTemplateIdRoot", new Field(
												"CHTReasonTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextCodeFixedCodedValue", new Field(
												"CHTReasonTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:code/@code",
												"CHTRTX",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextCodeFixedCodedValueCodeSystem", new Field(
												"CHTReasonTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextCodeFixedCodedValueDisplayName", new Field(
												"CHTReasonTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:code/@displayName",
												"Congenital hypothyroidism (CHT) Reason Text",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonText", new Field(
												"CHTReasonText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:value",
												"Detail about the CHT reason",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTReasonTextType", new Field(
												"CHTReasonTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship11']/x:observation/x:value/@xsi:type",
												"ST",
												"CHTReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer3TemplateId", new Field(
												"CHTer3TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship12",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer3TypeCode", new Field(
												"CHTer3TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/@typeCode",
												"COMP",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer3ContextConductionInd", new Field(
												"CHTer3ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/@contextConductionInd",
												"true",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer3TemplateIdRoot", new Field(
												"CHTer3TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTer3SeperatableInd", new Field(
												"CHTer3SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:seperatableInd/@value",
												"false",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextTemplateIdRoot", new Field(
												"CHTSupplementaryTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextTemplateId", new Field(
												"CHTSupplementaryTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#cHTSupplementaryText",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextClassCode", new Field(
												"CHTSupplementaryTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/@classCode",
												"OBS",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextMoodCode", new Field(
												"CHTSupplementaryTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/@moodCode",
												"EVN",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextCodeFixedCodedValue", new Field(
												"CHTSupplementaryTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:code/@code",
												"CHTSTX",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextCodeFixedCodedValueCodeSystem", new Field(
												"CHTSupplementaryTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextCodeFixedCodedValueDisplayName", new Field(
												"CHTSupplementaryTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:code/@displayName",
												"Congenital hypothyroidism (CHT) Supplementary Text",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryText", new Field(
												"CHTSupplementaryText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CHTSupplementaryTextType", new Field(
												"CHTSupplementaryTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component3']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship12']/x:observation/x:value/@xsi:type",
												"ST",
												"CHTSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentTemplateId", new Field(
												"MCADDComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:templateId/@extension",
												"COCD_TP146005GB02#component4",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentTypeCode", new Field(
												"MCADDComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/@typeCode",
												"COMP",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentContextConductionInd", new Field(
												"MCADDComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/@contextConductionInd",
												"true",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentTemplateIdRoot", new Field(
												"MCADDComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentSeperatableInd", new Field(
												"MCADDComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:seperatableInd/@value",
												"false",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationClassCode", new Field(
												"MCADDComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/@classCode",
												"OBS",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationMoodCode", new Field(
												"MCADDComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/@moodCode",
												"EVN",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationTemplateIdRoot", new Field(
												"MCADDComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationTemplateId", new Field(
												"MCADDComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#mCADDScreening",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationCodeFixedCodedValue", new Field(
												"MCADDComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:code/@code",
												"428056008",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"MCADDComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"MCADDComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:code/@displayName",
												"medium-chain acyl-coenzyme A dehydrogenase deficiency screening test",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDScreeningValue", new Field(
												"MCADDScreeningValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"MCADDScreeningResult",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDScreeningValueType", new Field(
												"MCADDScreeningValueType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"MCADDScreeningValue",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDBSLaboratoryInterpretationCode", new Field(
												"MCADDBSLaboratoryInterpretationCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:interpretationCode",
												"A code from the BSLaboratoryInterpretationCode vocabulary to describe this as a laboratory interpretation code for screening result",
												"false",
												"",
												"BSLaboratoryInterpretationCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1TemplateId", new Field(
												"MCADDer1TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship13",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1TypeCode", new Field(
												"MCADDer1TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/@typeCode",
												"COMP",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ContextConductionInd", new Field(
												"MCADDer1ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/@contextConductionInd",
												"true",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1TemplateIdRoot", new Field(
												"MCADDer1TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1SeperatableInd", new Field(
												"MCADDer1SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:seperatableInd/@value",
												"false",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusTemplateIdRoot", new Field(
												"MCADDer1ObservationStatusTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusTemplateId", new Field(
												"MCADDer1ObservationStatusTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#mCDScreeningSubStatus",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusClassCode", new Field(
												"MCADDer1ObservationStatusClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/@classCode",
												"OBS",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusMoodCode", new Field(
												"MCADDer1ObservationStatusMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/@moodCode",
												"EVN",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusCodeFixedCodedValue", new Field(
												"MCADDer1ObservationStatusCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:code/@code",
												"MCDSSS",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusCodeFixedCodedValueCodeSystem", new Field(
												"MCADDer1ObservationStatusCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer1ObservationStatusCodeFixedCodedValueDisplayName", new Field(
												"MCADDer1ObservationStatusCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:code/@displayName",
												"Medium-chain acyl-CoA dehydrogenase deficiency (MCADD) Screening Status Sub-Code",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDScreeningSubStatus", new Field(
												"MCADDScreeningSubStatus",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:value",
												"A code from the BSScreeningStatusSubCodes vocabulary",
												"false",
												"",
												"BSScreeningStatusSubCodes",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDScreeningSubStatusType", new Field(
												"MCADDScreeningSubStatusType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship13']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"MCADDScreeningSubStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer2TemplateId", new Field(
												"MCADDer2TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship14",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer2TypeCode", new Field(
												"MCADDer2TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/@typeCode",
												"COMP",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer2ContextConductionInd", new Field(
												"MCADDer2ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/@contextConductionInd",
												"true",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer2TemplateIdRoot", new Field(
												"MCADDer2TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer2SeperatableInd", new Field(
												"MCADDer2SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:seperatableInd/@value",
												"false",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextClassCode", new Field(
												"MCADDReasonTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/@classCode",
												"OBS",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextMoodCode", new Field(
												"MCADDReasonTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/@moodCode",
												"EVN",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextTemplateId", new Field(
												"MCADDReasonTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#mCDReasonText",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextTemplateIdRoot", new Field(
												"MCADDReasonTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextCodeFixedCodedValue", new Field(
												"MCADDReasonTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:code/@code",
												"MCDRTX",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextCodeFixedCodedValueCodeSystem", new Field(
												"MCADDReasonTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextCodeFixedCodedValueDisplayName", new Field(
												"MCADDReasonTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:code/@displayName",
												"Medium-chain acyl-CoA dehydrogenase deficiency (MCADD) Reason Text",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonText", new Field(
												"MCADDReasonText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:value",
												"Detail about the MCADD reason",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDReasonTextType", new Field(
												"MCADDReasonTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship14']/x:observation/x:value/@xsi:type",
												"ST",
												"MCADDReasonText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer3TemplateId", new Field(
												"MCADDer3TemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:templateId/@extension",
												"COCD_TP146005GB02#entryRelationship15",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer3TypeCode", new Field(
												"MCADDer3TypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/@typeCode",
												"COMP",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer3ContextConductionInd", new Field(
												"MCADDer3ContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/@contextConductionInd",
												"true",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer3TemplateIdRoot", new Field(
												"MCADDer3TemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDer3SeperatableInd", new Field(
												"MCADDer3SeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:seperatableInd/@value",
												"false",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextTemplateIdRoot", new Field(
												"MCADDSupplementaryTextTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextTemplateId", new Field(
												"MCADDSupplementaryTextTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#mCDSupplementaryText",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextClassCode", new Field(
												"MCADDSupplementaryTextClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/@classCode",
												"OBS",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextMoodCode", new Field(
												"MCADDSupplementaryTextMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/@moodCode",
												"EVN",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextCodeFixedCodedValue", new Field(
												"MCADDSupplementaryTextCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:code/@code",
												"MCDSTX",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextCodeFixedCodedValueCodeSystem", new Field(
												"MCADDSupplementaryTextCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextCodeFixedCodedValueDisplayName", new Field(
												"MCADDSupplementaryTextCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:code/@displayName",
												"Medium-chain acyl-CoA dehydrogenase deficiency (MCADD) Supplementary Text",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryText", new Field(
												"MCADDSupplementaryText",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MCADDSupplementaryTextType", new Field(
												"MCADDSupplementaryTextType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component4']/x:observation/x:entryRelationship[x:templateId/@extension='COCD_TP146005GB02#entryRelationship15']/x:observation/x:value/@xsi:type",
												"ST",
												"MCADDSupplementaryText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentTemplateId", new Field(
												"ScreeningLocationStatusComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:templateId/@extension",
												"COCD_TP146005GB02#component5",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentTypeCode", new Field(
												"ScreeningLocationStatusComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/@typeCode",
												"COMP",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentContextConductionInd", new Field(
												"ScreeningLocationStatusComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/@contextConductionInd",
												"true",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentTemplateIdRoot", new Field(
												"ScreeningLocationStatusComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentSeperatableInd", new Field(
												"ScreeningLocationStatusComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:seperatableInd/@value",
												"false",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationClassCode", new Field(
												"ScreeningLocationStatusComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/@classCode",
												"OBS",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationMoodCode", new Field(
												"ScreeningLocationStatusComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/@moodCode",
												"EVN",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationTemplateIdRoot", new Field(
												"ScreeningLocationStatusComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationTemplateId", new Field(
												"ScreeningLocationStatusComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#locStatus",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationCodeFixedCodedValue", new Field(
												"ScreeningLocationStatusComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:code/@code",
												"LOCSTS",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"ScreeningLocationStatusComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"ScreeningLocationStatusComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:code/@displayName",
												"Location Status",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusEffectiveTime", new Field(
												"ScreeningLocationStatusEffectiveTime",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:effectiveTime/@value",
												"The effective time of the observation.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatus", new Field(
												"ScreeningLocationStatus",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:value",
												"The value of the observation",
												"false",
												"",
												"BSScreeningLocStatus",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ScreeningLocationStatusType", new Field(
												"ScreeningLocationStatusType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component5']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"ScreeningLocationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentTemplateId", new Field(
												"LaboratoryCardSerialNumberComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:templateId/@extension",
												"COCD_TP146005GB02#component6",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentTypeCode", new Field(
												"LaboratoryCardSerialNumberComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/@typeCode",
												"COMP",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentContextConductionInd", new Field(
												"LaboratoryCardSerialNumberComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/@contextConductionInd",
												"true",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentTemplateIdRoot", new Field(
												"LaboratoryCardSerialNumberComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentSeperatableInd", new Field(
												"LaboratoryCardSerialNumberComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:seperatableInd/@value",
												"false",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationClassCode", new Field(
												"LaboratoryCardSerialNumberComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/@classCode",
												"OBS",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationMoodCode", new Field(
												"LaboratoryCardSerialNumberComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/@moodCode",
												"EVN",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationTemplateIdRoot", new Field(
												"LaboratoryCardSerialNumberComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationTemplateId", new Field(
												"LaboratoryCardSerialNumberComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#labCardSerialNum",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationCodeFixedCodedValue", new Field(
												"LaboratoryCardSerialNumberComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:code/@code",
												"LABSNO",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"LaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"LaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:code/@displayName",
												"Laboratory Card Serial Number",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumber", new Field(
												"LaboratoryCardSerialNumber",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:value",
												"The value of the LaboratoryCardSerialNumber observation",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LaboratoryCardSerialNumberType", new Field(
												"LaboratoryCardSerialNumberType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component6']/x:observation/x:value/@xsi:type",
												"ST",
												"LaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentTemplateId", new Field(
												"PreviousLaboratoryCardSerialNumberComponentTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:templateId/@extension",
												"COCD_TP146005GB02#component7",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentTypeCode", new Field(
												"PreviousLaboratoryCardSerialNumberComponentTypeCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/@typeCode",
												"COMP",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentContextConductionInd", new Field(
												"PreviousLaboratoryCardSerialNumberComponentContextConductionInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/@contextConductionInd",
												"true",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentTemplateIdRoot", new Field(
												"PreviousLaboratoryCardSerialNumberComponentTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentSeperatableInd", new Field(
												"PreviousLaboratoryCardSerialNumberComponentSeperatableInd",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:seperatableInd/@value",
												"false",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationClassCode", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationClassCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/@classCode",
												"OBS",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationMoodCode", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationMoodCode",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/@moodCode",
												"EVN",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationTemplateIdRoot", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationTemplateIdRoot",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationTemplateId", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationTemplateId",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:templateId/@extension",
												"COCD_TP146005GB02#prvLabCardSerialNum",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationCodeFixedCodedValue", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationCodeFixedCodedValue",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:code/@code",
												"LABSNP",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueCodeSystem", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueCodeSystem",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.220",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueDisplayName", new Field(
												"PreviousLaboratoryCardSerialNumberComponentObservationCodeFixedCodedValueDisplayName",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:code/@displayName",
												"Previous Laboratory Card Serial Number",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumber", new Field(
												"PreviousLaboratoryCardSerialNumber",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:value",
												"The value of the PreviousLaboratoryCardSerialNumber observation",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousLaboratoryCardSerialNumberType", new Field(
												"PreviousLaboratoryCardSerialNumberType",
												"x:component[x:templateId/@extension='COCD_TP146005GB02#component7']/x:observation/x:value/@xsi:type",
												"ST",
												"PreviousLaboratoryCardSerialNumber",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public BloodSpotScreening() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public BloodSpotScreening(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	