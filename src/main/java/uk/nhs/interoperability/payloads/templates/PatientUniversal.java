/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PatientUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PatientID PatientID}&gt; PatientID</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; PatientName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Sex</li>
 * <li>String SexNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} BirthTime</li>
 * <li>String BirthTimeNullFlavour</li>
 * <li>List&lt;{@link LanguageCommunication LanguageCommunication}&gt; Languages</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} RegisteredGPOrgId</li>
 * <li>String RegisteredGPOrgName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; RegisteredGPTelephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} RegisteredGPAddress</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PatientUniversal extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "PatientUniversal";
		protected static final String shortName = "PatientRole";
		protected static final String rootNode = "x:patientRole";
		protected static final String version = "COCD_TP145201GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param PatientID PatientID
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param PatientName PatientName
		 * @param Sex Sex
		 * @param SexNullFlavour SexNullFlavour
		 * @param BirthTime BirthTime
		 * @param BirthTimeNullFlavour BirthTimeNullFlavour
		 * @param Languages Languages
		 * @param RegisteredGPOrgId RegisteredGPOrgId
		 * @param RegisteredGPOrgName RegisteredGPOrgName
		 * @param RegisteredGPTelephone RegisteredGPTelephone
		 * @param RegisteredGPAddress RegisteredGPAddress
		 */
	    public PatientUniversal(
		List<uk.nhs.interoperability.payloads.commontypes.PatientID> PatientID, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> PatientName, CodedValue Sex, String SexNullFlavour, HL7Date BirthTime, String BirthTimeNullFlavour, 
		List<LanguageCommunication> Languages, uk.nhs.interoperability.payloads.commontypes.OrgID RegisteredGPOrgId, String RegisteredGPOrgName, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> RegisteredGPTelephone, uk.nhs.interoperability.payloads.commontypes.Address RegisteredGPAddress) {
			fields = new LinkedHashMap<String, Object>();
			
			setPatientID(PatientID);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setPatientName(PatientName);
			setSex(Sex);
			setSexNullFlavour(SexNullFlavour);
			setBirthTime(BirthTime);
			setBirthTimeNullFlavour(BirthTimeNullFlavour);
			setLanguages(Languages);
			setRegisteredGPOrgId(RegisteredGPOrgId);
			setRegisteredGPOrgName(RegisteredGPOrgName);
			setRegisteredGPTelephone(RegisteredGPTelephone);
			setRegisteredGPAddress(RegisteredGPAddress);
		}
	
		/**
		 * Identifier for the patient (NHS number or a local ID)
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PatientID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PatientID> getPatientID() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PatientID>)getValue("PatientID");
		}
		
		/**
		 * Identifier for the patient (NHS number or a local ID)
		 * <br><br>This field is MANDATORY
		 * @param PatientID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setPatientID(List PatientID) {
			setValue("PatientID", PatientID);
			return this;
		}
		
		/**
		 * Identifier for the patient (NHS number or a local ID)
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PatientID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PatientID objects.
		 * @param PatientID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal addPatientID(uk.nhs.interoperability.payloads.commontypes.PatientID PatientID) {
			addMultivalue("PatientID", PatientID);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * 
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getPatientName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("PatientName");
		}
		
		/**
		 * 
		 * @param PatientName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setPatientName(List PatientName) {
			setValue("PatientName", PatientName);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param PatientName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal addPatientName(uk.nhs.interoperability.payloads.commontypes.PersonName PatientName) {
			addMultivalue("PatientName", PatientName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return CodedValue object
		 */	
		public CodedValue getSex() {
			return (CodedValue)getValue("Sex");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getSexEnum() {
			CodedValue cv = (CodedValue)getValue("Sex");
			VocabularyEntry entry = VocabularyFactory.getVocab("Sex", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Sex)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Sex value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setSex(CodedValue Sex) {
			setValue("Sex", Sex);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Sex value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setSex(VocabularyEntry Sex) {
			Code c = new CodedValue(Sex);
			setValue("Sex", c);
			return this;
		}
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getSexNullFlavour() {
			return (String)getValue("SexNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getSexNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("SexNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param SexNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setSexNullFlavour(String SexNullFlavour) {
			setValue("SexNullFlavour", SexNullFlavour);
			return this;
		}
		
		
		/**
		 * 
		 * @return HL7Date object
		 */	
		public HL7Date getBirthTime() {
			return (HL7Date)getValue("BirthTime");
		}
		
		
		
		
		/**
		 * 
		 * @param BirthTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setBirthTime(HL7Date BirthTime) {
			setValue("BirthTime", BirthTime);
			return this;
		}
		
		
		/**
		 * A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getBirthTimeNullFlavour() {
			return (String)getValue("BirthTimeNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getBirthTimeNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("BirthTimeNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param BirthTimeNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setBirthTimeNullFlavour(String BirthTimeNullFlavour) {
			setValue("BirthTimeNullFlavour", BirthTimeNullFlavour);
			return this;
		}
		
		
		/**
		 * Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language
		 * @return List of LanguageCommunication objects
		 */
		public List<LanguageCommunication> getLanguages() {
			return (List<LanguageCommunication>)getValue("Languages");
		}
		
		/**
		 * Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language
		 * @param Languages value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setLanguages(List Languages) {
			setValue("Languages", Languages);
			return this;
		}
		
		/**
		 * Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language
		 * <br>Note: This adds a LanguageCommunication object, but this method can be called
		 * multiple times to add additional LanguageCommunication objects.
		 * @param Languages value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal addLanguages(LanguageCommunication Languages) {
			addMultivalue("Languages", Languages);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getRegisteredGPOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("RegisteredGPOrgId");
		}
		
		
		
		
		/**
		 * 
		 * @param RegisteredGPOrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setRegisteredGPOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID RegisteredGPOrgId) {
			setValue("RegisteredGPOrgId", RegisteredGPOrgId);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getRegisteredGPOrgName() {
			return (String)getValue("RegisteredGPOrgName");
		}
		
		
		
		
		/**
		 * 
		 * @param RegisteredGPOrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setRegisteredGPOrgName(String RegisteredGPOrgName) {
			setValue("RegisteredGPOrgName", RegisteredGPOrgName);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getRegisteredGPTelephone() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("RegisteredGPTelephone");
		}
		
		/**
		 * 
		 * @param RegisteredGPTelephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setRegisteredGPTelephone(List RegisteredGPTelephone) {
			setValue("RegisteredGPTelephone", RegisteredGPTelephone);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param RegisteredGPTelephone value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal addRegisteredGPTelephone(uk.nhs.interoperability.payloads.commontypes.Telecom RegisteredGPTelephone) {
			addMultivalue("RegisteredGPTelephone", RegisteredGPTelephone);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getRegisteredGPAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("RegisteredGPAddress");
		}
		
		
		
		
		/**
		 * 
		 * @param RegisteredGPAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientUniversal setRegisteredGPAddress(uk.nhs.interoperability.payloads.commontypes.Address RegisteredGPAddress) {
			setValue("RegisteredGPAddress", RegisteredGPAddress);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PAT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145201GB01#PatientRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientID", new Field(
												"PatientID",
												"x:id",
												"Identifier for the patient (NHS number or a local ID)",
												"true",
												"",
												"",
												"PatientID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"5",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientClassCode", new Field(
												"PatientClassCode",
												"x:patient/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientDeterminerCode", new Field(
												"PatientDeterminerCode",
												"x:patient/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTemplateIdRoot", new Field(
												"PatientTemplateIdRoot",
												"x:patient/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTemplateId", new Field(
												"PatientTemplateId",
												"x:patient/x:templateId/@extension",
												"COCD_TP145201GB01#patientPatient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientName", new Field(
												"PatientName",
												"x:patient/x:name",
												"",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Sex", new Field(
												"Sex",
												"x:patient/x:administrativeGenderCode",
												"",
												"false",
												"",
												"Sex",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SexNullFlavour", new Field(
												"SexNullFlavour",
												"x:patient/x:administrativeGenderCode/@nullFlavor",
												"A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthTime", new Field(
												"BirthTime",
												"x:patient/x:birthTime/@value",
												"",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BirthTimeNullFlavour", new Field(
												"BirthTimeNullFlavour",
												"x:patient/x:birthTime/@nullFlavor",
												"A null flavour can be used when the birth time cannot be provided. This must only be used when the birth time field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Languages", new Field(
												"Languages",
												"x:patient/x:languageCommunication",
												"Information about the patient's languages, their mode of communication and proficiency in that language, and whether it is their preferred language",
												"false",
												"",
												"",
												"LanguageCommunication",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationClassCode", new Field(
												"OrganisationClassCode",
												"x:providerOrganization/@classCode",
												"ORG",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationDeterminerCode", new Field(
												"OrganisationDeterminerCode",
												"x:providerOrganization/@determinerCode",
												"INSTANCE",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTemplateIdRoot", new Field(
												"OrganisationTemplateIdRoot",
												"x:providerOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTemplateId", new Field(
												"OrganisationTemplateId",
												"x:providerOrganization/x:templateId/@extension",
												"COCD_TP145201GB01#providerOrganization",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPOrgId", new Field(
												"RegisteredGPOrgId",
												"x:providerOrganization/x:id",
												"",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPOrgName", new Field(
												"RegisteredGPOrgName",
												"x:providerOrganization/x:name",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPTelephone", new Field(
												"RegisteredGPTelephone",
												"x:providerOrganization/x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPAddress", new Field(
												"RegisteredGPAddress",
												"x:providerOrganization/x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAOrganizationProviderTypeCode", new Field(
												"CDAOrganizationProviderTypeCode",
												"x:providerOrganization/x:standardIndustryClassCode/@code",
												"001",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAOrganizationProviderTypeCodeSystem", new Field(
												"CDAOrganizationProviderTypeCodeSystem",
												"x:providerOrganization/x:standardIndustryClassCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.289",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAOrganizationProviderTypeDisplayName", new Field(
												"CDAOrganizationProviderTypeDisplayName",
												"x:providerOrganization/x:standardIndustryClassCode/@displayName",
												"GP Practice",
												"RegisteredGPOrgId",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PatientUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PatientUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	
	
	