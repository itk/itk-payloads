/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Patientv2LanguageCommunication object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Language</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Mode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ProficiencyLevel</li>
 * <li>String PreferenceInd</li>
 * <li>String PreferenceIndNullFlavour</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Patientv2LanguageCommunication extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "Patientv2LanguageCommunication";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Language Language
		 * @param Mode Mode
		 * @param ProficiencyLevel ProficiencyLevel
		 * @param PreferenceInd PreferenceInd
		 * @param PreferenceIndNullFlavour PreferenceIndNullFlavour
		 */
	    public Patientv2LanguageCommunication(String Language, CodedValue Mode, CodedValue ProficiencyLevel, String PreferenceInd, String PreferenceIndNullFlavour) {
			fields = new LinkedHashMap<String, Object>();
			
			setLanguage(Language);
			setMode(Mode);
			setProficiencyLevel(ProficiencyLevel);
			setPreferenceInd(PreferenceInd);
			setPreferenceIndNullFlavour(PreferenceIndNullFlavour);
		}
	
		/**
		 * This is a simple string taken from the HumanLanguage vocabulary
		 * @return String object
		 */	
		public String getLanguage() {
			return (String)getValue("Language");
		}
		
		
		
		
		/**
		 * This is a simple string taken from the HumanLanguage vocabulary
		 * @param Language value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setLanguage(String Language) {
			setValue("Language", Language);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode
		 * @return CodedValue object
		 */	
		public CodedValue getMode() {
			return (CodedValue)getValue("Mode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode
		 * @return LanguageAbilityMode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode getModeEnum() {
			CodedValue cv = (CodedValue)getValue("Mode");
			VocabularyEntry entry = VocabularyFactory.getVocab("LanguageAbilityMode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode
		 * @param Mode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setMode(CodedValue Mode) {
			setValue("Mode", Mode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Mode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setMode(VocabularyEntry Mode) {
			Code c = new CodedValue(Mode);
			setValue("Mode", c);
			return this;
		}
		
		/**
		 * A code taken from the HL7 LanguageAbilityProficiency vocabulary or an alternative vocabulary to describe the patient's proficiency to communicate in the Mode stated, for the Language stated
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityProficiency" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency
		 * @return CodedValue object
		 */	
		public CodedValue getProficiencyLevel() {
			return (CodedValue)getValue("ProficiencyLevel");
		}
		
		
		
		/**
		 * A code taken from the HL7 LanguageAbilityProficiency vocabulary or an alternative vocabulary to describe the patient's proficiency to communicate in the Mode stated, for the Language stated
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityProficiency" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency
		 * @return LanguageAbilityProficiency enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency getProficiencyLevelEnum() {
			CodedValue cv = (CodedValue)getValue("ProficiencyLevel");
			VocabularyEntry entry = VocabularyFactory.getVocab("LanguageAbilityProficiency", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency)entry;
		}
		
		
		/**
		 * A code taken from the HL7 LanguageAbilityProficiency vocabulary or an alternative vocabulary to describe the patient's proficiency to communicate in the Mode stated, for the Language stated
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityProficiency" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency
		 * @param ProficiencyLevel value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setProficiencyLevel(CodedValue ProficiencyLevel) {
			setValue("ProficiencyLevel", ProficiencyLevel);
			return this;
		}
		
		
		/**
		 * A code taken from the HL7 LanguageAbilityProficiency vocabulary or an alternative vocabulary to describe the patient's proficiency to communicate in the Mode stated, for the Language stated
  		 * <br>NOTE: This field should be populated using the "LanguageAbilityProficiency" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ProficiencyLevel value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setProficiencyLevel(VocabularyEntry ProficiencyLevel) {
			Code c = new CodedValue(ProficiencyLevel);
			setValue("ProficiencyLevel", c);
			return this;
		}
		
		/**
		 * A flag used to indicate if the language stated in languageCode is preferred by the patient. Should be either 'true' or 'false'.
		 * @return String object
		 */	
		public String getPreferenceInd() {
			return (String)getValue("PreferenceInd");
		}
		
		
		
		
		/**
		 * A flag used to indicate if the language stated in languageCode is preferred by the patient. Should be either 'true' or 'false'.
		 * @param PreferenceInd value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setPreferenceInd(String PreferenceInd) {
			setValue("PreferenceInd", PreferenceInd);
			return this;
		}
		
		
		/**
		 * A 'null flavour' to indicate why a preference has not been recorded. The NullFlavour vocabulary can be used to populate this value. Should only be supplied if PreferenceInd is null.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getPreferenceIndNullFlavour() {
			return (String)getValue("PreferenceIndNullFlavour");
		}
		
		
		
		/**
		 * A 'null flavour' to indicate why a preference has not been recorded. The NullFlavour vocabulary can be used to populate this value. Should only be supplied if PreferenceInd is null.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getPreferenceIndNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("PreferenceIndNullFlavour"));
		}
		
		
		/**
		 * A 'null flavour' to indicate why a preference has not been recorded. The NullFlavour vocabulary can be used to populate this value. Should only be supplied if PreferenceInd is null.
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param PreferenceIndNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patientv2LanguageCommunication setPreferenceIndNullFlavour(String PreferenceIndNullFlavour) {
			setValue("PreferenceIndNullFlavour", PreferenceIndNullFlavour);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Language",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145201GB02#languageCommunication",
												"Language",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Language", new Field(
												"Language",
												"x:languageCode/@code",
												"This is a simple string taken from the HumanLanguage vocabulary",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Mode", new Field(
												"Mode",
												"x:modeCode",
												"",
												"false",
												"",
												"LanguageAbilityMode",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.400",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ProficiencyLevel", new Field(
												"ProficiencyLevel",
												"x:proficiencyLevelCode",
												"A code taken from the HL7 LanguageAbilityProficiency vocabulary or an alternative vocabulary to describe the patient's proficiency to communicate in the Mode stated, for the Language stated",
												"false",
												"",
												"LanguageAbilityProficiency",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.401",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreferenceInd", new Field(
												"PreferenceInd",
												"x:preferenceInd/@value",
												"A flag used to indicate if the language stated in languageCode is preferred by the patient. Should be either 'true' or 'false'.",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreferenceIndNullFlavour", new Field(
												"PreferenceIndNullFlavour",
												"x:preferenceInd/@nullFlavor",
												"A 'null flavour' to indicate why a preference has not been recorded. The NullFlavour vocabulary can be used to populate this value. Should only be supplied if PreferenceInd is null.",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Patientv2LanguageCommunication() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Patientv2LanguageCommunication(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}



	
	
	
	