/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PatientRelationshipParticipantRole object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PersonRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} AssociatedJobRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} PersonName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgDescription</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PatientRelationshipParticipantRole extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "PatientRelationshipParticipantRole";
		protected static final String shortName = "ParticipantRole";
		protected static final String rootNode = "";
		protected static final String version = "COCD_TP145226GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param PersonRole PersonRole
		 * @param AssociatedJobRole AssociatedJobRole
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param PersonName PersonName
		 * @param OrgId OrgId
		 * @param OrgDescription OrgDescription
		 */
	    public PatientRelationshipParticipantRole(
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> ID, CodedValue PersonRole, CodedValue AssociatedJobRole, uk.nhs.interoperability.payloads.commontypes.Address Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName PersonName, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgDescription) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setPersonRole(PersonRole);
			setAssociatedJobRole(AssociatedJobRole);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setPersonName(PersonName);
			setOrgId(OrgId);
			setOrgDescription(OrgDescription);
		}
	
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getID() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("ID");
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setID(List ID) {
			setValue("ID", ID);
			return this;
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param ID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole addID(uk.nhs.interoperability.payloads.commontypes.PersonID ID) {
			addMultivalue("ID", ID);
			return this;
		}
		
		
		/**
		 * A code to describe the Role of this related person (e.g. Key Worker, Formal Carer, etc)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getPersonRole() {
			return (CodedValue)getValue("PersonRole");
		}
		
		
		
		/**
		 * A code to describe the Role of this related person (e.g. Key Worker, Formal Carer, etc)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getPersonRoleEnum() {
			CodedValue cv = (CodedValue)getValue("PersonRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * A code to describe the Role of this related person (e.g. Key Worker, Formal Carer, etc)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @param PersonRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setPersonRole(CodedValue PersonRole) {
			setValue("PersonRole", PersonRole);
			return this;
		}
		
		
		/**
		 * A code to describe the Role of this related person (e.g. Key Worker, Formal Carer, etc)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PersonRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setPersonRole(VocabularyEntry PersonRole) {
			Code c = new CodedValue(PersonRole);
			setValue("PersonRole", c);
			return this;
		}
		
		/**
		 * A code to describe the job role of the related person - this must come from the same vocabulary as the PersonRole
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getAssociatedJobRole() {
			return (CodedValue)getValue("AssociatedJobRole");
		}
		
		
		
		/**
		 * A code to describe the job role of the related person - this must come from the same vocabulary as the PersonRole
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getAssociatedJobRoleEnum() {
			CodedValue cv = (CodedValue)getValue("AssociatedJobRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * A code to describe the job role of the related person - this must come from the same vocabulary as the PersonRole
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @param AssociatedJobRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setAssociatedJobRole(CodedValue AssociatedJobRole) {
			setValue("AssociatedJobRole", AssociatedJobRole);
			return this;
		}
		
		
		/**
		 * A code to describe the job role of the related person - this must come from the same vocabulary as the PersonRole
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param AssociatedJobRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setAssociatedJobRole(VocabularyEntry AssociatedJobRole) {
			Code c = new CodedValue(AssociatedJobRole);
			setValue("AssociatedJobRole", c);
			return this;
		}
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("Address");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			setValue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getPersonName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("PersonName");
		}
		
		
		
		
		/**
		 * Person name
		 * <br><br>This field is MANDATORY
		 * @param PersonName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setPersonName(uk.nhs.interoperability.payloads.commontypes.PersonName PersonName) {
			setValue("PersonName", PersonName);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * 
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * @return String object
		 */	
		public String getOrgDescription() {
			return (String)getValue("OrgDescription");
		}
		
		
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * @param OrgDescription value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PatientRelationshipParticipantRole setOrgDescription(String OrgDescription) {
			setValue("OrgDescription", OrgDescription);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ROL",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145226GB01#ParticipantRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdRoot", new Field(
												"ContentIdRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdExtension", new Field(
												"ContentIdExtension",
												"npfitlc:contentId/@extension",
												"COCD_TP145226GB01#ParticipantRole",
												"RelatedPerson",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id",
												"One or more identifier(s) including local identifiers to identify the person",
												"false",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("IDNullFlavour", new Field(
												"IDNullFlavour",
												"x:id/@nullFlavor",
												"NI",
												"",
												"ID",
												"",
												"",
												"",
												""
												));
	
		put("PersonRole", new Field(
												"PersonRole",
												"x:code",
												"A code to describe the Role of this related person (e.g. Key Worker, Formal Carer, etc)",
												"true",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssociatedJobRoleCode", new Field(
												"AssociatedJobRoleCode",
												"x:code/x:qualifier/x:name/@code",
												"AR",
												"AssociatedJobRole",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssociatedJobRoleDisplayName", new Field(
												"AssociatedJobRoleDisplayName",
												"x:code/x:qualifier/x:name/@displayName",
												"Associated Role",
												"AssociatedJobRole",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssociatedJobRole", new Field(
												"AssociatedJobRole",
												"x:code/x:qualifier/x:value",
												"A code to describe the job role of the related person - this must come from the same vocabulary as the PersonRole",
												"true",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"",
												"PersonRole",
												"",
												"",
												"true",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"",
												"true",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressNullFlavour", new Field(
												"AddressNullFlavour",
												"x:addr/@nullFlavor",
												"NI",
												"",
												"Address",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:playingEntity/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:playingEntity/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateIdRoot", new Field(
												"PersonTemplateIdRoot",
												"x:playingEntity/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateId", new Field(
												"PersonTemplateId",
												"x:playingEntity/x:templateId/@extension",
												"COCD_TP145226GB01#playingPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonName", new Field(
												"PersonName",
												"x:playingEntity/x:name",
												"Person name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:scopingEntity/@classCode",
												"ORG",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:scopingEntity/@determinerCode",
												"INSTANCE",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:scopingEntity/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:scopingEntity/x:templateId/@extension",
												"COCD_TP145226GB01#scopingOrganization",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:scopingEntity/x:id",
												"",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDescription", new Field(
												"OrgDescription",
												"x:scopingEntity/x:desc",
												"The description of the organisation associated with the ODS code",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PatientRelationshipParticipantRole() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PatientRelationshipParticipantRole(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	