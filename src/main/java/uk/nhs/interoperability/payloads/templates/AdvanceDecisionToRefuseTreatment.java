/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the AdvanceDecisionToRefuseTreatment object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ADRTPreference</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeRecorded</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} Author</li>
 * <li>String ADRTDocumentLocation</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ADRTDiscussed</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class AdvanceDecisionToRefuseTreatment extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "AdvanceDecisionToRefuseTreatment";
		protected static final String shortName = "PatientADRT";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP146420GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param EffectiveTime EffectiveTime
		 * @param ADRTPreference ADRTPreference
		 * @param TimeRecorded TimeRecorded
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param ADRTDocumentLocation ADRTDocumentLocation
		 * @param ADRTDiscussed ADRTDiscussed
		 */
	    public AdvanceDecisionToRefuseTreatment(String ID, HL7Date EffectiveTime, CodedValue ADRTPreference, HL7Date TimeRecorded, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.Author Author, String ADRTDocumentLocation, CodedValue ADRTDiscussed) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setEffectiveTime(EffectiveTime);
			setADRTPreference(ADRTPreference);
			setTimeRecorded(TimeRecorded);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setADRTDocumentLocation(ADRTDocumentLocation);
			setADRTDiscussed(ADRTDiscussed);
		}
	
		/**
		 * A DCE UUID to identify each instance of the patient ADRT preference
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of the patient ADRT preference
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLADRTprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getADRTPreference() {
			return (CodedValue)getValue("ADRTPreference");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLADRTprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT
		 * <br><br>This field is MANDATORY
		 * @return EoLADRTprefSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT getADRTPreferenceEnum() {
			CodedValue cv = (CodedValue)getValue("ADRTPreference");
			VocabularyEntry entry = VocabularyFactory.getVocab("EoLADRTprefSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLADRTprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT
		 * <br><br>This field is MANDATORY
		 * @param ADRTPreference value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setADRTPreference(CodedValue ADRTPreference) {
			setValue("ADRTPreference", ADRTPreference);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "EoLADRTprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ADRTPreference value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setADRTPreference(VocabularyEntry ADRTPreference) {
			Code c = new CodedValue(ADRTPreference);
			setValue("ADRTPreference", c);
			return this;
		}
		
		/**
		 * The time the author originally recorded the ADRT preferences
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTimeRecorded() {
			return (HL7Date)getValue("TimeRecorded");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the ADRT preferences
		 * <br><br>This field is MANDATORY
		 * @param TimeRecorded value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setTimeRecorded(HL7Date TimeRecorded) {
			setValue("TimeRecorded", TimeRecorded);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setAuthor(uk.nhs.interoperability.payloads.templates.Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The location of ADRT documentation
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getADRTDocumentLocation() {
			return (String)getValue("ADRTDocumentLocation");
		}
		
		
		
		
		/**
		 * The location of ADRT documentation
		 * <br><br>This field is MANDATORY
		 * @param ADRTDocumentLocation value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setADRTDocumentLocation(String ADRTDocumentLocation) {
			setValue("ADRTDocumentLocation", ADRTDocumentLocation);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ADRTDiscussedSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT
		 * @return CodedValue object
		 */	
		public CodedValue getADRTDiscussed() {
			return (CodedValue)getValue("ADRTDiscussed");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ADRTDiscussedSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT
		 * @return ADRTDiscussedSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT getADRTDiscussedEnum() {
			CodedValue cv = (CodedValue)getValue("ADRTDiscussed");
			VocabularyEntry entry = VocabularyFactory.getVocab("ADRTDiscussedSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ADRTDiscussedSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT
		 * @param ADRTDiscussed value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setADRTDiscussed(CodedValue ADRTDiscussed) {
			setValue("ADRTDiscussed", ADRTDiscussed);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ADRTDiscussedSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ADRTDiscussed value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AdvanceDecisionToRefuseTreatment setADRTDiscussed(VocabularyEntry ADRTDiscussed) {
			Code c = new CodedValue(ADRTDiscussed);
			setValue("ADRTDiscussed", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146420GB01#PatientADRT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of the patient ADRT preference",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeValue", new Field(
												"CodeValue",
												"x:code/@code",
												"EOLADP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeSystem", new Field(
												"CodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeDisplayName", new Field(
												"CodeDisplayName",
												"x:code/@displayName",
												"EOL Patient ADRT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTPreference", new Field(
												"ADRTPreference",
												"x:value",
												"",
												"true",
												"",
												"EoLADRTprefSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTPreferenceType", new Field(
												"ADRTPreferenceType",
												"x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"ADRTPreference",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeRecorded", new Field(
												"TimeRecorded",
												"x:time",
												"The time the author originally recorded the ADRT preferences",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146419GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateId", new Field(
												"ER1TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:templateId/@extension",
												"COCD_TP146420GB01#entryRelationship1",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TypeCode", new Field(
												"ER1TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1ContextConductionInd", new Field(
												"ER1ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateIdRoot", new Field(
												"ER1TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1SeperatableInd", new Field(
												"ER1SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:seperatableInd/@value",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocClassCode", new Field(
												"ADRTDocLocClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocMoodCode", new Field(
												"ADRTDocLocMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocTemplateIdRoot", new Field(
												"ADRTDocLocTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocTemplateId", new Field(
												"ADRTDocLocTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:templateId/@extension",
												"COCD_TP146420GB01#aDRTDocsLocation",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocValue", new Field(
												"ADRTDocLocValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:code/@code",
												"EOLADL",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocDisplayName", new Field(
												"ADRTDocLocDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:code/@displayName",
												"EOL Patient ADRT Docs Location",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocLocCodeSystem", new Field(
												"ADRTDocLocCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocumentLocation", new Field(
												"ADRTDocumentLocation",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:value",
												"The location of ADRT documentation",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDocumentLocationDataType", new Field(
												"ADRTDocumentLocationDataType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship1']/x:observation/x:value/@xsi:type",
												"ST",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TemplateId", new Field(
												"ER4TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:templateId/@extension",
												"COCD_TP146420GB01#entryRelationship4",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TemplateIdRoot", new Field(
												"ER4TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TypeCode", new Field(
												"ER4TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/@typeCode",
												"COMP",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4ContextConductionInd", new Field(
												"ER4ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/@contextConductionInd",
												"true",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4SeperatableInd", new Field(
												"ER4SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:seperatableInd/@value",
												"false",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedClassCode", new Field(
												"ADRTDiscussedClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/@classCode",
												"OBS",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedMoodCode", new Field(
												"ADRTDiscussedMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/@moodCode",
												"EVN",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedTemplateIdRoot", new Field(
												"ADRTDiscussedTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedTemplateId", new Field(
												"ADRTDiscussedTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:templateId/@extension",
												"COCD_TP146420GB01#aDRTDiscussed",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedCodeValue", new Field(
												"ADRTDiscussedCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:code/@code",
												"EOLADS",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedCodeSystem", new Field(
												"ADRTDiscussedCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedDisplayName", new Field(
												"ADRTDiscussedDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:code/@displayName",
												"EOL Patient ADRT Discussed",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussed", new Field(
												"ADRTDiscussed",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:value",
												"",
												"false",
												"",
												"ADRTDiscussedSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ADRTDiscussedType", new Field(
												"ADRTDiscussedType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146420GB01#entryRelationship4']/x:observation/x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"ADRTDiscussed",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public AdvanceDecisionToRefuseTreatment() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public AdvanceDecisionToRefuseTreatment(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	