/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the AuthoritytoLastingPowerofAttorney object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} AuthoritytoLPA</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} RecordedTime</li>
 * <li>{@link Author Author} Author</li>
 * <li>{@link PatientRelationshipParticipantRole PatientRelationshipParticipantRole} LPADetails</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class AuthoritytoLastingPowerofAttorney extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "AuthoritytoLastingPowerofAttorney";
		protected static final String shortName = "AuthorityToLPA";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP146417GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param EffectiveTime EffectiveTime
		 * @param AuthoritytoLPA AuthoritytoLPA
		 * @param RecordedTime RecordedTime
		 * @param Author Author
		 * @param LPADetails LPADetails
		 */
	    public AuthoritytoLastingPowerofAttorney(String ID, HL7Date EffectiveTime, CodedValue AuthoritytoLPA, HL7Date RecordedTime, Author Author, PatientRelationshipParticipantRole LPADetails) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setEffectiveTime(EffectiveTime);
			setAuthoritytoLPA(AuthoritytoLPA);
			setRecordedTime(RecordedTime);
			setAuthor(Author);
			setLPADetails(LPADetails);
		}
	
		/**
		 * A DCE UUID to identify each instance of authority to lasting power of attorney
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of authority to lasting power of attorney
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * A code that describes Authority of Lasting Power of Attorney
  		 * <br>NOTE: This field should be populated using the "AuthoritytoLPASnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getAuthoritytoLPA() {
			return (CodedValue)getValue("AuthoritytoLPA");
		}
		
		
		
		/**
		 * A code that describes Authority of Lasting Power of Attorney
  		 * <br>NOTE: This field should be populated using the "AuthoritytoLPASnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT
		 * <br><br>This field is MANDATORY
		 * @return AuthoritytoLPASnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT getAuthoritytoLPAEnum() {
			CodedValue cv = (CodedValue)getValue("AuthoritytoLPA");
			VocabularyEntry entry = VocabularyFactory.getVocab("AuthoritytoLPASnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT)entry;
		}
		
		
		/**
		 * A code that describes Authority of Lasting Power of Attorney
  		 * <br>NOTE: This field should be populated using the "AuthoritytoLPASnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT
		 * <br><br>This field is MANDATORY
		 * @param AuthoritytoLPA value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setAuthoritytoLPA(CodedValue AuthoritytoLPA) {
			setValue("AuthoritytoLPA", AuthoritytoLPA);
			return this;
		}
		
		
		/**
		 * A code that describes Authority of Lasting Power of Attorney
  		 * <br>NOTE: This field should be populated using the "AuthoritytoLPASnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param AuthoritytoLPA value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setAuthoritytoLPA(VocabularyEntry AuthoritytoLPA) {
			Code c = new CodedValue(AuthoritytoLPA);
			setValue("AuthoritytoLPA", c);
			return this;
		}
		
		/**
		 * The time the author originally recorded the authority to lasting power of attorney
		 * @return HL7Date object
		 */	
		public HL7Date getRecordedTime() {
			return (HL7Date)getValue("RecordedTime");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the authority to lasting power of attorney
		 * @param RecordedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setRecordedTime(HL7Date RecordedTime) {
			setValue("RecordedTime", RecordedTime);
			return this;
		}
		
		
		/**
		 * Template holding the actual author details
		 * @return Author object
		 */	
		public Author getAuthor() {
			return (Author)getValue("Author");
		}
		
		
		
		
		/**
		 * Template holding the actual author details
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setAuthor(Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * Details of the individual that holds the lasting power of attorney
		 * <br><br>This field is MANDATORY
		 * @return PatientRelationshipParticipantRole object
		 */	
		public PatientRelationshipParticipantRole getLPADetails() {
			return (PatientRelationshipParticipantRole)getValue("LPADetails");
		}
		
		
		
		
		/**
		 * Details of the individual that holds the lasting power of attorney
		 * <br><br>This field is MANDATORY
		 * @param LPADetails value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthoritytoLastingPowerofAttorney setLPADetails(PatientRelationshipParticipantRole LPADetails) {
			setValue("LPADetails", LPADetails);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146417GB01#AuthorityToLPA",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of authority to lasting power of attorney",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeValue", new Field(
												"CodeValue",
												"x:code/@code",
												"EOLLPA",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeSystem", new Field(
												"CodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeDisplayName", new Field(
												"CodeDisplayName",
												"x:code/@displayName",
												"EOL Authority to LPA",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthoritytoLPA", new Field(
												"AuthoritytoLPA",
												"x:value",
												"A code that describes Authority of Lasting Power of Attorney",
												"true",
												"",
												"AuthoritytoLPASnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthoritytoLPAType", new Field(
												"AuthoritytoLPAType",
												"x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146417GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/x:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/x:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												""
												));
	
		put("AuthorFunctionDisplayValue", new Field(
												"AuthorFunctionDisplayValue",
												"x:author/x:functionCode/@displayValue",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecordedTime", new Field(
												"RecordedTime",
												"x:author/x:time",
												"The time the author originally recorded the authority to lasting power of attorney",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"Template holding the actual author details",
												"false",
												"",
												"",
												"Author",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTypeCode", new Field(
												"SubjectTypeCode",
												"x:participant/@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectContextControlCode", new Field(
												"SubjectContextControlCode",
												"x:participant/@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateIdRoot", new Field(
												"SubjectTemplateIdRoot",
												"x:participant/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateId", new Field(
												"SubjectTemplateId",
												"x:participant/x:templateId/@extension",
												"COCD_TP146417GB01#participant",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectContentIdRoot", new Field(
												"SubjectContentIdRoot",
												"x:participant/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectContentId", new Field(
												"SubjectContentId",
												"x:participant/npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"LPADetails",
												""
												));
	
		put("LPADetails", new Field(
												"LPADetails",
												"x:participant/x:participantRole",
												"Details of the individual that holds the lasting power of attorney",
												"true",
												"",
												"",
												"PatientRelationshipParticipantRole",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public AuthoritytoLastingPowerofAttorney() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public AuthoritytoLastingPowerofAttorney(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	