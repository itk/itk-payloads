/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the AnticipatoryMedicineBoxIssueProcedure object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} AnticipatoryMedicineBoxIssueCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeIssued</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} RecordedTime</li>
 * <li>{@link Author Author} Author</li>
 * <li>{@link Performer Performer} IssuedBy</li>
 * <li>String LocationOfBox</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class AnticipatoryMedicineBoxIssueProcedure extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "AnticipatoryMedicineBoxIssueProcedure";
		protected static final String shortName = "AMBoxIssueProcedure";
		protected static final String rootNode = "x:procedure";
		protected static final String version = "COCD_TP146418GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param AnticipatoryMedicineBoxIssueCode AnticipatoryMedicineBoxIssueCode
		 * @param TimeIssued TimeIssued
		 * @param RecordedTime RecordedTime
		 * @param Author Author
		 * @param IssuedBy IssuedBy
		 * @param LocationOfBox LocationOfBox
		 */
	    public AnticipatoryMedicineBoxIssueProcedure(String ID, CodedValue AnticipatoryMedicineBoxIssueCode, HL7Date TimeIssued, HL7Date RecordedTime, Author Author, Performer IssuedBy, String LocationOfBox) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setAnticipatoryMedicineBoxIssueCode(AnticipatoryMedicineBoxIssueCode);
			setTimeIssued(TimeIssued);
			setRecordedTime(RecordedTime);
			setAuthor(Author);
			setIssuedBy(IssuedBy);
			setLocationOfBox(LocationOfBox);
		}
	
		/**
		 * A DCE UUID to identify each instance of issue of the anticipatory medicine box
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of issue of the anticipatory medicine box
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * This is a code to indicate that an anticipatory medicine box has been issued. This is optional - if not populated then the standard code (376201000000102) will be automatically used. Populating it here might be useful if you also want it to include a cross-reference to a text section
  		 * <br>NOTE: This field should be populated using the "EoLAnticipatoryMedicineBoxIssueSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT
		 * @return CodedValue object
		 */	
		public CodedValue getAnticipatoryMedicineBoxIssueCode() {
			return (CodedValue)getValue("AnticipatoryMedicineBoxIssueCode");
		}
		
		
		
		/**
		 * This is a code to indicate that an anticipatory medicine box has been issued. This is optional - if not populated then the standard code (376201000000102) will be automatically used. Populating it here might be useful if you also want it to include a cross-reference to a text section
  		 * <br>NOTE: This field should be populated using the "EoLAnticipatoryMedicineBoxIssueSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT
		 * @return EoLAnticipatoryMedicineBoxIssueSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT getAnticipatoryMedicineBoxIssueCodeEnum() {
			CodedValue cv = (CodedValue)getValue("AnticipatoryMedicineBoxIssueCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("EoLAnticipatoryMedicineBoxIssueSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT)entry;
		}
		
		
		/**
		 * This is a code to indicate that an anticipatory medicine box has been issued. This is optional - if not populated then the standard code (376201000000102) will be automatically used. Populating it here might be useful if you also want it to include a cross-reference to a text section
  		 * <br>NOTE: This field should be populated using the "EoLAnticipatoryMedicineBoxIssueSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT
		 * @param AnticipatoryMedicineBoxIssueCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setAnticipatoryMedicineBoxIssueCode(CodedValue AnticipatoryMedicineBoxIssueCode) {
			setValue("AnticipatoryMedicineBoxIssueCode", AnticipatoryMedicineBoxIssueCode);
			return this;
		}
		
		
		/**
		 * This is a code to indicate that an anticipatory medicine box has been issued. This is optional - if not populated then the standard code (376201000000102) will be automatically used. Populating it here might be useful if you also want it to include a cross-reference to a text section
  		 * <br>NOTE: This field should be populated using the "EoLAnticipatoryMedicineBoxIssueSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param AnticipatoryMedicineBoxIssueCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setAnticipatoryMedicineBoxIssueCode(VocabularyEntry AnticipatoryMedicineBoxIssueCode) {
			Code c = new CodedValue(AnticipatoryMedicineBoxIssueCode);
			setValue("AnticipatoryMedicineBoxIssueCode", c);
			return this;
		}
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTimeIssued() {
			return (HL7Date)getValue("TimeIssued");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param TimeIssued value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setTimeIssued(HL7Date TimeIssued) {
			setValue("TimeIssued", TimeIssued);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the patient relationship
		 * @return HL7Date object
		 */	
		public HL7Date getRecordedTime() {
			return (HL7Date)getValue("RecordedTime");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the patient relationship
		 * @param RecordedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setRecordedTime(HL7Date RecordedTime) {
			setValue("RecordedTime", RecordedTime);
			return this;
		}
		
		
		/**
		 * Template holding the actual author details
		 * @return Author object
		 */	
		public Author getAuthor() {
			return (Author)getValue("Author");
		}
		
		
		
		
		/**
		 * Template holding the actual author details
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setAuthor(Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The person who performs issue of anticipatory medicine box to patient
		 * @return Performer object
		 */	
		public Performer getIssuedBy() {
			return (Performer)getValue("IssuedBy");
		}
		
		
		
		
		/**
		 * The person who performs issue of anticipatory medicine box to patient
		 * @param IssuedBy value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setIssuedBy(Performer IssuedBy) {
			setValue("IssuedBy", IssuedBy);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getLocationOfBox() {
			return (String)getValue("LocationOfBox");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param LocationOfBox value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AnticipatoryMedicineBoxIssueProcedure setLocationOfBox(String LocationOfBox) {
			setValue("LocationOfBox", LocationOfBox);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"PROC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146418GB01#AMBoxIssueProcedure",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of issue of the anticipatory medicine box",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AnticipatoryMedicineBoxIssueCode", new Field(
												"AnticipatoryMedicineBoxIssueCode",
												"x:code",
												"This is a code to indicate that an anticipatory medicine box has been issued. This is optional - if not populated then the standard code (376201000000102) will be automatically used. Populating it here might be useful if you also want it to include a cross-reference to a text section",
												"false",
												"",
												"EoLAnticipatoryMedicineBoxIssueSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EoLAnticipatoryMedicineBoxIssueSnCT", new Field(
												"EoLAnticipatoryMedicineBoxIssueSnCT",
												"x:code/@code",
												"376201000000102",
												"",
												"AnticipatoryMedicineBoxIssueCode",
												"",
												"",
												"",
												""
												));
	
		put("EoLAnticipatoryMedicineBoxIssueSnCTCodeSystem", new Field(
												"EoLAnticipatoryMedicineBoxIssueSnCTCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"",
												"AnticipatoryMedicineBoxIssueCode",
												"",
												"",
												"",
												""
												));
	
		put("EoLAnticipatoryMedicineBoxIssueSnCTDisplayName", new Field(
												"EoLAnticipatoryMedicineBoxIssueSnCTDisplayName",
												"x:code/@displayName",
												"Issue of palliative care anticipatory medication box (procedure)",
												"",
												"AnticipatoryMedicineBoxIssueCode",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeIssued", new Field(
												"TimeIssued",
												"x:effectiveTime/@value",
												"",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146418GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecordedTime", new Field(
												"RecordedTime",
												"x:author/x:time",
												"The time the author originally recorded the patient relationship",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"Template holding the actual author details",
												"false",
												"",
												"",
												"Author",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/x:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/x:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionDisplayValue", new Field(
												"AuthorFunctionDisplayValue",
												"x:author/x:functionCode/@displayValue",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTypeCode", new Field(
												"PerformerTypeCode",
												"x:performer/@typeCode",
												"PRF",
												"IssuedBy",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTemplateIdRoot", new Field(
												"PerformerTemplateIdRoot",
												"x:performer/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"IssuedBy",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTemplateId", new Field(
												"PerformerTemplateId",
												"x:performer/x:templateId/@extension",
												"COCD_TP146418GB01#performer",
												"IssuedBy",
												"",
												"",
												"",
												"",
												""
												));
	
		put("IssuedBy", new Field(
												"IssuedBy",
												"x:performer/x:participant",
												"The person who performs issue of anticipatory medicine box to patient",
												"false",
												"",
												"",
												"Performer",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerContentId", new Field(
												"PerformerContentId",
												"x:performer/x:contentId",
												"...",
												"IssuedBy",
												"",
												"",
												"",
												"IssuedBy",
												""
												));
	
		put("PerformerContentIdRoot", new Field(
												"PerformerContentIdRoot",
												"x:performer/x:contentId",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"IssuedBy",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TypeCode", new Field(
												"ER1TypeCode",
												"x:entryRelationship/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1ContextConductionInd", new Field(
												"ER1ContextConductionInd",
												"x:entryRelationship/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateIdRoot", new Field(
												"ER1TemplateIdRoot",
												"x:entryRelationship/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateId", new Field(
												"ER1TemplateId",
												"x:entryRelationship/x:templateId/@extension",
												"COCD_TP146418GB01#entryRelationship1",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1SeperatableInd", new Field(
												"ER1SeperatableInd",
												"x:entryRelationship/x:seperatableInd/@value",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxClassCode", new Field(
												"AMBoxClassCode",
												"x:entryRelationship/x:observation/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxMoodCode", new Field(
												"AMBoxMoodCode",
												"x:entryRelationship/x:observation/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxTemplateIdRoot", new Field(
												"AMBoxTemplateIdRoot",
												"x:entryRelationship/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxTemplateId", new Field(
												"AMBoxTemplateId",
												"x:entryRelationship/x:observation/x:templateId/@extension",
												"COCD_TP146418GB01#aMBoxLocation",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxCodeValue", new Field(
												"AMBoxCodeValue",
												"x:entryRelationship/x:observation/x:code/@code",
												"EOLBLC",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxCodeSystem", new Field(
												"AMBoxCodeSystem",
												"x:entryRelationship/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AMBoxDisplayName", new Field(
												"AMBoxDisplayName",
												"x:entryRelationship/x:observation/x:code/@displayName",
												"EOL Anticipatory Medicine Box Location",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LocationOfBox", new Field(
												"LocationOfBox",
												"x:entryRelationship/x:observation/x:value",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LocationOfBoxDataType", new Field(
												"LocationOfBoxDataType",
												"x:entryRelationship/x:observation/x:value/@xsi:type",
												"ST",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public AnticipatoryMedicineBoxIssueProcedure() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public AnticipatoryMedicineBoxIssueProcedure(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	