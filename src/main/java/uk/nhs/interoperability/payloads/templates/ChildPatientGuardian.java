/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ChildPatientGuardian object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses PatientIDWithTraceStatuses}&gt; Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Role</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link ChildPatientGuardianChoice ChildPatientGuardianChoice} GuardianDetails</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ChildPatientGuardian extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ChildPatientGuardian";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param Role Role
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param GuardianDetails GuardianDetails
		 */
	    public ChildPatientGuardian(
		List<uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses> Id, CodedValue Role, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, ChildPatientGuardianChoice GuardianDetails) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setRole(Role);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setGuardianDetails(GuardianDetails);
		}
	
		/**
		 * Identification of the guardian using the NHS number, or a local identifier if the NHS number is not available
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses> getId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses>)getValue("Id");
		}
		
		/**
		 * Identification of the guardian using the NHS number, or a local identifier if the NHS number is not available
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian setId(List Id) {
			setValue("Id", Id);
			return this;
		}
		
		/**
		 * Identification of the guardian using the NHS number, or a local identifier if the NHS number is not available
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses objects.
		 * @param Id value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian addId(uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses Id) {
			addMultivalue("Id", Id);
			return this;
		}
		
		
		/**
		 * A code from the GuardianRoleType vocabulary to describe the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "GuardianRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType
		 * @return CodedValue object
		 */	
		public CodedValue getRole() {
			return (CodedValue)getValue("Role");
		}
		
		
		
		/**
		 * A code from the GuardianRoleType vocabulary to describe the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "GuardianRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType
		 * @return GuardianRoleType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType getRoleEnum() {
			CodedValue cv = (CodedValue)getValue("Role");
			VocabularyEntry entry = VocabularyFactory.getVocab("GuardianRoleType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType)entry;
		}
		
		
		/**
		 * A code from the GuardianRoleType vocabulary to describe the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "GuardianRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType
		 * @param Role value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian setRole(CodedValue Role) {
			setValue("Role", Role);
			return this;
		}
		
		
		/**
		 * A code from the GuardianRoleType vocabulary to describe the relationship between the patient and the named person
  		 * <br>NOTE: This field should be populated using the "GuardianRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Role value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian setRole(VocabularyEntry Role) {
			Code c = new CodedValue(Role);
			setValue("Role", c);
			return this;
		}
		
		/**
		 * A set of addresses for the patient's guardian (this is limited to a maximum of 4 types to reduce display issues in rendering CDA documents)
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * A set of addresses for the patient's guardian (this is limited to a maximum of 4 types to reduce display issues in rendering CDA documents)
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * A set of addresses for the patient's guardian (this is limited to a maximum of 4 types to reduce display issues in rendering CDA documents)
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * Telephone Number
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * The person or organisation who/which is or acts as the patient's guardian.
		 * <br><br>This field is MANDATORY
		 * @return ChildPatientGuardianChoice object
		 */	
		public ChildPatientGuardianChoice getGuardianDetails() {
			return (ChildPatientGuardianChoice)getValue("GuardianDetails");
		}
		
		
		
		
		/**
		 * The person or organisation who/which is or acts as the patient's guardian.
		 * <br><br>This field is MANDATORY
		 * @param GuardianDetails value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardian setGuardianDetails(ChildPatientGuardianChoice GuardianDetails) {
			setValue("GuardianDetails", GuardianDetails);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"GUARD",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145230GB02#guardian",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id",
												"Identification of the guardian using the NHS number, or a local identifier if the NHS number is not available",
												"false",
												"",
												"",
												"PatientIDWithTraceStatuses",
												"uk.nhs.interoperability.payloads.commontypes.",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Role", new Field(
												"Role",
												"x:code",
												"A code from the GuardianRoleType vocabulary to describe the relationship between the patient and the named person",
												"false",
												"",
												"GuardianRoleType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"A set of addresses for the patient's guardian (this is limited to a maximum of 4 types to reduce display issues in rendering CDA documents)",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianDetails", new Field(
												"GuardianDetails",
												"{childRootNodeName}",
												"The person or organisation who/which is or acts as the patient's guardian.",
												"true",
												"",
												"",
												"ChildPatientGuardianChoice",
												"",
												"",
												"",
												"",
												"(x:guardianPerson/x:templateId/@extension|x:guardianOrganization/x:templateId/@extension)[1]",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ChildPatientGuardian() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ChildPatientGuardian(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	