/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the AttachmentSubject object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} AwarenessCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SubjectRelationshipType</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; SubjectAddress</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; SubjectTelephoneNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; SubjectName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SubjectSex</li>
 * <li>String SubjectSexNullFlavour</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} SubjectBirthTime</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class AttachmentSubject extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "AttachmentSubject";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param AwarenessCode AwarenessCode
		 * @param SubjectRelationshipType SubjectRelationshipType
		 * @param SubjectAddress SubjectAddress
		 * @param SubjectTelephoneNumber SubjectTelephoneNumber
		 * @param SubjectName SubjectName
		 * @param SubjectSex SubjectSex
		 * @param SubjectSexNullFlavour SubjectSexNullFlavour
		 * @param SubjectBirthTime SubjectBirthTime
		 */
	    public AttachmentSubject(CodedValue AwarenessCode, CodedValue SubjectRelationshipType, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> SubjectAddress, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> SubjectTelephoneNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> SubjectName, CodedValue SubjectSex, String SubjectSexNullFlavour, HL7Date SubjectBirthTime) {
			fields = new LinkedHashMap<String, Object>();
			
			setAwarenessCode(AwarenessCode);
			setSubjectRelationshipType(SubjectRelationshipType);
			setSubjectAddress(SubjectAddress);
			setSubjectTelephoneNumber(SubjectTelephoneNumber);
			setSubjectName(SubjectName);
			setSubjectSex(SubjectSex);
			setSubjectSexNullFlavour(SubjectSexNullFlavour);
			setSubjectBirthTime(SubjectBirthTime);
		}
	
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "TargetAwareness" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness
		 * @return CodedValue object
		 */	
		public CodedValue getAwarenessCode() {
			return (CodedValue)getValue("AwarenessCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "TargetAwareness" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness
		 * @return TargetAwareness enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness getAwarenessCodeEnum() {
			CodedValue cv = (CodedValue)getValue("AwarenessCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("TargetAwareness", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "TargetAwareness" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness
		 * @param AwarenessCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setAwarenessCode(CodedValue AwarenessCode) {
			setValue("AwarenessCode", AwarenessCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "TargetAwareness" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param AwarenessCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setAwarenessCode(VocabularyEntry AwarenessCode) {
			Code c = new CodedValue(AwarenessCode);
			setValue("AwarenessCode", c);
			return this;
		}
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * @return CodedValue object
		 */	
		public CodedValue getSubjectRelationshipType() {
			return (CodedValue)getValue("SubjectRelationshipType");
		}
		
		
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * @return PersonalRelationshipRoleType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType getSubjectRelationshipTypeEnum() {
			CodedValue cv = (CodedValue)getValue("SubjectRelationshipType");
			VocabularyEntry entry = VocabularyFactory.getVocab("PersonalRelationshipRoleType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType)entry;
		}
		
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * @param SubjectRelationshipType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectRelationshipType(CodedValue SubjectRelationshipType) {
			setValue("SubjectRelationshipType", SubjectRelationshipType);
			return this;
		}
		
		
		/**
		 * A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person
  		 * <br>NOTE: This field should be populated using the "PersonalRelationshipRoleType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SubjectRelationshipType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectRelationshipType(VocabularyEntry SubjectRelationshipType) {
			Code c = new CodedValue(SubjectRelationshipType);
			setValue("SubjectRelationshipType", c);
			return this;
		}
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getSubjectAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("SubjectAddress");
		}
		
		/**
		 * 
		 * @param SubjectAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectAddress(List SubjectAddress) {
			setValue("SubjectAddress", SubjectAddress);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param SubjectAddress value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject addSubjectAddress(uk.nhs.interoperability.payloads.commontypes.Address SubjectAddress) {
			addMultivalue("SubjectAddress", SubjectAddress);
			return this;
		}
		
		
		/**
		 * Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getSubjectTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("SubjectTelephoneNumber");
		}
		
		/**
		 * Telephone Number
		 * @param SubjectTelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectTelephoneNumber(List SubjectTelephoneNumber) {
			setValue("SubjectTelephoneNumber", SubjectTelephoneNumber);
			return this;
		}
		
		/**
		 * Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param SubjectTelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject addSubjectTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom SubjectTelephoneNumber) {
			addMultivalue("SubjectTelephoneNumber", SubjectTelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getSubjectName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("SubjectName");
		}
		
		/**
		 * Person name
		 * @param SubjectName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectName(List SubjectName) {
			setValue("SubjectName", SubjectName);
			return this;
		}
		
		/**
		 * Person name
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param SubjectName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject addSubjectName(uk.nhs.interoperability.payloads.commontypes.PersonName SubjectName) {
			addMultivalue("SubjectName", SubjectName);
			return this;
		}
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return CodedValue object
		 */	
		public CodedValue getSubjectSex() {
			return (CodedValue)getValue("SubjectSex");
		}
		
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getSubjectSexEnum() {
			CodedValue cv = (CodedValue)getValue("SubjectSex");
			VocabularyEntry entry = VocabularyFactory.getVocab("Sex", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Sex)entry;
		}
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param SubjectSex value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectSex(CodedValue SubjectSex) {
			setValue("SubjectSex", SubjectSex);
			return this;
		}
		
		
		/**
		 * A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SubjectSex value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectSex(VocabularyEntry SubjectSex) {
			Code c = new CodedValue(SubjectSex);
			setValue("SubjectSex", c);
			return this;
		}
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getSubjectSexNullFlavour() {
			return (String)getValue("SubjectSexNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getSubjectSexNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("SubjectSexNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param SubjectSexNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectSexNullFlavour(String SubjectSexNullFlavour) {
			setValue("SubjectSexNullFlavour", SubjectSexNullFlavour);
			return this;
		}
		
		
		/**
		 * Date
		 * @return HL7Date object
		 */	
		public HL7Date getSubjectBirthTime() {
			return (HL7Date)getValue("SubjectBirthTime");
		}
		
		
		
		
		/**
		 * Date
		 * @param SubjectBirthTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AttachmentSubject setSubjectBirthTime(HL7Date SubjectBirthTime) {
			setValue("SubjectBirthTime", SubjectBirthTime);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TypeCode", new Field(
												"TypeCode",
												"@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContextControlCode", new Field(
												"ContextControlCode",
												"@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146224GB02#subject",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AwarenessCode", new Field(
												"AwarenessCode",
												"x:awarenessCode",
												"",
												"false",
												"",
												"TargetAwareness",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.411",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectClassCode", new Field(
												"SubjectClassCode",
												"x:participant/@classCode",
												"PRS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateIdRoot", new Field(
												"SubjectTemplateIdRoot",
												"x:participant/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateId", new Field(
												"SubjectTemplateId",
												"x:participant/x:templateId/@extension",
												"COCD_TP145213GB01#RelatedSubject",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectRelationshipType", new Field(
												"SubjectRelationshipType",
												"x:participant/x:code",
												"A code from the PersonalRelationshipRoleType or an alternative vocabulary to describe the relationship between the patient and the named subject person",
												"false",
												"",
												"PersonalRelationshipRoleType",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.407",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectAddress", new Field(
												"SubjectAddress",
												"x:participant/x:addr",
												"",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTelephoneNumber", new Field(
												"SubjectTelephoneNumber",
												"x:participant/x:telecom",
												"Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectSubjectClassCode", new Field(
												"SubjectSubjectClassCode",
												"x:participant/x:subject/@classCode",
												"PSN",
												"SubjectName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectSubjectDeterminerCode", new Field(
												"SubjectSubjectDeterminerCode",
												"x:participant/x:subject/@determinerCode",
												"INSTANCE",
												"SubjectName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectSubjectTemplateIdRoot", new Field(
												"SubjectSubjectTemplateIdRoot",
												"x:participant/x:subject/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SubjectName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectSubjectTemplateId", new Field(
												"SubjectSubjectTemplateId",
												"x:participant/x:subject/x:templateId/@extension",
												"COCD_TP145213GB01#subject",
												"SubjectName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectName", new Field(
												"SubjectName",
												"x:participant/x:subject/x:name",
												"Person name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectSex", new Field(
												"SubjectSex",
												"x:participant/x:subject/x:administrativeGenderCode",
												"A code from the Sex vocabulary or an alternative vocabulary to indicate the gender of the person",
												"false",
												"",
												"Sex",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.408",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectSexNullFlavour", new Field(
												"SubjectSexNullFlavour",
												"x:participant/x:subject/x:administrativeGenderCode/@nullFlavor",
												"A null flavour can be used when the sex cannot be provided. This must only be used when the sex field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectBirthTime", new Field(
												"SubjectBirthTime",
												"x:participant/x:subject/x:birthTime/@value",
												"Date",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdRoot", new Field(
												"ContentIdRoot",
												"x:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdExtension", new Field(
												"ContentIdExtension",
												"x:contentId/@extension",
												"COCD_TP145213GB01#RelatedSubject",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public AttachmentSubject() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public AttachmentSubject(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	