/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the DNACPRDecisionbySeniorResponsibleClinician object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} DNACPRPreference</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} SeniorResponsibleClinicianTimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} SeniorResponsibleClinicianAuthor</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DelegatedTimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} DelegatedAuthor</li>
 * <li>String DNACPRDocsLocation</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} ReviewDate</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ResuscitationStatus</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class DNACPRDecisionbySeniorResponsibleClinician extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "DNACPRDecisionbySeniorResponsibleClinician";
		protected static final String shortName = "DNACPRbySRClinician";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP146422GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param EffectiveTime EffectiveTime
		 * @param DNACPRPreference DNACPRPreference
		 * @param SeniorResponsibleClinicianTimeAuthored SeniorResponsibleClinicianTimeAuthored
		 * @param SeniorResponsibleClinicianAuthor SeniorResponsibleClinicianAuthor
		 * @param DelegatedTimeAuthored DelegatedTimeAuthored
		 * @param DelegatedAuthor DelegatedAuthor
		 * @param DNACPRDocsLocation DNACPRDocsLocation
		 * @param ReviewDate ReviewDate
		 * @param ResuscitationStatus ResuscitationStatus
		 */
	    public DNACPRDecisionbySeniorResponsibleClinician(String ID, HL7Date EffectiveTime, CodedValue DNACPRPreference, HL7Date SeniorResponsibleClinicianTimeAuthored, uk.nhs.interoperability.payloads.templates.Author SeniorResponsibleClinicianAuthor, HL7Date DelegatedTimeAuthored, uk.nhs.interoperability.payloads.templates.Author DelegatedAuthor, String DNACPRDocsLocation, HL7Date ReviewDate, CodedValue ResuscitationStatus) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setEffectiveTime(EffectiveTime);
			setDNACPRPreference(DNACPRPreference);
			setSeniorResponsibleClinicianTimeAuthored(SeniorResponsibleClinicianTimeAuthored);
			setSeniorResponsibleClinicianAuthor(SeniorResponsibleClinicianAuthor);
			setDelegatedTimeAuthored(DelegatedTimeAuthored);
			setDelegatedAuthor(DelegatedAuthor);
			setDNACPRDocsLocation(DNACPRDocsLocation);
			setReviewDate(ReviewDate);
			setResuscitationStatus(ResuscitationStatus);
		}
	
		/**
		 * A DCE UUID to identify each instance of the DNACPR decision by the senior responsible clinician.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of the DNACPR decision by the senior responsible clinician.
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * The time when DNACPR decision by the senior responsible clinician is recorded.
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * The time when DNACPR decision by the senior responsible clinician is recorded.
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DNACPRprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getDNACPRPreference() {
			return (CodedValue)getValue("DNACPRPreference");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DNACPRprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT
		 * <br><br>This field is MANDATORY
		 * @return DNACPRprefSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT getDNACPRPreferenceEnum() {
			CodedValue cv = (CodedValue)getValue("DNACPRPreference");
			VocabularyEntry entry = VocabularyFactory.getVocab("DNACPRprefSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DNACPRprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT
		 * <br><br>This field is MANDATORY
		 * @param DNACPRPreference value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setDNACPRPreference(CodedValue DNACPRPreference) {
			setValue("DNACPRPreference", DNACPRPreference);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DNACPRprefSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param DNACPRPreference value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setDNACPRPreference(VocabularyEntry DNACPRPreference) {
			Code c = new CodedValue(DNACPRPreference);
			setValue("DNACPRPreference", c);
			return this;
		}
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getSeniorResponsibleClinicianTimeAuthored() {
			return (HL7Date)getValue("SeniorResponsibleClinicianTimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param SeniorResponsibleClinicianTimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setSeniorResponsibleClinicianTimeAuthored(HL7Date SeniorResponsibleClinicianTimeAuthored) {
			setValue("SeniorResponsibleClinicianTimeAuthored", SeniorResponsibleClinicianTimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getSeniorResponsibleClinicianAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("SeniorResponsibleClinicianAuthor");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param SeniorResponsibleClinicianAuthor value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setSeniorResponsibleClinicianAuthor(uk.nhs.interoperability.payloads.templates.Author SeniorResponsibleClinicianAuthor) {
			setValue("SeniorResponsibleClinicianAuthor", SeniorResponsibleClinicianAuthor);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getDelegatedTimeAuthored() {
			return (HL7Date)getValue("DelegatedTimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param DelegatedTimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setDelegatedTimeAuthored(HL7Date DelegatedTimeAuthored) {
			setValue("DelegatedTimeAuthored", DelegatedTimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getDelegatedAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("DelegatedAuthor");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param DelegatedAuthor value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setDelegatedAuthor(uk.nhs.interoperability.payloads.templates.Author DelegatedAuthor) {
			setValue("DelegatedAuthor", DelegatedAuthor);
			return this;
		}
		
		
		/**
		 * Location of DNACPR documentation.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDNACPRDocsLocation() {
			return (String)getValue("DNACPRDocsLocation");
		}
		
		
		
		
		/**
		 * Location of DNACPR documentation.
		 * <br><br>This field is MANDATORY
		 * @param DNACPRDocsLocation value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setDNACPRDocsLocation(String DNACPRDocsLocation) {
			setValue("DNACPRDocsLocation", DNACPRDocsLocation);
			return this;
		}
		
		
		/**
		 * 
		 * @return HL7Date object
		 */	
		public HL7Date getReviewDate() {
			return (HL7Date)getValue("ReviewDate");
		}
		
		
		
		
		/**
		 * 
		 * @param ReviewDate value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setReviewDate(HL7Date ReviewDate) {
			setValue("ReviewDate", ReviewDate);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ResuscitationStatusSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT
		 * @return CodedValue object
		 */	
		public CodedValue getResuscitationStatus() {
			return (CodedValue)getValue("ResuscitationStatus");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ResuscitationStatusSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT
		 * @return ResuscitationStatusSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT getResuscitationStatusEnum() {
			CodedValue cv = (CodedValue)getValue("ResuscitationStatus");
			VocabularyEntry entry = VocabularyFactory.getVocab("ResuscitationStatusSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ResuscitationStatusSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT
		 * @param ResuscitationStatus value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setResuscitationStatus(CodedValue ResuscitationStatus) {
			setValue("ResuscitationStatus", ResuscitationStatus);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ResuscitationStatusSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ResuscitationStatus value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DNACPRDecisionbySeniorResponsibleClinician setResuscitationStatus(VocabularyEntry ResuscitationStatus) {
			Code c = new CodedValue(ResuscitationStatus);
			setValue("ResuscitationStatus", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146422GB01#DNACPRbySRClinician",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of the DNACPR decision by the senior responsible clinician.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeValue", new Field(
												"CodeValue",
												"x:code/@code",
												"EOLDCR",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeSystem", new Field(
												"CodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeDisplayName", new Field(
												"CodeDisplayName",
												"x:code/@displayName",
												"EOL DNACPR by Senior Responsible Clinician",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"The time when DNACPR decision by the senior responsible clinician is recorded.",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRPreference", new Field(
												"DNACPRPreference",
												"x:value",
												"",
												"true",
												"",
												"DNACPRprefSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRPreferenceType", new Field(
												"DNACPRPreferenceType",
												"x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"DNACPRPreference",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthorFunctionCodeValue", new Field(
												"SeniorResponsibleClinicianAuthorFunctionCodeValue",
												"x:author[x:functionCode/@code='OA']/x:functionCode/@code",
												"OA",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthorTypeCode", new Field(
												"SeniorResponsibleClinicianAuthorTypeCode",
												"x:author[x:functionCode/@code='OA']/@typeCode",
												"AUT",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthorContextControlCode", new Field(
												"SeniorResponsibleClinicianAuthorContextControlCode",
												"x:author[x:functionCode/@code='OA']/@contextControlCode",
												"OP",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthorContentId", new Field(
												"SeniorResponsibleClinicianAuthorContentId",
												"x:author[x:functionCode/@code='OA']/npfitlc:contentId/@extension",
												"...",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"SeniorResponsibleClinicianAuthor",
												"true"
												));
	
		put("SeniorResponsibleClinicianAuthorContentIdRoot", new Field(
												"SeniorResponsibleClinicianAuthorContentIdRoot",
												"x:author[x:functionCode/@code='OA']/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("SeniorResponsibleClinicianAuthorTemplateIdRoot", new Field(
												"SeniorResponsibleClinicianAuthorTemplateIdRoot",
												"x:author[x:functionCode/@code='OA']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("SeniorResponsibleClinicianAuthorTemplateId", new Field(
												"SeniorResponsibleClinicianAuthorTemplateId",
												"x:author[x:functionCode/@code='OA']/x:templateId/@extension",
												"COCD_TP146422GB01#author",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("SeniorResponsibleClinicianTimeAuthored", new Field(
												"SeniorResponsibleClinicianTimeAuthored",
												"x:author[x:functionCode/@code='OA']/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthor", new Field(
												"SeniorResponsibleClinicianAuthor",
												"x:author[x:functionCode/@code='OA']/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author[x:functionCode/@code='OA']/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthorFunctionCodeSystem", new Field(
												"SeniorResponsibleClinicianAuthorFunctionCodeSystem",
												"x:author[x:functionCode/@code='OA']/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SeniorResponsibleClinicianAuthorFunctionCodeDisplayName", new Field(
												"SeniorResponsibleClinicianAuthorFunctionCodeDisplayName",
												"x:author[x:functionCode/@code='OA']/x:functionCode/@displayName",
												"Originating Author",
												"SeniorResponsibleClinicianAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthorFunctionCodeValue", new Field(
												"DelegatedAuthorFunctionCodeValue",
												"x:author[x:functionCode/@code='DA']/x:functionCode/@code",
												"DA",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthorTypeCode", new Field(
												"DelegatedAuthorTypeCode",
												"x:author[x:functionCode/@code='DA']/@typeCode",
												"AUT",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthorContextControlCode", new Field(
												"DelegatedAuthorContextControlCode",
												"x:author[x:functionCode/@code='DA']/@contextControlCode",
												"OP",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthorContentId", new Field(
												"DelegatedAuthorContentId",
												"x:author[x:functionCode/@code='DA']/npfitlc:contentId/@extension",
												"...",
												"DelegatedAuthor",
												"",
												"",
												"",
												"DelegatedAuthor",
												"true"
												));
	
		put("DelegatedAuthorContentIdRoot", new Field(
												"DelegatedAuthorContentIdRoot",
												"x:author[x:functionCode/@code='DA']/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("DelegatedAuthorTemplateIdRoot", new Field(
												"DelegatedAuthorTemplateIdRoot",
												"x:author[x:functionCode/@code='DA']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("DelegatedAuthorTemplateId", new Field(
												"DelegatedAuthorTemplateId",
												"x:author[x:functionCode/@code='DA']/x:templateId/@extension",
												"COCD_TP146422GB01#author1",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("DelegatedTimeAuthored", new Field(
												"DelegatedTimeAuthored",
												"x:author[x:functionCode/@code='DA']/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthor", new Field(
												"DelegatedAuthor",
												"x:author[x:functionCode/@code='DA']/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author[x:functionCode/@code='DA']/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthorFunctionCodeSystem", new Field(
												"DelegatedAuthorFunctionCodeSystem",
												"x:author[x:functionCode/@code='DA']/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DelegatedAuthorFunctionCodeDisplayName", new Field(
												"DelegatedAuthorFunctionCodeDisplayName",
												"x:author[x:functionCode/@code='DA']/x:functionCode/@displayName",
												"Delegated Author",
												"DelegatedAuthor",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateId", new Field(
												"ER1TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:templateId/@extension",
												"COCD_TP146422GB01#entryRelationship1",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TemplateIdRoot", new Field(
												"ER1TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1TypeCode", new Field(
												"ER1TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1ContextConductionInd", new Field(
												"ER1ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER1SeperatableInd", new Field(
												"ER1SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:seperatableInd/@value",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocClassCode", new Field(
												"DNACPRDocsLocClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocMoodCode ", new Field(
												"DNACPRDocsLocMoodCode ",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocTemplateIdRoot", new Field(
												"DNACPRDocsLocTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocTemplateId", new Field(
												"DNACPRDocsLocTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:templateId/@extension",
												"COCD_TP146422GB01#dNACPRDocsLocation",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocCodeValue", new Field(
												"DNACPRDocsLocCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:code/@code",
												"EOLDDL",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocCodeSystem", new Field(
												"DNACPRDocsLocCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocCodeDisplayName", new Field(
												"DNACPRDocsLocCodeDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:code/@displayName",
												"EOL DNACPR Docs Location",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocation", new Field(
												"DNACPRDocsLocation",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:value",
												"Location of DNACPR documentation.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DNACPRDocsLocationDataType", new Field(
												"DNACPRDocsLocationDataType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship1']/x:observation/x:value/@xsi:type",
												"ST",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2TemplateId", new Field(
												"ER2TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:templateId/@extension",
												"COCD_TP146422GB01#entryRelationship2",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2TemplateIdRoot", new Field(
												"ER2TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2TypeCode", new Field(
												"ER2TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/@typeCode",
												"COMP",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2ContextConductionInd", new Field(
												"ER2ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/@contextConductionInd",
												"true",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER2SeperatableInd", new Field(
												"ER2SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:seperatableInd/@value",
												"false",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateTemplateIdRoot", new Field(
												"ReviewDateTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateTemplateId", new Field(
												"ReviewDateTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:templateId/@extension",
												"COCD_TP146422GB01#reviewDateDNACPR",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateClassCode", new Field(
												"ReviewDateClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/@classCode",
												"OBS",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateMoodCode", new Field(
												"ReviewDateMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/@moodCode",
												"EVN",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateCodeValue", new Field(
												"ReviewDateCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:code/@code",
												"EOLDRD",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateCodeSystem", new Field(
												"ReviewDateCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateDisplayName", new Field(
												"ReviewDateDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:code/@displayName",
												"EOL DNACPR Review Date",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDate", new Field(
												"ReviewDate",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:value/@value",
												"",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReviewDateDataType", new Field(
												"ReviewDateDataType",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship2']/x:observation/x:value/@xsi:type",
												"TS",
												"ReviewDate",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TemplateId", new Field(
												"ER4TemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:templateId/@extension",
												"COCD_TP146422GB01#entryRelationship4",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TemplateIdRoot", new Field(
												"ER4TemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4TypeCode", new Field(
												"ER4TypeCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/@typeCode",
												"COMP",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4ContextConductionInd", new Field(
												"ER4ContextConductionInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/@contextConductionInd",
												"true",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ER4SeperatableInd", new Field(
												"ER4SeperatableInd",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:seperatableInd/@value",
												"false",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResuscitationStatus", new Field(
												"ResuscitationStatus",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:value",
												"",
												"false",
												"",
												"ResuscitationStatusSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusTemplateIdRoot", new Field(
												"RStatusTemplateIdRoot",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusTemplateId", new Field(
												"RStatusTemplateId",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:templateId/@extension",
												"COCD_TP146422GB01#resuscitationStatus",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusClassCode", new Field(
												"RStatusClassCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/@classCode",
												"OBS",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusMoodCode", new Field(
												"RStatusMoodCode",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/@moodCode",
												"EVN",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusCodeValue", new Field(
												"RStatusCodeValue",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:code/@code",
												"EOLRST",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusCodeSystem", new Field(
												"RStatusCodeSystem",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RStatusCodeDisplayName", new Field(
												"RStatusCodeDisplayName",
												"x:entryRelationship[x:templateId/@extension='COCD_TP146422GB01#entryRelationship4']/x:observation/x:code/@displayName",
												"EOL Resuscitation Status",
												"ResuscitationStatus",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public DNACPRDecisionbySeniorResponsibleClinician() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public DNACPRDecisionbySeniorResponsibleClinician(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	
	