/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the RelatedPersonDetails object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeInvolvementStarted</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeInvolvementEnded</li>
 * <li>{@link PatientRelationshipParticipantRole PatientRelationshipParticipantRole} Participant</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class RelatedPersonDetails extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "RelatedPersonDetails";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param TimeInvolvementStarted TimeInvolvementStarted
		 * @param TimeInvolvementEnded TimeInvolvementEnded
		 * @param Participant Participant
		 */
	    public RelatedPersonDetails(HL7Date TimeInvolvementStarted, HL7Date TimeInvolvementEnded, PatientRelationshipParticipantRole Participant) {
			fields = new LinkedHashMap<String, Object>();
			
			setTimeInvolvementStarted(TimeInvolvementStarted);
			setTimeInvolvementEnded(TimeInvolvementEnded);
			setParticipant(Participant);
		}
	
		/**
		 * The time at which the participant became involved in the end of life care plan of patient
		 * @return HL7Date object
		 */	
		public HL7Date getTimeInvolvementStarted() {
			return (HL7Date)getValue("TimeInvolvementStarted");
		}
		
		
		
		
		/**
		 * The time at which the participant became involved in the end of life care plan of patient
		 * @param TimeInvolvementStarted value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedPersonDetails setTimeInvolvementStarted(HL7Date TimeInvolvementStarted) {
			setValue("TimeInvolvementStarted", TimeInvolvementStarted);
			return this;
		}
		
		
		/**
		 * The time at which the participant's involvement in the end of life care plan of patient ended (optional)
		 * @return HL7Date object
		 */	
		public HL7Date getTimeInvolvementEnded() {
			return (HL7Date)getValue("TimeInvolvementEnded");
		}
		
		
		
		
		/**
		 * The time at which the participant's involvement in the end of life care plan of patient ended (optional)
		 * @param TimeInvolvementEnded value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedPersonDetails setTimeInvolvementEnded(HL7Date TimeInvolvementEnded) {
			setValue("TimeInvolvementEnded", TimeInvolvementEnded);
			return this;
		}
		
		
		/**
		 * Details of the participant
		 * <br><br>This field is MANDATORY
		 * @return PatientRelationshipParticipantRole object
		 */	
		public PatientRelationshipParticipantRole getParticipant() {
			return (PatientRelationshipParticipantRole)getValue("Participant");
		}
		
		
		
		
		/**
		 * Details of the participant
		 * <br><br>This field is MANDATORY
		 * @param Participant value to set
		 * @return this To allow the use of the builder pattern
		 */
		public RelatedPersonDetails setParticipant(PatientRelationshipParticipantRole Participant) {
			setValue("Participant", Participant);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TypeCode", new Field(
												"TypeCode",
												"@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContextControlCode", new Field(
												"ContextControlCode",
												"@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146416GB01#participant",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdRoot", new Field(
												"ContentIdRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentId", new Field(
												"ContentId",
												"npfitlc:contentId/@extension",
												"COCD_TP145226GB01#ParticipantRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeInvolvementStarted", new Field(
												"TimeInvolvementStarted",
												"x:time/x:low",
												"The time at which the participant became involved in the end of life care plan of patient",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeInvolvementEnded", new Field(
												"TimeInvolvementEnded",
												"x:time/x:high",
												"The time at which the participant's involvement in the end of life care plan of patient ended (optional)",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Participant", new Field(
												"Participant",
												"x:participantRole",
												"Details of the participant",
												"true",
												"",
												"",
												"PatientRelationshipParticipantRole",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public RelatedPersonDetails() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public RelatedPersonDetails(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	
	