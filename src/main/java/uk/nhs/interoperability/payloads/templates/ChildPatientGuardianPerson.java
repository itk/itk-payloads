/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ChildPatientGuardianPerson object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} GuardianName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ChildPatientGuardianPerson extends AbstractPayload implements Payload , ChildPatientGuardianChoice {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ChildPatientGuardianPerson";
		protected static final String shortName = "guardianPerson";
		protected static final String rootNode = "x:guardianPerson";
		protected static final String version = "COCD_TP145230GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param GuardianName GuardianName
		 */
	    public ChildPatientGuardianPerson(uk.nhs.interoperability.payloads.commontypes.PersonName GuardianName) {
			fields = new LinkedHashMap<String, Object>();
			
			setGuardianName(GuardianName);
		}
	
		/**
		 * A name associated with the person who is the patient's guardian
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getGuardianName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("GuardianName");
		}
		
		
		
		
		/**
		 * A name associated with the person who is the patient's guardian
		 * <br><br>This field is MANDATORY
		 * @param GuardianName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianPerson setGuardianName(uk.nhs.interoperability.payloads.commontypes.PersonName GuardianName) {
			setValue("GuardianName", GuardianName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("GuardianPersonClassCode", new Field(
												"GuardianPersonClassCode",
												"@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianPersonDeterminerCode", new Field(
												"GuardianPersonDeterminerCode",
												"@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianPersonTemplateIdRoot", new Field(
												"GuardianPersonTemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianPersonTemplateId", new Field(
												"GuardianPersonTemplateId",
												"x:templateId/@extension",
												"COCD_TP145230GB02#guardianPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianName", new Field(
												"GuardianName",
												"x:name",
												"A name associated with the person who is the patient's guardian",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ChildPatientGuardianPerson() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ChildPatientGuardianPerson(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


			

	
	