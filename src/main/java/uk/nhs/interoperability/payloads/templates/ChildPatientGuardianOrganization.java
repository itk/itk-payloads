/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ChildPatientGuardianOrganization object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID}&gt; OrganisationId</li>
 * <li>List&lt;String&gt; OrganisationName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; OrganisationTelephoneNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; OrganisationAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} OrganisationType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ChildPatientGuardianOrganization extends AbstractPayload implements Payload , ChildPatientGuardianChoice {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ChildPatientGuardianOrganization";
		protected static final String shortName = "guardianOrganization";
		protected static final String rootNode = "x:guardianOrganization";
		protected static final String version = "COCD_TP145230GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param OrganisationId OrganisationId
		 * @param OrganisationName OrganisationName
		 * @param OrganisationTelephoneNumber OrganisationTelephoneNumber
		 * @param OrganisationAddress OrganisationAddress
		 * @param OrganisationType OrganisationType
		 */
	    public ChildPatientGuardianOrganization(
		List<uk.nhs.interoperability.payloads.commontypes.OrgID> OrganisationId, 
		List<String> OrganisationName, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> OrganisationTelephoneNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> OrganisationAddress, CodedValue OrganisationType) {
			fields = new LinkedHashMap<String, Object>();
			
			setOrganisationId(OrganisationId);
			setOrganisationName(OrganisationName);
			setOrganisationTelephoneNumber(OrganisationTelephoneNumber);
			setOrganisationAddress(OrganisationAddress);
			setOrganisationType(OrganisationType);
		}
	
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which is acting as the patient's guardian
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.OrgID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.OrgID> getOrganisationId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.OrgID>)getValue("OrganisationId");
		}
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which is acting as the patient's guardian
		 * <br><br>This field is MANDATORY
		 * @param OrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization setOrganisationId(List OrganisationId) {
			setValue("OrganisationId", OrganisationId);
			return this;
		}
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation which is acting as the patient's guardian
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.OrgID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.OrgID objects.
		 * @param OrganisationId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization addOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId) {
			addMultivalue("OrganisationId", OrganisationId);
			return this;
		}
		
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @return List of String objects
		 */
		public List<String> getOrganisationName() {
			return (List<String>)getValue("OrganisationName");
		}
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @param OrganisationName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization setOrganisationName(List OrganisationName) {
			setValue("OrganisationName", OrganisationName);
			return this;
		}
		
		/**
		 * The description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a String object, but this method can be called
		 * multiple times to add additional String objects.
		 * @param OrganisationName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization addOrganisationName(String OrganisationName) {
			addMultivalue("OrganisationName", OrganisationName);
			return this;
		}
		
		
		/**
		 * Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getOrganisationTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("OrganisationTelephoneNumber");
		}
		
		/**
		 * Telephone Number
		 * @param OrganisationTelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization setOrganisationTelephoneNumber(List OrganisationTelephoneNumber) {
			setValue("OrganisationTelephoneNumber", OrganisationTelephoneNumber);
			return this;
		}
		
		/**
		 * Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param OrganisationTelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization addOrganisationTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom OrganisationTelephoneNumber) {
			addMultivalue("OrganisationTelephoneNumber", OrganisationTelephoneNumber);
			return this;
		}
		
		
		/**
		 * A contact address for the organisation
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getOrganisationAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("OrganisationAddress");
		}
		
		/**
		 * A contact address for the organisation
		 * @param OrganisationAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization setOrganisationAddress(List OrganisationAddress) {
			setValue("OrganisationAddress", OrganisationAddress);
			return this;
		}
		
		/**
		 * A contact address for the organisation
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param OrganisationAddress value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization addOrganisationAddress(uk.nhs.interoperability.payloads.commontypes.Address OrganisationAddress) {
			addMultivalue("OrganisationAddress", OrganisationAddress);
			return this;
		}
		
		
		/**
		 * A code taken from the GuardianOrganisationType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "GuardianOrganisationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getOrganisationType() {
			return (CodedValue)getValue("OrganisationType");
		}
		
		
		
		/**
		 * A code taken from the GuardianOrganisationType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "GuardianOrganisationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType
		 * <br><br>This field is MANDATORY
		 * @return GuardianOrganisationType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType getOrganisationTypeEnum() {
			CodedValue cv = (CodedValue)getValue("OrganisationType");
			VocabularyEntry entry = VocabularyFactory.getVocab("GuardianOrganisationType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType)entry;
		}
		
		
		/**
		 * A code taken from the GuardianOrganisationType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "GuardianOrganisationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType
		 * <br><br>This field is MANDATORY
		 * @param OrganisationType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization setOrganisationType(CodedValue OrganisationType) {
			setValue("OrganisationType", OrganisationType);
			return this;
		}
		
		
		/**
		 * A code taken from the GuardianOrganisationType vocabulary to indicate the type of care provider
  		 * <br>NOTE: This field should be populated using the "GuardianOrganisationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param OrganisationType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ChildPatientGuardianOrganization setOrganisationType(VocabularyEntry OrganisationType) {
			Code c = new CodedValue(OrganisationType);
			setValue("OrganisationType", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("GuardianOrgClassCode", new Field(
												"GuardianOrgClassCode",
												"@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianOrgDeterminerCode", new Field(
												"GuardianOrgDeterminerCode",
												"@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianOrgTemplateIdRoot", new Field(
												"GuardianOrgTemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GuardianOrgTemplateId", new Field(
												"GuardianOrgTemplateId",
												"x:templateId/@extension",
												"COCD_TP145230GB02#guardianOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationId", new Field(
												"OrganisationId",
												"x:id",
												"An ODS code as an identifier that uniquely identifies the organisation which is acting as the patient's guardian",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationName", new Field(
												"OrganisationName",
												"x:name",
												"The description of the organisation associated with the ODS code",
												"true",
												"",
												"",
												"String",
												"",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTelephoneNumber", new Field(
												"OrganisationTelephoneNumber",
												"x:telecom",
												"Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationAddress", new Field(
												"OrganisationAddress",
												"x:addr",
												"A contact address for the organisation",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationType", new Field(
												"OrganisationType",
												"x:standardIndustryClassCode",
												"A code taken from the GuardianOrganisationType vocabulary to indicate the type of care provider",
												"true",
												"",
												"GuardianOrganisationType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ChildPatientGuardianOrganization() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ChildPatientGuardianOrganization(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	