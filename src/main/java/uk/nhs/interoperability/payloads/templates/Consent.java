/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Consent object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.ConsentID ConsentID}&gt; ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ConsentCode</li>
 * <li>String ConsentOriginalText</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Consent extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "Consent";
		protected static final String shortName = "Consent";
		protected static final String rootNode = "";
		protected static final String version = "COCD_TP146226GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param ConsentCode ConsentCode
		 * @param ConsentOriginalText ConsentOriginalText
		 */
	    public Consent(
		List<uk.nhs.interoperability.payloads.commontypes.ConsentID> ID, CodedValue ConsentCode, String ConsentOriginalText) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setConsentCode(ConsentCode);
			setConsentOriginalText(ConsentOriginalText);
		}
	
		/**
		 * An identifier to identify an instance of a consent
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.ConsentID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.ConsentID> getID() {
			return (List<uk.nhs.interoperability.payloads.commontypes.ConsentID>)getValue("ID");
		}
		
		/**
		 * An identifier to identify an instance of a consent
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Consent setID(List ID) {
			setValue("ID", ID);
			return this;
		}
		
		/**
		 * An identifier to identify an instance of a consent
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.ConsentID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.ConsentID objects.
		 * @param ID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Consent addID(uk.nhs.interoperability.payloads.commontypes.ConsentID ID) {
			addMultivalue("ID", ID);
			return this;
		}
		
		
		/**
		 * A code to describe the type of consent that is associated with the document
  		 * <br>NOTE: This field should be populated using the "DocumentConsentSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getConsentCode() {
			return (CodedValue)getValue("ConsentCode");
		}
		
		
		
		/**
		 * A code to describe the type of consent that is associated with the document
  		 * <br>NOTE: This field should be populated using the "DocumentConsentSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT
		 * <br><br>This field is MANDATORY
		 * @return DocumentConsentSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT getConsentCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ConsentCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("DocumentConsentSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT)entry;
		}
		
		
		/**
		 * A code to describe the type of consent that is associated with the document
  		 * <br>NOTE: This field should be populated using the "DocumentConsentSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT
		 * <br><br>This field is MANDATORY
		 * @param ConsentCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Consent setConsentCode(CodedValue ConsentCode) {
			setValue("ConsentCode", ConsentCode);
			return this;
		}
		
		
		/**
		 * A code to describe the type of consent that is associated with the document
  		 * <br>NOTE: This field should be populated using the "DocumentConsentSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ConsentCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Consent setConsentCode(VocabularyEntry ConsentCode) {
			Code c = new CodedValue(ConsentCode);
			setValue("ConsentCode", c);
			return this;
		}
		
		/**
		 * May be used if required to carry the text or phrase used as the basis for the coding
		 * @return String object
		 */	
		public String getConsentOriginalText() {
			return (String)getValue("ConsentOriginalText");
		}
		
		
		
		
		/**
		 * May be used if required to carry the text or phrase used as the basis for the coding
		 * @param ConsentOriginalText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Consent setConsentOriginalText(String ConsentOriginalText) {
			setValue("ConsentOriginalText", ConsentOriginalText);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"CONS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146226GB02#Consent",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id",
												"An identifier to identify an instance of a consent",
												"true",
												"",
												"",
												"ConsentID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConsentCode", new Field(
												"ConsentCode",
												"x:code",
												"A code to describe the type of consent that is associated with the document",
												"true",
												"",
												"DocumentConsentSnCT",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.334",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConsentOriginalText", new Field(
												"ConsentOriginalText",
												"x:code/@originalText",
												"May be used if required to carry the text or phrase used as the basis for the coding",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Consent() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Consent(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	