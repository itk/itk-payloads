/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the DocumentParticipantUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String RoleClass</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} RoleCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} Name</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CDAOrganizationType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class DocumentParticipantUniversal extends AbstractPayload implements Payload , Participant {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "DocumentParticipantUniversal";
		protected static final String shortName = "AssociatedEntity";
		protected static final String rootNode = "x:associatedEntity";
		protected static final String version = "COCD_TP145214GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param RoleClass RoleClass
		 * @param Id Id
		 * @param RoleCode RoleCode
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param Name Name
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 * @param CDAOrganizationType CDAOrganizationType
		 */
	    public DocumentParticipantUniversal(String RoleClass, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> Id, CodedValue RoleCode, uk.nhs.interoperability.payloads.commontypes.Address Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName Name, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName, CodedValue CDAOrganizationType) {
			fields = new LinkedHashMap<String, Object>();
			
			setRoleClass(RoleClass);
			setId(Id);
			setRoleCode(RoleCode);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setName(Name);
			setOrgId(OrgId);
			setOrgName(OrgName);
			setCDAOrganizationType(CDAOrganizationType);
		}
	
		/**
		 * The type of role using a value from the RoleClassAssociative vocabulary
  		 * <br>NOTE: This field should be populated using the "RoleClassAssociative" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getRoleClass() {
			return (String)getValue("RoleClass");
		}
		
		
		
		/**
		 * The type of role using a value from the RoleClassAssociative vocabulary
  		 * <br>NOTE: This field should be populated using the "RoleClassAssociative" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative
		 * <br><br>This field is MANDATORY
		 * @return RoleClassAssociative enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative getRoleClassEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative.getByCode((String)getValue("RoleClass"));
		}
		
		
		/**
		 * The type of role using a value from the RoleClassAssociative vocabulary
  		 * <br>NOTE: This field should be populated using the "RoleClassAssociative" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative
		 * <br><br>This field is MANDATORY
		 * @param RoleClass value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setRoleClass(String RoleClass) {
			setValue("RoleClass", RoleClass);
			return this;
		}
		
		
		/**
		 * Identification of the individual using a national or local identifier. When the person is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("Id");
		}
		
		/**
		 * Identification of the individual using a national or local identifier. When the person is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setId(List Id) {
			setValue("Id", Id);
			return this;
		}
		
		/**
		 * Identification of the individual using a national or local identifier. When the person is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param Id value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal addId(uk.nhs.interoperability.payloads.commontypes.PersonID Id) {
			addMultivalue("Id", Id);
			return this;
		}
		
		
		/**
		 * A code to describe the role of the individual
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getRoleCode() {
			return (CodedValue)getValue("RoleCode");
		}
		
		
		
		/**
		 * A code to describe the role of the individual
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getRoleCodeEnum() {
			CodedValue cv = (CodedValue)getValue("RoleCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code to describe the role of the individual
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param RoleCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setRoleCode(CodedValue RoleCode) {
			setValue("RoleCode", RoleCode);
			return this;
		}
		
		
		/**
		 * A code to describe the role of the individual
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param RoleCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setRoleCode(VocabularyEntry RoleCode) {
			Code c = new CodedValue(RoleCode);
			setValue("RoleCode", c);
			return this;
		}
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("Address");
		}
		
		
		
		
		/**
		 * 
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			setValue("Address", Address);
			return this;
		}
		
		
		/**
		 * Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * Telephone Number
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Person name
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @return CodedValue object
		 */	
		public CodedValue getCDAOrganizationType() {
			return (CodedValue)getValue("CDAOrganizationType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @return CDAOrganizationType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType getCDAOrganizationTypeEnum() {
			CodedValue cv = (CodedValue)getValue("CDAOrganizationType");
			VocabularyEntry entry = VocabularyFactory.getVocab("CDAOrganizationType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * @param CDAOrganizationType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setCDAOrganizationType(CodedValue CDAOrganizationType) {
			setValue("CDAOrganizationType", CDAOrganizationType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "CDAOrganizationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CDAOrganizationType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipantUniversal setCDAOrganizationType(VocabularyEntry CDAOrganizationType) {
			Code c = new CodedValue(CDAOrganizationType);
			setValue("CDAOrganizationType", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("RoleClass", new Field(
												"RoleClass",
												"@classCode",
												"The type of role using a value from the RoleClassAssociative vocabulary",
												"true",
												"",
												"RoleClassAssociative",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145214GB01#AssociatedEntity",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id",
												"Identification of the individual using a national or local identifier. When the person is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system",
												"false",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RoleCode", new Field(
												"RoleCode",
												"x:code",
												"A code to describe the role of the individual",
												"false",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.409",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:associatedPerson/@classCode",
												"PSN",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:associatedPerson/@determinerCode",
												"INSTANCE",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateIdRoot", new Field(
												"PersonTemplateIdRoot",
												"x:associatedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateId", new Field(
												"PersonTemplateId",
												"x:associatedPerson/x:templateId/@extension",
												"COCD_TP145214GB01#associatedPerson",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:associatedPerson/x:name",
												"Person name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationClassCode", new Field(
												"OrganisationClassCode",
												"x:scopingOrganization/@classCode",
												"ORG",
												"OrgName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationDeterminerCode", new Field(
												"OrganisationDeterminerCode",
												"x:scopingOrganization/@determinerCode",
												"INSTANCE",
												"OrgName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTemplateIdRoot", new Field(
												"OrganisationTemplateIdRoot",
												"x:scopingOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrgName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTemplateId", new Field(
												"OrganisationTemplateId",
												"x:scopingOrganization/x:templateId/@extension",
												"COCD_TP145214GB01#scopingOrganization",
												"OrgName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:scopingOrganization/x:id",
												"",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:scopingOrganization/x:name",
												"",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDAOrganizationType", new Field(
												"CDAOrganizationType",
												"x:scopingOrganization/x:standardIndustryClassCode",
												"",
												"false",
												"",
												"CDAOrganizationType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public DocumentParticipantUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public DocumentParticipantUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	