/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ParticipantOrganizationUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} RoleCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} Addr</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; Telecom</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrganisationId</li>
 * <li>String OrganisationDescription</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ParticipantOrganizationUniversal extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ParticipantOrganizationUniversal";
		protected static final String shortName = "ParticipantRole";
		protected static final String rootNode = "x:participantRole";
		protected static final String version = "COCD_TP000029GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param RoleCode RoleCode
		 * @param Addr Addr
		 * @param Telecom Telecom
		 * @param OrganisationId OrganisationId
		 * @param OrganisationDescription OrganisationDescription
		 */
	    public ParticipantOrganizationUniversal(CodedValue RoleCode, uk.nhs.interoperability.payloads.commontypes.Address Addr, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> Telecom, uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId, String OrganisationDescription) {
			fields = new LinkedHashMap<String, Object>();
			
			setRoleCode(RoleCode);
			setAddr(Addr);
			setTelecom(Telecom);
			setOrganisationId(OrganisationId);
			setOrganisationDescription(OrganisationDescription);
		}
	
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getRoleCode() {
			return (CodedValue)getValue("RoleCode");
		}
		
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getRoleCodeEnum() {
			CodedValue cv = (CodedValue)getValue("RoleCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param RoleCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal setRoleCode(CodedValue RoleCode) {
			setValue("RoleCode", RoleCode);
			return this;
		}
		
		
		/**
		 * A code to describe the organisation's role
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param RoleCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal setRoleCode(VocabularyEntry RoleCode) {
			Code c = new CodedValue(RoleCode);
			setValue("RoleCode", c);
			return this;
		}
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getAddr() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("Addr");
		}
		
		
		
		
		/**
		 * 
		 * @param Addr value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal setAddr(uk.nhs.interoperability.payloads.commontypes.Address Addr) {
			setValue("Addr", Addr);
			return this;
		}
		
		
		/**
		 * Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelecom() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("Telecom");
		}
		
		/**
		 * Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.
		 * @param Telecom value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal setTelecom(List Telecom) {
			setValue("Telecom", Telecom);
			return this;
		}
		
		/**
		 * Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param Telecom value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal addTelecom(uk.nhs.interoperability.payloads.commontypes.Telecom Telecom) {
			addMultivalue("Telecom", Telecom);
			return this;
		}
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation in the role.
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrganisationId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrganisationId");
		}
		
		
		
		
		/**
		 * An ODS code as an identifier that uniquely identifies the organisation in the role.
		 * <br><br>This field is MANDATORY
		 * @param OrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal setOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId) {
			setValue("OrganisationId", OrganisationId);
			return this;
		}
		
		
		/**
		 * Description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrganisationDescription() {
			return (String)getValue("OrganisationDescription");
		}
		
		
		
		
		/**
		 * Description of the organisation associated with the ODS code
		 * <br><br>This field is MANDATORY
		 * @param OrganisationDescription value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ParticipantOrganizationUniversal setOrganisationDescription(String OrganisationDescription) {
			setValue("OrganisationDescription", OrganisationDescription);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ROL",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP000029GB01#ParticipantRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RoleCode", new Field(
												"RoleCode",
												"x:code",
												"A code to describe the organisation's role",
												"false",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Addr", new Field(
												"Addr",
												"x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Telecom", new Field(
												"Telecom",
												"x:telecom",
												"Contact details for the organisation playing the role in the form of telephone numbers, fax numbers and email addresses.",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:scopingEntity/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:scopingEntity/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:scopingEntity/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:scopingEntity/x:templateId/@extension",
												"COCD_TP000029GB01#scopingOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationId", new Field(
												"OrganisationId",
												"x:scopingEntity/x:id",
												"An ODS code as an identifier that uniquely identifies the organisation in the role.",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationDescription", new Field(
												"OrganisationDescription",
												"x:scopingEntity/x:desc",
												"Description of the organisation associated with the ODS code",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ParticipantOrganizationUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ParticipantOrganizationUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	