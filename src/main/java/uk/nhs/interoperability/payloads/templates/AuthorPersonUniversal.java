/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the AuthorPersonUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} JobRoleName</li>
 * <li>String JobRoleNameNullFlavour</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} Name</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrganisationId</li>
 * <li>String OrganisationName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class AuthorPersonUniversal extends AbstractPayload implements Payload , Author, ChildScreeningAuthor {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "AuthorPersonUniversal";
		protected static final String shortName = "AssignedAuthor";
		protected static final String rootNode = "x:assignedAuthor";
		protected static final String version = "COCD_TP145200GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param JobRoleName JobRoleName
		 * @param JobRoleNameNullFlavour JobRoleNameNullFlavour
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param Name Name
		 * @param OrganisationId OrganisationId
		 * @param OrganisationName OrganisationName
		 */
	    public AuthorPersonUniversal(
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> Id, CodedValue JobRoleName, String JobRoleNameNullFlavour, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName Name, uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId, String OrganisationName) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setJobRoleName(JobRoleName);
			setJobRoleNameNullFlavour(JobRoleNameNullFlavour);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setName(Name);
			setOrganisationId(OrganisationId);
			setOrganisationName(OrganisationName);
		}
	
		/**
		 * This is an identifier for the Author. When the role is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("Id");
		}
		
		/**
		 * This is an identifier for the Author. When the role is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setId(List Id) {
			setValue("Id", Id);
			return this;
		}
		
		/**
		 * This is an identifier for the Author. When the role is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param Id value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal addId(uk.nhs.interoperability.payloads.commontypes.PersonID Id) {
			addMultivalue("Id", Id);
			return this;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Author (code)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return CodedValue object
		 */	
		public CodedValue getJobRoleName() {
			return (CodedValue)getValue("JobRoleName");
		}
		
		
		
		/**
		 * This is an identifier for the Job Role of the Author (code)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("JobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Author (code)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @param JobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setJobRoleName(CodedValue JobRoleName) {
			setValue("JobRoleName", JobRoleName);
			return this;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Author (code)
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param JobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setJobRoleName(VocabularyEntry JobRoleName) {
			Code c = new CodedValue(JobRoleName);
			setValue("JobRoleName", c);
			return this;
		}
		
		/**
		 * A null flavour can be used when the job role name cannot be provided. This must only be used when the job role name field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getJobRoleNameNullFlavour() {
			return (String)getValue("JobRoleNameNullFlavour");
		}
		
		
		
		/**
		 * A null flavour can be used when the job role name cannot be provided. This must only be used when the job role name field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getJobRoleNameNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("JobRoleNameNullFlavour"));
		}
		
		
		/**
		 * A null flavour can be used when the job role name cannot be provided. This must only be used when the job role name field is null
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param JobRoleNameNullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setJobRoleNameNullFlavour(String JobRoleNameNullFlavour) {
			setValue("JobRoleNameNullFlavour", JobRoleNameNullFlavour);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * 
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Author name
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Author name
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrganisationId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrganisationId");
		}
		
		
		
		
		/**
		 * 
		 * @param OrganisationId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setOrganisationId(uk.nhs.interoperability.payloads.commontypes.OrgID OrganisationId) {
			setValue("OrganisationId", OrganisationId);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getOrganisationName() {
			return (String)getValue("OrganisationName");
		}
		
		
		
		
		/**
		 * 
		 * @param OrganisationName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public AuthorPersonUniversal setOrganisationName(String OrganisationName) {
			setValue("OrganisationName", OrganisationName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("AssignedAuthorClassCode", new Field(
												"AssignedAuthorClassCode",
												"@classCode",
												"ASSIGNED",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedAuthorTemplateIdRoot", new Field(
												"AssignedAuthorTemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedAuthorTemplateId", new Field(
												"AssignedAuthorTemplateId",
												"x:templateId/@extension",
												"COCD_TP145200GB01#AssignedAuthor",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id",
												"This is an identifier for the Author. When the role is identified using SDS then the first instance of the XML element id is mandatory and shall carry the SDS User ID, the second instance of XML element id is required and shall carry the SDS User Role Profile ID when available on the sending system",
												"false",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("JobRoleName", new Field(
												"JobRoleName",
												"x:code",
												"This is an identifier for the Job Role of the Author (code)",
												"false",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("JobRoleNameNullFlavour", new Field(
												"JobRoleNameNullFlavour",
												"x:code/@nullFlavor",
												"A null flavour can be used when the job role name cannot be provided. This must only be used when the job role name field is null",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:addr",
												"",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedPersonClassCode", new Field(
												"AssignedPersonClassCode",
												"x:assignedPerson/@classCode",
												"PSN",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedPersonDeterminerCode", new Field(
												"AssignedPersonDeterminerCode",
												"x:assignedPerson/@determinerCode",
												"INSTANCE",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedPersonTemplateIdRoot", new Field(
												"AssignedPersonTemplateIdRoot",
												"x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedPersonTemplateId", new Field(
												"AssignedPersonTemplateId",
												"x:assignedPerson/x:templateId/@extension",
												"COCD_TP145200GB01#assignedPerson",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:assignedPerson/x:name",
												"Author name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationClassCode", new Field(
												"OrganisationClassCode",
												"x:representedOrganization/@classCode",
												"ORG",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationDeterminerCode", new Field(
												"OrganisationDeterminerCode",
												"x:representedOrganization/@determinerCode",
												"INSTANCE",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTemplateIdRoot", new Field(
												"OrganisationTemplateIdRoot",
												"x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationTemplateId", new Field(
												"OrganisationTemplateId",
												"x:representedOrganization/x:templateId/@extension",
												"COCD_TP145200GB01#representedOrganization",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationId", new Field(
												"OrganisationId",
												"x:representedOrganization/x:id",
												"",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrganisationName", new Field(
												"OrganisationName",
												"x:representedOrganization/x:name",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public AuthorPersonUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public AuthorPersonUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	