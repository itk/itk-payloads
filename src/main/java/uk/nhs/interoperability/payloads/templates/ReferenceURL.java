/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ReferenceURL object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ReferenceURLType</li>
 * <li>String URL</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ReferenceURL extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ReferenceURL";
		protected static final String shortName = "ReferenceURL";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP146248GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Id Id
		 * @param ReferenceURLType ReferenceURLType
		 * @param URL URL
		 */
	    public ReferenceURL(String Id, CodedValue ReferenceURLType, String URL) {
			fields = new LinkedHashMap<String, Object>();
			
			setId(Id);
			setReferenceURLType(ReferenceURLType);
			setURL(URL);
		}
	
		/**
		 * A DCE UUID to identify this instance of a reference URL
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this instance of a reference URL
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ReferenceURL setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ReferenceURLType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getReferenceURLType() {
			return (CodedValue)getValue("ReferenceURLType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ReferenceURLType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType
		 * <br><br>This field is MANDATORY
		 * @return ReferenceURLType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType getReferenceURLTypeEnum() {
			CodedValue cv = (CodedValue)getValue("ReferenceURLType");
			VocabularyEntry entry = VocabularyFactory.getVocab("ReferenceURLType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ReferenceURLType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType
		 * <br><br>This field is MANDATORY
		 * @param ReferenceURLType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ReferenceURL setReferenceURLType(CodedValue ReferenceURLType) {
			setValue("ReferenceURLType", ReferenceURLType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ReferenceURLType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ReferenceURLType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ReferenceURL setReferenceURLType(VocabularyEntry ReferenceURLType) {
			Code c = new CodedValue(ReferenceURLType);
			setValue("ReferenceURLType", c);
			return this;
		}
		
		/**
		 * The actual URL
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getURL() {
			return (String)getValue("URL");
		}
		
		
		
		
		/**
		 * The actual URL
		 * <br><br>This field is MANDATORY
		 * @param URL value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ReferenceURL setURL(String URL) {
			setValue("URL", URL);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146248GB01#ReferenceURL",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify this instance of a reference URL",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferenceURLType", new Field(
												"ReferenceURLType",
												"x:code",
												"",
												"true",
												"",
												"ReferenceURLType",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.338",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("URL", new Field(
												"URL",
												"x:text",
												"The actual URL",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ReferenceURL() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ReferenceURL(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	