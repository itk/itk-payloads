/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PrognosisAwareness object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PrognosisAwareness</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} RecordedTime</li>
 * <li>{@link Author Author} Author</li>
 * <li>{@link PatientRelationshipParticipantRole PatientRelationshipParticipantRole} MainInformalCarer</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PrognosisAwareness extends AbstractPayload implements Payload , CodedSection {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "PrognosisAwareness";
		protected static final String shortName = "PrognosisAwareness";
		protected static final String rootNode = "x:observation";
		protected static final String version = "COCD_TP146414GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param EffectiveTime EffectiveTime
		 * @param PrognosisAwareness PrognosisAwareness
		 * @param RecordedTime RecordedTime
		 * @param Author Author
		 * @param MainInformalCarer MainInformalCarer
		 */
	    public PrognosisAwareness(String ID, HL7Date EffectiveTime, CodedValue PrognosisAwareness, HL7Date RecordedTime, Author Author, PatientRelationshipParticipantRole MainInformalCarer) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setEffectiveTime(EffectiveTime);
			setPrognosisAwareness(PrognosisAwareness);
			setRecordedTime(RecordedTime);
			setAuthor(Author);
			setMainInformalCarer(MainInformalCarer);
		}
	
		/**
		 * A DCE UUID to identify each instance of prognosis awareness by the main informal carer
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each instance of prognosis awareness by the main informal carer
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PrognosisAwarenessSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getPrognosisAwareness() {
			return (CodedValue)getValue("PrognosisAwareness");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PrognosisAwarenessSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT
		 * <br><br>This field is MANDATORY
		 * @return PrognosisAwarenessSnCT enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT getPrognosisAwarenessEnum() {
			CodedValue cv = (CodedValue)getValue("PrognosisAwareness");
			VocabularyEntry entry = VocabularyFactory.getVocab("PrognosisAwarenessSnCT", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PrognosisAwarenessSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT
		 * <br><br>This field is MANDATORY
		 * @param PrognosisAwareness value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setPrognosisAwareness(CodedValue PrognosisAwareness) {
			setValue("PrognosisAwareness", PrognosisAwareness);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PrognosisAwarenessSnCT" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PrognosisAwareness value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setPrognosisAwareness(VocabularyEntry PrognosisAwareness) {
			Code c = new CodedValue(PrognosisAwareness);
			setValue("PrognosisAwareness", c);
			return this;
		}
		
		/**
		 * The time the author originally recorded the prognosis awareness
		 * @return HL7Date object
		 */	
		public HL7Date getRecordedTime() {
			return (HL7Date)getValue("RecordedTime");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the prognosis awareness
		 * @param RecordedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setRecordedTime(HL7Date RecordedTime) {
			setValue("RecordedTime", RecordedTime);
			return this;
		}
		
		
		/**
		 * An author created using one of the author templates
		 * @return Author object
		 */	
		public Author getAuthor() {
			return (Author)getValue("Author");
		}
		
		
		
		
		/**
		 * An author created using one of the author templates
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setAuthor(Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The participant of the prognosis awareness is the main informal carer
		 * <br><br>This field is MANDATORY
		 * @return PatientRelationshipParticipantRole object
		 */	
		public PatientRelationshipParticipantRole getMainInformalCarer() {
			return (PatientRelationshipParticipantRole)getValue("MainInformalCarer");
		}
		
		
		
		
		/**
		 * The participant of the prognosis awareness is the main informal carer
		 * <br><br>This field is MANDATORY
		 * @param MainInformalCarer value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PrognosisAwareness setMainInformalCarer(PatientRelationshipParticipantRole MainInformalCarer) {
			setValue("MainInformalCarer", MainInformalCarer);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146414GB01#PrognosisAwareness",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to identify each instance of prognosis awareness by the main informal carer",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeCode", new Field(
												"CodeCode",
												"x:code/@code",
												"EOLAOP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeCodeSystem", new Field(
												"CodeCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.435",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodeDisplayName", new Field(
												"CodeDisplayName",
												"x:code/@displayName",
												"EOL Aware of Prognosis",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StatusCode", new Field(
												"StatusCode",
												"x:statusCode/@code",
												"completed",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PrognosisAwareness", new Field(
												"PrognosisAwareness",
												"x:value",
												"",
												"true",
												"",
												"PrognosisAwarenessSnCT",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PrognosisAwarenessType", new Field(
												"PrognosisAwarenessType",
												"x:value/@xsi:type",
												"CV.NPfIT.CDA.Url",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PrognosisAwarenessRef", new Field(
												"PrognosisAwarenessRef",
												"x:value/x:originalText/x:reference/@value",
												"#a6",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecordedTime", new Field(
												"RecordedTime",
												"x:author/x:time",
												"The time the author originally recorded the prognosis awareness",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"An author created using one of the author templates",
												"false",
												"",
												"",
												"Author",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentIdExtension", new Field(
												"AuthorContentIdExtension",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												""
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146414GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCode", new Field(
												"AuthorFunctionCode",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorDisplayName", new Field(
												"AuthorDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTypeCode", new Field(
												"SubjectTypeCode",
												"x:participant/@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectContextControlCode", new Field(
												"SubjectContextControlCode",
												"x:participant/@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateIdRoot", new Field(
												"SubjectTemplateIdRoot",
												"x:participant/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTemplateId", new Field(
												"SubjectTemplateId",
												"x:participant/x:templateId/@extension",
												"COCD_TP146414GB01#participant",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectContentIdRoot", new Field(
												"SubjectContentIdRoot",
												"x:participant/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectContentIdExtension", new Field(
												"SubjectContentIdExtension",
												"x:participant/npfitlc:contentId/@extension",
												"COCD_TP145226GB01#ParticipantRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MainInformalCarer", new Field(
												"MainInformalCarer",
												"x:participant/x:participantRole",
												"The participant of the prognosis awareness is the main informal carer",
												"true",
												"",
												"",
												"PatientRelationshipParticipantRole",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PrognosisAwareness() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PrognosisAwareness(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	