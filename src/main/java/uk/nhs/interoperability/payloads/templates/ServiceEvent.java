/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ServiceEvent object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ClassCode</li>
 * <li>String Id</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EventCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.DateRange DateRange} EffectiveTime</li>
 * <li>List&lt;{@link ServiceEventPerformer ServiceEventPerformer}&gt; EventPerformer</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ServiceEvent extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "ServiceEvent";
		protected static final String shortName = "ServiceEvent";
		protected static final String rootNode = "x:serviceEvent";
		protected static final String version = "COCD_TP146227GB02";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ClassCode ClassCode
		 * @param Id Id
		 * @param EventCode EventCode
		 * @param EffectiveTime EffectiveTime
		 * @param EventPerformer EventPerformer
		 */
	    public ServiceEvent(String ClassCode, String Id, CodedValue EventCode, uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime, 
		List<ServiceEventPerformer> EventPerformer) {
			fields = new LinkedHashMap<String, Object>();
			
			setClassCode(ClassCode);
			setId(Id);
			setEventCode(EventCode);
			setEffectiveTime(EffectiveTime);
			setEventPerformer(EventPerformer);
		}
	
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7ActType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getClassCode() {
			return (String)getValue("ClassCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7ActType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType
		 * <br><br>This field is MANDATORY
		 * @return HL7ActType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType getClassCodeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType.getByCode((String)getValue("ClassCode"));
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7ActType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType
		 * <br><br>This field is MANDATORY
		 * @param ClassCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent setClassCode(String ClassCode) {
			setValue("ClassCode", ClassCode);
			return this;
		}
		
		
		/**
		 * A DCE UUID to identify the service event
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getId() {
			return (String)getValue("Id");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify the service event
		 * <br><br>This field is MANDATORY
		 * @param Id value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent setId(String Id) {
			setValue("Id", Id);
			return this;
		}
		
		
		/**
		 * A code from any vocabulary to describe the type of service event
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getEventCode() {
			return (CodedValue)getValue("EventCode");
		}
		
		
		
		
		/**
		 * A code from any vocabulary to describe the type of service event
		 * <br><br>This field is MANDATORY
		 * @param EventCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent setEventCode(CodedValue EventCode) {
			setValue("EventCode", EventCode);
			return this;
		}
		
		
		/**
		 * A code from any vocabulary to describe the type of service event
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EventCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent setEventCode(VocabularyEntry EventCode) {
			Code c = new CodedValue(EventCode);
			setValue("EventCode", c);
			return this;
		}
		
		/**
		 * The time the actual service event (as opposed to the encounter surrounding the event) took place
		 * @return uk.nhs.interoperability.payloads.commontypes.DateRange object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.DateRange getEffectiveTime() {
			return (uk.nhs.interoperability.payloads.commontypes.DateRange)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * The time the actual service event (as opposed to the encounter surrounding the event) took place
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent setEffectiveTime(uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * Details of care professionals involved with carrying out the service
		 * @return List of ServiceEventPerformer objects
		 */
		public List<ServiceEventPerformer> getEventPerformer() {
			return (List<ServiceEventPerformer>)getValue("EventPerformer");
		}
		
		/**
		 * Details of care professionals involved with carrying out the service
		 * @param EventPerformer value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent setEventPerformer(List EventPerformer) {
			setValue("EventPerformer", EventPerformer);
			return this;
		}
		
		/**
		 * Details of care professionals involved with carrying out the service
		 * <br>Note: This adds a ServiceEventPerformer object, but this method can be called
		 * multiple times to add additional ServiceEventPerformer objects.
		 * @param EventPerformer value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ServiceEvent addEventPerformer(ServiceEventPerformer EventPerformer) {
			addMultivalue("EventPerformer", EventPerformer);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"",
												"true",
												"",
												"HL7ActType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146227GB02#ServiceEvent",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Id", new Field(
												"Id",
												"x:id/@root",
												"A DCE UUID to identify the service event",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventCode", new Field(
												"EventCode",
												"x:code",
												"A code from any vocabulary to describe the type of service event",
												"true",
												"",
												"",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.335",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime",
												"The time the actual service event (as opposed to the encounter surrounding the event) took place",
												"false",
												"",
												"",
												"DateRange",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventPerformer", new Field(
												"EventPerformer",
												"x:performer",
												"Details of care professionals involved with carrying out the service",
												"false",
												"",
												"",
												"ServiceEventPerformer",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ServiceEvent() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ServiceEvent(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	