/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PersonUniversal object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; PersonId</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} JobRoleName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} PersonName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgId</li>
 * <li>String OrgName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PersonUniversal extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "PersonUniversal";
		protected static final String shortName = "AssignedEntity";
		protected static final String rootNode = "x:assignedEntity";
		protected static final String version = "COCD_TP145205GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param PersonId PersonId
		 * @param JobRoleName JobRoleName
		 * @param TelephoneNumber TelephoneNumber
		 * @param PersonName PersonName
		 * @param OrgId OrgId
		 * @param OrgName OrgName
		 */
	    public PersonUniversal(
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> PersonId, CodedValue JobRoleName, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, uk.nhs.interoperability.payloads.commontypes.PersonName PersonName, uk.nhs.interoperability.payloads.commontypes.OrgID OrgId, String OrgName) {
			fields = new LinkedHashMap<String, Object>();
			
			setPersonId(PersonId);
			setJobRoleName(JobRoleName);
			setTelephoneNumber(TelephoneNumber);
			setPersonName(PersonName);
			setOrgId(OrgId);
			setOrgName(OrgName);
		}
	
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getPersonId() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("PersonId");
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br><br>This field is MANDATORY
		 * @param PersonId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setPersonId(List PersonId) {
			setValue("PersonId", PersonId);
			return this;
		}
		
		/**
		 * One or more identifier(s) including local identifiers to identify the person
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param PersonId value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal addPersonId(uk.nhs.interoperability.payloads.commontypes.PersonID PersonId) {
			addMultivalue("PersonId", PersonId);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return CodedValue object
		 */	
		public CodedValue getJobRoleName() {
			return (CodedValue)getValue("JobRoleName");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("JobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @param JobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setJobRoleName(CodedValue JobRoleName) {
			setValue("JobRoleName", JobRoleName);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param JobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setJobRoleName(VocabularyEntry JobRoleName) {
			Code c = new CodedValue(JobRoleName);
			setValue("JobRoleName", c);
			return this;
		}
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getPersonName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("PersonName");
		}
		
		
		
		
		/**
		 * Person name
		 * <br><br>This field is MANDATORY
		 * @param PersonName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setPersonName(uk.nhs.interoperability.payloads.commontypes.PersonName PersonName) {
			setValue("PersonName", PersonName);
			return this;
		}
		
		
		/**
		 * An identifier that uniquely identifies the organisation which employs the person in the role
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgId() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgId");
		}
		
		
		
		
		/**
		 * An identifier that uniquely identifies the organisation which employs the person in the role
		 * @param OrgId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setOrgId(uk.nhs.interoperability.payloads.commontypes.OrgID OrgId) {
			setValue("OrgId", OrgId);
			return this;
		}
		
		
		/**
		 * Name of the organisation associated with the ODS code
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * Name of the organisation associated with the ODS code
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PersonUniversal setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP145205GB01#AssignedEntity",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonId", new Field(
												"PersonId",
												"x:id",
												"One or more identifier(s) including local identifiers to identify the person",
												"true",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("JobRoleName", new Field(
												"JobRoleName",
												"x:code",
												"",
												"false",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:assignedPerson/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:assignedPerson/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateIdRoot", new Field(
												"PersonTemplateIdRoot",
												"x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonTemplateId", new Field(
												"PersonTemplateId",
												"x:assignedPerson/x:templateId/@extension",
												"COCD_TP145205GB01#assignedPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonName", new Field(
												"PersonName",
												"x:assignedPerson/x:name",
												"Person name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:representedOrganization/@classCode",
												"ORG",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:representedOrganization/@determinerCode",
												"INSTANCE",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateIdRoot", new Field(
												"OrgTemplateIdRoot",
												"x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgTemplateId", new Field(
												"OrgTemplateId",
												"x:representedOrganization/x:templateId/@extension",
												"COCD_TP145205GB01#representedOrganization",
												"OrgId",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgId", new Field(
												"OrgId",
												"x:representedOrganization/x:id",
												"An identifier that uniquely identifies the organisation which employs the person in the role",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:representedOrganization/x:name",
												"Name of the organisation associated with the ODS code",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PersonUniversal() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PersonUniversal(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	