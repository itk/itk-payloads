/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.templates;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the TextSection object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String SectionId</li>
 * <li>String Title</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CDADocumentSectionType</li>
 * <li>String Text</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} Author</li>
 * <li>List&lt;{@link Section2 Section2}&gt; Section2</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class TextSection extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "templatesFieldConfig";
		protected static final String name = "TextSection";
		protected static final String shortName = "Section1";
		protected static final String rootNode = "";
		protected static final String version = "COCD_TP146229GB01";
		private static final String packg = "uk.nhs.interoperability.payloads.templates";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param SectionId String
		 * @param Title String
		 * @param Text XML
		 */
		public TextSection(String SectionId, String Title, String Text) {
			fields = new LinkedHashMap<String, Object>();
			
			setSectionId(SectionId);
			setTitle(Title);
			setText(Text);
		}

		/**
		 * A DCE UUID to identify each unique instance of a section
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getSectionId() {
			return (String)getValue("SectionId");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify each unique instance of a section
		 * <br><br>This field is MANDATORY
		 * @param SectionId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setSectionId(String SectionId) {
			setValue("SectionId", SectionId);
			return this;
		}
		
		
		/**
		 * Heading for the section
		 * @return String object
		 */	
		public String getTitle() {
			return (String)getValue("Title");
		}
		
		
		
		
		/**
		 * Heading for the section
		 * @param Title value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setTitle(String Title) {
			setValue("Title", Title);
			return this;
		}
		
		
		/**
		 * 
		 * @return CodedValue object
		 */	
		public CodedValue getCDADocumentSectionType() {
			return (CodedValue)getValue("CDADocumentSectionType");
		}
		
		
		
		
		/**
		 * 
		 * @param CDADocumentSectionType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setCDADocumentSectionType(CodedValue CDADocumentSectionType) {
			setValue("CDADocumentSectionType", CDADocumentSectionType);
			return this;
		}
		
		
		/**
		 * 
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CDADocumentSectionType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setCDADocumentSectionType(VocabularyEntry CDADocumentSectionType) {
			Code c = new CodedValue(CDADocumentSectionType);
			setValue("CDADocumentSectionType", c);
			return this;
		}
		
		/**
		 * Text content of section
		 * @return String object
		 */	
		public String getText() {
			return (String)getValue("Text");
		}
		
		
		
		
		/**
		 * Text content of section
		 * @param Text value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setText(String Text) {
			setValue("Text", Text);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setAuthor(uk.nhs.interoperability.payloads.templates.Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * Subsection (optional)
		 * @return List of Section2 objects
		 */
		public List<Section2> getSection2() {
			return (List<Section2>)getValue("Section2");
		}
		
		/**
		 * Subsection (optional)
		 * @param Section2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection setSection2(List Section2) {
			setValue("Section2", Section2);
			return this;
		}
		
		/**
		 * Subsection (optional)
		 * <br>Note: This adds a Section2 object, but this method can be called
		 * multiple times to add additional Section2 objects.
		 * @param Section2 value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public TextSection addSection2(Section2 Section2) {
			addMultivalue("Section2", Section2);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"DOCSECT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateIdRoot", new Field(
												"TemplateIdRoot",
												"x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplateId", new Field(
												"TemplateId",
												"x:templateId/@extension",
												"COCD_TP146229GB01#Section1",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SectionId", new Field(
												"SectionId",
												"x:id/@root",
												"A DCE UUID to identify each unique instance of a section",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Title", new Field(
												"Title",
												"x:title",
												"Heading for the section",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CDADocumentSectionType", new Field(
												"CDADocumentSectionType",
												"x:code",
												"",
												"false",
												"",
												"",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.330",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Text", new Field(
												"Text",
												"x:text",
												"Text content of section",
												"false",
												"",
												"",
												"XML",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateIdRoot", new Field(
												"AuthorTemplateIdRoot",
												"x:author/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("AuthorTemplateId", new Field(
												"AuthorTemplateId",
												"x:author/x:templateId/@extension",
												"COCD_TP146229GB01#author",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Section2", new Field(
												"Section2",
												"x:component",
												"Subsection (optional)",
												"false",
												"",
												"",
												"Section2",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public TextSection() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public TextSection(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	