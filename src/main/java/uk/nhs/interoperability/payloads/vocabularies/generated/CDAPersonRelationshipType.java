/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the CDAPersonRelationshipType vocabulary:
 * <ul>
 *   <li>_01 : Spouse</li>
 *   <li>_02 : Partner</li>
 *   <li>_03 : Parent</li>
 *   <li>_04 : Next of kin</li>
 *   <li>_05 : Guardian</li>
 *   <li>_06 : Foster parent</li>
 *   <li>_07 : Step parent</li>
 *   <li>_08 : Polygamous partner</li>
 *   <li>_09 : Child</li>
 *   <li>_10 : Dependant</li>
 *   <li>_11 : Non-dependant</li>
 *   <li>_12 : Mother</li>
 *   <li>_13 : Father</li>
 *   <li>_14 : Sister</li>
 *   <li>_15 : Brother</li>
 *   <li>_16 : Relative</li>
 *   <li>_17 : Proxy - Contact</li>
 *   <li>_18 : Proxy - Communication</li>
 *   <li>_19 : Proxy -- Contact and Communication</li>
 *   <li>_20 : Carer</li>
 *   <li>_21 : Self</li>
 *   <li>_98 : Not Known</li>
 *   <li>_99 : Not specified</li>
 *   <li>_22 : Wife</li>
 *   <li>_23 : Husband</li>
 *   <li>_24 : Maternal Grand-Father</li>
 *   <li>_25 : Maternal Grand-Mother</li>
 *   <li>_26 : Paternal Grand-Father</li>
 *   <li>_27 : Paternal Grand-Mother</li>
 *   <li>_28 : Grand-Son</li>
 *   <li>_29 : Grand-Daughter</li>
 *   <li>_30 : Aunt</li>
 *   <li>_31 : Uncle</li>
 *   <li>_32 : Niece</li>
 *   <li>_33 : Nephew</li>
 *   <li>_34 : Step-Mother</li>
 *   <li>_35 : Step-Father</li>
 *   <li>_36 : Step-Son</li>
 *   <li>_37 : Step-Daughter</li>
 *   <li>_38 : Civil Partner</li>
 *   <li>_39 : Ex-Wife</li>
 *   <li>_40 : Ex-Husband</li>
 *   <li>_41 : Ex-Civil Partner</li>
 *   <li>_42 : Son</li>
 *   <li>_43 : Daughter </li>
 *   <li>_44 : Grandparent</li>
 *   <li>_45 : Grandchild</li>
 *   <li>_46 : Friend</li>
 *   <li>_47 : Neighbour</li>
 *   <li>_48 : Work Colleague</li>
 *   <li>_49 : Person with Parental Responsibility</li>
 *   <li>_50 : Informal Partner</li>
 *   <li>_51 : Non-relative lived with for at least 5 years</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum CDAPersonRelationshipType implements VocabularyEntry {
	
	
	/**
	 * Spouse
	 */
	_01 ("01", "Spouse"),
	
	/**
	 * Partner
	 */
	_02 ("02", "Partner"),
	
	/**
	 * Parent
	 */
	_03 ("03", "Parent"),
	
	/**
	 * Next of kin
	 */
	_04 ("04", "Next of kin"),
	
	/**
	 * Guardian
	 */
	_05 ("05", "Guardian"),
	
	/**
	 * Foster parent
	 */
	_06 ("06", "Foster parent"),
	
	/**
	 * Step parent
	 */
	_07 ("07", "Step parent"),
	
	/**
	 * Polygamous partner
	 */
	_08 ("08", "Polygamous partner"),
	
	/**
	 * Child
	 */
	_09 ("09", "Child"),
	
	/**
	 * Dependant
	 */
	_10 ("10", "Dependant"),
	
	/**
	 * Non-dependant
	 */
	_11 ("11", "Non-dependant"),
	
	/**
	 * Mother
	 */
	_12 ("12", "Mother"),
	
	/**
	 * Father
	 */
	_13 ("13", "Father"),
	
	/**
	 * Sister
	 */
	_14 ("14", "Sister"),
	
	/**
	 * Brother
	 */
	_15 ("15", "Brother"),
	
	/**
	 * Relative
	 */
	_16 ("16", "Relative"),
	
	/**
	 * Proxy - Contact
	 */
	_17 ("17", "Proxy - Contact"),
	
	/**
	 * Proxy - Communication
	 */
	_18 ("18", "Proxy - Communication"),
	
	/**
	 * Proxy -- Contact and Communication
	 */
	_19 ("19", "Proxy -- Contact and Communication"),
	
	/**
	 * Carer
	 */
	_20 ("20", "Carer"),
	
	/**
	 * Self
	 */
	_21 ("21", "Self"),
	
	/**
	 * Not Known
	 */
	_98 ("98", "Not Known"),
	
	/**
	 * Not specified
	 */
	_99 ("99", "Not specified"),
	
	/**
	 * Wife
	 */
	_22 ("22", "Wife"),
	
	/**
	 * Husband
	 */
	_23 ("23", "Husband"),
	
	/**
	 * Maternal Grand-Father
	 */
	_24 ("24", "Maternal Grand-Father"),
	
	/**
	 * Maternal Grand-Mother
	 */
	_25 ("25", "Maternal Grand-Mother"),
	
	/**
	 * Paternal Grand-Father
	 */
	_26 ("26", "Paternal Grand-Father"),
	
	/**
	 * Paternal Grand-Mother
	 */
	_27 ("27", "Paternal Grand-Mother"),
	
	/**
	 * Grand-Son
	 */
	_28 ("28", "Grand-Son"),
	
	/**
	 * Grand-Daughter
	 */
	_29 ("29", "Grand-Daughter"),
	
	/**
	 * Aunt
	 */
	_30 ("30", "Aunt"),
	
	/**
	 * Uncle
	 */
	_31 ("31", "Uncle"),
	
	/**
	 * Niece
	 */
	_32 ("32", "Niece"),
	
	/**
	 * Nephew
	 */
	_33 ("33", "Nephew"),
	
	/**
	 * Step-Mother
	 */
	_34 ("34", "Step-Mother"),
	
	/**
	 * Step-Father
	 */
	_35 ("35", "Step-Father"),
	
	/**
	 * Step-Son
	 */
	_36 ("36", "Step-Son"),
	
	/**
	 * Step-Daughter
	 */
	_37 ("37", "Step-Daughter"),
	
	/**
	 * Civil Partner
	 */
	_38 ("38", "Civil Partner"),
	
	/**
	 * Ex-Wife
	 */
	_39 ("39", "Ex-Wife"),
	
	/**
	 * Ex-Husband
	 */
	_40 ("40", "Ex-Husband"),
	
	/**
	 * Ex-Civil Partner
	 */
	_41 ("41", "Ex-Civil Partner"),
	
	/**
	 * Son
	 */
	_42 ("42", "Son"),
	
	/**
	 * Daughter 
	 */
	_43 ("43", "Daughter"),
	
	/**
	 * Grandparent
	 */
	_44 ("44", "Grandparent"),
	
	/**
	 * Grandchild
	 */
	_45 ("45", "Grandchild"),
	
	/**
	 * Friend
	 */
	_46 ("46", "Friend"),
	
	/**
	 * Neighbour
	 */
	_47 ("47", "Neighbour"),
	
	/**
	 * Work Colleague
	 */
	_48 ("48", "Work Colleague"),
	
	/**
	 * Person with Parental Responsibility
	 */
	_49 ("49", "Person with Parental Responsibility"),
	
	/**
	 * Informal Partner
	 */
	_50 ("50", "Informal Partner"),
	
	/**
	 * Non-relative lived with for at least 5 years
	 */
	_51 ("51", "Non-relative lived with for at least 5 years"),
	
	/**
	 * Spouse
	 */
	_Spouse ("01", "Spouse"),
	
	/**
	 * Partner
	 */
	_Partner ("02", "Partner"),
	
	/**
	 * Parent
	 */
	_Parent ("03", "Parent"),
	
	/**
	 * Next of kin
	 */
	_Nextofkin ("04", "Next of kin"),
	
	/**
	 * Guardian
	 */
	_Guardian ("05", "Guardian"),
	
	/**
	 * Foster parent
	 */
	_Fosterparent ("06", "Foster parent"),
	
	/**
	 * Step parent
	 */
	_Stepparent ("07", "Step parent"),
	
	/**
	 * Polygamous partner
	 */
	_Polygamouspartner ("08", "Polygamous partner"),
	
	/**
	 * Child
	 */
	_Child ("09", "Child"),
	
	/**
	 * Dependant
	 */
	_Dependant ("10", "Dependant"),
	
	/**
	 * Non-dependant
	 */
	_Nondependant ("11", "Non-dependant"),
	
	/**
	 * Mother
	 */
	_Mother ("12", "Mother"),
	
	/**
	 * Father
	 */
	_Father ("13", "Father"),
	
	/**
	 * Sister
	 */
	_Sister ("14", "Sister"),
	
	/**
	 * Brother
	 */
	_Brother ("15", "Brother"),
	
	/**
	 * Relative
	 */
	_Relative ("16", "Relative"),
	
	/**
	 * Proxy - Contact
	 */
	_ProxyContact ("17", "Proxy - Contact"),
	
	/**
	 * Proxy - Communication
	 */
	_ProxyCommunication ("18", "Proxy - Communication"),
	
	/**
	 * Proxy -- Contact and Communication
	 */
	_ProxyContactandCommunication ("19", "Proxy -- Contact and Communication"),
	
	/**
	 * Carer
	 */
	_Carer ("20", "Carer"),
	
	/**
	 * Self
	 */
	_Self ("21", "Self"),
	
	/**
	 * Not Known
	 */
	_NotKnown ("98", "Not Known"),
	
	/**
	 * Not specified
	 */
	_Notspecified ("99", "Not specified"),
	
	/**
	 * Wife
	 */
	_Wife ("22", "Wife"),
	
	/**
	 * Husband
	 */
	_Husband ("23", "Husband"),
	
	/**
	 * Maternal Grand-Father
	 */
	_MaternalGrandFather ("24", "Maternal Grand-Father"),
	
	/**
	 * Maternal Grand-Mother
	 */
	_MaternalGrandMother ("25", "Maternal Grand-Mother"),
	
	/**
	 * Paternal Grand-Father
	 */
	_PaternalGrandFather ("26", "Paternal Grand-Father"),
	
	/**
	 * Paternal Grand-Mother
	 */
	_PaternalGrandMother ("27", "Paternal Grand-Mother"),
	
	/**
	 * Grand-Son
	 */
	_GrandSon ("28", "Grand-Son"),
	
	/**
	 * Grand-Daughter
	 */
	_GrandDaughter ("29", "Grand-Daughter"),
	
	/**
	 * Aunt
	 */
	_Aunt ("30", "Aunt"),
	
	/**
	 * Uncle
	 */
	_Uncle ("31", "Uncle"),
	
	/**
	 * Niece
	 */
	_Niece ("32", "Niece"),
	
	/**
	 * Nephew
	 */
	_Nephew ("33", "Nephew"),
	
	/**
	 * Step-Mother
	 */
	_StepMother ("34", "Step-Mother"),
	
	/**
	 * Step-Father
	 */
	_StepFather ("35", "Step-Father"),
	
	/**
	 * Step-Son
	 */
	_StepSon ("36", "Step-Son"),
	
	/**
	 * Step-Daughter
	 */
	_StepDaughter ("37", "Step-Daughter"),
	
	/**
	 * Civil Partner
	 */
	_CivilPartner ("38", "Civil Partner"),
	
	/**
	 * Ex-Wife
	 */
	_ExWife ("39", "Ex-Wife"),
	
	/**
	 * Ex-Husband
	 */
	_ExHusband ("40", "Ex-Husband"),
	
	/**
	 * Ex-Civil Partner
	 */
	_ExCivilPartner ("41", "Ex-Civil Partner"),
	
	/**
	 * Son
	 */
	_Son ("42", "Son"),
	
	/**
	 * Daughter 
	 */
	_Daughter ("43", "Daughter"),
	
	/**
	 * Grandparent
	 */
	_Grandparent ("44", "Grandparent"),
	
	/**
	 * Grandchild
	 */
	_Grandchild ("45", "Grandchild"),
	
	/**
	 * Friend
	 */
	_Friend ("46", "Friend"),
	
	/**
	 * Neighbour
	 */
	_Neighbour ("47", "Neighbour"),
	
	/**
	 * Work Colleague
	 */
	_WorkColleague ("48", "Work Colleague"),
	
	/**
	 * Person with Parental Responsibility
	 */
	_PersonwithParentalResponsibility ("49", "Person with Parental Responsibility"),
	
	/**
	 * Informal Partner
	 */
	_InformalPartner ("50", "Informal Partner"),
	
	/**
	 * Non-relative lived with for at least 5 years
	 */
	_Nonrelativelivedwithforatleast5years ("51", "Non-relative lived with for at least 5 years"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.16.45";

	CDAPersonRelationshipType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static CDAPersonRelationshipType getByCode(String code) {
		CDAPersonRelationshipType[] vals = values();
		for (CDAPersonRelationshipType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(CDAPersonRelationshipType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	