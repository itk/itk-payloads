/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the RoleCode vocabulary:
 * <ul>
 *   <li>_ACC : accident site</li>
 *   <li>_ALL : Allergy clinic</li>
 *   <li>_AMB : Ambulance</li>
 *   <li>_AMPUT : Amputee clinic</li>
 *   <li>_AUNT : aunt</li>
 *   <li>_B : Blind</li>
 *   <li>_BF : Beef</li>
 *   <li>_BILL : Billing Contact</li>
 *   <li>_BL : Broiler</li>
 *   <li>_BMTC : Bone marrow transplant clinic</li>
 *   <li>_BMTU : Bone marrow transplant unit</li>
 *   <li>_BR : Breeder</li>
 *   <li>_BREAST : Breast clinic</li>
 *   <li>_BRO : Brother</li>
 *   <li>_BROINLAW : brother-in-law</li>
 *   <li>_C : Calibrator</li>
 *   <li>_CANC : Child and adolescent neurology clinic</li>
 *   <li>_CAPC : Child and adolescent psychiatry clinic</li>
 *   <li>_CARD : Ambulatory Health Care Facilities; ClinicCenter; Rehabilitation Cardiac Facilities</li>
 *   <li>_CATH : Cardiac catheterization lab</li>
 *   <li>_CCU : Coronary care unit</li>
 *   <li>_CHEST : Chest unit</li>
 *   <li>_CHILD : Child</li>
 *   <li>_CHLDADOPT : adopted child</li>
 *   <li>_CHLDFOST : foster child</li>
 *   <li>_CHLDINLAW : child-in-law</li>
 *   <li>_CHR : Chronic Care Facility</li>
 *   <li>_CO : Companion</li>
 *   <li>_COAG : Coagulation clinic</li>
 *   <li>_COMM : Community Location</li>
 *   <li>_COUSN : cousin</li>
 *   <li>_CRS : Colon and rectal surgery clinic</li>
 *   <li>_CVDX : Cardiovascular diagnostics or therapeutics unit</li>
 *   <li>_DA : Dairy</li>
 *   <li>_DADDR : Delivery Address</li>
 *   <li>_DAU : natural daughter</li>
 *   <li>_DAUADOPT : adopted daughter</li>
 *   <li>_DAUFOST : foster daughter</li>
 *   <li>_DAUINLAW : daughter-in-law</li>
 *   <li>_DERM : Dermatology clinic</li>
 *   <li>_DOMPART : domestic partner</li>
 *   <li>_DR : Draft</li>
 *   <li>_DU : Dual</li>
 *   <li>_DX : Diagnostics or therapeutics unit</li>
 *   <li>_E : Electronic QC</li>
 *   <li>_ECHO : Echocardiography lab</li>
 *   <li>_ECON : emergency contact</li>
 *   <li>_ENDO : Endocrinology clinic</li>
 *   <li>_ENDOS : Ambulatory Health Care Facilities; ClinicCenter; Endoscopy</li>
 *   <li>_ENT : Otorhinolaryngology clinic</li>
 *   <li>_EPIL : Epilepsy unit</li>
 *   <li>_ER : Emergency room</li>
 *   <li>_ETU : Emergency trauma unit</li>
 *   <li>_F : Filler Proficiency</li>
 *   <li>_FAMDEP : family dependent</li>
 *   <li>_FAMMEMB : Family Member</li>
 *   <li>_FI : Fiber</li>
 *   <li>_FM : Family Member for Insurance Purposes</li>
 *   <li>_FMC : Family medicine clinic</li>
 *   <li>_FRND : unrelated friend</li>
 *   <li>_FSTUD : full-time student</li>
 *   <li>_FTH : Father</li>
 *   <li>_FTHINLAW : father-in-law</li>
 *   <li>_GACH : General acute care hospital</li>
 *   <li>_G : Group</li>
 *   <li>_GGRFTH : great grandfather</li>
 *   <li>_GGRMTH : great grandmother</li>
 *   <li>_GGRPRN : great grandparent</li>
 *   <li>_GI : Gastroenterology clinic</li>
 *   <li>_GIDX : Gastroenterology diagnostics or therapeutics lab</li>
 *   <li>_GIM : General internal medicine clinic</li>
 *   <li>_GRFTH : Grandfather</li>
 *   <li>_GRMTH : Grandmother</li>
 *   <li>_GRNDCHILD : grandchild</li>
 *   <li>_GRNDDAU : granddaughter</li>
 *   <li>_GRNDSON : grandson</li>
 *   <li>_GRPRN : Grandparent</li>
 *   <li>_GT : Guarantor</li>
 *   <li>_GYN : Gynecology clinic</li>
 *   <li>_HAND : Hand clinic</li>
 *   <li>_HANDIC : handicapped dependent</li>
 *   <li>_HBRO : half-brother</li>
 *   <li>_HD : Hemodialysis unit</li>
 *   <li>_HEM : Hematology clinic</li>
 *   <li>_HOSP : Hospital</li>
 *   <li>_HSIB : half-sibling</li>
 *   <li>_HTN : Hypertension clinic</li>
 *   <li>_HSIS : half-sister</li>
 *   <li>_HU : Hospital unit</li>
 *   <li>_HUSB : husband</li>
 *   <li>_ICU : Intensive care unit</li>
 *   <li>_IEC : Impairment evaluation center</li>
 *   <li>_INFD : Infectious disease clinic</li>
 *   <li>_INJ : injured plaintiff</li>
 *   <li>_INV : Infertility clinic</li>
 *   <li>_L : Pool</li>
 *   <li>_LY : Layer</li>
 *   <li>_LYMPH : Lympedema clinic</li>
 *   <li>_MGEN : Medical genetics clinic</li>
 *   <li>_MHSP : Military Hospital</li>
 *   <li>_MOBL : Mobile Unit</li>
 *   <li>_MT : Meat</li>
 *   <li>_MTH : Mother</li>
 *   <li>_MTHINLOAW : mother-in-law</li>
 *   <li>_MU : Multiplier</li>
 *   <li>_NBOR : neighbour</li>
 *   <li>_NBRO : natural brother</li>
 *   <li>_NCCF : Nursing or custodial care facility</li>
 *   <li>_NCCS : Neurology critical care and stroke unit</li>
 *   <li>_NCHILD : natural child</li>
 *   <li>_NEPH : Nephrology clinic</li>
 *   <li>_NEPHEW : nephew</li>
 *   <li>_NEUR : Neurology clinic</li>
 *   <li>_NFTH : natural father</li>
 *   <li>_NIECE : niece</li>
 *   <li>_NIENEPH : niecenephew</li>
 *   <li>_NMTH : natural mother</li>
 *   <li>_NOK : next of kin</li>
 *   <li>_NPRN : natural parent</li>
 *   <li>_NS : Neurosurgery unit</li>
 *   <li>_NSIB : natural sibling</li>
 *   <li>_NSIS : natural sister</li>
 *   <li>_O : Operator Proficiency</li>
 *   <li>_OB : Obstetrics clinic</li>
 *   <li>_OF : Outpatient facility</li>
 *   <li>_OMS : Ambulatory Health Care Facilities; ClinicCenter; Surgery, OralMaxillofacial</li>
 *   <li>_ONCL : Medical oncology clinic</li>
 *   <li>_OPH : Opthalmology clinic</li>
 *   <li>_ORG : organizational contact</li>
 *   <li>_ORTHO : Orthopedics clinic</li>
 *   <li>_P : Patient Specimen</li>
 *   <li>_PAINCL : Ambulatory Health Care Facilities; ClinicCenter; Pain</li>
 *   <li>_PAYOR : Payor Contact</li>
 *   <li>_PC : Ambulatory Health Care Facilities; ClinicCenter; Primary Care</li>
 *   <li>_PEDC : Pediatrics clinic</li>
 *   <li>_PEDCARD : Pediatric cardiology clinic</li>
 *   <li>_PEDE : Pediatric endocrinology clinic</li>
 *   <li>_PEDGI : Pediatric gastroenterology clinic</li>
 *   <li>_PEDHEM : Pediatric hematology clinic</li>
 *   <li>_PEDHO : Pediatric oncology clinic</li>
 *   <li>_PEDICU : Pediatric intensive care unit</li>
 *   <li>_PEDID : Pediatric infectious disease clinic</li>
 *   <li>_PEDNEPH : Pediatric nephrology clinic</li>
 *   <li>_PEDNICU : Pediatric neonatal intensive care unit</li>
 *   <li>_PEDRHEUM : Pediatric rheumatology clinic</li>
 *   <li>_PEDU : Pediatric unit</li>
 *   <li>_PH : Policy Holder</li>
 *   <li>_PHARM : Pharmacy</li>
 *   <li>_PHU : Hospital Units; Psychiatric Unit</li>
 *   <li>_PL : Pleasure</li>
 *   <li>_PLS : Plastic surgery clinic</li>
 *   <li>_POD : Ambulatory Health Care Facilities; ClinicCenter; Podiatric</li>
 *   <li>_PRC : Pain rehabilitation center</li>
 *   <li>_PREV : Preventive medicine clinic</li>
 *   <li>_PRN : Parent</li>
 *   <li>_PRNINLAW : parent in-law</li>
 *   <li>_PROCTO : Proctology clinic</li>
 *   <li>_PROFF : Providers Office</li>
 *   <li>_PROS : Prosthodontics clinic</li>
 *   <li>_PSI : Psychology clinic</li>
 *   <li>_PSTUD : part-time student</li>
 *   <li>_PSY : Psychiatry clinic</li>
 *   <li>_PSYCHF : Psychatric Care Facility</li>
 *   <li>_PT : Patient</li>
 *   <li>_PTRES : Patients Residence</li>
 *   <li>_Q : Quality Control</li>
 *   <li>_R : Replicate</li>
 *   <li>_RADDX : Ambulatory Health Care Facilities; ClinicCenter; Radiology</li>
 *   <li>_RC : Racing</li>
 *   <li>_RH : Hospitals; Rehabilitation Hospital</li>
 *   <li>_RHEUM : Rheumatology clinic</li>
 *   <li>_RHU : Hospital Units; Rehabilitation Unit</li>
 *   <li>_ROOM : Roommate</li>
 *   <li>_RTF : Residential treatment facility</li>
 *   <li>_SCHOOL : school</li>
 *   <li>_SELF : self</li>
 *   <li>_SH : Show</li>
 *   <li>_SIB : Sibling</li>
 *   <li>_SIBINLAW : sibling in-law</li>
 *   <li>_SIGOTHR : significant other</li>
 *   <li>_SIS : Sister</li>
 *   <li>_SISLINLAW : sister-in-law</li>
 *   <li>_SLEEP : Sleep disorders unit</li>
 *   <li>_SNF : Nursing  Custodial Care Facilities; Skilled Nursing Facility</li>
 *   <li>_SON : natural son</li>
 *   <li>_SONADOPT : adopted son</li>
 *   <li>_SONFOST : foster son</li>
 *   <li>_SONINLAW : son-in-law</li>
 *   <li>_SPMED : Sports medicine clinic</li>
 *   <li>_SPON : sponsored dependent</li>
 *   <li>_SPS : spouse</li>
 *   <li>_STPBRO : stepbrother</li>
 *   <li>_STPCHLD : step child</li>
 *   <li>_STPDAU : stepdaughter</li>
 *   <li>_STPFTH : stepfather</li>
 *   <li>_STPMTH : stepmother</li>
 *   <li>_STPPRN : stepparent</li>
 *   <li>_STPSIB : step sibling</li>
 *   <li>_STPSIS : stepsister</li>
 *   <li>_STPSON : stepson</li>
 *   <li>_STUD : student</li>
 *   <li>_SU : Surgery clinic</li>
 *   <li>_SURF : Residential Treatment Facilities; Substance Use Rehabilitation Facility</li>
 *   <li>_TR : Transplant clinic</li>
 *   <li>_TRAVEL : Travel and geographic medicine clinic</li>
 *   <li>_TRB : Tribal Member</li>
 *   <li>_UNCLE : uncle</li>
 *   <li>_URO : Urology clinic</li>
 *   <li>_V : Verifying</li>
 *   <li>_VL : Veal</li>
 *   <li>_WIFE : wife</li>
 *   <li>_WL : Wool</li>
 *   <li>_WND : Wound clinic</li>
 *   <li>_WO : Working</li>
 *   <li>_WORK : work site</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum RoleCode implements VocabularyEntry {
	
	
	/**
	 * accident site
	 */
	_ACC ("ACC", "accident site"),
	
	/**
	 * Allergy clinic
	 */
	_ALL ("ALL", "Allergy clinic"),
	
	/**
	 * Ambulance
	 */
	_AMB ("AMB", "Ambulance"),
	
	/**
	 * Amputee clinic
	 */
	_AMPUT ("AMPUT", "Amputee clinic"),
	
	/**
	 * aunt
	 */
	_AUNT ("AUNT", "aunt"),
	
	/**
	 * Blind
	 */
	_B ("B", "Blind"),
	
	/**
	 * Beef
	 */
	_BF ("BF", "Beef"),
	
	/**
	 * Billing Contact
	 */
	_BILL ("BILL", "Billing Contact"),
	
	/**
	 * Broiler
	 */
	_BL ("BL", "Broiler"),
	
	/**
	 * Bone marrow transplant clinic
	 */
	_BMTC ("BMTC", "Bone marrow transplant clinic"),
	
	/**
	 * Bone marrow transplant unit
	 */
	_BMTU ("BMTU", "Bone marrow transplant unit"),
	
	/**
	 * Breeder
	 */
	_BR ("BR", "Breeder"),
	
	/**
	 * Breast clinic
	 */
	_BREAST ("BREAST", "Breast clinic"),
	
	/**
	 * Brother
	 */
	_BRO ("BRO", "Brother"),
	
	/**
	 * brother-in-law
	 */
	_BROINLAW ("BROINLAW", "brother-in-law"),
	
	/**
	 * Calibrator
	 */
	_C ("C", "Calibrator"),
	
	/**
	 * Child and adolescent neurology clinic
	 */
	_CANC ("CANC", "Child and adolescent neurology clinic"),
	
	/**
	 * Child and adolescent psychiatry clinic
	 */
	_CAPC ("CAPC", "Child and adolescent psychiatry clinic"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Rehabilitation Cardiac Facilities
	 */
	_CARD ("CARD", "Ambulatory Health Care Facilities; Clinic/Center; Rehabilitation: Cardiac Facilities"),
	
	/**
	 * Cardiac catheterization lab
	 */
	_CATH ("CATH", "Cardiac catheterization lab"),
	
	/**
	 * Coronary care unit
	 */
	_CCU ("CCU", "Coronary care unit"),
	
	/**
	 * Chest unit
	 */
	_CHEST ("CHEST", "Chest unit"),
	
	/**
	 * Child
	 */
	_CHILD ("CHILD", "Child"),
	
	/**
	 * adopted child
	 */
	_CHLDADOPT ("CHLDADOPT", "adopted child"),
	
	/**
	 * foster child
	 */
	_CHLDFOST ("CHLDFOST", "foster child"),
	
	/**
	 * child-in-law
	 */
	_CHLDINLAW ("CHLDINLAW", "child-in-law"),
	
	/**
	 * Chronic Care Facility
	 */
	_CHR ("CHR", "Chronic Care Facility"),
	
	/**
	 * Companion
	 */
	_CO ("CO", "Companion"),
	
	/**
	 * Coagulation clinic
	 */
	_COAG ("COAG", "Coagulation clinic"),
	
	/**
	 * Community Location
	 */
	_COMM ("COMM", "Community Location"),
	
	/**
	 * cousin
	 */
	_COUSN ("COUSN", "cousin"),
	
	/**
	 * Colon and rectal surgery clinic
	 */
	_CRS ("CRS", "Colon and rectal surgery clinic"),
	
	/**
	 * Cardiovascular diagnostics or therapeutics unit
	 */
	_CVDX ("CVDX", "Cardiovascular diagnostics or therapeutics unit"),
	
	/**
	 * Dairy
	 */
	_DA ("DA", "Dairy"),
	
	/**
	 * Delivery Address
	 */
	_DADDR ("DADDR", "Delivery Address"),
	
	/**
	 * natural daughter
	 */
	_DAU ("DAU", "natural daughter"),
	
	/**
	 * adopted daughter
	 */
	_DAUADOPT ("DAUADOPT", "adopted daughter"),
	
	/**
	 * foster daughter
	 */
	_DAUFOST ("DAUFOST", "foster daughter"),
	
	/**
	 * daughter-in-law
	 */
	_DAUINLAW ("DAUINLAW", "daughter-in-law"),
	
	/**
	 * Dermatology clinic
	 */
	_DERM ("DERM", "Dermatology clinic"),
	
	/**
	 * domestic partner
	 */
	_DOMPART ("DOMPART", "domestic partner"),
	
	/**
	 * Draft
	 */
	_DR ("DR", "Draft"),
	
	/**
	 * Dual
	 */
	_DU ("DU", "Dual"),
	
	/**
	 * Diagnostics or therapeutics unit
	 */
	_DX ("DX", "Diagnostics or therapeutics unit"),
	
	/**
	 * Electronic QC
	 */
	_E ("E", "Electronic QC"),
	
	/**
	 * Echocardiography lab
	 */
	_ECHO ("ECHO", "Echocardiography lab"),
	
	/**
	 * emergency contact
	 */
	_ECON ("ECON", "emergency contact"),
	
	/**
	 * Endocrinology clinic
	 */
	_ENDO ("ENDO", "Endocrinology clinic"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Endoscopy
	 */
	_ENDOS ("ENDOS", "Ambulatory Health Care Facilities; Clinic/Center; Endoscopy"),
	
	/**
	 * Otorhinolaryngology clinic
	 */
	_ENT ("ENT", "Otorhinolaryngology clinic"),
	
	/**
	 * Epilepsy unit
	 */
	_EPIL ("EPIL", "Epilepsy unit"),
	
	/**
	 * Emergency room
	 */
	_ER ("ER", "Emergency room"),
	
	/**
	 * Emergency trauma unit
	 */
	_ETU ("ETU", "Emergency trauma unit"),
	
	/**
	 * Filler Proficiency
	 */
	_F ("F", "Filler Proficiency"),
	
	/**
	 * family dependent
	 */
	_FAMDEP ("FAMDEP", "family dependent"),
	
	/**
	 * Family Member
	 */
	_FAMMEMB ("FAMMEMB", "Family Member"),
	
	/**
	 * Fiber
	 */
	_FI ("FI", "Fiber"),
	
	/**
	 * Family Member for Insurance Purposes
	 */
	_FM ("FM", "Family Member for Insurance Purposes"),
	
	/**
	 * Family medicine clinic
	 */
	_FMC ("FMC", "Family medicine clinic"),
	
	/**
	 * unrelated friend
	 */
	_FRND ("FRND", "unrelated friend"),
	
	/**
	 * full-time student
	 */
	_FSTUD ("FSTUD", "full-time student"),
	
	/**
	 * Father
	 */
	_FTH ("FTH", "Father"),
	
	/**
	 * father-in-law
	 */
	_FTHINLAW ("FTHINLAW", "father-in-law"),
	
	/**
	 * General acute care hospital
	 */
	_GACH ("GACH", "General acute care hospital"),
	
	/**
	 * Group
	 */
	_G ("G", "Group"),
	
	/**
	 * great grandfather
	 */
	_GGRFTH ("GGRFTH", "great grandfather"),
	
	/**
	 * great grandmother
	 */
	_GGRMTH ("GGRMTH", "great grandmother"),
	
	/**
	 * great grandparent
	 */
	_GGRPRN ("GGRPRN", "great grandparent"),
	
	/**
	 * Gastroenterology clinic
	 */
	_GI ("GI", "Gastroenterology clinic"),
	
	/**
	 * Gastroenterology diagnostics or therapeutics lab
	 */
	_GIDX ("GIDX", "Gastroenterology diagnostics or therapeutics lab"),
	
	/**
	 * General internal medicine clinic
	 */
	_GIM ("GIM", "General internal medicine clinic"),
	
	/**
	 * Grandfather
	 */
	_GRFTH ("GRFTH", "Grandfather"),
	
	/**
	 * Grandmother
	 */
	_GRMTH ("GRMTH", "Grandmother"),
	
	/**
	 * grandchild
	 */
	_GRNDCHILD ("GRNDCHILD", "grandchild"),
	
	/**
	 * granddaughter
	 */
	_GRNDDAU ("GRNDDAU", "granddaughter"),
	
	/**
	 * grandson
	 */
	_GRNDSON ("GRNDSON", "grandson"),
	
	/**
	 * Grandparent
	 */
	_GRPRN ("GRPRN", "Grandparent"),
	
	/**
	 * Guarantor
	 */
	_GT ("GT", "Guarantor"),
	
	/**
	 * Gynecology clinic
	 */
	_GYN ("GYN", "Gynecology clinic"),
	
	/**
	 * Hand clinic
	 */
	_HAND ("HAND", "Hand clinic"),
	
	/**
	 * handicapped dependent
	 */
	_HANDIC ("HANDIC", "handicapped dependent"),
	
	/**
	 * half-brother
	 */
	_HBRO ("HBRO", "half-brother"),
	
	/**
	 * Hemodialysis unit
	 */
	_HD ("HD", "Hemodialysis unit"),
	
	/**
	 * Hematology clinic
	 */
	_HEM ("HEM", "Hematology clinic"),
	
	/**
	 * Hospital
	 */
	_HOSP ("HOSP", "Hospital"),
	
	/**
	 * half-sibling
	 */
	_HSIB ("HSIB", "half-sibling"),
	
	/**
	 * Hypertension clinic
	 */
	_HTN ("HTN", "Hypertension clinic"),
	
	/**
	 * half-sister
	 */
	_HSIS ("HSIS", "half-sister"),
	
	/**
	 * Hospital unit
	 */
	_HU ("HU", "Hospital unit"),
	
	/**
	 * husband
	 */
	_HUSB ("HUSB", "husband"),
	
	/**
	 * Intensive care unit
	 */
	_ICU ("ICU", "Intensive care unit"),
	
	/**
	 * Impairment evaluation center
	 */
	_IEC ("IEC", "Impairment evaluation center"),
	
	/**
	 * Infectious disease clinic
	 */
	_INFD ("INFD", "Infectious disease clinic"),
	
	/**
	 * injured plaintiff
	 */
	_INJ ("INJ", "injured plaintiff"),
	
	/**
	 * Infertility clinic
	 */
	_INV ("INV", "Infertility clinic"),
	
	/**
	 * Pool
	 */
	_L ("L", "Pool"),
	
	/**
	 * Layer
	 */
	_LY ("LY", "Layer"),
	
	/**
	 * Lympedema clinic
	 */
	_LYMPH ("LYMPH", "Lympedema clinic"),
	
	/**
	 * Medical genetics clinic
	 */
	_MGEN ("MGEN", "Medical genetics clinic"),
	
	/**
	 * Military Hospital
	 */
	_MHSP ("MHSP", "Military Hospital"),
	
	/**
	 * Mobile Unit
	 */
	_MOBL ("MOBL", "Mobile Unit"),
	
	/**
	 * Meat
	 */
	_MT ("MT", "Meat"),
	
	/**
	 * Mother
	 */
	_MTH ("MTH", "Mother"),
	
	/**
	 * mother-in-law
	 */
	_MTHINLOAW ("MTHINLOAW", "mother-in-law"),
	
	/**
	 * Multiplier
	 */
	_MU ("MU", "Multiplier"),
	
	/**
	 * neighbour
	 */
	_NBOR ("NBOR", "neighbour"),
	
	/**
	 * natural brother
	 */
	_NBRO ("NBRO", "natural brother"),
	
	/**
	 * Nursing or custodial care facility
	 */
	_NCCF ("NCCF", "Nursing or custodial care facility"),
	
	/**
	 * Neurology critical care and stroke unit
	 */
	_NCCS ("NCCS", "Neurology critical care and stroke unit"),
	
	/**
	 * natural child
	 */
	_NCHILD ("NCHILD", "natural child"),
	
	/**
	 * Nephrology clinic
	 */
	_NEPH ("NEPH", "Nephrology clinic"),
	
	/**
	 * nephew
	 */
	_NEPHEW ("NEPHEW", "nephew"),
	
	/**
	 * Neurology clinic
	 */
	_NEUR ("NEUR", "Neurology clinic"),
	
	/**
	 * natural father
	 */
	_NFTH ("NFTH", "natural father"),
	
	/**
	 * niece
	 */
	_NIECE ("NIECE", "niece"),
	
	/**
	 * niecenephew
	 */
	_NIENEPH ("NIENEPH", "niece/nephew"),
	
	/**
	 * natural mother
	 */
	_NMTH ("NMTH", "natural mother"),
	
	/**
	 * next of kin
	 */
	_NOK ("NOK", "next of kin"),
	
	/**
	 * natural parent
	 */
	_NPRN ("NPRN", "natural parent"),
	
	/**
	 * Neurosurgery unit
	 */
	_NS ("NS", "Neurosurgery unit"),
	
	/**
	 * natural sibling
	 */
	_NSIB ("NSIB", "natural sibling"),
	
	/**
	 * natural sister
	 */
	_NSIS ("NSIS", "natural sister"),
	
	/**
	 * Operator Proficiency
	 */
	_O ("O", "Operator Proficiency"),
	
	/**
	 * Obstetrics clinic
	 */
	_OB ("OB", "Obstetrics clinic"),
	
	/**
	 * Outpatient facility
	 */
	_OF ("OF", "Outpatient facility"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Surgery, OralMaxillofacial
	 */
	_OMS ("OMS", "Ambulatory Health Care Facilities; Clinic/Center; Surgery, Oral/Maxillofacial"),
	
	/**
	 * Medical oncology clinic
	 */
	_ONCL ("ONCL", "Medical oncology clinic"),
	
	/**
	 * Opthalmology clinic
	 */
	_OPH ("OPH", "Opthalmology clinic"),
	
	/**
	 * organizational contact
	 */
	_ORG ("ORG", "organizational contact"),
	
	/**
	 * Orthopedics clinic
	 */
	_ORTHO ("ORTHO", "Orthopedics clinic"),
	
	/**
	 * Patient Specimen
	 */
	_P ("P", "Patient Specimen"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Pain
	 */
	_PAINCL ("PAINCL", "Ambulatory Health Care Facilities; Clinic/Center; Pain"),
	
	/**
	 * Payor Contact
	 */
	_PAYOR ("PAYOR", "Payor Contact"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Primary Care
	 */
	_PC ("PC", "Ambulatory Health Care Facilities; Clinic/Center; Primary Care"),
	
	/**
	 * Pediatrics clinic
	 */
	_PEDC ("PEDC", "Pediatrics clinic"),
	
	/**
	 * Pediatric cardiology clinic
	 */
	_PEDCARD ("PEDCARD", "Pediatric cardiology clinic"),
	
	/**
	 * Pediatric endocrinology clinic
	 */
	_PEDE ("PEDE", "Pediatric endocrinology clinic"),
	
	/**
	 * Pediatric gastroenterology clinic
	 */
	_PEDGI ("PEDGI", "Pediatric gastroenterology clinic"),
	
	/**
	 * Pediatric hematology clinic
	 */
	_PEDHEM ("PEDHEM", "Pediatric hematology clinic"),
	
	/**
	 * Pediatric oncology clinic
	 */
	_PEDHO ("PEDHO", "Pediatric oncology clinic"),
	
	/**
	 * Pediatric intensive care unit
	 */
	_PEDICU ("PEDICU", "Pediatric intensive care unit"),
	
	/**
	 * Pediatric infectious disease clinic
	 */
	_PEDID ("PEDID", "Pediatric infectious disease clinic"),
	
	/**
	 * Pediatric nephrology clinic
	 */
	_PEDNEPH ("PEDNEPH", "Pediatric nephrology clinic"),
	
	/**
	 * Pediatric neonatal intensive care unit
	 */
	_PEDNICU ("PEDNICU", "Pediatric neonatal intensive care unit"),
	
	/**
	 * Pediatric rheumatology clinic
	 */
	_PEDRHEUM ("PEDRHEUM", "Pediatric rheumatology clinic"),
	
	/**
	 * Pediatric unit
	 */
	_PEDU ("PEDU", "Pediatric unit"),
	
	/**
	 * Policy Holder
	 */
	_PH ("PH", "Policy Holder"),
	
	/**
	 * Pharmacy
	 */
	_PHARM ("PHARM", "Pharmacy"),
	
	/**
	 * Hospital Units; Psychiatric Unit
	 */
	_PHU ("PHU", "Hospital Units; Psychiatric Unit"),
	
	/**
	 * Pleasure
	 */
	_PL ("PL", "Pleasure"),
	
	/**
	 * Plastic surgery clinic
	 */
	_PLS ("PLS", "Plastic surgery clinic"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Podiatric
	 */
	_POD ("POD", "Ambulatory Health Care Facilities; Clinic/Center; Podiatric"),
	
	/**
	 * Pain rehabilitation center
	 */
	_PRC ("PRC", "Pain rehabilitation center"),
	
	/**
	 * Preventive medicine clinic
	 */
	_PREV ("PREV", "Preventive medicine clinic"),
	
	/**
	 * Parent
	 */
	_PRN ("PRN", "Parent"),
	
	/**
	 * parent in-law
	 */
	_PRNINLAW ("PRNINLAW", "parent in-law"),
	
	/**
	 * Proctology clinic
	 */
	_PROCTO ("PROCTO", "Proctology clinic"),
	
	/**
	 * Providers Office
	 */
	_PROFF ("PROFF", "Provider's Office"),
	
	/**
	 * Prosthodontics clinic
	 */
	_PROS ("PROS", "Prosthodontics clinic"),
	
	/**
	 * Psychology clinic
	 */
	_PSI ("PSI", "Psychology clinic"),
	
	/**
	 * part-time student
	 */
	_PSTUD ("PSTUD", "part-time student"),
	
	/**
	 * Psychiatry clinic
	 */
	_PSY ("PSY", "Psychiatry clinic"),
	
	/**
	 * Psychatric Care Facility
	 */
	_PSYCHF ("PSYCHF", "Psychatric Care Facility"),
	
	/**
	 * Patient
	 */
	_PT ("PT", "Patient"),
	
	/**
	 * Patients Residence
	 */
	_PTRES ("PTRES", "Patient's Residence"),
	
	/**
	 * Quality Control
	 */
	_Q ("Q", "Quality Control"),
	
	/**
	 * Replicate
	 */
	_R ("R", "Replicate"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Radiology
	 */
	_RADDX ("RADDX", "Ambulatory Health Care Facilities; Clinic/Center; Radiology"),
	
	/**
	 * Racing
	 */
	_RC ("RC", "Racing"),
	
	/**
	 * Hospitals; Rehabilitation Hospital
	 */
	_RH ("RH", "Hospitals; Rehabilitation Hospital"),
	
	/**
	 * Rheumatology clinic
	 */
	_RHEUM ("RHEUM", "Rheumatology clinic"),
	
	/**
	 * Hospital Units; Rehabilitation Unit
	 */
	_RHU ("RHU", "Hospital Units; Rehabilitation Unit"),
	
	/**
	 * Roommate
	 */
	_ROOM ("ROOM", "Roommate"),
	
	/**
	 * Residential treatment facility
	 */
	_RTF ("RTF", "Residential treatment facility"),
	
	/**
	 * school
	 */
	_SCHOOL ("SCHOOL", "school"),
	
	/**
	 * self
	 */
	_SELF ("SELF", "self"),
	
	/**
	 * Show
	 */
	_SH ("SH", "Show"),
	
	/**
	 * Sibling
	 */
	_SIB ("SIB", "Sibling"),
	
	/**
	 * sibling in-law
	 */
	_SIBINLAW ("SIBINLAW", "sibling in-law"),
	
	/**
	 * significant other
	 */
	_SIGOTHR ("SIGOTHR", "significant other"),
	
	/**
	 * Sister
	 */
	_SIS ("SIS", "Sister"),
	
	/**
	 * sister-in-law
	 */
	_SISLINLAW ("SISLINLAW", "sister-in-law"),
	
	/**
	 * Sleep disorders unit
	 */
	_SLEEP ("SLEEP", "Sleep disorders unit"),
	
	/**
	 * Nursing  Custodial Care Facilities; Skilled Nursing Facility
	 */
	_SNF ("SNF", "Nursing & Custodial Care Facilities; Skilled Nursing Facility"),
	
	/**
	 * natural son
	 */
	_SON ("SON", "natural son"),
	
	/**
	 * adopted son
	 */
	_SONADOPT ("SONADOPT", "adopted son"),
	
	/**
	 * foster son
	 */
	_SONFOST ("SONFOST", "foster son"),
	
	/**
	 * son-in-law
	 */
	_SONINLAW ("SONINLAW", "son-in-law"),
	
	/**
	 * Sports medicine clinic
	 */
	_SPMED ("SPMED", "Sports medicine clinic"),
	
	/**
	 * sponsored dependent
	 */
	_SPON ("SPON", "sponsored dependent"),
	
	/**
	 * spouse
	 */
	_SPS ("SPS", "spouse"),
	
	/**
	 * stepbrother
	 */
	_STPBRO ("STPBRO", "stepbrother"),
	
	/**
	 * step child
	 */
	_STPCHLD ("STPCHLD", "step child"),
	
	/**
	 * stepdaughter
	 */
	_STPDAU ("STPDAU", "stepdaughter"),
	
	/**
	 * stepfather
	 */
	_STPFTH ("STPFTH", "stepfather"),
	
	/**
	 * stepmother
	 */
	_STPMTH ("STPMTH", "stepmother"),
	
	/**
	 * stepparent
	 */
	_STPPRN ("STPPRN", "stepparent"),
	
	/**
	 * step sibling
	 */
	_STPSIB ("STPSIB", "step sibling"),
	
	/**
	 * stepsister
	 */
	_STPSIS ("STPSIS", "stepsister"),
	
	/**
	 * stepson
	 */
	_STPSON ("STPSON", "stepson"),
	
	/**
	 * student
	 */
	_STUD ("STUD", "student"),
	
	/**
	 * Surgery clinic
	 */
	_SU ("SU", "Surgery clinic"),
	
	/**
	 * Residential Treatment Facilities; Substance Use Rehabilitation Facility
	 */
	_SURF ("SURF", "Residential Treatment Facilities; Substance Use Rehabilitation Facility"),
	
	/**
	 * Transplant clinic
	 */
	_TR ("TR", "Transplant clinic"),
	
	/**
	 * Travel and geographic medicine clinic
	 */
	_TRAVEL ("TRAVEL", "Travel and geographic medicine clinic"),
	
	/**
	 * Tribal Member
	 */
	_TRB ("TRB", "Tribal Member"),
	
	/**
	 * uncle
	 */
	_UNCLE ("UNCLE", "uncle"),
	
	/**
	 * Urology clinic
	 */
	_URO ("URO", "Urology clinic"),
	
	/**
	 * Verifying
	 */
	_V ("V", "Verifying"),
	
	/**
	 * Veal
	 */
	_VL ("VL", "Veal"),
	
	/**
	 * wife
	 */
	_WIFE ("WIFE", "wife"),
	
	/**
	 * Wool
	 */
	_WL ("WL", "Wool"),
	
	/**
	 * Wound clinic
	 */
	_WND ("WND", "Wound clinic"),
	
	/**
	 * Working
	 */
	_WO ("WO", "Working"),
	
	/**
	 * work site
	 */
	_WORK ("WORK", "work site"),
	
	/**
	 * accident site
	 */
	_accidentsite ("ACC", "accident site"),
	
	/**
	 * Allergy clinic
	 */
	_Allergyclinic ("ALL", "Allergy clinic"),
	
	/**
	 * Ambulance
	 */
	_Ambulance ("AMB", "Ambulance"),
	
	/**
	 * Amputee clinic
	 */
	_Amputeeclinic ("AMPUT", "Amputee clinic"),
	
	/**
	 * aunt
	 */
	_aunt ("AUNT", "aunt"),
	
	/**
	 * Blind
	 */
	_Blind ("B", "Blind"),
	
	/**
	 * Beef
	 */
	_Beef ("BF", "Beef"),
	
	/**
	 * Billing Contact
	 */
	_BillingContact ("BILL", "Billing Contact"),
	
	/**
	 * Broiler
	 */
	_Broiler ("BL", "Broiler"),
	
	/**
	 * Bone marrow transplant clinic
	 */
	_Bonemarrowtransplantclinic ("BMTC", "Bone marrow transplant clinic"),
	
	/**
	 * Bone marrow transplant unit
	 */
	_Bonemarrowtransplantunit ("BMTU", "Bone marrow transplant unit"),
	
	/**
	 * Breeder
	 */
	_Breeder ("BR", "Breeder"),
	
	/**
	 * Breast clinic
	 */
	_Breastclinic ("BREAST", "Breast clinic"),
	
	/**
	 * Brother
	 */
	_Brother ("BRO", "Brother"),
	
	/**
	 * brother-in-law
	 */
	_brotherinlaw ("BROINLAW", "brother-in-law"),
	
	/**
	 * Calibrator
	 */
	_Calibrator ("C", "Calibrator"),
	
	/**
	 * Child and adolescent neurology clinic
	 */
	_Childandadolescentneurologyclinic ("CANC", "Child and adolescent neurology clinic"),
	
	/**
	 * Child and adolescent psychiatry clinic
	 */
	_Childandadolescentpsychiatryclinic ("CAPC", "Child and adolescent psychiatry clinic"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Rehabilitation Cardiac Facilities
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterRehabilitationCardiacFacilities ("CARD", "Ambulatory Health Care Facilities; Clinic/Center; Rehabilitation: Cardiac Facilities"),
	
	/**
	 * Cardiac catheterization lab
	 */
	_Cardiaccatheterizationlab ("CATH", "Cardiac catheterization lab"),
	
	/**
	 * Coronary care unit
	 */
	_Coronarycareunit ("CCU", "Coronary care unit"),
	
	/**
	 * Chest unit
	 */
	_Chestunit ("CHEST", "Chest unit"),
	
	/**
	 * Child
	 */
	_Child ("CHILD", "Child"),
	
	/**
	 * adopted child
	 */
	_adoptedchild ("CHLDADOPT", "adopted child"),
	
	/**
	 * foster child
	 */
	_fosterchild ("CHLDFOST", "foster child"),
	
	/**
	 * child-in-law
	 */
	_childinlaw ("CHLDINLAW", "child-in-law"),
	
	/**
	 * Chronic Care Facility
	 */
	_ChronicCareFacility ("CHR", "Chronic Care Facility"),
	
	/**
	 * Companion
	 */
	_Companion ("CO", "Companion"),
	
	/**
	 * Coagulation clinic
	 */
	_Coagulationclinic ("COAG", "Coagulation clinic"),
	
	/**
	 * Community Location
	 */
	_CommunityLocation ("COMM", "Community Location"),
	
	/**
	 * cousin
	 */
	_cousin ("COUSN", "cousin"),
	
	/**
	 * Colon and rectal surgery clinic
	 */
	_Colonandrectalsurgeryclinic ("CRS", "Colon and rectal surgery clinic"),
	
	/**
	 * Cardiovascular diagnostics or therapeutics unit
	 */
	_Cardiovasculardiagnosticsortherapeuticsunit ("CVDX", "Cardiovascular diagnostics or therapeutics unit"),
	
	/**
	 * Dairy
	 */
	_Dairy ("DA", "Dairy"),
	
	/**
	 * Delivery Address
	 */
	_DeliveryAddress ("DADDR", "Delivery Address"),
	
	/**
	 * natural daughter
	 */
	_naturaldaughter ("DAU", "natural daughter"),
	
	/**
	 * adopted daughter
	 */
	_adopteddaughter ("DAUADOPT", "adopted daughter"),
	
	/**
	 * foster daughter
	 */
	_fosterdaughter ("DAUFOST", "foster daughter"),
	
	/**
	 * daughter-in-law
	 */
	_daughterinlaw ("DAUINLAW", "daughter-in-law"),
	
	/**
	 * Dermatology clinic
	 */
	_Dermatologyclinic ("DERM", "Dermatology clinic"),
	
	/**
	 * domestic partner
	 */
	_domesticpartner ("DOMPART", "domestic partner"),
	
	/**
	 * Draft
	 */
	_Draft ("DR", "Draft"),
	
	/**
	 * Dual
	 */
	_Dual ("DU", "Dual"),
	
	/**
	 * Diagnostics or therapeutics unit
	 */
	_Diagnosticsortherapeuticsunit ("DX", "Diagnostics or therapeutics unit"),
	
	/**
	 * Electronic QC
	 */
	_ElectronicQC ("E", "Electronic QC"),
	
	/**
	 * Echocardiography lab
	 */
	_Echocardiographylab ("ECHO", "Echocardiography lab"),
	
	/**
	 * emergency contact
	 */
	_emergencycontact ("ECON", "emergency contact"),
	
	/**
	 * Endocrinology clinic
	 */
	_Endocrinologyclinic ("ENDO", "Endocrinology clinic"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Endoscopy
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterEndoscopy ("ENDOS", "Ambulatory Health Care Facilities; Clinic/Center; Endoscopy"),
	
	/**
	 * Otorhinolaryngology clinic
	 */
	_Otorhinolaryngologyclinic ("ENT", "Otorhinolaryngology clinic"),
	
	/**
	 * Epilepsy unit
	 */
	_Epilepsyunit ("EPIL", "Epilepsy unit"),
	
	/**
	 * Emergency room
	 */
	_Emergencyroom ("ER", "Emergency room"),
	
	/**
	 * Emergency trauma unit
	 */
	_Emergencytraumaunit ("ETU", "Emergency trauma unit"),
	
	/**
	 * Filler Proficiency
	 */
	_FillerProficiency ("F", "Filler Proficiency"),
	
	/**
	 * family dependent
	 */
	_familydependent ("FAMDEP", "family dependent"),
	
	/**
	 * Family Member
	 */
	_FamilyMember ("FAMMEMB", "Family Member"),
	
	/**
	 * Fiber
	 */
	_Fiber ("FI", "Fiber"),
	
	/**
	 * Family Member for Insurance Purposes
	 */
	_FamilyMemberforInsurancePurposes ("FM", "Family Member for Insurance Purposes"),
	
	/**
	 * Family medicine clinic
	 */
	_Familymedicineclinic ("FMC", "Family medicine clinic"),
	
	/**
	 * unrelated friend
	 */
	_unrelatedfriend ("FRND", "unrelated friend"),
	
	/**
	 * full-time student
	 */
	_fulltimestudent ("FSTUD", "full-time student"),
	
	/**
	 * Father
	 */
	_Father ("FTH", "Father"),
	
	/**
	 * father-in-law
	 */
	_fatherinlaw ("FTHINLAW", "father-in-law"),
	
	/**
	 * General acute care hospital
	 */
	_Generalacutecarehospital ("GACH", "General acute care hospital"),
	
	/**
	 * Group
	 */
	_Group ("G", "Group"),
	
	/**
	 * great grandfather
	 */
	_greatgrandfather ("GGRFTH", "great grandfather"),
	
	/**
	 * great grandmother
	 */
	_greatgrandmother ("GGRMTH", "great grandmother"),
	
	/**
	 * great grandparent
	 */
	_greatgrandparent ("GGRPRN", "great grandparent"),
	
	/**
	 * Gastroenterology clinic
	 */
	_Gastroenterologyclinic ("GI", "Gastroenterology clinic"),
	
	/**
	 * Gastroenterology diagnostics or therapeutics lab
	 */
	_Gastroenterologydiagnosticsortherapeuticslab ("GIDX", "Gastroenterology diagnostics or therapeutics lab"),
	
	/**
	 * General internal medicine clinic
	 */
	_Generalinternalmedicineclinic ("GIM", "General internal medicine clinic"),
	
	/**
	 * Grandfather
	 */
	_Grandfather ("GRFTH", "Grandfather"),
	
	/**
	 * Grandmother
	 */
	_Grandmother ("GRMTH", "Grandmother"),
	
	/**
	 * grandchild
	 */
	_grandchild ("GRNDCHILD", "grandchild"),
	
	/**
	 * granddaughter
	 */
	_granddaughter ("GRNDDAU", "granddaughter"),
	
	/**
	 * grandson
	 */
	_grandson ("GRNDSON", "grandson"),
	
	/**
	 * Grandparent
	 */
	_Grandparent ("GRPRN", "Grandparent"),
	
	/**
	 * Guarantor
	 */
	_Guarantor ("GT", "Guarantor"),
	
	/**
	 * Gynecology clinic
	 */
	_Gynecologyclinic ("GYN", "Gynecology clinic"),
	
	/**
	 * Hand clinic
	 */
	_Handclinic ("HAND", "Hand clinic"),
	
	/**
	 * handicapped dependent
	 */
	_handicappeddependent ("HANDIC", "handicapped dependent"),
	
	/**
	 * half-brother
	 */
	_halfbrother ("HBRO", "half-brother"),
	
	/**
	 * Hemodialysis unit
	 */
	_Hemodialysisunit ("HD", "Hemodialysis unit"),
	
	/**
	 * Hematology clinic
	 */
	_Hematologyclinic ("HEM", "Hematology clinic"),
	
	/**
	 * Hospital
	 */
	_Hospital ("HOSP", "Hospital"),
	
	/**
	 * half-sibling
	 */
	_halfsibling ("HSIB", "half-sibling"),
	
	/**
	 * Hypertension clinic
	 */
	_Hypertensionclinic ("HTN", "Hypertension clinic"),
	
	/**
	 * half-sister
	 */
	_halfsister ("HSIS", "half-sister"),
	
	/**
	 * Hospital unit
	 */
	_Hospitalunit ("HU", "Hospital unit"),
	
	/**
	 * husband
	 */
	_husband ("HUSB", "husband"),
	
	/**
	 * Intensive care unit
	 */
	_Intensivecareunit ("ICU", "Intensive care unit"),
	
	/**
	 * Impairment evaluation center
	 */
	_Impairmentevaluationcenter ("IEC", "Impairment evaluation center"),
	
	/**
	 * Infectious disease clinic
	 */
	_Infectiousdiseaseclinic ("INFD", "Infectious disease clinic"),
	
	/**
	 * injured plaintiff
	 */
	_injuredplaintiff ("INJ", "injured plaintiff"),
	
	/**
	 * Infertility clinic
	 */
	_Infertilityclinic ("INV", "Infertility clinic"),
	
	/**
	 * Pool
	 */
	_Pool ("L", "Pool"),
	
	/**
	 * Layer
	 */
	_Layer ("LY", "Layer"),
	
	/**
	 * Lympedema clinic
	 */
	_Lympedemaclinic ("LYMPH", "Lympedema clinic"),
	
	/**
	 * Medical genetics clinic
	 */
	_Medicalgeneticsclinic ("MGEN", "Medical genetics clinic"),
	
	/**
	 * Military Hospital
	 */
	_MilitaryHospital ("MHSP", "Military Hospital"),
	
	/**
	 * Mobile Unit
	 */
	_MobileUnit ("MOBL", "Mobile Unit"),
	
	/**
	 * Meat
	 */
	_Meat ("MT", "Meat"),
	
	/**
	 * Mother
	 */
	_Mother ("MTH", "Mother"),
	
	/**
	 * mother-in-law
	 */
	_motherinlaw ("MTHINLOAW", "mother-in-law"),
	
	/**
	 * Multiplier
	 */
	_Multiplier ("MU", "Multiplier"),
	
	/**
	 * neighbour
	 */
	_neighbour ("NBOR", "neighbour"),
	
	/**
	 * natural brother
	 */
	_naturalbrother ("NBRO", "natural brother"),
	
	/**
	 * Nursing or custodial care facility
	 */
	_Nursingorcustodialcarefacility ("NCCF", "Nursing or custodial care facility"),
	
	/**
	 * Neurology critical care and stroke unit
	 */
	_Neurologycriticalcareandstrokeunit ("NCCS", "Neurology critical care and stroke unit"),
	
	/**
	 * natural child
	 */
	_naturalchild ("NCHILD", "natural child"),
	
	/**
	 * Nephrology clinic
	 */
	_Nephrologyclinic ("NEPH", "Nephrology clinic"),
	
	/**
	 * nephew
	 */
	_nephew ("NEPHEW", "nephew"),
	
	/**
	 * Neurology clinic
	 */
	_Neurologyclinic ("NEUR", "Neurology clinic"),
	
	/**
	 * natural father
	 */
	_naturalfather ("NFTH", "natural father"),
	
	/**
	 * niece
	 */
	_niece ("NIECE", "niece"),
	
	/**
	 * niecenephew
	 */
	_niecenephew ("NIENEPH", "niece/nephew"),
	
	/**
	 * natural mother
	 */
	_naturalmother ("NMTH", "natural mother"),
	
	/**
	 * next of kin
	 */
	_nextofkin ("NOK", "next of kin"),
	
	/**
	 * natural parent
	 */
	_naturalparent ("NPRN", "natural parent"),
	
	/**
	 * Neurosurgery unit
	 */
	_Neurosurgeryunit ("NS", "Neurosurgery unit"),
	
	/**
	 * natural sibling
	 */
	_naturalsibling ("NSIB", "natural sibling"),
	
	/**
	 * natural sister
	 */
	_naturalsister ("NSIS", "natural sister"),
	
	/**
	 * Operator Proficiency
	 */
	_OperatorProficiency ("O", "Operator Proficiency"),
	
	/**
	 * Obstetrics clinic
	 */
	_Obstetricsclinic ("OB", "Obstetrics clinic"),
	
	/**
	 * Outpatient facility
	 */
	_Outpatientfacility ("OF", "Outpatient facility"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Surgery, OralMaxillofacial
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterSurgeryOralMaxillofacial ("OMS", "Ambulatory Health Care Facilities; Clinic/Center; Surgery, Oral/Maxillofacial"),
	
	/**
	 * Medical oncology clinic
	 */
	_Medicaloncologyclinic ("ONCL", "Medical oncology clinic"),
	
	/**
	 * Opthalmology clinic
	 */
	_Opthalmologyclinic ("OPH", "Opthalmology clinic"),
	
	/**
	 * organizational contact
	 */
	_organizationalcontact ("ORG", "organizational contact"),
	
	/**
	 * Orthopedics clinic
	 */
	_Orthopedicsclinic ("ORTHO", "Orthopedics clinic"),
	
	/**
	 * Patient Specimen
	 */
	_PatientSpecimen ("P", "Patient Specimen"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Pain
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterPain ("PAINCL", "Ambulatory Health Care Facilities; Clinic/Center; Pain"),
	
	/**
	 * Payor Contact
	 */
	_PayorContact ("PAYOR", "Payor Contact"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Primary Care
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterPrimaryCare ("PC", "Ambulatory Health Care Facilities; Clinic/Center; Primary Care"),
	
	/**
	 * Pediatrics clinic
	 */
	_Pediatricsclinic ("PEDC", "Pediatrics clinic"),
	
	/**
	 * Pediatric cardiology clinic
	 */
	_Pediatriccardiologyclinic ("PEDCARD", "Pediatric cardiology clinic"),
	
	/**
	 * Pediatric endocrinology clinic
	 */
	_Pediatricendocrinologyclinic ("PEDE", "Pediatric endocrinology clinic"),
	
	/**
	 * Pediatric gastroenterology clinic
	 */
	_Pediatricgastroenterologyclinic ("PEDGI", "Pediatric gastroenterology clinic"),
	
	/**
	 * Pediatric hematology clinic
	 */
	_Pediatrichematologyclinic ("PEDHEM", "Pediatric hematology clinic"),
	
	/**
	 * Pediatric oncology clinic
	 */
	_Pediatriconcologyclinic ("PEDHO", "Pediatric oncology clinic"),
	
	/**
	 * Pediatric intensive care unit
	 */
	_Pediatricintensivecareunit ("PEDICU", "Pediatric intensive care unit"),
	
	/**
	 * Pediatric infectious disease clinic
	 */
	_Pediatricinfectiousdiseaseclinic ("PEDID", "Pediatric infectious disease clinic"),
	
	/**
	 * Pediatric nephrology clinic
	 */
	_Pediatricnephrologyclinic ("PEDNEPH", "Pediatric nephrology clinic"),
	
	/**
	 * Pediatric neonatal intensive care unit
	 */
	_Pediatricneonatalintensivecareunit ("PEDNICU", "Pediatric neonatal intensive care unit"),
	
	/**
	 * Pediatric rheumatology clinic
	 */
	_Pediatricrheumatologyclinic ("PEDRHEUM", "Pediatric rheumatology clinic"),
	
	/**
	 * Pediatric unit
	 */
	_Pediatricunit ("PEDU", "Pediatric unit"),
	
	/**
	 * Policy Holder
	 */
	_PolicyHolder ("PH", "Policy Holder"),
	
	/**
	 * Pharmacy
	 */
	_Pharmacy ("PHARM", "Pharmacy"),
	
	/**
	 * Hospital Units; Psychiatric Unit
	 */
	_HospitalUnitsPsychiatricUnit ("PHU", "Hospital Units; Psychiatric Unit"),
	
	/**
	 * Pleasure
	 */
	_Pleasure ("PL", "Pleasure"),
	
	/**
	 * Plastic surgery clinic
	 */
	_Plasticsurgeryclinic ("PLS", "Plastic surgery clinic"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Podiatric
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterPodiatric ("POD", "Ambulatory Health Care Facilities; Clinic/Center; Podiatric"),
	
	/**
	 * Pain rehabilitation center
	 */
	_Painrehabilitationcenter ("PRC", "Pain rehabilitation center"),
	
	/**
	 * Preventive medicine clinic
	 */
	_Preventivemedicineclinic ("PREV", "Preventive medicine clinic"),
	
	/**
	 * Parent
	 */
	_Parent ("PRN", "Parent"),
	
	/**
	 * parent in-law
	 */
	_parentinlaw ("PRNINLAW", "parent in-law"),
	
	/**
	 * Proctology clinic
	 */
	_Proctologyclinic ("PROCTO", "Proctology clinic"),
	
	/**
	 * Providers Office
	 */
	_ProvidersOffice ("PROFF", "Provider's Office"),
	
	/**
	 * Prosthodontics clinic
	 */
	_Prosthodonticsclinic ("PROS", "Prosthodontics clinic"),
	
	/**
	 * Psychology clinic
	 */
	_Psychologyclinic ("PSI", "Psychology clinic"),
	
	/**
	 * part-time student
	 */
	_parttimestudent ("PSTUD", "part-time student"),
	
	/**
	 * Psychiatry clinic
	 */
	_Psychiatryclinic ("PSY", "Psychiatry clinic"),
	
	/**
	 * Psychatric Care Facility
	 */
	_PsychatricCareFacility ("PSYCHF", "Psychatric Care Facility"),
	
	/**
	 * Patient
	 */
	_Patient ("PT", "Patient"),
	
	/**
	 * Patients Residence
	 */
	_PatientsResidence ("PTRES", "Patient's Residence"),
	
	/**
	 * Quality Control
	 */
	_QualityControl ("Q", "Quality Control"),
	
	/**
	 * Replicate
	 */
	_Replicate ("R", "Replicate"),
	
	/**
	 * Ambulatory Health Care Facilities; ClinicCenter; Radiology
	 */
	_AmbulatoryHealthCareFacilitiesClinicCenterRadiology ("RADDX", "Ambulatory Health Care Facilities; Clinic/Center; Radiology"),
	
	/**
	 * Racing
	 */
	_Racing ("RC", "Racing"),
	
	/**
	 * Hospitals; Rehabilitation Hospital
	 */
	_HospitalsRehabilitationHospital ("RH", "Hospitals; Rehabilitation Hospital"),
	
	/**
	 * Rheumatology clinic
	 */
	_Rheumatologyclinic ("RHEUM", "Rheumatology clinic"),
	
	/**
	 * Hospital Units; Rehabilitation Unit
	 */
	_HospitalUnitsRehabilitationUnit ("RHU", "Hospital Units; Rehabilitation Unit"),
	
	/**
	 * Roommate
	 */
	_Roommate ("ROOM", "Roommate"),
	
	/**
	 * Residential treatment facility
	 */
	_Residentialtreatmentfacility ("RTF", "Residential treatment facility"),
	
	/**
	 * school
	 */
	_school ("SCHOOL", "school"),
	
	/**
	 * self
	 */
	_self ("SELF", "self"),
	
	/**
	 * Show
	 */
	_Show ("SH", "Show"),
	
	/**
	 * Sibling
	 */
	_Sibling ("SIB", "Sibling"),
	
	/**
	 * sibling in-law
	 */
	_siblinginlaw ("SIBINLAW", "sibling in-law"),
	
	/**
	 * significant other
	 */
	_significantother ("SIGOTHR", "significant other"),
	
	/**
	 * Sister
	 */
	_Sister ("SIS", "Sister"),
	
	/**
	 * sister-in-law
	 */
	_sisterinlaw ("SISLINLAW", "sister-in-law"),
	
	/**
	 * Sleep disorders unit
	 */
	_Sleepdisordersunit ("SLEEP", "Sleep disorders unit"),
	
	/**
	 * Nursing  Custodial Care Facilities; Skilled Nursing Facility
	 */
	_NursingCustodialCareFacilitiesSkilledNursingFacility ("SNF", "Nursing & Custodial Care Facilities; Skilled Nursing Facility"),
	
	/**
	 * natural son
	 */
	_naturalson ("SON", "natural son"),
	
	/**
	 * adopted son
	 */
	_adoptedson ("SONADOPT", "adopted son"),
	
	/**
	 * foster son
	 */
	_fosterson ("SONFOST", "foster son"),
	
	/**
	 * son-in-law
	 */
	_soninlaw ("SONINLAW", "son-in-law"),
	
	/**
	 * Sports medicine clinic
	 */
	_Sportsmedicineclinic ("SPMED", "Sports medicine clinic"),
	
	/**
	 * sponsored dependent
	 */
	_sponsoreddependent ("SPON", "sponsored dependent"),
	
	/**
	 * spouse
	 */
	_spouse ("SPS", "spouse"),
	
	/**
	 * stepbrother
	 */
	_stepbrother ("STPBRO", "stepbrother"),
	
	/**
	 * step child
	 */
	_stepchild ("STPCHLD", "step child"),
	
	/**
	 * stepdaughter
	 */
	_stepdaughter ("STPDAU", "stepdaughter"),
	
	/**
	 * stepfather
	 */
	_stepfather ("STPFTH", "stepfather"),
	
	/**
	 * stepmother
	 */
	_stepmother ("STPMTH", "stepmother"),
	
	/**
	 * stepparent
	 */
	_stepparent ("STPPRN", "stepparent"),
	
	/**
	 * step sibling
	 */
	_stepsibling ("STPSIB", "step sibling"),
	
	/**
	 * stepsister
	 */
	_stepsister ("STPSIS", "stepsister"),
	
	/**
	 * stepson
	 */
	_stepson ("STPSON", "stepson"),
	
	/**
	 * student
	 */
	_student ("STUD", "student"),
	
	/**
	 * Surgery clinic
	 */
	_Surgeryclinic ("SU", "Surgery clinic"),
	
	/**
	 * Residential Treatment Facilities; Substance Use Rehabilitation Facility
	 */
	_ResidentialTreatmentFacilitiesSubstanceUseRehabilitationFacility ("SURF", "Residential Treatment Facilities; Substance Use Rehabilitation Facility"),
	
	/**
	 * Transplant clinic
	 */
	_Transplantclinic ("TR", "Transplant clinic"),
	
	/**
	 * Travel and geographic medicine clinic
	 */
	_Travelandgeographicmedicineclinic ("TRAVEL", "Travel and geographic medicine clinic"),
	
	/**
	 * Tribal Member
	 */
	_TribalMember ("TRB", "Tribal Member"),
	
	/**
	 * uncle
	 */
	_uncle ("UNCLE", "uncle"),
	
	/**
	 * Urology clinic
	 */
	_Urologyclinic ("URO", "Urology clinic"),
	
	/**
	 * Verifying
	 */
	_Verifying ("V", "Verifying"),
	
	/**
	 * Veal
	 */
	_Veal ("VL", "Veal"),
	
	/**
	 * wife
	 */
	_wife ("WIFE", "wife"),
	
	/**
	 * Wool
	 */
	_Wool ("WL", "Wool"),
	
	/**
	 * Wound clinic
	 */
	_Woundclinic ("WND", "Wound clinic"),
	
	/**
	 * Working
	 */
	_Working ("WO", "Working"),
	
	/**
	 * work site
	 */
	_worksite ("WORK", "work site"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.5.111";

	RoleCode(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static RoleCode getByCode(String code) {
		RoleCode[] vals = values();
		for (RoleCode val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(RoleCode other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	