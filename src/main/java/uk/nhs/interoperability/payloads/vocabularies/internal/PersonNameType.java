/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

public enum PersonNameType implements VocabularyEntry {
	/** Known as/conventional/the one you use */
	Legal("L"),
	/** Artist/Stage Name. Includes writer's pseudonym, stage name, etc */
	Artist("A"),
	/** Indigenous/Tribal e.g. Chief Red Cloud */
	Indigenous("I"),
	/** Religious e.g. Sister Mary Francis, Brother John */
	Religious("R"),
	/** Alphabetic transcription of name (Japanese: romaji) */
	Alphabetic("ABC"),
	/** Syllabic transcription of name (e.g., Japanese kana, Korean hangul) */
	Syllabic("SYL"),
	/** Ideographic representation of name (e.g., Japanese kanji, Chinese characters) */
	Ideographic("IDE"),
	/** Previous Name: Birth Name */
	Birth("PREVIOUS-BIRTH"),
	/** Previous Name: Maiden Name */
	Maiden("PREVIOUS-MAIDEN"),
	/** Previous Name: Bachelor */
	Bachelor("PREVIOUS-BACHELOR"),
	/** Previous Name */
	Previous("PREVIOUS"),
	/** Preferred Name */
	Preferred("PREFERRED");
	
	public String code;
	
	private PersonNameType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static PersonNameType getByCode(String code) {
		PersonNameType[] vals = values();
		for (PersonNameType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(PersonNameType other) {
		return (other.getCode().equals(code));
	}
	
}
