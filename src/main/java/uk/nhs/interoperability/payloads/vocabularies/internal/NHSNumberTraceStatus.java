/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT;

public enum NHSNumberTraceStatus implements VocabularyEntry {
	Traced("01"),
	PresentButNotTraced("02"),
	TraceRequired("03"),
	TraceAttemptedNoMatchOrMultipleMatch("04"),
	TraceNeedsToBeResolved("05"),
	TraceInProgress("06"),
	NotPresent("07"),
	TracePostponedBabyUnderSixWeeks("08");
	
	public String code;
	
	private NHSNumberTraceStatus(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}

	public static NHSNumberTraceStatus getByCode(String code) {
		NHSNumberTraceStatus[] vals = values();
		for (NHSNumberTraceStatus val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(NHSNumberTraceStatus other) {
		return (other.getCode().equals(code));
	}
	
}
