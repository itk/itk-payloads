/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the Ethnicity vocabulary:
 * <ul>
 *   <li>_A : British</li>
 *   <li>_B : Irish</li>
 *   <li>_C : Any other White background</li>
 *   <li>_D : White and Black Caribbean</li>
 *   <li>_E : White and Black African</li>
 *   <li>_F : White and Asian</li>
 *   <li>_G : Any other mixed background</li>
 *   <li>_H : Indian</li>
 *   <li>_J : Pakistani</li>
 *   <li>_K : Bangladeshi</li>
 *   <li>_L : Any other Asian background</li>
 *   <li>_M : Caribbean</li>
 *   <li>_N : African</li>
 *   <li>_P : Any other Black background</li>
 *   <li>_R : Chinese</li>
 *   <li>_S : Any other ethnic group</li>
 *   <li>_Z : Not stated</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum Ethnicity implements VocabularyEntry {
	
	
	/**
	 * British
	 */
	_A ("A", "British"),
	
	/**
	 * Irish
	 */
	_B ("B", "Irish"),
	
	/**
	 * Any other White background
	 */
	_C ("C", "Any other White background"),
	
	/**
	 * White and Black Caribbean
	 */
	_D ("D", "White and Black Caribbean"),
	
	/**
	 * White and Black African
	 */
	_E ("E", "White and Black African"),
	
	/**
	 * White and Asian
	 */
	_F ("F", "White and Asian"),
	
	/**
	 * Any other mixed background
	 */
	_G ("G", "Any other mixed background"),
	
	/**
	 * Indian
	 */
	_H ("H", "Indian"),
	
	/**
	 * Pakistani
	 */
	_J ("J", "Pakistani"),
	
	/**
	 * Bangladeshi
	 */
	_K ("K", "Bangladeshi"),
	
	/**
	 * Any other Asian background
	 */
	_L ("L", "Any other Asian background"),
	
	/**
	 * Caribbean
	 */
	_M ("M", "Caribbean"),
	
	/**
	 * African
	 */
	_N ("N", "African"),
	
	/**
	 * Any other Black background
	 */
	_P ("P", "Any other Black background"),
	
	/**
	 * Chinese
	 */
	_R ("R", "Chinese"),
	
	/**
	 * Any other ethnic group
	 */
	_S ("S", "Any other ethnic group"),
	
	/**
	 * Not stated
	 */
	_Z ("Z", "Not stated"),
	
	/**
	 * British
	 */
	_British ("A", "British"),
	
	/**
	 * Irish
	 */
	_Irish ("B", "Irish"),
	
	/**
	 * Any other White background
	 */
	_AnyotherWhitebackground ("C", "Any other White background"),
	
	/**
	 * White and Black Caribbean
	 */
	_WhiteandBlackCaribbean ("D", "White and Black Caribbean"),
	
	/**
	 * White and Black African
	 */
	_WhiteandBlackAfrican ("E", "White and Black African"),
	
	/**
	 * White and Asian
	 */
	_WhiteandAsian ("F", "White and Asian"),
	
	/**
	 * Any other mixed background
	 */
	_Anyothermixedbackground ("G", "Any other mixed background"),
	
	/**
	 * Indian
	 */
	_Indian ("H", "Indian"),
	
	/**
	 * Pakistani
	 */
	_Pakistani ("J", "Pakistani"),
	
	/**
	 * Bangladeshi
	 */
	_Bangladeshi ("K", "Bangladeshi"),
	
	/**
	 * Any other Asian background
	 */
	_AnyotherAsianbackground ("L", "Any other Asian background"),
	
	/**
	 * Caribbean
	 */
	_Caribbean ("M", "Caribbean"),
	
	/**
	 * African
	 */
	_African ("N", "African"),
	
	/**
	 * Any other Black background
	 */
	_AnyotherBlackbackground ("P", "Any other Black background"),
	
	/**
	 * Chinese
	 */
	_Chinese ("R", "Chinese"),
	
	/**
	 * Any other ethnic group
	 */
	_Anyotherethnicgroup ("S", "Any other ethnic group"),
	
	/**
	 * Not stated
	 */
	_Notstated ("Z", "Not stated"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.16.81";

	Ethnicity(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static Ethnicity getByCode(String code) {
		Ethnicity[] vals = values();
		for (Ethnicity val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(Ethnicity other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	