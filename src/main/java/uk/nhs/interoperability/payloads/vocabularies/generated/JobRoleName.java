/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the JobRoleName vocabulary:
 * <ul>
 *   <li>_NR0010 : Medical Director</li>
 *   <li>_NR0020 : Clinical Director - Medical</li>
 *   <li>_NR0030 : Professor</li>
 *   <li>_NR0040 : Senior Lecturer</li>
 *   <li>_NR0050 : Consultant</li>
 *   <li>_NR0055 : Dental surgeon acting as Hospital Consultant</li>
 *   <li>_NR0060 : Special salary scale in Public Health Medicine</li>
 *   <li>_NR0070 : Associate Specialist</li>
 *   <li>_NR0080 : Staff Grade</li>
 *   <li>_NR0090 : Hospital Practitioner</li>
 *   <li>_NR0100 : Clinical Assistant</li>
 *   <li>_NR0110 : Specialist Registrar</li>
 *   <li>_NR0120 : Senior Registrar Closed </li>
 *   <li>_NR0130 : Registrar Closed</li>
 *   <li>_NR0140 : Senior House Officer</li>
 *   <li>_NR0150 : House Officer - Pre Registration</li>
 *   <li>_NR0160 : House Officer - Post Registration</li>
 *   <li>_NR0170 : Trust Grade Doctor - House Officer level</li>
 *   <li>_NR0180 : Trust Grade Doctor - SHO level</li>
 *   <li>_NR0190 : Trust Grade Doctor - Specialist Registrar level</li>
 *   <li>_NR0200 : Trust Grade Doctor - Career Grade level</li>
 *   <li>_NR0210 : Director of Public Health</li>
 *   <li>_NR0215 : Asst Clinical Medical Officer</li>
 *   <li>_NR0220 : Clinical Medical Officer</li>
 *   <li>_NR0230 : Senior Clinical Medical Officer</li>
 *   <li>_NR0240 : Other Community Health Service</li>
 *   <li>_NR0243 : Other Community Health Service - Social Care Worker</li>
 *   <li>_NR0247 : Other Community Health Service - Admin Clerk</li>
 *   <li>_NR0250 : General Dental Practitioner</li>
 *   <li>_NR0260 : General Medical Practitioner</li>
 *   <li>_NR0270 : Salaried General Practitioner</li>
 *   <li>_NR0280 : Regional Dental Officer</li>
 *   <li>_NR0290 : Dental Clinical Director - Dental</li>
 *   <li>_NR0295 : Dental Assistant Clinical Director</li>
 *   <li>_NR0300 : Dental Officer</li>
 *   <li>_NR0310 : Senior Dental Officer</li>
 *   <li>_NR0320 : Salaried Dental Practitioner</li>
 *   <li>_NR0330 : Student Nurse - Adult Branch</li>
 *   <li>_NR0340 : Student Nurse - Child Branch</li>
 *   <li>_NR0350 : Student Nurse - Mental Health Branch</li>
 *   <li>_NR0360 : Student Nurse - Learning Disabilities Branch</li>
 *   <li>_NR0370 : Student Midwife</li>
 *   <li>_NR0380 : Student Health Visitor</li>
 *   <li>_NR0390 : Student District Nurse</li>
 *   <li>_NR0400 : Student School Nurse</li>
 *   <li>_NR0410 : Student Practice Nurse</li>
 *   <li>_NR0420 : Student Occupational Health Nurse</li>
 *   <li>_NR0430 : Student Community Paediatric Nurse</li>
 *   <li>_NR0440 : Student Community Mental Health Nurse</li>
 *   <li>_NR0450 : Student Community Learning Disabilities Nurse</li>
 *   <li>_NR0460 : Student Chiropodist</li>
 *   <li>_NR0470 : Student Dietitian</li>
 *   <li>_NR0480 : Student Occupational Therapist</li>
 *   <li>_NR0490 : Student Orthoptist</li>
 *   <li>_NR0500 : Student Physiotherapist</li>
 *   <li>_NR0510 : Student Radiographer - Diagnostic</li>
 *   <li>_NR0520 : Student Radiographer - Therapeutic</li>
 *   <li>_NR0530 : Student Speech  Language Therapist</li>
 *   <li>_NR0540 : Art, Music  Drama Student</li>
 *   <li>_NR0550 : Student Psychotherapist</li>
 *   <li>_NR0560 : Director of Nursing</li>
 *   <li>_NR0570 : Nurse Consultant</li>
 *   <li>_NR0580 : Nurse Manager</li>
 *   <li>_NR0590 : Modern Matron</li>
 *   <li>_NR0600 : Specialist Nurse Practitioner</li>
 *   <li>_NR0610 : SisterCharge Nurse</li>
 *   <li>_NR0620 : Staff Nurse</li>
 *   <li>_NR0630 : Enrolled Nurse</li>
 *   <li>_NR0640 : Midwife - Consultant</li>
 *   <li>_NR0650 : Midwife - Specialist Practitioner</li>
 *   <li>_NR0660 : Midwife - Manager</li>
 *   <li>_NR0670 : Midwife - SisterCharge Nurse</li>
 *   <li>_NR0680 : Midwife</li>
 *   <li>_NR0690 : Community Practitioner</li>
 *   <li>_NR0700 : Community Nurse</li>
 *   <li>_NR0710 : Art Therapist</li>
 *   <li>_NR0720 : Art Therapist Consultant</li>
 *   <li>_NR0730 : Art Therapist Manager</li>
 *   <li>_NR0740 : Art Therapist Specialist Practitioner</li>
 *   <li>_NR0750 : ChiropodistPodiatrist</li>
 *   <li>_NR0760 : ChiropodistPodiatrist Consultant</li>
 *   <li>_NR0770 : ChiropodistPodiatrist Manager</li>
 *   <li>_NR0780 : ChiropodistPodiatrist Specialist Practitioner</li>
 *   <li>_NR0790 : Dietitian</li>
 *   <li>_NR0800 : Dietitian Consultant</li>
 *   <li>_NR0810 : Dietitian Manager</li>
 *   <li>_NR0820 : Dietitian Specialist Practitioner</li>
 *   <li>_NR0830 : Drama Therapist</li>
 *   <li>_NR0840 : Drama Therapist Consultant</li>
 *   <li>_NR0850 : Drama Therapist Manager</li>
 *   <li>_NR0860 : Drama Therapist Specialist Practitioner</li>
 *   <li>_NR0870 : Multi Therapist</li>
 *   <li>_NR0880 : Multi Therapist Consultant</li>
 *   <li>_NR0890 : Multi Therapist Manager</li>
 *   <li>_NR0900 : Multi Therapist Specialist Practitioner</li>
 *   <li>_NR0910 : Music Therapist</li>
 *   <li>_NR0920 : Music Therapist Consultant</li>
 *   <li>_NR0930 : Music Therapist Manager</li>
 *   <li>_NR0940 : Music Therapist Specialist Practitioner</li>
 *   <li>_NR0950 : Occupational Therapist</li>
 *   <li>_NR0955 : Speech  Language Therapist</li>
 *   <li>_NR0960 : Occupational Therapist Consultant</li>
 *   <li>_NR0965 : Speech  Language Therapist Consultant</li>
 *   <li>_NR0970 : Occupational Therapist Manager</li>
 *   <li>_NR0975 : Speech  Language Therapist Manager</li>
 *   <li>_NR0980 : Occupational Therapy Specialist Practitioner</li>
 *   <li>_NR0985 : Speech  Language Therapist Specialist Practitioner</li>
 *   <li>_NR0990 : Orthoptist</li>
 *   <li>_NR1000 : Orthoptist Consultant</li>
 *   <li>_NR1010 : Orthoptist Manager</li>
 *   <li>_NR1020 : Orthoptist Specialist Practitioner</li>
 *   <li>_NR1030 : Orthotist</li>
 *   <li>_NR1040 : Orthotist Consultant</li>
 *   <li>_NR1050 : Orthotist Manager</li>
 *   <li>_NR1060 : Orthotist Specialist Practitioner</li>
 *   <li>_NR1070 : Paramedic</li>
 *   <li>_NR1080 : Paramedic Consultant</li>
 *   <li>_NR1090 : Paramedic Manager</li>
 *   <li>_NR1100 : Paramedic Specialist Practitioner</li>
 *   <li>_NR1110 : Physiotherapist</li>
 *   <li>_NR1120 : Physiotherapist Consultant</li>
 *   <li>_NR1130 : Physiotherapist Manager</li>
 *   <li>_NR1140 : Physiotherapist Specialist Practitioner</li>
 *   <li>_NR1150 : Prosthetist</li>
 *   <li>_NR1160 : Prosthetist Consultant</li>
 *   <li>_NR1170 : Prosthetist Manager</li>
 *   <li>_NR1180 : Prosthetist Specialist Practitioner</li>
 *   <li>_NR1190 : Radiographer - Diagnostic</li>
 *   <li>_NR1200 : Radiographer - Diagnostic, Consultant</li>
 *   <li>_NR1210 : Radiographer - Diagnostic, Manager</li>
 *   <li>_NR1220 : Radiographer - Diagnostic, Specialist Practitioner</li>
 *   <li>_NR1230 : Radiographer - Therapeutic</li>
 *   <li>_NR1240 : Radiographer - Therapeutic, Consultant</li>
 *   <li>_NR1250 : Radiographer - Therapeutic, Manager</li>
 *   <li>_NR1260 : Radiographer - Therapeutic, Specialist Practitioner</li>
 *   <li>_NR1270 : Clinical Director</li>
 *   <li>_NR1280 : Optometrist</li>
 *   <li>_NR1290 : Pharmacist</li>
 *   <li>_NR1300 : Psychotherapist</li>
 *   <li>_NR1310 : Clinical Psychologist</li>
 *   <li>_NR1320 : Chaplain</li>
 *   <li>_NR1330 : Social Worker</li>
 *   <li>_NR1340 : Approved Social Worker</li>
 *   <li>_NR1350 : Youth Worker</li>
 *   <li>_NR1360 : Specialist Practitioner</li>
 *   <li>_NR1370 : Practitioner</li>
 *   <li>_NR1380 : Technician - PST</li>
 *   <li>_NR1390 : Osteopath</li>
 *   <li>_NR1400 : Healthcare Scientist</li>
 *   <li>_NR1410 : Consultant Healthcare Scientist</li>
 *   <li>_NR1420 : Biomedical Scientist</li>
 *   <li>_NR1430 : Technician - Healthcare Scientists</li>
 *   <li>_NR1440 : Therapist</li>
 *   <li>_NR1450 : Health Care Support Worker</li>
 *   <li>_NR1460 : Social Care Support Worker</li>
 *   <li>_NR1470 : Home Help</li>
 *   <li>_NR1480 : Healthcare Assistant</li>
 *   <li>_NR1490 : Nursery Nurse</li>
 *   <li>_NR1500 : Play Therapist</li>
 *   <li>_NR1510 : Play Specialist</li>
 *   <li>_NR1520 : Technician - Addl Clinical Services</li>
 *   <li>_NR1530 : Technical Instructor</li>
 *   <li>_NR1540 : Associate Practitioner</li>
 *   <li>_NR1543 : Associate Practitioner - Nurse</li>
 *   <li>_NR1547 : Associate Practitioner - General Practitioner</li>
 *   <li>_NR1550 : Counsellor</li>
 *   <li>_NR1560 : HelperAssistant</li>
 *   <li>_NR1570 : Dental Surgery Assistant</li>
 *   <li>_NR1580 : Medical Laboratory Assistant</li>
 *   <li>_NR1590 : Phlebotomist</li>
 *   <li>_NR1600 : Cytoscreener</li>
 *   <li>_NR1610 : Student Technician</li>
 *   <li>_NR1620 : Trainee Scientist</li>
 *   <li>_NR1630 : Trainee Practitioner</li>
 *   <li>_NR1640 : Nursing Cadet</li>
 *   <li>_NR1650 : Healthcare Cadet</li>
 *   <li>_NR1660 : Pre-reg Pharmacist</li>
 *   <li>_NR1670 : Assistant Psychologist</li>
 *   <li>_NR1680 : Assistant Psychotherapist</li>
 *   <li>_NR1690 : Call Operator</li>
 *   <li>_NR1700 : Gateway Worker</li>
 *   <li>_NR1710 : Support, Time, Recovery Worker</li>
 *   <li>_NR1720 : Clerical Worker</li>
 *   <li>_NR1730 : Receptionist</li>
 *   <li>_NR1740 : Secretary</li>
 *   <li>_NR1750 : Personal Assistant</li>
 *   <li>_NR1760 : Medical Secretary</li>
 *   <li>_NR1770 : Officer</li>
 *   <li>_NR1780 : Manager</li>
 *   <li>_NR1790 : Senior Manager</li>
 *   <li>_NR1800 : Technician - Admin  Clerical</li>
 *   <li>_NR1810 : Accountant</li>
 *   <li>_NR1820 : Librarian</li>
 *   <li>_NR1830 : Interpreter</li>
 *   <li>_NR1840 : Analyst</li>
 *   <li>_NR1850 : Adviser</li>
 *   <li>_NR1860 : Researcher</li>
 *   <li>_NR1870 : Control Assistant</li>
 *   <li>_NR1880 : Architect</li>
 *   <li>_NR1890 : Lawyer</li>
 *   <li>_NR1900 : Surveyor</li>
 *   <li>_NR1910 : Chair</li>
 *   <li>_NR1920 : Chief Executive</li>
 *   <li>_NR1930 : Finance Director</li>
 *   <li>_NR1940 : Other Executive Director</li>
 *   <li>_NR1950 : Board Level Director</li>
 *   <li>_NR1960 : Non Executive Director</li>
 *   <li>_NR1970 : Childcare Co-ordinator</li>
 *   <li>_NR1980 : Main Informal Carer</li>
 *   <li>_NR1990 : Formal Carer</li>
 *   <li>_NR2000 : Additional person to be involved in decisions</li>
 *   <li>_NR2010 : LPA with authority</li>
 *   <li>_NR2020 : Key Worker</li>
 *   <li>_NR2030 : Senior Responsible Clinician</li>
 *   <li>_NR2040 : Palliative Care Physician</li>
 *   <li>_NR2050 : Specialist Palliative Care Nurse</li>
 *   <li>_NR2060 : Discharge Coordinator</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum JobRoleName implements VocabularyEntry {
	
	
	/**
	 * Medical Director
	 */
	_NR0010 ("NR0010", "Medical Director"),
	
	/**
	 * Clinical Director - Medical
	 */
	_NR0020 ("NR0020", "Clinical Director - Medical"),
	
	/**
	 * Professor
	 */
	_NR0030 ("NR0030", "Professor"),
	
	/**
	 * Senior Lecturer
	 */
	_NR0040 ("NR0040", "Senior Lecturer"),
	
	/**
	 * Consultant
	 */
	_NR0050 ("NR0050", "Consultant"),
	
	/**
	 * Dental surgeon acting as Hospital Consultant
	 */
	_NR0055 ("NR0055", "Dental surgeon acting as Hospital Consultant"),
	
	/**
	 * Special salary scale in Public Health Medicine
	 */
	_NR0060 ("NR0060", "Special salary scale in Public Health Medicine"),
	
	/**
	 * Associate Specialist
	 */
	_NR0070 ("NR0070", "Associate Specialist"),
	
	/**
	 * Staff Grade
	 */
	_NR0080 ("NR0080", "Staff Grade"),
	
	/**
	 * Hospital Practitioner
	 */
	_NR0090 ("NR0090", "Hospital Practitioner"),
	
	/**
	 * Clinical Assistant
	 */
	_NR0100 ("NR0100", "Clinical Assistant"),
	
	/**
	 * Specialist Registrar
	 */
	_NR0110 ("NR0110", "Specialist Registrar"),
	
	/**
	 * Senior Registrar Closed 
	 */
	_NR0120 ("NR0120", "Senior Registrar (Closed )"),
	
	/**
	 * Registrar Closed
	 */
	_NR0130 ("NR0130", "Registrar (Closed)"),
	
	/**
	 * Senior House Officer
	 */
	_NR0140 ("NR0140", "Senior House Officer"),
	
	/**
	 * House Officer - Pre Registration
	 */
	_NR0150 ("NR0150", "House Officer - Pre Registration"),
	
	/**
	 * House Officer - Post Registration
	 */
	_NR0160 ("NR0160", "House Officer - Post Registration"),
	
	/**
	 * Trust Grade Doctor - House Officer level
	 */
	_NR0170 ("NR0170", "Trust Grade Doctor - House Officer level"),
	
	/**
	 * Trust Grade Doctor - SHO level
	 */
	_NR0180 ("NR0180", "Trust Grade Doctor - SHO level"),
	
	/**
	 * Trust Grade Doctor - Specialist Registrar level
	 */
	_NR0190 ("NR0190", "Trust Grade Doctor - Specialist Registrar level"),
	
	/**
	 * Trust Grade Doctor - Career Grade level
	 */
	_NR0200 ("NR0200", "Trust Grade Doctor - Career Grade level"),
	
	/**
	 * Director of Public Health
	 */
	_NR0210 ("NR0210", "Director of Public Health"),
	
	/**
	 * Asst Clinical Medical Officer
	 */
	_NR0215 ("NR0215", "Asst. Clinical Medical Officer"),
	
	/**
	 * Clinical Medical Officer
	 */
	_NR0220 ("NR0220", "Clinical Medical Officer"),
	
	/**
	 * Senior Clinical Medical Officer
	 */
	_NR0230 ("NR0230", "Senior Clinical Medical Officer"),
	
	/**
	 * Other Community Health Service
	 */
	_NR0240 ("NR0240", "'Other' Community Health Service"),
	
	/**
	 * Other Community Health Service - Social Care Worker
	 */
	_NR0243 ("NR0243", "Other' Community Health Service - Social Care Worker"),
	
	/**
	 * Other Community Health Service - Admin Clerk
	 */
	_NR0247 ("NR0247", "Other' Community Health Service - Admin Clerk"),
	
	/**
	 * General Dental Practitioner
	 */
	_NR0250 ("NR0250", "General Dental Practitioner"),
	
	/**
	 * General Medical Practitioner
	 */
	_NR0260 ("NR0260", "General Medical Practitioner"),
	
	/**
	 * Salaried General Practitioner
	 */
	_NR0270 ("NR0270", "Salaried General Practitioner"),
	
	/**
	 * Regional Dental Officer
	 */
	_NR0280 ("NR0280", "Regional Dental Officer"),
	
	/**
	 * Dental Clinical Director - Dental
	 */
	_NR0290 ("NR0290", "Dental Clinical Director - Dental"),
	
	/**
	 * Dental Assistant Clinical Director
	 */
	_NR0295 ("NR0295", "Dental Assistant Clinical Director"),
	
	/**
	 * Dental Officer
	 */
	_NR0300 ("NR0300", "Dental Officer"),
	
	/**
	 * Senior Dental Officer
	 */
	_NR0310 ("NR0310", "Senior Dental Officer"),
	
	/**
	 * Salaried Dental Practitioner
	 */
	_NR0320 ("NR0320", "Salaried Dental Practitioner"),
	
	/**
	 * Student Nurse - Adult Branch
	 */
	_NR0330 ("NR0330", "Student Nurse - Adult Branch"),
	
	/**
	 * Student Nurse - Child Branch
	 */
	_NR0340 ("NR0340", "Student Nurse - Child Branch"),
	
	/**
	 * Student Nurse - Mental Health Branch
	 */
	_NR0350 ("NR0350", "Student Nurse - Mental Health Branch"),
	
	/**
	 * Student Nurse - Learning Disabilities Branch
	 */
	_NR0360 ("NR0360", "Student Nurse - Learning Disabilities Branch"),
	
	/**
	 * Student Midwife
	 */
	_NR0370 ("NR0370", "Student Midwife"),
	
	/**
	 * Student Health Visitor
	 */
	_NR0380 ("NR0380", "Student Health Visitor"),
	
	/**
	 * Student District Nurse
	 */
	_NR0390 ("NR0390", "Student District Nurse"),
	
	/**
	 * Student School Nurse
	 */
	_NR0400 ("NR0400", "Student School Nurse"),
	
	/**
	 * Student Practice Nurse
	 */
	_NR0410 ("NR0410", "Student Practice Nurse"),
	
	/**
	 * Student Occupational Health Nurse
	 */
	_NR0420 ("NR0420", "Student Occupational Health Nurse"),
	
	/**
	 * Student Community Paediatric Nurse
	 */
	_NR0430 ("NR0430", "Student Community Paediatric Nurse"),
	
	/**
	 * Student Community Mental Health Nurse
	 */
	_NR0440 ("NR0440", "Student Community Mental Health Nurse"),
	
	/**
	 * Student Community Learning Disabilities Nurse
	 */
	_NR0450 ("NR0450", "Student Community Learning Disabilities Nurse"),
	
	/**
	 * Student Chiropodist
	 */
	_NR0460 ("NR0460", "Student Chiropodist"),
	
	/**
	 * Student Dietitian
	 */
	_NR0470 ("NR0470", "Student Dietitian"),
	
	/**
	 * Student Occupational Therapist
	 */
	_NR0480 ("NR0480", "Student Occupational Therapist"),
	
	/**
	 * Student Orthoptist
	 */
	_NR0490 ("NR0490", "Student Orthoptist"),
	
	/**
	 * Student Physiotherapist
	 */
	_NR0500 ("NR0500", "Student Physiotherapist"),
	
	/**
	 * Student Radiographer - Diagnostic
	 */
	_NR0510 ("NR0510", "Student Radiographer - Diagnostic"),
	
	/**
	 * Student Radiographer - Therapeutic
	 */
	_NR0520 ("NR0520", "Student Radiographer - Therapeutic"),
	
	/**
	 * Student Speech  Language Therapist
	 */
	_NR0530 ("NR0530", "Student Speech & Language Therapist"),
	
	/**
	 * Art, Music  Drama Student
	 */
	_NR0540 ("NR0540", "Art, Music & Drama Student"),
	
	/**
	 * Student Psychotherapist
	 */
	_NR0550 ("NR0550", "Student Psychotherapist"),
	
	/**
	 * Director of Nursing
	 */
	_NR0560 ("NR0560", "Director of Nursing"),
	
	/**
	 * Nurse Consultant
	 */
	_NR0570 ("NR0570", "Nurse Consultant"),
	
	/**
	 * Nurse Manager
	 */
	_NR0580 ("NR0580", "Nurse Manager"),
	
	/**
	 * Modern Matron
	 */
	_NR0590 ("NR0590", "Modern Matron"),
	
	/**
	 * Specialist Nurse Practitioner
	 */
	_NR0600 ("NR0600", "Specialist Nurse Practitioner"),
	
	/**
	 * SisterCharge Nurse
	 */
	_NR0610 ("NR0610", "Sister/Charge Nurse"),
	
	/**
	 * Staff Nurse
	 */
	_NR0620 ("NR0620", "Staff Nurse"),
	
	/**
	 * Enrolled Nurse
	 */
	_NR0630 ("NR0630", "Enrolled Nurse"),
	
	/**
	 * Midwife - Consultant
	 */
	_NR0640 ("NR0640", "Midwife - Consultant"),
	
	/**
	 * Midwife - Specialist Practitioner
	 */
	_NR0650 ("NR0650", "Midwife - Specialist Practitioner"),
	
	/**
	 * Midwife - Manager
	 */
	_NR0660 ("NR0660", "Midwife - Manager"),
	
	/**
	 * Midwife - SisterCharge Nurse
	 */
	_NR0670 ("NR0670", "Midwife - Sister/Charge Nurse"),
	
	/**
	 * Midwife
	 */
	_NR0680 ("NR0680", "Midwife"),
	
	/**
	 * Community Practitioner
	 */
	_NR0690 ("NR0690", "Community Practitioner"),
	
	/**
	 * Community Nurse
	 */
	_NR0700 ("NR0700", "Community Nurse"),
	
	/**
	 * Art Therapist
	 */
	_NR0710 ("NR0710", "Art Therapist"),
	
	/**
	 * Art Therapist Consultant
	 */
	_NR0720 ("NR0720", "Art Therapist Consultant"),
	
	/**
	 * Art Therapist Manager
	 */
	_NR0730 ("NR0730", "Art Therapist Manager"),
	
	/**
	 * Art Therapist Specialist Practitioner
	 */
	_NR0740 ("NR0740", "Art Therapist Specialist Practitioner"),
	
	/**
	 * ChiropodistPodiatrist
	 */
	_NR0750 ("NR0750", "Chiropodist/Podiatrist"),
	
	/**
	 * ChiropodistPodiatrist Consultant
	 */
	_NR0760 ("NR0760", "Chiropodist/Podiatrist Consultant"),
	
	/**
	 * ChiropodistPodiatrist Manager
	 */
	_NR0770 ("NR0770", "Chiropodist/Podiatrist Manager"),
	
	/**
	 * ChiropodistPodiatrist Specialist Practitioner
	 */
	_NR0780 ("NR0780", "Chiropodist/Podiatrist Specialist Practitioner"),
	
	/**
	 * Dietitian
	 */
	_NR0790 ("NR0790", "Dietitian"),
	
	/**
	 * Dietitian Consultant
	 */
	_NR0800 ("NR0800", "Dietitian Consultant"),
	
	/**
	 * Dietitian Manager
	 */
	_NR0810 ("NR0810", "Dietitian Manager"),
	
	/**
	 * Dietitian Specialist Practitioner
	 */
	_NR0820 ("NR0820", "Dietitian Specialist Practitioner"),
	
	/**
	 * Drama Therapist
	 */
	_NR0830 ("NR0830", "Drama Therapist"),
	
	/**
	 * Drama Therapist Consultant
	 */
	_NR0840 ("NR0840", "Drama Therapist Consultant"),
	
	/**
	 * Drama Therapist Manager
	 */
	_NR0850 ("NR0850", "Drama Therapist Manager"),
	
	/**
	 * Drama Therapist Specialist Practitioner
	 */
	_NR0860 ("NR0860", "Drama Therapist Specialist Practitioner"),
	
	/**
	 * Multi Therapist
	 */
	_NR0870 ("NR0870", "Multi Therapist"),
	
	/**
	 * Multi Therapist Consultant
	 */
	_NR0880 ("NR0880", "Multi Therapist Consultant"),
	
	/**
	 * Multi Therapist Manager
	 */
	_NR0890 ("NR0890", "Multi Therapist Manager"),
	
	/**
	 * Multi Therapist Specialist Practitioner
	 */
	_NR0900 ("NR0900", "Multi Therapist Specialist Practitioner"),
	
	/**
	 * Music Therapist
	 */
	_NR0910 ("NR0910", "Music Therapist"),
	
	/**
	 * Music Therapist Consultant
	 */
	_NR0920 ("NR0920", "Music Therapist Consultant"),
	
	/**
	 * Music Therapist Manager
	 */
	_NR0930 ("NR0930", "Music Therapist Manager"),
	
	/**
	 * Music Therapist Specialist Practitioner
	 */
	_NR0940 ("NR0940", "Music Therapist Specialist Practitioner"),
	
	/**
	 * Occupational Therapist
	 */
	_NR0950 ("NR0950", "Occupational Therapist"),
	
	/**
	 * Speech  Language Therapist
	 */
	_NR0955 ("NR0955", "Speech & Language Therapist"),
	
	/**
	 * Occupational Therapist Consultant
	 */
	_NR0960 ("NR0960", "Occupational Therapist Consultant"),
	
	/**
	 * Speech  Language Therapist Consultant
	 */
	_NR0965 ("NR0965", "Speech & Language Therapist Consultant"),
	
	/**
	 * Occupational Therapist Manager
	 */
	_NR0970 ("NR0970", "Occupational Therapist Manager"),
	
	/**
	 * Speech  Language Therapist Manager
	 */
	_NR0975 ("NR0975", "Speech & Language Therapist Manager"),
	
	/**
	 * Occupational Therapy Specialist Practitioner
	 */
	_NR0980 ("NR0980", "Occupational Therapy Specialist Practitioner"),
	
	/**
	 * Speech  Language Therapist Specialist Practitioner
	 */
	_NR0985 ("NR0985", "Speech & Language Therapist Specialist Practitioner"),
	
	/**
	 * Orthoptist
	 */
	_NR0990 ("NR0990", "Orthoptist"),
	
	/**
	 * Orthoptist Consultant
	 */
	_NR1000 ("NR1000", "Orthoptist Consultant"),
	
	/**
	 * Orthoptist Manager
	 */
	_NR1010 ("NR1010", "Orthoptist Manager"),
	
	/**
	 * Orthoptist Specialist Practitioner
	 */
	_NR1020 ("NR1020", "Orthoptist Specialist Practitioner"),
	
	/**
	 * Orthotist
	 */
	_NR1030 ("NR1030", "Orthotist"),
	
	/**
	 * Orthotist Consultant
	 */
	_NR1040 ("NR1040", "Orthotist Consultant"),
	
	/**
	 * Orthotist Manager
	 */
	_NR1050 ("NR1050", "Orthotist Manager"),
	
	/**
	 * Orthotist Specialist Practitioner
	 */
	_NR1060 ("NR1060", "Orthotist Specialist Practitioner"),
	
	/**
	 * Paramedic
	 */
	_NR1070 ("NR1070", "Paramedic"),
	
	/**
	 * Paramedic Consultant
	 */
	_NR1080 ("NR1080", "Paramedic Consultant"),
	
	/**
	 * Paramedic Manager
	 */
	_NR1090 ("NR1090", "Paramedic Manager"),
	
	/**
	 * Paramedic Specialist Practitioner
	 */
	_NR1100 ("NR1100", "Paramedic Specialist Practitioner"),
	
	/**
	 * Physiotherapist
	 */
	_NR1110 ("NR1110", "Physiotherapist"),
	
	/**
	 * Physiotherapist Consultant
	 */
	_NR1120 ("NR1120", "Physiotherapist Consultant"),
	
	/**
	 * Physiotherapist Manager
	 */
	_NR1130 ("NR1130", "Physiotherapist Manager"),
	
	/**
	 * Physiotherapist Specialist Practitioner
	 */
	_NR1140 ("NR1140", "Physiotherapist Specialist Practitioner"),
	
	/**
	 * Prosthetist
	 */
	_NR1150 ("NR1150", "Prosthetist"),
	
	/**
	 * Prosthetist Consultant
	 */
	_NR1160 ("NR1160", "Prosthetist Consultant"),
	
	/**
	 * Prosthetist Manager
	 */
	_NR1170 ("NR1170", "Prosthetist Manager"),
	
	/**
	 * Prosthetist Specialist Practitioner
	 */
	_NR1180 ("NR1180", "Prosthetist Specialist Practitioner"),
	
	/**
	 * Radiographer - Diagnostic
	 */
	_NR1190 ("NR1190", "Radiographer - Diagnostic"),
	
	/**
	 * Radiographer - Diagnostic, Consultant
	 */
	_NR1200 ("NR1200", "Radiographer - Diagnostic, Consultant"),
	
	/**
	 * Radiographer - Diagnostic, Manager
	 */
	_NR1210 ("NR1210", "Radiographer - Diagnostic, Manager"),
	
	/**
	 * Radiographer - Diagnostic, Specialist Practitioner
	 */
	_NR1220 ("NR1220", "Radiographer - Diagnostic, Specialist Practitioner"),
	
	/**
	 * Radiographer - Therapeutic
	 */
	_NR1230 ("NR1230", "Radiographer - Therapeutic"),
	
	/**
	 * Radiographer - Therapeutic, Consultant
	 */
	_NR1240 ("NR1240", "Radiographer - Therapeutic, Consultant"),
	
	/**
	 * Radiographer - Therapeutic, Manager
	 */
	_NR1250 ("NR1250", "Radiographer - Therapeutic, Manager"),
	
	/**
	 * Radiographer - Therapeutic, Specialist Practitioner
	 */
	_NR1260 ("NR1260", "Radiographer - Therapeutic, Specialist Practitioner"),
	
	/**
	 * Clinical Director
	 */
	_NR1270 ("NR1270", "Clinical Director"),
	
	/**
	 * Optometrist
	 */
	_NR1280 ("NR1280", "Optometrist"),
	
	/**
	 * Pharmacist
	 */
	_NR1290 ("NR1290", "Pharmacist"),
	
	/**
	 * Psychotherapist
	 */
	_NR1300 ("NR1300", "Psychotherapist"),
	
	/**
	 * Clinical Psychologist
	 */
	_NR1310 ("NR1310", "Clinical Psychologist"),
	
	/**
	 * Chaplain
	 */
	_NR1320 ("NR1320", "Chaplain"),
	
	/**
	 * Social Worker
	 */
	_NR1330 ("NR1330", "Social Worker"),
	
	/**
	 * Approved Social Worker
	 */
	_NR1340 ("NR1340", "Approved Social Worker"),
	
	/**
	 * Youth Worker
	 */
	_NR1350 ("NR1350", "Youth Worker"),
	
	/**
	 * Specialist Practitioner
	 */
	_NR1360 ("NR1360", "Specialist Practitioner"),
	
	/**
	 * Practitioner
	 */
	_NR1370 ("NR1370", "Practitioner"),
	
	/**
	 * Technician - PST
	 */
	_NR1380 ("NR1380", "Technician - PS&T"),
	
	/**
	 * Osteopath
	 */
	_NR1390 ("NR1390", "Osteopath"),
	
	/**
	 * Healthcare Scientist
	 */
	_NR1400 ("NR1400", "Healthcare Scientist"),
	
	/**
	 * Consultant Healthcare Scientist
	 */
	_NR1410 ("NR1410", "Consultant Healthcare Scientist"),
	
	/**
	 * Biomedical Scientist
	 */
	_NR1420 ("NR1420", "Biomedical Scientist"),
	
	/**
	 * Technician - Healthcare Scientists
	 */
	_NR1430 ("NR1430", "Technician - Healthcare Scientists"),
	
	/**
	 * Therapist
	 */
	_NR1440 ("NR1440", "Therapist"),
	
	/**
	 * Health Care Support Worker
	 */
	_NR1450 ("NR1450", "Health Care Support Worker"),
	
	/**
	 * Social Care Support Worker
	 */
	_NR1460 ("NR1460", "Social Care Support Worker"),
	
	/**
	 * Home Help
	 */
	_NR1470 ("NR1470", "Home Help"),
	
	/**
	 * Healthcare Assistant
	 */
	_NR1480 ("NR1480", "Healthcare Assistant"),
	
	/**
	 * Nursery Nurse
	 */
	_NR1490 ("NR1490", "Nursery Nurse"),
	
	/**
	 * Play Therapist
	 */
	_NR1500 ("NR1500", "Play Therapist"),
	
	/**
	 * Play Specialist
	 */
	_NR1510 ("NR1510", "Play Specialist"),
	
	/**
	 * Technician - Addl Clinical Services
	 */
	_NR1520 ("NR1520", "Technician - Add'l Clinical Services"),
	
	/**
	 * Technical Instructor
	 */
	_NR1530 ("NR1530", "Technical Instructor"),
	
	/**
	 * Associate Practitioner
	 */
	_NR1540 ("NR1540", "Associate Practitioner"),
	
	/**
	 * Associate Practitioner - Nurse
	 */
	_NR1543 ("NR1543", "Associate Practitioner - Nurse"),
	
	/**
	 * Associate Practitioner - General Practitioner
	 */
	_NR1547 ("NR1547", "Associate Practitioner - General Practitioner"),
	
	/**
	 * Counsellor
	 */
	_NR1550 ("NR1550", "Counsellor"),
	
	/**
	 * HelperAssistant
	 */
	_NR1560 ("NR1560", "Helper/Assistant"),
	
	/**
	 * Dental Surgery Assistant
	 */
	_NR1570 ("NR1570", "Dental Surgery Assistant"),
	
	/**
	 * Medical Laboratory Assistant
	 */
	_NR1580 ("NR1580", "Medical Laboratory Assistant"),
	
	/**
	 * Phlebotomist
	 */
	_NR1590 ("NR1590", "Phlebotomist"),
	
	/**
	 * Cytoscreener
	 */
	_NR1600 ("NR1600", "Cytoscreener"),
	
	/**
	 * Student Technician
	 */
	_NR1610 ("NR1610", "Student Technician"),
	
	/**
	 * Trainee Scientist
	 */
	_NR1620 ("NR1620", "Trainee Scientist"),
	
	/**
	 * Trainee Practitioner
	 */
	_NR1630 ("NR1630", "Trainee Practitioner"),
	
	/**
	 * Nursing Cadet
	 */
	_NR1640 ("NR1640", "Nursing Cadet"),
	
	/**
	 * Healthcare Cadet
	 */
	_NR1650 ("NR1650", "Healthcare Cadet"),
	
	/**
	 * Pre-reg Pharmacist
	 */
	_NR1660 ("NR1660", "Pre-reg Pharmacist"),
	
	/**
	 * Assistant Psychologist
	 */
	_NR1670 ("NR1670", "Assistant Psychologist"),
	
	/**
	 * Assistant Psychotherapist
	 */
	_NR1680 ("NR1680", "Assistant Psychotherapist"),
	
	/**
	 * Call Operator
	 */
	_NR1690 ("NR1690", "Call Operator"),
	
	/**
	 * Gateway Worker
	 */
	_NR1700 ("NR1700", "Gateway Worker"),
	
	/**
	 * Support, Time, Recovery Worker
	 */
	_NR1710 ("NR1710", "Support, Time, Recovery Worker"),
	
	/**
	 * Clerical Worker
	 */
	_NR1720 ("NR1720", "Clerical Worker"),
	
	/**
	 * Receptionist
	 */
	_NR1730 ("NR1730", "Receptionist"),
	
	/**
	 * Secretary
	 */
	_NR1740 ("NR1740", "Secretary"),
	
	/**
	 * Personal Assistant
	 */
	_NR1750 ("NR1750", "Personal Assistant"),
	
	/**
	 * Medical Secretary
	 */
	_NR1760 ("NR1760", "Medical Secretary"),
	
	/**
	 * Officer
	 */
	_NR1770 ("NR1770", "Officer"),
	
	/**
	 * Manager
	 */
	_NR1780 ("NR1780", "Manager"),
	
	/**
	 * Senior Manager
	 */
	_NR1790 ("NR1790", "Senior Manager"),
	
	/**
	 * Technician - Admin  Clerical
	 */
	_NR1800 ("NR1800", "Technician - Admin & Clerical"),
	
	/**
	 * Accountant
	 */
	_NR1810 ("NR1810", "Accountant"),
	
	/**
	 * Librarian
	 */
	_NR1820 ("NR1820", "Librarian"),
	
	/**
	 * Interpreter
	 */
	_NR1830 ("NR1830", "Interpreter"),
	
	/**
	 * Analyst
	 */
	_NR1840 ("NR1840", "Analyst"),
	
	/**
	 * Adviser
	 */
	_NR1850 ("NR1850", "Adviser"),
	
	/**
	 * Researcher
	 */
	_NR1860 ("NR1860", "Researcher"),
	
	/**
	 * Control Assistant
	 */
	_NR1870 ("NR1870", "Control Assistant"),
	
	/**
	 * Architect
	 */
	_NR1880 ("NR1880", "Architect"),
	
	/**
	 * Lawyer
	 */
	_NR1890 ("NR1890", "Lawyer"),
	
	/**
	 * Surveyor
	 */
	_NR1900 ("NR1900", "Surveyor"),
	
	/**
	 * Chair
	 */
	_NR1910 ("NR1910", "Chair"),
	
	/**
	 * Chief Executive
	 */
	_NR1920 ("NR1920", "Chief Executive"),
	
	/**
	 * Finance Director
	 */
	_NR1930 ("NR1930", "Finance Director"),
	
	/**
	 * Other Executive Director
	 */
	_NR1940 ("NR1940", "Other Executive Director"),
	
	/**
	 * Board Level Director
	 */
	_NR1950 ("NR1950", "Board Level Director"),
	
	/**
	 * Non Executive Director
	 */
	_NR1960 ("NR1960", "Non Executive Director"),
	
	/**
	 * Childcare Co-ordinator
	 */
	_NR1970 ("NR1970", "Childcare Co-ordinator"),
	
	/**
	 * Main Informal Carer
	 */
	_NR1980 ("NR1980", "Main Informal Carer"),
	
	/**
	 * Formal Carer
	 */
	_NR1990 ("NR1990", "Formal Carer"),
	
	/**
	 * Additional person to be involved in decisions
	 */
	_NR2000 ("NR2000", "Additional person to be involved in decisions"),
	
	/**
	 * LPA with authority
	 */
	_NR2010 ("NR2010", "LPA with authority"),
	
	/**
	 * Key Worker
	 */
	_NR2020 ("NR2020", "Key Worker"),
	
	/**
	 * Senior Responsible Clinician
	 */
	_NR2030 ("NR2030", "Senior Responsible Clinician"),
	
	/**
	 * Palliative Care Physician
	 */
	_NR2040 ("NR2040", "Palliative Care Physician"),
	
	/**
	 * Specialist Palliative Care Nurse
	 */
	_NR2050 ("NR2050", "Specialist Palliative Care Nurse"),
	
	/**
	 * Discharge Coordinator
	 */
	_NR2060 ("NR2060", "Discharge Coordinator"),
	
	/**
	 * Medical Director
	 */
	_MedicalDirector ("NR0010", "Medical Director"),
	
	/**
	 * Clinical Director - Medical
	 */
	_ClinicalDirectorMedical ("NR0020", "Clinical Director - Medical"),
	
	/**
	 * Professor
	 */
	_Professor ("NR0030", "Professor"),
	
	/**
	 * Senior Lecturer
	 */
	_SeniorLecturer ("NR0040", "Senior Lecturer"),
	
	/**
	 * Consultant
	 */
	_Consultant ("NR0050", "Consultant"),
	
	/**
	 * Dental surgeon acting as Hospital Consultant
	 */
	_DentalsurgeonactingasHospitalConsultant ("NR0055", "Dental surgeon acting as Hospital Consultant"),
	
	/**
	 * Special salary scale in Public Health Medicine
	 */
	_SpecialsalaryscaleinPublicHealthMedicine ("NR0060", "Special salary scale in Public Health Medicine"),
	
	/**
	 * Associate Specialist
	 */
	_AssociateSpecialist ("NR0070", "Associate Specialist"),
	
	/**
	 * Staff Grade
	 */
	_StaffGrade ("NR0080", "Staff Grade"),
	
	/**
	 * Hospital Practitioner
	 */
	_HospitalPractitioner ("NR0090", "Hospital Practitioner"),
	
	/**
	 * Clinical Assistant
	 */
	_ClinicalAssistant ("NR0100", "Clinical Assistant"),
	
	/**
	 * Specialist Registrar
	 */
	_SpecialistRegistrar ("NR0110", "Specialist Registrar"),
	
	/**
	 * Senior Registrar Closed 
	 */
	_SeniorRegistrarClosed ("NR0120", "Senior Registrar (Closed )"),
	
	/**
	 * Registrar Closed
	 */
	_RegistrarClosed ("NR0130", "Registrar (Closed)"),
	
	/**
	 * Senior House Officer
	 */
	_SeniorHouseOfficer ("NR0140", "Senior House Officer"),
	
	/**
	 * House Officer - Pre Registration
	 */
	_HouseOfficerPreRegistration ("NR0150", "House Officer - Pre Registration"),
	
	/**
	 * House Officer - Post Registration
	 */
	_HouseOfficerPostRegistration ("NR0160", "House Officer - Post Registration"),
	
	/**
	 * Trust Grade Doctor - House Officer level
	 */
	_TrustGradeDoctorHouseOfficerlevel ("NR0170", "Trust Grade Doctor - House Officer level"),
	
	/**
	 * Trust Grade Doctor - SHO level
	 */
	_TrustGradeDoctorSHOlevel ("NR0180", "Trust Grade Doctor - SHO level"),
	
	/**
	 * Trust Grade Doctor - Specialist Registrar level
	 */
	_TrustGradeDoctorSpecialistRegistrarlevel ("NR0190", "Trust Grade Doctor - Specialist Registrar level"),
	
	/**
	 * Trust Grade Doctor - Career Grade level
	 */
	_TrustGradeDoctorCareerGradelevel ("NR0200", "Trust Grade Doctor - Career Grade level"),
	
	/**
	 * Director of Public Health
	 */
	_DirectorofPublicHealth ("NR0210", "Director of Public Health"),
	
	/**
	 * Asst Clinical Medical Officer
	 */
	_AsstClinicalMedicalOfficer ("NR0215", "Asst. Clinical Medical Officer"),
	
	/**
	 * Clinical Medical Officer
	 */
	_ClinicalMedicalOfficer ("NR0220", "Clinical Medical Officer"),
	
	/**
	 * Senior Clinical Medical Officer
	 */
	_SeniorClinicalMedicalOfficer ("NR0230", "Senior Clinical Medical Officer"),
	
	/**
	 * Other Community Health Service
	 */
	_OtherCommunityHealthService ("NR0240", "'Other' Community Health Service"),
	
	/**
	 * Other Community Health Service - Social Care Worker
	 */
	_OtherCommunityHealthServiceSocialCareWorker ("NR0243", "Other' Community Health Service - Social Care Worker"),
	
	/**
	 * Other Community Health Service - Admin Clerk
	 */
	_OtherCommunityHealthServiceAdminClerk ("NR0247", "Other' Community Health Service - Admin Clerk"),
	
	/**
	 * General Dental Practitioner
	 */
	_GeneralDentalPractitioner ("NR0250", "General Dental Practitioner"),
	
	/**
	 * General Medical Practitioner
	 */
	_GeneralMedicalPractitioner ("NR0260", "General Medical Practitioner"),
	
	/**
	 * Salaried General Practitioner
	 */
	_SalariedGeneralPractitioner ("NR0270", "Salaried General Practitioner"),
	
	/**
	 * Regional Dental Officer
	 */
	_RegionalDentalOfficer ("NR0280", "Regional Dental Officer"),
	
	/**
	 * Dental Clinical Director - Dental
	 */
	_DentalClinicalDirectorDental ("NR0290", "Dental Clinical Director - Dental"),
	
	/**
	 * Dental Assistant Clinical Director
	 */
	_DentalAssistantClinicalDirector ("NR0295", "Dental Assistant Clinical Director"),
	
	/**
	 * Dental Officer
	 */
	_DentalOfficer ("NR0300", "Dental Officer"),
	
	/**
	 * Senior Dental Officer
	 */
	_SeniorDentalOfficer ("NR0310", "Senior Dental Officer"),
	
	/**
	 * Salaried Dental Practitioner
	 */
	_SalariedDentalPractitioner ("NR0320", "Salaried Dental Practitioner"),
	
	/**
	 * Student Nurse - Adult Branch
	 */
	_StudentNurseAdultBranch ("NR0330", "Student Nurse - Adult Branch"),
	
	/**
	 * Student Nurse - Child Branch
	 */
	_StudentNurseChildBranch ("NR0340", "Student Nurse - Child Branch"),
	
	/**
	 * Student Nurse - Mental Health Branch
	 */
	_StudentNurseMentalHealthBranch ("NR0350", "Student Nurse - Mental Health Branch"),
	
	/**
	 * Student Nurse - Learning Disabilities Branch
	 */
	_StudentNurseLearningDisabilitiesBranch ("NR0360", "Student Nurse - Learning Disabilities Branch"),
	
	/**
	 * Student Midwife
	 */
	_StudentMidwife ("NR0370", "Student Midwife"),
	
	/**
	 * Student Health Visitor
	 */
	_StudentHealthVisitor ("NR0380", "Student Health Visitor"),
	
	/**
	 * Student District Nurse
	 */
	_StudentDistrictNurse ("NR0390", "Student District Nurse"),
	
	/**
	 * Student School Nurse
	 */
	_StudentSchoolNurse ("NR0400", "Student School Nurse"),
	
	/**
	 * Student Practice Nurse
	 */
	_StudentPracticeNurse ("NR0410", "Student Practice Nurse"),
	
	/**
	 * Student Occupational Health Nurse
	 */
	_StudentOccupationalHealthNurse ("NR0420", "Student Occupational Health Nurse"),
	
	/**
	 * Student Community Paediatric Nurse
	 */
	_StudentCommunityPaediatricNurse ("NR0430", "Student Community Paediatric Nurse"),
	
	/**
	 * Student Community Mental Health Nurse
	 */
	_StudentCommunityMentalHealthNurse ("NR0440", "Student Community Mental Health Nurse"),
	
	/**
	 * Student Community Learning Disabilities Nurse
	 */
	_StudentCommunityLearningDisabilitiesNurse ("NR0450", "Student Community Learning Disabilities Nurse"),
	
	/**
	 * Student Chiropodist
	 */
	_StudentChiropodist ("NR0460", "Student Chiropodist"),
	
	/**
	 * Student Dietitian
	 */
	_StudentDietitian ("NR0470", "Student Dietitian"),
	
	/**
	 * Student Occupational Therapist
	 */
	_StudentOccupationalTherapist ("NR0480", "Student Occupational Therapist"),
	
	/**
	 * Student Orthoptist
	 */
	_StudentOrthoptist ("NR0490", "Student Orthoptist"),
	
	/**
	 * Student Physiotherapist
	 */
	_StudentPhysiotherapist ("NR0500", "Student Physiotherapist"),
	
	/**
	 * Student Radiographer - Diagnostic
	 */
	_StudentRadiographerDiagnostic ("NR0510", "Student Radiographer - Diagnostic"),
	
	/**
	 * Student Radiographer - Therapeutic
	 */
	_StudentRadiographerTherapeutic ("NR0520", "Student Radiographer - Therapeutic"),
	
	/**
	 * Student Speech  Language Therapist
	 */
	_StudentSpeechLanguageTherapist ("NR0530", "Student Speech & Language Therapist"),
	
	/**
	 * Art, Music  Drama Student
	 */
	_ArtMusicDramaStudent ("NR0540", "Art, Music & Drama Student"),
	
	/**
	 * Student Psychotherapist
	 */
	_StudentPsychotherapist ("NR0550", "Student Psychotherapist"),
	
	/**
	 * Director of Nursing
	 */
	_DirectorofNursing ("NR0560", "Director of Nursing"),
	
	/**
	 * Nurse Consultant
	 */
	_NurseConsultant ("NR0570", "Nurse Consultant"),
	
	/**
	 * Nurse Manager
	 */
	_NurseManager ("NR0580", "Nurse Manager"),
	
	/**
	 * Modern Matron
	 */
	_ModernMatron ("NR0590", "Modern Matron"),
	
	/**
	 * Specialist Nurse Practitioner
	 */
	_SpecialistNursePractitioner ("NR0600", "Specialist Nurse Practitioner"),
	
	/**
	 * SisterCharge Nurse
	 */
	_SisterChargeNurse ("NR0610", "Sister/Charge Nurse"),
	
	/**
	 * Staff Nurse
	 */
	_StaffNurse ("NR0620", "Staff Nurse"),
	
	/**
	 * Enrolled Nurse
	 */
	_EnrolledNurse ("NR0630", "Enrolled Nurse"),
	
	/**
	 * Midwife - Consultant
	 */
	_MidwifeConsultant ("NR0640", "Midwife - Consultant"),
	
	/**
	 * Midwife - Specialist Practitioner
	 */
	_MidwifeSpecialistPractitioner ("NR0650", "Midwife - Specialist Practitioner"),
	
	/**
	 * Midwife - Manager
	 */
	_MidwifeManager ("NR0660", "Midwife - Manager"),
	
	/**
	 * Midwife - SisterCharge Nurse
	 */
	_MidwifeSisterChargeNurse ("NR0670", "Midwife - Sister/Charge Nurse"),
	
	/**
	 * Midwife
	 */
	_Midwife ("NR0680", "Midwife"),
	
	/**
	 * Community Practitioner
	 */
	_CommunityPractitioner ("NR0690", "Community Practitioner"),
	
	/**
	 * Community Nurse
	 */
	_CommunityNurse ("NR0700", "Community Nurse"),
	
	/**
	 * Art Therapist
	 */
	_ArtTherapist ("NR0710", "Art Therapist"),
	
	/**
	 * Art Therapist Consultant
	 */
	_ArtTherapistConsultant ("NR0720", "Art Therapist Consultant"),
	
	/**
	 * Art Therapist Manager
	 */
	_ArtTherapistManager ("NR0730", "Art Therapist Manager"),
	
	/**
	 * Art Therapist Specialist Practitioner
	 */
	_ArtTherapistSpecialistPractitioner ("NR0740", "Art Therapist Specialist Practitioner"),
	
	/**
	 * ChiropodistPodiatrist
	 */
	_ChiropodistPodiatrist ("NR0750", "Chiropodist/Podiatrist"),
	
	/**
	 * ChiropodistPodiatrist Consultant
	 */
	_ChiropodistPodiatristConsultant ("NR0760", "Chiropodist/Podiatrist Consultant"),
	
	/**
	 * ChiropodistPodiatrist Manager
	 */
	_ChiropodistPodiatristManager ("NR0770", "Chiropodist/Podiatrist Manager"),
	
	/**
	 * ChiropodistPodiatrist Specialist Practitioner
	 */
	_ChiropodistPodiatristSpecialistPractitioner ("NR0780", "Chiropodist/Podiatrist Specialist Practitioner"),
	
	/**
	 * Dietitian
	 */
	_Dietitian ("NR0790", "Dietitian"),
	
	/**
	 * Dietitian Consultant
	 */
	_DietitianConsultant ("NR0800", "Dietitian Consultant"),
	
	/**
	 * Dietitian Manager
	 */
	_DietitianManager ("NR0810", "Dietitian Manager"),
	
	/**
	 * Dietitian Specialist Practitioner
	 */
	_DietitianSpecialistPractitioner ("NR0820", "Dietitian Specialist Practitioner"),
	
	/**
	 * Drama Therapist
	 */
	_DramaTherapist ("NR0830", "Drama Therapist"),
	
	/**
	 * Drama Therapist Consultant
	 */
	_DramaTherapistConsultant ("NR0840", "Drama Therapist Consultant"),
	
	/**
	 * Drama Therapist Manager
	 */
	_DramaTherapistManager ("NR0850", "Drama Therapist Manager"),
	
	/**
	 * Drama Therapist Specialist Practitioner
	 */
	_DramaTherapistSpecialistPractitioner ("NR0860", "Drama Therapist Specialist Practitioner"),
	
	/**
	 * Multi Therapist
	 */
	_MultiTherapist ("NR0870", "Multi Therapist"),
	
	/**
	 * Multi Therapist Consultant
	 */
	_MultiTherapistConsultant ("NR0880", "Multi Therapist Consultant"),
	
	/**
	 * Multi Therapist Manager
	 */
	_MultiTherapistManager ("NR0890", "Multi Therapist Manager"),
	
	/**
	 * Multi Therapist Specialist Practitioner
	 */
	_MultiTherapistSpecialistPractitioner ("NR0900", "Multi Therapist Specialist Practitioner"),
	
	/**
	 * Music Therapist
	 */
	_MusicTherapist ("NR0910", "Music Therapist"),
	
	/**
	 * Music Therapist Consultant
	 */
	_MusicTherapistConsultant ("NR0920", "Music Therapist Consultant"),
	
	/**
	 * Music Therapist Manager
	 */
	_MusicTherapistManager ("NR0930", "Music Therapist Manager"),
	
	/**
	 * Music Therapist Specialist Practitioner
	 */
	_MusicTherapistSpecialistPractitioner ("NR0940", "Music Therapist Specialist Practitioner"),
	
	/**
	 * Occupational Therapist
	 */
	_OccupationalTherapist ("NR0950", "Occupational Therapist"),
	
	/**
	 * Speech  Language Therapist
	 */
	_SpeechLanguageTherapist ("NR0955", "Speech & Language Therapist"),
	
	/**
	 * Occupational Therapist Consultant
	 */
	_OccupationalTherapistConsultant ("NR0960", "Occupational Therapist Consultant"),
	
	/**
	 * Speech  Language Therapist Consultant
	 */
	_SpeechLanguageTherapistConsultant ("NR0965", "Speech & Language Therapist Consultant"),
	
	/**
	 * Occupational Therapist Manager
	 */
	_OccupationalTherapistManager ("NR0970", "Occupational Therapist Manager"),
	
	/**
	 * Speech  Language Therapist Manager
	 */
	_SpeechLanguageTherapistManager ("NR0975", "Speech & Language Therapist Manager"),
	
	/**
	 * Occupational Therapy Specialist Practitioner
	 */
	_OccupationalTherapySpecialistPractitioner ("NR0980", "Occupational Therapy Specialist Practitioner"),
	
	/**
	 * Speech  Language Therapist Specialist Practitioner
	 */
	_SpeechLanguageTherapistSpecialistPractitioner ("NR0985", "Speech & Language Therapist Specialist Practitioner"),
	
	/**
	 * Orthoptist
	 */
	_Orthoptist ("NR0990", "Orthoptist"),
	
	/**
	 * Orthoptist Consultant
	 */
	_OrthoptistConsultant ("NR1000", "Orthoptist Consultant"),
	
	/**
	 * Orthoptist Manager
	 */
	_OrthoptistManager ("NR1010", "Orthoptist Manager"),
	
	/**
	 * Orthoptist Specialist Practitioner
	 */
	_OrthoptistSpecialistPractitioner ("NR1020", "Orthoptist Specialist Practitioner"),
	
	/**
	 * Orthotist
	 */
	_Orthotist ("NR1030", "Orthotist"),
	
	/**
	 * Orthotist Consultant
	 */
	_OrthotistConsultant ("NR1040", "Orthotist Consultant"),
	
	/**
	 * Orthotist Manager
	 */
	_OrthotistManager ("NR1050", "Orthotist Manager"),
	
	/**
	 * Orthotist Specialist Practitioner
	 */
	_OrthotistSpecialistPractitioner ("NR1060", "Orthotist Specialist Practitioner"),
	
	/**
	 * Paramedic
	 */
	_Paramedic ("NR1070", "Paramedic"),
	
	/**
	 * Paramedic Consultant
	 */
	_ParamedicConsultant ("NR1080", "Paramedic Consultant"),
	
	/**
	 * Paramedic Manager
	 */
	_ParamedicManager ("NR1090", "Paramedic Manager"),
	
	/**
	 * Paramedic Specialist Practitioner
	 */
	_ParamedicSpecialistPractitioner ("NR1100", "Paramedic Specialist Practitioner"),
	
	/**
	 * Physiotherapist
	 */
	_Physiotherapist ("NR1110", "Physiotherapist"),
	
	/**
	 * Physiotherapist Consultant
	 */
	_PhysiotherapistConsultant ("NR1120", "Physiotherapist Consultant"),
	
	/**
	 * Physiotherapist Manager
	 */
	_PhysiotherapistManager ("NR1130", "Physiotherapist Manager"),
	
	/**
	 * Physiotherapist Specialist Practitioner
	 */
	_PhysiotherapistSpecialistPractitioner ("NR1140", "Physiotherapist Specialist Practitioner"),
	
	/**
	 * Prosthetist
	 */
	_Prosthetist ("NR1150", "Prosthetist"),
	
	/**
	 * Prosthetist Consultant
	 */
	_ProsthetistConsultant ("NR1160", "Prosthetist Consultant"),
	
	/**
	 * Prosthetist Manager
	 */
	_ProsthetistManager ("NR1170", "Prosthetist Manager"),
	
	/**
	 * Prosthetist Specialist Practitioner
	 */
	_ProsthetistSpecialistPractitioner ("NR1180", "Prosthetist Specialist Practitioner"),
	
	/**
	 * Radiographer - Diagnostic
	 */
	_RadiographerDiagnostic ("NR1190", "Radiographer - Diagnostic"),
	
	/**
	 * Radiographer - Diagnostic, Consultant
	 */
	_RadiographerDiagnosticConsultant ("NR1200", "Radiographer - Diagnostic, Consultant"),
	
	/**
	 * Radiographer - Diagnostic, Manager
	 */
	_RadiographerDiagnosticManager ("NR1210", "Radiographer - Diagnostic, Manager"),
	
	/**
	 * Radiographer - Diagnostic, Specialist Practitioner
	 */
	_RadiographerDiagnosticSpecialistPractitioner ("NR1220", "Radiographer - Diagnostic, Specialist Practitioner"),
	
	/**
	 * Radiographer - Therapeutic
	 */
	_RadiographerTherapeutic ("NR1230", "Radiographer - Therapeutic"),
	
	/**
	 * Radiographer - Therapeutic, Consultant
	 */
	_RadiographerTherapeuticConsultant ("NR1240", "Radiographer - Therapeutic, Consultant"),
	
	/**
	 * Radiographer - Therapeutic, Manager
	 */
	_RadiographerTherapeuticManager ("NR1250", "Radiographer - Therapeutic, Manager"),
	
	/**
	 * Radiographer - Therapeutic, Specialist Practitioner
	 */
	_RadiographerTherapeuticSpecialistPractitioner ("NR1260", "Radiographer - Therapeutic, Specialist Practitioner"),
	
	/**
	 * Clinical Director
	 */
	_ClinicalDirector ("NR1270", "Clinical Director"),
	
	/**
	 * Optometrist
	 */
	_Optometrist ("NR1280", "Optometrist"),
	
	/**
	 * Pharmacist
	 */
	_Pharmacist ("NR1290", "Pharmacist"),
	
	/**
	 * Psychotherapist
	 */
	_Psychotherapist ("NR1300", "Psychotherapist"),
	
	/**
	 * Clinical Psychologist
	 */
	_ClinicalPsychologist ("NR1310", "Clinical Psychologist"),
	
	/**
	 * Chaplain
	 */
	_Chaplain ("NR1320", "Chaplain"),
	
	/**
	 * Social Worker
	 */
	_SocialWorker ("NR1330", "Social Worker"),
	
	/**
	 * Approved Social Worker
	 */
	_ApprovedSocialWorker ("NR1340", "Approved Social Worker"),
	
	/**
	 * Youth Worker
	 */
	_YouthWorker ("NR1350", "Youth Worker"),
	
	/**
	 * Specialist Practitioner
	 */
	_SpecialistPractitioner ("NR1360", "Specialist Practitioner"),
	
	/**
	 * Practitioner
	 */
	_Practitioner ("NR1370", "Practitioner"),
	
	/**
	 * Technician - PST
	 */
	_TechnicianPST ("NR1380", "Technician - PS&T"),
	
	/**
	 * Osteopath
	 */
	_Osteopath ("NR1390", "Osteopath"),
	
	/**
	 * Healthcare Scientist
	 */
	_HealthcareScientist ("NR1400", "Healthcare Scientist"),
	
	/**
	 * Consultant Healthcare Scientist
	 */
	_ConsultantHealthcareScientist ("NR1410", "Consultant Healthcare Scientist"),
	
	/**
	 * Biomedical Scientist
	 */
	_BiomedicalScientist ("NR1420", "Biomedical Scientist"),
	
	/**
	 * Technician - Healthcare Scientists
	 */
	_TechnicianHealthcareScientists ("NR1430", "Technician - Healthcare Scientists"),
	
	/**
	 * Therapist
	 */
	_Therapist ("NR1440", "Therapist"),
	
	/**
	 * Health Care Support Worker
	 */
	_HealthCareSupportWorker ("NR1450", "Health Care Support Worker"),
	
	/**
	 * Social Care Support Worker
	 */
	_SocialCareSupportWorker ("NR1460", "Social Care Support Worker"),
	
	/**
	 * Home Help
	 */
	_HomeHelp ("NR1470", "Home Help"),
	
	/**
	 * Healthcare Assistant
	 */
	_HealthcareAssistant ("NR1480", "Healthcare Assistant"),
	
	/**
	 * Nursery Nurse
	 */
	_NurseryNurse ("NR1490", "Nursery Nurse"),
	
	/**
	 * Play Therapist
	 */
	_PlayTherapist ("NR1500", "Play Therapist"),
	
	/**
	 * Play Specialist
	 */
	_PlaySpecialist ("NR1510", "Play Specialist"),
	
	/**
	 * Technician - Addl Clinical Services
	 */
	_TechnicianAddlClinicalServices ("NR1520", "Technician - Add'l Clinical Services"),
	
	/**
	 * Technical Instructor
	 */
	_TechnicalInstructor ("NR1530", "Technical Instructor"),
	
	/**
	 * Associate Practitioner
	 */
	_AssociatePractitioner ("NR1540", "Associate Practitioner"),
	
	/**
	 * Associate Practitioner - Nurse
	 */
	_AssociatePractitionerNurse ("NR1543", "Associate Practitioner - Nurse"),
	
	/**
	 * Associate Practitioner - General Practitioner
	 */
	_AssociatePractitionerGeneralPractitioner ("NR1547", "Associate Practitioner - General Practitioner"),
	
	/**
	 * Counsellor
	 */
	_Counsellor ("NR1550", "Counsellor"),
	
	/**
	 * HelperAssistant
	 */
	_HelperAssistant ("NR1560", "Helper/Assistant"),
	
	/**
	 * Dental Surgery Assistant
	 */
	_DentalSurgeryAssistant ("NR1570", "Dental Surgery Assistant"),
	
	/**
	 * Medical Laboratory Assistant
	 */
	_MedicalLaboratoryAssistant ("NR1580", "Medical Laboratory Assistant"),
	
	/**
	 * Phlebotomist
	 */
	_Phlebotomist ("NR1590", "Phlebotomist"),
	
	/**
	 * Cytoscreener
	 */
	_Cytoscreener ("NR1600", "Cytoscreener"),
	
	/**
	 * Student Technician
	 */
	_StudentTechnician ("NR1610", "Student Technician"),
	
	/**
	 * Trainee Scientist
	 */
	_TraineeScientist ("NR1620", "Trainee Scientist"),
	
	/**
	 * Trainee Practitioner
	 */
	_TraineePractitioner ("NR1630", "Trainee Practitioner"),
	
	/**
	 * Nursing Cadet
	 */
	_NursingCadet ("NR1640", "Nursing Cadet"),
	
	/**
	 * Healthcare Cadet
	 */
	_HealthcareCadet ("NR1650", "Healthcare Cadet"),
	
	/**
	 * Pre-reg Pharmacist
	 */
	_PreregPharmacist ("NR1660", "Pre-reg Pharmacist"),
	
	/**
	 * Assistant Psychologist
	 */
	_AssistantPsychologist ("NR1670", "Assistant Psychologist"),
	
	/**
	 * Assistant Psychotherapist
	 */
	_AssistantPsychotherapist ("NR1680", "Assistant Psychotherapist"),
	
	/**
	 * Call Operator
	 */
	_CallOperator ("NR1690", "Call Operator"),
	
	/**
	 * Gateway Worker
	 */
	_GatewayWorker ("NR1700", "Gateway Worker"),
	
	/**
	 * Support, Time, Recovery Worker
	 */
	_SupportTimeRecoveryWorker ("NR1710", "Support, Time, Recovery Worker"),
	
	/**
	 * Clerical Worker
	 */
	_ClericalWorker ("NR1720", "Clerical Worker"),
	
	/**
	 * Receptionist
	 */
	_Receptionist ("NR1730", "Receptionist"),
	
	/**
	 * Secretary
	 */
	_Secretary ("NR1740", "Secretary"),
	
	/**
	 * Personal Assistant
	 */
	_PersonalAssistant ("NR1750", "Personal Assistant"),
	
	/**
	 * Medical Secretary
	 */
	_MedicalSecretary ("NR1760", "Medical Secretary"),
	
	/**
	 * Officer
	 */
	_Officer ("NR1770", "Officer"),
	
	/**
	 * Manager
	 */
	_Manager ("NR1780", "Manager"),
	
	/**
	 * Senior Manager
	 */
	_SeniorManager ("NR1790", "Senior Manager"),
	
	/**
	 * Technician - Admin  Clerical
	 */
	_TechnicianAdminClerical ("NR1800", "Technician - Admin & Clerical"),
	
	/**
	 * Accountant
	 */
	_Accountant ("NR1810", "Accountant"),
	
	/**
	 * Librarian
	 */
	_Librarian ("NR1820", "Librarian"),
	
	/**
	 * Interpreter
	 */
	_Interpreter ("NR1830", "Interpreter"),
	
	/**
	 * Analyst
	 */
	_Analyst ("NR1840", "Analyst"),
	
	/**
	 * Adviser
	 */
	_Adviser ("NR1850", "Adviser"),
	
	/**
	 * Researcher
	 */
	_Researcher ("NR1860", "Researcher"),
	
	/**
	 * Control Assistant
	 */
	_ControlAssistant ("NR1870", "Control Assistant"),
	
	/**
	 * Architect
	 */
	_Architect ("NR1880", "Architect"),
	
	/**
	 * Lawyer
	 */
	_Lawyer ("NR1890", "Lawyer"),
	
	/**
	 * Surveyor
	 */
	_Surveyor ("NR1900", "Surveyor"),
	
	/**
	 * Chair
	 */
	_Chair ("NR1910", "Chair"),
	
	/**
	 * Chief Executive
	 */
	_ChiefExecutive ("NR1920", "Chief Executive"),
	
	/**
	 * Finance Director
	 */
	_FinanceDirector ("NR1930", "Finance Director"),
	
	/**
	 * Other Executive Director
	 */
	_OtherExecutiveDirector ("NR1940", "Other Executive Director"),
	
	/**
	 * Board Level Director
	 */
	_BoardLevelDirector ("NR1950", "Board Level Director"),
	
	/**
	 * Non Executive Director
	 */
	_NonExecutiveDirector ("NR1960", "Non Executive Director"),
	
	/**
	 * Childcare Co-ordinator
	 */
	_ChildcareCoordinator ("NR1970", "Childcare Co-ordinator"),
	
	/**
	 * Main Informal Carer
	 */
	_MainInformalCarer ("NR1980", "Main Informal Carer"),
	
	/**
	 * Formal Carer
	 */
	_FormalCarer ("NR1990", "Formal Carer"),
	
	/**
	 * Additional person to be involved in decisions
	 */
	_Additionalpersontobeinvolvedindecisions ("NR2000", "Additional person to be involved in decisions"),
	
	/**
	 * LPA with authority
	 */
	_LPAwithauthority ("NR2010", "LPA with authority"),
	
	/**
	 * Key Worker
	 */
	_KeyWorker ("NR2020", "Key Worker"),
	
	/**
	 * Senior Responsible Clinician
	 */
	_SeniorResponsibleClinician ("NR2030", "Senior Responsible Clinician"),
	
	/**
	 * Palliative Care Physician
	 */
	_PalliativeCarePhysician ("NR2040", "Palliative Care Physician"),
	
	/**
	 * Specialist Palliative Care Nurse
	 */
	_SpecialistPalliativeCareNurse ("NR2050", "Specialist Palliative Care Nurse"),
	
	/**
	 * Discharge Coordinator
	 */
	_DischargeCoordinator ("NR2060", "Discharge Coordinator"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.124";

	JobRoleName(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static JobRoleName getByCode(String code) {
		JobRoleName[] vals = values();
		for (JobRoleName val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(JobRoleName other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	