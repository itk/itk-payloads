/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the BSScreeningStatusSubCodes vocabulary:
 * <ul>
 *   <li>_031 : Too young for reliable screening</li>
 *   <li>_032 : Too soon after transfusion less than 72 hours</li>
 *   <li>_033 : Unsuitable sample</li>
 *   <li>_034 : Insufficient sample</li>
 *   <li>_035 : Unsatisfactory Analysis</li>
 *   <li>_036 : Sickle-cell disease SCD - Transfused, repeat four months after last transfusion</li>
 *   <li>_037 : Sickle-cell disease SCD - Too premature for testing</li>
 *   <li>_038 : Congenital Hypothyroidism CHT - Pre-term</li>
 *   <li>_039 : Congenital Hypothyroidism CHT - Borderline result</li>
 *   <li>_0310 : Cystic fibrosis CF - Inconclusive</li>
 *   <li>_061 : Result consistent with haemoglobin C carrier</li>
 *   <li>_062 : Result consistent with haemoglobin D carrier</li>
 *   <li>_063 : Result consistent with haemoglobin E carrier</li>
 *   <li>_064 : Result consistent with haemoglobin O-Arab carrier</li>
 *   <li>_091 : Died</li>
 *   <li>_092 : Unreliable laboratory result</li>
 *   <li>_093 : Moved out of area</li>
 *   <li>_094 : Not contactable, reasonable efforts made</li>
 *   <li>_095 : Too old for screening</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum BSScreeningStatusSubCodes implements VocabularyEntry {
	
	
	/**
	 * Too young for reliable screening
	 */
	_031 ("031", "Too young for reliable screening"),
	
	/**
	 * Too soon after transfusion less than 72 hours
	 */
	_032 ("032", "Too soon after transfusion (less than 72 hours)"),
	
	/**
	 * Unsuitable sample
	 */
	_033 ("033", "Unsuitable sample"),
	
	/**
	 * Insufficient sample
	 */
	_034 ("034", "Insufficient sample"),
	
	/**
	 * Unsatisfactory Analysis
	 */
	_035 ("035", "Unsatisfactory Analysis"),
	
	/**
	 * Sickle-cell disease SCD - Transfused, repeat four months after last transfusion
	 */
	_036 ("036", "Sickle-cell disease (SCD) - Transfused, repeat four months after last transfusion"),
	
	/**
	 * Sickle-cell disease SCD - Too premature for testing
	 */
	_037 ("037", "Sickle-cell disease (SCD) - Too premature for testing"),
	
	/**
	 * Congenital Hypothyroidism CHT - Pre-term
	 */
	_038 ("038", "Congenital Hypothyroidism (CHT) - Pre-term"),
	
	/**
	 * Congenital Hypothyroidism CHT - Borderline result
	 */
	_039 ("039", "Congenital Hypothyroidism (CHT) - Borderline result"),
	
	/**
	 * Cystic fibrosis CF - Inconclusive
	 */
	_0310 ("0310", "Cystic fibrosis (CF) - Inconclusive"),
	
	/**
	 * Result consistent with haemoglobin C carrier
	 */
	_061 ("061", "Result consistent with haemoglobin C carrier"),
	
	/**
	 * Result consistent with haemoglobin D carrier
	 */
	_062 ("062", "Result consistent with haemoglobin D carrier"),
	
	/**
	 * Result consistent with haemoglobin E carrier
	 */
	_063 ("063", "Result consistent with haemoglobin E carrier"),
	
	/**
	 * Result consistent with haemoglobin O-Arab carrier
	 */
	_064 ("064", "Result consistent with haemoglobin O-Arab carrier"),
	
	/**
	 * Died
	 */
	_091 ("091", "Died"),
	
	/**
	 * Unreliable laboratory result
	 */
	_092 ("092", "Unreliable laboratory result"),
	
	/**
	 * Moved out of area
	 */
	_093 ("093", "Moved out of area"),
	
	/**
	 * Not contactable, reasonable efforts made
	 */
	_094 ("094", "Not contactable, reasonable efforts made"),
	
	/**
	 * Too old for screening
	 */
	_095 ("095", "Too old for screening"),
	
	/**
	 * Too young for reliable screening
	 */
	_Tooyoungforreliablescreening ("031", "Too young for reliable screening"),
	
	/**
	 * Too soon after transfusion less than 72 hours
	 */
	_Toosoonaftertransfusionlessthan72hours ("032", "Too soon after transfusion (less than 72 hours)"),
	
	/**
	 * Unsuitable sample
	 */
	_Unsuitablesample ("033", "Unsuitable sample"),
	
	/**
	 * Insufficient sample
	 */
	_Insufficientsample ("034", "Insufficient sample"),
	
	/**
	 * Unsatisfactory Analysis
	 */
	_UnsatisfactoryAnalysis ("035", "Unsatisfactory Analysis"),
	
	/**
	 * Sickle-cell disease SCD - Transfused, repeat four months after last transfusion
	 */
	_SicklecelldiseaseSCDTransfusedrepeatfourmonthsafterlasttransfusion ("036", "Sickle-cell disease (SCD) - Transfused, repeat four months after last transfusion"),
	
	/**
	 * Sickle-cell disease SCD - Too premature for testing
	 */
	_SicklecelldiseaseSCDTooprematurefortesting ("037", "Sickle-cell disease (SCD) - Too premature for testing"),
	
	/**
	 * Congenital Hypothyroidism CHT - Pre-term
	 */
	_CongenitalHypothyroidismCHTPreterm ("038", "Congenital Hypothyroidism (CHT) - Pre-term"),
	
	/**
	 * Congenital Hypothyroidism CHT - Borderline result
	 */
	_CongenitalHypothyroidismCHTBorderlineresult ("039", "Congenital Hypothyroidism (CHT) - Borderline result"),
	
	/**
	 * Cystic fibrosis CF - Inconclusive
	 */
	_CysticfibrosisCFInconclusive ("0310", "Cystic fibrosis (CF) - Inconclusive"),
	
	/**
	 * Result consistent with haemoglobin C carrier
	 */
	_ResultconsistentwithhaemoglobinCcarrier ("061", "Result consistent with haemoglobin C carrier"),
	
	/**
	 * Result consistent with haemoglobin D carrier
	 */
	_ResultconsistentwithhaemoglobinDcarrier ("062", "Result consistent with haemoglobin D carrier"),
	
	/**
	 * Result consistent with haemoglobin E carrier
	 */
	_ResultconsistentwithhaemoglobinEcarrier ("063", "Result consistent with haemoglobin E carrier"),
	
	/**
	 * Result consistent with haemoglobin O-Arab carrier
	 */
	_ResultconsistentwithhaemoglobinOArabcarrier ("064", "Result consistent with haemoglobin O-Arab carrier"),
	
	/**
	 * Died
	 */
	_Died ("091", "Died"),
	
	/**
	 * Unreliable laboratory result
	 */
	_Unreliablelaboratoryresult ("092", "Unreliable laboratory result"),
	
	/**
	 * Moved out of area
	 */
	_Movedoutofarea ("093", "Moved out of area"),
	
	/**
	 * Not contactable, reasonable efforts made
	 */
	_Notcontactablereasonableeffortsmade ("094", "Not contactable, reasonable efforts made"),
	
	/**
	 * Too old for screening
	 */
	_Toooldforscreening ("095", "Too old for screening"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.487";

	BSScreeningStatusSubCodes(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static BSScreeningStatusSubCodes getByCode(String code) {
		BSScreeningStatusSubCodes[] vals = values();
		for (BSScreeningStatusSubCodes val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(BSScreeningStatusSubCodes other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	