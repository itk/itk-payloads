/**
 * These are java enumerations representing various vocabularies that
 * can be populated into coded fields in various payloads. A field will
 * indicate (in it's javadoc comment) when a specific vocabulary should
 * be used.
 */
package uk.nhs.interoperability.payloads.vocabularies.generated;