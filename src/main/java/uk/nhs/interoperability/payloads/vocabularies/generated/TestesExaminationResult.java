/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the TestesExaminationResult vocabulary:
 * <ul>
 *   <li>_00 : Not tested</li>
 *   <li>_01 : No abnormalities</li>
 *   <li>_02 : Unilateral abnormality suspected Right</li>
 *   <li>_03 : Unilateral abnormality suspected Left</li>
 *   <li>_04 : Bi-lateral abnormality suspected</li>
 *   <li>_09 : Screen Declined</li>
 *   <li>_98 : Incomplete</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum TestesExaminationResult implements VocabularyEntry {
	
	
	/**
	 * Not tested
	 */
	_00 ("00", "Not tested"),
	
	/**
	 * No abnormalities
	 */
	_01 ("01", "No abnormalities"),
	
	/**
	 * Unilateral abnormality suspected Right
	 */
	_02 ("02", "Unilateral abnormality suspected (Right)"),
	
	/**
	 * Unilateral abnormality suspected Left
	 */
	_03 ("03", "Unilateral abnormality suspected (Left)"),
	
	/**
	 * Bi-lateral abnormality suspected
	 */
	_04 ("04", "Bi-lateral abnormality suspected"),
	
	/**
	 * Screen Declined
	 */
	_09 ("09", "Screen Declined"),
	
	/**
	 * Incomplete
	 */
	_98 ("98", "Incomplete"),
	
	/**
	 * Not tested
	 */
	_Nottested ("00", "Not tested"),
	
	/**
	 * No abnormalities
	 */
	_Noabnormalities ("01", "No abnormalities"),
	
	/**
	 * Unilateral abnormality suspected Right
	 */
	_UnilateralabnormalitysuspectedRight ("02", "Unilateral abnormality suspected (Right)"),
	
	/**
	 * Unilateral abnormality suspected Left
	 */
	_UnilateralabnormalitysuspectedLeft ("03", "Unilateral abnormality suspected (Left)"),
	
	/**
	 * Bi-lateral abnormality suspected
	 */
	_Bilateralabnormalitysuspected ("04", "Bi-lateral abnormality suspected"),
	
	/**
	 * Screen Declined
	 */
	_ScreenDeclined ("09", "Screen Declined"),
	
	/**
	 * Incomplete
	 */
	_Incomplete ("98", "Incomplete"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.449";

	TestesExaminationResult(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static TestesExaminationResult getByCode(String code) {
		TestesExaminationResult[] vals = values();
		for (TestesExaminationResult val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(TestesExaminationResult other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	