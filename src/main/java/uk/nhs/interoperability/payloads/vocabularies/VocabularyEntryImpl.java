/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies;

import uk.nhs.interoperability.payloads.vocabularies.generated.ActStatus;

public class VocabularyEntryImpl implements VocabularyEntry {

	private String code;
	private String displayName;
	private String oid;
	
	public VocabularyEntryImpl(final String code, final String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	public VocabularyEntryImpl(final String code) {
		this.code = code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public void setOID(String oid) {
		this.oid = oid;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public final String getCode() {
		return this.code;
	}

	@Override
	public final String getDisplayName() {
		return this.displayName;
	}

	@Override
	public String getOID() {
		return this.oid;
	}
	
	public VocabularyEntryImpl getByCode(String code) {
		if (getCode().equals(code)) {
			return this;
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
