/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

public enum NullFlavour implements VocabularyEntry {
	/** NI */
	NI("NI"),
	/** NA */
	NA("NA"),
	/** Unknown - A proper value is applicable, but not known **/
	Unknown("UNK"),
	/** Not asked - This information has not been sought (e.g., patient was not asked) **/
	NotAsked("NASK"),
	/** Asked but unknown - Information was sought but not found (e.g., patient was asked but didn't know) **/
	AskedButUnknown("ASKU"),
	/** Temporarily unavailable - Information is not available at this time but it is expected that it will be available later. **/
	TemporarilyUnavailable("NAV"),
	/** OTH */
	OTH("OTH"),
	/** PINF */
	PINF("PINF"),
	/** NINF */
	NINF("NINF");

	//TODO: Find descriptions for the other null flavours and clarify whether MSK is allowed.
	
	public String code;
	
	private NullFlavour(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static NullFlavour getByCode(String code) {
		NullFlavour[] vals = values();
		for (NullFlavour val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(NullFlavour other) {
		return (other.getCode().equals(code));
	}
	
}
