/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the TargetAwareness vocabulary:
 * <ul>
 *   <li>_D : denying</li>
 *   <li>_F : full awareness</li>
 *   <li>_I : incapable</li>
 *   <li>_M : marginal</li>
 *   <li>_P : partial</li>
 *   <li>_U : uninformed</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum TargetAwareness implements VocabularyEntry {
	
	
	/**
	 * denying
	 */
	_D ("D", "denying"),
	
	/**
	 * full awareness
	 */
	_F ("F", "full awareness"),
	
	/**
	 * incapable
	 */
	_I ("I", "incapable"),
	
	/**
	 * marginal
	 */
	_M ("M", "marginal"),
	
	/**
	 * partial
	 */
	_P ("P", "partial"),
	
	/**
	 * uninformed
	 */
	_U ("U", "uninformed"),
	
	/**
	 * denying
	 */
	_denying ("D", "denying"),
	
	/**
	 * full awareness
	 */
	_fullawareness ("F", "full awareness"),
	
	/**
	 * incapable
	 */
	_incapable ("I", "incapable"),
	
	/**
	 * marginal
	 */
	_marginal ("M", "marginal"),
	
	/**
	 * partial
	 */
	_partial ("P", "partial"),
	
	/**
	 * uninformed
	 */
	_uninformed ("U", "uninformed"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.5.137";

	TargetAwareness(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static TargetAwareness getByCode(String code) {
		TargetAwareness[] vals = values();
		for (TargetAwareness val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(TargetAwareness other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	