/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT;

public enum AddressType implements VocabularyEntry {
	/** Used primarily to visit an address */
	PhysicalVisit("PHYS"),
	/** Used to send mail */
	PostalAddress("PST"),
	/** A temporary address, may be good for visit or mailing. Note that an address history can provide more detailed information. */
	Temporary("TMP"),
	/** A flag indicating that the address is bad, in fact, useless. */
	Bad("BAD"),
	/** A communication address at a home, attempted contacts for business purposes might intrude privacy and chances are one will contact family or other household members instead of the person one wishes to call. Typically used with urgent cases, or if no other contacts are available. */
	Home("H"),
	/** The primary home, to reach a person after business hours */
	PrimaryHome("HP"),
	/** A vacation home, to reach a person while on vacation */
	Vacation("HV"),
	/** An office address. First choice for business related contacts during business hours */
	WorkPlace("WP"),
	/** Alphabetic transcription of name (Japanese: romaji) */
	Alphabetic("ABC"),
	/** Syllabic transcription of name (e.g., Japanese kana, Korean hangul) */
	Syllabic("SYL"),
	/** Ideographic representation of name (e.g., Japanese kanji, Chinese characters) */
	Ideographic("IDE");
	
	public String code;
	
	private AddressType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}

	public static AddressType getByCode(String code) {
		AddressType[] vals = values();
		for (AddressType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(AddressType other) {
		return (other.getCode().equals(code));
	}
	
}
