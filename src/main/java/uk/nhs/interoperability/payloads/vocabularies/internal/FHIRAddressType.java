/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT;

public enum FHIRAddressType implements VocabularyEntry {
	/** A communication address at a home. */
	home("home"),
	/** An office address. First choice for business related contacts during business hours. */
	work("work"),
	/** A temporary address. The period can provide more detailed information. */
	temp("temp"),
	/** This address is no longer in use (or was never correct, but retained for records). */
	old("old");
	
	public String code;
	
	private FHIRAddressType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}

	public static FHIRAddressType getByCode(String code) {
		FHIRAddressType[] vals = values();
		for (FHIRAddressType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(FHIRAddressType other) {
		return (other.getCode().equals(code));
	}
	
}
