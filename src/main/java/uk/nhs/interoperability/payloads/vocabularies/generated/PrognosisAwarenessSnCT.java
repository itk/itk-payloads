/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the PrognosisAwarenessSnCT vocabulary:
 * <ul>
 *   <li>_751961000000104 : Relative aware of prognosis situation</li>
 *   <li>_760101000000101 : Relative unaware of prognosis situation</li>
 *   <li>_751941000000100 : Carer aware of prognosis situation</li>
 *   <li>_711951000000105 : Carer unaware of prognosis situation</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum PrognosisAwarenessSnCT implements VocabularyEntry {
	
	
	/**
	 * Relative aware of prognosis situation
	 */
	_751961000000104 ("751961000000104", "Relative aware of prognosis (situation)"),
	
	/**
	 * Relative unaware of prognosis situation
	 */
	_760101000000101 ("760101000000101", "Relative unaware of prognosis (situation)"),
	
	/**
	 * Carer aware of prognosis situation
	 */
	_751941000000100 ("751941000000100", "Carer aware of prognosis (situation)"),
	
	/**
	 * Carer unaware of prognosis situation
	 */
	_711951000000105 ("711951000000105", "Carer unaware of prognosis (situation)"),
	
	/**
	 * Relative aware of prognosis situation
	 */
	_Relativeawareofprognosissituation ("751961000000104", "Relative aware of prognosis (situation)"),
	
	/**
	 * Relative unaware of prognosis situation
	 */
	_Relativeunawareofprognosissituation ("760101000000101", "Relative unaware of prognosis (situation)"),
	
	/**
	 * Carer aware of prognosis situation
	 */
	_Carerawareofprognosissituation ("751941000000100", "Carer aware of prognosis (situation)"),
	
	/**
	 * Carer unaware of prognosis situation
	 */
	_Carerunawareofprognosissituation ("711951000000105", "Carer unaware of prognosis (situation)"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	PrognosisAwarenessSnCT(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static PrognosisAwarenessSnCT getByCode(String code) {
		PrognosisAwarenessSnCT[] vals = values();
		for (PrognosisAwarenessSnCT val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(PrognosisAwarenessSnCT other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	