/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the ParticipationType vocabulary:
 * <ul>
 *   <li>_ADM : Admitter</li>
 *   <li>_ATND : Attender</li>
 *   <li>_CON : Consultant</li>
 *   <li>_DIS : Discharger</li>
 *   <li>_ESC : Escort</li>
 *   <li>_REF : Referrer</li>
 *   <li>_IND : Indirect target</li>
 *   <li>_BEN : Beneficiary</li>
 *   <li>_COV : Coverage target</li>
 *   <li>_HLD : Holder</li>
 *   <li>_RCT : Record target</li>
 *   <li>_RCV : Receiver</li>
 *   <li>_AUT : Author originator</li>
 *   <li>_ENT : Data entry person</li>
 *   <li>_INF : Informant</li>
 *   <li>_WIT : Witness</li>
 *   <li>_NOT : Ugent notification contact</li>
 *   <li>_PRCP : Primary information recipient</li>
 *   <li>_REFB : Referred by</li>
 *   <li>_REFT : Referred to</li>
 *   <li>_TRC : Tracker</li>
 *   <li>_PRF : Performer</li>
 *   <li>_DIST : Distributor</li>
 *   <li>_PPRF : Primary performer</li>
 *   <li>_SPRF : Secondary performer</li>
 *   <li>_DIR : Direct target</li>
 *   <li>_CSM : Consumable</li>
 *   <li>_TPA : Therapeutic agent</li>
 *   <li>_DEV : Device</li>
 *   <li>_NRD : Non-reuseable device</li>
 *   <li>_RDV : Reusable device</li>
 *   <li>_SBJ : Subject</li>
 *   <li>_SPC : Specimen</li>
 *   <li>_BBY : Baby</li>
 *   <li>_DON : Donor</li>
 *   <li>_PRD : Product</li>
 *   <li>_LOC : Location</li>
 *   <li>_DST : Destination</li>
 *   <li>_ELOC : Entry location</li>
 *   <li>_ORG : Origin</li>
 *   <li>_RML : Remote</li>
 *   <li>_VIA : Via</li>
 *   <li>_VRF : Verifier</li>
 *   <li>_AUTHEN : Authenticator</li>
 *   <li>_LA : Legal authenticator</li>
 *   <li>_CST : Custodian</li>
 *   <li>_RESP : Responsible party</li>
 *   <li>_DENT : Data Enterer</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum ParticipationType implements VocabularyEntry {
	
	
	/**
	 * Admitter
	 */
	_ADM ("ADM", "Admitter"),
	
	/**
	 * Attender
	 */
	_ATND ("ATND", "Attender"),
	
	/**
	 * Consultant
	 */
	_CON ("CON", "Consultant"),
	
	/**
	 * Discharger
	 */
	_DIS ("DIS", "Discharger"),
	
	/**
	 * Escort
	 */
	_ESC ("ESC", "Escort"),
	
	/**
	 * Referrer
	 */
	_REF ("REF", "Referrer"),
	
	/**
	 * Indirect target
	 */
	_IND ("IND", "Indirect target"),
	
	/**
	 * Beneficiary
	 */
	_BEN ("BEN", "Beneficiary"),
	
	/**
	 * Coverage target
	 */
	_COV ("COV", "Coverage target"),
	
	/**
	 * Holder
	 */
	_HLD ("HLD", "Holder"),
	
	/**
	 * Record target
	 */
	_RCT ("RCT", "Record target"),
	
	/**
	 * Receiver
	 */
	_RCV ("RCV", "Receiver"),
	
	/**
	 * Author originator
	 */
	_AUT ("AUT", "Author (originator)"),
	
	/**
	 * Data entry person
	 */
	_ENT ("ENT", "Data entry person"),
	
	/**
	 * Informant
	 */
	_INF ("INF", "Informant"),
	
	/**
	 * Witness
	 */
	_WIT ("WIT", "Witness"),
	
	/**
	 * Ugent notification contact
	 */
	_NOT ("NOT", "Ugent notification contact"),
	
	/**
	 * Primary information recipient
	 */
	_PRCP ("PRCP", "Primary information recipient"),
	
	/**
	 * Referred by
	 */
	_REFB ("REFB", "Referred by"),
	
	/**
	 * Referred to
	 */
	_REFT ("REFT", "Referred to"),
	
	/**
	 * Tracker
	 */
	_TRC ("TRC", "Tracker"),
	
	/**
	 * Performer
	 */
	_PRF ("PRF", "Performer"),
	
	/**
	 * Distributor
	 */
	_DIST ("DIST", "Distributor"),
	
	/**
	 * Primary performer
	 */
	_PPRF ("PPRF", "Primary performer"),
	
	/**
	 * Secondary performer
	 */
	_SPRF ("SPRF", "Secondary performer"),
	
	/**
	 * Direct target
	 */
	_DIR ("DIR", "Direct target"),
	
	/**
	 * Consumable
	 */
	_CSM ("CSM", "Consumable"),
	
	/**
	 * Therapeutic agent
	 */
	_TPA ("TPA", "Therapeutic agent"),
	
	/**
	 * Device
	 */
	_DEV ("DEV", "Device"),
	
	/**
	 * Non-reuseable device
	 */
	_NRD ("NRD", "Non-reuseable device"),
	
	/**
	 * Reusable device
	 */
	_RDV ("RDV", "Reusable device"),
	
	/**
	 * Subject
	 */
	_SBJ ("SBJ", "Subject"),
	
	/**
	 * Specimen
	 */
	_SPC ("SPC", "Specimen"),
	
	/**
	 * Baby
	 */
	_BBY ("BBY", "Baby"),
	
	/**
	 * Donor
	 */
	_DON ("DON", "Donor"),
	
	/**
	 * Product
	 */
	_PRD ("PRD", "Product"),
	
	/**
	 * Location
	 */
	_LOC ("LOC", "Location"),
	
	/**
	 * Destination
	 */
	_DST ("DST", "Destination"),
	
	/**
	 * Entry location
	 */
	_ELOC ("ELOC", "Entry location"),
	
	/**
	 * Origin
	 */
	_ORG ("ORG", "Origin"),
	
	/**
	 * Remote
	 */
	_RML ("RML", "Remote"),
	
	/**
	 * Via
	 */
	_VIA ("VIA", "Via"),
	
	/**
	 * Verifier
	 */
	_VRF ("VRF", "Verifier"),
	
	/**
	 * Authenticator
	 */
	_AUTHEN ("AUTHEN", "Authenticator"),
	
	/**
	 * Legal authenticator
	 */
	_LA ("LA", "Legal authenticator"),
	
	/**
	 * Custodian
	 */
	_CST ("CST", "Custodian"),
	
	/**
	 * Responsible party
	 */
	_RESP ("RESP", "Responsible party"),
	
	/**
	 * Data Enterer
	 */
	_DENT ("DENT", "Data Enterer"),
	
	/**
	 * Admitter
	 */
	_Admitter ("ADM", "Admitter"),
	
	/**
	 * Attender
	 */
	_Attender ("ATND", "Attender"),
	
	/**
	 * Consultant
	 */
	_Consultant ("CON", "Consultant"),
	
	/**
	 * Discharger
	 */
	_Discharger ("DIS", "Discharger"),
	
	/**
	 * Escort
	 */
	_Escort ("ESC", "Escort"),
	
	/**
	 * Referrer
	 */
	_Referrer ("REF", "Referrer"),
	
	/**
	 * Indirect target
	 */
	_Indirecttarget ("IND", "Indirect target"),
	
	/**
	 * Beneficiary
	 */
	_Beneficiary ("BEN", "Beneficiary"),
	
	/**
	 * Coverage target
	 */
	_Coveragetarget ("COV", "Coverage target"),
	
	/**
	 * Holder
	 */
	_Holder ("HLD", "Holder"),
	
	/**
	 * Record target
	 */
	_Recordtarget ("RCT", "Record target"),
	
	/**
	 * Receiver
	 */
	_Receiver ("RCV", "Receiver"),
	
	/**
	 * Author originator
	 */
	_Authororiginator ("AUT", "Author (originator)"),
	
	/**
	 * Data entry person
	 */
	_Dataentryperson ("ENT", "Data entry person"),
	
	/**
	 * Informant
	 */
	_Informant ("INF", "Informant"),
	
	/**
	 * Witness
	 */
	_Witness ("WIT", "Witness"),
	
	/**
	 * Ugent notification contact
	 */
	_Ugentnotificationcontact ("NOT", "Ugent notification contact"),
	
	/**
	 * Primary information recipient
	 */
	_Primaryinformationrecipient ("PRCP", "Primary information recipient"),
	
	/**
	 * Referred by
	 */
	_Referredby ("REFB", "Referred by"),
	
	/**
	 * Referred to
	 */
	_Referredto ("REFT", "Referred to"),
	
	/**
	 * Tracker
	 */
	_Tracker ("TRC", "Tracker"),
	
	/**
	 * Performer
	 */
	_Performer ("PRF", "Performer"),
	
	/**
	 * Distributor
	 */
	_Distributor ("DIST", "Distributor"),
	
	/**
	 * Primary performer
	 */
	_Primaryperformer ("PPRF", "Primary performer"),
	
	/**
	 * Secondary performer
	 */
	_Secondaryperformer ("SPRF", "Secondary performer"),
	
	/**
	 * Direct target
	 */
	_Directtarget ("DIR", "Direct target"),
	
	/**
	 * Consumable
	 */
	_Consumable ("CSM", "Consumable"),
	
	/**
	 * Therapeutic agent
	 */
	_Therapeuticagent ("TPA", "Therapeutic agent"),
	
	/**
	 * Device
	 */
	_Device ("DEV", "Device"),
	
	/**
	 * Non-reuseable device
	 */
	_Nonreuseabledevice ("NRD", "Non-reuseable device"),
	
	/**
	 * Reusable device
	 */
	_Reusabledevice ("RDV", "Reusable device"),
	
	/**
	 * Subject
	 */
	_Subject ("SBJ", "Subject"),
	
	/**
	 * Specimen
	 */
	_Specimen ("SPC", "Specimen"),
	
	/**
	 * Baby
	 */
	_Baby ("BBY", "Baby"),
	
	/**
	 * Donor
	 */
	_Donor ("DON", "Donor"),
	
	/**
	 * Product
	 */
	_Product ("PRD", "Product"),
	
	/**
	 * Location
	 */
	_Location ("LOC", "Location"),
	
	/**
	 * Destination
	 */
	_Destination ("DST", "Destination"),
	
	/**
	 * Entry location
	 */
	_Entrylocation ("ELOC", "Entry location"),
	
	/**
	 * Origin
	 */
	_Origin ("ORG", "Origin"),
	
	/**
	 * Remote
	 */
	_Remote ("RML", "Remote"),
	
	/**
	 * Via
	 */
	_Via ("VIA", "Via"),
	
	/**
	 * Verifier
	 */
	_Verifier ("VRF", "Verifier"),
	
	/**
	 * Authenticator
	 */
	_Authenticator ("AUTHEN", "Authenticator"),
	
	/**
	 * Legal authenticator
	 */
	_Legalauthenticator ("LA", "Legal authenticator"),
	
	/**
	 * Custodian
	 */
	_Custodian ("CST", "Custodian"),
	
	/**
	 * Responsible party
	 */
	_Responsibleparty ("RESP", "Responsible party"),
	
	/**
	 * Data Enterer
	 */
	_DataEnterer ("DENT", "Data Enterer"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.1.5.90.11.1";

	ParticipationType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static ParticipationType getByCode(String code) {
		ParticipationType[] vals = values();
		for (ParticipationType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(ParticipationType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	