/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies;

import java.util.HashMap;

public class Vocabulary {
	private String vocabName;
	private HashMap<String, VocabularyEntry> entries;
	
	public Vocabulary(String name) {
		this.vocabName = name;
		this.entries = new HashMap<String, VocabularyEntry>();
	}

	public String getVocabName() {
		return vocabName;
	}

	public HashMap<String, VocabularyEntry> getEntries() {
		return entries;
	}
	
	public VocabularyEntry getEntry(String code) {
		return entries.get(code);
	}
	
	public void add(String key, VocabularyEntry entry) {
		entries.put(key, entry);
	}
	
	
}
