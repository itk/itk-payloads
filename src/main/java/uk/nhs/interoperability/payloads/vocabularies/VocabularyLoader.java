/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.metadata.VocabClassInfo;
import uk.nhs.interoperability.payloads.util.PropertyReader;

/**
 * Nationally agreed vocabularies are published on TRUD within the
 * NHS Interoperability Specifications Reference Pack bundle:
 * https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases
 *
 * @author adha3
 */
public class VocabularyLoader {
	private static final Logger logger = LoggerFactory.getLogger(VocabularyLoader.class);

	public static void loadVocabs(VocabProcessor processor) {
		
		ArrayList<VocabClassInfo> vocabInfoList = new ArrayList<VocabClassInfo>();
		
		vocabInfoList.addAll(
				locateVocabFiles(PropertyReader.getProperty("nationalHL7v3VocabsDir"),
				   	   PropertyReader.getProperty("nationalHL7v3Vocabs"),
				   	   null,
				   	   processor)
		);
		
		vocabInfoList.addAll(
				locateVocabFiles(PropertyReader.getProperty("nationalSnomedVocabsDir"),
					   PropertyReader.getProperty("nationalSnomedVocabs"), 
					   PropertyReader.getProperty("nationalSnomedOID"),
					   processor)
		);
		
		vocabInfoList.addAll(
				locateVocabFiles(PropertyReader.getProperty("localVocabsDir"),
			   	   	   PropertyReader.getProperty("localVocabs"),
			   	   	   null,
			   	   	   processor)
		);
		
		// Now generate a factory for creating vocabs
		String srcPath = PropertyReader.getProperty("srcPath");
		GenerateVocabFactory.generateVocabFactory(vocabInfoList, srcPath);
	}
	
	private static ArrayList<VocabClassInfo> locateVocabFiles(String directory, String filenames, String oid, VocabProcessor processor) {
		ArrayList<VocabClassInfo> vocabInfoList = new ArrayList<VocabClassInfo>();
		
		if ((directory == null) || (filenames == null)) {
			return vocabInfoList;
		}
		if ((directory.length()==0) || (filenames.length()==0)) {
			return vocabInfoList;
		}
		
		String[] patterns = filenames.split(",");
		for(String filenamePattern : patterns) {
			File[] files = getFiles(directory, filenamePattern.trim());
			if (files.length==0) {
				logger.info("No match found for: '{}' in directory: {}", filenamePattern.trim(), directory);
			}
			for (File file: files) {
				if (!file.exists()) {
					logger.info("Cannot find file: {}", file.getAbsolutePath());
				} else {
					VocabClassInfo vocabInfo = null;
					if (oid == null) {
						vocabInfo = processor.processVocab(file);
					} else {
						vocabInfo = processor.processVocab(file, oid);
					}
					if (vocabInfo != null) {
						vocabInfoList.add(vocabInfo);
					}
				}
			}
		}
		return vocabInfoList;
	}
	
	private static File[] getFiles(final String directory, final String pattern) {
		File dir = new File(directory);
		File[] files = dir.listFiles(
			new FilenameFilter() {
				@Override
			    public boolean accept(File dir, String name) {
			        if (name.equals(pattern))
			        	return true;
					return name.matches(pattern);
			    }
			});
		return files;
	}
}
