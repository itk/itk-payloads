/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.ActStatus;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT;

/**
 * These codes are used to identify the attributes within SNOMED CT compositional
 * grammar fields 
 * @author Adam Hatherly
 * @see uk.nhs.interoperability.payloads.CompositionalStatement
 */
public enum DocumentTypes implements VocabularyEntry {
	/** Clinical document descriptor **/
	ClinicalDocumentDescriptor("810301000000103", "Clinical document descriptor"),
	/** Type of clinical document */
	DocumentType("810311000000101", "Type of clinical document"),
	/** Care setting of clinical document */
	CareSetting("810321000000107", "Care setting of clinical document");
	
	public String code;
	public String displayName;
	public static final String oid = PropertyReader.getProperty("nationalSnomedOID");
	
	private DocumentTypes(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return this.displayName;
	}

	@Override
	public String getOID() {
		return this.oid;
	}

	public static DocumentTypes getByCode(String code) {
		DocumentTypes[] vals = values();
		for (DocumentTypes val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(DocumentTypes other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
