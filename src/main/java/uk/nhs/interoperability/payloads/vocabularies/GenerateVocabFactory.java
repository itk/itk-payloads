/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import uk.nhs.interoperability.payloads.metadata.VocabClassInfo;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.GlobalConstants;

public class GenerateVocabFactory implements GlobalConstants {
	
	private static final List<String> exceptions =
			Arrays.asList("package-info.java" ,"DatePrecision.java");
	
	protected static void generateVocabFactory(ArrayList<VocabClassInfo> vocabInfoList, String srcPath) {
		
		srcPath = srcPath + VOCABPATH;
		
		// Add internal vocabs to the list
		vocabInfoList.addAll(getInternalVocabs(srcPath));
		
		String content =
			"/**\n" +
			" * Note: This class is generated automatically when GenerateVocabularies is run\n" +
			" * @author Adam Hatherly\n" +
			" */\n" +
			"package uk.nhs.interoperability.payloads.vocabularies;\n\n" +
			"public class VocabularyFactory {\n";
		
		// Generate the factory method
		content = content +
			"	public static VocabularyEntry getVocab(String name, String oid, String code) {\n" +
			"		if (oid==null) oid = \"\";\n";

		for (VocabClassInfo info : vocabInfoList) {
			String name = info.getVocabName();
			String oid = info.getOid();
			String subPackage = "generated.";
			if (info.isInternal()) {
				subPackage = "internal.";
			}
			
			content = content +
			"		if (name.equals(\"" + name + "\")) {\n" +
			"			return uk.nhs.interoperability.payloads.vocabularies." + subPackage + name + ".getByCode(code);\n" +
			"		}\n";
			
			if (oid != null) {
				content = content +
				"		if (oid.equals(\"" + oid + "\")) {\n" +
				"			return uk.nhs.interoperability.payloads.vocabularies.generated." + name + ".getByCode(code);\n" +
				"		}\n";
			}
		}

		content = content +
			"		return null;\n" +
			"	}\n" +
			"}\n";
		FileWriter.writeFile(srcPath + "/VocabularyFactory.java", content.getBytes());
	}
	
	private static ArrayList<VocabClassInfo> getInternalVocabs(String vocabSrcPath) {
		ArrayList<VocabClassInfo> vocabInfoList = new ArrayList<VocabClassInfo>();
		String path = vocabSrcPath + "/internal";
		
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		if (listOfFiles != null) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					String name = listOfFiles[i].getName();
					if (!exceptions.contains(name)) {
						VocabClassInfo vocabClass = new VocabClassInfo();
						vocabClass.setVocabName(name.substring(0, name.lastIndexOf('.')));
						vocabClass.setInternal(true);
						vocabInfoList.add(vocabClass);
					}
				}
			}
		}
		return vocabInfoList;
	}
}
