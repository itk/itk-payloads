/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the CDAOrganizationType vocabulary:
 * <ul>
 *   <li>_001 : General Medical Practice</li>
 *   <li>_002 : General Dental Practice</li>
 *   <li>_003 : Community Pharmacy</li>
 *   <li>_004 : Community Opticians</li>
 *   <li>_005 : Primary Care Trust</li>
 *   <li>_006 : Strategic Health Authority</li>
 *   <li>_007 : Special Health Authority</li>
 *   <li>_008 : Acute Trust</li>
 *   <li>_009 : Care Trust</li>
 *   <li>_010 : Community Trust</li>
 *   <li>_011 : Diagnostic and Investigation Centre</li>
 *   <li>_012 : Walk-in Centre</li>
 *   <li>_013 : NHS Direct</li>
 *   <li>_014 : Local Authority Social Services Department</li>
 *   <li>_015 : Nursing Home</li>
 *   <li>_016 : Residential Home</li>
 *   <li>_017 : Hospice</li>
 *   <li>_018 : Ambulance Trust</li>
 *   <li>_019 : Private Hospital</li>
 *   <li>_020 : GMP Deputising Service</li>
 *   <li>_021 : Nursing Agency</li>
 *   <li>_999 : Not specified</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum CDAOrganizationType implements VocabularyEntry {
	
	
	/**
	 * General Medical Practice
	 */
	_001 ("001", "General Medical Practice"),
	
	/**
	 * General Dental Practice
	 */
	_002 ("002", "General Dental Practice"),
	
	/**
	 * Community Pharmacy
	 */
	_003 ("003", "Community Pharmacy"),
	
	/**
	 * Community Opticians
	 */
	_004 ("004", "Community Opticians"),
	
	/**
	 * Primary Care Trust
	 */
	_005 ("005", "Primary Care Trust"),
	
	/**
	 * Strategic Health Authority
	 */
	_006 ("006", "Strategic Health Authority"),
	
	/**
	 * Special Health Authority
	 */
	_007 ("007", "Special Health Authority"),
	
	/**
	 * Acute Trust
	 */
	_008 ("008", "Acute Trust"),
	
	/**
	 * Care Trust
	 */
	_009 ("009", "Care Trust"),
	
	/**
	 * Community Trust
	 */
	_010 ("010", "Community Trust"),
	
	/**
	 * Diagnostic and Investigation Centre
	 */
	_011 ("011", "Diagnostic and Investigation Centre"),
	
	/**
	 * Walk-in Centre
	 */
	_012 ("012", "Walk-in Centre"),
	
	/**
	 * NHS Direct
	 */
	_013 ("013", "NHS Direct"),
	
	/**
	 * Local Authority Social Services Department
	 */
	_014 ("014", "Local Authority Social Services Department"),
	
	/**
	 * Nursing Home
	 */
	_015 ("015", "Nursing Home"),
	
	/**
	 * Residential Home
	 */
	_016 ("016", "Residential Home"),
	
	/**
	 * Hospice
	 */
	_017 ("017", "Hospice"),
	
	/**
	 * Ambulance Trust
	 */
	_018 ("018", "Ambulance Trust"),
	
	/**
	 * Private Hospital
	 */
	_019 ("019", "Private Hospital"),
	
	/**
	 * GMP Deputising Service
	 */
	_020 ("020", "GMP Deputising Service"),
	
	/**
	 * Nursing Agency
	 */
	_021 ("021", "Nursing Agency"),
	
	/**
	 * Not specified
	 */
	_999 ("999", "Not specified"),
	
	/**
	 * General Medical Practice
	 */
	_GeneralMedicalPractice ("001", "General Medical Practice"),
	
	/**
	 * General Dental Practice
	 */
	_GeneralDentalPractice ("002", "General Dental Practice"),
	
	/**
	 * Community Pharmacy
	 */
	_CommunityPharmacy ("003", "Community Pharmacy"),
	
	/**
	 * Community Opticians
	 */
	_CommunityOpticians ("004", "Community Opticians"),
	
	/**
	 * Primary Care Trust
	 */
	_PrimaryCareTrust ("005", "Primary Care Trust"),
	
	/**
	 * Strategic Health Authority
	 */
	_StrategicHealthAuthority ("006", "Strategic Health Authority"),
	
	/**
	 * Special Health Authority
	 */
	_SpecialHealthAuthority ("007", "Special Health Authority"),
	
	/**
	 * Acute Trust
	 */
	_AcuteTrust ("008", "Acute Trust"),
	
	/**
	 * Care Trust
	 */
	_CareTrust ("009", "Care Trust"),
	
	/**
	 * Community Trust
	 */
	_CommunityTrust ("010", "Community Trust"),
	
	/**
	 * Diagnostic and Investigation Centre
	 */
	_DiagnosticandInvestigationCentre ("011", "Diagnostic and Investigation Centre"),
	
	/**
	 * Walk-in Centre
	 */
	_WalkinCentre ("012", "Walk-in Centre"),
	
	/**
	 * NHS Direct
	 */
	_NHSDirect ("013", "NHS Direct"),
	
	/**
	 * Local Authority Social Services Department
	 */
	_LocalAuthoritySocialServicesDepartment ("014", "Local Authority Social Services Department"),
	
	/**
	 * Nursing Home
	 */
	_NursingHome ("015", "Nursing Home"),
	
	/**
	 * Residential Home
	 */
	_ResidentialHome ("016", "Residential Home"),
	
	/**
	 * Hospice
	 */
	_Hospice ("017", "Hospice"),
	
	/**
	 * Ambulance Trust
	 */
	_AmbulanceTrust ("018", "Ambulance Trust"),
	
	/**
	 * Private Hospital
	 */
	_PrivateHospital ("019", "Private Hospital"),
	
	/**
	 * GMP Deputising Service
	 */
	_GMPDeputisingService ("020", "GMP Deputising Service"),
	
	/**
	 * Nursing Agency
	 */
	_NursingAgency ("021", "Nursing Agency"),
	
	/**
	 * Not specified
	 */
	_Notspecified ("999", "Not specified"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.191";

	CDAOrganizationType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static CDAOrganizationType getByCode(String code) {
		CDAOrganizationType[] vals = values();
		for (CDAOrganizationType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(CDAOrganizationType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	