/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the SCDScreeningResult vocabulary:
 * <ul>
 *   <li>_01 : Specimen received in laboratory</li>
 *   <li>_02 : Screening declined</li>
 *   <li>_03 : Further sample required</li>
 *   <li>_04 : Condition not suspected</li>
 *   <li>_05 : Carrier</li>
 *   <li>_06 : Sickle Cell Disease not suspected, carrier of other haemoglobin</li>
 *   <li>_07 : Condition not suspected, other disorders follow up</li>
 *   <li>_08 : Condition suspected</li>
 *   <li>_09 : Not screenedscreening incomplete</li>
 *   <li>_10 : Sickle Cell Disease not suspected by DNA No other haemoglobinthalassemia excluded</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum SCDScreeningResult implements VocabularyEntry {
	
	
	/**
	 * Specimen received in laboratory
	 */
	_01 ("01", "Specimen received in laboratory"),
	
	/**
	 * Screening declined
	 */
	_02 ("02", "Screening declined"),
	
	/**
	 * Further sample required
	 */
	_03 ("03", "Further sample required"),
	
	/**
	 * Condition not suspected
	 */
	_04 ("04", "Condition not suspected"),
	
	/**
	 * Carrier
	 */
	_05 ("05", "Carrier"),
	
	/**
	 * Sickle Cell Disease not suspected, carrier of other haemoglobin
	 */
	_06 ("06", "Sickle Cell Disease not suspected, carrier of other haemoglobin"),
	
	/**
	 * Condition not suspected, other disorders follow up
	 */
	_07 ("07", "Condition not suspected, other disorders follow up"),
	
	/**
	 * Condition suspected
	 */
	_08 ("08", "Condition suspected"),
	
	/**
	 * Not screenedscreening incomplete
	 */
	_09 ("09", "Not screened/screening incomplete"),
	
	/**
	 * Sickle Cell Disease not suspected by DNA No other haemoglobinthalassemia excluded
	 */
	_10 ("10", "Sickle Cell Disease not suspected (by DNA) No other haemoglobin/thalassemia excluded"),
	
	/**
	 * Specimen received in laboratory
	 */
	_Specimenreceivedinlaboratory ("01", "Specimen received in laboratory"),
	
	/**
	 * Screening declined
	 */
	_Screeningdeclined ("02", "Screening declined"),
	
	/**
	 * Further sample required
	 */
	_Furthersamplerequired ("03", "Further sample required"),
	
	/**
	 * Condition not suspected
	 */
	_Conditionnotsuspected ("04", "Condition not suspected"),
	
	/**
	 * Carrier
	 */
	_Carrier ("05", "Carrier"),
	
	/**
	 * Sickle Cell Disease not suspected, carrier of other haemoglobin
	 */
	_SickleCellDiseasenotsuspectedcarrierofotherhaemoglobin ("06", "Sickle Cell Disease not suspected, carrier of other haemoglobin"),
	
	/**
	 * Condition not suspected, other disorders follow up
	 */
	_Conditionnotsuspectedotherdisordersfollowup ("07", "Condition not suspected, other disorders follow up"),
	
	/**
	 * Condition suspected
	 */
	_Conditionsuspected ("08", "Condition suspected"),
	
	/**
	 * Not screenedscreening incomplete
	 */
	_Notscreenedscreeningincomplete ("09", "Not screened/screening incomplete"),
	
	/**
	 * Sickle Cell Disease not suspected by DNA No other haemoglobinthalassemia excluded
	 */
	_SickleCellDiseasenotsuspectedbyDNANootherhaemoglobinthalassemiaexcluded ("10", "Sickle Cell Disease not suspected (by DNA) No other haemoglobin/thalassemia excluded"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.462";

	SCDScreeningResult(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static SCDScreeningResult getByCode(String code) {
		SCDScreeningResult[] vals = values();
		for (SCDScreeningResult val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(SCDScreeningResult other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	