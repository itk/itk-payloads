/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the CorrespondenceCaresettingtype vocabulary:
 * <ul>
 *   <li>_278032008 : Preventive service</li>
 *   <li>_310003005 : Child assessment service</li>
 *   <li>_310004004 : Audiological service</li>
 *   <li>_310005003 : Diagnostic audiology service</li>
 *   <li>_310006002 : Adult diagnostic audiology service</li>
 *   <li>_310007006 : Paediatric diagnostic audiology service</li>
 *   <li>_310008001 : Audiological screening service</li>
 *   <li>_310009009 : Neonatal audiological screening service</li>
 *   <li>_310010004 : Distraction test audiological screening service</li>
 *   <li>_310011000 : Aural rehabilitation service</li>
 *   <li>_310012007 : Cochlear implant service</li>
 *   <li>_310013002 : Adult cochlear implant service</li>
 *   <li>_310014008 : Paediatric cochlear implant service</li>
 *   <li>_310015009 : Hearing aid service</li>
 *   <li>_310016005 : Adult hearing aid service</li>
 *   <li>_310017001 : Paediatric hearing aid service</li>
 *   <li>_310018006 : Speech-reading training service</li>
 *   <li>_310019003 : Tinnitus management service</li>
 *   <li>_310020009 : Hearing therapy service</li>
 *   <li>_310021008 : Assistive listening device service</li>
 *   <li>_310023006 : Radiotherapy service</li>
 *   <li>_310024000 : Colposcopy service</li>
 *   <li>_310025004 : Complementary therapy service</li>
 *   <li>_310026003 : Counselling service</li>
 *   <li>_310027007 : Mental health counselling service</li>
 *   <li>_310028002 : Diagnostic investigation service</li>
 *   <li>_310029005 : Domiciliary visit service</li>
 *   <li>_310030000 : Endoscopy service</li>
 *   <li>_310031001 : Family planning service</li>
 *   <li>_310062002 : Pregnancy termination service</li>
 *   <li>_310064001 : Occupational health service</li>
 *   <li>_310065000 : Open access service</li>
 *   <li>_310067008 : Community paediatric service</li>
 *   <li>_310070007 : Special care baby service</li>
 *   <li>_310072004 : Acute pain service</li>
 *   <li>_310073009 : Palliative care service</li>
 *   <li>_310076001 : Clinical biochemistry service</li>
 *   <li>_310080006 : Pharmacy service</li>
 *   <li>_310081005 : Professional allied to medicine service</li>
 *   <li>_310082003 : Arts therapy services</li>
 *   <li>_310083008 : Art therapy service</li>
 *   <li>_310084002 : Dance therapy service</li>
 *   <li>_310085001 : Drama therapy service</li>
 *   <li>_310086000 : Music therapy service</li>
 *   <li>_310087009 : Podiatry service</li>
 *   <li>_310088004 : Community-based podiatry service</li>
 *   <li>_310089007 : Hospital-based podiatry service</li>
 *   <li>_310090003 : Dietetics service</li>
 *   <li>_310091004 : Community-based dietetics service</li>
 *   <li>_310092006 : Hospital-based dietetics service</li>
 *   <li>_310093001 : Occupational therapy service</li>
 *   <li>_310094007 : Community-based occupational therapy service</li>
 *   <li>_310095008 : Social services occupational therapy service</li>
 *   <li>_310096009 : Hospital-based occupational therapy service</li>
 *   <li>_310098005 : Hospital-based physiotherapy service</li>
 *   <li>_310099002 : Child physiotherapy service</li>
 *   <li>_310100005 : Play therapy service</li>
 *   <li>_310101009 : Speech and language therapy service</li>
 *   <li>_310102002 : Community-based speech and language therapy service</li>
 *   <li>_310103007 : Hospital-based speech and language therapy service</li>
 *   <li>_310104001 : Child speech and language therapy service</li>
 *   <li>_310105000 : Optometry service</li>
 *   <li>_310106004 : Orthoptics service</li>
 *   <li>_310107008 : Hospital orthoptics service</li>
 *   <li>_310108003 : Community orthoptics service</li>
 *   <li>_310109006 : Orthotics service</li>
 *   <li>_310110001 : Hospital orthotics service</li>
 *   <li>_310111002 : Community orthotics service</li>
 *   <li>_310112009 : Surgical fitting service</li>
 *   <li>_310113004 : Hospital surgical fitting service</li>
 *   <li>_310114005 : Community surgical fitting service</li>
 *   <li>_310119000 : Liaison psychiatry service</li>
 *   <li>_310120006 : Mental handicap psychiatry service</li>
 *   <li>_310121005 : Psychogeriatric service</li>
 *   <li>_310122003 : Rehabilitation psychiatry service</li>
 *   <li>_310123008 : Psychology service</li>
 *   <li>_310126000 : Breast screening service</li>
 *   <li>_310127009 : Magnetic resonance imaging service</li>
 *   <li>_310128004 : Computerised tomography service</li>
 *   <li>_310130002 : Head injury rehabilitation service</li>
 *   <li>_310131003 : Community rehabilitation service</li>
 *   <li>_310132005 : Young disabled service</li>
 *   <li>_310133000 : Swallow clinic</li>
 *   <li>_310143002 : Dental surgery service</li>
 *   <li>_310144008 : General dental surgery service</li>
 *   <li>_310150003 : Endocrine surgery service</li>
 *   <li>_310151004 : Gastrointestinal surgery service</li>
 *   <li>_310152006 : General gastrointestinal surgery service</li>
 *   <li>_310157000 : Hand surgery service</li>
 *   <li>_310158005 : Hepatobiliary surgical service</li>
 *   <li>_310161006 : Orthopaedic service</li>
 *   <li>_310162004 : Pancreatic surgery service</li>
 *   <li>_310166001 : Trauma surgery service</li>
 *   <li>_310169008 : Ultrasonography service</li>
 *   <li>_310200001 : Cytology service</li>
 *   <li>_373654008 : Medical referral service</li>
 *   <li>_394538003 : Paediatric neurology</li>
 *   <li>_394539006 : Paediatric surgery</li>
 *   <li>_394576009 : Accident  emergency</li>
 *   <li>_394577000 : Anaesthetics</li>
 *   <li>_394578005 : Audiological medicine</li>
 *   <li>_394579002 : Cardiology</li>
 *   <li>_394580004 : Clinical genetics</li>
 *   <li>_394581000 : Community medicine</li>
 *   <li>_394582007 : Dermatology</li>
 *   <li>_394583002 : Endocrinology</li>
 *   <li>_394584008 : Gastroenterology</li>
 *   <li>_394585009 : Obstetrics and gynaecology</li>
 *   <li>_394586005 : Gynaecology</li>
 *   <li>_394588006 : Child and adolescent psychiatry</li>
 *   <li>_394589003 : Nephrology</li>
 *   <li>_394590007 : Thoracic medicine</li>
 *   <li>_394591006 : Neurology</li>
 *   <li>_394592004 : Clinical oncology</li>
 *   <li>_394593009 : Medical oncology</li>
 *   <li>_394594003 : Ophthalmology</li>
 *   <li>_394595002 : Pathology</li>
 *   <li>_394596001 : Chemical pathology</li>
 *   <li>_394597005 : Histopathology</li>
 *   <li>_394598000 : Immunopathology</li>
 *   <li>_394599008 : Neuropathology</li>
 *   <li>_394600006 : Clinical pharmacology</li>
 *   <li>_394601005 : Clinical physiology</li>
 *   <li>_394602003 : Rehabilitation - speciality</li>
 *   <li>_394603008 : Cardiothoracic surgery</li>
 *   <li>_394604002 : Ear, nose and throat surgery</li>
 *   <li>_394605001 : Oral surgery</li>
 *   <li>_394606000 : Restorative dentistry</li>
 *   <li>_394607009 : Paediatric dentistry</li>
 *   <li>_394608004 : Orthodontics</li>
 *   <li>_394609007 : General surgery</li>
 *   <li>_394610002 : Neurosurgery</li>
 *   <li>_394611003 : Plastic surgery - speciality</li>
 *   <li>_394612005 : Urology</li>
 *   <li>_394649004 : Nuclear medicine - speciality</li>
 *   <li>_394801008 : Trauma  orthopaedics</li>
 *   <li>_394802001 : General medicine</li>
 *   <li>_394803006 : Clinical haematology</li>
 *   <li>_394804000 : Clinical cytogenetics and molecular genetics</li>
 *   <li>_394805004 : Clinical immunologyallergy</li>
 *   <li>_394806003 : Palliative medicine</li>
 *   <li>_394807007 : Infectious diseases specialty</li>
 *   <li>_394808002 : Genito-urinary medicine</li>
 *   <li>_394809005 : Clinical neuro-physiology</li>
 *   <li>_394810000 : Rheumatology</li>
 *   <li>_394811001 : Geriatric medicine</li>
 *   <li>_394812008 : Dental medicine specialties</li>
 *   <li>_394813003 : Medical ophthalmology</li>
 *   <li>_394814009 : General practice specialty</li>
 *   <li>_394815005 : Mental handicap specialty</li>
 *   <li>_394816006 : Mental illness specialty</li>
 *   <li>_394817002 : Forensic psychiatry</li>
 *   <li>_394818007 : Old age psychiatry</li>
 *   <li>_394819004 : Transfusion medicine</li>
 *   <li>_394820005 : Medical microbiology</li>
 *   <li>_394821009 : Occupational medicine</li>
 *   <li>_394882004 : Pain management specialty</li>
 *   <li>_394913002 : Psychotherapy specialty</li>
 *   <li>_394914008 : Radiology - speciality</li>
 *   <li>_394915009 : General pathology specialty</li>
 *   <li>_394916005 : Haematology specialty</li>
 *   <li>_395086005 : Community specialist palliative care</li>
 *   <li>_395092004 : Specialist palliative care</li>
 *   <li>_395104009 : Cancer primary healthcare multidisciplinary team</li>
 *   <li>_408439002 : Allergy - speciality</li>
 *   <li>_408440000 : Public health medicine</li>
 *   <li>_408441001 : Endodontics - speciality</li>
 *   <li>_408442008 : Haemophilia - speciality</li>
 *   <li>_408443003 : General medical practice</li>
 *   <li>_408444009 : General dental practice</li>
 *   <li>_408445005 : Neonatology</li>
 *   <li>_408446006 : Gynaecological oncology</li>
 *   <li>_408447002 : Respite care - speciality</li>
 *   <li>_408448007 : Tropical medicine</li>
 *   <li>_408449004 : Surgical dentistry</li>
 *   <li>_408450004 : Sleep studies - speciality</li>
 *   <li>_408451000 : Community learning disabilities team</li>
 *   <li>_408452007 : Behavioural intervention team</li>
 *   <li>_408454008 : Clinical microbiology</li>
 *   <li>_408455009 : Interventional radiology - speciality</li>
 *   <li>_408456005 : Thoracic surgery</li>
 *   <li>_408457001 : Maxillofacial surgery</li>
 *   <li>_408459003 : Paediatric cardiology</li>
 *   <li>_408460008 : Prosthodontics</li>
 *   <li>_408461007 : Periodontics</li>
 *   <li>_408462000 : Burns care</li>
 *   <li>_408463005 : Vascular surgery</li>
 *   <li>_408464004 : Colorectal surgery</li>
 *   <li>_408465003 : Oral and maxillofacial surgery</li>
 *   <li>_408466002 : Cardiac surgery</li>
 *   <li>_408467006 : Adult mental illness - speciality</li>
 *   <li>_408468001 : Learning disability - speciality</li>
 *   <li>_408469009 : Breast surgery</li>
 *   <li>_408470005 : Obstetrics</li>
 *   <li>_408471009 : Cardiothoracic transplantation</li>
 *   <li>_408472002 : Hepatology</li>
 *   <li>_408473007 : Public health dentistry</li>
 *   <li>_408474001 : Hepatobiliary and pancreatic surgery</li>
 *   <li>_408475000 : Diabetic medicine</li>
 *   <li>_408477008 : Transplantation surgery</li>
 *   <li>_408478003 : Critical care medicine</li>
 *   <li>_408479006 : Upper gastrointestinal surgery</li>
 *   <li>_408480009 : Clinical immunology</li>
 *   <li>_409967009 : Toxicology</li>
 *   <li>_409968004 : Preventive medicine</li>
 *   <li>_409971007 : Emergency medical services</li>
 *   <li>_410001006 : Military medicine</li>
 *   <li>_410005002 : Dive medicine</li>
 *   <li>_413294000 : Community health services</li>
 *   <li>_413299005 : Early years services</li>
 *   <li>_416304004 : Osteopathic manipulative medicine</li>
 *   <li>_417887005 : Paediatric otolaryngology</li>
 *   <li>_418002000 : Paediatric oncology</li>
 *   <li>_418018006 : Dermatologic surgery</li>
 *   <li>_418058008 : Paediatric gastroenterology</li>
 *   <li>_418112009 : Pulmonary medicine</li>
 *   <li>_418535003 : Paediatric immunology</li>
 *   <li>_418652005 : Paediatric haematology</li>
 *   <li>_418862001 : Paediatric infectious diseases</li>
 *   <li>_418960008 : Otolaryngology</li>
 *   <li>_419043006 : Urological oncology</li>
 *   <li>_419170002 : Paediatric pulmonology</li>
 *   <li>_419192003 : Internal medicine</li>
 *   <li>_419215006 : Paediatric intensive care</li>
 *   <li>_419321007 : Surgical oncology</li>
 *   <li>_419365004 : Paediatric nephrology</li>
 *   <li>_419472004 : Paediatric rheumatology</li>
 *   <li>_419610006 : Paediatric endocrinology</li>
 *   <li>_419677000 : Paediatric neurology oncology</li>
 *   <li>_419772000 : Family practice</li>
 *   <li>_419815003 : Radiation oncology</li>
 *   <li>_419917007 : Paediatric emergency medicine</li>
 *   <li>_419983000 : Paediatric ophthalmology</li>
 *   <li>_420112009 : Paediatric bone marrow transplantation</li>
 *   <li>_420208008 : Paediatric genetics</li>
 *   <li>_420419008 : Strabismus surgery</li>
 *   <li>_420708009 : Refractive surgery</li>
 *   <li>_420985004 : Vitreo-retinal surgery</li>
 *   <li>_421582004 : Corneal surgery</li>
 *   <li>_421661004 : Blood banking and transfusion medicine specialty</li>
 *   <li>_421885009 : Glaucoma surgery</li>
 *   <li>_422171003 : Ophthalmic plastic surgery</li>
 *   <li>_422191005 : Ophthalmic surgery</li>
 *   <li>_422349000 : Cataract surgery</li>
 *   <li>_444933003 : Home hospice service</li>
 *   <li>_445449000 : Acute care hospice service</li>
 *   <li>_445715009 : Blood and marrow transplantation</li>
 *   <li>_699650006 : Community based physiotherapy service</li>
 *   <li>_705150003 : Domiciliary physiotherapy service</li>
 *   <li>_708171007 : Vascular ultrasound service</li>
 *   <li>_708172000 : Cardiac ultrasound service</li>
 *   <li>_708173005 : Obstetric ultrasound service</li>
 *   <li>_708174004 : Interventional radiology service</li>
 *   <li>_2461000175101 : Pulmonary rehabilitation service</li>
 *   <li>_89301000000108 : Community mental health team</li>
 *   <li>_89311000000105 : Crisis prevention assessment and treatment team</li>
 *   <li>_90741000000103 : Midwife episode - specialty</li>
 *   <li>_90891000000100 : Community health services medical - specialty</li>
 *   <li>_90901000000104 : Community health services dental - specialty</li>
 *   <li>_91901000000109 : Assertive outreach team</li>
 *   <li>_92151000000102 : Mental health crisis resolution team</li>
 *   <li>_92191000000105 : Early intervention in psychosis team</li>
 *   <li>_92221000000103 : Mental health home treatment team</li>
 *   <li>_108501000000103 : Well babies - specialty</li>
 *   <li>_109201000000109 : Substance misuse team</li>
 *   <li>_109581000000102 : Intermediate care - specialty</li>
 *   <li>_321391000000108 : Genetic science</li>
 *   <li>_321401000000106 : Genomics</li>
 *   <li>_827611000000102 : Acute medicine</li>
 *   <li>_827621000000108 : Addiction service</li>
 *   <li>_827631000000105 : Emergency ambulance service</li>
 *   <li>_827641000000101 : Anticoagulant service</li>
 *   <li>_827651000000103 : Cardiac rehabilitation service</li>
 *   <li>_827671000000107 : Community learning disability nursing</li>
 *   <li>_827681000000109 : Community midwifery</li>
 *   <li>_827691000000106 : Community psychiatric nursing</li>
 *   <li>_827961000000107 : Paediatric audiological medicine</li>
 *   <li>_827971000000100 : Paediatric burns care</li>
 *   <li>_827981000000103 : Paediatric cystic fibrosis service</li>
 *   <li>_827991000000101 : Paediatric dermatology</li>
 *   <li>_828021000000101 : Perinatal psychiatry</li>
 *   <li>_828031000000104 : Paediatric diabetic medicine</li>
 *   <li>_828061000000109 : Paediatric gastrointestinal surgery</li>
 *   <li>_828071000000102 : Paediatric maxillofacial surgery</li>
 *   <li>_828081000000100 : Paediatric cardiac surgery</li>
 *   <li>_828121000000102 : Paediatric neurosurgery</li>
 *   <li>_828131000000100 : Paediatric plastic surgery</li>
 *   <li>_828141000000109 : Paediatric urology</li>
 *   <li>_828151000000107 : Audiometry</li>
 *   <li>_828161000000105 : Dental and maxillofacial radiology</li>
 *   <li>_828171000000103 : Paediatric interventional radiology</li>
 *   <li>_828181000000101 : Community sexual and reproductive health</li>
 *   <li>_828191000000104 : Dental hygiene service</li>
 *   <li>_828201000000102 : Dental surgery assistance service</li>
 *   <li>_828211000000100 : Paediatric thoracic surgery</li>
 *   <li>_828221000000106 : Paediatric transplantation surgery</li>
 *   <li>_828241000000104 : Paediatric trauma and orthopaedics</li>
 *   <li>_828251000000101 : Oral pathology</li>
 *   <li>_828261000000103 : Stroke medicine</li>
 *   <li>_828271000000105 : Diagnostic imaging - specialty</li>
 *   <li>_828281000000107 : Eating disorders service</li>
 *   <li>_828291000000109 : Dispensing optometry service</li>
 *   <li>_828301000000108 : Electrocardiography service</li>
 *   <li>_828311000000105 : General nursing</li>
 *   <li>_828321000000104 : Health visiting</li>
 *   <li>_828331000000102 : Homeopathy service</li>
 *   <li>_828341000000106 : Learning disability nursing</li>
 *   <li>_828351000000109 : Medical photography</li>
 *   <li>_828361000000107 : Mental health nursing</li>
 *   <li>_828371000000100 : Well man service</li>
 *   <li>_828381000000103 : Well woman service</li>
 *   <li>_828391000000101 : Special care dentistry</li>
 *   <li>_828401000000103 : School nursing</li>
 *   <li>_828411000000101 : Childrens nursing</li>
 *   <li>_828421000000107 : Respiratory physiology</li>
 *   <li>_828431000000109 : Prosthetics</li>
 *   <li>_828441000000100 : Paediatric pain management</li>
 *   <li>_828451000000102 : Psychiatric intensive care</li>
 *   <li>_828461000000104 : Electroencephalography</li>
 *   <li>_828491000000105 : Oral medicine</li>
 *   <li>_828511000000102 : NHS 24</li>
 *   <li>_828521000000108 : NHS Direct</li>
 *   <li>_828531000000105 : Nursery nursing</li>
 *   <li>_828541000000101 : Obstetrics - antenatal</li>
 *   <li>_828551000000103 : Obstetrics - postnatal</li>
 *   <li>_828811000000104 : Child psychiatry service</li>
 *   <li>_828821000000105 : Adolescent psychiatry service</li>
 *   <li>_828831000000107 : Physiological measurement - specialty</li>
 *   <li>_828841000000103 : Dental therapy</li>
 *   <li>_828851000000100 : Diagnostic radiography</li>
 *   <li>_828861000000102 : Programmed pulmonary rehabilitation service</li>
 *   <li>_828871000000109 : GP general practice obstetrics</li>
 *   <li>_828881000000106 : Intensive care medicine</li>
 *   <li>_828891000000108 : Medical physics</li>
 *   <li>_828901000000109 : Endocrinology and diabetes</li>
 *   <li>_828911000000106 : Surgical podiatry</li>
 *   <li>_828921000000100 : Therapeutic radiography</li>
 *   <li>_828931000000103 : Virology</li>
 *   <li>_828941000000107 : Oral microbiology</li>
 *   <li>_828951000000105 : Paediatric neurodisability</li>
 *   <li>_829131000000107 : Paediatric immunology and allergy</li>
 *   <li>_829141000000103 : Paediatric metabolic medicine</li>
 *   <li>_829151000000100 : Prosthetics or orthotics</li>
 *   <li>_829211000000107 : Ultrasound diagnostic imaging - specialty</li>
 *   <li>_829951000000102 : Industrial therapy service</li>
 *   <li>_829961000000104 : Out of hours service</li>
 *   <li>_829971000000106 : Physiotherapy service</li>
 *   <li>_829981000000108 : Community child health service</li>
 *   <li>_829991000000105 : Medical microbiology and virology</li>
 *   <li>_892731000000107 : Dental medicine service</li>
 *   <li>_892811000000109 : Adult mental health service</li>
 *   <li>_893041000000108 : Transient ischaemic attack service</li>
 *   <li>_893051000000106 : Clinical allergy service</li>
 *   <li>_893081000000100 : Respiratory medicine service</li>
 *   <li>_893121000000102 : Spinal surgery service</li>
 *   <li>_893201000000102 : Sport and exercise medicine service</li>
 *   <li>_893211000000100 : Spinal injuries service</li>
 *   <li>_893221000000106 : Specialist rehabilitation service</li>
 *   <li>_893231000000108 : Podiatric surgery service</li>
 *   <li>_893241000000104 : Midwifery service</li>
 *   <li>_893251000000101 : Mental health recovery and rehabilitation service</li>
 *   <li>_893261000000103 : Mental health dual diagnosis service</li>
 *   <li>_893271000000105 : Medical virology service</li>
 *   <li>_893301000000108 : Local specialist rehabilitation service</li>
 *   <li>_893321000000104 : Diabetic education service</li>
 *   <li>_893331000000102 : Dementia assessment service</li>
 *   <li>_893341000000106 : Congenital heart disease service</li>
 *   <li>_893351000000109 : Complex specialised rehabilitation service</li>
 *   <li>_893381000000103 : Clinical psychology service</li>
 *   <li>_893391000000101 : Adult cystic fibrosis service</li>
 *   <li>_893601000000100 : Paediatric medical oncology service</li>
 *   <li>_893611000000103 : Paediatric epilepsy service</li>
 *   <li>_893621000000109 : Paediatric ear nose and throat service</li>
 *   <li>_893651000000104 : Paediatric clinical immunology and allergy service</li>
 *   <li>_893661000000101 : Paediatric clinical haematology service</li>
 *   <li>_893711000000109 : Neonatal critical care service</li>
 *   <li>_893771000000104 : Paediatric respiratory medicine service</li>
 *   <li>_901221000000102 : Perinatal mental health service</li>
 *   <li>_907271000000106 : Genetics laboratory service</li>
 *   <li>_907301000000109 : NHS 111 service</li>
 *   <li>_908981000000101 : Remote triage and advice service</li>
 *   <li>_911221000000100 : Telecare monitoring service</li>
 *   <li>_911231000000103 : Telehealth monitoring service</li>
 *   <li>_911381000000108 : Telehealthcare service</li>
 *   <li>_931791000000100 : Community midwifery service</li>
 *   <li>_931801000000101 : Community nursing service</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum CorrespondenceCaresettingtype implements VocabularyEntry {
	
	
	/**
	 * Preventive service
	 */
	_278032008 ("278032008", "Preventive service"),
	
	/**
	 * Child assessment service
	 */
	_310003005 ("310003005", "Child assessment service"),
	
	/**
	 * Audiological service
	 */
	_310004004 ("310004004", "Audiological service"),
	
	/**
	 * Diagnostic audiology service
	 */
	_310005003 ("310005003", "Diagnostic audiology service"),
	
	/**
	 * Adult diagnostic audiology service
	 */
	_310006002 ("310006002", "Adult diagnostic audiology service"),
	
	/**
	 * Paediatric diagnostic audiology service
	 */
	_310007006 ("310007006", "Paediatric diagnostic audiology service"),
	
	/**
	 * Audiological screening service
	 */
	_310008001 ("310008001", "Audiological screening service"),
	
	/**
	 * Neonatal audiological screening service
	 */
	_310009009 ("310009009", "Neonatal audiological screening service"),
	
	/**
	 * Distraction test audiological screening service
	 */
	_310010004 ("310010004", "Distraction test audiological screening service"),
	
	/**
	 * Aural rehabilitation service
	 */
	_310011000 ("310011000", "Aural rehabilitation service"),
	
	/**
	 * Cochlear implant service
	 */
	_310012007 ("310012007", "Cochlear implant service"),
	
	/**
	 * Adult cochlear implant service
	 */
	_310013002 ("310013002", "Adult cochlear implant service"),
	
	/**
	 * Paediatric cochlear implant service
	 */
	_310014008 ("310014008", "Paediatric cochlear implant service"),
	
	/**
	 * Hearing aid service
	 */
	_310015009 ("310015009", "Hearing aid service"),
	
	/**
	 * Adult hearing aid service
	 */
	_310016005 ("310016005", "Adult hearing aid service"),
	
	/**
	 * Paediatric hearing aid service
	 */
	_310017001 ("310017001", "Paediatric hearing aid service"),
	
	/**
	 * Speech-reading training service
	 */
	_310018006 ("310018006", "Speech-reading training service"),
	
	/**
	 * Tinnitus management service
	 */
	_310019003 ("310019003", "Tinnitus management service"),
	
	/**
	 * Hearing therapy service
	 */
	_310020009 ("310020009", "Hearing therapy service"),
	
	/**
	 * Assistive listening device service
	 */
	_310021008 ("310021008", "Assistive listening device service"),
	
	/**
	 * Radiotherapy service
	 */
	_310023006 ("310023006", "Radiotherapy service"),
	
	/**
	 * Colposcopy service
	 */
	_310024000 ("310024000", "Colposcopy service"),
	
	/**
	 * Complementary therapy service
	 */
	_310025004 ("310025004", "Complementary therapy service"),
	
	/**
	 * Counselling service
	 */
	_310026003 ("310026003", "Counselling service"),
	
	/**
	 * Mental health counselling service
	 */
	_310027007 ("310027007", "Mental health counselling service"),
	
	/**
	 * Diagnostic investigation service
	 */
	_310028002 ("310028002", "Diagnostic investigation service"),
	
	/**
	 * Domiciliary visit service
	 */
	_310029005 ("310029005", "Domiciliary visit service"),
	
	/**
	 * Endoscopy service
	 */
	_310030000 ("310030000", "Endoscopy service"),
	
	/**
	 * Family planning service
	 */
	_310031001 ("310031001", "Family planning service"),
	
	/**
	 * Pregnancy termination service
	 */
	_310062002 ("310062002", "Pregnancy termination service"),
	
	/**
	 * Occupational health service
	 */
	_310064001 ("310064001", "Occupational health service"),
	
	/**
	 * Open access service
	 */
	_310065000 ("310065000", "Open access service"),
	
	/**
	 * Community paediatric service
	 */
	_310067008 ("310067008", "Community paediatric service"),
	
	/**
	 * Special care baby service
	 */
	_310070007 ("310070007", "Special care baby service"),
	
	/**
	 * Acute pain service
	 */
	_310072004 ("310072004", "Acute pain service"),
	
	/**
	 * Palliative care service
	 */
	_310073009 ("310073009", "Palliative care service"),
	
	/**
	 * Clinical biochemistry service
	 */
	_310076001 ("310076001", "Clinical biochemistry service"),
	
	/**
	 * Pharmacy service
	 */
	_310080006 ("310080006", "Pharmacy service"),
	
	/**
	 * Professional allied to medicine service
	 */
	_310081005 ("310081005", "Professional allied to medicine service"),
	
	/**
	 * Arts therapy services
	 */
	_310082003 ("310082003", "Arts therapy services"),
	
	/**
	 * Art therapy service
	 */
	_310083008 ("310083008", "Art therapy service"),
	
	/**
	 * Dance therapy service
	 */
	_310084002 ("310084002", "Dance therapy service"),
	
	/**
	 * Drama therapy service
	 */
	_310085001 ("310085001", "Drama therapy service"),
	
	/**
	 * Music therapy service
	 */
	_310086000 ("310086000", "Music therapy service"),
	
	/**
	 * Podiatry service
	 */
	_310087009 ("310087009", "Podiatry service"),
	
	/**
	 * Community-based podiatry service
	 */
	_310088004 ("310088004", "Community-based podiatry service"),
	
	/**
	 * Hospital-based podiatry service
	 */
	_310089007 ("310089007", "Hospital-based podiatry service"),
	
	/**
	 * Dietetics service
	 */
	_310090003 ("310090003", "Dietetics service"),
	
	/**
	 * Community-based dietetics service
	 */
	_310091004 ("310091004", "Community-based dietetics service"),
	
	/**
	 * Hospital-based dietetics service
	 */
	_310092006 ("310092006", "Hospital-based dietetics service"),
	
	/**
	 * Occupational therapy service
	 */
	_310093001 ("310093001", "Occupational therapy service"),
	
	/**
	 * Community-based occupational therapy service
	 */
	_310094007 ("310094007", "Community-based occupational therapy service"),
	
	/**
	 * Social services occupational therapy service
	 */
	_310095008 ("310095008", "Social services occupational therapy service"),
	
	/**
	 * Hospital-based occupational therapy service
	 */
	_310096009 ("310096009", "Hospital-based occupational therapy service"),
	
	/**
	 * Hospital-based physiotherapy service
	 */
	_310098005 ("310098005", "Hospital-based physiotherapy service"),
	
	/**
	 * Child physiotherapy service
	 */
	_310099002 ("310099002", "Child physiotherapy service"),
	
	/**
	 * Play therapy service
	 */
	_310100005 ("310100005", "Play therapy service"),
	
	/**
	 * Speech and language therapy service
	 */
	_310101009 ("310101009", "Speech and language therapy service"),
	
	/**
	 * Community-based speech and language therapy service
	 */
	_310102002 ("310102002", "Community-based speech and language therapy service"),
	
	/**
	 * Hospital-based speech and language therapy service
	 */
	_310103007 ("310103007", "Hospital-based speech and language therapy service"),
	
	/**
	 * Child speech and language therapy service
	 */
	_310104001 ("310104001", "Child speech and language therapy service"),
	
	/**
	 * Optometry service
	 */
	_310105000 ("310105000", "Optometry service"),
	
	/**
	 * Orthoptics service
	 */
	_310106004 ("310106004", "Orthoptics service"),
	
	/**
	 * Hospital orthoptics service
	 */
	_310107008 ("310107008", "Hospital orthoptics service"),
	
	/**
	 * Community orthoptics service
	 */
	_310108003 ("310108003", "Community orthoptics service"),
	
	/**
	 * Orthotics service
	 */
	_310109006 ("310109006", "Orthotics service"),
	
	/**
	 * Hospital orthotics service
	 */
	_310110001 ("310110001", "Hospital orthotics service"),
	
	/**
	 * Community orthotics service
	 */
	_310111002 ("310111002", "Community orthotics service"),
	
	/**
	 * Surgical fitting service
	 */
	_310112009 ("310112009", "Surgical fitting service"),
	
	/**
	 * Hospital surgical fitting service
	 */
	_310113004 ("310113004", "Hospital surgical fitting service"),
	
	/**
	 * Community surgical fitting service
	 */
	_310114005 ("310114005", "Community surgical fitting service"),
	
	/**
	 * Liaison psychiatry service
	 */
	_310119000 ("310119000", "Liaison psychiatry service"),
	
	/**
	 * Mental handicap psychiatry service
	 */
	_310120006 ("310120006", "Mental handicap psychiatry service"),
	
	/**
	 * Psychogeriatric service
	 */
	_310121005 ("310121005", "Psychogeriatric service"),
	
	/**
	 * Rehabilitation psychiatry service
	 */
	_310122003 ("310122003", "Rehabilitation psychiatry service"),
	
	/**
	 * Psychology service
	 */
	_310123008 ("310123008", "Psychology service"),
	
	/**
	 * Breast screening service
	 */
	_310126000 ("310126000", "Breast screening service"),
	
	/**
	 * Magnetic resonance imaging service
	 */
	_310127009 ("310127009", "Magnetic resonance imaging service"),
	
	/**
	 * Computerised tomography service
	 */
	_310128004 ("310128004", "Computerised tomography service"),
	
	/**
	 * Head injury rehabilitation service
	 */
	_310130002 ("310130002", "Head injury rehabilitation service"),
	
	/**
	 * Community rehabilitation service
	 */
	_310131003 ("310131003", "Community rehabilitation service"),
	
	/**
	 * Young disabled service
	 */
	_310132005 ("310132005", "Young disabled service"),
	
	/**
	 * Swallow clinic
	 */
	_310133000 ("310133000", "Swallow clinic"),
	
	/**
	 * Dental surgery service
	 */
	_310143002 ("310143002", "Dental surgery service"),
	
	/**
	 * General dental surgery service
	 */
	_310144008 ("310144008", "General dental surgery service"),
	
	/**
	 * Endocrine surgery service
	 */
	_310150003 ("310150003", "Endocrine surgery service"),
	
	/**
	 * Gastrointestinal surgery service
	 */
	_310151004 ("310151004", "Gastrointestinal surgery service"),
	
	/**
	 * General gastrointestinal surgery service
	 */
	_310152006 ("310152006", "General gastrointestinal surgery service"),
	
	/**
	 * Hand surgery service
	 */
	_310157000 ("310157000", "Hand surgery service"),
	
	/**
	 * Hepatobiliary surgical service
	 */
	_310158005 ("310158005", "Hepatobiliary surgical service"),
	
	/**
	 * Orthopaedic service
	 */
	_310161006 ("310161006", "Orthopaedic service"),
	
	/**
	 * Pancreatic surgery service
	 */
	_310162004 ("310162004", "Pancreatic surgery service"),
	
	/**
	 * Trauma surgery service
	 */
	_310166001 ("310166001", "Trauma surgery service"),
	
	/**
	 * Ultrasonography service
	 */
	_310169008 ("310169008", "Ultrasonography service"),
	
	/**
	 * Cytology service
	 */
	_310200001 ("310200001", "Cytology service"),
	
	/**
	 * Medical referral service
	 */
	_373654008 ("373654008", "Medical referral service"),
	
	/**
	 * Paediatric neurology
	 */
	_394538003 ("394538003", "Paediatric neurology"),
	
	/**
	 * Paediatric surgery
	 */
	_394539006 ("394539006", "Paediatric surgery"),
	
	/**
	 * Accident  emergency
	 */
	_394576009 ("394576009", "Accident & emergency"),
	
	/**
	 * Anaesthetics
	 */
	_394577000 ("394577000", "Anaesthetics"),
	
	/**
	 * Audiological medicine
	 */
	_394578005 ("394578005", "Audiological medicine"),
	
	/**
	 * Cardiology
	 */
	_394579002 ("394579002", "Cardiology"),
	
	/**
	 * Clinical genetics
	 */
	_394580004 ("394580004", "Clinical genetics"),
	
	/**
	 * Community medicine
	 */
	_394581000 ("394581000", "Community medicine"),
	
	/**
	 * Dermatology
	 */
	_394582007 ("394582007", "Dermatology"),
	
	/**
	 * Endocrinology
	 */
	_394583002 ("394583002", "Endocrinology"),
	
	/**
	 * Gastroenterology
	 */
	_394584008 ("394584008", "Gastroenterology"),
	
	/**
	 * Obstetrics and gynaecology
	 */
	_394585009 ("394585009", "Obstetrics and gynaecology"),
	
	/**
	 * Gynaecology
	 */
	_394586005 ("394586005", "Gynaecology"),
	
	/**
	 * Child and adolescent psychiatry
	 */
	_394588006 ("394588006", "Child and adolescent psychiatry"),
	
	/**
	 * Nephrology
	 */
	_394589003 ("394589003", "Nephrology"),
	
	/**
	 * Thoracic medicine
	 */
	_394590007 ("394590007", "Thoracic medicine"),
	
	/**
	 * Neurology
	 */
	_394591006 ("394591006", "Neurology"),
	
	/**
	 * Clinical oncology
	 */
	_394592004 ("394592004", "Clinical oncology"),
	
	/**
	 * Medical oncology
	 */
	_394593009 ("394593009", "Medical oncology"),
	
	/**
	 * Ophthalmology
	 */
	_394594003 ("394594003", "Ophthalmology"),
	
	/**
	 * Pathology
	 */
	_394595002 ("394595002", "Pathology"),
	
	/**
	 * Chemical pathology
	 */
	_394596001 ("394596001", "Chemical pathology"),
	
	/**
	 * Histopathology
	 */
	_394597005 ("394597005", "Histopathology"),
	
	/**
	 * Immunopathology
	 */
	_394598000 ("394598000", "Immunopathology"),
	
	/**
	 * Neuropathology
	 */
	_394599008 ("394599008", "Neuropathology"),
	
	/**
	 * Clinical pharmacology
	 */
	_394600006 ("394600006", "Clinical pharmacology"),
	
	/**
	 * Clinical physiology
	 */
	_394601005 ("394601005", "Clinical physiology"),
	
	/**
	 * Rehabilitation - speciality
	 */
	_394602003 ("394602003", "Rehabilitation - speciality"),
	
	/**
	 * Cardiothoracic surgery
	 */
	_394603008 ("394603008", "Cardiothoracic surgery"),
	
	/**
	 * Ear, nose and throat surgery
	 */
	_394604002 ("394604002", "Ear, nose and throat surgery"),
	
	/**
	 * Oral surgery
	 */
	_394605001 ("394605001", "Oral surgery"),
	
	/**
	 * Restorative dentistry
	 */
	_394606000 ("394606000", "Restorative dentistry"),
	
	/**
	 * Paediatric dentistry
	 */
	_394607009 ("394607009", "Paediatric dentistry"),
	
	/**
	 * Orthodontics
	 */
	_394608004 ("394608004", "Orthodontics"),
	
	/**
	 * General surgery
	 */
	_394609007 ("394609007", "General surgery"),
	
	/**
	 * Neurosurgery
	 */
	_394610002 ("394610002", "Neurosurgery"),
	
	/**
	 * Plastic surgery - speciality
	 */
	_394611003 ("394611003", "Plastic surgery - speciality"),
	
	/**
	 * Urology
	 */
	_394612005 ("394612005", "Urology"),
	
	/**
	 * Nuclear medicine - speciality
	 */
	_394649004 ("394649004", "Nuclear medicine - speciality"),
	
	/**
	 * Trauma  orthopaedics
	 */
	_394801008 ("394801008", "Trauma & orthopaedics"),
	
	/**
	 * General medicine
	 */
	_394802001 ("394802001", "General medicine"),
	
	/**
	 * Clinical haematology
	 */
	_394803006 ("394803006", "Clinical haematology"),
	
	/**
	 * Clinical cytogenetics and molecular genetics
	 */
	_394804000 ("394804000", "Clinical cytogenetics and molecular genetics"),
	
	/**
	 * Clinical immunologyallergy
	 */
	_394805004 ("394805004", "Clinical immunology/allergy"),
	
	/**
	 * Palliative medicine
	 */
	_394806003 ("394806003", "Palliative medicine"),
	
	/**
	 * Infectious diseases specialty
	 */
	_394807007 ("394807007", "Infectious diseases (specialty)"),
	
	/**
	 * Genito-urinary medicine
	 */
	_394808002 ("394808002", "Genito-urinary medicine"),
	
	/**
	 * Clinical neuro-physiology
	 */
	_394809005 ("394809005", "Clinical neuro-physiology"),
	
	/**
	 * Rheumatology
	 */
	_394810000 ("394810000", "Rheumatology"),
	
	/**
	 * Geriatric medicine
	 */
	_394811001 ("394811001", "Geriatric medicine"),
	
	/**
	 * Dental medicine specialties
	 */
	_394812008 ("394812008", "Dental medicine specialties"),
	
	/**
	 * Medical ophthalmology
	 */
	_394813003 ("394813003", "Medical ophthalmology"),
	
	/**
	 * General practice specialty
	 */
	_394814009 ("394814009", "General practice (specialty)"),
	
	/**
	 * Mental handicap specialty
	 */
	_394815005 ("394815005", "Mental handicap (specialty)"),
	
	/**
	 * Mental illness specialty
	 */
	_394816006 ("394816006", "Mental illness (specialty)"),
	
	/**
	 * Forensic psychiatry
	 */
	_394817002 ("394817002", "Forensic psychiatry"),
	
	/**
	 * Old age psychiatry
	 */
	_394818007 ("394818007", "Old age psychiatry"),
	
	/**
	 * Transfusion medicine
	 */
	_394819004 ("394819004", "Transfusion medicine"),
	
	/**
	 * Medical microbiology
	 */
	_394820005 ("394820005", "Medical microbiology"),
	
	/**
	 * Occupational medicine
	 */
	_394821009 ("394821009", "Occupational medicine"),
	
	/**
	 * Pain management specialty
	 */
	_394882004 ("394882004", "Pain management (specialty)"),
	
	/**
	 * Psychotherapy specialty
	 */
	_394913002 ("394913002", "Psychotherapy (specialty)"),
	
	/**
	 * Radiology - speciality
	 */
	_394914008 ("394914008", "Radiology - speciality"),
	
	/**
	 * General pathology specialty
	 */
	_394915009 ("394915009", "General pathology (specialty)"),
	
	/**
	 * Haematology specialty
	 */
	_394916005 ("394916005", "Haematology (specialty)"),
	
	/**
	 * Community specialist palliative care
	 */
	_395086005 ("395086005", "Community specialist palliative care"),
	
	/**
	 * Specialist palliative care
	 */
	_395092004 ("395092004", "Specialist palliative care"),
	
	/**
	 * Cancer primary healthcare multidisciplinary team
	 */
	_395104009 ("395104009", "Cancer primary healthcare multidisciplinary team"),
	
	/**
	 * Allergy - speciality
	 */
	_408439002 ("408439002", "Allergy - speciality"),
	
	/**
	 * Public health medicine
	 */
	_408440000 ("408440000", "Public health medicine"),
	
	/**
	 * Endodontics - speciality
	 */
	_408441001 ("408441001", "Endodontics - speciality"),
	
	/**
	 * Haemophilia - speciality
	 */
	_408442008 ("408442008", "Haemophilia - speciality"),
	
	/**
	 * General medical practice
	 */
	_408443003 ("408443003", "General medical practice"),
	
	/**
	 * General dental practice
	 */
	_408444009 ("408444009", "General dental practice"),
	
	/**
	 * Neonatology
	 */
	_408445005 ("408445005", "Neonatology"),
	
	/**
	 * Gynaecological oncology
	 */
	_408446006 ("408446006", "Gynaecological oncology"),
	
	/**
	 * Respite care - speciality
	 */
	_408447002 ("408447002", "Respite care - speciality"),
	
	/**
	 * Tropical medicine
	 */
	_408448007 ("408448007", "Tropical medicine"),
	
	/**
	 * Surgical dentistry
	 */
	_408449004 ("408449004", "Surgical dentistry"),
	
	/**
	 * Sleep studies - speciality
	 */
	_408450004 ("408450004", "Sleep studies - speciality"),
	
	/**
	 * Community learning disabilities team
	 */
	_408451000 ("408451000", "Community learning disabilities team"),
	
	/**
	 * Behavioural intervention team
	 */
	_408452007 ("408452007", "Behavioural intervention team"),
	
	/**
	 * Clinical microbiology
	 */
	_408454008 ("408454008", "Clinical microbiology"),
	
	/**
	 * Interventional radiology - speciality
	 */
	_408455009 ("408455009", "Interventional radiology - speciality"),
	
	/**
	 * Thoracic surgery
	 */
	_408456005 ("408456005", "Thoracic surgery"),
	
	/**
	 * Maxillofacial surgery
	 */
	_408457001 ("408457001", "Maxillofacial surgery"),
	
	/**
	 * Paediatric cardiology
	 */
	_408459003 ("408459003", "Paediatric cardiology"),
	
	/**
	 * Prosthodontics
	 */
	_408460008 ("408460008", "Prosthodontics"),
	
	/**
	 * Periodontics
	 */
	_408461007 ("408461007", "Periodontics"),
	
	/**
	 * Burns care
	 */
	_408462000 ("408462000", "Burns care"),
	
	/**
	 * Vascular surgery
	 */
	_408463005 ("408463005", "Vascular surgery"),
	
	/**
	 * Colorectal surgery
	 */
	_408464004 ("408464004", "Colorectal surgery"),
	
	/**
	 * Oral and maxillofacial surgery
	 */
	_408465003 ("408465003", "Oral and maxillofacial surgery"),
	
	/**
	 * Cardiac surgery
	 */
	_408466002 ("408466002", "Cardiac surgery"),
	
	/**
	 * Adult mental illness - speciality
	 */
	_408467006 ("408467006", "Adult mental illness - speciality"),
	
	/**
	 * Learning disability - speciality
	 */
	_408468001 ("408468001", "Learning disability - speciality"),
	
	/**
	 * Breast surgery
	 */
	_408469009 ("408469009", "Breast surgery"),
	
	/**
	 * Obstetrics
	 */
	_408470005 ("408470005", "Obstetrics"),
	
	/**
	 * Cardiothoracic transplantation
	 */
	_408471009 ("408471009", "Cardiothoracic transplantation"),
	
	/**
	 * Hepatology
	 */
	_408472002 ("408472002", "Hepatology"),
	
	/**
	 * Public health dentistry
	 */
	_408473007 ("408473007", "Public health dentistry"),
	
	/**
	 * Hepatobiliary and pancreatic surgery
	 */
	_408474001 ("408474001", "Hepatobiliary and pancreatic surgery"),
	
	/**
	 * Diabetic medicine
	 */
	_408475000 ("408475000", "Diabetic medicine"),
	
	/**
	 * Transplantation surgery
	 */
	_408477008 ("408477008", "Transplantation surgery"),
	
	/**
	 * Critical care medicine
	 */
	_408478003 ("408478003", "Critical care medicine"),
	
	/**
	 * Upper gastrointestinal surgery
	 */
	_408479006 ("408479006", "Upper gastrointestinal surgery"),
	
	/**
	 * Clinical immunology
	 */
	_408480009 ("408480009", "Clinical immunology"),
	
	/**
	 * Toxicology
	 */
	_409967009 ("409967009", "Toxicology"),
	
	/**
	 * Preventive medicine
	 */
	_409968004 ("409968004", "Preventive medicine"),
	
	/**
	 * Emergency medical services
	 */
	_409971007 ("409971007", "Emergency medical services"),
	
	/**
	 * Military medicine
	 */
	_410001006 ("410001006", "Military medicine"),
	
	/**
	 * Dive medicine
	 */
	_410005002 ("410005002", "Dive medicine"),
	
	/**
	 * Community health services
	 */
	_413294000 ("413294000", "Community health services"),
	
	/**
	 * Early years services
	 */
	_413299005 ("413299005", "Early years services"),
	
	/**
	 * Osteopathic manipulative medicine
	 */
	_416304004 ("416304004", "Osteopathic manipulative medicine"),
	
	/**
	 * Paediatric otolaryngology
	 */
	_417887005 ("417887005", "Paediatric otolaryngology"),
	
	/**
	 * Paediatric oncology
	 */
	_418002000 ("418002000", "Paediatric oncology"),
	
	/**
	 * Dermatologic surgery
	 */
	_418018006 ("418018006", "Dermatologic surgery"),
	
	/**
	 * Paediatric gastroenterology
	 */
	_418058008 ("418058008", "Paediatric gastroenterology"),
	
	/**
	 * Pulmonary medicine
	 */
	_418112009 ("418112009", "Pulmonary medicine"),
	
	/**
	 * Paediatric immunology
	 */
	_418535003 ("418535003", "Paediatric immunology"),
	
	/**
	 * Paediatric haematology
	 */
	_418652005 ("418652005", "Paediatric haematology"),
	
	/**
	 * Paediatric infectious diseases
	 */
	_418862001 ("418862001", "Paediatric infectious diseases"),
	
	/**
	 * Otolaryngology
	 */
	_418960008 ("418960008", "Otolaryngology"),
	
	/**
	 * Urological oncology
	 */
	_419043006 ("419043006", "Urological oncology"),
	
	/**
	 * Paediatric pulmonology
	 */
	_419170002 ("419170002", "Paediatric pulmonology"),
	
	/**
	 * Internal medicine
	 */
	_419192003 ("419192003", "Internal medicine"),
	
	/**
	 * Paediatric intensive care
	 */
	_419215006 ("419215006", "Paediatric intensive care"),
	
	/**
	 * Surgical oncology
	 */
	_419321007 ("419321007", "Surgical oncology"),
	
	/**
	 * Paediatric nephrology
	 */
	_419365004 ("419365004", "Paediatric nephrology"),
	
	/**
	 * Paediatric rheumatology
	 */
	_419472004 ("419472004", "Paediatric rheumatology"),
	
	/**
	 * Paediatric endocrinology
	 */
	_419610006 ("419610006", "Paediatric endocrinology"),
	
	/**
	 * Paediatric neurology oncology
	 */
	_419677000 ("419677000", "Paediatric neurology oncology"),
	
	/**
	 * Family practice
	 */
	_419772000 ("419772000", "Family practice"),
	
	/**
	 * Radiation oncology
	 */
	_419815003 ("419815003", "Radiation oncology"),
	
	/**
	 * Paediatric emergency medicine
	 */
	_419917007 ("419917007", "Paediatric emergency medicine"),
	
	/**
	 * Paediatric ophthalmology
	 */
	_419983000 ("419983000", "Paediatric ophthalmology"),
	
	/**
	 * Paediatric bone marrow transplantation
	 */
	_420112009 ("420112009", "Paediatric bone marrow transplantation"),
	
	/**
	 * Paediatric genetics
	 */
	_420208008 ("420208008", "Paediatric genetics"),
	
	/**
	 * Strabismus surgery
	 */
	_420419008 ("420419008", "Strabismus surgery"),
	
	/**
	 * Refractive surgery
	 */
	_420708009 ("420708009", "Refractive surgery"),
	
	/**
	 * Vitreo-retinal surgery
	 */
	_420985004 ("420985004", "Vitreo-retinal surgery"),
	
	/**
	 * Corneal surgery
	 */
	_421582004 ("421582004", "Corneal surgery"),
	
	/**
	 * Blood banking and transfusion medicine specialty
	 */
	_421661004 ("421661004", "Blood banking and transfusion medicine (specialty)"),
	
	/**
	 * Glaucoma surgery
	 */
	_421885009 ("421885009", "Glaucoma surgery"),
	
	/**
	 * Ophthalmic plastic surgery
	 */
	_422171003 ("422171003", "Ophthalmic plastic surgery"),
	
	/**
	 * Ophthalmic surgery
	 */
	_422191005 ("422191005", "Ophthalmic surgery"),
	
	/**
	 * Cataract surgery
	 */
	_422349000 ("422349000", "Cataract surgery"),
	
	/**
	 * Home hospice service
	 */
	_444933003 ("444933003", "Home hospice service"),
	
	/**
	 * Acute care hospice service
	 */
	_445449000 ("445449000", "Acute care hospice service"),
	
	/**
	 * Blood and marrow transplantation
	 */
	_445715009 ("445715009", "Blood and marrow transplantation"),
	
	/**
	 * Community based physiotherapy service
	 */
	_699650006 ("699650006", "Community based physiotherapy service"),
	
	/**
	 * Domiciliary physiotherapy service
	 */
	_705150003 ("705150003", "Domiciliary physiotherapy service"),
	
	/**
	 * Vascular ultrasound service
	 */
	_708171007 ("708171007", "Vascular ultrasound service"),
	
	/**
	 * Cardiac ultrasound service
	 */
	_708172000 ("708172000", "Cardiac ultrasound service"),
	
	/**
	 * Obstetric ultrasound service
	 */
	_708173005 ("708173005", "Obstetric ultrasound service"),
	
	/**
	 * Interventional radiology service
	 */
	_708174004 ("708174004", "Interventional radiology service"),
	
	/**
	 * Pulmonary rehabilitation service
	 */
	_2461000175101 ("2461000175101", "Pulmonary rehabilitation service"),
	
	/**
	 * Community mental health team
	 */
	_89301000000108 ("89301000000108", "Community mental health team"),
	
	/**
	 * Crisis prevention assessment and treatment team
	 */
	_89311000000105 ("89311000000105", "Crisis prevention assessment and treatment team"),
	
	/**
	 * Midwife episode - specialty
	 */
	_90741000000103 ("90741000000103", "Midwife episode - specialty"),
	
	/**
	 * Community health services medical - specialty
	 */
	_90891000000100 ("90891000000100", "Community health services medical - specialty"),
	
	/**
	 * Community health services dental - specialty
	 */
	_90901000000104 ("90901000000104", "Community health services dental - specialty"),
	
	/**
	 * Assertive outreach team
	 */
	_91901000000109 ("91901000000109", "Assertive outreach team"),
	
	/**
	 * Mental health crisis resolution team
	 */
	_92151000000102 ("92151000000102", "Mental health crisis resolution team"),
	
	/**
	 * Early intervention in psychosis team
	 */
	_92191000000105 ("92191000000105", "Early intervention in psychosis team"),
	
	/**
	 * Mental health home treatment team
	 */
	_92221000000103 ("92221000000103", "Mental health home treatment team"),
	
	/**
	 * Well babies - specialty
	 */
	_108501000000103 ("108501000000103", "Well babies - specialty"),
	
	/**
	 * Substance misuse team
	 */
	_109201000000109 ("109201000000109", "Substance misuse team"),
	
	/**
	 * Intermediate care - specialty
	 */
	_109581000000102 ("109581000000102", "Intermediate care - specialty"),
	
	/**
	 * Genetic science
	 */
	_321391000000108 ("321391000000108", "Genetic science"),
	
	/**
	 * Genomics
	 */
	_321401000000106 ("321401000000106", "Genomics"),
	
	/**
	 * Acute medicine
	 */
	_827611000000102 ("827611000000102", "Acute medicine"),
	
	/**
	 * Addiction service
	 */
	_827621000000108 ("827621000000108", "Addiction service"),
	
	/**
	 * Emergency ambulance service
	 */
	_827631000000105 ("827631000000105", "Emergency ambulance service"),
	
	/**
	 * Anticoagulant service
	 */
	_827641000000101 ("827641000000101", "Anticoagulant service"),
	
	/**
	 * Cardiac rehabilitation service
	 */
	_827651000000103 ("827651000000103", "Cardiac rehabilitation service"),
	
	/**
	 * Community learning disability nursing
	 */
	_827671000000107 ("827671000000107", "Community learning disability nursing"),
	
	/**
	 * Community midwifery
	 */
	_827681000000109 ("827681000000109", "Community midwifery"),
	
	/**
	 * Community psychiatric nursing
	 */
	_827691000000106 ("827691000000106", "Community psychiatric nursing"),
	
	/**
	 * Paediatric audiological medicine
	 */
	_827961000000107 ("827961000000107", "Paediatric audiological medicine"),
	
	/**
	 * Paediatric burns care
	 */
	_827971000000100 ("827971000000100", "Paediatric burns care"),
	
	/**
	 * Paediatric cystic fibrosis service
	 */
	_827981000000103 ("827981000000103", "Paediatric cystic fibrosis service"),
	
	/**
	 * Paediatric dermatology
	 */
	_827991000000101 ("827991000000101", "Paediatric dermatology"),
	
	/**
	 * Perinatal psychiatry
	 */
	_828021000000101 ("828021000000101", "Perinatal psychiatry"),
	
	/**
	 * Paediatric diabetic medicine
	 */
	_828031000000104 ("828031000000104", "Paediatric diabetic medicine"),
	
	/**
	 * Paediatric gastrointestinal surgery
	 */
	_828061000000109 ("828061000000109", "Paediatric gastrointestinal surgery"),
	
	/**
	 * Paediatric maxillofacial surgery
	 */
	_828071000000102 ("828071000000102", "Paediatric maxillofacial surgery"),
	
	/**
	 * Paediatric cardiac surgery
	 */
	_828081000000100 ("828081000000100", "Paediatric cardiac surgery"),
	
	/**
	 * Paediatric neurosurgery
	 */
	_828121000000102 ("828121000000102", "Paediatric neurosurgery"),
	
	/**
	 * Paediatric plastic surgery
	 */
	_828131000000100 ("828131000000100", "Paediatric plastic surgery"),
	
	/**
	 * Paediatric urology
	 */
	_828141000000109 ("828141000000109", "Paediatric urology"),
	
	/**
	 * Audiometry
	 */
	_828151000000107 ("828151000000107", "Audiometry"),
	
	/**
	 * Dental and maxillofacial radiology
	 */
	_828161000000105 ("828161000000105", "Dental and maxillofacial radiology"),
	
	/**
	 * Paediatric interventional radiology
	 */
	_828171000000103 ("828171000000103", "Paediatric interventional radiology"),
	
	/**
	 * Community sexual and reproductive health
	 */
	_828181000000101 ("828181000000101", "Community sexual and reproductive health"),
	
	/**
	 * Dental hygiene service
	 */
	_828191000000104 ("828191000000104", "Dental hygiene service"),
	
	/**
	 * Dental surgery assistance service
	 */
	_828201000000102 ("828201000000102", "Dental surgery assistance service"),
	
	/**
	 * Paediatric thoracic surgery
	 */
	_828211000000100 ("828211000000100", "Paediatric thoracic surgery"),
	
	/**
	 * Paediatric transplantation surgery
	 */
	_828221000000106 ("828221000000106", "Paediatric transplantation surgery"),
	
	/**
	 * Paediatric trauma and orthopaedics
	 */
	_828241000000104 ("828241000000104", "Paediatric trauma and orthopaedics"),
	
	/**
	 * Oral pathology
	 */
	_828251000000101 ("828251000000101", "Oral pathology"),
	
	/**
	 * Stroke medicine
	 */
	_828261000000103 ("828261000000103", "Stroke medicine"),
	
	/**
	 * Diagnostic imaging - specialty
	 */
	_828271000000105 ("828271000000105", "Diagnostic imaging - specialty"),
	
	/**
	 * Eating disorders service
	 */
	_828281000000107 ("828281000000107", "Eating disorders service"),
	
	/**
	 * Dispensing optometry service
	 */
	_828291000000109 ("828291000000109", "Dispensing optometry service"),
	
	/**
	 * Electrocardiography service
	 */
	_828301000000108 ("828301000000108", "Electrocardiography service"),
	
	/**
	 * General nursing
	 */
	_828311000000105 ("828311000000105", "General nursing"),
	
	/**
	 * Health visiting
	 */
	_828321000000104 ("828321000000104", "Health visiting"),
	
	/**
	 * Homeopathy service
	 */
	_828331000000102 ("828331000000102", "Homeopathy service"),
	
	/**
	 * Learning disability nursing
	 */
	_828341000000106 ("828341000000106", "Learning disability nursing"),
	
	/**
	 * Medical photography
	 */
	_828351000000109 ("828351000000109", "Medical photography"),
	
	/**
	 * Mental health nursing
	 */
	_828361000000107 ("828361000000107", "Mental health nursing"),
	
	/**
	 * Well man service
	 */
	_828371000000100 ("828371000000100", "Well man service"),
	
	/**
	 * Well woman service
	 */
	_828381000000103 ("828381000000103", "Well woman service"),
	
	/**
	 * Special care dentistry
	 */
	_828391000000101 ("828391000000101", "Special care dentistry"),
	
	/**
	 * School nursing
	 */
	_828401000000103 ("828401000000103", "School nursing"),
	
	/**
	 * Childrens nursing
	 */
	_828411000000101 ("828411000000101", "Children's nursing"),
	
	/**
	 * Respiratory physiology
	 */
	_828421000000107 ("828421000000107", "Respiratory physiology"),
	
	/**
	 * Prosthetics
	 */
	_828431000000109 ("828431000000109", "Prosthetics"),
	
	/**
	 * Paediatric pain management
	 */
	_828441000000100 ("828441000000100", "Paediatric pain management"),
	
	/**
	 * Psychiatric intensive care
	 */
	_828451000000102 ("828451000000102", "Psychiatric intensive care"),
	
	/**
	 * Electroencephalography
	 */
	_828461000000104 ("828461000000104", "Electroencephalography"),
	
	/**
	 * Oral medicine
	 */
	_828491000000105 ("828491000000105", "Oral medicine"),
	
	/**
	 * NHS 24
	 */
	_828511000000102 ("828511000000102", "NHS 24"),
	
	/**
	 * NHS Direct
	 */
	_828521000000108 ("828521000000108", "NHS Direct"),
	
	/**
	 * Nursery nursing
	 */
	_828531000000105 ("828531000000105", "Nursery nursing"),
	
	/**
	 * Obstetrics - antenatal
	 */
	_828541000000101 ("828541000000101", "Obstetrics - antenatal"),
	
	/**
	 * Obstetrics - postnatal
	 */
	_828551000000103 ("828551000000103", "Obstetrics - postnatal"),
	
	/**
	 * Child psychiatry service
	 */
	_828811000000104 ("828811000000104", "Child psychiatry service"),
	
	/**
	 * Adolescent psychiatry service
	 */
	_828821000000105 ("828821000000105", "Adolescent psychiatry service"),
	
	/**
	 * Physiological measurement - specialty
	 */
	_828831000000107 ("828831000000107", "Physiological measurement - specialty"),
	
	/**
	 * Dental therapy
	 */
	_828841000000103 ("828841000000103", "Dental therapy"),
	
	/**
	 * Diagnostic radiography
	 */
	_828851000000100 ("828851000000100", "Diagnostic radiography"),
	
	/**
	 * Programmed pulmonary rehabilitation service
	 */
	_828861000000102 ("828861000000102", "Programmed pulmonary rehabilitation service"),
	
	/**
	 * GP general practice obstetrics
	 */
	_828871000000109 ("828871000000109", "GP (general practice) obstetrics"),
	
	/**
	 * Intensive care medicine
	 */
	_828881000000106 ("828881000000106", "Intensive care medicine"),
	
	/**
	 * Medical physics
	 */
	_828891000000108 ("828891000000108", "Medical physics"),
	
	/**
	 * Endocrinology and diabetes
	 */
	_828901000000109 ("828901000000109", "Endocrinology and diabetes"),
	
	/**
	 * Surgical podiatry
	 */
	_828911000000106 ("828911000000106", "Surgical podiatry"),
	
	/**
	 * Therapeutic radiography
	 */
	_828921000000100 ("828921000000100", "Therapeutic radiography"),
	
	/**
	 * Virology
	 */
	_828931000000103 ("828931000000103", "Virology"),
	
	/**
	 * Oral microbiology
	 */
	_828941000000107 ("828941000000107", "Oral microbiology"),
	
	/**
	 * Paediatric neurodisability
	 */
	_828951000000105 ("828951000000105", "Paediatric neurodisability"),
	
	/**
	 * Paediatric immunology and allergy
	 */
	_829131000000107 ("829131000000107", "Paediatric immunology and allergy"),
	
	/**
	 * Paediatric metabolic medicine
	 */
	_829141000000103 ("829141000000103", "Paediatric metabolic medicine"),
	
	/**
	 * Prosthetics or orthotics
	 */
	_829151000000100 ("829151000000100", "Prosthetics or orthotics"),
	
	/**
	 * Ultrasound diagnostic imaging - specialty
	 */
	_829211000000107 ("829211000000107", "Ultrasound diagnostic imaging - specialty"),
	
	/**
	 * Industrial therapy service
	 */
	_829951000000102 ("829951000000102", "Industrial therapy service"),
	
	/**
	 * Out of hours service
	 */
	_829961000000104 ("829961000000104", "Out of hours service"),
	
	/**
	 * Physiotherapy service
	 */
	_829971000000106 ("829971000000106", "Physiotherapy service"),
	
	/**
	 * Community child health service
	 */
	_829981000000108 ("829981000000108", "Community child health service"),
	
	/**
	 * Medical microbiology and virology
	 */
	_829991000000105 ("829991000000105", "Medical microbiology and virology"),
	
	/**
	 * Dental medicine service
	 */
	_892731000000107 ("892731000000107", "Dental medicine service"),
	
	/**
	 * Adult mental health service
	 */
	_892811000000109 ("892811000000109", "Adult mental health service"),
	
	/**
	 * Transient ischaemic attack service
	 */
	_893041000000108 ("893041000000108", "Transient ischaemic attack service"),
	
	/**
	 * Clinical allergy service
	 */
	_893051000000106 ("893051000000106", "Clinical allergy service"),
	
	/**
	 * Respiratory medicine service
	 */
	_893081000000100 ("893081000000100", "Respiratory medicine service"),
	
	/**
	 * Spinal surgery service
	 */
	_893121000000102 ("893121000000102", "Spinal surgery service"),
	
	/**
	 * Sport and exercise medicine service
	 */
	_893201000000102 ("893201000000102", "Sport and exercise medicine service"),
	
	/**
	 * Spinal injuries service
	 */
	_893211000000100 ("893211000000100", "Spinal injuries service"),
	
	/**
	 * Specialist rehabilitation service
	 */
	_893221000000106 ("893221000000106", "Specialist rehabilitation service"),
	
	/**
	 * Podiatric surgery service
	 */
	_893231000000108 ("893231000000108", "Podiatric surgery service"),
	
	/**
	 * Midwifery service
	 */
	_893241000000104 ("893241000000104", "Midwifery service"),
	
	/**
	 * Mental health recovery and rehabilitation service
	 */
	_893251000000101 ("893251000000101", "Mental health recovery and rehabilitation service"),
	
	/**
	 * Mental health dual diagnosis service
	 */
	_893261000000103 ("893261000000103", "Mental health dual diagnosis service"),
	
	/**
	 * Medical virology service
	 */
	_893271000000105 ("893271000000105", "Medical virology service"),
	
	/**
	 * Local specialist rehabilitation service
	 */
	_893301000000108 ("893301000000108", "Local specialist rehabilitation service"),
	
	/**
	 * Diabetic education service
	 */
	_893321000000104 ("893321000000104", "Diabetic education service"),
	
	/**
	 * Dementia assessment service
	 */
	_893331000000102 ("893331000000102", "Dementia assessment service"),
	
	/**
	 * Congenital heart disease service
	 */
	_893341000000106 ("893341000000106", "Congenital heart disease service"),
	
	/**
	 * Complex specialised rehabilitation service
	 */
	_893351000000109 ("893351000000109", "Complex specialised rehabilitation service"),
	
	/**
	 * Clinical psychology service
	 */
	_893381000000103 ("893381000000103", "Clinical psychology service"),
	
	/**
	 * Adult cystic fibrosis service
	 */
	_893391000000101 ("893391000000101", "Adult cystic fibrosis service"),
	
	/**
	 * Paediatric medical oncology service
	 */
	_893601000000100 ("893601000000100", "Paediatric medical oncology service"),
	
	/**
	 * Paediatric epilepsy service
	 */
	_893611000000103 ("893611000000103", "Paediatric epilepsy service"),
	
	/**
	 * Paediatric ear nose and throat service
	 */
	_893621000000109 ("893621000000109", "Paediatric ear nose and throat service"),
	
	/**
	 * Paediatric clinical immunology and allergy service
	 */
	_893651000000104 ("893651000000104", "Paediatric clinical immunology and allergy service"),
	
	/**
	 * Paediatric clinical haematology service
	 */
	_893661000000101 ("893661000000101", "Paediatric clinical haematology service"),
	
	/**
	 * Neonatal critical care service
	 */
	_893711000000109 ("893711000000109", "Neonatal critical care service"),
	
	/**
	 * Paediatric respiratory medicine service
	 */
	_893771000000104 ("893771000000104", "Paediatric respiratory medicine service"),
	
	/**
	 * Perinatal mental health service
	 */
	_901221000000102 ("901221000000102", "Perinatal mental health service"),
	
	/**
	 * Genetics laboratory service
	 */
	_907271000000106 ("907271000000106", "Genetics laboratory service"),
	
	/**
	 * NHS 111 service
	 */
	_907301000000109 ("907301000000109", "NHS 111 service"),
	
	/**
	 * Remote triage and advice service
	 */
	_908981000000101 ("908981000000101", "Remote triage and advice service"),
	
	/**
	 * Telecare monitoring service
	 */
	_911221000000100 ("911221000000100", "Telecare monitoring service"),
	
	/**
	 * Telehealth monitoring service
	 */
	_911231000000103 ("911231000000103", "Telehealth monitoring service"),
	
	/**
	 * Telehealthcare service
	 */
	_911381000000108 ("911381000000108", "Telehealthcare service"),
	
	/**
	 * Community midwifery service
	 */
	_931791000000100 ("931791000000100", "Community midwifery service"),
	
	/**
	 * Community nursing service
	 */
	_931801000000101 ("931801000000101", "Community nursing service"),
	
	/**
	 * Preventive service
	 */
	_Preventiveservice ("278032008", "Preventive service"),
	
	/**
	 * Child assessment service
	 */
	_Childassessmentservice ("310003005", "Child assessment service"),
	
	/**
	 * Audiological service
	 */
	_Audiologicalservice ("310004004", "Audiological service"),
	
	/**
	 * Diagnostic audiology service
	 */
	_Diagnosticaudiologyservice ("310005003", "Diagnostic audiology service"),
	
	/**
	 * Adult diagnostic audiology service
	 */
	_Adultdiagnosticaudiologyservice ("310006002", "Adult diagnostic audiology service"),
	
	/**
	 * Paediatric diagnostic audiology service
	 */
	_Paediatricdiagnosticaudiologyservice ("310007006", "Paediatric diagnostic audiology service"),
	
	/**
	 * Audiological screening service
	 */
	_Audiologicalscreeningservice ("310008001", "Audiological screening service"),
	
	/**
	 * Neonatal audiological screening service
	 */
	_Neonatalaudiologicalscreeningservice ("310009009", "Neonatal audiological screening service"),
	
	/**
	 * Distraction test audiological screening service
	 */
	_Distractiontestaudiologicalscreeningservice ("310010004", "Distraction test audiological screening service"),
	
	/**
	 * Aural rehabilitation service
	 */
	_Auralrehabilitationservice ("310011000", "Aural rehabilitation service"),
	
	/**
	 * Cochlear implant service
	 */
	_Cochlearimplantservice ("310012007", "Cochlear implant service"),
	
	/**
	 * Adult cochlear implant service
	 */
	_Adultcochlearimplantservice ("310013002", "Adult cochlear implant service"),
	
	/**
	 * Paediatric cochlear implant service
	 */
	_Paediatriccochlearimplantservice ("310014008", "Paediatric cochlear implant service"),
	
	/**
	 * Hearing aid service
	 */
	_Hearingaidservice ("310015009", "Hearing aid service"),
	
	/**
	 * Adult hearing aid service
	 */
	_Adulthearingaidservice ("310016005", "Adult hearing aid service"),
	
	/**
	 * Paediatric hearing aid service
	 */
	_Paediatrichearingaidservice ("310017001", "Paediatric hearing aid service"),
	
	/**
	 * Speech-reading training service
	 */
	_Speechreadingtrainingservice ("310018006", "Speech-reading training service"),
	
	/**
	 * Tinnitus management service
	 */
	_Tinnitusmanagementservice ("310019003", "Tinnitus management service"),
	
	/**
	 * Hearing therapy service
	 */
	_Hearingtherapyservice ("310020009", "Hearing therapy service"),
	
	/**
	 * Assistive listening device service
	 */
	_Assistivelisteningdeviceservice ("310021008", "Assistive listening device service"),
	
	/**
	 * Radiotherapy service
	 */
	_Radiotherapyservice ("310023006", "Radiotherapy service"),
	
	/**
	 * Colposcopy service
	 */
	_Colposcopyservice ("310024000", "Colposcopy service"),
	
	/**
	 * Complementary therapy service
	 */
	_Complementarytherapyservice ("310025004", "Complementary therapy service"),
	
	/**
	 * Counselling service
	 */
	_Counsellingservice ("310026003", "Counselling service"),
	
	/**
	 * Mental health counselling service
	 */
	_Mentalhealthcounsellingservice ("310027007", "Mental health counselling service"),
	
	/**
	 * Diagnostic investigation service
	 */
	_Diagnosticinvestigationservice ("310028002", "Diagnostic investigation service"),
	
	/**
	 * Domiciliary visit service
	 */
	_Domiciliaryvisitservice ("310029005", "Domiciliary visit service"),
	
	/**
	 * Endoscopy service
	 */
	_Endoscopyservice ("310030000", "Endoscopy service"),
	
	/**
	 * Family planning service
	 */
	_Familyplanningservice ("310031001", "Family planning service"),
	
	/**
	 * Pregnancy termination service
	 */
	_Pregnancyterminationservice ("310062002", "Pregnancy termination service"),
	
	/**
	 * Occupational health service
	 */
	_Occupationalhealthservice ("310064001", "Occupational health service"),
	
	/**
	 * Open access service
	 */
	_Openaccessservice ("310065000", "Open access service"),
	
	/**
	 * Community paediatric service
	 */
	_Communitypaediatricservice ("310067008", "Community paediatric service"),
	
	/**
	 * Special care baby service
	 */
	_Specialcarebabyservice ("310070007", "Special care baby service"),
	
	/**
	 * Acute pain service
	 */
	_Acutepainservice ("310072004", "Acute pain service"),
	
	/**
	 * Palliative care service
	 */
	_Palliativecareservice ("310073009", "Palliative care service"),
	
	/**
	 * Clinical biochemistry service
	 */
	_Clinicalbiochemistryservice ("310076001", "Clinical biochemistry service"),
	
	/**
	 * Pharmacy service
	 */
	_Pharmacyservice ("310080006", "Pharmacy service"),
	
	/**
	 * Professional allied to medicine service
	 */
	_Professionalalliedtomedicineservice ("310081005", "Professional allied to medicine service"),
	
	/**
	 * Arts therapy services
	 */
	_Artstherapyservices ("310082003", "Arts therapy services"),
	
	/**
	 * Art therapy service
	 */
	_Arttherapyservice ("310083008", "Art therapy service"),
	
	/**
	 * Dance therapy service
	 */
	_Dancetherapyservice ("310084002", "Dance therapy service"),
	
	/**
	 * Drama therapy service
	 */
	_Dramatherapyservice ("310085001", "Drama therapy service"),
	
	/**
	 * Music therapy service
	 */
	_Musictherapyservice ("310086000", "Music therapy service"),
	
	/**
	 * Podiatry service
	 */
	_Podiatryservice ("310087009", "Podiatry service"),
	
	/**
	 * Community-based podiatry service
	 */
	_Communitybasedpodiatryservice ("310088004", "Community-based podiatry service"),
	
	/**
	 * Hospital-based podiatry service
	 */
	_Hospitalbasedpodiatryservice ("310089007", "Hospital-based podiatry service"),
	
	/**
	 * Dietetics service
	 */
	_Dieteticsservice ("310090003", "Dietetics service"),
	
	/**
	 * Community-based dietetics service
	 */
	_Communitybaseddieteticsservice ("310091004", "Community-based dietetics service"),
	
	/**
	 * Hospital-based dietetics service
	 */
	_Hospitalbaseddieteticsservice ("310092006", "Hospital-based dietetics service"),
	
	/**
	 * Occupational therapy service
	 */
	_Occupationaltherapyservice ("310093001", "Occupational therapy service"),
	
	/**
	 * Community-based occupational therapy service
	 */
	_Communitybasedoccupationaltherapyservice ("310094007", "Community-based occupational therapy service"),
	
	/**
	 * Social services occupational therapy service
	 */
	_Socialservicesoccupationaltherapyservice ("310095008", "Social services occupational therapy service"),
	
	/**
	 * Hospital-based occupational therapy service
	 */
	_Hospitalbasedoccupationaltherapyservice ("310096009", "Hospital-based occupational therapy service"),
	
	/**
	 * Hospital-based physiotherapy service
	 */
	_Hospitalbasedphysiotherapyservice ("310098005", "Hospital-based physiotherapy service"),
	
	/**
	 * Child physiotherapy service
	 */
	_Childphysiotherapyservice ("310099002", "Child physiotherapy service"),
	
	/**
	 * Play therapy service
	 */
	_Playtherapyservice ("310100005", "Play therapy service"),
	
	/**
	 * Speech and language therapy service
	 */
	_Speechandlanguagetherapyservice ("310101009", "Speech and language therapy service"),
	
	/**
	 * Community-based speech and language therapy service
	 */
	_Communitybasedspeechandlanguagetherapyservice ("310102002", "Community-based speech and language therapy service"),
	
	/**
	 * Hospital-based speech and language therapy service
	 */
	_Hospitalbasedspeechandlanguagetherapyservice ("310103007", "Hospital-based speech and language therapy service"),
	
	/**
	 * Child speech and language therapy service
	 */
	_Childspeechandlanguagetherapyservice ("310104001", "Child speech and language therapy service"),
	
	/**
	 * Optometry service
	 */
	_Optometryservice ("310105000", "Optometry service"),
	
	/**
	 * Orthoptics service
	 */
	_Orthopticsservice ("310106004", "Orthoptics service"),
	
	/**
	 * Hospital orthoptics service
	 */
	_Hospitalorthopticsservice ("310107008", "Hospital orthoptics service"),
	
	/**
	 * Community orthoptics service
	 */
	_Communityorthopticsservice ("310108003", "Community orthoptics service"),
	
	/**
	 * Orthotics service
	 */
	_Orthoticsservice ("310109006", "Orthotics service"),
	
	/**
	 * Hospital orthotics service
	 */
	_Hospitalorthoticsservice ("310110001", "Hospital orthotics service"),
	
	/**
	 * Community orthotics service
	 */
	_Communityorthoticsservice ("310111002", "Community orthotics service"),
	
	/**
	 * Surgical fitting service
	 */
	_Surgicalfittingservice ("310112009", "Surgical fitting service"),
	
	/**
	 * Hospital surgical fitting service
	 */
	_Hospitalsurgicalfittingservice ("310113004", "Hospital surgical fitting service"),
	
	/**
	 * Community surgical fitting service
	 */
	_Communitysurgicalfittingservice ("310114005", "Community surgical fitting service"),
	
	/**
	 * Liaison psychiatry service
	 */
	_Liaisonpsychiatryservice ("310119000", "Liaison psychiatry service"),
	
	/**
	 * Mental handicap psychiatry service
	 */
	_Mentalhandicappsychiatryservice ("310120006", "Mental handicap psychiatry service"),
	
	/**
	 * Psychogeriatric service
	 */
	_Psychogeriatricservice ("310121005", "Psychogeriatric service"),
	
	/**
	 * Rehabilitation psychiatry service
	 */
	_Rehabilitationpsychiatryservice ("310122003", "Rehabilitation psychiatry service"),
	
	/**
	 * Psychology service
	 */
	_Psychologyservice ("310123008", "Psychology service"),
	
	/**
	 * Breast screening service
	 */
	_Breastscreeningservice ("310126000", "Breast screening service"),
	
	/**
	 * Magnetic resonance imaging service
	 */
	_Magneticresonanceimagingservice ("310127009", "Magnetic resonance imaging service"),
	
	/**
	 * Computerised tomography service
	 */
	_Computerisedtomographyservice ("310128004", "Computerised tomography service"),
	
	/**
	 * Head injury rehabilitation service
	 */
	_Headinjuryrehabilitationservice ("310130002", "Head injury rehabilitation service"),
	
	/**
	 * Community rehabilitation service
	 */
	_Communityrehabilitationservice ("310131003", "Community rehabilitation service"),
	
	/**
	 * Young disabled service
	 */
	_Youngdisabledservice ("310132005", "Young disabled service"),
	
	/**
	 * Swallow clinic
	 */
	_Swallowclinic ("310133000", "Swallow clinic"),
	
	/**
	 * Dental surgery service
	 */
	_Dentalsurgeryservice ("310143002", "Dental surgery service"),
	
	/**
	 * General dental surgery service
	 */
	_Generaldentalsurgeryservice ("310144008", "General dental surgery service"),
	
	/**
	 * Endocrine surgery service
	 */
	_Endocrinesurgeryservice ("310150003", "Endocrine surgery service"),
	
	/**
	 * Gastrointestinal surgery service
	 */
	_Gastrointestinalsurgeryservice ("310151004", "Gastrointestinal surgery service"),
	
	/**
	 * General gastrointestinal surgery service
	 */
	_Generalgastrointestinalsurgeryservice ("310152006", "General gastrointestinal surgery service"),
	
	/**
	 * Hand surgery service
	 */
	_Handsurgeryservice ("310157000", "Hand surgery service"),
	
	/**
	 * Hepatobiliary surgical service
	 */
	_Hepatobiliarysurgicalservice ("310158005", "Hepatobiliary surgical service"),
	
	/**
	 * Orthopaedic service
	 */
	_Orthopaedicservice ("310161006", "Orthopaedic service"),
	
	/**
	 * Pancreatic surgery service
	 */
	_Pancreaticsurgeryservice ("310162004", "Pancreatic surgery service"),
	
	/**
	 * Trauma surgery service
	 */
	_Traumasurgeryservice ("310166001", "Trauma surgery service"),
	
	/**
	 * Ultrasonography service
	 */
	_Ultrasonographyservice ("310169008", "Ultrasonography service"),
	
	/**
	 * Cytology service
	 */
	_Cytologyservice ("310200001", "Cytology service"),
	
	/**
	 * Medical referral service
	 */
	_Medicalreferralservice ("373654008", "Medical referral service"),
	
	/**
	 * Paediatric neurology
	 */
	_Paediatricneurology ("394538003", "Paediatric neurology"),
	
	/**
	 * Paediatric surgery
	 */
	_Paediatricsurgery ("394539006", "Paediatric surgery"),
	
	/**
	 * Accident  emergency
	 */
	_Accidentemergency ("394576009", "Accident & emergency"),
	
	/**
	 * Anaesthetics
	 */
	_Anaesthetics ("394577000", "Anaesthetics"),
	
	/**
	 * Audiological medicine
	 */
	_Audiologicalmedicine ("394578005", "Audiological medicine"),
	
	/**
	 * Cardiology
	 */
	_Cardiology ("394579002", "Cardiology"),
	
	/**
	 * Clinical genetics
	 */
	_Clinicalgenetics ("394580004", "Clinical genetics"),
	
	/**
	 * Community medicine
	 */
	_Communitymedicine ("394581000", "Community medicine"),
	
	/**
	 * Dermatology
	 */
	_Dermatology ("394582007", "Dermatology"),
	
	/**
	 * Endocrinology
	 */
	_Endocrinology ("394583002", "Endocrinology"),
	
	/**
	 * Gastroenterology
	 */
	_Gastroenterology ("394584008", "Gastroenterology"),
	
	/**
	 * Obstetrics and gynaecology
	 */
	_Obstetricsandgynaecology ("394585009", "Obstetrics and gynaecology"),
	
	/**
	 * Gynaecology
	 */
	_Gynaecology ("394586005", "Gynaecology"),
	
	/**
	 * Child and adolescent psychiatry
	 */
	_Childandadolescentpsychiatry ("394588006", "Child and adolescent psychiatry"),
	
	/**
	 * Nephrology
	 */
	_Nephrology ("394589003", "Nephrology"),
	
	/**
	 * Thoracic medicine
	 */
	_Thoracicmedicine ("394590007", "Thoracic medicine"),
	
	/**
	 * Neurology
	 */
	_Neurology ("394591006", "Neurology"),
	
	/**
	 * Clinical oncology
	 */
	_Clinicaloncology ("394592004", "Clinical oncology"),
	
	/**
	 * Medical oncology
	 */
	_Medicaloncology ("394593009", "Medical oncology"),
	
	/**
	 * Ophthalmology
	 */
	_Ophthalmology ("394594003", "Ophthalmology"),
	
	/**
	 * Pathology
	 */
	_Pathology ("394595002", "Pathology"),
	
	/**
	 * Chemical pathology
	 */
	_Chemicalpathology ("394596001", "Chemical pathology"),
	
	/**
	 * Histopathology
	 */
	_Histopathology ("394597005", "Histopathology"),
	
	/**
	 * Immunopathology
	 */
	_Immunopathology ("394598000", "Immunopathology"),
	
	/**
	 * Neuropathology
	 */
	_Neuropathology ("394599008", "Neuropathology"),
	
	/**
	 * Clinical pharmacology
	 */
	_Clinicalpharmacology ("394600006", "Clinical pharmacology"),
	
	/**
	 * Clinical physiology
	 */
	_Clinicalphysiology ("394601005", "Clinical physiology"),
	
	/**
	 * Rehabilitation - speciality
	 */
	_Rehabilitationspeciality ("394602003", "Rehabilitation - speciality"),
	
	/**
	 * Cardiothoracic surgery
	 */
	_Cardiothoracicsurgery ("394603008", "Cardiothoracic surgery"),
	
	/**
	 * Ear, nose and throat surgery
	 */
	_Earnoseandthroatsurgery ("394604002", "Ear, nose and throat surgery"),
	
	/**
	 * Oral surgery
	 */
	_Oralsurgery ("394605001", "Oral surgery"),
	
	/**
	 * Restorative dentistry
	 */
	_Restorativedentistry ("394606000", "Restorative dentistry"),
	
	/**
	 * Paediatric dentistry
	 */
	_Paediatricdentistry ("394607009", "Paediatric dentistry"),
	
	/**
	 * Orthodontics
	 */
	_Orthodontics ("394608004", "Orthodontics"),
	
	/**
	 * General surgery
	 */
	_Generalsurgery ("394609007", "General surgery"),
	
	/**
	 * Neurosurgery
	 */
	_Neurosurgery ("394610002", "Neurosurgery"),
	
	/**
	 * Plastic surgery - speciality
	 */
	_Plasticsurgeryspeciality ("394611003", "Plastic surgery - speciality"),
	
	/**
	 * Urology
	 */
	_Urology ("394612005", "Urology"),
	
	/**
	 * Nuclear medicine - speciality
	 */
	_Nuclearmedicinespeciality ("394649004", "Nuclear medicine - speciality"),
	
	/**
	 * Trauma  orthopaedics
	 */
	_Traumaorthopaedics ("394801008", "Trauma & orthopaedics"),
	
	/**
	 * General medicine
	 */
	_Generalmedicine ("394802001", "General medicine"),
	
	/**
	 * Clinical haematology
	 */
	_Clinicalhaematology ("394803006", "Clinical haematology"),
	
	/**
	 * Clinical cytogenetics and molecular genetics
	 */
	_Clinicalcytogeneticsandmoleculargenetics ("394804000", "Clinical cytogenetics and molecular genetics"),
	
	/**
	 * Clinical immunologyallergy
	 */
	_Clinicalimmunologyallergy ("394805004", "Clinical immunology/allergy"),
	
	/**
	 * Palliative medicine
	 */
	_Palliativemedicine ("394806003", "Palliative medicine"),
	
	/**
	 * Infectious diseases specialty
	 */
	_Infectiousdiseasesspecialty ("394807007", "Infectious diseases (specialty)"),
	
	/**
	 * Genito-urinary medicine
	 */
	_Genitourinarymedicine ("394808002", "Genito-urinary medicine"),
	
	/**
	 * Clinical neuro-physiology
	 */
	_Clinicalneurophysiology ("394809005", "Clinical neuro-physiology"),
	
	/**
	 * Rheumatology
	 */
	_Rheumatology ("394810000", "Rheumatology"),
	
	/**
	 * Geriatric medicine
	 */
	_Geriatricmedicine ("394811001", "Geriatric medicine"),
	
	/**
	 * Dental medicine specialties
	 */
	_Dentalmedicinespecialties ("394812008", "Dental medicine specialties"),
	
	/**
	 * Medical ophthalmology
	 */
	_Medicalophthalmology ("394813003", "Medical ophthalmology"),
	
	/**
	 * General practice specialty
	 */
	_Generalpracticespecialty ("394814009", "General practice (specialty)"),
	
	/**
	 * Mental handicap specialty
	 */
	_Mentalhandicapspecialty ("394815005", "Mental handicap (specialty)"),
	
	/**
	 * Mental illness specialty
	 */
	_Mentalillnessspecialty ("394816006", "Mental illness (specialty)"),
	
	/**
	 * Forensic psychiatry
	 */
	_Forensicpsychiatry ("394817002", "Forensic psychiatry"),
	
	/**
	 * Old age psychiatry
	 */
	_Oldagepsychiatry ("394818007", "Old age psychiatry"),
	
	/**
	 * Transfusion medicine
	 */
	_Transfusionmedicine ("394819004", "Transfusion medicine"),
	
	/**
	 * Medical microbiology
	 */
	_Medicalmicrobiology ("394820005", "Medical microbiology"),
	
	/**
	 * Occupational medicine
	 */
	_Occupationalmedicine ("394821009", "Occupational medicine"),
	
	/**
	 * Pain management specialty
	 */
	_Painmanagementspecialty ("394882004", "Pain management (specialty)"),
	
	/**
	 * Psychotherapy specialty
	 */
	_Psychotherapyspecialty ("394913002", "Psychotherapy (specialty)"),
	
	/**
	 * Radiology - speciality
	 */
	_Radiologyspeciality ("394914008", "Radiology - speciality"),
	
	/**
	 * General pathology specialty
	 */
	_Generalpathologyspecialty ("394915009", "General pathology (specialty)"),
	
	/**
	 * Haematology specialty
	 */
	_Haematologyspecialty ("394916005", "Haematology (specialty)"),
	
	/**
	 * Community specialist palliative care
	 */
	_Communityspecialistpalliativecare ("395086005", "Community specialist palliative care"),
	
	/**
	 * Specialist palliative care
	 */
	_Specialistpalliativecare ("395092004", "Specialist palliative care"),
	
	/**
	 * Cancer primary healthcare multidisciplinary team
	 */
	_Cancerprimaryhealthcaremultidisciplinaryteam ("395104009", "Cancer primary healthcare multidisciplinary team"),
	
	/**
	 * Allergy - speciality
	 */
	_Allergyspeciality ("408439002", "Allergy - speciality"),
	
	/**
	 * Public health medicine
	 */
	_Publichealthmedicine ("408440000", "Public health medicine"),
	
	/**
	 * Endodontics - speciality
	 */
	_Endodonticsspeciality ("408441001", "Endodontics - speciality"),
	
	/**
	 * Haemophilia - speciality
	 */
	_Haemophiliaspeciality ("408442008", "Haemophilia - speciality"),
	
	/**
	 * General medical practice
	 */
	_Generalmedicalpractice ("408443003", "General medical practice"),
	
	/**
	 * General dental practice
	 */
	_Generaldentalpractice ("408444009", "General dental practice"),
	
	/**
	 * Neonatology
	 */
	_Neonatology ("408445005", "Neonatology"),
	
	/**
	 * Gynaecological oncology
	 */
	_Gynaecologicaloncology ("408446006", "Gynaecological oncology"),
	
	/**
	 * Respite care - speciality
	 */
	_Respitecarespeciality ("408447002", "Respite care - speciality"),
	
	/**
	 * Tropical medicine
	 */
	_Tropicalmedicine ("408448007", "Tropical medicine"),
	
	/**
	 * Surgical dentistry
	 */
	_Surgicaldentistry ("408449004", "Surgical dentistry"),
	
	/**
	 * Sleep studies - speciality
	 */
	_Sleepstudiesspeciality ("408450004", "Sleep studies - speciality"),
	
	/**
	 * Community learning disabilities team
	 */
	_Communitylearningdisabilitiesteam ("408451000", "Community learning disabilities team"),
	
	/**
	 * Behavioural intervention team
	 */
	_Behaviouralinterventionteam ("408452007", "Behavioural intervention team"),
	
	/**
	 * Clinical microbiology
	 */
	_Clinicalmicrobiology ("408454008", "Clinical microbiology"),
	
	/**
	 * Interventional radiology - speciality
	 */
	_Interventionalradiologyspeciality ("408455009", "Interventional radiology - speciality"),
	
	/**
	 * Thoracic surgery
	 */
	_Thoracicsurgery ("408456005", "Thoracic surgery"),
	
	/**
	 * Maxillofacial surgery
	 */
	_Maxillofacialsurgery ("408457001", "Maxillofacial surgery"),
	
	/**
	 * Paediatric cardiology
	 */
	_Paediatriccardiology ("408459003", "Paediatric cardiology"),
	
	/**
	 * Prosthodontics
	 */
	_Prosthodontics ("408460008", "Prosthodontics"),
	
	/**
	 * Periodontics
	 */
	_Periodontics ("408461007", "Periodontics"),
	
	/**
	 * Burns care
	 */
	_Burnscare ("408462000", "Burns care"),
	
	/**
	 * Vascular surgery
	 */
	_Vascularsurgery ("408463005", "Vascular surgery"),
	
	/**
	 * Colorectal surgery
	 */
	_Colorectalsurgery ("408464004", "Colorectal surgery"),
	
	/**
	 * Oral and maxillofacial surgery
	 */
	_Oralandmaxillofacialsurgery ("408465003", "Oral and maxillofacial surgery"),
	
	/**
	 * Cardiac surgery
	 */
	_Cardiacsurgery ("408466002", "Cardiac surgery"),
	
	/**
	 * Adult mental illness - speciality
	 */
	_Adultmentalillnessspeciality ("408467006", "Adult mental illness - speciality"),
	
	/**
	 * Learning disability - speciality
	 */
	_Learningdisabilityspeciality ("408468001", "Learning disability - speciality"),
	
	/**
	 * Breast surgery
	 */
	_Breastsurgery ("408469009", "Breast surgery"),
	
	/**
	 * Obstetrics
	 */
	_Obstetrics ("408470005", "Obstetrics"),
	
	/**
	 * Cardiothoracic transplantation
	 */
	_Cardiothoracictransplantation ("408471009", "Cardiothoracic transplantation"),
	
	/**
	 * Hepatology
	 */
	_Hepatology ("408472002", "Hepatology"),
	
	/**
	 * Public health dentistry
	 */
	_Publichealthdentistry ("408473007", "Public health dentistry"),
	
	/**
	 * Hepatobiliary and pancreatic surgery
	 */
	_Hepatobiliaryandpancreaticsurgery ("408474001", "Hepatobiliary and pancreatic surgery"),
	
	/**
	 * Diabetic medicine
	 */
	_Diabeticmedicine ("408475000", "Diabetic medicine"),
	
	/**
	 * Transplantation surgery
	 */
	_Transplantationsurgery ("408477008", "Transplantation surgery"),
	
	/**
	 * Critical care medicine
	 */
	_Criticalcaremedicine ("408478003", "Critical care medicine"),
	
	/**
	 * Upper gastrointestinal surgery
	 */
	_Uppergastrointestinalsurgery ("408479006", "Upper gastrointestinal surgery"),
	
	/**
	 * Clinical immunology
	 */
	_Clinicalimmunology ("408480009", "Clinical immunology"),
	
	/**
	 * Toxicology
	 */
	_Toxicology ("409967009", "Toxicology"),
	
	/**
	 * Preventive medicine
	 */
	_Preventivemedicine ("409968004", "Preventive medicine"),
	
	/**
	 * Emergency medical services
	 */
	_Emergencymedicalservices ("409971007", "Emergency medical services"),
	
	/**
	 * Military medicine
	 */
	_Militarymedicine ("410001006", "Military medicine"),
	
	/**
	 * Dive medicine
	 */
	_Divemedicine ("410005002", "Dive medicine"),
	
	/**
	 * Community health services
	 */
	_Communityhealthservices ("413294000", "Community health services"),
	
	/**
	 * Early years services
	 */
	_Earlyyearsservices ("413299005", "Early years services"),
	
	/**
	 * Osteopathic manipulative medicine
	 */
	_Osteopathicmanipulativemedicine ("416304004", "Osteopathic manipulative medicine"),
	
	/**
	 * Paediatric otolaryngology
	 */
	_Paediatricotolaryngology ("417887005", "Paediatric otolaryngology"),
	
	/**
	 * Paediatric oncology
	 */
	_Paediatriconcology ("418002000", "Paediatric oncology"),
	
	/**
	 * Dermatologic surgery
	 */
	_Dermatologicsurgery ("418018006", "Dermatologic surgery"),
	
	/**
	 * Paediatric gastroenterology
	 */
	_Paediatricgastroenterology ("418058008", "Paediatric gastroenterology"),
	
	/**
	 * Pulmonary medicine
	 */
	_Pulmonarymedicine ("418112009", "Pulmonary medicine"),
	
	/**
	 * Paediatric immunology
	 */
	_Paediatricimmunology ("418535003", "Paediatric immunology"),
	
	/**
	 * Paediatric haematology
	 */
	_Paediatrichaematology ("418652005", "Paediatric haematology"),
	
	/**
	 * Paediatric infectious diseases
	 */
	_Paediatricinfectiousdiseases ("418862001", "Paediatric infectious diseases"),
	
	/**
	 * Otolaryngology
	 */
	_Otolaryngology ("418960008", "Otolaryngology"),
	
	/**
	 * Urological oncology
	 */
	_Urologicaloncology ("419043006", "Urological oncology"),
	
	/**
	 * Paediatric pulmonology
	 */
	_Paediatricpulmonology ("419170002", "Paediatric pulmonology"),
	
	/**
	 * Internal medicine
	 */
	_Internalmedicine ("419192003", "Internal medicine"),
	
	/**
	 * Paediatric intensive care
	 */
	_Paediatricintensivecare ("419215006", "Paediatric intensive care"),
	
	/**
	 * Surgical oncology
	 */
	_Surgicaloncology ("419321007", "Surgical oncology"),
	
	/**
	 * Paediatric nephrology
	 */
	_Paediatricnephrology ("419365004", "Paediatric nephrology"),
	
	/**
	 * Paediatric rheumatology
	 */
	_Paediatricrheumatology ("419472004", "Paediatric rheumatology"),
	
	/**
	 * Paediatric endocrinology
	 */
	_Paediatricendocrinology ("419610006", "Paediatric endocrinology"),
	
	/**
	 * Paediatric neurology oncology
	 */
	_Paediatricneurologyoncology ("419677000", "Paediatric neurology oncology"),
	
	/**
	 * Family practice
	 */
	_Familypractice ("419772000", "Family practice"),
	
	/**
	 * Radiation oncology
	 */
	_Radiationoncology ("419815003", "Radiation oncology"),
	
	/**
	 * Paediatric emergency medicine
	 */
	_Paediatricemergencymedicine ("419917007", "Paediatric emergency medicine"),
	
	/**
	 * Paediatric ophthalmology
	 */
	_Paediatricophthalmology ("419983000", "Paediatric ophthalmology"),
	
	/**
	 * Paediatric bone marrow transplantation
	 */
	_Paediatricbonemarrowtransplantation ("420112009", "Paediatric bone marrow transplantation"),
	
	/**
	 * Paediatric genetics
	 */
	_Paediatricgenetics ("420208008", "Paediatric genetics"),
	
	/**
	 * Strabismus surgery
	 */
	_Strabismussurgery ("420419008", "Strabismus surgery"),
	
	/**
	 * Refractive surgery
	 */
	_Refractivesurgery ("420708009", "Refractive surgery"),
	
	/**
	 * Vitreo-retinal surgery
	 */
	_Vitreoretinalsurgery ("420985004", "Vitreo-retinal surgery"),
	
	/**
	 * Corneal surgery
	 */
	_Cornealsurgery ("421582004", "Corneal surgery"),
	
	/**
	 * Blood banking and transfusion medicine specialty
	 */
	_Bloodbankingandtransfusionmedicinespecialty ("421661004", "Blood banking and transfusion medicine (specialty)"),
	
	/**
	 * Glaucoma surgery
	 */
	_Glaucomasurgery ("421885009", "Glaucoma surgery"),
	
	/**
	 * Ophthalmic plastic surgery
	 */
	_Ophthalmicplasticsurgery ("422171003", "Ophthalmic plastic surgery"),
	
	/**
	 * Ophthalmic surgery
	 */
	_Ophthalmicsurgery ("422191005", "Ophthalmic surgery"),
	
	/**
	 * Cataract surgery
	 */
	_Cataractsurgery ("422349000", "Cataract surgery"),
	
	/**
	 * Home hospice service
	 */
	_Homehospiceservice ("444933003", "Home hospice service"),
	
	/**
	 * Acute care hospice service
	 */
	_Acutecarehospiceservice ("445449000", "Acute care hospice service"),
	
	/**
	 * Blood and marrow transplantation
	 */
	_Bloodandmarrowtransplantation ("445715009", "Blood and marrow transplantation"),
	
	/**
	 * Community based physiotherapy service
	 */
	_Communitybasedphysiotherapyservice ("699650006", "Community based physiotherapy service"),
	
	/**
	 * Domiciliary physiotherapy service
	 */
	_Domiciliaryphysiotherapyservice ("705150003", "Domiciliary physiotherapy service"),
	
	/**
	 * Vascular ultrasound service
	 */
	_Vascularultrasoundservice ("708171007", "Vascular ultrasound service"),
	
	/**
	 * Cardiac ultrasound service
	 */
	_Cardiacultrasoundservice ("708172000", "Cardiac ultrasound service"),
	
	/**
	 * Obstetric ultrasound service
	 */
	_Obstetricultrasoundservice ("708173005", "Obstetric ultrasound service"),
	
	/**
	 * Interventional radiology service
	 */
	_Interventionalradiologyservice ("708174004", "Interventional radiology service"),
	
	/**
	 * Pulmonary rehabilitation service
	 */
	_Pulmonaryrehabilitationservice ("2461000175101", "Pulmonary rehabilitation service"),
	
	/**
	 * Community mental health team
	 */
	_Communitymentalhealthteam ("89301000000108", "Community mental health team"),
	
	/**
	 * Crisis prevention assessment and treatment team
	 */
	_Crisispreventionassessmentandtreatmentteam ("89311000000105", "Crisis prevention assessment and treatment team"),
	
	/**
	 * Midwife episode - specialty
	 */
	_Midwifeepisodespecialty ("90741000000103", "Midwife episode - specialty"),
	
	/**
	 * Community health services medical - specialty
	 */
	_Communityhealthservicesmedicalspecialty ("90891000000100", "Community health services medical - specialty"),
	
	/**
	 * Community health services dental - specialty
	 */
	_Communityhealthservicesdentalspecialty ("90901000000104", "Community health services dental - specialty"),
	
	/**
	 * Assertive outreach team
	 */
	_Assertiveoutreachteam ("91901000000109", "Assertive outreach team"),
	
	/**
	 * Mental health crisis resolution team
	 */
	_Mentalhealthcrisisresolutionteam ("92151000000102", "Mental health crisis resolution team"),
	
	/**
	 * Early intervention in psychosis team
	 */
	_Earlyinterventioninpsychosisteam ("92191000000105", "Early intervention in psychosis team"),
	
	/**
	 * Mental health home treatment team
	 */
	_Mentalhealthhometreatmentteam ("92221000000103", "Mental health home treatment team"),
	
	/**
	 * Well babies - specialty
	 */
	_Wellbabiesspecialty ("108501000000103", "Well babies - specialty"),
	
	/**
	 * Substance misuse team
	 */
	_Substancemisuseteam ("109201000000109", "Substance misuse team"),
	
	/**
	 * Intermediate care - specialty
	 */
	_Intermediatecarespecialty ("109581000000102", "Intermediate care - specialty"),
	
	/**
	 * Genetic science
	 */
	_Geneticscience ("321391000000108", "Genetic science"),
	
	/**
	 * Genomics
	 */
	_Genomics ("321401000000106", "Genomics"),
	
	/**
	 * Acute medicine
	 */
	_Acutemedicine ("827611000000102", "Acute medicine"),
	
	/**
	 * Addiction service
	 */
	_Addictionservice ("827621000000108", "Addiction service"),
	
	/**
	 * Emergency ambulance service
	 */
	_Emergencyambulanceservice ("827631000000105", "Emergency ambulance service"),
	
	/**
	 * Anticoagulant service
	 */
	_Anticoagulantservice ("827641000000101", "Anticoagulant service"),
	
	/**
	 * Cardiac rehabilitation service
	 */
	_Cardiacrehabilitationservice ("827651000000103", "Cardiac rehabilitation service"),
	
	/**
	 * Community learning disability nursing
	 */
	_Communitylearningdisabilitynursing ("827671000000107", "Community learning disability nursing"),
	
	/**
	 * Community midwifery
	 */
	_Communitymidwifery ("827681000000109", "Community midwifery"),
	
	/**
	 * Community psychiatric nursing
	 */
	_Communitypsychiatricnursing ("827691000000106", "Community psychiatric nursing"),
	
	/**
	 * Paediatric audiological medicine
	 */
	_Paediatricaudiologicalmedicine ("827961000000107", "Paediatric audiological medicine"),
	
	/**
	 * Paediatric burns care
	 */
	_Paediatricburnscare ("827971000000100", "Paediatric burns care"),
	
	/**
	 * Paediatric cystic fibrosis service
	 */
	_Paediatriccysticfibrosisservice ("827981000000103", "Paediatric cystic fibrosis service"),
	
	/**
	 * Paediatric dermatology
	 */
	_Paediatricdermatology ("827991000000101", "Paediatric dermatology"),
	
	/**
	 * Perinatal psychiatry
	 */
	_Perinatalpsychiatry ("828021000000101", "Perinatal psychiatry"),
	
	/**
	 * Paediatric diabetic medicine
	 */
	_Paediatricdiabeticmedicine ("828031000000104", "Paediatric diabetic medicine"),
	
	/**
	 * Paediatric gastrointestinal surgery
	 */
	_Paediatricgastrointestinalsurgery ("828061000000109", "Paediatric gastrointestinal surgery"),
	
	/**
	 * Paediatric maxillofacial surgery
	 */
	_Paediatricmaxillofacialsurgery ("828071000000102", "Paediatric maxillofacial surgery"),
	
	/**
	 * Paediatric cardiac surgery
	 */
	_Paediatriccardiacsurgery ("828081000000100", "Paediatric cardiac surgery"),
	
	/**
	 * Paediatric neurosurgery
	 */
	_Paediatricneurosurgery ("828121000000102", "Paediatric neurosurgery"),
	
	/**
	 * Paediatric plastic surgery
	 */
	_Paediatricplasticsurgery ("828131000000100", "Paediatric plastic surgery"),
	
	/**
	 * Paediatric urology
	 */
	_Paediatricurology ("828141000000109", "Paediatric urology"),
	
	/**
	 * Audiometry
	 */
	_Audiometry ("828151000000107", "Audiometry"),
	
	/**
	 * Dental and maxillofacial radiology
	 */
	_Dentalandmaxillofacialradiology ("828161000000105", "Dental and maxillofacial radiology"),
	
	/**
	 * Paediatric interventional radiology
	 */
	_Paediatricinterventionalradiology ("828171000000103", "Paediatric interventional radiology"),
	
	/**
	 * Community sexual and reproductive health
	 */
	_Communitysexualandreproductivehealth ("828181000000101", "Community sexual and reproductive health"),
	
	/**
	 * Dental hygiene service
	 */
	_Dentalhygieneservice ("828191000000104", "Dental hygiene service"),
	
	/**
	 * Dental surgery assistance service
	 */
	_Dentalsurgeryassistanceservice ("828201000000102", "Dental surgery assistance service"),
	
	/**
	 * Paediatric thoracic surgery
	 */
	_Paediatricthoracicsurgery ("828211000000100", "Paediatric thoracic surgery"),
	
	/**
	 * Paediatric transplantation surgery
	 */
	_Paediatrictransplantationsurgery ("828221000000106", "Paediatric transplantation surgery"),
	
	/**
	 * Paediatric trauma and orthopaedics
	 */
	_Paediatrictraumaandorthopaedics ("828241000000104", "Paediatric trauma and orthopaedics"),
	
	/**
	 * Oral pathology
	 */
	_Oralpathology ("828251000000101", "Oral pathology"),
	
	/**
	 * Stroke medicine
	 */
	_Strokemedicine ("828261000000103", "Stroke medicine"),
	
	/**
	 * Diagnostic imaging - specialty
	 */
	_Diagnosticimagingspecialty ("828271000000105", "Diagnostic imaging - specialty"),
	
	/**
	 * Eating disorders service
	 */
	_Eatingdisordersservice ("828281000000107", "Eating disorders service"),
	
	/**
	 * Dispensing optometry service
	 */
	_Dispensingoptometryservice ("828291000000109", "Dispensing optometry service"),
	
	/**
	 * Electrocardiography service
	 */
	_Electrocardiographyservice ("828301000000108", "Electrocardiography service"),
	
	/**
	 * General nursing
	 */
	_Generalnursing ("828311000000105", "General nursing"),
	
	/**
	 * Health visiting
	 */
	_Healthvisiting ("828321000000104", "Health visiting"),
	
	/**
	 * Homeopathy service
	 */
	_Homeopathyservice ("828331000000102", "Homeopathy service"),
	
	/**
	 * Learning disability nursing
	 */
	_Learningdisabilitynursing ("828341000000106", "Learning disability nursing"),
	
	/**
	 * Medical photography
	 */
	_Medicalphotography ("828351000000109", "Medical photography"),
	
	/**
	 * Mental health nursing
	 */
	_Mentalhealthnursing ("828361000000107", "Mental health nursing"),
	
	/**
	 * Well man service
	 */
	_Wellmanservice ("828371000000100", "Well man service"),
	
	/**
	 * Well woman service
	 */
	_Wellwomanservice ("828381000000103", "Well woman service"),
	
	/**
	 * Special care dentistry
	 */
	_Specialcaredentistry ("828391000000101", "Special care dentistry"),
	
	/**
	 * School nursing
	 */
	_Schoolnursing ("828401000000103", "School nursing"),
	
	/**
	 * Childrens nursing
	 */
	_Childrensnursing ("828411000000101", "Children's nursing"),
	
	/**
	 * Respiratory physiology
	 */
	_Respiratoryphysiology ("828421000000107", "Respiratory physiology"),
	
	/**
	 * Prosthetics
	 */
	_Prosthetics ("828431000000109", "Prosthetics"),
	
	/**
	 * Paediatric pain management
	 */
	_Paediatricpainmanagement ("828441000000100", "Paediatric pain management"),
	
	/**
	 * Psychiatric intensive care
	 */
	_Psychiatricintensivecare ("828451000000102", "Psychiatric intensive care"),
	
	/**
	 * Electroencephalography
	 */
	_Electroencephalography ("828461000000104", "Electroencephalography"),
	
	/**
	 * Oral medicine
	 */
	_Oralmedicine ("828491000000105", "Oral medicine"),
	
	/**
	 * NHS 24
	 */
	_NHS24 ("828511000000102", "NHS 24"),
	
	/**
	 * NHS Direct
	 */
	_NHSDirect ("828521000000108", "NHS Direct"),
	
	/**
	 * Nursery nursing
	 */
	_Nurserynursing ("828531000000105", "Nursery nursing"),
	
	/**
	 * Obstetrics - antenatal
	 */
	_Obstetricsantenatal ("828541000000101", "Obstetrics - antenatal"),
	
	/**
	 * Obstetrics - postnatal
	 */
	_Obstetricspostnatal ("828551000000103", "Obstetrics - postnatal"),
	
	/**
	 * Child psychiatry service
	 */
	_Childpsychiatryservice ("828811000000104", "Child psychiatry service"),
	
	/**
	 * Adolescent psychiatry service
	 */
	_Adolescentpsychiatryservice ("828821000000105", "Adolescent psychiatry service"),
	
	/**
	 * Physiological measurement - specialty
	 */
	_Physiologicalmeasurementspecialty ("828831000000107", "Physiological measurement - specialty"),
	
	/**
	 * Dental therapy
	 */
	_Dentaltherapy ("828841000000103", "Dental therapy"),
	
	/**
	 * Diagnostic radiography
	 */
	_Diagnosticradiography ("828851000000100", "Diagnostic radiography"),
	
	/**
	 * Programmed pulmonary rehabilitation service
	 */
	_Programmedpulmonaryrehabilitationservice ("828861000000102", "Programmed pulmonary rehabilitation service"),
	
	/**
	 * GP general practice obstetrics
	 */
	_GPgeneralpracticeobstetrics ("828871000000109", "GP (general practice) obstetrics"),
	
	/**
	 * Intensive care medicine
	 */
	_Intensivecaremedicine ("828881000000106", "Intensive care medicine"),
	
	/**
	 * Medical physics
	 */
	_Medicalphysics ("828891000000108", "Medical physics"),
	
	/**
	 * Endocrinology and diabetes
	 */
	_Endocrinologyanddiabetes ("828901000000109", "Endocrinology and diabetes"),
	
	/**
	 * Surgical podiatry
	 */
	_Surgicalpodiatry ("828911000000106", "Surgical podiatry"),
	
	/**
	 * Therapeutic radiography
	 */
	_Therapeuticradiography ("828921000000100", "Therapeutic radiography"),
	
	/**
	 * Virology
	 */
	_Virology ("828931000000103", "Virology"),
	
	/**
	 * Oral microbiology
	 */
	_Oralmicrobiology ("828941000000107", "Oral microbiology"),
	
	/**
	 * Paediatric neurodisability
	 */
	_Paediatricneurodisability ("828951000000105", "Paediatric neurodisability"),
	
	/**
	 * Paediatric immunology and allergy
	 */
	_Paediatricimmunologyandallergy ("829131000000107", "Paediatric immunology and allergy"),
	
	/**
	 * Paediatric metabolic medicine
	 */
	_Paediatricmetabolicmedicine ("829141000000103", "Paediatric metabolic medicine"),
	
	/**
	 * Prosthetics or orthotics
	 */
	_Prostheticsororthotics ("829151000000100", "Prosthetics or orthotics"),
	
	/**
	 * Ultrasound diagnostic imaging - specialty
	 */
	_Ultrasounddiagnosticimagingspecialty ("829211000000107", "Ultrasound diagnostic imaging - specialty"),
	
	/**
	 * Industrial therapy service
	 */
	_Industrialtherapyservice ("829951000000102", "Industrial therapy service"),
	
	/**
	 * Out of hours service
	 */
	_Outofhoursservice ("829961000000104", "Out of hours service"),
	
	/**
	 * Physiotherapy service
	 */
	_Physiotherapyservice ("829971000000106", "Physiotherapy service"),
	
	/**
	 * Community child health service
	 */
	_Communitychildhealthservice ("829981000000108", "Community child health service"),
	
	/**
	 * Medical microbiology and virology
	 */
	_Medicalmicrobiologyandvirology ("829991000000105", "Medical microbiology and virology"),
	
	/**
	 * Dental medicine service
	 */
	_Dentalmedicineservice ("892731000000107", "Dental medicine service"),
	
	/**
	 * Adult mental health service
	 */
	_Adultmentalhealthservice ("892811000000109", "Adult mental health service"),
	
	/**
	 * Transient ischaemic attack service
	 */
	_Transientischaemicattackservice ("893041000000108", "Transient ischaemic attack service"),
	
	/**
	 * Clinical allergy service
	 */
	_Clinicalallergyservice ("893051000000106", "Clinical allergy service"),
	
	/**
	 * Respiratory medicine service
	 */
	_Respiratorymedicineservice ("893081000000100", "Respiratory medicine service"),
	
	/**
	 * Spinal surgery service
	 */
	_Spinalsurgeryservice ("893121000000102", "Spinal surgery service"),
	
	/**
	 * Sport and exercise medicine service
	 */
	_Sportandexercisemedicineservice ("893201000000102", "Sport and exercise medicine service"),
	
	/**
	 * Spinal injuries service
	 */
	_Spinalinjuriesservice ("893211000000100", "Spinal injuries service"),
	
	/**
	 * Specialist rehabilitation service
	 */
	_Specialistrehabilitationservice ("893221000000106", "Specialist rehabilitation service"),
	
	/**
	 * Podiatric surgery service
	 */
	_Podiatricsurgeryservice ("893231000000108", "Podiatric surgery service"),
	
	/**
	 * Midwifery service
	 */
	_Midwiferyservice ("893241000000104", "Midwifery service"),
	
	/**
	 * Mental health recovery and rehabilitation service
	 */
	_Mentalhealthrecoveryandrehabilitationservice ("893251000000101", "Mental health recovery and rehabilitation service"),
	
	/**
	 * Mental health dual diagnosis service
	 */
	_Mentalhealthdualdiagnosisservice ("893261000000103", "Mental health dual diagnosis service"),
	
	/**
	 * Medical virology service
	 */
	_Medicalvirologyservice ("893271000000105", "Medical virology service"),
	
	/**
	 * Local specialist rehabilitation service
	 */
	_Localspecialistrehabilitationservice ("893301000000108", "Local specialist rehabilitation service"),
	
	/**
	 * Diabetic education service
	 */
	_Diabeticeducationservice ("893321000000104", "Diabetic education service"),
	
	/**
	 * Dementia assessment service
	 */
	_Dementiaassessmentservice ("893331000000102", "Dementia assessment service"),
	
	/**
	 * Congenital heart disease service
	 */
	_Congenitalheartdiseaseservice ("893341000000106", "Congenital heart disease service"),
	
	/**
	 * Complex specialised rehabilitation service
	 */
	_Complexspecialisedrehabilitationservice ("893351000000109", "Complex specialised rehabilitation service"),
	
	/**
	 * Clinical psychology service
	 */
	_Clinicalpsychologyservice ("893381000000103", "Clinical psychology service"),
	
	/**
	 * Adult cystic fibrosis service
	 */
	_Adultcysticfibrosisservice ("893391000000101", "Adult cystic fibrosis service"),
	
	/**
	 * Paediatric medical oncology service
	 */
	_Paediatricmedicaloncologyservice ("893601000000100", "Paediatric medical oncology service"),
	
	/**
	 * Paediatric epilepsy service
	 */
	_Paediatricepilepsyservice ("893611000000103", "Paediatric epilepsy service"),
	
	/**
	 * Paediatric ear nose and throat service
	 */
	_Paediatricearnoseandthroatservice ("893621000000109", "Paediatric ear nose and throat service"),
	
	/**
	 * Paediatric clinical immunology and allergy service
	 */
	_Paediatricclinicalimmunologyandallergyservice ("893651000000104", "Paediatric clinical immunology and allergy service"),
	
	/**
	 * Paediatric clinical haematology service
	 */
	_Paediatricclinicalhaematologyservice ("893661000000101", "Paediatric clinical haematology service"),
	
	/**
	 * Neonatal critical care service
	 */
	_Neonatalcriticalcareservice ("893711000000109", "Neonatal critical care service"),
	
	/**
	 * Paediatric respiratory medicine service
	 */
	_Paediatricrespiratorymedicineservice ("893771000000104", "Paediatric respiratory medicine service"),
	
	/**
	 * Perinatal mental health service
	 */
	_Perinatalmentalhealthservice ("901221000000102", "Perinatal mental health service"),
	
	/**
	 * Genetics laboratory service
	 */
	_Geneticslaboratoryservice ("907271000000106", "Genetics laboratory service"),
	
	/**
	 * NHS 111 service
	 */
	_NHS111service ("907301000000109", "NHS 111 service"),
	
	/**
	 * Remote triage and advice service
	 */
	_Remotetriageandadviceservice ("908981000000101", "Remote triage and advice service"),
	
	/**
	 * Telecare monitoring service
	 */
	_Telecaremonitoringservice ("911221000000100", "Telecare monitoring service"),
	
	/**
	 * Telehealth monitoring service
	 */
	_Telehealthmonitoringservice ("911231000000103", "Telehealth monitoring service"),
	
	/**
	 * Telehealthcare service
	 */
	_Telehealthcareservice ("911381000000108", "Telehealthcare service"),
	
	/**
	 * Community midwifery service
	 */
	_Communitymidwiferyservice ("931791000000100", "Community midwifery service"),
	
	/**
	 * Community nursing service
	 */
	_Communitynursingservice ("931801000000101", "Community nursing service"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	CorrespondenceCaresettingtype(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static CorrespondenceCaresettingtype getByCode(String code) {
		CorrespondenceCaresettingtype[] vals = values();
		for (CorrespondenceCaresettingtype val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(CorrespondenceCaresettingtype other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	