/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the PersonalRelationshipRoleType vocabulary:
 * <ul>
 *   <li>_AUNT : aunt</li>
 *   <li>_BRO : Brother</li>
 *   <li>_BROINLAW : brother-in-law</li>
 *   <li>_CHILD : Child</li>
 *   <li>_CHLDADOPT : adopted child</li>
 *   <li>_CHLDFOST : foster child</li>
 *   <li>_CHLDINLAW : child in-law</li>
 *   <li>_COUSN : cousin</li>
 *   <li>_DAU : natural daughter</li>
 *   <li>_DAUADOPT : adopted daughter</li>
 *   <li>_DAUFOST : foster daughter</li>
 *   <li>_DAUINLAW : daughter in-law</li>
 *   <li>_DOMPART : domestic partner</li>
 *   <li>_FAMMEMB : Family Member</li>
 *   <li>_FRND : unrelated friend</li>
 *   <li>_FTH : Father</li>
 *   <li>_FTHINLAW : father-in-law</li>
 *   <li>_GGRFTH : great grandfather</li>
 *   <li>_GGRPRN : great grandparent</li>
 *   <li>_GGRMTH : great grandmother</li>
 *   <li>_GRFTH : Grandfather</li>
 *   <li>_GRMTH : Grandmother</li>
 *   <li>_GRNDCHILD : grandchild</li>
 *   <li>_GRNDDAU : granddaughter</li>
 *   <li>_GRNDSON : grandson</li>
 *   <li>_GRPRN : Grandparent</li>
 *   <li>_HBRO : half-brother</li>
 *   <li>_HSIB : half-sibling</li>
 *   <li>_HSIS : half-sister</li>
 *   <li>_HUSB : husband</li>
 *   <li>_MTH : Mother</li>
 *   <li>_MTHINLOAW : mother-in-law</li>
 *   <li>_NBOR : neighbor</li>
 *   <li>_NBRO : natural brother</li>
 *   <li>_NCHILD : natural child</li>
 *   <li>_NEPHEW : nephew</li>
 *   <li>_NFTH : natural father</li>
 *   <li>_NIECE : niece</li>
 *   <li>_NIENEPH : niecenephew</li>
 *   <li>_NMTH : natural mother</li>
 *   <li>_NPRN : natural parent</li>
 *   <li>_NSIB : natural sibling</li>
 *   <li>_NSIS : natural sister</li>
 *   <li>_PRN : Parent</li>
 *   <li>_PRNINLAW : parent in-law</li>
 *   <li>_ROOM : Roommate</li>
 *   <li>_SIB : Sibling</li>
 *   <li>_SIBINLAW : sibling in-law</li>
 *   <li>_SIS : Sister</li>
 *   <li>_SISLINLAW : sister-in-law</li>
 *   <li>_SON : natural son</li>
 *   <li>_SONADOPT : adopted son</li>
 *   <li>_SONFOST : foster son</li>
 *   <li>_SONINLAW : son in-law</li>
 *   <li>_STPBRO : stepbrother</li>
 *   <li>_STPCHLD : step child</li>
 *   <li>_STPDAU : stepdaughter</li>
 *   <li>_STPFTH : stepfather</li>
 *   <li>_STPMTH : stepmother</li>
 *   <li>_STPPRN : step parent</li>
 *   <li>_STPSIB : step sibling</li>
 *   <li>_STPSIS : stepsister</li>
 *   <li>_STPSON : stepson</li>
 *   <li>_SIGOTHR : significant other</li>
 *   <li>_SPS : spouse</li>
 *   <li>_UNCLE : uncle</li>
 *   <li>_WIFE : wife</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum PersonalRelationshipRoleType implements VocabularyEntry {
	
	
	/**
	 * aunt
	 */
	_AUNT ("AUNT", "aunt"),
	
	/**
	 * Brother
	 */
	_BRO ("BRO", "Brother"),
	
	/**
	 * brother-in-law
	 */
	_BROINLAW ("BROINLAW", "brother-in-law"),
	
	/**
	 * Child
	 */
	_CHILD ("CHILD", "Child"),
	
	/**
	 * adopted child
	 */
	_CHLDADOPT ("CHLDADOPT", "adopted child"),
	
	/**
	 * foster child
	 */
	_CHLDFOST ("CHLDFOST", "foster child"),
	
	/**
	 * child in-law
	 */
	_CHLDINLAW ("CHLDINLAW", "child in-law"),
	
	/**
	 * cousin
	 */
	_COUSN ("COUSN", "cousin"),
	
	/**
	 * natural daughter
	 */
	_DAU ("DAU", "natural daughter"),
	
	/**
	 * adopted daughter
	 */
	_DAUADOPT ("DAUADOPT", "adopted daughter"),
	
	/**
	 * foster daughter
	 */
	_DAUFOST ("DAUFOST", "foster daughter"),
	
	/**
	 * daughter in-law
	 */
	_DAUINLAW ("DAUINLAW", "daughter in-law"),
	
	/**
	 * domestic partner
	 */
	_DOMPART ("DOMPART", "domestic partner"),
	
	/**
	 * Family Member
	 */
	_FAMMEMB ("FAMMEMB", "Family Member"),
	
	/**
	 * unrelated friend
	 */
	_FRND ("FRND", "unrelated friend"),
	
	/**
	 * Father
	 */
	_FTH ("FTH", "Father"),
	
	/**
	 * father-in-law
	 */
	_FTHINLAW ("FTHINLAW", "father-in-law"),
	
	/**
	 * great grandfather
	 */
	_GGRFTH ("GGRFTH", "great grandfather"),
	
	/**
	 * great grandparent
	 */
	_GGRPRN ("GGRPRN", "great grandparent"),
	
	/**
	 * great grandmother
	 */
	_GGRMTH ("GGRMTH", "great grandmother"),
	
	/**
	 * Grandfather
	 */
	_GRFTH ("GRFTH", "Grandfather"),
	
	/**
	 * Grandmother
	 */
	_GRMTH ("GRMTH", "Grandmother"),
	
	/**
	 * grandchild
	 */
	_GRNDCHILD ("GRNDCHILD", "grandchild"),
	
	/**
	 * granddaughter
	 */
	_GRNDDAU ("GRNDDAU", "granddaughter"),
	
	/**
	 * grandson
	 */
	_GRNDSON ("GRNDSON", "grandson"),
	
	/**
	 * Grandparent
	 */
	_GRPRN ("GRPRN", "Grandparent"),
	
	/**
	 * half-brother
	 */
	_HBRO ("HBRO", "half-brother"),
	
	/**
	 * half-sibling
	 */
	_HSIB ("HSIB", "half-sibling"),
	
	/**
	 * half-sister
	 */
	_HSIS ("HSIS", "half-sister"),
	
	/**
	 * husband
	 */
	_HUSB ("HUSB", "husband"),
	
	/**
	 * Mother
	 */
	_MTH ("MTH", "Mother"),
	
	/**
	 * mother-in-law
	 */
	_MTHINLOAW ("MTHINLOAW", "mother-in-law"),
	
	/**
	 * neighbor
	 */
	_NBOR ("NBOR", "neighbor"),
	
	/**
	 * natural brother
	 */
	_NBRO ("NBRO", "natural brother"),
	
	/**
	 * natural child
	 */
	_NCHILD ("NCHILD", "natural child"),
	
	/**
	 * nephew
	 */
	_NEPHEW ("NEPHEW", "nephew"),
	
	/**
	 * natural father
	 */
	_NFTH ("NFTH", "natural father"),
	
	/**
	 * niece
	 */
	_NIECE ("NIECE", "niece"),
	
	/**
	 * niecenephew
	 */
	_NIENEPH ("NIENEPH", "niece/nephew"),
	
	/**
	 * natural mother
	 */
	_NMTH ("NMTH", "natural mother"),
	
	/**
	 * natural parent
	 */
	_NPRN ("NPRN", "natural parent"),
	
	/**
	 * natural sibling
	 */
	_NSIB ("NSIB", "natural sibling"),
	
	/**
	 * natural sister
	 */
	_NSIS ("NSIS", "natural sister"),
	
	/**
	 * Parent
	 */
	_PRN ("PRN", "Parent"),
	
	/**
	 * parent in-law
	 */
	_PRNINLAW ("PRNINLAW", "parent in-law"),
	
	/**
	 * Roommate
	 */
	_ROOM ("ROOM", "Roommate"),
	
	/**
	 * Sibling
	 */
	_SIB ("SIB", "Sibling"),
	
	/**
	 * sibling in-law
	 */
	_SIBINLAW ("SIBINLAW", "sibling in-law"),
	
	/**
	 * Sister
	 */
	_SIS ("SIS", "Sister"),
	
	/**
	 * sister-in-law
	 */
	_SISLINLAW ("SISLINLAW", "sister-in-law"),
	
	/**
	 * natural son
	 */
	_SON ("SON", "natural son"),
	
	/**
	 * adopted son
	 */
	_SONADOPT ("SONADOPT", "adopted son"),
	
	/**
	 * foster son
	 */
	_SONFOST ("SONFOST", "foster son"),
	
	/**
	 * son in-law
	 */
	_SONINLAW ("SONINLAW", "son in-law"),
	
	/**
	 * stepbrother
	 */
	_STPBRO ("STPBRO", "stepbrother"),
	
	/**
	 * step child
	 */
	_STPCHLD ("STPCHLD", "step child"),
	
	/**
	 * stepdaughter
	 */
	_STPDAU ("STPDAU", "stepdaughter"),
	
	/**
	 * stepfather
	 */
	_STPFTH ("STPFTH", "stepfather"),
	
	/**
	 * stepmother
	 */
	_STPMTH ("STPMTH", "stepmother"),
	
	/**
	 * step parent
	 */
	_STPPRN ("STPPRN", "step parent"),
	
	/**
	 * step sibling
	 */
	_STPSIB ("STPSIB", "step sibling"),
	
	/**
	 * stepsister
	 */
	_STPSIS ("STPSIS", "stepsister"),
	
	/**
	 * stepson
	 */
	_STPSON ("STPSON", "stepson"),
	
	/**
	 * significant other
	 */
	_SIGOTHR ("SIGOTHR", "significant other"),
	
	/**
	 * spouse
	 */
	_SPS ("SPS", "spouse"),
	
	/**
	 * uncle
	 */
	_UNCLE ("UNCLE", "uncle"),
	
	/**
	 * wife
	 */
	_WIFE ("WIFE", "wife"),
	
	/**
	 * aunt
	 */
	_aunt ("AUNT", "aunt"),
	
	/**
	 * Brother
	 */
	_Brother ("BRO", "Brother"),
	
	/**
	 * brother-in-law
	 */
	_brotherinlaw ("BROINLAW", "brother-in-law"),
	
	/**
	 * Child
	 */
	_Child ("CHILD", "Child"),
	
	/**
	 * adopted child
	 */
	_adoptedchild ("CHLDADOPT", "adopted child"),
	
	/**
	 * foster child
	 */
	_fosterchild ("CHLDFOST", "foster child"),
	
	/**
	 * child in-law
	 */
	_childinlaw ("CHLDINLAW", "child in-law"),
	
	/**
	 * cousin
	 */
	_cousin ("COUSN", "cousin"),
	
	/**
	 * natural daughter
	 */
	_naturaldaughter ("DAU", "natural daughter"),
	
	/**
	 * adopted daughter
	 */
	_adopteddaughter ("DAUADOPT", "adopted daughter"),
	
	/**
	 * foster daughter
	 */
	_fosterdaughter ("DAUFOST", "foster daughter"),
	
	/**
	 * daughter in-law
	 */
	_daughterinlaw ("DAUINLAW", "daughter in-law"),
	
	/**
	 * domestic partner
	 */
	_domesticpartner ("DOMPART", "domestic partner"),
	
	/**
	 * Family Member
	 */
	_FamilyMember ("FAMMEMB", "Family Member"),
	
	/**
	 * unrelated friend
	 */
	_unrelatedfriend ("FRND", "unrelated friend"),
	
	/**
	 * Father
	 */
	_Father ("FTH", "Father"),
	
	/**
	 * father-in-law
	 */
	_fatherinlaw ("FTHINLAW", "father-in-law"),
	
	/**
	 * great grandfather
	 */
	_greatgrandfather ("GGRFTH", "great grandfather"),
	
	/**
	 * great grandparent
	 */
	_greatgrandparent ("GGRPRN", "great grandparent"),
	
	/**
	 * great grandmother
	 */
	_greatgrandmother ("GGRMTH", "great grandmother"),
	
	/**
	 * Grandfather
	 */
	_Grandfather ("GRFTH", "Grandfather"),
	
	/**
	 * Grandmother
	 */
	_Grandmother ("GRMTH", "Grandmother"),
	
	/**
	 * grandchild
	 */
	_grandchild ("GRNDCHILD", "grandchild"),
	
	/**
	 * granddaughter
	 */
	_granddaughter ("GRNDDAU", "granddaughter"),
	
	/**
	 * grandson
	 */
	_grandson ("GRNDSON", "grandson"),
	
	/**
	 * Grandparent
	 */
	_Grandparent ("GRPRN", "Grandparent"),
	
	/**
	 * half-brother
	 */
	_halfbrother ("HBRO", "half-brother"),
	
	/**
	 * half-sibling
	 */
	_halfsibling ("HSIB", "half-sibling"),
	
	/**
	 * half-sister
	 */
	_halfsister ("HSIS", "half-sister"),
	
	/**
	 * husband
	 */
	_husband ("HUSB", "husband"),
	
	/**
	 * Mother
	 */
	_Mother ("MTH", "Mother"),
	
	/**
	 * mother-in-law
	 */
	_motherinlaw ("MTHINLOAW", "mother-in-law"),
	
	/**
	 * neighbor
	 */
	_neighbor ("NBOR", "neighbor"),
	
	/**
	 * natural brother
	 */
	_naturalbrother ("NBRO", "natural brother"),
	
	/**
	 * natural child
	 */
	_naturalchild ("NCHILD", "natural child"),
	
	/**
	 * nephew
	 */
	_nephew ("NEPHEW", "nephew"),
	
	/**
	 * natural father
	 */
	_naturalfather ("NFTH", "natural father"),
	
	/**
	 * niece
	 */
	_niece ("NIECE", "niece"),
	
	/**
	 * niecenephew
	 */
	_niecenephew ("NIENEPH", "niece/nephew"),
	
	/**
	 * natural mother
	 */
	_naturalmother ("NMTH", "natural mother"),
	
	/**
	 * natural parent
	 */
	_naturalparent ("NPRN", "natural parent"),
	
	/**
	 * natural sibling
	 */
	_naturalsibling ("NSIB", "natural sibling"),
	
	/**
	 * natural sister
	 */
	_naturalsister ("NSIS", "natural sister"),
	
	/**
	 * Parent
	 */
	_Parent ("PRN", "Parent"),
	
	/**
	 * parent in-law
	 */
	_parentinlaw ("PRNINLAW", "parent in-law"),
	
	/**
	 * Roommate
	 */
	_Roommate ("ROOM", "Roommate"),
	
	/**
	 * Sibling
	 */
	_Sibling ("SIB", "Sibling"),
	
	/**
	 * sibling in-law
	 */
	_siblinginlaw ("SIBINLAW", "sibling in-law"),
	
	/**
	 * Sister
	 */
	_Sister ("SIS", "Sister"),
	
	/**
	 * sister-in-law
	 */
	_sisterinlaw ("SISLINLAW", "sister-in-law"),
	
	/**
	 * natural son
	 */
	_naturalson ("SON", "natural son"),
	
	/**
	 * adopted son
	 */
	_adoptedson ("SONADOPT", "adopted son"),
	
	/**
	 * foster son
	 */
	_fosterson ("SONFOST", "foster son"),
	
	/**
	 * son in-law
	 */
	_soninlaw ("SONINLAW", "son in-law"),
	
	/**
	 * stepbrother
	 */
	_stepbrother ("STPBRO", "stepbrother"),
	
	/**
	 * step child
	 */
	_stepchild ("STPCHLD", "step child"),
	
	/**
	 * stepdaughter
	 */
	_stepdaughter ("STPDAU", "stepdaughter"),
	
	/**
	 * stepfather
	 */
	_stepfather ("STPFTH", "stepfather"),
	
	/**
	 * stepmother
	 */
	_stepmother ("STPMTH", "stepmother"),
	
	/**
	 * step parent
	 */
	_stepparent ("STPPRN", "step parent"),
	
	/**
	 * step sibling
	 */
	_stepsibling ("STPSIB", "step sibling"),
	
	/**
	 * stepsister
	 */
	_stepsister ("STPSIS", "stepsister"),
	
	/**
	 * stepson
	 */
	_stepson ("STPSON", "stepson"),
	
	/**
	 * significant other
	 */
	_significantother ("SIGOTHR", "significant other"),
	
	/**
	 * spouse
	 */
	_spouse ("SPS", "spouse"),
	
	/**
	 * uncle
	 */
	_uncle ("UNCLE", "uncle"),
	
	/**
	 * wife
	 */
	_wife ("WIFE", "wife"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.1.11.19563";

	PersonalRelationshipRoleType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static PersonalRelationshipRoleType getByCode(String code) {
		PersonalRelationshipRoleType[] vals = values();
		for (PersonalRelationshipRoleType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(PersonalRelationshipRoleType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	