/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

public enum TelecomUseType implements VocabularyEntry {
	/** A communication address at a home, attempted contacts for business purposes might intrude privacy and chances are one will contact family or other household members instead of the person one wishes to call. Typically used with urgent cases, or if no other contacts are available. **/
	HomeAddress("H"),
	/** The primary home, to reach a person after business hours. **/
	PrimaryHome("HP"),
	/** A vacation home, to reach a person while on vacation. **/
	VacationHome("HV"),
	/** An office address. First choice for business related contacts during business hours. **/
	WorkPlace("WP"),
	/** An automated answering machine used for less urgent cases and if the main purpose of contact is to leave a message or access an automated announcement. **/
	AnsweringService("AS"),
	/** A contact specifically designated to be used for emergencies. This is the first choice in emergencies, independent of any other use codes. **/
	EmergencyContact("EC"),
	/** A paging device suitable to solicit a callback or to leave a very short message. **/
	Pager("PG"),
	/** A telecommunication device that moves and stays with its owner. May have characteristics of all other use codes, suitable for urgent matters, not the first choice for routine business. **/
	MobileContact("MC");
	
	public String code;
	
	private TelecomUseType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static TelecomUseType getByCode(String code) {
		TelecomUseType[] vals = values();
		for (TelecomUseType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(TelecomUseType other) {
		return (other.getCode().equals(code));
	}
}
