/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the Documenttype vocabulary:
 * <ul>
 *   <li>_134188003 : Statement of special educational needs</li>
 *   <li>_184809009 : Employment report - no examination</li>
 *   <li>_184811000 : NHS employee report - no examination</li>
 *   <li>_184813002 : Government department employee - report only</li>
 *   <li>_184816005 : Local authority employee - report</li>
 *   <li>_184823006 : Special activities med report</li>
 *   <li>_184850001 : Insurance report - travel cancellation</li>
 *   <li>_184873009 : Social security claim report</li>
 *   <li>_184880006 : Application to register disabled report</li>
 *   <li>_184881005 : Disabled registration application - minimal report</li>
 *   <li>_184882003 : Disabled registration application - full report</li>
 *   <li>_185005006 : Report from Health Visitor</li>
 *   <li>_185014001 : Report to benefits agency</li>
 *   <li>_185076008 : BAAF CD-adoptchild report</li>
 *   <li>_185078009 : BAAF D-child 2yrs rep sent</li>
 *   <li>_186065003 : Power of attorney medical report</li>
 *   <li>_229059009 : Report</li>
 *   <li>_270102001 : Report for Coroner</li>
 *   <li>_270107007 : Coroners postmortem report</li>
 *   <li>_270116006 : Postmortem report</li>
 *   <li>_270358003 : Life assurance - preliminary report</li>
 *   <li>_270370004 : Driving licence fitness report</li>
 *   <li>_270372007 : Disabled driver orange badge report</li>
 *   <li>_270389004 : Mobility allowance claim representative</li>
 *   <li>_270390008 : BAAF Adult 12 - adoption applicant report</li>
 *   <li>_270840000 : Disabled drivers orange badge</li>
 *   <li>_271452006 : Solicitors report</li>
 *   <li>_271531001 : BAAF B12 - adoption birth history</li>
 *   <li>_307881004 : DSS RMO RM2 report</li>
 *   <li>_308375000 : Report for Procurator-Fiscal</li>
 *   <li>_308376004 : Police surgeon postmortem report</li>
 *   <li>_308575004 : Employment report</li>
 *   <li>_308584004 : DLA 370 report</li>
 *   <li>_308585003 : DS 1500 report</li>
 *   <li>_308588001 : BAAF C infant report</li>
 *   <li>_308589009 : BAAF D-child 2yrs report</li>
 *   <li>_308619006 : War pensions report</li>
 *   <li>_308621001 : RM10-DHSS DMO report</li>
 *   <li>_310601008 : Report to Drug Safety Research Unit</li>
 *   <li>_310854009 : Housing report</li>
 *   <li>_313277003 : Adoption - birth history report</li>
 *   <li>_371524004 : Clinical report</li>
 *   <li>_371525003 : Clinical procedure report</li>
 *   <li>_371526002 : Operative report</li>
 *   <li>_371527006 : Radiology report</li>
 *   <li>_371529009 : History and physical report</li>
 *   <li>_371530004 : Clinical consultation report</li>
 *   <li>_371531000 : Encounter report</li>
 *   <li>_371532007 : Progress report</li>
 *   <li>_371534008 : Summary report</li>
 *   <li>_371535009 : Transfer summary report</li>
 *   <li>_371536005 : Flow sheet report</li>
 *   <li>_371537001 : Consent report</li>
 *   <li>_371538006 : Advance directive report</li>
 *   <li>_371539003 : Conference report</li>
 *   <li>_371540001 : Supervising physician report</li>
 *   <li>_371541002 : Provider comment report</li>
 *   <li>_371542009 : Admission history and physical report</li>
 *   <li>_371543004 : Comprehensive history and physical report</li>
 *   <li>_371544005 : Targeted history and physical report</li>
 *   <li>_371545006 : Confirmatory consultation report</li>
 *   <li>_371546007 : Educational visit report</li>
 *   <li>_373942005 : Discharge summary</li>
 *   <li>_399345000 : Adult echocardiography procedure report</li>
 *   <li>_404187006 : Statement of needs</li>
 *   <li>_406550009 : Summary of needs</li>
 *   <li>_416700005 : Written operative report</li>
 *   <li>_417185000 : Dictated operative report</li>
 *   <li>_422735006 : Summary clinical document</li>
 *   <li>_423876004 : Clinical document</li>
 *   <li>_444754002 : Hospital inpatient progress report</li>
 *   <li>_444804000 : Multidisciplinary care conference report</li>
 *   <li>_445042000 : Hospital outpatient progress report</li>
 *   <li>_697978002 : Provider orders for life-sustaining treatment</li>
 *   <li>_163251000000107 : FocusActOrEvent concept</li>
 *   <li>_163261000000105 : Primary and Community Care Event</li>
 *   <li>_163271000000103 : General Practitioner Event</li>
 *   <li>_163281000000101 : Practice Nurse Event</li>
 *   <li>_163291000000104 : District Nurse Event</li>
 *   <li>_163301000000100 : Physiotherapist Event</li>
 *   <li>_163311000000103 : Occupational Therapist Event</li>
 *   <li>_163321000000109 : Community Midwife Event</li>
 *   <li>_163331000000106 : Health Visitor Event</li>
 *   <li>_163341000000102 : Chiropodist Event</li>
 *   <li>_163351000000104 : Dietician Event</li>
 *   <li>_163361000000101 : Out of Hours Event</li>
 *   <li>_163371000000108 : Secondary Care Event</li>
 *   <li>_163381000000105 : Outpatient Clinic Attendance</li>
 *   <li>_163391000000107 : Discharge from Inpatient Care</li>
 *   <li>_163401000000105 : Discharge from Day Case Care</li>
 *   <li>_163411000000107 : Ward Attendance</li>
 *   <li>_163421000000101 : Single Assessment Process Event</li>
 *   <li>_163431000000104 : Single Assessment Process Contact Assessment</li>
 *   <li>_163441000000108 : Single Assessment Process Overview Assessment</li>
 *   <li>_163451000000106 : Mental Health Event</li>
 *   <li>_163461000000109 : Entry to Mental Health Services</li>
 *   <li>_163471000000102 : Care Programme Approach Summary</li>
 *   <li>_163481000000100 : Discharge from Mental Health Services</li>
 *   <li>_163491000000103 : Medication Management Event</li>
 *   <li>_163501000000109 : Prescription</li>
 *   <li>_163511000000106 : Prescription Cancel Request</li>
 *   <li>_163521000000100 : Prescription Cancel Response</li>
 *   <li>_163531000000103 : Protocol Supply Medication</li>
 *   <li>_163541000000107 : Dispensed Medication</li>
 *   <li>_163551000000105 : Personally Administered Medication</li>
 *   <li>_163561000000108 : Diagnostic Imaging Event</li>
 *   <li>_163571000000101 : Diagnostic Imaging Encounter</li>
 *   <li>_163581000000104 : Diagnostic Imaging Report</li>
 *   <li>_163751000000100 : Care Programme Approach summary</li>
 *   <li>_181221000000100 : Diagnostic Imaging Request Event - FocusActOrEvent</li>
 *   <li>_185291000000100 : Emergency Department Event</li>
 *   <li>_185301000000101 : Minor Injuries Department Event</li>
 *   <li>_185311000000104 : Walk-in Centre Event</li>
 *   <li>_193821000000101 : Pathology Event</li>
 *   <li>_193831000000104 : Pathology Request Event</li>
 *   <li>_193841000000108 : Pathology Report Event</li>
 *   <li>_196971000000103 : General Practice Initial Summary</li>
 *   <li>_196981000000101 : General Practice Summary</li>
 *   <li>_197391000000101 : NHS Direct Event</li>
 *   <li>_283281000000100 : HealthSpace summary</li>
 *   <li>_285091000000108 : Nullification Document</li>
 *   <li>_285101000000100 : Refusal To Seal</li>
 *   <li>_304391000000103 : Statement of educational needs</li>
 *   <li>_305751000000109 : Sealed Envelope Event</li>
 *   <li>_305761000000107 : Unseal Report</li>
 *   <li>_305771000000100 : Seal Report</li>
 *   <li>_312341000000104 : Urgent Care Summary Report</li>
 *   <li>_312421000000103 : NHS Direct Summary Report</li>
 *   <li>_312711000000101 : Ambulance Service Patient Summary Report</li>
 *   <li>_312991000000108 : Mental Health Act Assessment Outside Specialist Services</li>
 *   <li>_313001000000107 : Update to Mental Health Act Assessment Non-Specialist Services</li>
 *   <li>_313011000000109 : Entry to Mental Health and Substance Misuse Specialist Services</li>
 *   <li>_313021000000103 : Update to Mental Health and Substance Misuse Specialist Services</li>
 *   <li>_313031000000101 : Exit from Mental Health and Substance Misuse Specialist Services</li>
 *   <li>_313041000000105 : Mental Health and Substance Misuse Specialist Services Care Programme Approach Review Summary</li>
 *   <li>_313051000000108 : Mental Health and Substance Misuse Specialist Services Models of Care Review Summary</li>
 *   <li>_313061000000106 : Mental Health and Substance Misuse Specialist Services Unspecified Care Review Summary</li>
 *   <li>_313071000000104 : Admission to Inpatient Care</li>
 *   <li>_313081000000102 : Assessment Event</li>
 *   <li>_313091000000100 : Contact Assessment</li>
 *   <li>_313101000000108 : Overview Assessment</li>
 *   <li>_313111000000105 : Integrated Care and Support Plan</li>
 *   <li>_313121000000104 : Specialist Assessment Outcome</li>
 *   <li>_313141000000106 : Mental Health and Substance Misuse Specialist Services Event</li>
 *   <li>_320151000000108 : Biographical Information</li>
 *   <li>_339561000000108 : Withdrawal Notification</li>
 *   <li>_384101000000107 : Telehealth Event</li>
 *   <li>_384111000000109 : Personal Health Monitoring Report</li>
 *   <li>_384121000000103 : Out of Tolerance Notification</li>
 *   <li>_395641000000101 : British Association for Adoption and Fostering B2 birth history report</li>
 *   <li>_461421000000103 : BAAF B1 obstetric report</li>
 *   <li>_491811000000109 : Personal Health Monitoring Report - Action Required</li>
 *   <li>_491821000000103 : Personal Health Monitoring Report - For Information Only</li>
 *   <li>_491831000000101 : Personal Health Monitoring Report - Routine Scheduled</li>
 *   <li>_715591000000108 : Hospital Discharge Notification to Social Care</li>
 *   <li>_715601000000102 : Assessment Notification Under Section 2 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715611000000100 : Assessment Notification Accept Response Under Section 2 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715621000000106 : Assessment Notification Reject Response Under Section 2 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715631000000108 : Discharge Notification Under Section 5 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715641000000104 : Discharge Notification Agreement Under Section 5 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715651000000101 : Discharge Notification Disagreement Under Section 5 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715661000000103 : Cancellation Notification Under Section 3 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715681000000107 : Cancellation of Section 5 Notification Under Section 3 of the Community Care Delayed Discharges etc Act 2003</li>
 *   <li>_715721000000100 : Assessment Notification Under Section 2 of the Community Care Delayed Discharges etc Act 2003 Document</li>
 *   <li>_715731000000103 : Discharge Notification Under Section 5 of the Community Care Delayed Discharges etc Act 2003 Document</li>
 *   <li>_716061000000101 : Diagnostic imaging procedure report</li>
 *   <li>_716371000000105 : Diagnostic imaging summary report</li>
 *   <li>_717321000000108 : Diagnostic imaging report</li>
 *   <li>_750001000000109 : Integrated Care and Support Plan - Continuing Health Care Funded</li>
 *   <li>_750011000000106 : Integrated Care and Support Plan - Funded Nursing Care</li>
 *   <li>_750021000000100 : Integrated Care and Support Plan - Local Authority Joint Funded</li>
 *   <li>_750031000000103 : Integrated Care and Support Plan - Local Authority Only Funded</li>
 *   <li>_762391000000107 : Cancer multidisciplinary team report</li>
 *   <li>_766751000000101 : NHS Continuing Healthcare</li>
 *   <li>_766761000000103 : NHS Continuing Healthcare Fast Track Pathway Tool</li>
 *   <li>_766771000000105 : NHS Continuing Healthcare Fast Track Pathway Tool Acceptance</li>
 *   <li>_766781000000107 : NHS Continuing Healthcare Checklist Assessment</li>
 *   <li>_766791000000109 : NHS Continuing Healthcare Checklist Assessment Acceptance</li>
 *   <li>_766801000000108 : NHS Continuing Healthcare Request for Assessment</li>
 *   <li>_766811000000105 : NHS Continuing Healthcare Request for Assessment Acceptance</li>
 *   <li>_766821000000104 : NHS Continuing Healthcare Decision Support Tool</li>
 *   <li>_766831000000102 : NHS Continuing Healthcare Cancellation Notification</li>
 *   <li>_768131000000105 : NHS Continuing Healthcare Fast Track Pathway Tool Rejection</li>
 *   <li>_768141000000101 : NHS Continuing Healthcare Checklist Assessment Rejection</li>
 *   <li>_768151000000103 : NHS Continuing Healthcare Request for Assessment Rejection</li>
 *   <li>_801531000000106 : Outpatient Clinic Attendance - Consultant Referral Letter</li>
 *   <li>_801541000000102 : Outpatient Clinic Attendance - GP Letter</li>
 *   <li>_801551000000104 : Outpatient Clinic Attendance - Patient Letter</li>
 *   <li>_801561000000101 : Outpatient Clinic Attendance - Maxillofacial and Tertiary Letter</li>
 *   <li>_801571000000108 : Outpatient Clinic Attendance - Results Letter</li>
 *   <li>_801581000000105 : Outpatient Clinic Attendance - No Patient Copy Letter</li>
 *   <li>_801591000000107 : Outpatient Clinic Attendance - Safeguarded Letter</li>
 *   <li>_801601000000101 : Outpatient Clinic Attendance - Administration Letter</li>
 *   <li>_801611000000104 : Outpatient Clinic Attendance - Private Practice Letter</li>
 *   <li>_819541000000103 : NHS 111 Event</li>
 *   <li>_819551000000100 : NHS 111 Report</li>
 *   <li>_828791000000100 : NHS 111 Interim Ambulance Request</li>
 *   <li>_828801000000101 : NHS 111 Final Ambulance Request</li>
 *   <li>_829581000000103 : Life insurance report</li>
 *   <li>_846951000000108 : GP Referral</li>
 *   <li>_846961000000106 : GP Referral Document</li>
 *   <li>_846971000000104 : GP Referral Accept Response</li>
 *   <li>_846981000000102 : GP Referral Reject Response</li>
 *   <li>_859741000000101 : Telehealth Referral</li>
 *   <li>_859751000000103 : Telehealth Clinician Response</li>
 *   <li>_861411000000103 : End of Life Care Document</li>
 *   <li>_861421000000109 : End of Life Care Coordination Summary</li>
 *   <li>_866371000000107 : Child Screening Report</li>
 *   <li>_909021000000108 : Safeguarding report</li>
 *   <li>_909031000000105 : Adult safeguarding report</li>
 *   <li>_909041000000101 : Child safeguarding report</li>
 *   <li>_914851000000108 : Ambulatory ECG electrocardiography monitoring report</li>
 *   <li>_914861000000106 : ETT exercise tolerance test report</li>
 *   <li>_918401000000103 : Implanted device maintenance report</li>
 *   <li>_927071000000108 : Upper gastrointestinal tract endoscopy report</li>
 *   <li>_927081000000105 : Lower gastrointestinal tract endoscopy report</li>
 *   <li>_958141000000101 : BAAF C-infant report sent</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum Documenttype implements VocabularyEntry {
	
	
	/**
	 * Statement of special educational needs
	 */
	_134188003 ("134188003", "Statement of special educational needs"),
	
	/**
	 * Employment report - no examination
	 */
	_184809009 ("184809009", "Employment report - no examination"),
	
	/**
	 * NHS employee report - no examination
	 */
	_184811000 ("184811000", "NHS employee report - no examination"),
	
	/**
	 * Government department employee - report only
	 */
	_184813002 ("184813002", "Government department employee - report only"),
	
	/**
	 * Local authority employee - report
	 */
	_184816005 ("184816005", "Local authority employee - report"),
	
	/**
	 * Special activities med report
	 */
	_184823006 ("184823006", "Special activities med report"),
	
	/**
	 * Insurance report - travel cancellation
	 */
	_184850001 ("184850001", "Insurance report - travel cancellation"),
	
	/**
	 * Social security claim report
	 */
	_184873009 ("184873009", "Social security claim report"),
	
	/**
	 * Application to register disabled report
	 */
	_184880006 ("184880006", "Application to register disabled report"),
	
	/**
	 * Disabled registration application - minimal report
	 */
	_184881005 ("184881005", "Disabled registration application - minimal report"),
	
	/**
	 * Disabled registration application - full report
	 */
	_184882003 ("184882003", "Disabled registration application - full report"),
	
	/**
	 * Report from Health Visitor
	 */
	_185005006 ("185005006", "Report from Health Visitor"),
	
	/**
	 * Report to benefits agency
	 */
	_185014001 ("185014001", "Report to benefits agency"),
	
	/**
	 * BAAF CD-adoptchild report
	 */
	_185076008 ("185076008", "BAAF C/D-adopt:child report"),
	
	/**
	 * BAAF D-child 2yrs rep sent
	 */
	_185078009 ("185078009", "BAAF D-child 2+yrs rep sent"),
	
	/**
	 * Power of attorney medical report
	 */
	_186065003 ("186065003", "Power of attorney medical report"),
	
	/**
	 * Report
	 */
	_229059009 ("229059009", "Report"),
	
	/**
	 * Report for Coroner
	 */
	_270102001 ("270102001", "Report for Coroner"),
	
	/**
	 * Coroners postmortem report
	 */
	_270107007 ("270107007", "Coroner's postmortem report"),
	
	/**
	 * Postmortem report
	 */
	_270116006 ("270116006", "Postmortem report"),
	
	/**
	 * Life assurance - preliminary report
	 */
	_270358003 ("270358003", "Life assurance - preliminary report"),
	
	/**
	 * Driving licence fitness report
	 */
	_270370004 ("270370004", "Driving licence fitness report"),
	
	/**
	 * Disabled driver orange badge report
	 */
	_270372007 ("270372007", "Disabled driver orange badge report"),
	
	/**
	 * Mobility allowance claim representative
	 */
	_270389004 ("270389004", "Mobility allowance claim representative"),
	
	/**
	 * BAAF Adult 12 - adoption applicant report
	 */
	_270390008 ("270390008", "BAAF Adult 1/2 - adoption: applicant report"),
	
	/**
	 * Disabled drivers orange badge
	 */
	_270840000 ("270840000", "Disabled drivers orange badge"),
	
	/**
	 * Solicitors report
	 */
	_271452006 ("271452006", "Solicitors report"),
	
	/**
	 * BAAF B12 - adoption birth history
	 */
	_271531001 ("271531001", "BAAF B1/2 - adoption: birth history"),
	
	/**
	 * DSS RMO RM2 report
	 */
	_307881004 ("307881004", "DSS RMO RM2 report"),
	
	/**
	 * Report for Procurator-Fiscal
	 */
	_308375000 ("308375000", "Report for Procurator-Fiscal"),
	
	/**
	 * Police surgeon postmortem report
	 */
	_308376004 ("308376004", "Police surgeon postmortem report"),
	
	/**
	 * Employment report
	 */
	_308575004 ("308575004", "Employment report"),
	
	/**
	 * DLA 370 report
	 */
	_308584004 ("308584004", "DLA 370 report"),
	
	/**
	 * DS 1500 report
	 */
	_308585003 ("308585003", "DS 1500 report"),
	
	/**
	 * BAAF C infant report
	 */
	_308588001 ("308588001", "BAAF C infant report"),
	
	/**
	 * BAAF D-child 2yrs report
	 */
	_308589009 ("308589009", "BAAF D-child 2+yrs report"),
	
	/**
	 * War pensions report
	 */
	_308619006 ("308619006", "War pensions report"),
	
	/**
	 * RM10-DHSS DMO report
	 */
	_308621001 ("308621001", "RM10-DHSS DMO report"),
	
	/**
	 * Report to Drug Safety Research Unit
	 */
	_310601008 ("310601008", "Report to Drug Safety Research Unit"),
	
	/**
	 * Housing report
	 */
	_310854009 ("310854009", "Housing report"),
	
	/**
	 * Adoption - birth history report
	 */
	_313277003 ("313277003", "Adoption - birth history report"),
	
	/**
	 * Clinical report
	 */
	_371524004 ("371524004", "Clinical report"),
	
	/**
	 * Clinical procedure report
	 */
	_371525003 ("371525003", "Clinical procedure report"),
	
	/**
	 * Operative report
	 */
	_371526002 ("371526002", "Operative report"),
	
	/**
	 * Radiology report
	 */
	_371527006 ("371527006", "Radiology report"),
	
	/**
	 * History and physical report
	 */
	_371529009 ("371529009", "History and physical report"),
	
	/**
	 * Clinical consultation report
	 */
	_371530004 ("371530004", "Clinical consultation report"),
	
	/**
	 * Encounter report
	 */
	_371531000 ("371531000", "Encounter report"),
	
	/**
	 * Progress report
	 */
	_371532007 ("371532007", "Progress report"),
	
	/**
	 * Summary report
	 */
	_371534008 ("371534008", "Summary report"),
	
	/**
	 * Transfer summary report
	 */
	_371535009 ("371535009", "Transfer summary report"),
	
	/**
	 * Flow sheet report
	 */
	_371536005 ("371536005", "Flow sheet report"),
	
	/**
	 * Consent report
	 */
	_371537001 ("371537001", "Consent report"),
	
	/**
	 * Advance directive report
	 */
	_371538006 ("371538006", "Advance directive report"),
	
	/**
	 * Conference report
	 */
	_371539003 ("371539003", "Conference report"),
	
	/**
	 * Supervising physician report
	 */
	_371540001 ("371540001", "Supervising physician report"),
	
	/**
	 * Provider comment report
	 */
	_371541002 ("371541002", "Provider comment report"),
	
	/**
	 * Admission history and physical report
	 */
	_371542009 ("371542009", "Admission history and physical report"),
	
	/**
	 * Comprehensive history and physical report
	 */
	_371543004 ("371543004", "Comprehensive history and physical report"),
	
	/**
	 * Targeted history and physical report
	 */
	_371544005 ("371544005", "Targeted history and physical report"),
	
	/**
	 * Confirmatory consultation report
	 */
	_371545006 ("371545006", "Confirmatory consultation report"),
	
	/**
	 * Educational visit report
	 */
	_371546007 ("371546007", "Educational visit report"),
	
	/**
	 * Discharge summary
	 */
	_373942005 ("373942005", "Discharge summary"),
	
	/**
	 * Adult echocardiography procedure report
	 */
	_399345000 ("399345000", "Adult echocardiography procedure report"),
	
	/**
	 * Statement of needs
	 */
	_404187006 ("404187006", "Statement of needs"),
	
	/**
	 * Summary of needs
	 */
	_406550009 ("406550009", "Summary of needs"),
	
	/**
	 * Written operative report
	 */
	_416700005 ("416700005", "Written operative report"),
	
	/**
	 * Dictated operative report
	 */
	_417185000 ("417185000", "Dictated operative report"),
	
	/**
	 * Summary clinical document
	 */
	_422735006 ("422735006", "Summary clinical document"),
	
	/**
	 * Clinical document
	 */
	_423876004 ("423876004", "Clinical document"),
	
	/**
	 * Hospital inpatient progress report
	 */
	_444754002 ("444754002", "Hospital inpatient progress report"),
	
	/**
	 * Multidisciplinary care conference report
	 */
	_444804000 ("444804000", "Multidisciplinary care conference report"),
	
	/**
	 * Hospital outpatient progress report
	 */
	_445042000 ("445042000", "Hospital outpatient progress report"),
	
	/**
	 * Provider orders for life-sustaining treatment
	 */
	_697978002 ("697978002", "Provider orders for life-sustaining treatment"),
	
	/**
	 * FocusActOrEvent concept
	 */
	_163251000000107 ("163251000000107", "FocusActOrEvent concept"),
	
	/**
	 * Primary and Community Care Event
	 */
	_163261000000105 ("163261000000105", "Primary and Community Care Event"),
	
	/**
	 * General Practitioner Event
	 */
	_163271000000103 ("163271000000103", "General Practitioner Event"),
	
	/**
	 * Practice Nurse Event
	 */
	_163281000000101 ("163281000000101", "Practice Nurse Event"),
	
	/**
	 * District Nurse Event
	 */
	_163291000000104 ("163291000000104", "District Nurse Event"),
	
	/**
	 * Physiotherapist Event
	 */
	_163301000000100 ("163301000000100", "Physiotherapist Event"),
	
	/**
	 * Occupational Therapist Event
	 */
	_163311000000103 ("163311000000103", "Occupational Therapist Event"),
	
	/**
	 * Community Midwife Event
	 */
	_163321000000109 ("163321000000109", "Community Midwife Event"),
	
	/**
	 * Health Visitor Event
	 */
	_163331000000106 ("163331000000106", "Health Visitor Event"),
	
	/**
	 * Chiropodist Event
	 */
	_163341000000102 ("163341000000102", "Chiropodist Event"),
	
	/**
	 * Dietician Event
	 */
	_163351000000104 ("163351000000104", "Dietician Event"),
	
	/**
	 * Out of Hours Event
	 */
	_163361000000101 ("163361000000101", "Out of Hours Event"),
	
	/**
	 * Secondary Care Event
	 */
	_163371000000108 ("163371000000108", "Secondary Care Event"),
	
	/**
	 * Outpatient Clinic Attendance
	 */
	_163381000000105 ("163381000000105", "Outpatient Clinic Attendance"),
	
	/**
	 * Discharge from Inpatient Care
	 */
	_163391000000107 ("163391000000107", "Discharge from Inpatient Care"),
	
	/**
	 * Discharge from Day Case Care
	 */
	_163401000000105 ("163401000000105", "Discharge from Day Case Care"),
	
	/**
	 * Ward Attendance
	 */
	_163411000000107 ("163411000000107", "Ward Attendance"),
	
	/**
	 * Single Assessment Process Event
	 */
	_163421000000101 ("163421000000101", "Single Assessment Process Event"),
	
	/**
	 * Single Assessment Process Contact Assessment
	 */
	_163431000000104 ("163431000000104", "Single Assessment Process Contact Assessment"),
	
	/**
	 * Single Assessment Process Overview Assessment
	 */
	_163441000000108 ("163441000000108", "Single Assessment Process Overview Assessment"),
	
	/**
	 * Mental Health Event
	 */
	_163451000000106 ("163451000000106", "Mental Health Event"),
	
	/**
	 * Entry to Mental Health Services
	 */
	_163461000000109 ("163461000000109", "Entry to Mental Health Services"),
	
	/**
	 * Care Programme Approach Summary
	 */
	_163471000000102 ("163471000000102", "Care Programme Approach Summary"),
	
	/**
	 * Discharge from Mental Health Services
	 */
	_163481000000100 ("163481000000100", "Discharge from Mental Health Services"),
	
	/**
	 * Medication Management Event
	 */
	_163491000000103 ("163491000000103", "Medication Management Event"),
	
	/**
	 * Prescription
	 */
	_163501000000109 ("163501000000109", "Prescription"),
	
	/**
	 * Prescription Cancel Request
	 */
	_163511000000106 ("163511000000106", "Prescription Cancel Request"),
	
	/**
	 * Prescription Cancel Response
	 */
	_163521000000100 ("163521000000100", "Prescription Cancel Response"),
	
	/**
	 * Protocol Supply Medication
	 */
	_163531000000103 ("163531000000103", "Protocol Supply Medication"),
	
	/**
	 * Dispensed Medication
	 */
	_163541000000107 ("163541000000107", "Dispensed Medication"),
	
	/**
	 * Personally Administered Medication
	 */
	_163551000000105 ("163551000000105", "Personally Administered Medication"),
	
	/**
	 * Diagnostic Imaging Event
	 */
	_163561000000108 ("163561000000108", "Diagnostic Imaging Event"),
	
	/**
	 * Diagnostic Imaging Encounter
	 */
	_163571000000101 ("163571000000101", "Diagnostic Imaging Encounter"),
	
	/**
	 * Diagnostic Imaging Report
	 */
	_163581000000104 ("163581000000104", "Diagnostic Imaging Report"),
	
	/**
	 * Care Programme Approach summary
	 */
	_163751000000100 ("163751000000100", "Care Programme Approach summary"),
	
	/**
	 * Diagnostic Imaging Request Event - FocusActOrEvent
	 */
	_181221000000100 ("181221000000100", "Diagnostic Imaging Request Event - FocusActOrEvent"),
	
	/**
	 * Emergency Department Event
	 */
	_185291000000100 ("185291000000100", "Emergency Department Event"),
	
	/**
	 * Minor Injuries Department Event
	 */
	_185301000000101 ("185301000000101", "Minor Injuries Department Event"),
	
	/**
	 * Walk-in Centre Event
	 */
	_185311000000104 ("185311000000104", "Walk-in Centre Event"),
	
	/**
	 * Pathology Event
	 */
	_193821000000101 ("193821000000101", "Pathology Event"),
	
	/**
	 * Pathology Request Event
	 */
	_193831000000104 ("193831000000104", "Pathology Request Event"),
	
	/**
	 * Pathology Report Event
	 */
	_193841000000108 ("193841000000108", "Pathology Report Event"),
	
	/**
	 * General Practice Initial Summary
	 */
	_196971000000103 ("196971000000103", "General Practice Initial Summary"),
	
	/**
	 * General Practice Summary
	 */
	_196981000000101 ("196981000000101", "General Practice Summary"),
	
	/**
	 * NHS Direct Event
	 */
	_197391000000101 ("197391000000101", "NHS Direct Event"),
	
	/**
	 * HealthSpace summary
	 */
	_283281000000100 ("283281000000100", "HealthSpace summary"),
	
	/**
	 * Nullification Document
	 */
	_285091000000108 ("285091000000108", "Nullification Document"),
	
	/**
	 * Refusal To Seal
	 */
	_285101000000100 ("285101000000100", "Refusal To Seal"),
	
	/**
	 * Statement of educational needs
	 */
	_304391000000103 ("304391000000103", "Statement of educational needs"),
	
	/**
	 * Sealed Envelope Event
	 */
	_305751000000109 ("305751000000109", "Sealed Envelope Event"),
	
	/**
	 * Unseal Report
	 */
	_305761000000107 ("305761000000107", "Unseal Report"),
	
	/**
	 * Seal Report
	 */
	_305771000000100 ("305771000000100", "Seal Report"),
	
	/**
	 * Urgent Care Summary Report
	 */
	_312341000000104 ("312341000000104", "Urgent Care Summary Report"),
	
	/**
	 * NHS Direct Summary Report
	 */
	_312421000000103 ("312421000000103", "NHS Direct Summary Report"),
	
	/**
	 * Ambulance Service Patient Summary Report
	 */
	_312711000000101 ("312711000000101", "Ambulance Service Patient Summary Report"),
	
	/**
	 * Mental Health Act Assessment Outside Specialist Services
	 */
	_312991000000108 ("312991000000108", "Mental Health Act Assessment Outside Specialist Services"),
	
	/**
	 * Update to Mental Health Act Assessment Non-Specialist Services
	 */
	_313001000000107 ("313001000000107", "Update to Mental Health Act Assessment Non-Specialist Services"),
	
	/**
	 * Entry to Mental Health and Substance Misuse Specialist Services
	 */
	_313011000000109 ("313011000000109", "Entry to Mental Health and Substance Misuse Specialist Services"),
	
	/**
	 * Update to Mental Health and Substance Misuse Specialist Services
	 */
	_313021000000103 ("313021000000103", "Update to Mental Health and Substance Misuse Specialist Services"),
	
	/**
	 * Exit from Mental Health and Substance Misuse Specialist Services
	 */
	_313031000000101 ("313031000000101", "Exit from Mental Health and Substance Misuse Specialist Services"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Care Programme Approach Review Summary
	 */
	_313041000000105 ("313041000000105", "Mental Health and Substance Misuse Specialist Services Care Programme Approach Review Summary"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Models of Care Review Summary
	 */
	_313051000000108 ("313051000000108", "Mental Health and Substance Misuse Specialist Services Models of Care Review Summary"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Unspecified Care Review Summary
	 */
	_313061000000106 ("313061000000106", "Mental Health and Substance Misuse Specialist Services Unspecified Care Review Summary"),
	
	/**
	 * Admission to Inpatient Care
	 */
	_313071000000104 ("313071000000104", "Admission to Inpatient Care"),
	
	/**
	 * Assessment Event
	 */
	_313081000000102 ("313081000000102", "Assessment Event"),
	
	/**
	 * Contact Assessment
	 */
	_313091000000100 ("313091000000100", "Contact Assessment"),
	
	/**
	 * Overview Assessment
	 */
	_313101000000108 ("313101000000108", "Overview Assessment"),
	
	/**
	 * Integrated Care and Support Plan
	 */
	_313111000000105 ("313111000000105", "Integrated Care and Support Plan"),
	
	/**
	 * Specialist Assessment Outcome
	 */
	_313121000000104 ("313121000000104", "Specialist Assessment Outcome"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Event
	 */
	_313141000000106 ("313141000000106", "Mental Health and Substance Misuse Specialist Services Event"),
	
	/**
	 * Biographical Information
	 */
	_320151000000108 ("320151000000108", "Biographical Information"),
	
	/**
	 * Withdrawal Notification
	 */
	_339561000000108 ("339561000000108", "Withdrawal Notification"),
	
	/**
	 * Telehealth Event
	 */
	_384101000000107 ("384101000000107", "Telehealth Event"),
	
	/**
	 * Personal Health Monitoring Report
	 */
	_384111000000109 ("384111000000109", "Personal Health Monitoring Report"),
	
	/**
	 * Out of Tolerance Notification
	 */
	_384121000000103 ("384121000000103", "Out of Tolerance Notification"),
	
	/**
	 * British Association for Adoption and Fostering B2 birth history report
	 */
	_395641000000101 ("395641000000101", "British Association for Adoption and Fostering B2 birth history report"),
	
	/**
	 * BAAF B1 obstetric report
	 */
	_461421000000103 ("461421000000103", "BAAF B1 obstetric report"),
	
	/**
	 * Personal Health Monitoring Report - Action Required
	 */
	_491811000000109 ("491811000000109", "Personal Health Monitoring Report - Action Required"),
	
	/**
	 * Personal Health Monitoring Report - For Information Only
	 */
	_491821000000103 ("491821000000103", "Personal Health Monitoring Report - For Information Only"),
	
	/**
	 * Personal Health Monitoring Report - Routine Scheduled
	 */
	_491831000000101 ("491831000000101", "Personal Health Monitoring Report - Routine Scheduled"),
	
	/**
	 * Hospital Discharge Notification to Social Care
	 */
	_715591000000108 ("715591000000108", "Hospital Discharge Notification to Social Care"),
	
	/**
	 * Assessment Notification Under Section 2 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715601000000102 ("715601000000102", "Assessment Notification Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Assessment Notification Accept Response Under Section 2 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715611000000100 ("715611000000100", "Assessment Notification Accept Response Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Assessment Notification Reject Response Under Section 2 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715621000000106 ("715621000000106", "Assessment Notification Reject Response Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Discharge Notification Under Section 5 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715631000000108 ("715631000000108", "Discharge Notification Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Discharge Notification Agreement Under Section 5 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715641000000104 ("715641000000104", "Discharge Notification Agreement Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Discharge Notification Disagreement Under Section 5 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715651000000101 ("715651000000101", "Discharge Notification Disagreement Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Cancellation Notification Under Section 3 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715661000000103 ("715661000000103", "Cancellation Notification Under Section 3 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Cancellation of Section 5 Notification Under Section 3 of the Community Care Delayed Discharges etc Act 2003
	 */
	_715681000000107 ("715681000000107", "Cancellation of Section 5 Notification Under Section 3 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Assessment Notification Under Section 2 of the Community Care Delayed Discharges etc Act 2003 Document
	 */
	_715721000000100 ("715721000000100", "Assessment Notification Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003 Document"),
	
	/**
	 * Discharge Notification Under Section 5 of the Community Care Delayed Discharges etc Act 2003 Document
	 */
	_715731000000103 ("715731000000103", "Discharge Notification Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003 Document"),
	
	/**
	 * Diagnostic imaging procedure report
	 */
	_716061000000101 ("716061000000101", "Diagnostic imaging procedure report"),
	
	/**
	 * Diagnostic imaging summary report
	 */
	_716371000000105 ("716371000000105", "Diagnostic imaging summary report"),
	
	/**
	 * Diagnostic imaging report
	 */
	_717321000000108 ("717321000000108", "Diagnostic imaging report"),
	
	/**
	 * Integrated Care and Support Plan - Continuing Health Care Funded
	 */
	_750001000000109 ("750001000000109", "Integrated Care and Support Plan - Continuing Health Care Funded"),
	
	/**
	 * Integrated Care and Support Plan - Funded Nursing Care
	 */
	_750011000000106 ("750011000000106", "Integrated Care and Support Plan - Funded Nursing Care"),
	
	/**
	 * Integrated Care and Support Plan - Local Authority Joint Funded
	 */
	_750021000000100 ("750021000000100", "Integrated Care and Support Plan - Local Authority Joint Funded"),
	
	/**
	 * Integrated Care and Support Plan - Local Authority Only Funded
	 */
	_750031000000103 ("750031000000103", "Integrated Care and Support Plan - Local Authority Only Funded"),
	
	/**
	 * Cancer multidisciplinary team report
	 */
	_762391000000107 ("762391000000107", "Cancer multidisciplinary team report"),
	
	/**
	 * NHS Continuing Healthcare
	 */
	_766751000000101 ("766751000000101", "NHS Continuing Healthcare"),
	
	/**
	 * NHS Continuing Healthcare Fast Track Pathway Tool
	 */
	_766761000000103 ("766761000000103", "NHS Continuing Healthcare Fast Track Pathway Tool"),
	
	/**
	 * NHS Continuing Healthcare Fast Track Pathway Tool Acceptance
	 */
	_766771000000105 ("766771000000105", "NHS Continuing Healthcare Fast Track Pathway Tool Acceptance"),
	
	/**
	 * NHS Continuing Healthcare Checklist Assessment
	 */
	_766781000000107 ("766781000000107", "NHS Continuing Healthcare Checklist Assessment"),
	
	/**
	 * NHS Continuing Healthcare Checklist Assessment Acceptance
	 */
	_766791000000109 ("766791000000109", "NHS Continuing Healthcare Checklist Assessment Acceptance"),
	
	/**
	 * NHS Continuing Healthcare Request for Assessment
	 */
	_766801000000108 ("766801000000108", "NHS Continuing Healthcare Request for Assessment"),
	
	/**
	 * NHS Continuing Healthcare Request for Assessment Acceptance
	 */
	_766811000000105 ("766811000000105", "NHS Continuing Healthcare Request for Assessment Acceptance"),
	
	/**
	 * NHS Continuing Healthcare Decision Support Tool
	 */
	_766821000000104 ("766821000000104", "NHS Continuing Healthcare Decision Support Tool"),
	
	/**
	 * NHS Continuing Healthcare Cancellation Notification
	 */
	_766831000000102 ("766831000000102", "NHS Continuing Healthcare Cancellation Notification"),
	
	/**
	 * NHS Continuing Healthcare Fast Track Pathway Tool Rejection
	 */
	_768131000000105 ("768131000000105", "NHS Continuing Healthcare Fast Track Pathway Tool Rejection"),
	
	/**
	 * NHS Continuing Healthcare Checklist Assessment Rejection
	 */
	_768141000000101 ("768141000000101", "NHS Continuing Healthcare Checklist Assessment Rejection"),
	
	/**
	 * NHS Continuing Healthcare Request for Assessment Rejection
	 */
	_768151000000103 ("768151000000103", "NHS Continuing Healthcare Request for Assessment Rejection"),
	
	/**
	 * Outpatient Clinic Attendance - Consultant Referral Letter
	 */
	_801531000000106 ("801531000000106", "Outpatient Clinic Attendance - Consultant Referral Letter"),
	
	/**
	 * Outpatient Clinic Attendance - GP Letter
	 */
	_801541000000102 ("801541000000102", "Outpatient Clinic Attendance - GP Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Patient Letter
	 */
	_801551000000104 ("801551000000104", "Outpatient Clinic Attendance - Patient Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Maxillofacial and Tertiary Letter
	 */
	_801561000000101 ("801561000000101", "Outpatient Clinic Attendance - Maxillofacial and Tertiary Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Results Letter
	 */
	_801571000000108 ("801571000000108", "Outpatient Clinic Attendance - Results Letter"),
	
	/**
	 * Outpatient Clinic Attendance - No Patient Copy Letter
	 */
	_801581000000105 ("801581000000105", "Outpatient Clinic Attendance - No Patient Copy Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Safeguarded Letter
	 */
	_801591000000107 ("801591000000107", "Outpatient Clinic Attendance - Safeguarded Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Administration Letter
	 */
	_801601000000101 ("801601000000101", "Outpatient Clinic Attendance - Administration Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Private Practice Letter
	 */
	_801611000000104 ("801611000000104", "Outpatient Clinic Attendance - Private Practice Letter"),
	
	/**
	 * NHS 111 Event
	 */
	_819541000000103 ("819541000000103", "NHS 111 Event"),
	
	/**
	 * NHS 111 Report
	 */
	_819551000000100 ("819551000000100", "NHS 111 Report"),
	
	/**
	 * NHS 111 Interim Ambulance Request
	 */
	_828791000000100 ("828791000000100", "NHS 111 Interim Ambulance Request"),
	
	/**
	 * NHS 111 Final Ambulance Request
	 */
	_828801000000101 ("828801000000101", "NHS 111 Final Ambulance Request"),
	
	/**
	 * Life insurance report
	 */
	_829581000000103 ("829581000000103", "Life insurance report"),
	
	/**
	 * GP Referral
	 */
	_846951000000108 ("846951000000108", "GP Referral"),
	
	/**
	 * GP Referral Document
	 */
	_846961000000106 ("846961000000106", "GP Referral Document"),
	
	/**
	 * GP Referral Accept Response
	 */
	_846971000000104 ("846971000000104", "GP Referral Accept Response"),
	
	/**
	 * GP Referral Reject Response
	 */
	_846981000000102 ("846981000000102", "GP Referral Reject Response"),
	
	/**
	 * Telehealth Referral
	 */
	_859741000000101 ("859741000000101", "Telehealth Referral"),
	
	/**
	 * Telehealth Clinician Response
	 */
	_859751000000103 ("859751000000103", "Telehealth Clinician Response"),
	
	/**
	 * End of Life Care Document
	 */
	_861411000000103 ("861411000000103", "End of Life Care Document"),
	
	/**
	 * End of Life Care Coordination Summary
	 */
	_861421000000109 ("861421000000109", "End of Life Care Coordination Summary"),
	
	/**
	 * Child Screening Report
	 */
	_866371000000107 ("866371000000107", "Child Screening Report"),
	
	/**
	 * Safeguarding report
	 */
	_909021000000108 ("909021000000108", "Safeguarding report"),
	
	/**
	 * Adult safeguarding report
	 */
	_909031000000105 ("909031000000105", "Adult safeguarding report"),
	
	/**
	 * Child safeguarding report
	 */
	_909041000000101 ("909041000000101", "Child safeguarding report"),
	
	/**
	 * Ambulatory ECG electrocardiography monitoring report
	 */
	_914851000000108 ("914851000000108", "Ambulatory ECG (electrocardiography) monitoring report"),
	
	/**
	 * ETT exercise tolerance test report
	 */
	_914861000000106 ("914861000000106", "ETT (exercise tolerance test) report"),
	
	/**
	 * Implanted device maintenance report
	 */
	_918401000000103 ("918401000000103", "Implanted device maintenance report"),
	
	/**
	 * Upper gastrointestinal tract endoscopy report
	 */
	_927071000000108 ("927071000000108", "Upper gastrointestinal tract endoscopy report"),
	
	/**
	 * Lower gastrointestinal tract endoscopy report
	 */
	_927081000000105 ("927081000000105", "Lower gastrointestinal tract endoscopy report"),
	
	/**
	 * BAAF C-infant report sent
	 */
	_958141000000101 ("958141000000101", "BAAF C-infant report sent"),
	
	/**
	 * Statement of special educational needs
	 */
	_Statementofspecialeducationalneeds ("134188003", "Statement of special educational needs"),
	
	/**
	 * Employment report - no examination
	 */
	_Employmentreportnoexamination ("184809009", "Employment report - no examination"),
	
	/**
	 * NHS employee report - no examination
	 */
	_NHSemployeereportnoexamination ("184811000", "NHS employee report - no examination"),
	
	/**
	 * Government department employee - report only
	 */
	_Governmentdepartmentemployeereportonly ("184813002", "Government department employee - report only"),
	
	/**
	 * Local authority employee - report
	 */
	_Localauthorityemployeereport ("184816005", "Local authority employee - report"),
	
	/**
	 * Special activities med report
	 */
	_Specialactivitiesmedreport ("184823006", "Special activities med report"),
	
	/**
	 * Insurance report - travel cancellation
	 */
	_Insurancereporttravelcancellation ("184850001", "Insurance report - travel cancellation"),
	
	/**
	 * Social security claim report
	 */
	_Socialsecurityclaimreport ("184873009", "Social security claim report"),
	
	/**
	 * Application to register disabled report
	 */
	_Applicationtoregisterdisabledreport ("184880006", "Application to register disabled report"),
	
	/**
	 * Disabled registration application - minimal report
	 */
	_Disabledregistrationapplicationminimalreport ("184881005", "Disabled registration application - minimal report"),
	
	/**
	 * Disabled registration application - full report
	 */
	_Disabledregistrationapplicationfullreport ("184882003", "Disabled registration application - full report"),
	
	/**
	 * Report from Health Visitor
	 */
	_ReportfromHealthVisitor ("185005006", "Report from Health Visitor"),
	
	/**
	 * Report to benefits agency
	 */
	_Reporttobenefitsagency ("185014001", "Report to benefits agency"),
	
	/**
	 * BAAF CD-adoptchild report
	 */
	_BAAFCDadoptchildreport ("185076008", "BAAF C/D-adopt:child report"),
	
	/**
	 * BAAF D-child 2yrs rep sent
	 */
	_BAAFDchild2yrsrepsent ("185078009", "BAAF D-child 2+yrs rep sent"),
	
	/**
	 * Power of attorney medical report
	 */
	_Powerofattorneymedicalreport ("186065003", "Power of attorney medical report"),
	
	/**
	 * Report
	 */
	_Report ("229059009", "Report"),
	
	/**
	 * Report for Coroner
	 */
	_ReportforCoroner ("270102001", "Report for Coroner"),
	
	/**
	 * Coroners postmortem report
	 */
	_Coronerspostmortemreport ("270107007", "Coroner's postmortem report"),
	
	/**
	 * Postmortem report
	 */
	_Postmortemreport ("270116006", "Postmortem report"),
	
	/**
	 * Life assurance - preliminary report
	 */
	_Lifeassurancepreliminaryreport ("270358003", "Life assurance - preliminary report"),
	
	/**
	 * Driving licence fitness report
	 */
	_Drivinglicencefitnessreport ("270370004", "Driving licence fitness report"),
	
	/**
	 * Disabled driver orange badge report
	 */
	_Disableddriverorangebadgereport ("270372007", "Disabled driver orange badge report"),
	
	/**
	 * Mobility allowance claim representative
	 */
	_Mobilityallowanceclaimrepresentative ("270389004", "Mobility allowance claim representative"),
	
	/**
	 * BAAF Adult 12 - adoption applicant report
	 */
	_BAAFAdult12adoptionapplicantreport ("270390008", "BAAF Adult 1/2 - adoption: applicant report"),
	
	/**
	 * Disabled drivers orange badge
	 */
	_Disableddriversorangebadge ("270840000", "Disabled drivers orange badge"),
	
	/**
	 * Solicitors report
	 */
	_Solicitorsreport ("271452006", "Solicitors report"),
	
	/**
	 * BAAF B12 - adoption birth history
	 */
	_BAAFB12adoptionbirthhistory ("271531001", "BAAF B1/2 - adoption: birth history"),
	
	/**
	 * DSS RMO RM2 report
	 */
	_DSSRMORM2report ("307881004", "DSS RMO RM2 report"),
	
	/**
	 * Report for Procurator-Fiscal
	 */
	_ReportforProcuratorFiscal ("308375000", "Report for Procurator-Fiscal"),
	
	/**
	 * Police surgeon postmortem report
	 */
	_Policesurgeonpostmortemreport ("308376004", "Police surgeon postmortem report"),
	
	/**
	 * Employment report
	 */
	_Employmentreport ("308575004", "Employment report"),
	
	/**
	 * DLA 370 report
	 */
	_DLA370report ("308584004", "DLA 370 report"),
	
	/**
	 * DS 1500 report
	 */
	_DS1500report ("308585003", "DS 1500 report"),
	
	/**
	 * BAAF C infant report
	 */
	_BAAFCinfantreport ("308588001", "BAAF C infant report"),
	
	/**
	 * BAAF D-child 2yrs report
	 */
	_BAAFDchild2yrsreport ("308589009", "BAAF D-child 2+yrs report"),
	
	/**
	 * War pensions report
	 */
	_Warpensionsreport ("308619006", "War pensions report"),
	
	/**
	 * RM10-DHSS DMO report
	 */
	_RM10DHSSDMOreport ("308621001", "RM10-DHSS DMO report"),
	
	/**
	 * Report to Drug Safety Research Unit
	 */
	_ReporttoDrugSafetyResearchUnit ("310601008", "Report to Drug Safety Research Unit"),
	
	/**
	 * Housing report
	 */
	_Housingreport ("310854009", "Housing report"),
	
	/**
	 * Adoption - birth history report
	 */
	_Adoptionbirthhistoryreport ("313277003", "Adoption - birth history report"),
	
	/**
	 * Clinical report
	 */
	_Clinicalreport ("371524004", "Clinical report"),
	
	/**
	 * Clinical procedure report
	 */
	_Clinicalprocedurereport ("371525003", "Clinical procedure report"),
	
	/**
	 * Operative report
	 */
	_Operativereport ("371526002", "Operative report"),
	
	/**
	 * Radiology report
	 */
	_Radiologyreport ("371527006", "Radiology report"),
	
	/**
	 * History and physical report
	 */
	_Historyandphysicalreport ("371529009", "History and physical report"),
	
	/**
	 * Clinical consultation report
	 */
	_Clinicalconsultationreport ("371530004", "Clinical consultation report"),
	
	/**
	 * Encounter report
	 */
	_Encounterreport ("371531000", "Encounter report"),
	
	/**
	 * Progress report
	 */
	_Progressreport ("371532007", "Progress report"),
	
	/**
	 * Summary report
	 */
	_Summaryreport ("371534008", "Summary report"),
	
	/**
	 * Transfer summary report
	 */
	_Transfersummaryreport ("371535009", "Transfer summary report"),
	
	/**
	 * Flow sheet report
	 */
	_Flowsheetreport ("371536005", "Flow sheet report"),
	
	/**
	 * Consent report
	 */
	_Consentreport ("371537001", "Consent report"),
	
	/**
	 * Advance directive report
	 */
	_Advancedirectivereport ("371538006", "Advance directive report"),
	
	/**
	 * Conference report
	 */
	_Conferencereport ("371539003", "Conference report"),
	
	/**
	 * Supervising physician report
	 */
	_Supervisingphysicianreport ("371540001", "Supervising physician report"),
	
	/**
	 * Provider comment report
	 */
	_Providercommentreport ("371541002", "Provider comment report"),
	
	/**
	 * Admission history and physical report
	 */
	_Admissionhistoryandphysicalreport ("371542009", "Admission history and physical report"),
	
	/**
	 * Comprehensive history and physical report
	 */
	_Comprehensivehistoryandphysicalreport ("371543004", "Comprehensive history and physical report"),
	
	/**
	 * Targeted history and physical report
	 */
	_Targetedhistoryandphysicalreport ("371544005", "Targeted history and physical report"),
	
	/**
	 * Confirmatory consultation report
	 */
	_Confirmatoryconsultationreport ("371545006", "Confirmatory consultation report"),
	
	/**
	 * Educational visit report
	 */
	_Educationalvisitreport ("371546007", "Educational visit report"),
	
	/**
	 * Discharge summary
	 */
	_Dischargesummary ("373942005", "Discharge summary"),
	
	/**
	 * Adult echocardiography procedure report
	 */
	_Adultechocardiographyprocedurereport ("399345000", "Adult echocardiography procedure report"),
	
	/**
	 * Statement of needs
	 */
	_Statementofneeds ("404187006", "Statement of needs"),
	
	/**
	 * Summary of needs
	 */
	_Summaryofneeds ("406550009", "Summary of needs"),
	
	/**
	 * Written operative report
	 */
	_Writtenoperativereport ("416700005", "Written operative report"),
	
	/**
	 * Dictated operative report
	 */
	_Dictatedoperativereport ("417185000", "Dictated operative report"),
	
	/**
	 * Summary clinical document
	 */
	_Summaryclinicaldocument ("422735006", "Summary clinical document"),
	
	/**
	 * Clinical document
	 */
	_Clinicaldocument ("423876004", "Clinical document"),
	
	/**
	 * Hospital inpatient progress report
	 */
	_Hospitalinpatientprogressreport ("444754002", "Hospital inpatient progress report"),
	
	/**
	 * Multidisciplinary care conference report
	 */
	_Multidisciplinarycareconferencereport ("444804000", "Multidisciplinary care conference report"),
	
	/**
	 * Hospital outpatient progress report
	 */
	_Hospitaloutpatientprogressreport ("445042000", "Hospital outpatient progress report"),
	
	/**
	 * Provider orders for life-sustaining treatment
	 */
	_Providerordersforlifesustainingtreatment ("697978002", "Provider orders for life-sustaining treatment"),
	
	/**
	 * FocusActOrEvent concept
	 */
	_FocusActOrEventconcept ("163251000000107", "FocusActOrEvent concept"),
	
	/**
	 * Primary and Community Care Event
	 */
	_PrimaryandCommunityCareEvent ("163261000000105", "Primary and Community Care Event"),
	
	/**
	 * General Practitioner Event
	 */
	_GeneralPractitionerEvent ("163271000000103", "General Practitioner Event"),
	
	/**
	 * Practice Nurse Event
	 */
	_PracticeNurseEvent ("163281000000101", "Practice Nurse Event"),
	
	/**
	 * District Nurse Event
	 */
	_DistrictNurseEvent ("163291000000104", "District Nurse Event"),
	
	/**
	 * Physiotherapist Event
	 */
	_PhysiotherapistEvent ("163301000000100", "Physiotherapist Event"),
	
	/**
	 * Occupational Therapist Event
	 */
	_OccupationalTherapistEvent ("163311000000103", "Occupational Therapist Event"),
	
	/**
	 * Community Midwife Event
	 */
	_CommunityMidwifeEvent ("163321000000109", "Community Midwife Event"),
	
	/**
	 * Health Visitor Event
	 */
	_HealthVisitorEvent ("163331000000106", "Health Visitor Event"),
	
	/**
	 * Chiropodist Event
	 */
	_ChiropodistEvent ("163341000000102", "Chiropodist Event"),
	
	/**
	 * Dietician Event
	 */
	_DieticianEvent ("163351000000104", "Dietician Event"),
	
	/**
	 * Out of Hours Event
	 */
	_OutofHoursEvent ("163361000000101", "Out of Hours Event"),
	
	/**
	 * Secondary Care Event
	 */
	_SecondaryCareEvent ("163371000000108", "Secondary Care Event"),
	
	/**
	 * Outpatient Clinic Attendance
	 */
	_OutpatientClinicAttendance ("163381000000105", "Outpatient Clinic Attendance"),
	
	/**
	 * Discharge from Inpatient Care
	 */
	_DischargefromInpatientCare ("163391000000107", "Discharge from Inpatient Care"),
	
	/**
	 * Discharge from Day Case Care
	 */
	_DischargefromDayCaseCare ("163401000000105", "Discharge from Day Case Care"),
	
	/**
	 * Ward Attendance
	 */
	_WardAttendance ("163411000000107", "Ward Attendance"),
	
	/**
	 * Single Assessment Process Event
	 */
	_SingleAssessmentProcessEvent ("163421000000101", "Single Assessment Process Event"),
	
	/**
	 * Single Assessment Process Contact Assessment
	 */
	_SingleAssessmentProcessContactAssessment ("163431000000104", "Single Assessment Process Contact Assessment"),
	
	/**
	 * Single Assessment Process Overview Assessment
	 */
	_SingleAssessmentProcessOverviewAssessment ("163441000000108", "Single Assessment Process Overview Assessment"),
	
	/**
	 * Mental Health Event
	 */
	_MentalHealthEvent ("163451000000106", "Mental Health Event"),
	
	/**
	 * Entry to Mental Health Services
	 */
	_EntrytoMentalHealthServices ("163461000000109", "Entry to Mental Health Services"),
	
	/**
	 * Care Programme Approach Summary
	 */
	_CareProgrammeApproachSummary ("163471000000102", "Care Programme Approach Summary"),
	
	/**
	 * Discharge from Mental Health Services
	 */
	_DischargefromMentalHealthServices ("163481000000100", "Discharge from Mental Health Services"),
	
	/**
	 * Medication Management Event
	 */
	_MedicationManagementEvent ("163491000000103", "Medication Management Event"),
	
	/**
	 * Prescription
	 */
	_Prescription ("163501000000109", "Prescription"),
	
	/**
	 * Prescription Cancel Request
	 */
	_PrescriptionCancelRequest ("163511000000106", "Prescription Cancel Request"),
	
	/**
	 * Prescription Cancel Response
	 */
	_PrescriptionCancelResponse ("163521000000100", "Prescription Cancel Response"),
	
	/**
	 * Protocol Supply Medication
	 */
	_ProtocolSupplyMedication ("163531000000103", "Protocol Supply Medication"),
	
	/**
	 * Dispensed Medication
	 */
	_DispensedMedication ("163541000000107", "Dispensed Medication"),
	
	/**
	 * Personally Administered Medication
	 */
	_PersonallyAdministeredMedication ("163551000000105", "Personally Administered Medication"),
	
	/**
	 * Diagnostic Imaging Event
	 */
	_DiagnosticImagingEvent ("163561000000108", "Diagnostic Imaging Event"),
	
	/**
	 * Diagnostic Imaging Encounter
	 */
	_DiagnosticImagingEncounter ("163571000000101", "Diagnostic Imaging Encounter"),
	
	/**
	 * Diagnostic Imaging Report
	 */
	_DiagnosticImagingReport ("163581000000104", "Diagnostic Imaging Report"),
	
	/**
	 * Care Programme Approach summary
	 */
	_CareProgrammeApproachsummary ("163751000000100", "Care Programme Approach summary"),
	
	/**
	 * Diagnostic Imaging Request Event - FocusActOrEvent
	 */
	_DiagnosticImagingRequestEventFocusActOrEvent ("181221000000100", "Diagnostic Imaging Request Event - FocusActOrEvent"),
	
	/**
	 * Emergency Department Event
	 */
	_EmergencyDepartmentEvent ("185291000000100", "Emergency Department Event"),
	
	/**
	 * Minor Injuries Department Event
	 */
	_MinorInjuriesDepartmentEvent ("185301000000101", "Minor Injuries Department Event"),
	
	/**
	 * Walk-in Centre Event
	 */
	_WalkinCentreEvent ("185311000000104", "Walk-in Centre Event"),
	
	/**
	 * Pathology Event
	 */
	_PathologyEvent ("193821000000101", "Pathology Event"),
	
	/**
	 * Pathology Request Event
	 */
	_PathologyRequestEvent ("193831000000104", "Pathology Request Event"),
	
	/**
	 * Pathology Report Event
	 */
	_PathologyReportEvent ("193841000000108", "Pathology Report Event"),
	
	/**
	 * General Practice Initial Summary
	 */
	_GeneralPracticeInitialSummary ("196971000000103", "General Practice Initial Summary"),
	
	/**
	 * General Practice Summary
	 */
	_GeneralPracticeSummary ("196981000000101", "General Practice Summary"),
	
	/**
	 * NHS Direct Event
	 */
	_NHSDirectEvent ("197391000000101", "NHS Direct Event"),
	
	/**
	 * HealthSpace summary
	 */
	_HealthSpacesummary ("283281000000100", "HealthSpace summary"),
	
	/**
	 * Nullification Document
	 */
	_NullificationDocument ("285091000000108", "Nullification Document"),
	
	/**
	 * Refusal To Seal
	 */
	_RefusalToSeal ("285101000000100", "Refusal To Seal"),
	
	/**
	 * Statement of educational needs
	 */
	_Statementofeducationalneeds ("304391000000103", "Statement of educational needs"),
	
	/**
	 * Sealed Envelope Event
	 */
	_SealedEnvelopeEvent ("305751000000109", "Sealed Envelope Event"),
	
	/**
	 * Unseal Report
	 */
	_UnsealReport ("305761000000107", "Unseal Report"),
	
	/**
	 * Seal Report
	 */
	_SealReport ("305771000000100", "Seal Report"),
	
	/**
	 * Urgent Care Summary Report
	 */
	_UrgentCareSummaryReport ("312341000000104", "Urgent Care Summary Report"),
	
	/**
	 * NHS Direct Summary Report
	 */
	_NHSDirectSummaryReport ("312421000000103", "NHS Direct Summary Report"),
	
	/**
	 * Ambulance Service Patient Summary Report
	 */
	_AmbulanceServicePatientSummaryReport ("312711000000101", "Ambulance Service Patient Summary Report"),
	
	/**
	 * Mental Health Act Assessment Outside Specialist Services
	 */
	_MentalHealthActAssessmentOutsideSpecialistServices ("312991000000108", "Mental Health Act Assessment Outside Specialist Services"),
	
	/**
	 * Update to Mental Health Act Assessment Non-Specialist Services
	 */
	_UpdatetoMentalHealthActAssessmentNonSpecialistServices ("313001000000107", "Update to Mental Health Act Assessment Non-Specialist Services"),
	
	/**
	 * Entry to Mental Health and Substance Misuse Specialist Services
	 */
	_EntrytoMentalHealthandSubstanceMisuseSpecialistServices ("313011000000109", "Entry to Mental Health and Substance Misuse Specialist Services"),
	
	/**
	 * Update to Mental Health and Substance Misuse Specialist Services
	 */
	_UpdatetoMentalHealthandSubstanceMisuseSpecialistServices ("313021000000103", "Update to Mental Health and Substance Misuse Specialist Services"),
	
	/**
	 * Exit from Mental Health and Substance Misuse Specialist Services
	 */
	_ExitfromMentalHealthandSubstanceMisuseSpecialistServices ("313031000000101", "Exit from Mental Health and Substance Misuse Specialist Services"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Care Programme Approach Review Summary
	 */
	_MentalHealthandSubstanceMisuseSpecialistServicesCareProgrammeApproachReviewSummary ("313041000000105", "Mental Health and Substance Misuse Specialist Services Care Programme Approach Review Summary"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Models of Care Review Summary
	 */
	_MentalHealthandSubstanceMisuseSpecialistServicesModelsofCareReviewSummary ("313051000000108", "Mental Health and Substance Misuse Specialist Services Models of Care Review Summary"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Unspecified Care Review Summary
	 */
	_MentalHealthandSubstanceMisuseSpecialistServicesUnspecifiedCareReviewSummary ("313061000000106", "Mental Health and Substance Misuse Specialist Services Unspecified Care Review Summary"),
	
	/**
	 * Admission to Inpatient Care
	 */
	_AdmissiontoInpatientCare ("313071000000104", "Admission to Inpatient Care"),
	
	/**
	 * Assessment Event
	 */
	_AssessmentEvent ("313081000000102", "Assessment Event"),
	
	/**
	 * Contact Assessment
	 */
	_ContactAssessment ("313091000000100", "Contact Assessment"),
	
	/**
	 * Overview Assessment
	 */
	_OverviewAssessment ("313101000000108", "Overview Assessment"),
	
	/**
	 * Integrated Care and Support Plan
	 */
	_IntegratedCareandSupportPlan ("313111000000105", "Integrated Care and Support Plan"),
	
	/**
	 * Specialist Assessment Outcome
	 */
	_SpecialistAssessmentOutcome ("313121000000104", "Specialist Assessment Outcome"),
	
	/**
	 * Mental Health and Substance Misuse Specialist Services Event
	 */
	_MentalHealthandSubstanceMisuseSpecialistServicesEvent ("313141000000106", "Mental Health and Substance Misuse Specialist Services Event"),
	
	/**
	 * Biographical Information
	 */
	_BiographicalInformation ("320151000000108", "Biographical Information"),
	
	/**
	 * Withdrawal Notification
	 */
	_WithdrawalNotification ("339561000000108", "Withdrawal Notification"),
	
	/**
	 * Telehealth Event
	 */
	_TelehealthEvent ("384101000000107", "Telehealth Event"),
	
	/**
	 * Personal Health Monitoring Report
	 */
	_PersonalHealthMonitoringReport ("384111000000109", "Personal Health Monitoring Report"),
	
	/**
	 * Out of Tolerance Notification
	 */
	_OutofToleranceNotification ("384121000000103", "Out of Tolerance Notification"),
	
	/**
	 * British Association for Adoption and Fostering B2 birth history report
	 */
	_BritishAssociationforAdoptionandFosteringB2birthhistoryreport ("395641000000101", "British Association for Adoption and Fostering B2 birth history report"),
	
	/**
	 * BAAF B1 obstetric report
	 */
	_BAAFB1obstetricreport ("461421000000103", "BAAF B1 obstetric report"),
	
	/**
	 * Personal Health Monitoring Report - Action Required
	 */
	_PersonalHealthMonitoringReportActionRequired ("491811000000109", "Personal Health Monitoring Report - Action Required"),
	
	/**
	 * Personal Health Monitoring Report - For Information Only
	 */
	_PersonalHealthMonitoringReportForInformationOnly ("491821000000103", "Personal Health Monitoring Report - For Information Only"),
	
	/**
	 * Personal Health Monitoring Report - Routine Scheduled
	 */
	_PersonalHealthMonitoringReportRoutineScheduled ("491831000000101", "Personal Health Monitoring Report - Routine Scheduled"),
	
	/**
	 * Hospital Discharge Notification to Social Care
	 */
	_HospitalDischargeNotificationtoSocialCare ("715591000000108", "Hospital Discharge Notification to Social Care"),
	
	/**
	 * Assessment Notification Under Section 2 of the Community Care Delayed Discharges etc Act 2003
	 */
	_AssessmentNotificationUnderSection2oftheCommunityCareDelayedDischargesetcAct2003 ("715601000000102", "Assessment Notification Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Assessment Notification Accept Response Under Section 2 of the Community Care Delayed Discharges etc Act 2003
	 */
	_AssessmentNotificationAcceptResponseUnderSection2oftheCommunityCareDelayedDischargesetcAct2003 ("715611000000100", "Assessment Notification Accept Response Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Assessment Notification Reject Response Under Section 2 of the Community Care Delayed Discharges etc Act 2003
	 */
	_AssessmentNotificationRejectResponseUnderSection2oftheCommunityCareDelayedDischargesetcAct2003 ("715621000000106", "Assessment Notification Reject Response Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Discharge Notification Under Section 5 of the Community Care Delayed Discharges etc Act 2003
	 */
	_DischargeNotificationUnderSection5oftheCommunityCareDelayedDischargesetcAct2003 ("715631000000108", "Discharge Notification Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Discharge Notification Agreement Under Section 5 of the Community Care Delayed Discharges etc Act 2003
	 */
	_DischargeNotificationAgreementUnderSection5oftheCommunityCareDelayedDischargesetcAct2003 ("715641000000104", "Discharge Notification Agreement Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Discharge Notification Disagreement Under Section 5 of the Community Care Delayed Discharges etc Act 2003
	 */
	_DischargeNotificationDisagreementUnderSection5oftheCommunityCareDelayedDischargesetcAct2003 ("715651000000101", "Discharge Notification Disagreement Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Cancellation Notification Under Section 3 of the Community Care Delayed Discharges etc Act 2003
	 */
	_CancellationNotificationUnderSection3oftheCommunityCareDelayedDischargesetcAct2003 ("715661000000103", "Cancellation Notification Under Section 3 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Cancellation of Section 5 Notification Under Section 3 of the Community Care Delayed Discharges etc Act 2003
	 */
	_CancellationofSection5NotificationUnderSection3oftheCommunityCareDelayedDischargesetcAct2003 ("715681000000107", "Cancellation of Section 5 Notification Under Section 3 of the Community Care (Delayed Discharges etc) Act 2003"),
	
	/**
	 * Assessment Notification Under Section 2 of the Community Care Delayed Discharges etc Act 2003 Document
	 */
	_AssessmentNotificationUnderSection2oftheCommunityCareDelayedDischargesetcAct2003Document ("715721000000100", "Assessment Notification Under Section 2 of the Community Care (Delayed Discharges etc) Act 2003 Document"),
	
	/**
	 * Discharge Notification Under Section 5 of the Community Care Delayed Discharges etc Act 2003 Document
	 */
	_DischargeNotificationUnderSection5oftheCommunityCareDelayedDischargesetcAct2003Document ("715731000000103", "Discharge Notification Under Section 5 of the Community Care (Delayed Discharges etc) Act 2003 Document"),
	
	/**
	 * Diagnostic imaging procedure report
	 */
	_Diagnosticimagingprocedurereport ("716061000000101", "Diagnostic imaging procedure report"),
	
	/**
	 * Diagnostic imaging summary report
	 */
	_Diagnosticimagingsummaryreport ("716371000000105", "Diagnostic imaging summary report"),
	
	/**
	 * Diagnostic imaging report
	 */
	_Diagnosticimagingreport ("717321000000108", "Diagnostic imaging report"),
	
	/**
	 * Integrated Care and Support Plan - Continuing Health Care Funded
	 */
	_IntegratedCareandSupportPlanContinuingHealthCareFunded ("750001000000109", "Integrated Care and Support Plan - Continuing Health Care Funded"),
	
	/**
	 * Integrated Care and Support Plan - Funded Nursing Care
	 */
	_IntegratedCareandSupportPlanFundedNursingCare ("750011000000106", "Integrated Care and Support Plan - Funded Nursing Care"),
	
	/**
	 * Integrated Care and Support Plan - Local Authority Joint Funded
	 */
	_IntegratedCareandSupportPlanLocalAuthorityJointFunded ("750021000000100", "Integrated Care and Support Plan - Local Authority Joint Funded"),
	
	/**
	 * Integrated Care and Support Plan - Local Authority Only Funded
	 */
	_IntegratedCareandSupportPlanLocalAuthorityOnlyFunded ("750031000000103", "Integrated Care and Support Plan - Local Authority Only Funded"),
	
	/**
	 * Cancer multidisciplinary team report
	 */
	_Cancermultidisciplinaryteamreport ("762391000000107", "Cancer multidisciplinary team report"),
	
	/**
	 * NHS Continuing Healthcare
	 */
	_NHSContinuingHealthcare ("766751000000101", "NHS Continuing Healthcare"),
	
	/**
	 * NHS Continuing Healthcare Fast Track Pathway Tool
	 */
	_NHSContinuingHealthcareFastTrackPathwayTool ("766761000000103", "NHS Continuing Healthcare Fast Track Pathway Tool"),
	
	/**
	 * NHS Continuing Healthcare Fast Track Pathway Tool Acceptance
	 */
	_NHSContinuingHealthcareFastTrackPathwayToolAcceptance ("766771000000105", "NHS Continuing Healthcare Fast Track Pathway Tool Acceptance"),
	
	/**
	 * NHS Continuing Healthcare Checklist Assessment
	 */
	_NHSContinuingHealthcareChecklistAssessment ("766781000000107", "NHS Continuing Healthcare Checklist Assessment"),
	
	/**
	 * NHS Continuing Healthcare Checklist Assessment Acceptance
	 */
	_NHSContinuingHealthcareChecklistAssessmentAcceptance ("766791000000109", "NHS Continuing Healthcare Checklist Assessment Acceptance"),
	
	/**
	 * NHS Continuing Healthcare Request for Assessment
	 */
	_NHSContinuingHealthcareRequestforAssessment ("766801000000108", "NHS Continuing Healthcare Request for Assessment"),
	
	/**
	 * NHS Continuing Healthcare Request for Assessment Acceptance
	 */
	_NHSContinuingHealthcareRequestforAssessmentAcceptance ("766811000000105", "NHS Continuing Healthcare Request for Assessment Acceptance"),
	
	/**
	 * NHS Continuing Healthcare Decision Support Tool
	 */
	_NHSContinuingHealthcareDecisionSupportTool ("766821000000104", "NHS Continuing Healthcare Decision Support Tool"),
	
	/**
	 * NHS Continuing Healthcare Cancellation Notification
	 */
	_NHSContinuingHealthcareCancellationNotification ("766831000000102", "NHS Continuing Healthcare Cancellation Notification"),
	
	/**
	 * NHS Continuing Healthcare Fast Track Pathway Tool Rejection
	 */
	_NHSContinuingHealthcareFastTrackPathwayToolRejection ("768131000000105", "NHS Continuing Healthcare Fast Track Pathway Tool Rejection"),
	
	/**
	 * NHS Continuing Healthcare Checklist Assessment Rejection
	 */
	_NHSContinuingHealthcareChecklistAssessmentRejection ("768141000000101", "NHS Continuing Healthcare Checklist Assessment Rejection"),
	
	/**
	 * NHS Continuing Healthcare Request for Assessment Rejection
	 */
	_NHSContinuingHealthcareRequestforAssessmentRejection ("768151000000103", "NHS Continuing Healthcare Request for Assessment Rejection"),
	
	/**
	 * Outpatient Clinic Attendance - Consultant Referral Letter
	 */
	_OutpatientClinicAttendanceConsultantReferralLetter ("801531000000106", "Outpatient Clinic Attendance - Consultant Referral Letter"),
	
	/**
	 * Outpatient Clinic Attendance - GP Letter
	 */
	_OutpatientClinicAttendanceGPLetter ("801541000000102", "Outpatient Clinic Attendance - GP Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Patient Letter
	 */
	_OutpatientClinicAttendancePatientLetter ("801551000000104", "Outpatient Clinic Attendance - Patient Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Maxillofacial and Tertiary Letter
	 */
	_OutpatientClinicAttendanceMaxillofacialandTertiaryLetter ("801561000000101", "Outpatient Clinic Attendance - Maxillofacial and Tertiary Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Results Letter
	 */
	_OutpatientClinicAttendanceResultsLetter ("801571000000108", "Outpatient Clinic Attendance - Results Letter"),
	
	/**
	 * Outpatient Clinic Attendance - No Patient Copy Letter
	 */
	_OutpatientClinicAttendanceNoPatientCopyLetter ("801581000000105", "Outpatient Clinic Attendance - No Patient Copy Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Safeguarded Letter
	 */
	_OutpatientClinicAttendanceSafeguardedLetter ("801591000000107", "Outpatient Clinic Attendance - Safeguarded Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Administration Letter
	 */
	_OutpatientClinicAttendanceAdministrationLetter ("801601000000101", "Outpatient Clinic Attendance - Administration Letter"),
	
	/**
	 * Outpatient Clinic Attendance - Private Practice Letter
	 */
	_OutpatientClinicAttendancePrivatePracticeLetter ("801611000000104", "Outpatient Clinic Attendance - Private Practice Letter"),
	
	/**
	 * NHS 111 Event
	 */
	_NHS111Event ("819541000000103", "NHS 111 Event"),
	
	/**
	 * NHS 111 Report
	 */
	_NHS111Report ("819551000000100", "NHS 111 Report"),
	
	/**
	 * NHS 111 Interim Ambulance Request
	 */
	_NHS111InterimAmbulanceRequest ("828791000000100", "NHS 111 Interim Ambulance Request"),
	
	/**
	 * NHS 111 Final Ambulance Request
	 */
	_NHS111FinalAmbulanceRequest ("828801000000101", "NHS 111 Final Ambulance Request"),
	
	/**
	 * Life insurance report
	 */
	_Lifeinsurancereport ("829581000000103", "Life insurance report"),
	
	/**
	 * GP Referral
	 */
	_GPReferral ("846951000000108", "GP Referral"),
	
	/**
	 * GP Referral Document
	 */
	_GPReferralDocument ("846961000000106", "GP Referral Document"),
	
	/**
	 * GP Referral Accept Response
	 */
	_GPReferralAcceptResponse ("846971000000104", "GP Referral Accept Response"),
	
	/**
	 * GP Referral Reject Response
	 */
	_GPReferralRejectResponse ("846981000000102", "GP Referral Reject Response"),
	
	/**
	 * Telehealth Referral
	 */
	_TelehealthReferral ("859741000000101", "Telehealth Referral"),
	
	/**
	 * Telehealth Clinician Response
	 */
	_TelehealthClinicianResponse ("859751000000103", "Telehealth Clinician Response"),
	
	/**
	 * End of Life Care Document
	 */
	_EndofLifeCareDocument ("861411000000103", "End of Life Care Document"),
	
	/**
	 * End of Life Care Coordination Summary
	 */
	_EndofLifeCareCoordinationSummary ("861421000000109", "End of Life Care Coordination Summary"),
	
	/**
	 * Child Screening Report
	 */
	_ChildScreeningReport ("866371000000107", "Child Screening Report"),
	
	/**
	 * Safeguarding report
	 */
	_Safeguardingreport ("909021000000108", "Safeguarding report"),
	
	/**
	 * Adult safeguarding report
	 */
	_Adultsafeguardingreport ("909031000000105", "Adult safeguarding report"),
	
	/**
	 * Child safeguarding report
	 */
	_Childsafeguardingreport ("909041000000101", "Child safeguarding report"),
	
	/**
	 * Ambulatory ECG electrocardiography monitoring report
	 */
	_AmbulatoryECGelectrocardiographymonitoringreport ("914851000000108", "Ambulatory ECG (electrocardiography) monitoring report"),
	
	/**
	 * ETT exercise tolerance test report
	 */
	_ETTexercisetolerancetestreport ("914861000000106", "ETT (exercise tolerance test) report"),
	
	/**
	 * Implanted device maintenance report
	 */
	_Implanteddevicemaintenancereport ("918401000000103", "Implanted device maintenance report"),
	
	/**
	 * Upper gastrointestinal tract endoscopy report
	 */
	_Uppergastrointestinaltractendoscopyreport ("927071000000108", "Upper gastrointestinal tract endoscopy report"),
	
	/**
	 * Lower gastrointestinal tract endoscopy report
	 */
	_Lowergastrointestinaltractendoscopyreport ("927081000000105", "Lower gastrointestinal tract endoscopy report"),
	
	/**
	 * BAAF C-infant report sent
	 */
	_BAAFCinfantreportsent ("958141000000101", "BAAF C-infant report sent"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	Documenttype(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static Documenttype getByCode(String code) {
		Documenttype[] vals = values();
		for (Documenttype val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(Documenttype other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	