/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the RoleClassAssociative vocabulary:
 * <ul>
 *   <li>_ACCESS : access</li>
 *   <li>_ASSIGNED : assigned entity</li>
 *   <li>_BIRTHPL : birthplace</li>
 *   <li>_CAREGIVER : caregiver</li>
 *   <li>_CASESBJ : case subject</li>
 *   <li>_CIT : citizen</li>
 *   <li>_CRINV : clinical research investigator</li>
 *   <li>_CRSPNSR : clinical research sponsor</li>
 *   <li>_COMPAR : commissioning party</li>
 *   <li>_COVPTY : covered party</li>
 *   <li>_DSDLOC : dedicated service delivery location</li>
 *   <li>_ECON : emergency contact</li>
 *   <li>_EMP : employee</li>
 *   <li>_EXPR : exposed entity</li>
 *   <li>_GUAR : guarantor</li>
 *   <li>_GUARD : guardian</li>
 *   <li>_HLTHCHRT : health chart</li>
 *   <li>_HLD : held entity</li>
 *   <li>_IDENT : identified entity</li>
 *   <li>_ISDLOC : incidental service delivery location</li>
 *   <li>_INVSBJ : investigation subject</li>
 *   <li>_LIC : licensed entity</li>
 *   <li>_MNT : maintained entity</li>
 *   <li>_MANU : manufactured product</li>
 *   <li>_MIL : military person</li>
 *   <li>_NOK : next of kin</li>
 *   <li>_NOT : notary public</li>
 *   <li>_OWN : owned entity</li>
 *   <li>_PAT : patient</li>
 *   <li>_PAYEE : payee</li>
 *   <li>_PAYOR : invoice payor</li>
 *   <li>_POLHOLD : policy holder</li>
 *   <li>_PROV : healthcare provider</li>
 *   <li>_PRS : personal relationship</li>
 *   <li>_QUAL : qualified entity</li>
 *   <li>_RGPR : regulated product</li>
 *   <li>_RESBJ : research subject</li>
 *   <li>_RET : retailed material</li>
 *   <li>_SDLOC : service delivery location</li>
 *   <li>_SGNOFF : signing authority or officer</li>
 *   <li>_SPNSR : sponsor</li>
 *   <li>_STD : student</li>
 *   <li>_TERR : territory of authority</li>
 *   <li>_THER : therapeutic agent</li>
 *   <li>_UNDWRT : underwriter</li>
 *   <li>_WRTE : warranted product</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum RoleClassAssociative implements VocabularyEntry {
	
	
	/**
	 * access
	 */
	_ACCESS ("ACCESS", "access"),
	
	/**
	 * assigned entity
	 */
	_ASSIGNED ("ASSIGNED", "assigned entity"),
	
	/**
	 * birthplace
	 */
	_BIRTHPL ("BIRTHPL", "birthplace"),
	
	/**
	 * caregiver
	 */
	_CAREGIVER ("CAREGIVER", "caregiver"),
	
	/**
	 * case subject
	 */
	_CASESBJ ("CASESBJ", "case subject"),
	
	/**
	 * citizen
	 */
	_CIT ("CIT", "citizen"),
	
	/**
	 * clinical research investigator
	 */
	_CRINV ("CRINV", "clinical research investigator"),
	
	/**
	 * clinical research sponsor
	 */
	_CRSPNSR ("CRSPNSR", "clinical research sponsor"),
	
	/**
	 * commissioning party
	 */
	_COMPAR ("COMPAR", "commissioning party"),
	
	/**
	 * covered party
	 */
	_COVPTY ("COVPTY", "covered party"),
	
	/**
	 * dedicated service delivery location
	 */
	_DSDLOC ("DSDLOC", "dedicated service delivery location"),
	
	/**
	 * emergency contact
	 */
	_ECON ("ECON", "emergency contact"),
	
	/**
	 * employee
	 */
	_EMP ("EMP", "employee"),
	
	/**
	 * exposed entity
	 */
	_EXPR ("EXPR", "exposed entity"),
	
	/**
	 * guarantor
	 */
	_GUAR ("GUAR", "guarantor"),
	
	/**
	 * guardian
	 */
	_GUARD ("GUARD", "guardian"),
	
	/**
	 * health chart
	 */
	_HLTHCHRT ("HLTHCHRT", "health chart"),
	
	/**
	 * held entity
	 */
	_HLD ("HLD", "held entity"),
	
	/**
	 * identified entity
	 */
	_IDENT ("IDENT", "identified entity"),
	
	/**
	 * incidental service delivery location
	 */
	_ISDLOC ("ISDLOC", "incidental service delivery location"),
	
	/**
	 * investigation subject
	 */
	_INVSBJ ("INVSBJ", "investigation subject"),
	
	/**
	 * licensed entity
	 */
	_LIC ("LIC", "licensed entity"),
	
	/**
	 * maintained entity
	 */
	_MNT ("MNT", "maintained entity"),
	
	/**
	 * manufactured product
	 */
	_MANU ("MANU", "manufactured product"),
	
	/**
	 * military person
	 */
	_MIL ("MIL", "military person"),
	
	/**
	 * next of kin
	 */
	_NOK ("NOK", "next of kin"),
	
	/**
	 * notary public
	 */
	_NOT ("NOT", "notary public"),
	
	/**
	 * owned entity
	 */
	_OWN ("OWN", "owned entity"),
	
	/**
	 * patient
	 */
	_PAT ("PAT", "patient"),
	
	/**
	 * payee
	 */
	_PAYEE ("PAYEE", "payee"),
	
	/**
	 * invoice payor
	 */
	_PAYOR ("PAYOR", "invoice payor"),
	
	/**
	 * policy holder
	 */
	_POLHOLD ("POLHOLD", "policy holder"),
	
	/**
	 * healthcare provider
	 */
	_PROV ("PROV", "healthcare provider"),
	
	/**
	 * personal relationship
	 */
	_PRS ("PRS", "personal relationship"),
	
	/**
	 * qualified entity
	 */
	_QUAL ("QUAL", "qualified entity"),
	
	/**
	 * regulated product
	 */
	_RGPR ("RGPR", "regulated product"),
	
	/**
	 * research subject
	 */
	_RESBJ ("RESBJ", "research subject"),
	
	/**
	 * retailed material
	 */
	_RET ("RET", "retailed material"),
	
	/**
	 * service delivery location
	 */
	_SDLOC ("SDLOC", "service delivery location"),
	
	/**
	 * signing authority or officer
	 */
	_SGNOFF ("SGNOFF", "signing authority or officer"),
	
	/**
	 * sponsor
	 */
	_SPNSR ("SPNSR", "sponsor"),
	
	/**
	 * student
	 */
	_STD ("STD", "student"),
	
	/**
	 * territory of authority
	 */
	_TERR ("TERR", "territory of authority"),
	
	/**
	 * therapeutic agent
	 */
	_THER ("THER", "therapeutic agent"),
	
	/**
	 * underwriter
	 */
	_UNDWRT ("UNDWRT", "underwriter"),
	
	/**
	 * warranted product
	 */
	_WRTE ("WRTE", "warranted product"),
	
	/**
	 * access
	 */
	_access ("ACCESS", "access"),
	
	/**
	 * assigned entity
	 */
	_assignedentity ("ASSIGNED", "assigned entity"),
	
	/**
	 * birthplace
	 */
	_birthplace ("BIRTHPL", "birthplace"),
	
	/**
	 * caregiver
	 */
	_caregiver ("CAREGIVER", "caregiver"),
	
	/**
	 * case subject
	 */
	_casesubject ("CASESBJ", "case subject"),
	
	/**
	 * citizen
	 */
	_citizen ("CIT", "citizen"),
	
	/**
	 * clinical research investigator
	 */
	_clinicalresearchinvestigator ("CRINV", "clinical research investigator"),
	
	/**
	 * clinical research sponsor
	 */
	_clinicalresearchsponsor ("CRSPNSR", "clinical research sponsor"),
	
	/**
	 * commissioning party
	 */
	_commissioningparty ("COMPAR", "commissioning party"),
	
	/**
	 * covered party
	 */
	_coveredparty ("COVPTY", "covered party"),
	
	/**
	 * dedicated service delivery location
	 */
	_dedicatedservicedeliverylocation ("DSDLOC", "dedicated service delivery location"),
	
	/**
	 * emergency contact
	 */
	_emergencycontact ("ECON", "emergency contact"),
	
	/**
	 * employee
	 */
	_employee ("EMP", "employee"),
	
	/**
	 * exposed entity
	 */
	_exposedentity ("EXPR", "exposed entity"),
	
	/**
	 * guarantor
	 */
	_guarantor ("GUAR", "guarantor"),
	
	/**
	 * guardian
	 */
	_guardian ("GUARD", "guardian"),
	
	/**
	 * health chart
	 */
	_healthchart ("HLTHCHRT", "health chart"),
	
	/**
	 * held entity
	 */
	_heldentity ("HLD", "held entity"),
	
	/**
	 * identified entity
	 */
	_identifiedentity ("IDENT", "identified entity"),
	
	/**
	 * incidental service delivery location
	 */
	_incidentalservicedeliverylocation ("ISDLOC", "incidental service delivery location"),
	
	/**
	 * investigation subject
	 */
	_investigationsubject ("INVSBJ", "investigation subject"),
	
	/**
	 * licensed entity
	 */
	_licensedentity ("LIC", "licensed entity"),
	
	/**
	 * maintained entity
	 */
	_maintainedentity ("MNT", "maintained entity"),
	
	/**
	 * manufactured product
	 */
	_manufacturedproduct ("MANU", "manufactured product"),
	
	/**
	 * military person
	 */
	_militaryperson ("MIL", "military person"),
	
	/**
	 * next of kin
	 */
	_nextofkin ("NOK", "next of kin"),
	
	/**
	 * notary public
	 */
	_notarypublic ("NOT", "notary public"),
	
	/**
	 * owned entity
	 */
	_ownedentity ("OWN", "owned entity"),
	
	/**
	 * patient
	 */
	_patient ("PAT", "patient"),
	
	/**
	 * payee
	 */
	_payee ("PAYEE", "payee"),
	
	/**
	 * invoice payor
	 */
	_invoicepayor ("PAYOR", "invoice payor"),
	
	/**
	 * policy holder
	 */
	_policyholder ("POLHOLD", "policy holder"),
	
	/**
	 * healthcare provider
	 */
	_healthcareprovider ("PROV", "healthcare provider"),
	
	/**
	 * personal relationship
	 */
	_personalrelationship ("PRS", "personal relationship"),
	
	/**
	 * qualified entity
	 */
	_qualifiedentity ("QUAL", "qualified entity"),
	
	/**
	 * regulated product
	 */
	_regulatedproduct ("RGPR", "regulated product"),
	
	/**
	 * research subject
	 */
	_researchsubject ("RESBJ", "research subject"),
	
	/**
	 * retailed material
	 */
	_retailedmaterial ("RET", "retailed material"),
	
	/**
	 * service delivery location
	 */
	_servicedeliverylocation ("SDLOC", "service delivery location"),
	
	/**
	 * signing authority or officer
	 */
	_signingauthorityorofficer ("SGNOFF", "signing authority or officer"),
	
	/**
	 * sponsor
	 */
	_sponsor ("SPNSR", "sponsor"),
	
	/**
	 * student
	 */
	_student ("STD", "student"),
	
	/**
	 * territory of authority
	 */
	_territoryofauthority ("TERR", "territory of authority"),
	
	/**
	 * therapeutic agent
	 */
	_therapeuticagent ("THER", "therapeutic agent"),
	
	/**
	 * underwriter
	 */
	_underwriter ("UNDWRT", "underwriter"),
	
	/**
	 * warranted product
	 */
	_warrantedproduct ("WRTE", "warranted product"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.1.11.19313";

	RoleClassAssociative(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static RoleClassAssociative getByCode(String code) {
		RoleClassAssociative[] vals = values();
		for (RoleClassAssociative val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(RoleClassAssociative other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	