/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the Encounterdisposition vocabulary:
 * <ul>
 *   <li>_417005 : Hospital re-admission</li>
 *   <li>_1505002 : Hospital admission for isolation</li>
 *   <li>_1917008 : Patient discharge, deceased, medicolegal case</li>
 *   <li>_2252009 : Hospital admission, urgent, 48 hours</li>
 *   <li>_2876009 : Hospital admission, type unclassified, explain by report</li>
 *   <li>_3133002 : Patient discharge, deceased, autopsy</li>
 *   <li>_3780001 : Routine patient disposition, no follow-up planned</li>
 *   <li>_4563007 : Hospital admission, transfer from other hospital or health care facility</li>
 *   <li>_5161006 : Speciality clinic admission</li>
 *   <li>_8715000 : Hospital admission, elective</li>
 *   <li>_10161009 : Patient discharge, to home, with assistance</li>
 *   <li>_10378005 : Hospital admission, emergency, from emergency room, accidental injury</li>
 *   <li>_11545006 : Emergency room admission, dead on arrival DOA</li>
 *   <li>_12616003 : Patient discharge, escaped from custody</li>
 *   <li>_15584006 : Hospital admission, elective, with partial pre-admission work-up</li>
 *   <li>_18083007 : Hospital admission, emergency, indirect</li>
 *   <li>_19951005 : Hospital admission, emergency, from emergency room, medical nature</li>
 *   <li>_20541009 : Patient on pass</li>
 *   <li>_23473000 : Hospital admission, for research investigation</li>
 *   <li>_25986004 : Hospital admission, under police custody</li>
 *   <li>_32485007 : Hospital admission</li>
 *   <li>_34596002 : Patient discharge, elopement</li>
 *   <li>_36723004 : Hospital admission, pre-nursing home placement</li>
 *   <li>_40274000 : General outpatient clinic admission</li>
 *   <li>_41819004 : Patient discharge, deceased, donation of body</li>
 *   <li>_45702004 : Hospital admission, precertified by medical audit action</li>
 *   <li>_47031007 : Patient discharge, type unclassified, explain by report</li>
 *   <li>_47348005 : Hospital admission, mother, for observation, delivered outside of hospital</li>
 *   <li>_48183000 : Hospital admission, special</li>
 *   <li>_50331008 : Emergency room admission, followed by release</li>
 *   <li>_50699000 : Hospital admission, short-term</li>
 *   <li>_50849002 : Emergency room admission</li>
 *   <li>_50861005 : Patient discharge, to legal custody</li>
 *   <li>_51032003 : Hospital admission, donor for transplant organ</li>
 *   <li>_51501005 : Hospital admission, parent, for in-hospital child care</li>
 *   <li>_52748007 : Hospital admission, involuntary</li>
 *   <li>_55402005 : Hospital admission, for laboratory work-up, radiography, etc</li>
 *   <li>_58000006 : Patient discharge</li>
 *   <li>_60059000 : Hospital admission, infant, for observation, delivered outside of hospital</li>
 *   <li>_61342007 : Patient follow-up planned and scheduled</li>
 *   <li>_63551005 : Hospital admission, from remote area, by means of special transportation</li>
 *   <li>_65043002 : Hospital admission, short-term, day care</li>
 *   <li>_65537008 : Patient discharge, to home, routine</li>
 *   <li>_70755000 : Hospital admission, by legal authority commitment</li>
 *   <li>_71290004 : Hospital admission, limited to designated procedures</li>
 *   <li>_73607007 : Hospital admission, emergency, from emergency room</li>
 *   <li>_74857009 : Hospital admission, short-term, 24 hours</li>
 *   <li>_75004002 : Emergency room admission, died in emergency room</li>
 *   <li>_76464004 : Hospital admission, for observation</li>
 *   <li>_78680009 : Hospital admission, emergency, direct</li>
 *   <li>_79779006 : Patient discharge, deceased, no autopsy</li>
 *   <li>_81672003 : Hospital admission, elective, without pre-admission work-up</li>
 *   <li>_82942009 : Hospital admission, blood donor</li>
 *   <li>_86400002 : Patient discharge, to home, ambulatory</li>
 *   <li>_89334003 : Patient discharge, deceased, to anatomic board</li>
 *   <li>_91310009 : Patient follow-up to return when and if necessary</li>
 *   <li>_107726003 : Patient disposition</li>
 *   <li>_112689000 : Hospital admission, elective, with complete pre-admission work-up</li>
 *   <li>_112690009 : Hospital admission, boarder, for social reasons</li>
 *   <li>_135847007 : Involuntary admission</li>
 *   <li>_135848002 : Voluntary admission</li>
 *   <li>_177079006 : Admission review</li>
 *   <li>_183430001 : Holiday relief admission</li>
 *   <li>_183446009 : Admission to cardiac intensive care unit</li>
 *   <li>_183447000 : Admission to respiratory intensive care unit</li>
 *   <li>_183448005 : Admission to neurological intensive care unit</li>
 *   <li>_183449002 : Admission to metabolic intensive care unit</li>
 *   <li>_183450002 : Admission to burn unit</li>
 *   <li>_183452005 : Emergency hospital admission</li>
 *   <li>_183455007 : Admit psychiatric emergency</li>
 *   <li>_183457004 : Admit geriatric emergency</li>
 *   <li>_183458009 : Admit paediatric emergency</li>
 *   <li>_183459001 : Admit gynaecological emergency</li>
 *   <li>_183460006 : Admit obstetric emergency</li>
 *   <li>_183461005 : Admit orthopaedic emergency</li>
 *   <li>_183462003 : Admit ENT emergency</li>
 *   <li>_183463008 : Admit trauma emergency</li>
 *   <li>_183464002 : Admit ophthalmological emergency</li>
 *   <li>_183465001 : Admit rheumatology emergency</li>
 *   <li>_183466000 : Admit dermatology emergency</li>
 *   <li>_183467009 : Admit neurology emergency</li>
 *   <li>_183468004 : Admit urology emergency</li>
 *   <li>_183469007 : Admit radiotherapy emergency</li>
 *   <li>_183470008 : Admit haematology emergency</li>
 *   <li>_183471007 : Admit plastic surgery emergency</li>
 *   <li>_183472000 : Admit diabetic emergency</li>
 *   <li>_183473005 : Admit oral surgical emergency</li>
 *   <li>_183474004 : Admit psychogeriatric emergency</li>
 *   <li>_183475003 : Admit renal medicine emergency</li>
 *   <li>_183476002 : Admit neurosurgical emergency</li>
 *   <li>_183477006 : Admit cardiothoracic emergency</li>
 *   <li>_183478001 : Emergency admission, asthma</li>
 *   <li>_183481006 : Non-urgent hospital admission</li>
 *   <li>_183487005 : Non-urgent medical admission</li>
 *   <li>_183488000 : Non-urgent surgical admission</li>
 *   <li>_183489008 : Non-urgent psychiatric admission</li>
 *   <li>_183491000 : Non-urgent geriatric admission</li>
 *   <li>_183492007 : Non-urgent paediatric admission</li>
 *   <li>_183493002 : Non-urgent gynaecological admission</li>
 *   <li>_183494008 : Non-urgent obstetric admission</li>
 *   <li>_183495009 : Non-urgent orthopaedic admission</li>
 *   <li>_183496005 : Non-urgent ENT admission</li>
 *   <li>_183497001 : Non-urgent trauma admission</li>
 *   <li>_183498006 : Non-urgent ophthalmological admission</li>
 *   <li>_183499003 : Non-urgent rheumatology admission</li>
 *   <li>_183500007 : Non-urgent dermatology admission</li>
 *   <li>_183501006 : Non-urgent neurology admission</li>
 *   <li>_183502004 : Non-urgent urology admission</li>
 *   <li>_183503009 : Non-urgent radiotherapy admission</li>
 *   <li>_183504003 : Non-urgent haematology admission</li>
 *   <li>_183505002 : Non-urgent plastic surgery admission</li>
 *   <li>_183506001 : Non-urgent diabetic admission</li>
 *   <li>_183507005 : Non-urgent respiratory admission</li>
 *   <li>_183508000 : Non-urgent psychogeriatric admission</li>
 *   <li>_183509008 : Non-urgent renal medicine admission</li>
 *   <li>_183510003 : Non-urgent neurosurgical admission</li>
 *   <li>_183511004 : Non-urgent cardiothoracic admission</li>
 *   <li>_183512006 : Non-urgent oral surgical admission</li>
 *   <li>_183672007 : Delayed discharge to nursing home</li>
 *   <li>_183919006 : Urgent admission to hospice</li>
 *   <li>_183920000 : Routine admission to hospice</li>
 *   <li>_183921001 : Admission to hospice for respite</li>
 *   <li>_183955003 : Patient self-discharge</li>
 *   <li>_225928004 : Patient self-discharge against medical advice</li>
 *   <li>_281685003 : Admission for care</li>
 *   <li>_304566005 : Admission for treatment</li>
 *   <li>_304567001 : Admission for long-term care</li>
 *   <li>_304568006 : Admission for respite care</li>
 *   <li>_305056002 : Admission procedure</li>
 *   <li>_305224000 : Admission by health worker</li>
 *   <li>_305226003 : Admission by Accident and Emergency doctor</li>
 *   <li>_305227007 : Admission by anaesthetist</li>
 *   <li>_305228002 : Admission by clinical oncologist</li>
 *   <li>_305229005 : Admission by radiotherapist</li>
 *   <li>_305230000 : Admission by GP</li>
 *   <li>_305231001 : Admission by own GP</li>
 *   <li>_305232008 : Admission by GP partner</li>
 *   <li>_305239004 : Admission by family planning doctor</li>
 *   <li>_305240002 : Admission by intensive care specialist</li>
 *   <li>_305241003 : Admission by adult intensive care specialist</li>
 *   <li>_305242005 : Admission by paediatric intensive care specialist</li>
 *   <li>_305243000 : Admission by paediatrician</li>
 *   <li>_305244006 : Admission by community paediatrician</li>
 *   <li>_305245007 : Admission by neonatologist</li>
 *   <li>_305246008 : Admission by paediatric neurologist</li>
 *   <li>_305247004 : Admission by paediatric oncologist</li>
 *   <li>_305248009 : Admission by pain management specialist</li>
 *   <li>_305249001 : Admission by obstetrician  gynaecologist</li>
 *   <li>_305250001 : Admission by gynaecologist</li>
 *   <li>_305251002 : Admission by obstetrician</li>
 *   <li>_305252009 : Admission by occupational health physician</li>
 *   <li>_305253004 : Admission by pathologist</li>
 *   <li>_305254005 : Admission by blood transfusion doctor</li>
 *   <li>_305255006 : Admission by chemical pathologist</li>
 *   <li>_305256007 : Admission by general pathologist</li>
 *   <li>_305257003 : Admission by haematologist</li>
 *   <li>_305258008 : Admission by medical microbiologist</li>
 *   <li>_305259000 : Admission by neuropathologist</li>
 *   <li>_305260005 : Admission by physician</li>
 *   <li>_305261009 : Admission by clinical allergist</li>
 *   <li>_305262002 : Admission by cardiologist</li>
 *   <li>_305263007 : Admission by care of the elderly physician</li>
 *   <li>_305264001 : Admission by chest physician</li>
 *   <li>_305265000 : Admission by thoracic physician</li>
 *   <li>_305266004 : Admission by respiratory physician</li>
 *   <li>_305267008 : Admission by clinical immunologist</li>
 *   <li>_305268003 : Admission by clinical neurophysiologist</li>
 *   <li>_305269006 : Admission by clinical pharmacologist</li>
 *   <li>_305270007 : Admission by clinical physiologist</li>
 *   <li>_305271006 : Admission by dermatologist</li>
 *   <li>_305272004 : Admission by endocrinologist</li>
 *   <li>_305274003 : Admission by gastroenterologist</li>
 *   <li>_305275002 : Admission by general physician</li>
 *   <li>_305276001 : Admission by genitourinary medicine physician</li>
 *   <li>_305277005 : Admission by infectious diseases physician</li>
 *   <li>_305278000 : Admission by medical ophthalmologist</li>
 *   <li>_305279008 : Admission by nephrologist</li>
 *   <li>_305280006 : Admission by neurologist</li>
 *   <li>_305281005 : Admission by nuclear medicine physician</li>
 *   <li>_305282003 : Admission by rheumatologist</li>
 *   <li>_305283008 : Admission by rehabilitation physician</li>
 *   <li>_305284002 : Admission by palliative care physician</li>
 *   <li>_305285001 : Admission by psychiatrist</li>
 *   <li>_305286000 : Admission by child and adolescent psychiatrist</li>
 *   <li>_305287009 : Admission by forensic psychiatrist</li>
 *   <li>_305288004 : Admission by liaison psychiatrist</li>
 *   <li>_305289007 : Admission by psychogeriatrician</li>
 *   <li>_305290003 : Admission by psychiatrist for mental handicap</li>
 *   <li>_305291004 : Admission by rehabilitation psychiatrist</li>
 *   <li>_305292006 : Admission by radiologist</li>
 *   <li>_305293001 : Admission by surgeon</li>
 *   <li>_305294007 : Admission by breast surgeon</li>
 *   <li>_305295008 : Admission by cardiothoracic surgeon</li>
 *   <li>_305296009 : Admission by thoracic surgeon</li>
 *   <li>_305297000 : Admission by cardiac surgeon</li>
 *   <li>_305298005 : Admission by dental surgeon</li>
 *   <li>_305299002 : Admission by orthodontist</li>
 *   <li>_305300005 : Admission by paediatric dentist</li>
 *   <li>_305301009 : Admission by restorative dentist</li>
 *   <li>_305302002 : Admission by ear, nose and throat surgeon</li>
 *   <li>_305303007 : Admission by endocrine surgeon</li>
 *   <li>_305304001 : Admission by gastrointestinal surgeon</li>
 *   <li>_305305000 : Admission by general gastrointestinal surgeon</li>
 *   <li>_305306004 : Admission by upper gastrointestinal surgeon</li>
 *   <li>_305307008 : Admission by colorectal surgeon</li>
 *   <li>_305308003 : Admission by general surgeon</li>
 *   <li>_305309006 : Admission by hand surgeon</li>
 *   <li>_305310001 : Admission by hepatobiliary surgeon</li>
 *   <li>_305311002 : Admission by neurosurgeon</li>
 *   <li>_305312009 : Admission by ophthalmologist</li>
 *   <li>_305313004 : Admission by oral surgeon</li>
 *   <li>_305314005 : Admission by orthopaedic surgeon</li>
 *   <li>_305315006 : Admission by paediatric surgeon</li>
 *   <li>_305316007 : Admission by pancreatic surgeon</li>
 *   <li>_305317003 : Admission by plastic surgeon</li>
 *   <li>_305318008 : Admission by transplant surgeon</li>
 *   <li>_305319000 : Admission by trauma surgeon</li>
 *   <li>_305320006 : Admission by urologist</li>
 *   <li>_305321005 : Admission by vascular surgeon</li>
 *   <li>_305322003 : Admission by health care assistant</li>
 *   <li>_305323008 : Admission by midwife</li>
 *   <li>_305324002 : Admission by community-based midwife</li>
 *   <li>_305325001 : Admission by hospital-based midwife</li>
 *   <li>_305326000 : Admission by nurse</li>
 *   <li>_305327009 : Admission by agency nurse</li>
 *   <li>_305328004 : Admission by enrolled nurse</li>
 *   <li>_305329007 : Admission by general nurse</li>
 *   <li>_305330002 : Admission by mental health nurse</li>
 *   <li>_305331003 : Admission by nurse for the mentally handicapped</li>
 *   <li>_305332005 : Admission by sick childrens nurse</li>
 *   <li>_305333000 : Admission by nurse practitioner</li>
 *   <li>_305334006 : Admission by nursing auxiliary</li>
 *   <li>_305335007 : Admission to establishment</li>
 *   <li>_305336008 : Admission to hospice</li>
 *   <li>_305337004 : Admission to community hospital</li>
 *   <li>_305338009 : Admission to GP hospital</li>
 *   <li>_305339001 : Admission to private hospital</li>
 *   <li>_305340004 : Admission to long stay hospital</li>
 *   <li>_305341000 : Admission to tertiary referral hospital</li>
 *   <li>_305342007 : Admission to ward</li>
 *   <li>_305343002 : Admission to day ward</li>
 *   <li>_305344008 : Admission to day hospital</li>
 *   <li>_305345009 : Admission to psychiatric day hospital</li>
 *   <li>_305346005 : Admission to psychogeriatric day hospital</li>
 *   <li>_305347001 : Admission to elderly severely mentally ill day hospital</li>
 *   <li>_305348006 : Admission to care of the elderly day hospital</li>
 *   <li>_305349003 : Admission to department</li>
 *   <li>_305350003 : Admission to anaesthetic department</li>
 *   <li>_305351004 : Admission to intensive care unit</li>
 *   <li>_305352006 : Admission to adult intensive care unit</li>
 *   <li>_305353001 : Admission to paediatric intensive care unit</li>
 *   <li>_305354007 : Admission to medical department</li>
 *   <li>_305355008 : Admission to clinical allergy department</li>
 *   <li>_305356009 : Admission to audiology department</li>
 *   <li>_305357000 : Admission to cardiology department</li>
 *   <li>_305358005 : Admission to chest medicine department</li>
 *   <li>_305359002 : Admission to thoracic medicine department</li>
 *   <li>_305360007 : Admission to respiratory medicine department</li>
 *   <li>_305361006 : Admission to clinical immunology department</li>
 *   <li>_305362004 : Admission to clinical neurophysiology department</li>
 *   <li>_305363009 : Admission to clinical pharmacology department</li>
 *   <li>_305364003 : Admission to clinical physiology department</li>
 *   <li>_305365002 : Admission to dermatology department</li>
 *   <li>_305366001 : Admission to endocrinology department</li>
 *   <li>_305367005 : Admission to gastroenterology department</li>
 *   <li>_305368000 : Admission to general medical department</li>
 *   <li>_305369008 : Admission to genetics department</li>
 *   <li>_305370009 : Admission to clinical genetics department</li>
 *   <li>_305371008 : Admission to clinical cytogenetics department</li>
 *   <li>_305372001 : Admission to clinical molecular genetics department</li>
 *   <li>_305374000 : Admission to genitourinary medicine department</li>
 *   <li>_305375004 : Admission to care of the elderly department</li>
 *   <li>_305376003 : Admission to infectious diseases department</li>
 *   <li>_305377007 : Admission to medical ophthalmology department</li>
 *   <li>_305378002 : Admission to nephrology department</li>
 *   <li>_305379005 : Admission to neurology department</li>
 *   <li>_305380008 : Admission to nuclear medicine department</li>
 *   <li>_305381007 : Admission to palliative care department</li>
 *   <li>_305382000 : Admission to rehabilitation department</li>
 *   <li>_305383005 : Admission to rheumatology department</li>
 *   <li>_305384004 : Admission to obstetrics and gynaecology department</li>
 *   <li>_305385003 : Admission to gynaecology department</li>
 *   <li>_305386002 : Admission to obstetrics department</li>
 *   <li>_305387006 : Admission to paediatric department</li>
 *   <li>_305388001 : Admission to special care baby unit</li>
 *   <li>_305389009 : Admission to paediatric neurology department</li>
 *   <li>_305390000 : Admission to paediatric oncology department</li>
 *   <li>_305391001 : Admission to pain management department</li>
 *   <li>_305392008 : Admission to pathology department</li>
 *   <li>_305393003 : Admission to blood transfusion department</li>
 *   <li>_305394009 : Admission to chemical pathology department</li>
 *   <li>_305395005 : Admission to general pathology department</li>
 *   <li>_305396006 : Admission to haematology department</li>
 *   <li>_305397002 : Admission to medical microbiology department</li>
 *   <li>_305398007 : Admission to the mortuary</li>
 *   <li>_305399004 : Admission to neuropathology department</li>
 *   <li>_305400006 : Admission to psychiatry department</li>
 *   <li>_305401005 : Admission to child and adolescent psychiatry department</li>
 *   <li>_305402003 : Admission to forensic psychiatry department</li>
 *   <li>_305403008 : Admission to psychogeriatric department</li>
 *   <li>_305404002 : Admission to mental handicap psychiatry department</li>
 *   <li>_305405001 : Admission to rehabilitation psychiatry department</li>
 *   <li>_305406000 : Admission to radiology department</li>
 *   <li>_305407009 : Admission to occupational health department</li>
 *   <li>_305408004 : Admission to surgical department</li>
 *   <li>_305409007 : Admission to breast surgery department</li>
 *   <li>_305410002 : Admission to cardiothoracic surgery department</li>
 *   <li>_305411003 : Admission to thoracic surgery department</li>
 *   <li>_305412005 : Admission to cardiac surgery department</li>
 *   <li>_305413000 : Admission to dental surgery department</li>
 *   <li>_305414006 : Admission to orthodontics department</li>
 *   <li>_305415007 : Admission to paediatric dentistry department</li>
 *   <li>_305416008 : Admission to restorative dentistry department</li>
 *   <li>_305417004 : Admission to ear, nose and throat department</li>
 *   <li>_305418009 : Admission to endocrine surgery department</li>
 *   <li>_305419001 : Admission to gastrointestinal surgery department</li>
 *   <li>_305420007 : Admission to general gastrointestinal surgery department</li>
 *   <li>_305421006 : Admission to upper gastrointestinal surgery department</li>
 *   <li>_305422004 : Admission to colorectal surgery department</li>
 *   <li>_305423009 : Admission to general surgical department</li>
 *   <li>_305424003 : Admission to hepatobiliary surgical department</li>
 *   <li>_305425002 : Admission to neurosurgical department</li>
 *   <li>_305426001 : Admission to ophthalmology department</li>
 *   <li>_305427005 : Admission to oral surgery department</li>
 *   <li>_305428000 : Admission to orthopaedic department</li>
 *   <li>_305429008 : Admission to pancreatic surgery department</li>
 *   <li>_305430003 : Admission to paediatric surgical department</li>
 *   <li>_305431004 : Admission to plastic surgery department</li>
 *   <li>_305432006 : Admission to surgical transplant department</li>
 *   <li>_305433001 : Admission to trauma surgery department</li>
 *   <li>_305434007 : Admission to urology department</li>
 *   <li>_305435008 : Admission to vascular surgery department</li>
 *   <li>_306382007 : Discharge by health worker</li>
 *   <li>_306383002 : Discharge by counsellor</li>
 *   <li>_306385009 : Discharge by bereavement counsellor</li>
 *   <li>_306386005 : Discharge by genetic counsellor</li>
 *   <li>_306387001 : Discharge by marriage guidance counsellor</li>
 *   <li>_306388006 : Discharge by mental health counsellor</li>
 *   <li>_306390007 : Discharge by Accident and Emergency doctor</li>
 *   <li>_306391006 : Discharge by anaesthetist</li>
 *   <li>_306392004 : Discharge by clinical oncologist</li>
 *   <li>_306393009 : Discharge by radiotherapist</li>
 *   <li>_306394003 : Discharge by family planning doctor</li>
 *   <li>_306396001 : Discharge by own GP</li>
 *   <li>_306397005 : Discharge by partner of GP</li>
 *   <li>_306404005 : Discharge by intensive care specialist</li>
 *   <li>_306405006 : Discharge by adult intensive care specialist</li>
 *   <li>_306406007 : Discharge by paediatric intensive care specialist</li>
 *   <li>_306407003 : Discharge by pain management specialist</li>
 *   <li>_306408008 : Discharge by pathologist</li>
 *   <li>_306409000 : Discharge by blood transfusion doctor</li>
 *   <li>_306410005 : Discharge by chemical pathologist</li>
 *   <li>_306411009 : Discharge by general pathologist</li>
 *   <li>_306412002 : Discharge by haematologist</li>
 *   <li>_306413007 : Discharge by immunopathologist</li>
 *   <li>_306414001 : Discharge by medical microbiologist</li>
 *   <li>_306415000 : Discharge by neuropathologist</li>
 *   <li>_306416004 : Discharge by physician</li>
 *   <li>_306417008 : Discharge by clinical allergist</li>
 *   <li>_306418003 : Discharge by cardiologist</li>
 *   <li>_306419006 : Discharge by chest physician</li>
 *   <li>_306420000 : Discharge by thoracic physician</li>
 *   <li>_306422008 : Discharge by clinical haematologist</li>
 *   <li>_306423003 : Discharge by clinical immunologist</li>
 *   <li>_306424009 : Discharge by clinical neurophysiologist</li>
 *   <li>_306425005 : Discharge by clinical pharmacologist</li>
 *   <li>_306426006 : Discharge by clinical physiologist</li>
 *   <li>_306427002 : Discharge by dermatologist</li>
 *   <li>_306428007 : Discharge by endocrinologist</li>
 *   <li>_306429004 : Discharge by gastroenterologist</li>
 *   <li>_306430009 : Discharge by general physician</li>
 *   <li>_306431008 : Discharge by geneticist</li>
 *   <li>_306432001 : Discharge by clinical geneticist</li>
 *   <li>_306433006 : Discharge by clinical cytogeneticist</li>
 *   <li>_306434000 : Discharge by clinical molecular geneticist</li>
 *   <li>_306435004 : Discharge by genitourinary medicine physician</li>
 *   <li>_306436003 : Discharge by care of the elderly physician</li>
 *   <li>_306437007 : Discharge by infectious diseases physician</li>
 *   <li>_306438002 : Discharge by medical ophthalmologist</li>
 *   <li>_306439005 : Discharge by nephrologist</li>
 *   <li>_306440007 : Discharge by neurologist</li>
 *   <li>_306441006 : Discharge by nuclear medicine physician</li>
 *   <li>_306442004 : Discharge by palliative care physician</li>
 *   <li>_306443009 : Discharge by rehabilitation physician</li>
 *   <li>_306444003 : Discharge by rheumatologist</li>
 *   <li>_306445002 : Discharge by paediatrician</li>
 *   <li>_306446001 : Discharge by community paediatrician</li>
 *   <li>_306447005 : Discharge by neonatologist</li>
 *   <li>_306448000 : Discharge by paediatric neurologist</li>
 *   <li>_306449008 : Discharge by paediatric oncologist</li>
 *   <li>_306450008 : Discharge by obstetrician and gynaecologist</li>
 *   <li>_306451007 : Discharge by gynaecologist</li>
 *   <li>_306452000 : Discharge by obstetrician</li>
 *   <li>_306453005 : Discharge by psychiatrist</li>
 *   <li>_306454004 : Discharge by child and adolescent psychiatrist</li>
 *   <li>_306455003 : Discharge by forensic psychiatrist</li>
 *   <li>_306456002 : Discharge by liaison psychiatrist</li>
 *   <li>_306457006 : Discharge by psychogeriatrician</li>
 *   <li>_306458001 : Discharge by psychiatrist for mental handicap</li>
 *   <li>_306459009 : Discharge by rehabilitation psychiatrist</li>
 *   <li>_306460004 : Discharge by radiologist</li>
 *   <li>_306461000 : Discharge by occupational health physician</li>
 *   <li>_306462007 : Discharge by surgeon</li>
 *   <li>_306463002 : Discharge by breast surgeon</li>
 *   <li>_306464008 : Discharge by cardiothoracic surgeon</li>
 *   <li>_306465009 : Discharge by thoracic surgeon</li>
 *   <li>_306466005 : Discharge by cardiac surgeon</li>
 *   <li>_306467001 : Discharge by dental surgeon</li>
 *   <li>_306468006 : Discharge by orthodontist</li>
 *   <li>_306469003 : Discharge by paediatric dentist</li>
 *   <li>_306470002 : Discharge by restorative dentist</li>
 *   <li>_306471003 : Discharge by ear, nose and throat surgeon</li>
 *   <li>_306472005 : Discharge by endocrine surgeon</li>
 *   <li>_306473000 : Discharge by gastrointestinal surgeon</li>
 *   <li>_306474006 : Discharge by general gastrointestinal surgeon</li>
 *   <li>_306475007 : Discharge by upper gastrointestinal surgeon</li>
 *   <li>_306476008 : Discharge by colorectal surgeon</li>
 *   <li>_306477004 : Discharge by general surgeon</li>
 *   <li>_306478009 : Discharge by hand surgeon</li>
 *   <li>_306479001 : Discharge by hepatobiliary surgeon</li>
 *   <li>_306480003 : Discharge by neurosurgeon</li>
 *   <li>_306481004 : Discharge by ophthalmologist</li>
 *   <li>_306482006 : Discharge by oral surgeon</li>
 *   <li>_306483001 : Discharge by orthopaedic surgeon</li>
 *   <li>_306484007 : Discharge by paediatric surgeon</li>
 *   <li>_306486009 : Discharge by pancreatic surgeon</li>
 *   <li>_306487000 : Discharge by plastic surgeon</li>
 *   <li>_306488005 : Discharge by transplant surgeon</li>
 *   <li>_306489002 : Discharge by trauma surgeon</li>
 *   <li>_306490006 : Discharge by urologist</li>
 *   <li>_306491005 : Discharge by vascular surgeon</li>
 *   <li>_306492003 : Discharge by nurse</li>
 *   <li>_306493008 : Discharge by agency nurse</li>
 *   <li>_306494002 : Discharge by clinical nurse specialist</li>
 *   <li>_306495001 : Discharge by breast care nurse</li>
 *   <li>_306496000 : Discharge by cardiac rehabilitation nurse</li>
 *   <li>_306497009 : Discharge by contact tracing nurse</li>
 *   <li>_306498004 : Discharge by continence nurse</li>
 *   <li>_306499007 : Discharge by diabetic liaison nurse</li>
 *   <li>_306500003 : Discharge by genitourinary nurse</li>
 *   <li>_306501004 : Discharge by lymphoedema care nurse</li>
 *   <li>_306502006 : Discharge by nurse behavioural therapist</li>
 *   <li>_306503001 : Discharge by nurse psychotherapist</li>
 *   <li>_306504007 : Discharge by pain management nurse</li>
 *   <li>_306505008 : Discharge by paediatric nurse</li>
 *   <li>_306506009 : Discharge by psychiatric nurse</li>
 *   <li>_306507000 : Discharge by oncology nurse</li>
 *   <li>_306508005 : Discharge by rheumatology nurse specialist</li>
 *   <li>_306509002 : Discharge by stoma nurse</li>
 *   <li>_306510007 : Discharge by stomatherapist</li>
 *   <li>_306511006 : Discharge by community-based nurse</li>
 *   <li>_306512004 : Discharge by community nurse</li>
 *   <li>_306513009 : Discharge by community psychiatric nurse</li>
 *   <li>_306514003 : Discharge by company nurse</li>
 *   <li>_306515002 : Discharge by district nurse</li>
 *   <li>_306516001 : Discharge by health visitor</li>
 *   <li>_306517005 : Discharge by practice nurse</li>
 *   <li>_306518000 : Discharge by liaison nurse</li>
 *   <li>_306519008 : Discharge by nurse practitioner</li>
 *   <li>_306520002 : Discharge by outreach nurse</li>
 *   <li>_306521003 : Discharge by research nurse</li>
 *   <li>_306522005 : Discharge by midwife</li>
 *   <li>_306523000 : Discharge by community-based midwife</li>
 *   <li>_306524006 : Discharge by hospital-based midwife</li>
 *   <li>_306525007 : Discharge by educational psychologist</li>
 *   <li>_306526008 : Discharge by psychotherapist</li>
 *   <li>_306527004 : Discharge by professional allied to medicine</li>
 *   <li>_306528009 : Discharge by arts therapist</li>
 *   <li>_306530006 : Discharge by dance therapist</li>
 *   <li>_306531005 : Discharge by drama therapist</li>
 *   <li>_306532003 : Discharge by music therapist</li>
 *   <li>_306533008 : Discharge by play therapist</li>
 *   <li>_306534002 : Discharge by audiologist</li>
 *   <li>_306535001 : Discharge by audiology technician</li>
 *   <li>_306536000 : Discharge by podiatrist</li>
 *   <li>_306538004 : Discharge by community-based podiatrist</li>
 *   <li>_306539007 : Discharge by hospital-based podiatrist</li>
 *   <li>_306540009 : Discharge by dietitian</li>
 *   <li>_306541008 : Discharge by hospital-based dietitian</li>
 *   <li>_306542001 : Discharge by community-based dietitian</li>
 *   <li>_306543006 : Discharge by occupational therapist</li>
 *   <li>_306544000 : Discharge by community-based occupational therapist</li>
 *   <li>_306545004 : Discharge by social services department occupational therapist</li>
 *   <li>_306546003 : Discharge by hospital-based occupational therapist</li>
 *   <li>_306547007 : Discharge by optometrist</li>
 *   <li>_306548002 : Discharge by orthoptist</li>
 *   <li>_306549005 : Discharge by orthotist</li>
 *   <li>_306550005 : Discharge by surgical fitter</li>
 *   <li>_306551009 : Discharge by physiotherapist</li>
 *   <li>_306552002 : Discharge by community-based physiotherapist</li>
 *   <li>_306553007 : Discharge by hospital-based physiotherapist</li>
 *   <li>_306554001 : Discharge by radiographer</li>
 *   <li>_306555000 : Discharge by diagnostic radiographer</li>
 *   <li>_306556004 : Discharge by therapeutic radiographer</li>
 *   <li>_306557008 : Discharge by speech and language therapist</li>
 *   <li>_306558003 : Discharge by community-based speech and language therapist</li>
 *   <li>_306559006 : Discharge by hospital-based speech and language therapist</li>
 *   <li>_306560001 : Self-discharge</li>
 *   <li>_306561002 : Discharge from establishment</li>
 *   <li>_306562009 : Discharge from service</li>
 *   <li>_306563004 : Discharge from Accident and Emergency service</li>
 *   <li>_306564005 : Discharge from anaesthetic service</li>
 *   <li>_306565006 : Discharge from clinical oncology service</li>
 *   <li>_306566007 : Discharge from radiotherapy service</li>
 *   <li>_306567003 : Discharge from family planning service</li>
 *   <li>_306568008 : Discharge from intensive care service</li>
 *   <li>_306569000 : Discharge from adult intensive care service</li>
 *   <li>_306570004 : Discharge from paediatric intensive care service</li>
 *   <li>_306571000 : Discharge from medical service</li>
 *   <li>_306572007 : Discharge from clinical allergy service</li>
 *   <li>_306573002 : Discharge from audiology service</li>
 *   <li>_306574008 : Discharge from cardiology service</li>
 *   <li>_306575009 : Discharge from chest medicine service</li>
 *   <li>_306576005 : Discharge from respiratory medicine service</li>
 *   <li>_306577001 : Discharge from thoracic medicine service</li>
 *   <li>_306578006 : Discharge from clinical immunology service</li>
 *   <li>_306579003 : Discharge from clinical neurophysiology service</li>
 *   <li>_306580000 : Discharge from clinical pharmacology service</li>
 *   <li>_306581001 : Discharge from clinical physiology service</li>
 *   <li>_306582008 : Discharge from dermatology service</li>
 *   <li>_306583003 : Discharge from endocrinology service</li>
 *   <li>_306584009 : Discharge from gastroenterology service</li>
 *   <li>_306585005 : Discharge from general medical service</li>
 *   <li>_306586006 : Discharge from genetics service</li>
 *   <li>_306588007 : Discharge from clinical genetics service</li>
 *   <li>_306589004 : Discharge from clinical cytogenetics service</li>
 *   <li>_306590008 : Discharge from clinical molecular genetics service</li>
 *   <li>_306591007 : Discharge from genitourinary medicine service</li>
 *   <li>_306592000 : Discharge from care of the elderly service</li>
 *   <li>_306593005 : Discharge from infectious diseases service</li>
 *   <li>_306594004 : Discharge from nephrology service</li>
 *   <li>_306595003 : Discharge from neurology service</li>
 *   <li>_306596002 : Discharge from nuclear medicine service</li>
 *   <li>_306597006 : Discharge from palliative care service</li>
 *   <li>_306598001 : Discharge from rheumatology service</li>
 *   <li>_306599009 : Discharge from paediatric service</li>
 *   <li>_306600007 : Discharge from community paediatric service</li>
 *   <li>_306601006 : Discharge from paediatric neurology service</li>
 *   <li>_306602004 : Discharge from paediatric oncology service</li>
 *   <li>_306603009 : Discharge from special care baby service</li>
 *   <li>_306604003 : Discharge from obstetrics and gynaecology service</li>
 *   <li>_306605002 : Discharge from obstetrics service</li>
 *   <li>_306606001 : Discharge from gynaecology service</li>
 *   <li>_306607005 : Discharge from psychiatry service</li>
 *   <li>_306608000 : Discharge from child and adolescent psychiatry service</li>
 *   <li>_306609008 : Discharge from forensic psychiatry service</li>
 *   <li>_306610003 : Discharge from liaison psychiatry service</li>
 *   <li>_306611004 : Discharge from mental handicap psychiatry service</li>
 *   <li>_306612006 : Discharge from psychogeriatrician service</li>
 *   <li>_306613001 : Discharge from rehabilitation psychiatry service</li>
 *   <li>_306614007 : Discharge from pathology service</li>
 *   <li>_306615008 : Discharge from blood transfusion service</li>
 *   <li>_306616009 : Discharge from chemical pathology service</li>
 *   <li>_306617000 : Discharge from haematology service</li>
 *   <li>_306618005 : Discharge from pain management service</li>
 *   <li>_306619002 : Discharge from occupational health service</li>
 *   <li>_306620008 : Discharge from psychotherapy service</li>
 *   <li>_306621007 : Discharge from professionals allied to medicine service</li>
 *   <li>_306622000 : Discharge from arts therapy service</li>
 *   <li>_306623005 : Discharge from podiatry service</li>
 *   <li>_306624004 : Discharge from community podiatry service</li>
 *   <li>_306625003 : Discharge from hospital podiatry service</li>
 *   <li>_306626002 : Discharge from dietetics service</li>
 *   <li>_306627006 : Discharge from hospital dietetics service</li>
 *   <li>_306628001 : Discharge from community dietetics service</li>
 *   <li>_306629009 : Discharge from occupational therapy service</li>
 *   <li>_306630004 : Discharge from hospital occupational therapy service</li>
 *   <li>_306631000 : Discharge from community occupational therapy service</li>
 *   <li>_306632007 : Discharge from orthoptics service</li>
 *   <li>_306633002 : Discharge from hospital orthoptics service</li>
 *   <li>_306634008 : Discharge from community orthoptics service</li>
 *   <li>_306635009 : Discharge from physiotherapy service</li>
 *   <li>_306636005 : Discharge from hospital physiotherapy service</li>
 *   <li>_306637001 : Discharge from community physiotherapy service</li>
 *   <li>_306638006 : Discharge from speech and language therapy service</li>
 *   <li>_306639003 : Discharge from hospital speech and language therapy service</li>
 *   <li>_306640001 : Discharge from community speech and language therapy service</li>
 *   <li>_306641002 : Discharge from orthotics service</li>
 *   <li>_306642009 : Discharge from hospital orthotics service</li>
 *   <li>_306643004 : Discharge from community orthotics service</li>
 *   <li>_306644005 : Discharge from surgical fitting service</li>
 *   <li>_306645006 : Discharge from hospital surgical fitting service</li>
 *   <li>_306646007 : Discharge from community surgical fitting service</li>
 *   <li>_306647003 : Discharge from radiology service</li>
 *   <li>_306648008 : Discharge from surgical service</li>
 *   <li>_306649000 : Discharge from breast surgery service</li>
 *   <li>_306650000 : Discharge from cardiothoracic surgery service</li>
 *   <li>_306651001 : Discharge from thoracic surgery service</li>
 *   <li>_306652008 : Discharge from cardiac surgery service</li>
 *   <li>_306653003 : Discharge from dental surgery service</li>
 *   <li>_306654009 : Discharge from orthodontics service</li>
 *   <li>_306655005 : Discharge from paediatric dentistry service</li>
 *   <li>_306656006 : Discharge from restorative dentistry service</li>
 *   <li>_306657002 : Discharge from ear, nose and throat service</li>
 *   <li>_306658007 : Discharge from endocrine surgery service</li>
 *   <li>_306659004 : Discharge from gastrointestinal surgical service</li>
 *   <li>_306660009 : Discharge from general gastrointestinal surgical service</li>
 *   <li>_306661008 : Discharge from upper gastrointestinal surgical service</li>
 *   <li>_306662001 : Discharge from colorectal surgery service</li>
 *   <li>_306663006 : Discharge from general surgical service</li>
 *   <li>_306664000 : Discharge from hand surgery service</li>
 *   <li>_306665004 : Discharge from neurosurgical service</li>
 *   <li>_306666003 : Discharge from ophthalmology service</li>
 *   <li>_306667007 : Discharge from oral surgery service</li>
 *   <li>_306668002 : Discharge from orthopaedic service</li>
 *   <li>_306669005 : Discharge from pancreatic surgery service</li>
 *   <li>_306670006 : Discharge from paediatric surgical service</li>
 *   <li>_306671005 : Discharge from plastic surgery service</li>
 *   <li>_306672003 : Discharge from transplant surgery service</li>
 *   <li>_306673008 : Discharge from trauma service</li>
 *   <li>_306674002 : Discharge from urology service</li>
 *   <li>_306675001 : Discharge from vascular surgery service</li>
 *   <li>_306676000 : Discharge from hospice</li>
 *   <li>_306677009 : Discharge from day hospital</li>
 *   <li>_306678004 : Discharge from psychiatry day hospital</li>
 *   <li>_306679007 : Discharge from psychogeriatric day hospital</li>
 *   <li>_306680005 : Discharge from care of the elderly day hospital</li>
 *   <li>_306681009 : Discharge from hospice day hospital</li>
 *   <li>_306682002 : Discharge from ward</li>
 *   <li>_306683007 : Discharge from day ward</li>
 *   <li>_306684001 : Case closure by social services department</li>
 *   <li>_306685000 : Discharge to establishment</li>
 *   <li>_306689006 : Discharge to home</li>
 *   <li>_306690002 : Discharge to relatives home</li>
 *   <li>_306691003 : Discharge to residential home</li>
 *   <li>_306692005 : Discharge to private residential home</li>
 *   <li>_306693000 : Discharge to part III residential home</li>
 *   <li>_306694006 : Discharge to nursing home</li>
 *   <li>_306695007 : Discharge to private nursing home</li>
 *   <li>_306696008 : Discharge to part III accommodation</li>
 *   <li>_306697004 : Discharge to sheltered housing</li>
 *   <li>_306698009 : Discharge to warden controlled accommodation</li>
 *   <li>_306699001 : Discharge to hospital</li>
 *   <li>_306700000 : Discharge to long stay hospital</li>
 *   <li>_306701001 : Discharge to community hospital</li>
 *   <li>_306703003 : Discharge to tertiary referral hospital</li>
 *   <li>_306705005 : Discharge to police custody</li>
 *   <li>_306706006 : Discharge to ward</li>
 *   <li>_306707002 : Discharge to day ward</li>
 *   <li>_306729003 : Case closure by social worker</li>
 *   <li>_306731007 : Admission by general dental surgeon</li>
 *   <li>_306732000 : Admission to general dental surgery department</li>
 *   <li>_306733005 : Discharge by general dental surgeon</li>
 *   <li>_306803007 : Admission to stroke unit</li>
 *   <li>_306804001 : Admission to young disabled unit</li>
 *   <li>_306808003 : Discharge from stroke service</li>
 *   <li>_306809006 : Discharge from young disabled service</li>
 *   <li>_306858005 : Discharge by audiological physician</li>
 *   <li>_306862004 : Discharge from dance therapy service</li>
 *   <li>_306863009 : Discharge from drama therapy service</li>
 *   <li>_306864003 : Discharge from music therapy service</li>
 *   <li>_306967009 : Admission to hand surgery department</li>
 *   <li>_307064007 : Admission by clinical haematologist</li>
 *   <li>_307382004 : Discharge from head injury rehabilitation service</li>
 *   <li>_307383009 : Discharge from community rehabilitation service</li>
 *   <li>_307838002 : Discharge by person</li>
 *   <li>_308018004 : Discharge from rehabilitation service</li>
 *   <li>_308251003 : Admission to clinical oncology department</li>
 *   <li>_308252005 : Admission to radiotherapy department</li>
 *   <li>_308253000 : Admission to diabetic department</li>
 *   <li>_308283009 : Discharge from hospital</li>
 *   <li>_309568009 : Delayed discharge - social services</li>
 *   <li>_310361003 : Non-urgent cardiological admission</li>
 *   <li>_310513006 : Discharge by medical oncologist</li>
 *   <li>_310518002 : Admission by medical oncologist</li>
 *   <li>_313331005 : Admission for social reasons</li>
 *   <li>_313385005 : Admit cardiology emergency</li>
 *   <li>_397769005 : Unexpected admission to high dependency unit</li>
 *   <li>_397945004 : Unexpected admission to intensive care unit</li>
 *   <li>_398162007 : Admission to high dependency unit</li>
 *   <li>_405614004 : Unexpected hospital admission</li>
 *   <li>_405807003 : Discharge to convalescence</li>
 *   <li>_408489005 : Admit respiratory emergency</li>
 *   <li>_408501008 : Admit COPD emergency</li>
 *   <li>_413128002 : Discharge from learning disability team</li>
 *   <li>_416683003 : Admit heart failure emergency</li>
 *   <li>_427675001 : Admission from</li>
 *   <li>_25131000000105 : Discharge from intermediate care</li>
 *   <li>_25241000000106 : Discharge from care</li>
 *   <li>_84901000000108 : Admission to high dependency unit or intensive therapy unit following anaesthetic adverse event</li>
 *   <li>_88861000000105 : Discharge from care against professional advice</li>
 *   <li>_88881000000101 : Discharge from care in accordance with professional advice</li>
 *   <li>_91981000000104 : Admission to regional secure unit</li>
 *   <li>_91991000000102 : Admission to medium secure unit</li>
 *   <li>_92231000000101 : Admission to psychiatric intensive care unit</li>
 *   <li>_108171000000102 : Admission to high secure unit</li>
 *   <li>_163781000000106 : Admission to mental health specialist services</li>
 *   <li>_200201000000101 : Discharge from practice nurse heart failure clinic</li>
 *   <li>_203221000000100 : Antenatal admission</li>
 *   <li>_242171000000106 : Admission under the Mental Health Act</li>
 *   <li>_388751000000101 : Admission by co-operative general practitioner</li>
 *   <li>_395581000000108 : Admission by deputising general practitioner</li>
 *   <li>_395661000000100 : Emergency psychiatric admission under Mental Health Act 1983 England and Wales</li>
 *   <li>_401751000000103 : Discharge by general practice registrar</li>
 *   <li>_424191000000103 : Discharge by co-operative GP general practitioner</li>
 *   <li>_424211000000104 : Admission by GP general practitioner registrar</li>
 *   <li>_431341000000109 : Discharge by deputising general practitioner</li>
 *   <li>_437171000000107 : Admission by associate GP general practitioner</li>
 *   <li>_443291000000109 : Discharge by associate general practitioner</li>
 *   <li>_449201000000100 : Admission by assistant GP general practitioner</li>
 *   <li>_455271000000109 : Discharge by assistant general practitioner</li>
 *   <li>_466881000000106 : Discharge by locum general practitioner</li>
 *   <li>_476981000000106 : Admission by locum general practitioner</li>
 *   <li>_782351000000102 : Discharge from primary healthcare team</li>
 *   <li>_790241000000105 : Discharge from care following non-attendance</li>
 *   <li>_818361000000102 : Discharge from community specialist palliative care</li>
 *   <li>_818941000000102 : Discharge from advanced primary nurse care</li>
 *   <li>_864811000000104 : 111 contact disposition to general practitioner</li>
 *   <li>_864831000000107 : 111 contact disposition to pharmacist</li>
 *   <li>_864851000000100 : 111 contact disposition to out of hours service</li>
 *   <li>_864871000000109 : 111 contact disposition to community service</li>
 *   <li>_864891000000108 : 111 contact disposition to mental health service</li>
 *   <li>_864911000000106 : 111 contact disposition to Social Services</li>
 *   <li>_864931000000103 : 111 contact disposition to 999 transfer</li>
 *   <li>_864951000000105 : 111 contact disposition to accident and emergency department</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum Encounterdisposition implements VocabularyEntry {
	
	
	/**
	 * Hospital re-admission
	 */
	_417005 ("417005", "Hospital re-admission"),
	
	/**
	 * Hospital admission for isolation
	 */
	_1505002 ("1505002", "Hospital admission for isolation"),
	
	/**
	 * Patient discharge, deceased, medicolegal case
	 */
	_1917008 ("1917008", "Patient discharge, deceased, medicolegal case"),
	
	/**
	 * Hospital admission, urgent, 48 hours
	 */
	_2252009 ("2252009", "Hospital admission, urgent, 48 hours"),
	
	/**
	 * Hospital admission, type unclassified, explain by report
	 */
	_2876009 ("2876009", "Hospital admission, type unclassified, explain by report"),
	
	/**
	 * Patient discharge, deceased, autopsy
	 */
	_3133002 ("3133002", "Patient discharge, deceased, autopsy"),
	
	/**
	 * Routine patient disposition, no follow-up planned
	 */
	_3780001 ("3780001", "Routine patient disposition, no follow-up planned"),
	
	/**
	 * Hospital admission, transfer from other hospital or health care facility
	 */
	_4563007 ("4563007", "Hospital admission, transfer from other hospital or health care facility"),
	
	/**
	 * Speciality clinic admission
	 */
	_5161006 ("5161006", "Speciality clinic admission"),
	
	/**
	 * Hospital admission, elective
	 */
	_8715000 ("8715000", "Hospital admission, elective"),
	
	/**
	 * Patient discharge, to home, with assistance
	 */
	_10161009 ("10161009", "Patient discharge, to home, with assistance"),
	
	/**
	 * Hospital admission, emergency, from emergency room, accidental injury
	 */
	_10378005 ("10378005", "Hospital admission, emergency, from emergency room, accidental injury"),
	
	/**
	 * Emergency room admission, dead on arrival DOA
	 */
	_11545006 ("11545006", "Emergency room admission, dead on arrival (DOA)"),
	
	/**
	 * Patient discharge, escaped from custody
	 */
	_12616003 ("12616003", "Patient discharge, escaped from custody"),
	
	/**
	 * Hospital admission, elective, with partial pre-admission work-up
	 */
	_15584006 ("15584006", "Hospital admission, elective, with partial pre-admission work-up"),
	
	/**
	 * Hospital admission, emergency, indirect
	 */
	_18083007 ("18083007", "Hospital admission, emergency, indirect"),
	
	/**
	 * Hospital admission, emergency, from emergency room, medical nature
	 */
	_19951005 ("19951005", "Hospital admission, emergency, from emergency room, medical nature"),
	
	/**
	 * Patient on pass
	 */
	_20541009 ("20541009", "Patient on pass"),
	
	/**
	 * Hospital admission, for research investigation
	 */
	_23473000 ("23473000", "Hospital admission, for research investigation"),
	
	/**
	 * Hospital admission, under police custody
	 */
	_25986004 ("25986004", "Hospital admission, under police custody"),
	
	/**
	 * Hospital admission
	 */
	_32485007 ("32485007", "Hospital admission"),
	
	/**
	 * Patient discharge, elopement
	 */
	_34596002 ("34596002", "Patient discharge, elopement"),
	
	/**
	 * Hospital admission, pre-nursing home placement
	 */
	_36723004 ("36723004", "Hospital admission, pre-nursing home placement"),
	
	/**
	 * General outpatient clinic admission
	 */
	_40274000 ("40274000", "General outpatient clinic admission"),
	
	/**
	 * Patient discharge, deceased, donation of body
	 */
	_41819004 ("41819004", "Patient discharge, deceased, donation of body"),
	
	/**
	 * Hospital admission, precertified by medical audit action
	 */
	_45702004 ("45702004", "Hospital admission, precertified by medical audit action"),
	
	/**
	 * Patient discharge, type unclassified, explain by report
	 */
	_47031007 ("47031007", "Patient discharge, type unclassified, explain by report"),
	
	/**
	 * Hospital admission, mother, for observation, delivered outside of hospital
	 */
	_47348005 ("47348005", "Hospital admission, mother, for observation, delivered outside of hospital"),
	
	/**
	 * Hospital admission, special
	 */
	_48183000 ("48183000", "Hospital admission, special"),
	
	/**
	 * Emergency room admission, followed by release
	 */
	_50331008 ("50331008", "Emergency room admission, followed by release"),
	
	/**
	 * Hospital admission, short-term
	 */
	_50699000 ("50699000", "Hospital admission, short-term"),
	
	/**
	 * Emergency room admission
	 */
	_50849002 ("50849002", "Emergency room admission"),
	
	/**
	 * Patient discharge, to legal custody
	 */
	_50861005 ("50861005", "Patient discharge, to legal custody"),
	
	/**
	 * Hospital admission, donor for transplant organ
	 */
	_51032003 ("51032003", "Hospital admission, donor for transplant organ"),
	
	/**
	 * Hospital admission, parent, for in-hospital child care
	 */
	_51501005 ("51501005", "Hospital admission, parent, for in-hospital child care"),
	
	/**
	 * Hospital admission, involuntary
	 */
	_52748007 ("52748007", "Hospital admission, involuntary"),
	
	/**
	 * Hospital admission, for laboratory work-up, radiography, etc
	 */
	_55402005 ("55402005", "Hospital admission, for laboratory work-up, radiography, etc."),
	
	/**
	 * Patient discharge
	 */
	_58000006 ("58000006", "Patient discharge"),
	
	/**
	 * Hospital admission, infant, for observation, delivered outside of hospital
	 */
	_60059000 ("60059000", "Hospital admission, infant, for observation, delivered outside of hospital"),
	
	/**
	 * Patient follow-up planned and scheduled
	 */
	_61342007 ("61342007", "Patient follow-up planned and scheduled"),
	
	/**
	 * Hospital admission, from remote area, by means of special transportation
	 */
	_63551005 ("63551005", "Hospital admission, from remote area, by means of special transportation"),
	
	/**
	 * Hospital admission, short-term, day care
	 */
	_65043002 ("65043002", "Hospital admission, short-term, day care"),
	
	/**
	 * Patient discharge, to home, routine
	 */
	_65537008 ("65537008", "Patient discharge, to home, routine"),
	
	/**
	 * Hospital admission, by legal authority commitment
	 */
	_70755000 ("70755000", "Hospital admission, by legal authority (commitment)"),
	
	/**
	 * Hospital admission, limited to designated procedures
	 */
	_71290004 ("71290004", "Hospital admission, limited to designated procedures"),
	
	/**
	 * Hospital admission, emergency, from emergency room
	 */
	_73607007 ("73607007", "Hospital admission, emergency, from emergency room"),
	
	/**
	 * Hospital admission, short-term, 24 hours
	 */
	_74857009 ("74857009", "Hospital admission, short-term, 24 hours"),
	
	/**
	 * Emergency room admission, died in emergency room
	 */
	_75004002 ("75004002", "Emergency room admission, died in emergency room"),
	
	/**
	 * Hospital admission, for observation
	 */
	_76464004 ("76464004", "Hospital admission, for observation"),
	
	/**
	 * Hospital admission, emergency, direct
	 */
	_78680009 ("78680009", "Hospital admission, emergency, direct"),
	
	/**
	 * Patient discharge, deceased, no autopsy
	 */
	_79779006 ("79779006", "Patient discharge, deceased, no autopsy"),
	
	/**
	 * Hospital admission, elective, without pre-admission work-up
	 */
	_81672003 ("81672003", "Hospital admission, elective, without pre-admission work-up"),
	
	/**
	 * Hospital admission, blood donor
	 */
	_82942009 ("82942009", "Hospital admission, blood donor"),
	
	/**
	 * Patient discharge, to home, ambulatory
	 */
	_86400002 ("86400002", "Patient discharge, to home, ambulatory"),
	
	/**
	 * Patient discharge, deceased, to anatomic board
	 */
	_89334003 ("89334003", "Patient discharge, deceased, to anatomic board"),
	
	/**
	 * Patient follow-up to return when and if necessary
	 */
	_91310009 ("91310009", "Patient follow-up to return when and if necessary"),
	
	/**
	 * Patient disposition
	 */
	_107726003 ("107726003", "Patient disposition"),
	
	/**
	 * Hospital admission, elective, with complete pre-admission work-up
	 */
	_112689000 ("112689000", "Hospital admission, elective, with complete pre-admission work-up"),
	
	/**
	 * Hospital admission, boarder, for social reasons
	 */
	_112690009 ("112690009", "Hospital admission, boarder, for social reasons"),
	
	/**
	 * Involuntary admission
	 */
	_135847007 ("135847007", "Involuntary admission"),
	
	/**
	 * Voluntary admission
	 */
	_135848002 ("135848002", "Voluntary admission"),
	
	/**
	 * Admission review
	 */
	_177079006 ("177079006", "Admission review"),
	
	/**
	 * Holiday relief admission
	 */
	_183430001 ("183430001", "Holiday relief admission"),
	
	/**
	 * Admission to cardiac intensive care unit
	 */
	_183446009 ("183446009", "Admission to cardiac intensive care unit"),
	
	/**
	 * Admission to respiratory intensive care unit
	 */
	_183447000 ("183447000", "Admission to respiratory intensive care unit"),
	
	/**
	 * Admission to neurological intensive care unit
	 */
	_183448005 ("183448005", "Admission to neurological intensive care unit"),
	
	/**
	 * Admission to metabolic intensive care unit
	 */
	_183449002 ("183449002", "Admission to metabolic intensive care unit"),
	
	/**
	 * Admission to burn unit
	 */
	_183450002 ("183450002", "Admission to burn unit"),
	
	/**
	 * Emergency hospital admission
	 */
	_183452005 ("183452005", "Emergency hospital admission"),
	
	/**
	 * Admit psychiatric emergency
	 */
	_183455007 ("183455007", "Admit psychiatric emergency"),
	
	/**
	 * Admit geriatric emergency
	 */
	_183457004 ("183457004", "Admit geriatric emergency"),
	
	/**
	 * Admit paediatric emergency
	 */
	_183458009 ("183458009", "Admit paediatric emergency"),
	
	/**
	 * Admit gynaecological emergency
	 */
	_183459001 ("183459001", "Admit gynaecological emergency"),
	
	/**
	 * Admit obstetric emergency
	 */
	_183460006 ("183460006", "Admit obstetric emergency"),
	
	/**
	 * Admit orthopaedic emergency
	 */
	_183461005 ("183461005", "Admit orthopaedic emergency"),
	
	/**
	 * Admit ENT emergency
	 */
	_183462003 ("183462003", "Admit ENT emergency"),
	
	/**
	 * Admit trauma emergency
	 */
	_183463008 ("183463008", "Admit trauma emergency"),
	
	/**
	 * Admit ophthalmological emergency
	 */
	_183464002 ("183464002", "Admit ophthalmological emergency"),
	
	/**
	 * Admit rheumatology emergency
	 */
	_183465001 ("183465001", "Admit rheumatology emergency"),
	
	/**
	 * Admit dermatology emergency
	 */
	_183466000 ("183466000", "Admit dermatology emergency"),
	
	/**
	 * Admit neurology emergency
	 */
	_183467009 ("183467009", "Admit neurology emergency"),
	
	/**
	 * Admit urology emergency
	 */
	_183468004 ("183468004", "Admit urology emergency"),
	
	/**
	 * Admit radiotherapy emergency
	 */
	_183469007 ("183469007", "Admit radiotherapy emergency"),
	
	/**
	 * Admit haematology emergency
	 */
	_183470008 ("183470008", "Admit haematology emergency"),
	
	/**
	 * Admit plastic surgery emergency
	 */
	_183471007 ("183471007", "Admit plastic surgery emergency"),
	
	/**
	 * Admit diabetic emergency
	 */
	_183472000 ("183472000", "Admit diabetic emergency"),
	
	/**
	 * Admit oral surgical emergency
	 */
	_183473005 ("183473005", "Admit oral surgical emergency"),
	
	/**
	 * Admit psychogeriatric emergency
	 */
	_183474004 ("183474004", "Admit psychogeriatric emergency"),
	
	/**
	 * Admit renal medicine emergency
	 */
	_183475003 ("183475003", "Admit renal medicine emergency"),
	
	/**
	 * Admit neurosurgical emergency
	 */
	_183476002 ("183476002", "Admit neurosurgical emergency"),
	
	/**
	 * Admit cardiothoracic emergency
	 */
	_183477006 ("183477006", "Admit cardiothoracic emergency"),
	
	/**
	 * Emergency admission, asthma
	 */
	_183478001 ("183478001", "Emergency admission, asthma"),
	
	/**
	 * Non-urgent hospital admission
	 */
	_183481006 ("183481006", "Non-urgent hospital admission"),
	
	/**
	 * Non-urgent medical admission
	 */
	_183487005 ("183487005", "Non-urgent medical admission"),
	
	/**
	 * Non-urgent surgical admission
	 */
	_183488000 ("183488000", "Non-urgent surgical admission"),
	
	/**
	 * Non-urgent psychiatric admission
	 */
	_183489008 ("183489008", "Non-urgent psychiatric admission"),
	
	/**
	 * Non-urgent geriatric admission
	 */
	_183491000 ("183491000", "Non-urgent geriatric admission"),
	
	/**
	 * Non-urgent paediatric admission
	 */
	_183492007 ("183492007", "Non-urgent paediatric admission"),
	
	/**
	 * Non-urgent gynaecological admission
	 */
	_183493002 ("183493002", "Non-urgent gynaecological admission"),
	
	/**
	 * Non-urgent obstetric admission
	 */
	_183494008 ("183494008", "Non-urgent obstetric admission"),
	
	/**
	 * Non-urgent orthopaedic admission
	 */
	_183495009 ("183495009", "Non-urgent orthopaedic admission"),
	
	/**
	 * Non-urgent ENT admission
	 */
	_183496005 ("183496005", "Non-urgent ENT admission"),
	
	/**
	 * Non-urgent trauma admission
	 */
	_183497001 ("183497001", "Non-urgent trauma admission"),
	
	/**
	 * Non-urgent ophthalmological admission
	 */
	_183498006 ("183498006", "Non-urgent ophthalmological admission"),
	
	/**
	 * Non-urgent rheumatology admission
	 */
	_183499003 ("183499003", "Non-urgent rheumatology admission"),
	
	/**
	 * Non-urgent dermatology admission
	 */
	_183500007 ("183500007", "Non-urgent dermatology admission"),
	
	/**
	 * Non-urgent neurology admission
	 */
	_183501006 ("183501006", "Non-urgent neurology admission"),
	
	/**
	 * Non-urgent urology admission
	 */
	_183502004 ("183502004", "Non-urgent urology admission"),
	
	/**
	 * Non-urgent radiotherapy admission
	 */
	_183503009 ("183503009", "Non-urgent radiotherapy admission"),
	
	/**
	 * Non-urgent haematology admission
	 */
	_183504003 ("183504003", "Non-urgent haematology admission"),
	
	/**
	 * Non-urgent plastic surgery admission
	 */
	_183505002 ("183505002", "Non-urgent plastic surgery admission"),
	
	/**
	 * Non-urgent diabetic admission
	 */
	_183506001 ("183506001", "Non-urgent diabetic admission"),
	
	/**
	 * Non-urgent respiratory admission
	 */
	_183507005 ("183507005", "Non-urgent respiratory admission"),
	
	/**
	 * Non-urgent psychogeriatric admission
	 */
	_183508000 ("183508000", "Non-urgent psychogeriatric admission"),
	
	/**
	 * Non-urgent renal medicine admission
	 */
	_183509008 ("183509008", "Non-urgent renal medicine admission"),
	
	/**
	 * Non-urgent neurosurgical admission
	 */
	_183510003 ("183510003", "Non-urgent neurosurgical admission"),
	
	/**
	 * Non-urgent cardiothoracic admission
	 */
	_183511004 ("183511004", "Non-urgent cardiothoracic admission"),
	
	/**
	 * Non-urgent oral surgical admission
	 */
	_183512006 ("183512006", "Non-urgent oral surgical admission"),
	
	/**
	 * Delayed discharge to nursing home
	 */
	_183672007 ("183672007", "Delayed discharge to nursing home"),
	
	/**
	 * Urgent admission to hospice
	 */
	_183919006 ("183919006", "Urgent admission to hospice"),
	
	/**
	 * Routine admission to hospice
	 */
	_183920000 ("183920000", "Routine admission to hospice"),
	
	/**
	 * Admission to hospice for respite
	 */
	_183921001 ("183921001", "Admission to hospice for respite"),
	
	/**
	 * Patient self-discharge
	 */
	_183955003 ("183955003", "Patient self-discharge"),
	
	/**
	 * Patient self-discharge against medical advice
	 */
	_225928004 ("225928004", "Patient self-discharge against medical advice"),
	
	/**
	 * Admission for care
	 */
	_281685003 ("281685003", "Admission for care"),
	
	/**
	 * Admission for treatment
	 */
	_304566005 ("304566005", "Admission for treatment"),
	
	/**
	 * Admission for long-term care
	 */
	_304567001 ("304567001", "Admission for long-term care"),
	
	/**
	 * Admission for respite care
	 */
	_304568006 ("304568006", "Admission for respite care"),
	
	/**
	 * Admission procedure
	 */
	_305056002 ("305056002", "Admission procedure"),
	
	/**
	 * Admission by health worker
	 */
	_305224000 ("305224000", "Admission by health worker"),
	
	/**
	 * Admission by Accident and Emergency doctor
	 */
	_305226003 ("305226003", "Admission by Accident and Emergency doctor"),
	
	/**
	 * Admission by anaesthetist
	 */
	_305227007 ("305227007", "Admission by anaesthetist"),
	
	/**
	 * Admission by clinical oncologist
	 */
	_305228002 ("305228002", "Admission by clinical oncologist"),
	
	/**
	 * Admission by radiotherapist
	 */
	_305229005 ("305229005", "Admission by radiotherapist"),
	
	/**
	 * Admission by GP
	 */
	_305230000 ("305230000", "Admission by GP"),
	
	/**
	 * Admission by own GP
	 */
	_305231001 ("305231001", "Admission by own GP"),
	
	/**
	 * Admission by GP partner
	 */
	_305232008 ("305232008", "Admission by GP partner"),
	
	/**
	 * Admission by family planning doctor
	 */
	_305239004 ("305239004", "Admission by family planning doctor"),
	
	/**
	 * Admission by intensive care specialist
	 */
	_305240002 ("305240002", "Admission by intensive care specialist"),
	
	/**
	 * Admission by adult intensive care specialist
	 */
	_305241003 ("305241003", "Admission by adult intensive care specialist"),
	
	/**
	 * Admission by paediatric intensive care specialist
	 */
	_305242005 ("305242005", "Admission by paediatric intensive care specialist"),
	
	/**
	 * Admission by paediatrician
	 */
	_305243000 ("305243000", "Admission by paediatrician"),
	
	/**
	 * Admission by community paediatrician
	 */
	_305244006 ("305244006", "Admission by community paediatrician"),
	
	/**
	 * Admission by neonatologist
	 */
	_305245007 ("305245007", "Admission by neonatologist"),
	
	/**
	 * Admission by paediatric neurologist
	 */
	_305246008 ("305246008", "Admission by paediatric neurologist"),
	
	/**
	 * Admission by paediatric oncologist
	 */
	_305247004 ("305247004", "Admission by paediatric oncologist"),
	
	/**
	 * Admission by pain management specialist
	 */
	_305248009 ("305248009", "Admission by pain management specialist"),
	
	/**
	 * Admission by obstetrician  gynaecologist
	 */
	_305249001 ("305249001", "Admission by obstetrician & gynaecologist"),
	
	/**
	 * Admission by gynaecologist
	 */
	_305250001 ("305250001", "Admission by gynaecologist"),
	
	/**
	 * Admission by obstetrician
	 */
	_305251002 ("305251002", "Admission by obstetrician"),
	
	/**
	 * Admission by occupational health physician
	 */
	_305252009 ("305252009", "Admission by occupational health physician"),
	
	/**
	 * Admission by pathologist
	 */
	_305253004 ("305253004", "Admission by pathologist"),
	
	/**
	 * Admission by blood transfusion doctor
	 */
	_305254005 ("305254005", "Admission by blood transfusion doctor"),
	
	/**
	 * Admission by chemical pathologist
	 */
	_305255006 ("305255006", "Admission by chemical pathologist"),
	
	/**
	 * Admission by general pathologist
	 */
	_305256007 ("305256007", "Admission by general pathologist"),
	
	/**
	 * Admission by haematologist
	 */
	_305257003 ("305257003", "Admission by haematologist"),
	
	/**
	 * Admission by medical microbiologist
	 */
	_305258008 ("305258008", "Admission by medical microbiologist"),
	
	/**
	 * Admission by neuropathologist
	 */
	_305259000 ("305259000", "Admission by neuropathologist"),
	
	/**
	 * Admission by physician
	 */
	_305260005 ("305260005", "Admission by physician"),
	
	/**
	 * Admission by clinical allergist
	 */
	_305261009 ("305261009", "Admission by clinical allergist"),
	
	/**
	 * Admission by cardiologist
	 */
	_305262002 ("305262002", "Admission by cardiologist"),
	
	/**
	 * Admission by care of the elderly physician
	 */
	_305263007 ("305263007", "Admission by care of the elderly physician"),
	
	/**
	 * Admission by chest physician
	 */
	_305264001 ("305264001", "Admission by chest physician"),
	
	/**
	 * Admission by thoracic physician
	 */
	_305265000 ("305265000", "Admission by thoracic physician"),
	
	/**
	 * Admission by respiratory physician
	 */
	_305266004 ("305266004", "Admission by respiratory physician"),
	
	/**
	 * Admission by clinical immunologist
	 */
	_305267008 ("305267008", "Admission by clinical immunologist"),
	
	/**
	 * Admission by clinical neurophysiologist
	 */
	_305268003 ("305268003", "Admission by clinical neurophysiologist"),
	
	/**
	 * Admission by clinical pharmacologist
	 */
	_305269006 ("305269006", "Admission by clinical pharmacologist"),
	
	/**
	 * Admission by clinical physiologist
	 */
	_305270007 ("305270007", "Admission by clinical physiologist"),
	
	/**
	 * Admission by dermatologist
	 */
	_305271006 ("305271006", "Admission by dermatologist"),
	
	/**
	 * Admission by endocrinologist
	 */
	_305272004 ("305272004", "Admission by endocrinologist"),
	
	/**
	 * Admission by gastroenterologist
	 */
	_305274003 ("305274003", "Admission by gastroenterologist"),
	
	/**
	 * Admission by general physician
	 */
	_305275002 ("305275002", "Admission by general physician"),
	
	/**
	 * Admission by genitourinary medicine physician
	 */
	_305276001 ("305276001", "Admission by genitourinary medicine physician"),
	
	/**
	 * Admission by infectious diseases physician
	 */
	_305277005 ("305277005", "Admission by infectious diseases physician"),
	
	/**
	 * Admission by medical ophthalmologist
	 */
	_305278000 ("305278000", "Admission by medical ophthalmologist"),
	
	/**
	 * Admission by nephrologist
	 */
	_305279008 ("305279008", "Admission by nephrologist"),
	
	/**
	 * Admission by neurologist
	 */
	_305280006 ("305280006", "Admission by neurologist"),
	
	/**
	 * Admission by nuclear medicine physician
	 */
	_305281005 ("305281005", "Admission by nuclear medicine physician"),
	
	/**
	 * Admission by rheumatologist
	 */
	_305282003 ("305282003", "Admission by rheumatologist"),
	
	/**
	 * Admission by rehabilitation physician
	 */
	_305283008 ("305283008", "Admission by rehabilitation physician"),
	
	/**
	 * Admission by palliative care physician
	 */
	_305284002 ("305284002", "Admission by palliative care physician"),
	
	/**
	 * Admission by psychiatrist
	 */
	_305285001 ("305285001", "Admission by psychiatrist"),
	
	/**
	 * Admission by child and adolescent psychiatrist
	 */
	_305286000 ("305286000", "Admission by child and adolescent psychiatrist"),
	
	/**
	 * Admission by forensic psychiatrist
	 */
	_305287009 ("305287009", "Admission by forensic psychiatrist"),
	
	/**
	 * Admission by liaison psychiatrist
	 */
	_305288004 ("305288004", "Admission by liaison psychiatrist"),
	
	/**
	 * Admission by psychogeriatrician
	 */
	_305289007 ("305289007", "Admission by psychogeriatrician"),
	
	/**
	 * Admission by psychiatrist for mental handicap
	 */
	_305290003 ("305290003", "Admission by psychiatrist for mental handicap"),
	
	/**
	 * Admission by rehabilitation psychiatrist
	 */
	_305291004 ("305291004", "Admission by rehabilitation psychiatrist"),
	
	/**
	 * Admission by radiologist
	 */
	_305292006 ("305292006", "Admission by radiologist"),
	
	/**
	 * Admission by surgeon
	 */
	_305293001 ("305293001", "Admission by surgeon"),
	
	/**
	 * Admission by breast surgeon
	 */
	_305294007 ("305294007", "Admission by breast surgeon"),
	
	/**
	 * Admission by cardiothoracic surgeon
	 */
	_305295008 ("305295008", "Admission by cardiothoracic surgeon"),
	
	/**
	 * Admission by thoracic surgeon
	 */
	_305296009 ("305296009", "Admission by thoracic surgeon"),
	
	/**
	 * Admission by cardiac surgeon
	 */
	_305297000 ("305297000", "Admission by cardiac surgeon"),
	
	/**
	 * Admission by dental surgeon
	 */
	_305298005 ("305298005", "Admission by dental surgeon"),
	
	/**
	 * Admission by orthodontist
	 */
	_305299002 ("305299002", "Admission by orthodontist"),
	
	/**
	 * Admission by paediatric dentist
	 */
	_305300005 ("305300005", "Admission by paediatric dentist"),
	
	/**
	 * Admission by restorative dentist
	 */
	_305301009 ("305301009", "Admission by restorative dentist"),
	
	/**
	 * Admission by ear, nose and throat surgeon
	 */
	_305302002 ("305302002", "Admission by ear, nose and throat surgeon"),
	
	/**
	 * Admission by endocrine surgeon
	 */
	_305303007 ("305303007", "Admission by endocrine surgeon"),
	
	/**
	 * Admission by gastrointestinal surgeon
	 */
	_305304001 ("305304001", "Admission by gastrointestinal surgeon"),
	
	/**
	 * Admission by general gastrointestinal surgeon
	 */
	_305305000 ("305305000", "Admission by general gastrointestinal surgeon"),
	
	/**
	 * Admission by upper gastrointestinal surgeon
	 */
	_305306004 ("305306004", "Admission by upper gastrointestinal surgeon"),
	
	/**
	 * Admission by colorectal surgeon
	 */
	_305307008 ("305307008", "Admission by colorectal surgeon"),
	
	/**
	 * Admission by general surgeon
	 */
	_305308003 ("305308003", "Admission by general surgeon"),
	
	/**
	 * Admission by hand surgeon
	 */
	_305309006 ("305309006", "Admission by hand surgeon"),
	
	/**
	 * Admission by hepatobiliary surgeon
	 */
	_305310001 ("305310001", "Admission by hepatobiliary surgeon"),
	
	/**
	 * Admission by neurosurgeon
	 */
	_305311002 ("305311002", "Admission by neurosurgeon"),
	
	/**
	 * Admission by ophthalmologist
	 */
	_305312009 ("305312009", "Admission by ophthalmologist"),
	
	/**
	 * Admission by oral surgeon
	 */
	_305313004 ("305313004", "Admission by oral surgeon"),
	
	/**
	 * Admission by orthopaedic surgeon
	 */
	_305314005 ("305314005", "Admission by orthopaedic surgeon"),
	
	/**
	 * Admission by paediatric surgeon
	 */
	_305315006 ("305315006", "Admission by paediatric surgeon"),
	
	/**
	 * Admission by pancreatic surgeon
	 */
	_305316007 ("305316007", "Admission by pancreatic surgeon"),
	
	/**
	 * Admission by plastic surgeon
	 */
	_305317003 ("305317003", "Admission by plastic surgeon"),
	
	/**
	 * Admission by transplant surgeon
	 */
	_305318008 ("305318008", "Admission by transplant surgeon"),
	
	/**
	 * Admission by trauma surgeon
	 */
	_305319000 ("305319000", "Admission by trauma surgeon"),
	
	/**
	 * Admission by urologist
	 */
	_305320006 ("305320006", "Admission by urologist"),
	
	/**
	 * Admission by vascular surgeon
	 */
	_305321005 ("305321005", "Admission by vascular surgeon"),
	
	/**
	 * Admission by health care assistant
	 */
	_305322003 ("305322003", "Admission by health care assistant"),
	
	/**
	 * Admission by midwife
	 */
	_305323008 ("305323008", "Admission by midwife"),
	
	/**
	 * Admission by community-based midwife
	 */
	_305324002 ("305324002", "Admission by community-based midwife"),
	
	/**
	 * Admission by hospital-based midwife
	 */
	_305325001 ("305325001", "Admission by hospital-based midwife"),
	
	/**
	 * Admission by nurse
	 */
	_305326000 ("305326000", "Admission by nurse"),
	
	/**
	 * Admission by agency nurse
	 */
	_305327009 ("305327009", "Admission by agency nurse"),
	
	/**
	 * Admission by enrolled nurse
	 */
	_305328004 ("305328004", "Admission by enrolled nurse"),
	
	/**
	 * Admission by general nurse
	 */
	_305329007 ("305329007", "Admission by general nurse"),
	
	/**
	 * Admission by mental health nurse
	 */
	_305330002 ("305330002", "Admission by mental health nurse"),
	
	/**
	 * Admission by nurse for the mentally handicapped
	 */
	_305331003 ("305331003", "Admission by nurse for the mentally handicapped"),
	
	/**
	 * Admission by sick childrens nurse
	 */
	_305332005 ("305332005", "Admission by sick children's nurse"),
	
	/**
	 * Admission by nurse practitioner
	 */
	_305333000 ("305333000", "Admission by nurse practitioner"),
	
	/**
	 * Admission by nursing auxiliary
	 */
	_305334006 ("305334006", "Admission by nursing auxiliary"),
	
	/**
	 * Admission to establishment
	 */
	_305335007 ("305335007", "Admission to establishment"),
	
	/**
	 * Admission to hospice
	 */
	_305336008 ("305336008", "Admission to hospice"),
	
	/**
	 * Admission to community hospital
	 */
	_305337004 ("305337004", "Admission to community hospital"),
	
	/**
	 * Admission to GP hospital
	 */
	_305338009 ("305338009", "Admission to GP hospital"),
	
	/**
	 * Admission to private hospital
	 */
	_305339001 ("305339001", "Admission to private hospital"),
	
	/**
	 * Admission to long stay hospital
	 */
	_305340004 ("305340004", "Admission to long stay hospital"),
	
	/**
	 * Admission to tertiary referral hospital
	 */
	_305341000 ("305341000", "Admission to tertiary referral hospital"),
	
	/**
	 * Admission to ward
	 */
	_305342007 ("305342007", "Admission to ward"),
	
	/**
	 * Admission to day ward
	 */
	_305343002 ("305343002", "Admission to day ward"),
	
	/**
	 * Admission to day hospital
	 */
	_305344008 ("305344008", "Admission to day hospital"),
	
	/**
	 * Admission to psychiatric day hospital
	 */
	_305345009 ("305345009", "Admission to psychiatric day hospital"),
	
	/**
	 * Admission to psychogeriatric day hospital
	 */
	_305346005 ("305346005", "Admission to psychogeriatric day hospital"),
	
	/**
	 * Admission to elderly severely mentally ill day hospital
	 */
	_305347001 ("305347001", "Admission to elderly severely mentally ill day hospital"),
	
	/**
	 * Admission to care of the elderly day hospital
	 */
	_305348006 ("305348006", "Admission to care of the elderly day hospital"),
	
	/**
	 * Admission to department
	 */
	_305349003 ("305349003", "Admission to department"),
	
	/**
	 * Admission to anaesthetic department
	 */
	_305350003 ("305350003", "Admission to anaesthetic department"),
	
	/**
	 * Admission to intensive care unit
	 */
	_305351004 ("305351004", "Admission to intensive care unit"),
	
	/**
	 * Admission to adult intensive care unit
	 */
	_305352006 ("305352006", "Admission to adult intensive care unit"),
	
	/**
	 * Admission to paediatric intensive care unit
	 */
	_305353001 ("305353001", "Admission to paediatric intensive care unit"),
	
	/**
	 * Admission to medical department
	 */
	_305354007 ("305354007", "Admission to medical department"),
	
	/**
	 * Admission to clinical allergy department
	 */
	_305355008 ("305355008", "Admission to clinical allergy department"),
	
	/**
	 * Admission to audiology department
	 */
	_305356009 ("305356009", "Admission to audiology department"),
	
	/**
	 * Admission to cardiology department
	 */
	_305357000 ("305357000", "Admission to cardiology department"),
	
	/**
	 * Admission to chest medicine department
	 */
	_305358005 ("305358005", "Admission to chest medicine department"),
	
	/**
	 * Admission to thoracic medicine department
	 */
	_305359002 ("305359002", "Admission to thoracic medicine department"),
	
	/**
	 * Admission to respiratory medicine department
	 */
	_305360007 ("305360007", "Admission to respiratory medicine department"),
	
	/**
	 * Admission to clinical immunology department
	 */
	_305361006 ("305361006", "Admission to clinical immunology department"),
	
	/**
	 * Admission to clinical neurophysiology department
	 */
	_305362004 ("305362004", "Admission to clinical neurophysiology department"),
	
	/**
	 * Admission to clinical pharmacology department
	 */
	_305363009 ("305363009", "Admission to clinical pharmacology department"),
	
	/**
	 * Admission to clinical physiology department
	 */
	_305364003 ("305364003", "Admission to clinical physiology department"),
	
	/**
	 * Admission to dermatology department
	 */
	_305365002 ("305365002", "Admission to dermatology department"),
	
	/**
	 * Admission to endocrinology department
	 */
	_305366001 ("305366001", "Admission to endocrinology department"),
	
	/**
	 * Admission to gastroenterology department
	 */
	_305367005 ("305367005", "Admission to gastroenterology department"),
	
	/**
	 * Admission to general medical department
	 */
	_305368000 ("305368000", "Admission to general medical department"),
	
	/**
	 * Admission to genetics department
	 */
	_305369008 ("305369008", "Admission to genetics department"),
	
	/**
	 * Admission to clinical genetics department
	 */
	_305370009 ("305370009", "Admission to clinical genetics department"),
	
	/**
	 * Admission to clinical cytogenetics department
	 */
	_305371008 ("305371008", "Admission to clinical cytogenetics department"),
	
	/**
	 * Admission to clinical molecular genetics department
	 */
	_305372001 ("305372001", "Admission to clinical molecular genetics department"),
	
	/**
	 * Admission to genitourinary medicine department
	 */
	_305374000 ("305374000", "Admission to genitourinary medicine department"),
	
	/**
	 * Admission to care of the elderly department
	 */
	_305375004 ("305375004", "Admission to care of the elderly department"),
	
	/**
	 * Admission to infectious diseases department
	 */
	_305376003 ("305376003", "Admission to infectious diseases department"),
	
	/**
	 * Admission to medical ophthalmology department
	 */
	_305377007 ("305377007", "Admission to medical ophthalmology department"),
	
	/**
	 * Admission to nephrology department
	 */
	_305378002 ("305378002", "Admission to nephrology department"),
	
	/**
	 * Admission to neurology department
	 */
	_305379005 ("305379005", "Admission to neurology department"),
	
	/**
	 * Admission to nuclear medicine department
	 */
	_305380008 ("305380008", "Admission to nuclear medicine department"),
	
	/**
	 * Admission to palliative care department
	 */
	_305381007 ("305381007", "Admission to palliative care department"),
	
	/**
	 * Admission to rehabilitation department
	 */
	_305382000 ("305382000", "Admission to rehabilitation department"),
	
	/**
	 * Admission to rheumatology department
	 */
	_305383005 ("305383005", "Admission to rheumatology department"),
	
	/**
	 * Admission to obstetrics and gynaecology department
	 */
	_305384004 ("305384004", "Admission to obstetrics and gynaecology department"),
	
	/**
	 * Admission to gynaecology department
	 */
	_305385003 ("305385003", "Admission to gynaecology department"),
	
	/**
	 * Admission to obstetrics department
	 */
	_305386002 ("305386002", "Admission to obstetrics department"),
	
	/**
	 * Admission to paediatric department
	 */
	_305387006 ("305387006", "Admission to paediatric department"),
	
	/**
	 * Admission to special care baby unit
	 */
	_305388001 ("305388001", "Admission to special care baby unit"),
	
	/**
	 * Admission to paediatric neurology department
	 */
	_305389009 ("305389009", "Admission to paediatric neurology department"),
	
	/**
	 * Admission to paediatric oncology department
	 */
	_305390000 ("305390000", "Admission to paediatric oncology department"),
	
	/**
	 * Admission to pain management department
	 */
	_305391001 ("305391001", "Admission to pain management department"),
	
	/**
	 * Admission to pathology department
	 */
	_305392008 ("305392008", "Admission to pathology department"),
	
	/**
	 * Admission to blood transfusion department
	 */
	_305393003 ("305393003", "Admission to blood transfusion department"),
	
	/**
	 * Admission to chemical pathology department
	 */
	_305394009 ("305394009", "Admission to chemical pathology department"),
	
	/**
	 * Admission to general pathology department
	 */
	_305395005 ("305395005", "Admission to general pathology department"),
	
	/**
	 * Admission to haematology department
	 */
	_305396006 ("305396006", "Admission to haematology department"),
	
	/**
	 * Admission to medical microbiology department
	 */
	_305397002 ("305397002", "Admission to medical microbiology department"),
	
	/**
	 * Admission to the mortuary
	 */
	_305398007 ("305398007", "Admission to the mortuary"),
	
	/**
	 * Admission to neuropathology department
	 */
	_305399004 ("305399004", "Admission to neuropathology department"),
	
	/**
	 * Admission to psychiatry department
	 */
	_305400006 ("305400006", "Admission to psychiatry department"),
	
	/**
	 * Admission to child and adolescent psychiatry department
	 */
	_305401005 ("305401005", "Admission to child and adolescent psychiatry department"),
	
	/**
	 * Admission to forensic psychiatry department
	 */
	_305402003 ("305402003", "Admission to forensic psychiatry department"),
	
	/**
	 * Admission to psychogeriatric department
	 */
	_305403008 ("305403008", "Admission to psychogeriatric department"),
	
	/**
	 * Admission to mental handicap psychiatry department
	 */
	_305404002 ("305404002", "Admission to mental handicap psychiatry department"),
	
	/**
	 * Admission to rehabilitation psychiatry department
	 */
	_305405001 ("305405001", "Admission to rehabilitation psychiatry department"),
	
	/**
	 * Admission to radiology department
	 */
	_305406000 ("305406000", "Admission to radiology department"),
	
	/**
	 * Admission to occupational health department
	 */
	_305407009 ("305407009", "Admission to occupational health department"),
	
	/**
	 * Admission to surgical department
	 */
	_305408004 ("305408004", "Admission to surgical department"),
	
	/**
	 * Admission to breast surgery department
	 */
	_305409007 ("305409007", "Admission to breast surgery department"),
	
	/**
	 * Admission to cardiothoracic surgery department
	 */
	_305410002 ("305410002", "Admission to cardiothoracic surgery department"),
	
	/**
	 * Admission to thoracic surgery department
	 */
	_305411003 ("305411003", "Admission to thoracic surgery department"),
	
	/**
	 * Admission to cardiac surgery department
	 */
	_305412005 ("305412005", "Admission to cardiac surgery department"),
	
	/**
	 * Admission to dental surgery department
	 */
	_305413000 ("305413000", "Admission to dental surgery department"),
	
	/**
	 * Admission to orthodontics department
	 */
	_305414006 ("305414006", "Admission to orthodontics department"),
	
	/**
	 * Admission to paediatric dentistry department
	 */
	_305415007 ("305415007", "Admission to paediatric dentistry department"),
	
	/**
	 * Admission to restorative dentistry department
	 */
	_305416008 ("305416008", "Admission to restorative dentistry department"),
	
	/**
	 * Admission to ear, nose and throat department
	 */
	_305417004 ("305417004", "Admission to ear, nose and throat department"),
	
	/**
	 * Admission to endocrine surgery department
	 */
	_305418009 ("305418009", "Admission to endocrine surgery department"),
	
	/**
	 * Admission to gastrointestinal surgery department
	 */
	_305419001 ("305419001", "Admission to gastrointestinal surgery department"),
	
	/**
	 * Admission to general gastrointestinal surgery department
	 */
	_305420007 ("305420007", "Admission to general gastrointestinal surgery department"),
	
	/**
	 * Admission to upper gastrointestinal surgery department
	 */
	_305421006 ("305421006", "Admission to upper gastrointestinal surgery department"),
	
	/**
	 * Admission to colorectal surgery department
	 */
	_305422004 ("305422004", "Admission to colorectal surgery department"),
	
	/**
	 * Admission to general surgical department
	 */
	_305423009 ("305423009", "Admission to general surgical department"),
	
	/**
	 * Admission to hepatobiliary surgical department
	 */
	_305424003 ("305424003", "Admission to hepatobiliary surgical department"),
	
	/**
	 * Admission to neurosurgical department
	 */
	_305425002 ("305425002", "Admission to neurosurgical department"),
	
	/**
	 * Admission to ophthalmology department
	 */
	_305426001 ("305426001", "Admission to ophthalmology department"),
	
	/**
	 * Admission to oral surgery department
	 */
	_305427005 ("305427005", "Admission to oral surgery department"),
	
	/**
	 * Admission to orthopaedic department
	 */
	_305428000 ("305428000", "Admission to orthopaedic department"),
	
	/**
	 * Admission to pancreatic surgery department
	 */
	_305429008 ("305429008", "Admission to pancreatic surgery department"),
	
	/**
	 * Admission to paediatric surgical department
	 */
	_305430003 ("305430003", "Admission to paediatric surgical department"),
	
	/**
	 * Admission to plastic surgery department
	 */
	_305431004 ("305431004", "Admission to plastic surgery department"),
	
	/**
	 * Admission to surgical transplant department
	 */
	_305432006 ("305432006", "Admission to surgical transplant department"),
	
	/**
	 * Admission to trauma surgery department
	 */
	_305433001 ("305433001", "Admission to trauma surgery department"),
	
	/**
	 * Admission to urology department
	 */
	_305434007 ("305434007", "Admission to urology department"),
	
	/**
	 * Admission to vascular surgery department
	 */
	_305435008 ("305435008", "Admission to vascular surgery department"),
	
	/**
	 * Discharge by health worker
	 */
	_306382007 ("306382007", "Discharge by health worker"),
	
	/**
	 * Discharge by counsellor
	 */
	_306383002 ("306383002", "Discharge by counsellor"),
	
	/**
	 * Discharge by bereavement counsellor
	 */
	_306385009 ("306385009", "Discharge by bereavement counsellor"),
	
	/**
	 * Discharge by genetic counsellor
	 */
	_306386005 ("306386005", "Discharge by genetic counsellor"),
	
	/**
	 * Discharge by marriage guidance counsellor
	 */
	_306387001 ("306387001", "Discharge by marriage guidance counsellor"),
	
	/**
	 * Discharge by mental health counsellor
	 */
	_306388006 ("306388006", "Discharge by mental health counsellor"),
	
	/**
	 * Discharge by Accident and Emergency doctor
	 */
	_306390007 ("306390007", "Discharge by Accident and Emergency doctor"),
	
	/**
	 * Discharge by anaesthetist
	 */
	_306391006 ("306391006", "Discharge by anaesthetist"),
	
	/**
	 * Discharge by clinical oncologist
	 */
	_306392004 ("306392004", "Discharge by clinical oncologist"),
	
	/**
	 * Discharge by radiotherapist
	 */
	_306393009 ("306393009", "Discharge by radiotherapist"),
	
	/**
	 * Discharge by family planning doctor
	 */
	_306394003 ("306394003", "Discharge by family planning doctor"),
	
	/**
	 * Discharge by own GP
	 */
	_306396001 ("306396001", "Discharge by own GP"),
	
	/**
	 * Discharge by partner of GP
	 */
	_306397005 ("306397005", "Discharge by partner of GP"),
	
	/**
	 * Discharge by intensive care specialist
	 */
	_306404005 ("306404005", "Discharge by intensive care specialist"),
	
	/**
	 * Discharge by adult intensive care specialist
	 */
	_306405006 ("306405006", "Discharge by adult intensive care specialist"),
	
	/**
	 * Discharge by paediatric intensive care specialist
	 */
	_306406007 ("306406007", "Discharge by paediatric intensive care specialist"),
	
	/**
	 * Discharge by pain management specialist
	 */
	_306407003 ("306407003", "Discharge by pain management specialist"),
	
	/**
	 * Discharge by pathologist
	 */
	_306408008 ("306408008", "Discharge by pathologist"),
	
	/**
	 * Discharge by blood transfusion doctor
	 */
	_306409000 ("306409000", "Discharge by blood transfusion doctor"),
	
	/**
	 * Discharge by chemical pathologist
	 */
	_306410005 ("306410005", "Discharge by chemical pathologist"),
	
	/**
	 * Discharge by general pathologist
	 */
	_306411009 ("306411009", "Discharge by general pathologist"),
	
	/**
	 * Discharge by haematologist
	 */
	_306412002 ("306412002", "Discharge by haematologist"),
	
	/**
	 * Discharge by immunopathologist
	 */
	_306413007 ("306413007", "Discharge by immunopathologist"),
	
	/**
	 * Discharge by medical microbiologist
	 */
	_306414001 ("306414001", "Discharge by medical microbiologist"),
	
	/**
	 * Discharge by neuropathologist
	 */
	_306415000 ("306415000", "Discharge by neuropathologist"),
	
	/**
	 * Discharge by physician
	 */
	_306416004 ("306416004", "Discharge by physician"),
	
	/**
	 * Discharge by clinical allergist
	 */
	_306417008 ("306417008", "Discharge by clinical allergist"),
	
	/**
	 * Discharge by cardiologist
	 */
	_306418003 ("306418003", "Discharge by cardiologist"),
	
	/**
	 * Discharge by chest physician
	 */
	_306419006 ("306419006", "Discharge by chest physician"),
	
	/**
	 * Discharge by thoracic physician
	 */
	_306420000 ("306420000", "Discharge by thoracic physician"),
	
	/**
	 * Discharge by clinical haematologist
	 */
	_306422008 ("306422008", "Discharge by clinical haematologist"),
	
	/**
	 * Discharge by clinical immunologist
	 */
	_306423003 ("306423003", "Discharge by clinical immunologist"),
	
	/**
	 * Discharge by clinical neurophysiologist
	 */
	_306424009 ("306424009", "Discharge by clinical neurophysiologist"),
	
	/**
	 * Discharge by clinical pharmacologist
	 */
	_306425005 ("306425005", "Discharge by clinical pharmacologist"),
	
	/**
	 * Discharge by clinical physiologist
	 */
	_306426006 ("306426006", "Discharge by clinical physiologist"),
	
	/**
	 * Discharge by dermatologist
	 */
	_306427002 ("306427002", "Discharge by dermatologist"),
	
	/**
	 * Discharge by endocrinologist
	 */
	_306428007 ("306428007", "Discharge by endocrinologist"),
	
	/**
	 * Discharge by gastroenterologist
	 */
	_306429004 ("306429004", "Discharge by gastroenterologist"),
	
	/**
	 * Discharge by general physician
	 */
	_306430009 ("306430009", "Discharge by general physician"),
	
	/**
	 * Discharge by geneticist
	 */
	_306431008 ("306431008", "Discharge by geneticist"),
	
	/**
	 * Discharge by clinical geneticist
	 */
	_306432001 ("306432001", "Discharge by clinical geneticist"),
	
	/**
	 * Discharge by clinical cytogeneticist
	 */
	_306433006 ("306433006", "Discharge by clinical cytogeneticist"),
	
	/**
	 * Discharge by clinical molecular geneticist
	 */
	_306434000 ("306434000", "Discharge by clinical molecular geneticist"),
	
	/**
	 * Discharge by genitourinary medicine physician
	 */
	_306435004 ("306435004", "Discharge by genitourinary medicine physician"),
	
	/**
	 * Discharge by care of the elderly physician
	 */
	_306436003 ("306436003", "Discharge by care of the elderly physician"),
	
	/**
	 * Discharge by infectious diseases physician
	 */
	_306437007 ("306437007", "Discharge by infectious diseases physician"),
	
	/**
	 * Discharge by medical ophthalmologist
	 */
	_306438002 ("306438002", "Discharge by medical ophthalmologist"),
	
	/**
	 * Discharge by nephrologist
	 */
	_306439005 ("306439005", "Discharge by nephrologist"),
	
	/**
	 * Discharge by neurologist
	 */
	_306440007 ("306440007", "Discharge by neurologist"),
	
	/**
	 * Discharge by nuclear medicine physician
	 */
	_306441006 ("306441006", "Discharge by nuclear medicine physician"),
	
	/**
	 * Discharge by palliative care physician
	 */
	_306442004 ("306442004", "Discharge by palliative care physician"),
	
	/**
	 * Discharge by rehabilitation physician
	 */
	_306443009 ("306443009", "Discharge by rehabilitation physician"),
	
	/**
	 * Discharge by rheumatologist
	 */
	_306444003 ("306444003", "Discharge by rheumatologist"),
	
	/**
	 * Discharge by paediatrician
	 */
	_306445002 ("306445002", "Discharge by paediatrician"),
	
	/**
	 * Discharge by community paediatrician
	 */
	_306446001 ("306446001", "Discharge by community paediatrician"),
	
	/**
	 * Discharge by neonatologist
	 */
	_306447005 ("306447005", "Discharge by neonatologist"),
	
	/**
	 * Discharge by paediatric neurologist
	 */
	_306448000 ("306448000", "Discharge by paediatric neurologist"),
	
	/**
	 * Discharge by paediatric oncologist
	 */
	_306449008 ("306449008", "Discharge by paediatric oncologist"),
	
	/**
	 * Discharge by obstetrician and gynaecologist
	 */
	_306450008 ("306450008", "Discharge by obstetrician and gynaecologist"),
	
	/**
	 * Discharge by gynaecologist
	 */
	_306451007 ("306451007", "Discharge by gynaecologist"),
	
	/**
	 * Discharge by obstetrician
	 */
	_306452000 ("306452000", "Discharge by obstetrician"),
	
	/**
	 * Discharge by psychiatrist
	 */
	_306453005 ("306453005", "Discharge by psychiatrist"),
	
	/**
	 * Discharge by child and adolescent psychiatrist
	 */
	_306454004 ("306454004", "Discharge by child and adolescent psychiatrist"),
	
	/**
	 * Discharge by forensic psychiatrist
	 */
	_306455003 ("306455003", "Discharge by forensic psychiatrist"),
	
	/**
	 * Discharge by liaison psychiatrist
	 */
	_306456002 ("306456002", "Discharge by liaison psychiatrist"),
	
	/**
	 * Discharge by psychogeriatrician
	 */
	_306457006 ("306457006", "Discharge by psychogeriatrician"),
	
	/**
	 * Discharge by psychiatrist for mental handicap
	 */
	_306458001 ("306458001", "Discharge by psychiatrist for mental handicap"),
	
	/**
	 * Discharge by rehabilitation psychiatrist
	 */
	_306459009 ("306459009", "Discharge by rehabilitation psychiatrist"),
	
	/**
	 * Discharge by radiologist
	 */
	_306460004 ("306460004", "Discharge by radiologist"),
	
	/**
	 * Discharge by occupational health physician
	 */
	_306461000 ("306461000", "Discharge by occupational health physician"),
	
	/**
	 * Discharge by surgeon
	 */
	_306462007 ("306462007", "Discharge by surgeon"),
	
	/**
	 * Discharge by breast surgeon
	 */
	_306463002 ("306463002", "Discharge by breast surgeon"),
	
	/**
	 * Discharge by cardiothoracic surgeon
	 */
	_306464008 ("306464008", "Discharge by cardiothoracic surgeon"),
	
	/**
	 * Discharge by thoracic surgeon
	 */
	_306465009 ("306465009", "Discharge by thoracic surgeon"),
	
	/**
	 * Discharge by cardiac surgeon
	 */
	_306466005 ("306466005", "Discharge by cardiac surgeon"),
	
	/**
	 * Discharge by dental surgeon
	 */
	_306467001 ("306467001", "Discharge by dental surgeon"),
	
	/**
	 * Discharge by orthodontist
	 */
	_306468006 ("306468006", "Discharge by orthodontist"),
	
	/**
	 * Discharge by paediatric dentist
	 */
	_306469003 ("306469003", "Discharge by paediatric dentist"),
	
	/**
	 * Discharge by restorative dentist
	 */
	_306470002 ("306470002", "Discharge by restorative dentist"),
	
	/**
	 * Discharge by ear, nose and throat surgeon
	 */
	_306471003 ("306471003", "Discharge by ear, nose and throat surgeon"),
	
	/**
	 * Discharge by endocrine surgeon
	 */
	_306472005 ("306472005", "Discharge by endocrine surgeon"),
	
	/**
	 * Discharge by gastrointestinal surgeon
	 */
	_306473000 ("306473000", "Discharge by gastrointestinal surgeon"),
	
	/**
	 * Discharge by general gastrointestinal surgeon
	 */
	_306474006 ("306474006", "Discharge by general gastrointestinal surgeon"),
	
	/**
	 * Discharge by upper gastrointestinal surgeon
	 */
	_306475007 ("306475007", "Discharge by upper gastrointestinal surgeon"),
	
	/**
	 * Discharge by colorectal surgeon
	 */
	_306476008 ("306476008", "Discharge by colorectal surgeon"),
	
	/**
	 * Discharge by general surgeon
	 */
	_306477004 ("306477004", "Discharge by general surgeon"),
	
	/**
	 * Discharge by hand surgeon
	 */
	_306478009 ("306478009", "Discharge by hand surgeon"),
	
	/**
	 * Discharge by hepatobiliary surgeon
	 */
	_306479001 ("306479001", "Discharge by hepatobiliary surgeon"),
	
	/**
	 * Discharge by neurosurgeon
	 */
	_306480003 ("306480003", "Discharge by neurosurgeon"),
	
	/**
	 * Discharge by ophthalmologist
	 */
	_306481004 ("306481004", "Discharge by ophthalmologist"),
	
	/**
	 * Discharge by oral surgeon
	 */
	_306482006 ("306482006", "Discharge by oral surgeon"),
	
	/**
	 * Discharge by orthopaedic surgeon
	 */
	_306483001 ("306483001", "Discharge by orthopaedic surgeon"),
	
	/**
	 * Discharge by paediatric surgeon
	 */
	_306484007 ("306484007", "Discharge by paediatric surgeon"),
	
	/**
	 * Discharge by pancreatic surgeon
	 */
	_306486009 ("306486009", "Discharge by pancreatic surgeon"),
	
	/**
	 * Discharge by plastic surgeon
	 */
	_306487000 ("306487000", "Discharge by plastic surgeon"),
	
	/**
	 * Discharge by transplant surgeon
	 */
	_306488005 ("306488005", "Discharge by transplant surgeon"),
	
	/**
	 * Discharge by trauma surgeon
	 */
	_306489002 ("306489002", "Discharge by trauma surgeon"),
	
	/**
	 * Discharge by urologist
	 */
	_306490006 ("306490006", "Discharge by urologist"),
	
	/**
	 * Discharge by vascular surgeon
	 */
	_306491005 ("306491005", "Discharge by vascular surgeon"),
	
	/**
	 * Discharge by nurse
	 */
	_306492003 ("306492003", "Discharge by nurse"),
	
	/**
	 * Discharge by agency nurse
	 */
	_306493008 ("306493008", "Discharge by agency nurse"),
	
	/**
	 * Discharge by clinical nurse specialist
	 */
	_306494002 ("306494002", "Discharge by clinical nurse specialist"),
	
	/**
	 * Discharge by breast care nurse
	 */
	_306495001 ("306495001", "Discharge by breast care nurse"),
	
	/**
	 * Discharge by cardiac rehabilitation nurse
	 */
	_306496000 ("306496000", "Discharge by cardiac rehabilitation nurse"),
	
	/**
	 * Discharge by contact tracing nurse
	 */
	_306497009 ("306497009", "Discharge by contact tracing nurse"),
	
	/**
	 * Discharge by continence nurse
	 */
	_306498004 ("306498004", "Discharge by continence nurse"),
	
	/**
	 * Discharge by diabetic liaison nurse
	 */
	_306499007 ("306499007", "Discharge by diabetic liaison nurse"),
	
	/**
	 * Discharge by genitourinary nurse
	 */
	_306500003 ("306500003", "Discharge by genitourinary nurse"),
	
	/**
	 * Discharge by lymphoedema care nurse
	 */
	_306501004 ("306501004", "Discharge by lymphoedema care nurse"),
	
	/**
	 * Discharge by nurse behavioural therapist
	 */
	_306502006 ("306502006", "Discharge by nurse behavioural therapist"),
	
	/**
	 * Discharge by nurse psychotherapist
	 */
	_306503001 ("306503001", "Discharge by nurse psychotherapist"),
	
	/**
	 * Discharge by pain management nurse
	 */
	_306504007 ("306504007", "Discharge by pain management nurse"),
	
	/**
	 * Discharge by paediatric nurse
	 */
	_306505008 ("306505008", "Discharge by paediatric nurse"),
	
	/**
	 * Discharge by psychiatric nurse
	 */
	_306506009 ("306506009", "Discharge by psychiatric nurse"),
	
	/**
	 * Discharge by oncology nurse
	 */
	_306507000 ("306507000", "Discharge by oncology nurse"),
	
	/**
	 * Discharge by rheumatology nurse specialist
	 */
	_306508005 ("306508005", "Discharge by rheumatology nurse specialist"),
	
	/**
	 * Discharge by stoma nurse
	 */
	_306509002 ("306509002", "Discharge by stoma nurse"),
	
	/**
	 * Discharge by stomatherapist
	 */
	_306510007 ("306510007", "Discharge by stomatherapist"),
	
	/**
	 * Discharge by community-based nurse
	 */
	_306511006 ("306511006", "Discharge by community-based nurse"),
	
	/**
	 * Discharge by community nurse
	 */
	_306512004 ("306512004", "Discharge by community nurse"),
	
	/**
	 * Discharge by community psychiatric nurse
	 */
	_306513009 ("306513009", "Discharge by community psychiatric nurse"),
	
	/**
	 * Discharge by company nurse
	 */
	_306514003 ("306514003", "Discharge by company nurse"),
	
	/**
	 * Discharge by district nurse
	 */
	_306515002 ("306515002", "Discharge by district nurse"),
	
	/**
	 * Discharge by health visitor
	 */
	_306516001 ("306516001", "Discharge by health visitor"),
	
	/**
	 * Discharge by practice nurse
	 */
	_306517005 ("306517005", "Discharge by practice nurse"),
	
	/**
	 * Discharge by liaison nurse
	 */
	_306518000 ("306518000", "Discharge by liaison nurse"),
	
	/**
	 * Discharge by nurse practitioner
	 */
	_306519008 ("306519008", "Discharge by nurse practitioner"),
	
	/**
	 * Discharge by outreach nurse
	 */
	_306520002 ("306520002", "Discharge by outreach nurse"),
	
	/**
	 * Discharge by research nurse
	 */
	_306521003 ("306521003", "Discharge by research nurse"),
	
	/**
	 * Discharge by midwife
	 */
	_306522005 ("306522005", "Discharge by midwife"),
	
	/**
	 * Discharge by community-based midwife
	 */
	_306523000 ("306523000", "Discharge by community-based midwife"),
	
	/**
	 * Discharge by hospital-based midwife
	 */
	_306524006 ("306524006", "Discharge by hospital-based midwife"),
	
	/**
	 * Discharge by educational psychologist
	 */
	_306525007 ("306525007", "Discharge by educational psychologist"),
	
	/**
	 * Discharge by psychotherapist
	 */
	_306526008 ("306526008", "Discharge by psychotherapist"),
	
	/**
	 * Discharge by professional allied to medicine
	 */
	_306527004 ("306527004", "Discharge by professional allied to medicine"),
	
	/**
	 * Discharge by arts therapist
	 */
	_306528009 ("306528009", "Discharge by arts therapist"),
	
	/**
	 * Discharge by dance therapist
	 */
	_306530006 ("306530006", "Discharge by dance therapist"),
	
	/**
	 * Discharge by drama therapist
	 */
	_306531005 ("306531005", "Discharge by drama therapist"),
	
	/**
	 * Discharge by music therapist
	 */
	_306532003 ("306532003", "Discharge by music therapist"),
	
	/**
	 * Discharge by play therapist
	 */
	_306533008 ("306533008", "Discharge by play therapist"),
	
	/**
	 * Discharge by audiologist
	 */
	_306534002 ("306534002", "Discharge by audiologist"),
	
	/**
	 * Discharge by audiology technician
	 */
	_306535001 ("306535001", "Discharge by audiology technician"),
	
	/**
	 * Discharge by podiatrist
	 */
	_306536000 ("306536000", "Discharge by podiatrist"),
	
	/**
	 * Discharge by community-based podiatrist
	 */
	_306538004 ("306538004", "Discharge by community-based podiatrist"),
	
	/**
	 * Discharge by hospital-based podiatrist
	 */
	_306539007 ("306539007", "Discharge by hospital-based podiatrist"),
	
	/**
	 * Discharge by dietitian
	 */
	_306540009 ("306540009", "Discharge by dietitian"),
	
	/**
	 * Discharge by hospital-based dietitian
	 */
	_306541008 ("306541008", "Discharge by hospital-based dietitian"),
	
	/**
	 * Discharge by community-based dietitian
	 */
	_306542001 ("306542001", "Discharge by community-based dietitian"),
	
	/**
	 * Discharge by occupational therapist
	 */
	_306543006 ("306543006", "Discharge by occupational therapist"),
	
	/**
	 * Discharge by community-based occupational therapist
	 */
	_306544000 ("306544000", "Discharge by community-based occupational therapist"),
	
	/**
	 * Discharge by social services department occupational therapist
	 */
	_306545004 ("306545004", "Discharge by social services department occupational therapist"),
	
	/**
	 * Discharge by hospital-based occupational therapist
	 */
	_306546003 ("306546003", "Discharge by hospital-based occupational therapist"),
	
	/**
	 * Discharge by optometrist
	 */
	_306547007 ("306547007", "Discharge by optometrist"),
	
	/**
	 * Discharge by orthoptist
	 */
	_306548002 ("306548002", "Discharge by orthoptist"),
	
	/**
	 * Discharge by orthotist
	 */
	_306549005 ("306549005", "Discharge by orthotist"),
	
	/**
	 * Discharge by surgical fitter
	 */
	_306550005 ("306550005", "Discharge by surgical fitter"),
	
	/**
	 * Discharge by physiotherapist
	 */
	_306551009 ("306551009", "Discharge by physiotherapist"),
	
	/**
	 * Discharge by community-based physiotherapist
	 */
	_306552002 ("306552002", "Discharge by community-based physiotherapist"),
	
	/**
	 * Discharge by hospital-based physiotherapist
	 */
	_306553007 ("306553007", "Discharge by hospital-based physiotherapist"),
	
	/**
	 * Discharge by radiographer
	 */
	_306554001 ("306554001", "Discharge by radiographer"),
	
	/**
	 * Discharge by diagnostic radiographer
	 */
	_306555000 ("306555000", "Discharge by diagnostic radiographer"),
	
	/**
	 * Discharge by therapeutic radiographer
	 */
	_306556004 ("306556004", "Discharge by therapeutic radiographer"),
	
	/**
	 * Discharge by speech and language therapist
	 */
	_306557008 ("306557008", "Discharge by speech and language therapist"),
	
	/**
	 * Discharge by community-based speech and language therapist
	 */
	_306558003 ("306558003", "Discharge by community-based speech and language therapist"),
	
	/**
	 * Discharge by hospital-based speech and language therapist
	 */
	_306559006 ("306559006", "Discharge by hospital-based speech and language therapist"),
	
	/**
	 * Self-discharge
	 */
	_306560001 ("306560001", "Self-discharge"),
	
	/**
	 * Discharge from establishment
	 */
	_306561002 ("306561002", "Discharge from establishment"),
	
	/**
	 * Discharge from service
	 */
	_306562009 ("306562009", "Discharge from service"),
	
	/**
	 * Discharge from Accident and Emergency service
	 */
	_306563004 ("306563004", "Discharge from Accident and Emergency service"),
	
	/**
	 * Discharge from anaesthetic service
	 */
	_306564005 ("306564005", "Discharge from anaesthetic service"),
	
	/**
	 * Discharge from clinical oncology service
	 */
	_306565006 ("306565006", "Discharge from clinical oncology service"),
	
	/**
	 * Discharge from radiotherapy service
	 */
	_306566007 ("306566007", "Discharge from radiotherapy service"),
	
	/**
	 * Discharge from family planning service
	 */
	_306567003 ("306567003", "Discharge from family planning service"),
	
	/**
	 * Discharge from intensive care service
	 */
	_306568008 ("306568008", "Discharge from intensive care service"),
	
	/**
	 * Discharge from adult intensive care service
	 */
	_306569000 ("306569000", "Discharge from adult intensive care service"),
	
	/**
	 * Discharge from paediatric intensive care service
	 */
	_306570004 ("306570004", "Discharge from paediatric intensive care service"),
	
	/**
	 * Discharge from medical service
	 */
	_306571000 ("306571000", "Discharge from medical service"),
	
	/**
	 * Discharge from clinical allergy service
	 */
	_306572007 ("306572007", "Discharge from clinical allergy service"),
	
	/**
	 * Discharge from audiology service
	 */
	_306573002 ("306573002", "Discharge from audiology service"),
	
	/**
	 * Discharge from cardiology service
	 */
	_306574008 ("306574008", "Discharge from cardiology service"),
	
	/**
	 * Discharge from chest medicine service
	 */
	_306575009 ("306575009", "Discharge from chest medicine service"),
	
	/**
	 * Discharge from respiratory medicine service
	 */
	_306576005 ("306576005", "Discharge from respiratory medicine service"),
	
	/**
	 * Discharge from thoracic medicine service
	 */
	_306577001 ("306577001", "Discharge from thoracic medicine service"),
	
	/**
	 * Discharge from clinical immunology service
	 */
	_306578006 ("306578006", "Discharge from clinical immunology service"),
	
	/**
	 * Discharge from clinical neurophysiology service
	 */
	_306579003 ("306579003", "Discharge from clinical neurophysiology service"),
	
	/**
	 * Discharge from clinical pharmacology service
	 */
	_306580000 ("306580000", "Discharge from clinical pharmacology service"),
	
	/**
	 * Discharge from clinical physiology service
	 */
	_306581001 ("306581001", "Discharge from clinical physiology service"),
	
	/**
	 * Discharge from dermatology service
	 */
	_306582008 ("306582008", "Discharge from dermatology service"),
	
	/**
	 * Discharge from endocrinology service
	 */
	_306583003 ("306583003", "Discharge from endocrinology service"),
	
	/**
	 * Discharge from gastroenterology service
	 */
	_306584009 ("306584009", "Discharge from gastroenterology service"),
	
	/**
	 * Discharge from general medical service
	 */
	_306585005 ("306585005", "Discharge from general medical service"),
	
	/**
	 * Discharge from genetics service
	 */
	_306586006 ("306586006", "Discharge from genetics service"),
	
	/**
	 * Discharge from clinical genetics service
	 */
	_306588007 ("306588007", "Discharge from clinical genetics service"),
	
	/**
	 * Discharge from clinical cytogenetics service
	 */
	_306589004 ("306589004", "Discharge from clinical cytogenetics service"),
	
	/**
	 * Discharge from clinical molecular genetics service
	 */
	_306590008 ("306590008", "Discharge from clinical molecular genetics service"),
	
	/**
	 * Discharge from genitourinary medicine service
	 */
	_306591007 ("306591007", "Discharge from genitourinary medicine service"),
	
	/**
	 * Discharge from care of the elderly service
	 */
	_306592000 ("306592000", "Discharge from care of the elderly service"),
	
	/**
	 * Discharge from infectious diseases service
	 */
	_306593005 ("306593005", "Discharge from infectious diseases service"),
	
	/**
	 * Discharge from nephrology service
	 */
	_306594004 ("306594004", "Discharge from nephrology service"),
	
	/**
	 * Discharge from neurology service
	 */
	_306595003 ("306595003", "Discharge from neurology service"),
	
	/**
	 * Discharge from nuclear medicine service
	 */
	_306596002 ("306596002", "Discharge from nuclear medicine service"),
	
	/**
	 * Discharge from palliative care service
	 */
	_306597006 ("306597006", "Discharge from palliative care service"),
	
	/**
	 * Discharge from rheumatology service
	 */
	_306598001 ("306598001", "Discharge from rheumatology service"),
	
	/**
	 * Discharge from paediatric service
	 */
	_306599009 ("306599009", "Discharge from paediatric service"),
	
	/**
	 * Discharge from community paediatric service
	 */
	_306600007 ("306600007", "Discharge from community paediatric service"),
	
	/**
	 * Discharge from paediatric neurology service
	 */
	_306601006 ("306601006", "Discharge from paediatric neurology service"),
	
	/**
	 * Discharge from paediatric oncology service
	 */
	_306602004 ("306602004", "Discharge from paediatric oncology service"),
	
	/**
	 * Discharge from special care baby service
	 */
	_306603009 ("306603009", "Discharge from special care baby service"),
	
	/**
	 * Discharge from obstetrics and gynaecology service
	 */
	_306604003 ("306604003", "Discharge from obstetrics and gynaecology service"),
	
	/**
	 * Discharge from obstetrics service
	 */
	_306605002 ("306605002", "Discharge from obstetrics service"),
	
	/**
	 * Discharge from gynaecology service
	 */
	_306606001 ("306606001", "Discharge from gynaecology service"),
	
	/**
	 * Discharge from psychiatry service
	 */
	_306607005 ("306607005", "Discharge from psychiatry service"),
	
	/**
	 * Discharge from child and adolescent psychiatry service
	 */
	_306608000 ("306608000", "Discharge from child and adolescent psychiatry service"),
	
	/**
	 * Discharge from forensic psychiatry service
	 */
	_306609008 ("306609008", "Discharge from forensic psychiatry service"),
	
	/**
	 * Discharge from liaison psychiatry service
	 */
	_306610003 ("306610003", "Discharge from liaison psychiatry service"),
	
	/**
	 * Discharge from mental handicap psychiatry service
	 */
	_306611004 ("306611004", "Discharge from mental handicap psychiatry service"),
	
	/**
	 * Discharge from psychogeriatrician service
	 */
	_306612006 ("306612006", "Discharge from psychogeriatrician service"),
	
	/**
	 * Discharge from rehabilitation psychiatry service
	 */
	_306613001 ("306613001", "Discharge from rehabilitation psychiatry service"),
	
	/**
	 * Discharge from pathology service
	 */
	_306614007 ("306614007", "Discharge from pathology service"),
	
	/**
	 * Discharge from blood transfusion service
	 */
	_306615008 ("306615008", "Discharge from blood transfusion service"),
	
	/**
	 * Discharge from chemical pathology service
	 */
	_306616009 ("306616009", "Discharge from chemical pathology service"),
	
	/**
	 * Discharge from haematology service
	 */
	_306617000 ("306617000", "Discharge from haematology service"),
	
	/**
	 * Discharge from pain management service
	 */
	_306618005 ("306618005", "Discharge from pain management service"),
	
	/**
	 * Discharge from occupational health service
	 */
	_306619002 ("306619002", "Discharge from occupational health service"),
	
	/**
	 * Discharge from psychotherapy service
	 */
	_306620008 ("306620008", "Discharge from psychotherapy service"),
	
	/**
	 * Discharge from professionals allied to medicine service
	 */
	_306621007 ("306621007", "Discharge from professionals allied to medicine service"),
	
	/**
	 * Discharge from arts therapy service
	 */
	_306622000 ("306622000", "Discharge from arts therapy service"),
	
	/**
	 * Discharge from podiatry service
	 */
	_306623005 ("306623005", "Discharge from podiatry service"),
	
	/**
	 * Discharge from community podiatry service
	 */
	_306624004 ("306624004", "Discharge from community podiatry service"),
	
	/**
	 * Discharge from hospital podiatry service
	 */
	_306625003 ("306625003", "Discharge from hospital podiatry service"),
	
	/**
	 * Discharge from dietetics service
	 */
	_306626002 ("306626002", "Discharge from dietetics service"),
	
	/**
	 * Discharge from hospital dietetics service
	 */
	_306627006 ("306627006", "Discharge from hospital dietetics service"),
	
	/**
	 * Discharge from community dietetics service
	 */
	_306628001 ("306628001", "Discharge from community dietetics service"),
	
	/**
	 * Discharge from occupational therapy service
	 */
	_306629009 ("306629009", "Discharge from occupational therapy service"),
	
	/**
	 * Discharge from hospital occupational therapy service
	 */
	_306630004 ("306630004", "Discharge from hospital occupational therapy service"),
	
	/**
	 * Discharge from community occupational therapy service
	 */
	_306631000 ("306631000", "Discharge from community occupational therapy service"),
	
	/**
	 * Discharge from orthoptics service
	 */
	_306632007 ("306632007", "Discharge from orthoptics service"),
	
	/**
	 * Discharge from hospital orthoptics service
	 */
	_306633002 ("306633002", "Discharge from hospital orthoptics service"),
	
	/**
	 * Discharge from community orthoptics service
	 */
	_306634008 ("306634008", "Discharge from community orthoptics service"),
	
	/**
	 * Discharge from physiotherapy service
	 */
	_306635009 ("306635009", "Discharge from physiotherapy service"),
	
	/**
	 * Discharge from hospital physiotherapy service
	 */
	_306636005 ("306636005", "Discharge from hospital physiotherapy service"),
	
	/**
	 * Discharge from community physiotherapy service
	 */
	_306637001 ("306637001", "Discharge from community physiotherapy service"),
	
	/**
	 * Discharge from speech and language therapy service
	 */
	_306638006 ("306638006", "Discharge from speech and language therapy service"),
	
	/**
	 * Discharge from hospital speech and language therapy service
	 */
	_306639003 ("306639003", "Discharge from hospital speech and language therapy service"),
	
	/**
	 * Discharge from community speech and language therapy service
	 */
	_306640001 ("306640001", "Discharge from community speech and language therapy service"),
	
	/**
	 * Discharge from orthotics service
	 */
	_306641002 ("306641002", "Discharge from orthotics service"),
	
	/**
	 * Discharge from hospital orthotics service
	 */
	_306642009 ("306642009", "Discharge from hospital orthotics service"),
	
	/**
	 * Discharge from community orthotics service
	 */
	_306643004 ("306643004", "Discharge from community orthotics service"),
	
	/**
	 * Discharge from surgical fitting service
	 */
	_306644005 ("306644005", "Discharge from surgical fitting service"),
	
	/**
	 * Discharge from hospital surgical fitting service
	 */
	_306645006 ("306645006", "Discharge from hospital surgical fitting service"),
	
	/**
	 * Discharge from community surgical fitting service
	 */
	_306646007 ("306646007", "Discharge from community surgical fitting service"),
	
	/**
	 * Discharge from radiology service
	 */
	_306647003 ("306647003", "Discharge from radiology service"),
	
	/**
	 * Discharge from surgical service
	 */
	_306648008 ("306648008", "Discharge from surgical service"),
	
	/**
	 * Discharge from breast surgery service
	 */
	_306649000 ("306649000", "Discharge from breast surgery service"),
	
	/**
	 * Discharge from cardiothoracic surgery service
	 */
	_306650000 ("306650000", "Discharge from cardiothoracic surgery service"),
	
	/**
	 * Discharge from thoracic surgery service
	 */
	_306651001 ("306651001", "Discharge from thoracic surgery service"),
	
	/**
	 * Discharge from cardiac surgery service
	 */
	_306652008 ("306652008", "Discharge from cardiac surgery service"),
	
	/**
	 * Discharge from dental surgery service
	 */
	_306653003 ("306653003", "Discharge from dental surgery service"),
	
	/**
	 * Discharge from orthodontics service
	 */
	_306654009 ("306654009", "Discharge from orthodontics service"),
	
	/**
	 * Discharge from paediatric dentistry service
	 */
	_306655005 ("306655005", "Discharge from paediatric dentistry service"),
	
	/**
	 * Discharge from restorative dentistry service
	 */
	_306656006 ("306656006", "Discharge from restorative dentistry service"),
	
	/**
	 * Discharge from ear, nose and throat service
	 */
	_306657002 ("306657002", "Discharge from ear, nose and throat service"),
	
	/**
	 * Discharge from endocrine surgery service
	 */
	_306658007 ("306658007", "Discharge from endocrine surgery service"),
	
	/**
	 * Discharge from gastrointestinal surgical service
	 */
	_306659004 ("306659004", "Discharge from gastrointestinal surgical service"),
	
	/**
	 * Discharge from general gastrointestinal surgical service
	 */
	_306660009 ("306660009", "Discharge from general gastrointestinal surgical service"),
	
	/**
	 * Discharge from upper gastrointestinal surgical service
	 */
	_306661008 ("306661008", "Discharge from upper gastrointestinal surgical service"),
	
	/**
	 * Discharge from colorectal surgery service
	 */
	_306662001 ("306662001", "Discharge from colorectal surgery service"),
	
	/**
	 * Discharge from general surgical service
	 */
	_306663006 ("306663006", "Discharge from general surgical service"),
	
	/**
	 * Discharge from hand surgery service
	 */
	_306664000 ("306664000", "Discharge from hand surgery service"),
	
	/**
	 * Discharge from neurosurgical service
	 */
	_306665004 ("306665004", "Discharge from neurosurgical service"),
	
	/**
	 * Discharge from ophthalmology service
	 */
	_306666003 ("306666003", "Discharge from ophthalmology service"),
	
	/**
	 * Discharge from oral surgery service
	 */
	_306667007 ("306667007", "Discharge from oral surgery service"),
	
	/**
	 * Discharge from orthopaedic service
	 */
	_306668002 ("306668002", "Discharge from orthopaedic service"),
	
	/**
	 * Discharge from pancreatic surgery service
	 */
	_306669005 ("306669005", "Discharge from pancreatic surgery service"),
	
	/**
	 * Discharge from paediatric surgical service
	 */
	_306670006 ("306670006", "Discharge from paediatric surgical service"),
	
	/**
	 * Discharge from plastic surgery service
	 */
	_306671005 ("306671005", "Discharge from plastic surgery service"),
	
	/**
	 * Discharge from transplant surgery service
	 */
	_306672003 ("306672003", "Discharge from transplant surgery service"),
	
	/**
	 * Discharge from trauma service
	 */
	_306673008 ("306673008", "Discharge from trauma service"),
	
	/**
	 * Discharge from urology service
	 */
	_306674002 ("306674002", "Discharge from urology service"),
	
	/**
	 * Discharge from vascular surgery service
	 */
	_306675001 ("306675001", "Discharge from vascular surgery service"),
	
	/**
	 * Discharge from hospice
	 */
	_306676000 ("306676000", "Discharge from hospice"),
	
	/**
	 * Discharge from day hospital
	 */
	_306677009 ("306677009", "Discharge from day hospital"),
	
	/**
	 * Discharge from psychiatry day hospital
	 */
	_306678004 ("306678004", "Discharge from psychiatry day hospital"),
	
	/**
	 * Discharge from psychogeriatric day hospital
	 */
	_306679007 ("306679007", "Discharge from psychogeriatric day hospital"),
	
	/**
	 * Discharge from care of the elderly day hospital
	 */
	_306680005 ("306680005", "Discharge from care of the elderly day hospital"),
	
	/**
	 * Discharge from hospice day hospital
	 */
	_306681009 ("306681009", "Discharge from hospice day hospital"),
	
	/**
	 * Discharge from ward
	 */
	_306682002 ("306682002", "Discharge from ward"),
	
	/**
	 * Discharge from day ward
	 */
	_306683007 ("306683007", "Discharge from day ward"),
	
	/**
	 * Case closure by social services department
	 */
	_306684001 ("306684001", "Case closure by social services department"),
	
	/**
	 * Discharge to establishment
	 */
	_306685000 ("306685000", "Discharge to establishment"),
	
	/**
	 * Discharge to home
	 */
	_306689006 ("306689006", "Discharge to home"),
	
	/**
	 * Discharge to relatives home
	 */
	_306690002 ("306690002", "Discharge to relative's home"),
	
	/**
	 * Discharge to residential home
	 */
	_306691003 ("306691003", "Discharge to residential home"),
	
	/**
	 * Discharge to private residential home
	 */
	_306692005 ("306692005", "Discharge to private residential home"),
	
	/**
	 * Discharge to part III residential home
	 */
	_306693000 ("306693000", "Discharge to part III residential home"),
	
	/**
	 * Discharge to nursing home
	 */
	_306694006 ("306694006", "Discharge to nursing home"),
	
	/**
	 * Discharge to private nursing home
	 */
	_306695007 ("306695007", "Discharge to private nursing home"),
	
	/**
	 * Discharge to part III accommodation
	 */
	_306696008 ("306696008", "Discharge to part III accommodation"),
	
	/**
	 * Discharge to sheltered housing
	 */
	_306697004 ("306697004", "Discharge to sheltered housing"),
	
	/**
	 * Discharge to warden controlled accommodation
	 */
	_306698009 ("306698009", "Discharge to warden controlled accommodation"),
	
	/**
	 * Discharge to hospital
	 */
	_306699001 ("306699001", "Discharge to hospital"),
	
	/**
	 * Discharge to long stay hospital
	 */
	_306700000 ("306700000", "Discharge to long stay hospital"),
	
	/**
	 * Discharge to community hospital
	 */
	_306701001 ("306701001", "Discharge to community hospital"),
	
	/**
	 * Discharge to tertiary referral hospital
	 */
	_306703003 ("306703003", "Discharge to tertiary referral hospital"),
	
	/**
	 * Discharge to police custody
	 */
	_306705005 ("306705005", "Discharge to police custody"),
	
	/**
	 * Discharge to ward
	 */
	_306706006 ("306706006", "Discharge to ward"),
	
	/**
	 * Discharge to day ward
	 */
	_306707002 ("306707002", "Discharge to day ward"),
	
	/**
	 * Case closure by social worker
	 */
	_306729003 ("306729003", "Case closure by social worker"),
	
	/**
	 * Admission by general dental surgeon
	 */
	_306731007 ("306731007", "Admission by general dental surgeon"),
	
	/**
	 * Admission to general dental surgery department
	 */
	_306732000 ("306732000", "Admission to general dental surgery department"),
	
	/**
	 * Discharge by general dental surgeon
	 */
	_306733005 ("306733005", "Discharge by general dental surgeon"),
	
	/**
	 * Admission to stroke unit
	 */
	_306803007 ("306803007", "Admission to stroke unit"),
	
	/**
	 * Admission to young disabled unit
	 */
	_306804001 ("306804001", "Admission to young disabled unit"),
	
	/**
	 * Discharge from stroke service
	 */
	_306808003 ("306808003", "Discharge from stroke service"),
	
	/**
	 * Discharge from young disabled service
	 */
	_306809006 ("306809006", "Discharge from young disabled service"),
	
	/**
	 * Discharge by audiological physician
	 */
	_306858005 ("306858005", "Discharge by audiological physician"),
	
	/**
	 * Discharge from dance therapy service
	 */
	_306862004 ("306862004", "Discharge from dance therapy service"),
	
	/**
	 * Discharge from drama therapy service
	 */
	_306863009 ("306863009", "Discharge from drama therapy service"),
	
	/**
	 * Discharge from music therapy service
	 */
	_306864003 ("306864003", "Discharge from music therapy service"),
	
	/**
	 * Admission to hand surgery department
	 */
	_306967009 ("306967009", "Admission to hand surgery department"),
	
	/**
	 * Admission by clinical haematologist
	 */
	_307064007 ("307064007", "Admission by clinical haematologist"),
	
	/**
	 * Discharge from head injury rehabilitation service
	 */
	_307382004 ("307382004", "Discharge from head injury rehabilitation service"),
	
	/**
	 * Discharge from community rehabilitation service
	 */
	_307383009 ("307383009", "Discharge from community rehabilitation service"),
	
	/**
	 * Discharge by person
	 */
	_307838002 ("307838002", "Discharge by person"),
	
	/**
	 * Discharge from rehabilitation service
	 */
	_308018004 ("308018004", "Discharge from rehabilitation service"),
	
	/**
	 * Admission to clinical oncology department
	 */
	_308251003 ("308251003", "Admission to clinical oncology department"),
	
	/**
	 * Admission to radiotherapy department
	 */
	_308252005 ("308252005", "Admission to radiotherapy department"),
	
	/**
	 * Admission to diabetic department
	 */
	_308253000 ("308253000", "Admission to diabetic department"),
	
	/**
	 * Discharge from hospital
	 */
	_308283009 ("308283009", "Discharge from hospital"),
	
	/**
	 * Delayed discharge - social services
	 */
	_309568009 ("309568009", "Delayed discharge - social services"),
	
	/**
	 * Non-urgent cardiological admission
	 */
	_310361003 ("310361003", "Non-urgent cardiological admission"),
	
	/**
	 * Discharge by medical oncologist
	 */
	_310513006 ("310513006", "Discharge by medical oncologist"),
	
	/**
	 * Admission by medical oncologist
	 */
	_310518002 ("310518002", "Admission by medical oncologist"),
	
	/**
	 * Admission for social reasons
	 */
	_313331005 ("313331005", "Admission for social reasons"),
	
	/**
	 * Admit cardiology emergency
	 */
	_313385005 ("313385005", "Admit cardiology emergency"),
	
	/**
	 * Unexpected admission to high dependency unit
	 */
	_397769005 ("397769005", "Unexpected admission to high dependency unit"),
	
	/**
	 * Unexpected admission to intensive care unit
	 */
	_397945004 ("397945004", "Unexpected admission to intensive care unit"),
	
	/**
	 * Admission to high dependency unit
	 */
	_398162007 ("398162007", "Admission to high dependency unit"),
	
	/**
	 * Unexpected hospital admission
	 */
	_405614004 ("405614004", "Unexpected hospital admission"),
	
	/**
	 * Discharge to convalescence
	 */
	_405807003 ("405807003", "Discharge to convalescence"),
	
	/**
	 * Admit respiratory emergency
	 */
	_408489005 ("408489005", "Admit respiratory emergency"),
	
	/**
	 * Admit COPD emergency
	 */
	_408501008 ("408501008", "Admit COPD emergency"),
	
	/**
	 * Discharge from learning disability team
	 */
	_413128002 ("413128002", "Discharge from learning disability team"),
	
	/**
	 * Admit heart failure emergency
	 */
	_416683003 ("416683003", "Admit heart failure emergency"),
	
	/**
	 * Admission from
	 */
	_427675001 ("427675001", "Admission from"),
	
	/**
	 * Discharge from intermediate care
	 */
	_25131000000105 ("25131000000105", "Discharge from intermediate care"),
	
	/**
	 * Discharge from care
	 */
	_25241000000106 ("25241000000106", "Discharge from care"),
	
	/**
	 * Admission to high dependency unit or intensive therapy unit following anaesthetic adverse event
	 */
	_84901000000108 ("84901000000108", "Admission to high dependency unit or intensive therapy unit following anaesthetic adverse event"),
	
	/**
	 * Discharge from care against professional advice
	 */
	_88861000000105 ("88861000000105", "Discharge from care against professional advice"),
	
	/**
	 * Discharge from care in accordance with professional advice
	 */
	_88881000000101 ("88881000000101", "Discharge from care in accordance with professional advice"),
	
	/**
	 * Admission to regional secure unit
	 */
	_91981000000104 ("91981000000104", "Admission to regional secure unit"),
	
	/**
	 * Admission to medium secure unit
	 */
	_91991000000102 ("91991000000102", "Admission to medium secure unit"),
	
	/**
	 * Admission to psychiatric intensive care unit
	 */
	_92231000000101 ("92231000000101", "Admission to psychiatric intensive care unit"),
	
	/**
	 * Admission to high secure unit
	 */
	_108171000000102 ("108171000000102", "Admission to high secure unit"),
	
	/**
	 * Admission to mental health specialist services
	 */
	_163781000000106 ("163781000000106", "Admission to mental health specialist services"),
	
	/**
	 * Discharge from practice nurse heart failure clinic
	 */
	_200201000000101 ("200201000000101", "Discharge from practice nurse heart failure clinic"),
	
	/**
	 * Antenatal admission
	 */
	_203221000000100 ("203221000000100", "Antenatal admission"),
	
	/**
	 * Admission under the Mental Health Act
	 */
	_242171000000106 ("242171000000106", "Admission under the Mental Health Act"),
	
	/**
	 * Admission by co-operative general practitioner
	 */
	_388751000000101 ("388751000000101", "Admission by co-operative general practitioner"),
	
	/**
	 * Admission by deputising general practitioner
	 */
	_395581000000108 ("395581000000108", "Admission by deputising general practitioner"),
	
	/**
	 * Emergency psychiatric admission under Mental Health Act 1983 England and Wales
	 */
	_395661000000100 ("395661000000100", "Emergency psychiatric admission under Mental Health Act 1983 (England and Wales)"),
	
	/**
	 * Discharge by general practice registrar
	 */
	_401751000000103 ("401751000000103", "Discharge by general practice registrar"),
	
	/**
	 * Discharge by co-operative GP general practitioner
	 */
	_424191000000103 ("424191000000103", "Discharge by co-operative GP (general practitioner)"),
	
	/**
	 * Admission by GP general practitioner registrar
	 */
	_424211000000104 ("424211000000104", "Admission by GP (general practitioner) registrar"),
	
	/**
	 * Discharge by deputising general practitioner
	 */
	_431341000000109 ("431341000000109", "Discharge by deputising general practitioner"),
	
	/**
	 * Admission by associate GP general practitioner
	 */
	_437171000000107 ("437171000000107", "Admission by associate GP (general practitioner)"),
	
	/**
	 * Discharge by associate general practitioner
	 */
	_443291000000109 ("443291000000109", "Discharge by associate general practitioner"),
	
	/**
	 * Admission by assistant GP general practitioner
	 */
	_449201000000100 ("449201000000100", "Admission by assistant GP (general practitioner)"),
	
	/**
	 * Discharge by assistant general practitioner
	 */
	_455271000000109 ("455271000000109", "Discharge by assistant general practitioner"),
	
	/**
	 * Discharge by locum general practitioner
	 */
	_466881000000106 ("466881000000106", "Discharge by locum general practitioner"),
	
	/**
	 * Admission by locum general practitioner
	 */
	_476981000000106 ("476981000000106", "Admission by locum general practitioner"),
	
	/**
	 * Discharge from primary healthcare team
	 */
	_782351000000102 ("782351000000102", "Discharge from primary healthcare team"),
	
	/**
	 * Discharge from care following non-attendance
	 */
	_790241000000105 ("790241000000105", "Discharge from care following non-attendance"),
	
	/**
	 * Discharge from community specialist palliative care
	 */
	_818361000000102 ("818361000000102", "Discharge from community specialist palliative care"),
	
	/**
	 * Discharge from advanced primary nurse care
	 */
	_818941000000102 ("818941000000102", "Discharge from advanced primary nurse care"),
	
	/**
	 * 111 contact disposition to general practitioner
	 */
	_864811000000104 ("864811000000104", "111 contact disposition to general practitioner"),
	
	/**
	 * 111 contact disposition to pharmacist
	 */
	_864831000000107 ("864831000000107", "111 contact disposition to pharmacist"),
	
	/**
	 * 111 contact disposition to out of hours service
	 */
	_864851000000100 ("864851000000100", "111 contact disposition to out of hours service"),
	
	/**
	 * 111 contact disposition to community service
	 */
	_864871000000109 ("864871000000109", "111 contact disposition to community service"),
	
	/**
	 * 111 contact disposition to mental health service
	 */
	_864891000000108 ("864891000000108", "111 contact disposition to mental health service"),
	
	/**
	 * 111 contact disposition to Social Services
	 */
	_864911000000106 ("864911000000106", "111 contact disposition to Social Services"),
	
	/**
	 * 111 contact disposition to 999 transfer
	 */
	_864931000000103 ("864931000000103", "111 contact disposition to 999 transfer"),
	
	/**
	 * 111 contact disposition to accident and emergency department
	 */
	_864951000000105 ("864951000000105", "111 contact disposition to accident and emergency department"),
	
	/**
	 * Hospital re-admission
	 */
	_Hospitalreadmission ("417005", "Hospital re-admission"),
	
	/**
	 * Hospital admission for isolation
	 */
	_Hospitaladmissionforisolation ("1505002", "Hospital admission for isolation"),
	
	/**
	 * Patient discharge, deceased, medicolegal case
	 */
	_Patientdischargedeceasedmedicolegalcase ("1917008", "Patient discharge, deceased, medicolegal case"),
	
	/**
	 * Hospital admission, urgent, 48 hours
	 */
	_Hospitaladmissionurgent48hours ("2252009", "Hospital admission, urgent, 48 hours"),
	
	/**
	 * Hospital admission, type unclassified, explain by report
	 */
	_Hospitaladmissiontypeunclassifiedexplainbyreport ("2876009", "Hospital admission, type unclassified, explain by report"),
	
	/**
	 * Patient discharge, deceased, autopsy
	 */
	_Patientdischargedeceasedautopsy ("3133002", "Patient discharge, deceased, autopsy"),
	
	/**
	 * Routine patient disposition, no follow-up planned
	 */
	_Routinepatientdispositionnofollowupplanned ("3780001", "Routine patient disposition, no follow-up planned"),
	
	/**
	 * Hospital admission, transfer from other hospital or health care facility
	 */
	_Hospitaladmissiontransferfromotherhospitalorhealthcarefacility ("4563007", "Hospital admission, transfer from other hospital or health care facility"),
	
	/**
	 * Speciality clinic admission
	 */
	_Specialityclinicadmission ("5161006", "Speciality clinic admission"),
	
	/**
	 * Hospital admission, elective
	 */
	_Hospitaladmissionelective ("8715000", "Hospital admission, elective"),
	
	/**
	 * Patient discharge, to home, with assistance
	 */
	_Patientdischargetohomewithassistance ("10161009", "Patient discharge, to home, with assistance"),
	
	/**
	 * Hospital admission, emergency, from emergency room, accidental injury
	 */
	_Hospitaladmissionemergencyfromemergencyroomaccidentalinjury ("10378005", "Hospital admission, emergency, from emergency room, accidental injury"),
	
	/**
	 * Emergency room admission, dead on arrival DOA
	 */
	_EmergencyroomadmissiondeadonarrivalDOA ("11545006", "Emergency room admission, dead on arrival (DOA)"),
	
	/**
	 * Patient discharge, escaped from custody
	 */
	_Patientdischargeescapedfromcustody ("12616003", "Patient discharge, escaped from custody"),
	
	/**
	 * Hospital admission, elective, with partial pre-admission work-up
	 */
	_Hospitaladmissionelectivewithpartialpreadmissionworkup ("15584006", "Hospital admission, elective, with partial pre-admission work-up"),
	
	/**
	 * Hospital admission, emergency, indirect
	 */
	_Hospitaladmissionemergencyindirect ("18083007", "Hospital admission, emergency, indirect"),
	
	/**
	 * Hospital admission, emergency, from emergency room, medical nature
	 */
	_Hospitaladmissionemergencyfromemergencyroommedicalnature ("19951005", "Hospital admission, emergency, from emergency room, medical nature"),
	
	/**
	 * Patient on pass
	 */
	_Patientonpass ("20541009", "Patient on pass"),
	
	/**
	 * Hospital admission, for research investigation
	 */
	_Hospitaladmissionforresearchinvestigation ("23473000", "Hospital admission, for research investigation"),
	
	/**
	 * Hospital admission, under police custody
	 */
	_Hospitaladmissionunderpolicecustody ("25986004", "Hospital admission, under police custody"),
	
	/**
	 * Hospital admission
	 */
	_Hospitaladmission ("32485007", "Hospital admission"),
	
	/**
	 * Patient discharge, elopement
	 */
	_Patientdischargeelopement ("34596002", "Patient discharge, elopement"),
	
	/**
	 * Hospital admission, pre-nursing home placement
	 */
	_Hospitaladmissionprenursinghomeplacement ("36723004", "Hospital admission, pre-nursing home placement"),
	
	/**
	 * General outpatient clinic admission
	 */
	_Generaloutpatientclinicadmission ("40274000", "General outpatient clinic admission"),
	
	/**
	 * Patient discharge, deceased, donation of body
	 */
	_Patientdischargedeceaseddonationofbody ("41819004", "Patient discharge, deceased, donation of body"),
	
	/**
	 * Hospital admission, precertified by medical audit action
	 */
	_Hospitaladmissionprecertifiedbymedicalauditaction ("45702004", "Hospital admission, precertified by medical audit action"),
	
	/**
	 * Patient discharge, type unclassified, explain by report
	 */
	_Patientdischargetypeunclassifiedexplainbyreport ("47031007", "Patient discharge, type unclassified, explain by report"),
	
	/**
	 * Hospital admission, mother, for observation, delivered outside of hospital
	 */
	_Hospitaladmissionmotherforobservationdeliveredoutsideofhospital ("47348005", "Hospital admission, mother, for observation, delivered outside of hospital"),
	
	/**
	 * Hospital admission, special
	 */
	_Hospitaladmissionspecial ("48183000", "Hospital admission, special"),
	
	/**
	 * Emergency room admission, followed by release
	 */
	_Emergencyroomadmissionfollowedbyrelease ("50331008", "Emergency room admission, followed by release"),
	
	/**
	 * Hospital admission, short-term
	 */
	_Hospitaladmissionshortterm ("50699000", "Hospital admission, short-term"),
	
	/**
	 * Emergency room admission
	 */
	_Emergencyroomadmission ("50849002", "Emergency room admission"),
	
	/**
	 * Patient discharge, to legal custody
	 */
	_Patientdischargetolegalcustody ("50861005", "Patient discharge, to legal custody"),
	
	/**
	 * Hospital admission, donor for transplant organ
	 */
	_Hospitaladmissiondonorfortransplantorgan ("51032003", "Hospital admission, donor for transplant organ"),
	
	/**
	 * Hospital admission, parent, for in-hospital child care
	 */
	_Hospitaladmissionparentforinhospitalchildcare ("51501005", "Hospital admission, parent, for in-hospital child care"),
	
	/**
	 * Hospital admission, involuntary
	 */
	_Hospitaladmissioninvoluntary ("52748007", "Hospital admission, involuntary"),
	
	/**
	 * Hospital admission, for laboratory work-up, radiography, etc
	 */
	_Hospitaladmissionforlaboratoryworkupradiographyetc ("55402005", "Hospital admission, for laboratory work-up, radiography, etc."),
	
	/**
	 * Patient discharge
	 */
	_Patientdischarge ("58000006", "Patient discharge"),
	
	/**
	 * Hospital admission, infant, for observation, delivered outside of hospital
	 */
	_Hospitaladmissioninfantforobservationdeliveredoutsideofhospital ("60059000", "Hospital admission, infant, for observation, delivered outside of hospital"),
	
	/**
	 * Patient follow-up planned and scheduled
	 */
	_Patientfollowupplannedandscheduled ("61342007", "Patient follow-up planned and scheduled"),
	
	/**
	 * Hospital admission, from remote area, by means of special transportation
	 */
	_Hospitaladmissionfromremoteareabymeansofspecialtransportation ("63551005", "Hospital admission, from remote area, by means of special transportation"),
	
	/**
	 * Hospital admission, short-term, day care
	 */
	_Hospitaladmissionshorttermdaycare ("65043002", "Hospital admission, short-term, day care"),
	
	/**
	 * Patient discharge, to home, routine
	 */
	_Patientdischargetohomeroutine ("65537008", "Patient discharge, to home, routine"),
	
	/**
	 * Hospital admission, by legal authority commitment
	 */
	_Hospitaladmissionbylegalauthoritycommitment ("70755000", "Hospital admission, by legal authority (commitment)"),
	
	/**
	 * Hospital admission, limited to designated procedures
	 */
	_Hospitaladmissionlimitedtodesignatedprocedures ("71290004", "Hospital admission, limited to designated procedures"),
	
	/**
	 * Hospital admission, emergency, from emergency room
	 */
	_Hospitaladmissionemergencyfromemergencyroom ("73607007", "Hospital admission, emergency, from emergency room"),
	
	/**
	 * Hospital admission, short-term, 24 hours
	 */
	_Hospitaladmissionshortterm24hours ("74857009", "Hospital admission, short-term, 24 hours"),
	
	/**
	 * Emergency room admission, died in emergency room
	 */
	_Emergencyroomadmissiondiedinemergencyroom ("75004002", "Emergency room admission, died in emergency room"),
	
	/**
	 * Hospital admission, for observation
	 */
	_Hospitaladmissionforobservation ("76464004", "Hospital admission, for observation"),
	
	/**
	 * Hospital admission, emergency, direct
	 */
	_Hospitaladmissionemergencydirect ("78680009", "Hospital admission, emergency, direct"),
	
	/**
	 * Patient discharge, deceased, no autopsy
	 */
	_Patientdischargedeceasednoautopsy ("79779006", "Patient discharge, deceased, no autopsy"),
	
	/**
	 * Hospital admission, elective, without pre-admission work-up
	 */
	_Hospitaladmissionelectivewithoutpreadmissionworkup ("81672003", "Hospital admission, elective, without pre-admission work-up"),
	
	/**
	 * Hospital admission, blood donor
	 */
	_Hospitaladmissionblooddonor ("82942009", "Hospital admission, blood donor"),
	
	/**
	 * Patient discharge, to home, ambulatory
	 */
	_Patientdischargetohomeambulatory ("86400002", "Patient discharge, to home, ambulatory"),
	
	/**
	 * Patient discharge, deceased, to anatomic board
	 */
	_Patientdischargedeceasedtoanatomicboard ("89334003", "Patient discharge, deceased, to anatomic board"),
	
	/**
	 * Patient follow-up to return when and if necessary
	 */
	_Patientfollowuptoreturnwhenandifnecessary ("91310009", "Patient follow-up to return when and if necessary"),
	
	/**
	 * Patient disposition
	 */
	_Patientdisposition ("107726003", "Patient disposition"),
	
	/**
	 * Hospital admission, elective, with complete pre-admission work-up
	 */
	_Hospitaladmissionelectivewithcompletepreadmissionworkup ("112689000", "Hospital admission, elective, with complete pre-admission work-up"),
	
	/**
	 * Hospital admission, boarder, for social reasons
	 */
	_Hospitaladmissionboarderforsocialreasons ("112690009", "Hospital admission, boarder, for social reasons"),
	
	/**
	 * Involuntary admission
	 */
	_Involuntaryadmission ("135847007", "Involuntary admission"),
	
	/**
	 * Voluntary admission
	 */
	_Voluntaryadmission ("135848002", "Voluntary admission"),
	
	/**
	 * Admission review
	 */
	_Admissionreview ("177079006", "Admission review"),
	
	/**
	 * Holiday relief admission
	 */
	_Holidayreliefadmission ("183430001", "Holiday relief admission"),
	
	/**
	 * Admission to cardiac intensive care unit
	 */
	_Admissiontocardiacintensivecareunit ("183446009", "Admission to cardiac intensive care unit"),
	
	/**
	 * Admission to respiratory intensive care unit
	 */
	_Admissiontorespiratoryintensivecareunit ("183447000", "Admission to respiratory intensive care unit"),
	
	/**
	 * Admission to neurological intensive care unit
	 */
	_Admissiontoneurologicalintensivecareunit ("183448005", "Admission to neurological intensive care unit"),
	
	/**
	 * Admission to metabolic intensive care unit
	 */
	_Admissiontometabolicintensivecareunit ("183449002", "Admission to metabolic intensive care unit"),
	
	/**
	 * Admission to burn unit
	 */
	_Admissiontoburnunit ("183450002", "Admission to burn unit"),
	
	/**
	 * Emergency hospital admission
	 */
	_Emergencyhospitaladmission ("183452005", "Emergency hospital admission"),
	
	/**
	 * Admit psychiatric emergency
	 */
	_Admitpsychiatricemergency ("183455007", "Admit psychiatric emergency"),
	
	/**
	 * Admit geriatric emergency
	 */
	_Admitgeriatricemergency ("183457004", "Admit geriatric emergency"),
	
	/**
	 * Admit paediatric emergency
	 */
	_Admitpaediatricemergency ("183458009", "Admit paediatric emergency"),
	
	/**
	 * Admit gynaecological emergency
	 */
	_Admitgynaecologicalemergency ("183459001", "Admit gynaecological emergency"),
	
	/**
	 * Admit obstetric emergency
	 */
	_Admitobstetricemergency ("183460006", "Admit obstetric emergency"),
	
	/**
	 * Admit orthopaedic emergency
	 */
	_Admitorthopaedicemergency ("183461005", "Admit orthopaedic emergency"),
	
	/**
	 * Admit ENT emergency
	 */
	_AdmitENTemergency ("183462003", "Admit ENT emergency"),
	
	/**
	 * Admit trauma emergency
	 */
	_Admittraumaemergency ("183463008", "Admit trauma emergency"),
	
	/**
	 * Admit ophthalmological emergency
	 */
	_Admitophthalmologicalemergency ("183464002", "Admit ophthalmological emergency"),
	
	/**
	 * Admit rheumatology emergency
	 */
	_Admitrheumatologyemergency ("183465001", "Admit rheumatology emergency"),
	
	/**
	 * Admit dermatology emergency
	 */
	_Admitdermatologyemergency ("183466000", "Admit dermatology emergency"),
	
	/**
	 * Admit neurology emergency
	 */
	_Admitneurologyemergency ("183467009", "Admit neurology emergency"),
	
	/**
	 * Admit urology emergency
	 */
	_Admiturologyemergency ("183468004", "Admit urology emergency"),
	
	/**
	 * Admit radiotherapy emergency
	 */
	_Admitradiotherapyemergency ("183469007", "Admit radiotherapy emergency"),
	
	/**
	 * Admit haematology emergency
	 */
	_Admithaematologyemergency ("183470008", "Admit haematology emergency"),
	
	/**
	 * Admit plastic surgery emergency
	 */
	_Admitplasticsurgeryemergency ("183471007", "Admit plastic surgery emergency"),
	
	/**
	 * Admit diabetic emergency
	 */
	_Admitdiabeticemergency ("183472000", "Admit diabetic emergency"),
	
	/**
	 * Admit oral surgical emergency
	 */
	_Admitoralsurgicalemergency ("183473005", "Admit oral surgical emergency"),
	
	/**
	 * Admit psychogeriatric emergency
	 */
	_Admitpsychogeriatricemergency ("183474004", "Admit psychogeriatric emergency"),
	
	/**
	 * Admit renal medicine emergency
	 */
	_Admitrenalmedicineemergency ("183475003", "Admit renal medicine emergency"),
	
	/**
	 * Admit neurosurgical emergency
	 */
	_Admitneurosurgicalemergency ("183476002", "Admit neurosurgical emergency"),
	
	/**
	 * Admit cardiothoracic emergency
	 */
	_Admitcardiothoracicemergency ("183477006", "Admit cardiothoracic emergency"),
	
	/**
	 * Emergency admission, asthma
	 */
	_Emergencyadmissionasthma ("183478001", "Emergency admission, asthma"),
	
	/**
	 * Non-urgent hospital admission
	 */
	_Nonurgenthospitaladmission ("183481006", "Non-urgent hospital admission"),
	
	/**
	 * Non-urgent medical admission
	 */
	_Nonurgentmedicaladmission ("183487005", "Non-urgent medical admission"),
	
	/**
	 * Non-urgent surgical admission
	 */
	_Nonurgentsurgicaladmission ("183488000", "Non-urgent surgical admission"),
	
	/**
	 * Non-urgent psychiatric admission
	 */
	_Nonurgentpsychiatricadmission ("183489008", "Non-urgent psychiatric admission"),
	
	/**
	 * Non-urgent geriatric admission
	 */
	_Nonurgentgeriatricadmission ("183491000", "Non-urgent geriatric admission"),
	
	/**
	 * Non-urgent paediatric admission
	 */
	_Nonurgentpaediatricadmission ("183492007", "Non-urgent paediatric admission"),
	
	/**
	 * Non-urgent gynaecological admission
	 */
	_Nonurgentgynaecologicaladmission ("183493002", "Non-urgent gynaecological admission"),
	
	/**
	 * Non-urgent obstetric admission
	 */
	_Nonurgentobstetricadmission ("183494008", "Non-urgent obstetric admission"),
	
	/**
	 * Non-urgent orthopaedic admission
	 */
	_Nonurgentorthopaedicadmission ("183495009", "Non-urgent orthopaedic admission"),
	
	/**
	 * Non-urgent ENT admission
	 */
	_NonurgentENTadmission ("183496005", "Non-urgent ENT admission"),
	
	/**
	 * Non-urgent trauma admission
	 */
	_Nonurgenttraumaadmission ("183497001", "Non-urgent trauma admission"),
	
	/**
	 * Non-urgent ophthalmological admission
	 */
	_Nonurgentophthalmologicaladmission ("183498006", "Non-urgent ophthalmological admission"),
	
	/**
	 * Non-urgent rheumatology admission
	 */
	_Nonurgentrheumatologyadmission ("183499003", "Non-urgent rheumatology admission"),
	
	/**
	 * Non-urgent dermatology admission
	 */
	_Nonurgentdermatologyadmission ("183500007", "Non-urgent dermatology admission"),
	
	/**
	 * Non-urgent neurology admission
	 */
	_Nonurgentneurologyadmission ("183501006", "Non-urgent neurology admission"),
	
	/**
	 * Non-urgent urology admission
	 */
	_Nonurgenturologyadmission ("183502004", "Non-urgent urology admission"),
	
	/**
	 * Non-urgent radiotherapy admission
	 */
	_Nonurgentradiotherapyadmission ("183503009", "Non-urgent radiotherapy admission"),
	
	/**
	 * Non-urgent haematology admission
	 */
	_Nonurgenthaematologyadmission ("183504003", "Non-urgent haematology admission"),
	
	/**
	 * Non-urgent plastic surgery admission
	 */
	_Nonurgentplasticsurgeryadmission ("183505002", "Non-urgent plastic surgery admission"),
	
	/**
	 * Non-urgent diabetic admission
	 */
	_Nonurgentdiabeticadmission ("183506001", "Non-urgent diabetic admission"),
	
	/**
	 * Non-urgent respiratory admission
	 */
	_Nonurgentrespiratoryadmission ("183507005", "Non-urgent respiratory admission"),
	
	/**
	 * Non-urgent psychogeriatric admission
	 */
	_Nonurgentpsychogeriatricadmission ("183508000", "Non-urgent psychogeriatric admission"),
	
	/**
	 * Non-urgent renal medicine admission
	 */
	_Nonurgentrenalmedicineadmission ("183509008", "Non-urgent renal medicine admission"),
	
	/**
	 * Non-urgent neurosurgical admission
	 */
	_Nonurgentneurosurgicaladmission ("183510003", "Non-urgent neurosurgical admission"),
	
	/**
	 * Non-urgent cardiothoracic admission
	 */
	_Nonurgentcardiothoracicadmission ("183511004", "Non-urgent cardiothoracic admission"),
	
	/**
	 * Non-urgent oral surgical admission
	 */
	_Nonurgentoralsurgicaladmission ("183512006", "Non-urgent oral surgical admission"),
	
	/**
	 * Delayed discharge to nursing home
	 */
	_Delayeddischargetonursinghome ("183672007", "Delayed discharge to nursing home"),
	
	/**
	 * Urgent admission to hospice
	 */
	_Urgentadmissiontohospice ("183919006", "Urgent admission to hospice"),
	
	/**
	 * Routine admission to hospice
	 */
	_Routineadmissiontohospice ("183920000", "Routine admission to hospice"),
	
	/**
	 * Admission to hospice for respite
	 */
	_Admissiontohospiceforrespite ("183921001", "Admission to hospice for respite"),
	
	/**
	 * Patient self-discharge
	 */
	_Patientselfdischarge ("183955003", "Patient self-discharge"),
	
	/**
	 * Patient self-discharge against medical advice
	 */
	_Patientselfdischargeagainstmedicaladvice ("225928004", "Patient self-discharge against medical advice"),
	
	/**
	 * Admission for care
	 */
	_Admissionforcare ("281685003", "Admission for care"),
	
	/**
	 * Admission for treatment
	 */
	_Admissionfortreatment ("304566005", "Admission for treatment"),
	
	/**
	 * Admission for long-term care
	 */
	_Admissionforlongtermcare ("304567001", "Admission for long-term care"),
	
	/**
	 * Admission for respite care
	 */
	_Admissionforrespitecare ("304568006", "Admission for respite care"),
	
	/**
	 * Admission procedure
	 */
	_Admissionprocedure ("305056002", "Admission procedure"),
	
	/**
	 * Admission by health worker
	 */
	_Admissionbyhealthworker ("305224000", "Admission by health worker"),
	
	/**
	 * Admission by Accident and Emergency doctor
	 */
	_AdmissionbyAccidentandEmergencydoctor ("305226003", "Admission by Accident and Emergency doctor"),
	
	/**
	 * Admission by anaesthetist
	 */
	_Admissionbyanaesthetist ("305227007", "Admission by anaesthetist"),
	
	/**
	 * Admission by clinical oncologist
	 */
	_Admissionbyclinicaloncologist ("305228002", "Admission by clinical oncologist"),
	
	/**
	 * Admission by radiotherapist
	 */
	_Admissionbyradiotherapist ("305229005", "Admission by radiotherapist"),
	
	/**
	 * Admission by GP
	 */
	_AdmissionbyGP ("305230000", "Admission by GP"),
	
	/**
	 * Admission by own GP
	 */
	_AdmissionbyownGP ("305231001", "Admission by own GP"),
	
	/**
	 * Admission by GP partner
	 */
	_AdmissionbyGPpartner ("305232008", "Admission by GP partner"),
	
	/**
	 * Admission by family planning doctor
	 */
	_Admissionbyfamilyplanningdoctor ("305239004", "Admission by family planning doctor"),
	
	/**
	 * Admission by intensive care specialist
	 */
	_Admissionbyintensivecarespecialist ("305240002", "Admission by intensive care specialist"),
	
	/**
	 * Admission by adult intensive care specialist
	 */
	_Admissionbyadultintensivecarespecialist ("305241003", "Admission by adult intensive care specialist"),
	
	/**
	 * Admission by paediatric intensive care specialist
	 */
	_Admissionbypaediatricintensivecarespecialist ("305242005", "Admission by paediatric intensive care specialist"),
	
	/**
	 * Admission by paediatrician
	 */
	_Admissionbypaediatrician ("305243000", "Admission by paediatrician"),
	
	/**
	 * Admission by community paediatrician
	 */
	_Admissionbycommunitypaediatrician ("305244006", "Admission by community paediatrician"),
	
	/**
	 * Admission by neonatologist
	 */
	_Admissionbyneonatologist ("305245007", "Admission by neonatologist"),
	
	/**
	 * Admission by paediatric neurologist
	 */
	_Admissionbypaediatricneurologist ("305246008", "Admission by paediatric neurologist"),
	
	/**
	 * Admission by paediatric oncologist
	 */
	_Admissionbypaediatriconcologist ("305247004", "Admission by paediatric oncologist"),
	
	/**
	 * Admission by pain management specialist
	 */
	_Admissionbypainmanagementspecialist ("305248009", "Admission by pain management specialist"),
	
	/**
	 * Admission by obstetrician  gynaecologist
	 */
	_Admissionbyobstetriciangynaecologist ("305249001", "Admission by obstetrician & gynaecologist"),
	
	/**
	 * Admission by gynaecologist
	 */
	_Admissionbygynaecologist ("305250001", "Admission by gynaecologist"),
	
	/**
	 * Admission by obstetrician
	 */
	_Admissionbyobstetrician ("305251002", "Admission by obstetrician"),
	
	/**
	 * Admission by occupational health physician
	 */
	_Admissionbyoccupationalhealthphysician ("305252009", "Admission by occupational health physician"),
	
	/**
	 * Admission by pathologist
	 */
	_Admissionbypathologist ("305253004", "Admission by pathologist"),
	
	/**
	 * Admission by blood transfusion doctor
	 */
	_Admissionbybloodtransfusiondoctor ("305254005", "Admission by blood transfusion doctor"),
	
	/**
	 * Admission by chemical pathologist
	 */
	_Admissionbychemicalpathologist ("305255006", "Admission by chemical pathologist"),
	
	/**
	 * Admission by general pathologist
	 */
	_Admissionbygeneralpathologist ("305256007", "Admission by general pathologist"),
	
	/**
	 * Admission by haematologist
	 */
	_Admissionbyhaematologist ("305257003", "Admission by haematologist"),
	
	/**
	 * Admission by medical microbiologist
	 */
	_Admissionbymedicalmicrobiologist ("305258008", "Admission by medical microbiologist"),
	
	/**
	 * Admission by neuropathologist
	 */
	_Admissionbyneuropathologist ("305259000", "Admission by neuropathologist"),
	
	/**
	 * Admission by physician
	 */
	_Admissionbyphysician ("305260005", "Admission by physician"),
	
	/**
	 * Admission by clinical allergist
	 */
	_Admissionbyclinicalallergist ("305261009", "Admission by clinical allergist"),
	
	/**
	 * Admission by cardiologist
	 */
	_Admissionbycardiologist ("305262002", "Admission by cardiologist"),
	
	/**
	 * Admission by care of the elderly physician
	 */
	_Admissionbycareoftheelderlyphysician ("305263007", "Admission by care of the elderly physician"),
	
	/**
	 * Admission by chest physician
	 */
	_Admissionbychestphysician ("305264001", "Admission by chest physician"),
	
	/**
	 * Admission by thoracic physician
	 */
	_Admissionbythoracicphysician ("305265000", "Admission by thoracic physician"),
	
	/**
	 * Admission by respiratory physician
	 */
	_Admissionbyrespiratoryphysician ("305266004", "Admission by respiratory physician"),
	
	/**
	 * Admission by clinical immunologist
	 */
	_Admissionbyclinicalimmunologist ("305267008", "Admission by clinical immunologist"),
	
	/**
	 * Admission by clinical neurophysiologist
	 */
	_Admissionbyclinicalneurophysiologist ("305268003", "Admission by clinical neurophysiologist"),
	
	/**
	 * Admission by clinical pharmacologist
	 */
	_Admissionbyclinicalpharmacologist ("305269006", "Admission by clinical pharmacologist"),
	
	/**
	 * Admission by clinical physiologist
	 */
	_Admissionbyclinicalphysiologist ("305270007", "Admission by clinical physiologist"),
	
	/**
	 * Admission by dermatologist
	 */
	_Admissionbydermatologist ("305271006", "Admission by dermatologist"),
	
	/**
	 * Admission by endocrinologist
	 */
	_Admissionbyendocrinologist ("305272004", "Admission by endocrinologist"),
	
	/**
	 * Admission by gastroenterologist
	 */
	_Admissionbygastroenterologist ("305274003", "Admission by gastroenterologist"),
	
	/**
	 * Admission by general physician
	 */
	_Admissionbygeneralphysician ("305275002", "Admission by general physician"),
	
	/**
	 * Admission by genitourinary medicine physician
	 */
	_Admissionbygenitourinarymedicinephysician ("305276001", "Admission by genitourinary medicine physician"),
	
	/**
	 * Admission by infectious diseases physician
	 */
	_Admissionbyinfectiousdiseasesphysician ("305277005", "Admission by infectious diseases physician"),
	
	/**
	 * Admission by medical ophthalmologist
	 */
	_Admissionbymedicalophthalmologist ("305278000", "Admission by medical ophthalmologist"),
	
	/**
	 * Admission by nephrologist
	 */
	_Admissionbynephrologist ("305279008", "Admission by nephrologist"),
	
	/**
	 * Admission by neurologist
	 */
	_Admissionbyneurologist ("305280006", "Admission by neurologist"),
	
	/**
	 * Admission by nuclear medicine physician
	 */
	_Admissionbynuclearmedicinephysician ("305281005", "Admission by nuclear medicine physician"),
	
	/**
	 * Admission by rheumatologist
	 */
	_Admissionbyrheumatologist ("305282003", "Admission by rheumatologist"),
	
	/**
	 * Admission by rehabilitation physician
	 */
	_Admissionbyrehabilitationphysician ("305283008", "Admission by rehabilitation physician"),
	
	/**
	 * Admission by palliative care physician
	 */
	_Admissionbypalliativecarephysician ("305284002", "Admission by palliative care physician"),
	
	/**
	 * Admission by psychiatrist
	 */
	_Admissionbypsychiatrist ("305285001", "Admission by psychiatrist"),
	
	/**
	 * Admission by child and adolescent psychiatrist
	 */
	_Admissionbychildandadolescentpsychiatrist ("305286000", "Admission by child and adolescent psychiatrist"),
	
	/**
	 * Admission by forensic psychiatrist
	 */
	_Admissionbyforensicpsychiatrist ("305287009", "Admission by forensic psychiatrist"),
	
	/**
	 * Admission by liaison psychiatrist
	 */
	_Admissionbyliaisonpsychiatrist ("305288004", "Admission by liaison psychiatrist"),
	
	/**
	 * Admission by psychogeriatrician
	 */
	_Admissionbypsychogeriatrician ("305289007", "Admission by psychogeriatrician"),
	
	/**
	 * Admission by psychiatrist for mental handicap
	 */
	_Admissionbypsychiatristformentalhandicap ("305290003", "Admission by psychiatrist for mental handicap"),
	
	/**
	 * Admission by rehabilitation psychiatrist
	 */
	_Admissionbyrehabilitationpsychiatrist ("305291004", "Admission by rehabilitation psychiatrist"),
	
	/**
	 * Admission by radiologist
	 */
	_Admissionbyradiologist ("305292006", "Admission by radiologist"),
	
	/**
	 * Admission by surgeon
	 */
	_Admissionbysurgeon ("305293001", "Admission by surgeon"),
	
	/**
	 * Admission by breast surgeon
	 */
	_Admissionbybreastsurgeon ("305294007", "Admission by breast surgeon"),
	
	/**
	 * Admission by cardiothoracic surgeon
	 */
	_Admissionbycardiothoracicsurgeon ("305295008", "Admission by cardiothoracic surgeon"),
	
	/**
	 * Admission by thoracic surgeon
	 */
	_Admissionbythoracicsurgeon ("305296009", "Admission by thoracic surgeon"),
	
	/**
	 * Admission by cardiac surgeon
	 */
	_Admissionbycardiacsurgeon ("305297000", "Admission by cardiac surgeon"),
	
	/**
	 * Admission by dental surgeon
	 */
	_Admissionbydentalsurgeon ("305298005", "Admission by dental surgeon"),
	
	/**
	 * Admission by orthodontist
	 */
	_Admissionbyorthodontist ("305299002", "Admission by orthodontist"),
	
	/**
	 * Admission by paediatric dentist
	 */
	_Admissionbypaediatricdentist ("305300005", "Admission by paediatric dentist"),
	
	/**
	 * Admission by restorative dentist
	 */
	_Admissionbyrestorativedentist ("305301009", "Admission by restorative dentist"),
	
	/**
	 * Admission by ear, nose and throat surgeon
	 */
	_Admissionbyearnoseandthroatsurgeon ("305302002", "Admission by ear, nose and throat surgeon"),
	
	/**
	 * Admission by endocrine surgeon
	 */
	_Admissionbyendocrinesurgeon ("305303007", "Admission by endocrine surgeon"),
	
	/**
	 * Admission by gastrointestinal surgeon
	 */
	_Admissionbygastrointestinalsurgeon ("305304001", "Admission by gastrointestinal surgeon"),
	
	/**
	 * Admission by general gastrointestinal surgeon
	 */
	_Admissionbygeneralgastrointestinalsurgeon ("305305000", "Admission by general gastrointestinal surgeon"),
	
	/**
	 * Admission by upper gastrointestinal surgeon
	 */
	_Admissionbyuppergastrointestinalsurgeon ("305306004", "Admission by upper gastrointestinal surgeon"),
	
	/**
	 * Admission by colorectal surgeon
	 */
	_Admissionbycolorectalsurgeon ("305307008", "Admission by colorectal surgeon"),
	
	/**
	 * Admission by general surgeon
	 */
	_Admissionbygeneralsurgeon ("305308003", "Admission by general surgeon"),
	
	/**
	 * Admission by hand surgeon
	 */
	_Admissionbyhandsurgeon ("305309006", "Admission by hand surgeon"),
	
	/**
	 * Admission by hepatobiliary surgeon
	 */
	_Admissionbyhepatobiliarysurgeon ("305310001", "Admission by hepatobiliary surgeon"),
	
	/**
	 * Admission by neurosurgeon
	 */
	_Admissionbyneurosurgeon ("305311002", "Admission by neurosurgeon"),
	
	/**
	 * Admission by ophthalmologist
	 */
	_Admissionbyophthalmologist ("305312009", "Admission by ophthalmologist"),
	
	/**
	 * Admission by oral surgeon
	 */
	_Admissionbyoralsurgeon ("305313004", "Admission by oral surgeon"),
	
	/**
	 * Admission by orthopaedic surgeon
	 */
	_Admissionbyorthopaedicsurgeon ("305314005", "Admission by orthopaedic surgeon"),
	
	/**
	 * Admission by paediatric surgeon
	 */
	_Admissionbypaediatricsurgeon ("305315006", "Admission by paediatric surgeon"),
	
	/**
	 * Admission by pancreatic surgeon
	 */
	_Admissionbypancreaticsurgeon ("305316007", "Admission by pancreatic surgeon"),
	
	/**
	 * Admission by plastic surgeon
	 */
	_Admissionbyplasticsurgeon ("305317003", "Admission by plastic surgeon"),
	
	/**
	 * Admission by transplant surgeon
	 */
	_Admissionbytransplantsurgeon ("305318008", "Admission by transplant surgeon"),
	
	/**
	 * Admission by trauma surgeon
	 */
	_Admissionbytraumasurgeon ("305319000", "Admission by trauma surgeon"),
	
	/**
	 * Admission by urologist
	 */
	_Admissionbyurologist ("305320006", "Admission by urologist"),
	
	/**
	 * Admission by vascular surgeon
	 */
	_Admissionbyvascularsurgeon ("305321005", "Admission by vascular surgeon"),
	
	/**
	 * Admission by health care assistant
	 */
	_Admissionbyhealthcareassistant ("305322003", "Admission by health care assistant"),
	
	/**
	 * Admission by midwife
	 */
	_Admissionbymidwife ("305323008", "Admission by midwife"),
	
	/**
	 * Admission by community-based midwife
	 */
	_Admissionbycommunitybasedmidwife ("305324002", "Admission by community-based midwife"),
	
	/**
	 * Admission by hospital-based midwife
	 */
	_Admissionbyhospitalbasedmidwife ("305325001", "Admission by hospital-based midwife"),
	
	/**
	 * Admission by nurse
	 */
	_Admissionbynurse ("305326000", "Admission by nurse"),
	
	/**
	 * Admission by agency nurse
	 */
	_Admissionbyagencynurse ("305327009", "Admission by agency nurse"),
	
	/**
	 * Admission by enrolled nurse
	 */
	_Admissionbyenrollednurse ("305328004", "Admission by enrolled nurse"),
	
	/**
	 * Admission by general nurse
	 */
	_Admissionbygeneralnurse ("305329007", "Admission by general nurse"),
	
	/**
	 * Admission by mental health nurse
	 */
	_Admissionbymentalhealthnurse ("305330002", "Admission by mental health nurse"),
	
	/**
	 * Admission by nurse for the mentally handicapped
	 */
	_Admissionbynurseforthementallyhandicapped ("305331003", "Admission by nurse for the mentally handicapped"),
	
	/**
	 * Admission by sick childrens nurse
	 */
	_Admissionbysickchildrensnurse ("305332005", "Admission by sick children's nurse"),
	
	/**
	 * Admission by nurse practitioner
	 */
	_Admissionbynursepractitioner ("305333000", "Admission by nurse practitioner"),
	
	/**
	 * Admission by nursing auxiliary
	 */
	_Admissionbynursingauxiliary ("305334006", "Admission by nursing auxiliary"),
	
	/**
	 * Admission to establishment
	 */
	_Admissiontoestablishment ("305335007", "Admission to establishment"),
	
	/**
	 * Admission to hospice
	 */
	_Admissiontohospice ("305336008", "Admission to hospice"),
	
	/**
	 * Admission to community hospital
	 */
	_Admissiontocommunityhospital ("305337004", "Admission to community hospital"),
	
	/**
	 * Admission to GP hospital
	 */
	_AdmissiontoGPhospital ("305338009", "Admission to GP hospital"),
	
	/**
	 * Admission to private hospital
	 */
	_Admissiontoprivatehospital ("305339001", "Admission to private hospital"),
	
	/**
	 * Admission to long stay hospital
	 */
	_Admissiontolongstayhospital ("305340004", "Admission to long stay hospital"),
	
	/**
	 * Admission to tertiary referral hospital
	 */
	_Admissiontotertiaryreferralhospital ("305341000", "Admission to tertiary referral hospital"),
	
	/**
	 * Admission to ward
	 */
	_Admissiontoward ("305342007", "Admission to ward"),
	
	/**
	 * Admission to day ward
	 */
	_Admissiontodayward ("305343002", "Admission to day ward"),
	
	/**
	 * Admission to day hospital
	 */
	_Admissiontodayhospital ("305344008", "Admission to day hospital"),
	
	/**
	 * Admission to psychiatric day hospital
	 */
	_Admissiontopsychiatricdayhospital ("305345009", "Admission to psychiatric day hospital"),
	
	/**
	 * Admission to psychogeriatric day hospital
	 */
	_Admissiontopsychogeriatricdayhospital ("305346005", "Admission to psychogeriatric day hospital"),
	
	/**
	 * Admission to elderly severely mentally ill day hospital
	 */
	_Admissiontoelderlyseverelymentallyilldayhospital ("305347001", "Admission to elderly severely mentally ill day hospital"),
	
	/**
	 * Admission to care of the elderly day hospital
	 */
	_Admissiontocareoftheelderlydayhospital ("305348006", "Admission to care of the elderly day hospital"),
	
	/**
	 * Admission to department
	 */
	_Admissiontodepartment ("305349003", "Admission to department"),
	
	/**
	 * Admission to anaesthetic department
	 */
	_Admissiontoanaestheticdepartment ("305350003", "Admission to anaesthetic department"),
	
	/**
	 * Admission to intensive care unit
	 */
	_Admissiontointensivecareunit ("305351004", "Admission to intensive care unit"),
	
	/**
	 * Admission to adult intensive care unit
	 */
	_Admissiontoadultintensivecareunit ("305352006", "Admission to adult intensive care unit"),
	
	/**
	 * Admission to paediatric intensive care unit
	 */
	_Admissiontopaediatricintensivecareunit ("305353001", "Admission to paediatric intensive care unit"),
	
	/**
	 * Admission to medical department
	 */
	_Admissiontomedicaldepartment ("305354007", "Admission to medical department"),
	
	/**
	 * Admission to clinical allergy department
	 */
	_Admissiontoclinicalallergydepartment ("305355008", "Admission to clinical allergy department"),
	
	/**
	 * Admission to audiology department
	 */
	_Admissiontoaudiologydepartment ("305356009", "Admission to audiology department"),
	
	/**
	 * Admission to cardiology department
	 */
	_Admissiontocardiologydepartment ("305357000", "Admission to cardiology department"),
	
	/**
	 * Admission to chest medicine department
	 */
	_Admissiontochestmedicinedepartment ("305358005", "Admission to chest medicine department"),
	
	/**
	 * Admission to thoracic medicine department
	 */
	_Admissiontothoracicmedicinedepartment ("305359002", "Admission to thoracic medicine department"),
	
	/**
	 * Admission to respiratory medicine department
	 */
	_Admissiontorespiratorymedicinedepartment ("305360007", "Admission to respiratory medicine department"),
	
	/**
	 * Admission to clinical immunology department
	 */
	_Admissiontoclinicalimmunologydepartment ("305361006", "Admission to clinical immunology department"),
	
	/**
	 * Admission to clinical neurophysiology department
	 */
	_Admissiontoclinicalneurophysiologydepartment ("305362004", "Admission to clinical neurophysiology department"),
	
	/**
	 * Admission to clinical pharmacology department
	 */
	_Admissiontoclinicalpharmacologydepartment ("305363009", "Admission to clinical pharmacology department"),
	
	/**
	 * Admission to clinical physiology department
	 */
	_Admissiontoclinicalphysiologydepartment ("305364003", "Admission to clinical physiology department"),
	
	/**
	 * Admission to dermatology department
	 */
	_Admissiontodermatologydepartment ("305365002", "Admission to dermatology department"),
	
	/**
	 * Admission to endocrinology department
	 */
	_Admissiontoendocrinologydepartment ("305366001", "Admission to endocrinology department"),
	
	/**
	 * Admission to gastroenterology department
	 */
	_Admissiontogastroenterologydepartment ("305367005", "Admission to gastroenterology department"),
	
	/**
	 * Admission to general medical department
	 */
	_Admissiontogeneralmedicaldepartment ("305368000", "Admission to general medical department"),
	
	/**
	 * Admission to genetics department
	 */
	_Admissiontogeneticsdepartment ("305369008", "Admission to genetics department"),
	
	/**
	 * Admission to clinical genetics department
	 */
	_Admissiontoclinicalgeneticsdepartment ("305370009", "Admission to clinical genetics department"),
	
	/**
	 * Admission to clinical cytogenetics department
	 */
	_Admissiontoclinicalcytogeneticsdepartment ("305371008", "Admission to clinical cytogenetics department"),
	
	/**
	 * Admission to clinical molecular genetics department
	 */
	_Admissiontoclinicalmoleculargeneticsdepartment ("305372001", "Admission to clinical molecular genetics department"),
	
	/**
	 * Admission to genitourinary medicine department
	 */
	_Admissiontogenitourinarymedicinedepartment ("305374000", "Admission to genitourinary medicine department"),
	
	/**
	 * Admission to care of the elderly department
	 */
	_Admissiontocareoftheelderlydepartment ("305375004", "Admission to care of the elderly department"),
	
	/**
	 * Admission to infectious diseases department
	 */
	_Admissiontoinfectiousdiseasesdepartment ("305376003", "Admission to infectious diseases department"),
	
	/**
	 * Admission to medical ophthalmology department
	 */
	_Admissiontomedicalophthalmologydepartment ("305377007", "Admission to medical ophthalmology department"),
	
	/**
	 * Admission to nephrology department
	 */
	_Admissiontonephrologydepartment ("305378002", "Admission to nephrology department"),
	
	/**
	 * Admission to neurology department
	 */
	_Admissiontoneurologydepartment ("305379005", "Admission to neurology department"),
	
	/**
	 * Admission to nuclear medicine department
	 */
	_Admissiontonuclearmedicinedepartment ("305380008", "Admission to nuclear medicine department"),
	
	/**
	 * Admission to palliative care department
	 */
	_Admissiontopalliativecaredepartment ("305381007", "Admission to palliative care department"),
	
	/**
	 * Admission to rehabilitation department
	 */
	_Admissiontorehabilitationdepartment ("305382000", "Admission to rehabilitation department"),
	
	/**
	 * Admission to rheumatology department
	 */
	_Admissiontorheumatologydepartment ("305383005", "Admission to rheumatology department"),
	
	/**
	 * Admission to obstetrics and gynaecology department
	 */
	_Admissiontoobstetricsandgynaecologydepartment ("305384004", "Admission to obstetrics and gynaecology department"),
	
	/**
	 * Admission to gynaecology department
	 */
	_Admissiontogynaecologydepartment ("305385003", "Admission to gynaecology department"),
	
	/**
	 * Admission to obstetrics department
	 */
	_Admissiontoobstetricsdepartment ("305386002", "Admission to obstetrics department"),
	
	/**
	 * Admission to paediatric department
	 */
	_Admissiontopaediatricdepartment ("305387006", "Admission to paediatric department"),
	
	/**
	 * Admission to special care baby unit
	 */
	_Admissiontospecialcarebabyunit ("305388001", "Admission to special care baby unit"),
	
	/**
	 * Admission to paediatric neurology department
	 */
	_Admissiontopaediatricneurologydepartment ("305389009", "Admission to paediatric neurology department"),
	
	/**
	 * Admission to paediatric oncology department
	 */
	_Admissiontopaediatriconcologydepartment ("305390000", "Admission to paediatric oncology department"),
	
	/**
	 * Admission to pain management department
	 */
	_Admissiontopainmanagementdepartment ("305391001", "Admission to pain management department"),
	
	/**
	 * Admission to pathology department
	 */
	_Admissiontopathologydepartment ("305392008", "Admission to pathology department"),
	
	/**
	 * Admission to blood transfusion department
	 */
	_Admissiontobloodtransfusiondepartment ("305393003", "Admission to blood transfusion department"),
	
	/**
	 * Admission to chemical pathology department
	 */
	_Admissiontochemicalpathologydepartment ("305394009", "Admission to chemical pathology department"),
	
	/**
	 * Admission to general pathology department
	 */
	_Admissiontogeneralpathologydepartment ("305395005", "Admission to general pathology department"),
	
	/**
	 * Admission to haematology department
	 */
	_Admissiontohaematologydepartment ("305396006", "Admission to haematology department"),
	
	/**
	 * Admission to medical microbiology department
	 */
	_Admissiontomedicalmicrobiologydepartment ("305397002", "Admission to medical microbiology department"),
	
	/**
	 * Admission to the mortuary
	 */
	_Admissiontothemortuary ("305398007", "Admission to the mortuary"),
	
	/**
	 * Admission to neuropathology department
	 */
	_Admissiontoneuropathologydepartment ("305399004", "Admission to neuropathology department"),
	
	/**
	 * Admission to psychiatry department
	 */
	_Admissiontopsychiatrydepartment ("305400006", "Admission to psychiatry department"),
	
	/**
	 * Admission to child and adolescent psychiatry department
	 */
	_Admissiontochildandadolescentpsychiatrydepartment ("305401005", "Admission to child and adolescent psychiatry department"),
	
	/**
	 * Admission to forensic psychiatry department
	 */
	_Admissiontoforensicpsychiatrydepartment ("305402003", "Admission to forensic psychiatry department"),
	
	/**
	 * Admission to psychogeriatric department
	 */
	_Admissiontopsychogeriatricdepartment ("305403008", "Admission to psychogeriatric department"),
	
	/**
	 * Admission to mental handicap psychiatry department
	 */
	_Admissiontomentalhandicappsychiatrydepartment ("305404002", "Admission to mental handicap psychiatry department"),
	
	/**
	 * Admission to rehabilitation psychiatry department
	 */
	_Admissiontorehabilitationpsychiatrydepartment ("305405001", "Admission to rehabilitation psychiatry department"),
	
	/**
	 * Admission to radiology department
	 */
	_Admissiontoradiologydepartment ("305406000", "Admission to radiology department"),
	
	/**
	 * Admission to occupational health department
	 */
	_Admissiontooccupationalhealthdepartment ("305407009", "Admission to occupational health department"),
	
	/**
	 * Admission to surgical department
	 */
	_Admissiontosurgicaldepartment ("305408004", "Admission to surgical department"),
	
	/**
	 * Admission to breast surgery department
	 */
	_Admissiontobreastsurgerydepartment ("305409007", "Admission to breast surgery department"),
	
	/**
	 * Admission to cardiothoracic surgery department
	 */
	_Admissiontocardiothoracicsurgerydepartment ("305410002", "Admission to cardiothoracic surgery department"),
	
	/**
	 * Admission to thoracic surgery department
	 */
	_Admissiontothoracicsurgerydepartment ("305411003", "Admission to thoracic surgery department"),
	
	/**
	 * Admission to cardiac surgery department
	 */
	_Admissiontocardiacsurgerydepartment ("305412005", "Admission to cardiac surgery department"),
	
	/**
	 * Admission to dental surgery department
	 */
	_Admissiontodentalsurgerydepartment ("305413000", "Admission to dental surgery department"),
	
	/**
	 * Admission to orthodontics department
	 */
	_Admissiontoorthodonticsdepartment ("305414006", "Admission to orthodontics department"),
	
	/**
	 * Admission to paediatric dentistry department
	 */
	_Admissiontopaediatricdentistrydepartment ("305415007", "Admission to paediatric dentistry department"),
	
	/**
	 * Admission to restorative dentistry department
	 */
	_Admissiontorestorativedentistrydepartment ("305416008", "Admission to restorative dentistry department"),
	
	/**
	 * Admission to ear, nose and throat department
	 */
	_Admissiontoearnoseandthroatdepartment ("305417004", "Admission to ear, nose and throat department"),
	
	/**
	 * Admission to endocrine surgery department
	 */
	_Admissiontoendocrinesurgerydepartment ("305418009", "Admission to endocrine surgery department"),
	
	/**
	 * Admission to gastrointestinal surgery department
	 */
	_Admissiontogastrointestinalsurgerydepartment ("305419001", "Admission to gastrointestinal surgery department"),
	
	/**
	 * Admission to general gastrointestinal surgery department
	 */
	_Admissiontogeneralgastrointestinalsurgerydepartment ("305420007", "Admission to general gastrointestinal surgery department"),
	
	/**
	 * Admission to upper gastrointestinal surgery department
	 */
	_Admissiontouppergastrointestinalsurgerydepartment ("305421006", "Admission to upper gastrointestinal surgery department"),
	
	/**
	 * Admission to colorectal surgery department
	 */
	_Admissiontocolorectalsurgerydepartment ("305422004", "Admission to colorectal surgery department"),
	
	/**
	 * Admission to general surgical department
	 */
	_Admissiontogeneralsurgicaldepartment ("305423009", "Admission to general surgical department"),
	
	/**
	 * Admission to hepatobiliary surgical department
	 */
	_Admissiontohepatobiliarysurgicaldepartment ("305424003", "Admission to hepatobiliary surgical department"),
	
	/**
	 * Admission to neurosurgical department
	 */
	_Admissiontoneurosurgicaldepartment ("305425002", "Admission to neurosurgical department"),
	
	/**
	 * Admission to ophthalmology department
	 */
	_Admissiontoophthalmologydepartment ("305426001", "Admission to ophthalmology department"),
	
	/**
	 * Admission to oral surgery department
	 */
	_Admissiontooralsurgerydepartment ("305427005", "Admission to oral surgery department"),
	
	/**
	 * Admission to orthopaedic department
	 */
	_Admissiontoorthopaedicdepartment ("305428000", "Admission to orthopaedic department"),
	
	/**
	 * Admission to pancreatic surgery department
	 */
	_Admissiontopancreaticsurgerydepartment ("305429008", "Admission to pancreatic surgery department"),
	
	/**
	 * Admission to paediatric surgical department
	 */
	_Admissiontopaediatricsurgicaldepartment ("305430003", "Admission to paediatric surgical department"),
	
	/**
	 * Admission to plastic surgery department
	 */
	_Admissiontoplasticsurgerydepartment ("305431004", "Admission to plastic surgery department"),
	
	/**
	 * Admission to surgical transplant department
	 */
	_Admissiontosurgicaltransplantdepartment ("305432006", "Admission to surgical transplant department"),
	
	/**
	 * Admission to trauma surgery department
	 */
	_Admissiontotraumasurgerydepartment ("305433001", "Admission to trauma surgery department"),
	
	/**
	 * Admission to urology department
	 */
	_Admissiontourologydepartment ("305434007", "Admission to urology department"),
	
	/**
	 * Admission to vascular surgery department
	 */
	_Admissiontovascularsurgerydepartment ("305435008", "Admission to vascular surgery department"),
	
	/**
	 * Discharge by health worker
	 */
	_Dischargebyhealthworker ("306382007", "Discharge by health worker"),
	
	/**
	 * Discharge by counsellor
	 */
	_Dischargebycounsellor ("306383002", "Discharge by counsellor"),
	
	/**
	 * Discharge by bereavement counsellor
	 */
	_Dischargebybereavementcounsellor ("306385009", "Discharge by bereavement counsellor"),
	
	/**
	 * Discharge by genetic counsellor
	 */
	_Dischargebygeneticcounsellor ("306386005", "Discharge by genetic counsellor"),
	
	/**
	 * Discharge by marriage guidance counsellor
	 */
	_Dischargebymarriageguidancecounsellor ("306387001", "Discharge by marriage guidance counsellor"),
	
	/**
	 * Discharge by mental health counsellor
	 */
	_Dischargebymentalhealthcounsellor ("306388006", "Discharge by mental health counsellor"),
	
	/**
	 * Discharge by Accident and Emergency doctor
	 */
	_DischargebyAccidentandEmergencydoctor ("306390007", "Discharge by Accident and Emergency doctor"),
	
	/**
	 * Discharge by anaesthetist
	 */
	_Dischargebyanaesthetist ("306391006", "Discharge by anaesthetist"),
	
	/**
	 * Discharge by clinical oncologist
	 */
	_Dischargebyclinicaloncologist ("306392004", "Discharge by clinical oncologist"),
	
	/**
	 * Discharge by radiotherapist
	 */
	_Dischargebyradiotherapist ("306393009", "Discharge by radiotherapist"),
	
	/**
	 * Discharge by family planning doctor
	 */
	_Dischargebyfamilyplanningdoctor ("306394003", "Discharge by family planning doctor"),
	
	/**
	 * Discharge by own GP
	 */
	_DischargebyownGP ("306396001", "Discharge by own GP"),
	
	/**
	 * Discharge by partner of GP
	 */
	_DischargebypartnerofGP ("306397005", "Discharge by partner of GP"),
	
	/**
	 * Discharge by intensive care specialist
	 */
	_Dischargebyintensivecarespecialist ("306404005", "Discharge by intensive care specialist"),
	
	/**
	 * Discharge by adult intensive care specialist
	 */
	_Dischargebyadultintensivecarespecialist ("306405006", "Discharge by adult intensive care specialist"),
	
	/**
	 * Discharge by paediatric intensive care specialist
	 */
	_Dischargebypaediatricintensivecarespecialist ("306406007", "Discharge by paediatric intensive care specialist"),
	
	/**
	 * Discharge by pain management specialist
	 */
	_Dischargebypainmanagementspecialist ("306407003", "Discharge by pain management specialist"),
	
	/**
	 * Discharge by pathologist
	 */
	_Dischargebypathologist ("306408008", "Discharge by pathologist"),
	
	/**
	 * Discharge by blood transfusion doctor
	 */
	_Dischargebybloodtransfusiondoctor ("306409000", "Discharge by blood transfusion doctor"),
	
	/**
	 * Discharge by chemical pathologist
	 */
	_Dischargebychemicalpathologist ("306410005", "Discharge by chemical pathologist"),
	
	/**
	 * Discharge by general pathologist
	 */
	_Dischargebygeneralpathologist ("306411009", "Discharge by general pathologist"),
	
	/**
	 * Discharge by haematologist
	 */
	_Dischargebyhaematologist ("306412002", "Discharge by haematologist"),
	
	/**
	 * Discharge by immunopathologist
	 */
	_Dischargebyimmunopathologist ("306413007", "Discharge by immunopathologist"),
	
	/**
	 * Discharge by medical microbiologist
	 */
	_Dischargebymedicalmicrobiologist ("306414001", "Discharge by medical microbiologist"),
	
	/**
	 * Discharge by neuropathologist
	 */
	_Dischargebyneuropathologist ("306415000", "Discharge by neuropathologist"),
	
	/**
	 * Discharge by physician
	 */
	_Dischargebyphysician ("306416004", "Discharge by physician"),
	
	/**
	 * Discharge by clinical allergist
	 */
	_Dischargebyclinicalallergist ("306417008", "Discharge by clinical allergist"),
	
	/**
	 * Discharge by cardiologist
	 */
	_Dischargebycardiologist ("306418003", "Discharge by cardiologist"),
	
	/**
	 * Discharge by chest physician
	 */
	_Dischargebychestphysician ("306419006", "Discharge by chest physician"),
	
	/**
	 * Discharge by thoracic physician
	 */
	_Dischargebythoracicphysician ("306420000", "Discharge by thoracic physician"),
	
	/**
	 * Discharge by clinical haematologist
	 */
	_Dischargebyclinicalhaematologist ("306422008", "Discharge by clinical haematologist"),
	
	/**
	 * Discharge by clinical immunologist
	 */
	_Dischargebyclinicalimmunologist ("306423003", "Discharge by clinical immunologist"),
	
	/**
	 * Discharge by clinical neurophysiologist
	 */
	_Dischargebyclinicalneurophysiologist ("306424009", "Discharge by clinical neurophysiologist"),
	
	/**
	 * Discharge by clinical pharmacologist
	 */
	_Dischargebyclinicalpharmacologist ("306425005", "Discharge by clinical pharmacologist"),
	
	/**
	 * Discharge by clinical physiologist
	 */
	_Dischargebyclinicalphysiologist ("306426006", "Discharge by clinical physiologist"),
	
	/**
	 * Discharge by dermatologist
	 */
	_Dischargebydermatologist ("306427002", "Discharge by dermatologist"),
	
	/**
	 * Discharge by endocrinologist
	 */
	_Dischargebyendocrinologist ("306428007", "Discharge by endocrinologist"),
	
	/**
	 * Discharge by gastroenterologist
	 */
	_Dischargebygastroenterologist ("306429004", "Discharge by gastroenterologist"),
	
	/**
	 * Discharge by general physician
	 */
	_Dischargebygeneralphysician ("306430009", "Discharge by general physician"),
	
	/**
	 * Discharge by geneticist
	 */
	_Dischargebygeneticist ("306431008", "Discharge by geneticist"),
	
	/**
	 * Discharge by clinical geneticist
	 */
	_Dischargebyclinicalgeneticist ("306432001", "Discharge by clinical geneticist"),
	
	/**
	 * Discharge by clinical cytogeneticist
	 */
	_Dischargebyclinicalcytogeneticist ("306433006", "Discharge by clinical cytogeneticist"),
	
	/**
	 * Discharge by clinical molecular geneticist
	 */
	_Dischargebyclinicalmoleculargeneticist ("306434000", "Discharge by clinical molecular geneticist"),
	
	/**
	 * Discharge by genitourinary medicine physician
	 */
	_Dischargebygenitourinarymedicinephysician ("306435004", "Discharge by genitourinary medicine physician"),
	
	/**
	 * Discharge by care of the elderly physician
	 */
	_Dischargebycareoftheelderlyphysician ("306436003", "Discharge by care of the elderly physician"),
	
	/**
	 * Discharge by infectious diseases physician
	 */
	_Dischargebyinfectiousdiseasesphysician ("306437007", "Discharge by infectious diseases physician"),
	
	/**
	 * Discharge by medical ophthalmologist
	 */
	_Dischargebymedicalophthalmologist ("306438002", "Discharge by medical ophthalmologist"),
	
	/**
	 * Discharge by nephrologist
	 */
	_Dischargebynephrologist ("306439005", "Discharge by nephrologist"),
	
	/**
	 * Discharge by neurologist
	 */
	_Dischargebyneurologist ("306440007", "Discharge by neurologist"),
	
	/**
	 * Discharge by nuclear medicine physician
	 */
	_Dischargebynuclearmedicinephysician ("306441006", "Discharge by nuclear medicine physician"),
	
	/**
	 * Discharge by palliative care physician
	 */
	_Dischargebypalliativecarephysician ("306442004", "Discharge by palliative care physician"),
	
	/**
	 * Discharge by rehabilitation physician
	 */
	_Dischargebyrehabilitationphysician ("306443009", "Discharge by rehabilitation physician"),
	
	/**
	 * Discharge by rheumatologist
	 */
	_Dischargebyrheumatologist ("306444003", "Discharge by rheumatologist"),
	
	/**
	 * Discharge by paediatrician
	 */
	_Dischargebypaediatrician ("306445002", "Discharge by paediatrician"),
	
	/**
	 * Discharge by community paediatrician
	 */
	_Dischargebycommunitypaediatrician ("306446001", "Discharge by community paediatrician"),
	
	/**
	 * Discharge by neonatologist
	 */
	_Dischargebyneonatologist ("306447005", "Discharge by neonatologist"),
	
	/**
	 * Discharge by paediatric neurologist
	 */
	_Dischargebypaediatricneurologist ("306448000", "Discharge by paediatric neurologist"),
	
	/**
	 * Discharge by paediatric oncologist
	 */
	_Dischargebypaediatriconcologist ("306449008", "Discharge by paediatric oncologist"),
	
	/**
	 * Discharge by obstetrician and gynaecologist
	 */
	_Dischargebyobstetricianandgynaecologist ("306450008", "Discharge by obstetrician and gynaecologist"),
	
	/**
	 * Discharge by gynaecologist
	 */
	_Dischargebygynaecologist ("306451007", "Discharge by gynaecologist"),
	
	/**
	 * Discharge by obstetrician
	 */
	_Dischargebyobstetrician ("306452000", "Discharge by obstetrician"),
	
	/**
	 * Discharge by psychiatrist
	 */
	_Dischargebypsychiatrist ("306453005", "Discharge by psychiatrist"),
	
	/**
	 * Discharge by child and adolescent psychiatrist
	 */
	_Dischargebychildandadolescentpsychiatrist ("306454004", "Discharge by child and adolescent psychiatrist"),
	
	/**
	 * Discharge by forensic psychiatrist
	 */
	_Dischargebyforensicpsychiatrist ("306455003", "Discharge by forensic psychiatrist"),
	
	/**
	 * Discharge by liaison psychiatrist
	 */
	_Dischargebyliaisonpsychiatrist ("306456002", "Discharge by liaison psychiatrist"),
	
	/**
	 * Discharge by psychogeriatrician
	 */
	_Dischargebypsychogeriatrician ("306457006", "Discharge by psychogeriatrician"),
	
	/**
	 * Discharge by psychiatrist for mental handicap
	 */
	_Dischargebypsychiatristformentalhandicap ("306458001", "Discharge by psychiatrist for mental handicap"),
	
	/**
	 * Discharge by rehabilitation psychiatrist
	 */
	_Dischargebyrehabilitationpsychiatrist ("306459009", "Discharge by rehabilitation psychiatrist"),
	
	/**
	 * Discharge by radiologist
	 */
	_Dischargebyradiologist ("306460004", "Discharge by radiologist"),
	
	/**
	 * Discharge by occupational health physician
	 */
	_Dischargebyoccupationalhealthphysician ("306461000", "Discharge by occupational health physician"),
	
	/**
	 * Discharge by surgeon
	 */
	_Dischargebysurgeon ("306462007", "Discharge by surgeon"),
	
	/**
	 * Discharge by breast surgeon
	 */
	_Dischargebybreastsurgeon ("306463002", "Discharge by breast surgeon"),
	
	/**
	 * Discharge by cardiothoracic surgeon
	 */
	_Dischargebycardiothoracicsurgeon ("306464008", "Discharge by cardiothoracic surgeon"),
	
	/**
	 * Discharge by thoracic surgeon
	 */
	_Dischargebythoracicsurgeon ("306465009", "Discharge by thoracic surgeon"),
	
	/**
	 * Discharge by cardiac surgeon
	 */
	_Dischargebycardiacsurgeon ("306466005", "Discharge by cardiac surgeon"),
	
	/**
	 * Discharge by dental surgeon
	 */
	_Dischargebydentalsurgeon ("306467001", "Discharge by dental surgeon"),
	
	/**
	 * Discharge by orthodontist
	 */
	_Dischargebyorthodontist ("306468006", "Discharge by orthodontist"),
	
	/**
	 * Discharge by paediatric dentist
	 */
	_Dischargebypaediatricdentist ("306469003", "Discharge by paediatric dentist"),
	
	/**
	 * Discharge by restorative dentist
	 */
	_Dischargebyrestorativedentist ("306470002", "Discharge by restorative dentist"),
	
	/**
	 * Discharge by ear, nose and throat surgeon
	 */
	_Dischargebyearnoseandthroatsurgeon ("306471003", "Discharge by ear, nose and throat surgeon"),
	
	/**
	 * Discharge by endocrine surgeon
	 */
	_Dischargebyendocrinesurgeon ("306472005", "Discharge by endocrine surgeon"),
	
	/**
	 * Discharge by gastrointestinal surgeon
	 */
	_Dischargebygastrointestinalsurgeon ("306473000", "Discharge by gastrointestinal surgeon"),
	
	/**
	 * Discharge by general gastrointestinal surgeon
	 */
	_Dischargebygeneralgastrointestinalsurgeon ("306474006", "Discharge by general gastrointestinal surgeon"),
	
	/**
	 * Discharge by upper gastrointestinal surgeon
	 */
	_Dischargebyuppergastrointestinalsurgeon ("306475007", "Discharge by upper gastrointestinal surgeon"),
	
	/**
	 * Discharge by colorectal surgeon
	 */
	_Dischargebycolorectalsurgeon ("306476008", "Discharge by colorectal surgeon"),
	
	/**
	 * Discharge by general surgeon
	 */
	_Dischargebygeneralsurgeon ("306477004", "Discharge by general surgeon"),
	
	/**
	 * Discharge by hand surgeon
	 */
	_Dischargebyhandsurgeon ("306478009", "Discharge by hand surgeon"),
	
	/**
	 * Discharge by hepatobiliary surgeon
	 */
	_Dischargebyhepatobiliarysurgeon ("306479001", "Discharge by hepatobiliary surgeon"),
	
	/**
	 * Discharge by neurosurgeon
	 */
	_Dischargebyneurosurgeon ("306480003", "Discharge by neurosurgeon"),
	
	/**
	 * Discharge by ophthalmologist
	 */
	_Dischargebyophthalmologist ("306481004", "Discharge by ophthalmologist"),
	
	/**
	 * Discharge by oral surgeon
	 */
	_Dischargebyoralsurgeon ("306482006", "Discharge by oral surgeon"),
	
	/**
	 * Discharge by orthopaedic surgeon
	 */
	_Dischargebyorthopaedicsurgeon ("306483001", "Discharge by orthopaedic surgeon"),
	
	/**
	 * Discharge by paediatric surgeon
	 */
	_Dischargebypaediatricsurgeon ("306484007", "Discharge by paediatric surgeon"),
	
	/**
	 * Discharge by pancreatic surgeon
	 */
	_Dischargebypancreaticsurgeon ("306486009", "Discharge by pancreatic surgeon"),
	
	/**
	 * Discharge by plastic surgeon
	 */
	_Dischargebyplasticsurgeon ("306487000", "Discharge by plastic surgeon"),
	
	/**
	 * Discharge by transplant surgeon
	 */
	_Dischargebytransplantsurgeon ("306488005", "Discharge by transplant surgeon"),
	
	/**
	 * Discharge by trauma surgeon
	 */
	_Dischargebytraumasurgeon ("306489002", "Discharge by trauma surgeon"),
	
	/**
	 * Discharge by urologist
	 */
	_Dischargebyurologist ("306490006", "Discharge by urologist"),
	
	/**
	 * Discharge by vascular surgeon
	 */
	_Dischargebyvascularsurgeon ("306491005", "Discharge by vascular surgeon"),
	
	/**
	 * Discharge by nurse
	 */
	_Dischargebynurse ("306492003", "Discharge by nurse"),
	
	/**
	 * Discharge by agency nurse
	 */
	_Dischargebyagencynurse ("306493008", "Discharge by agency nurse"),
	
	/**
	 * Discharge by clinical nurse specialist
	 */
	_Dischargebyclinicalnursespecialist ("306494002", "Discharge by clinical nurse specialist"),
	
	/**
	 * Discharge by breast care nurse
	 */
	_Dischargebybreastcarenurse ("306495001", "Discharge by breast care nurse"),
	
	/**
	 * Discharge by cardiac rehabilitation nurse
	 */
	_Dischargebycardiacrehabilitationnurse ("306496000", "Discharge by cardiac rehabilitation nurse"),
	
	/**
	 * Discharge by contact tracing nurse
	 */
	_Dischargebycontacttracingnurse ("306497009", "Discharge by contact tracing nurse"),
	
	/**
	 * Discharge by continence nurse
	 */
	_Dischargebycontinencenurse ("306498004", "Discharge by continence nurse"),
	
	/**
	 * Discharge by diabetic liaison nurse
	 */
	_Dischargebydiabeticliaisonnurse ("306499007", "Discharge by diabetic liaison nurse"),
	
	/**
	 * Discharge by genitourinary nurse
	 */
	_Dischargebygenitourinarynurse ("306500003", "Discharge by genitourinary nurse"),
	
	/**
	 * Discharge by lymphoedema care nurse
	 */
	_Dischargebylymphoedemacarenurse ("306501004", "Discharge by lymphoedema care nurse"),
	
	/**
	 * Discharge by nurse behavioural therapist
	 */
	_Dischargebynursebehaviouraltherapist ("306502006", "Discharge by nurse behavioural therapist"),
	
	/**
	 * Discharge by nurse psychotherapist
	 */
	_Dischargebynursepsychotherapist ("306503001", "Discharge by nurse psychotherapist"),
	
	/**
	 * Discharge by pain management nurse
	 */
	_Dischargebypainmanagementnurse ("306504007", "Discharge by pain management nurse"),
	
	/**
	 * Discharge by paediatric nurse
	 */
	_Dischargebypaediatricnurse ("306505008", "Discharge by paediatric nurse"),
	
	/**
	 * Discharge by psychiatric nurse
	 */
	_Dischargebypsychiatricnurse ("306506009", "Discharge by psychiatric nurse"),
	
	/**
	 * Discharge by oncology nurse
	 */
	_Dischargebyoncologynurse ("306507000", "Discharge by oncology nurse"),
	
	/**
	 * Discharge by rheumatology nurse specialist
	 */
	_Dischargebyrheumatologynursespecialist ("306508005", "Discharge by rheumatology nurse specialist"),
	
	/**
	 * Discharge by stoma nurse
	 */
	_Dischargebystomanurse ("306509002", "Discharge by stoma nurse"),
	
	/**
	 * Discharge by stomatherapist
	 */
	_Dischargebystomatherapist ("306510007", "Discharge by stomatherapist"),
	
	/**
	 * Discharge by community-based nurse
	 */
	_Dischargebycommunitybasednurse ("306511006", "Discharge by community-based nurse"),
	
	/**
	 * Discharge by community nurse
	 */
	_Dischargebycommunitynurse ("306512004", "Discharge by community nurse"),
	
	/**
	 * Discharge by community psychiatric nurse
	 */
	_Dischargebycommunitypsychiatricnurse ("306513009", "Discharge by community psychiatric nurse"),
	
	/**
	 * Discharge by company nurse
	 */
	_Dischargebycompanynurse ("306514003", "Discharge by company nurse"),
	
	/**
	 * Discharge by district nurse
	 */
	_Dischargebydistrictnurse ("306515002", "Discharge by district nurse"),
	
	/**
	 * Discharge by health visitor
	 */
	_Dischargebyhealthvisitor ("306516001", "Discharge by health visitor"),
	
	/**
	 * Discharge by practice nurse
	 */
	_Dischargebypracticenurse ("306517005", "Discharge by practice nurse"),
	
	/**
	 * Discharge by liaison nurse
	 */
	_Dischargebyliaisonnurse ("306518000", "Discharge by liaison nurse"),
	
	/**
	 * Discharge by nurse practitioner
	 */
	_Dischargebynursepractitioner ("306519008", "Discharge by nurse practitioner"),
	
	/**
	 * Discharge by outreach nurse
	 */
	_Dischargebyoutreachnurse ("306520002", "Discharge by outreach nurse"),
	
	/**
	 * Discharge by research nurse
	 */
	_Dischargebyresearchnurse ("306521003", "Discharge by research nurse"),
	
	/**
	 * Discharge by midwife
	 */
	_Dischargebymidwife ("306522005", "Discharge by midwife"),
	
	/**
	 * Discharge by community-based midwife
	 */
	_Dischargebycommunitybasedmidwife ("306523000", "Discharge by community-based midwife"),
	
	/**
	 * Discharge by hospital-based midwife
	 */
	_Dischargebyhospitalbasedmidwife ("306524006", "Discharge by hospital-based midwife"),
	
	/**
	 * Discharge by educational psychologist
	 */
	_Dischargebyeducationalpsychologist ("306525007", "Discharge by educational psychologist"),
	
	/**
	 * Discharge by psychotherapist
	 */
	_Dischargebypsychotherapist ("306526008", "Discharge by psychotherapist"),
	
	/**
	 * Discharge by professional allied to medicine
	 */
	_Dischargebyprofessionalalliedtomedicine ("306527004", "Discharge by professional allied to medicine"),
	
	/**
	 * Discharge by arts therapist
	 */
	_Dischargebyartstherapist ("306528009", "Discharge by arts therapist"),
	
	/**
	 * Discharge by dance therapist
	 */
	_Dischargebydancetherapist ("306530006", "Discharge by dance therapist"),
	
	/**
	 * Discharge by drama therapist
	 */
	_Dischargebydramatherapist ("306531005", "Discharge by drama therapist"),
	
	/**
	 * Discharge by music therapist
	 */
	_Dischargebymusictherapist ("306532003", "Discharge by music therapist"),
	
	/**
	 * Discharge by play therapist
	 */
	_Dischargebyplaytherapist ("306533008", "Discharge by play therapist"),
	
	/**
	 * Discharge by audiologist
	 */
	_Dischargebyaudiologist ("306534002", "Discharge by audiologist"),
	
	/**
	 * Discharge by audiology technician
	 */
	_Dischargebyaudiologytechnician ("306535001", "Discharge by audiology technician"),
	
	/**
	 * Discharge by podiatrist
	 */
	_Dischargebypodiatrist ("306536000", "Discharge by podiatrist"),
	
	/**
	 * Discharge by community-based podiatrist
	 */
	_Dischargebycommunitybasedpodiatrist ("306538004", "Discharge by community-based podiatrist"),
	
	/**
	 * Discharge by hospital-based podiatrist
	 */
	_Dischargebyhospitalbasedpodiatrist ("306539007", "Discharge by hospital-based podiatrist"),
	
	/**
	 * Discharge by dietitian
	 */
	_Dischargebydietitian ("306540009", "Discharge by dietitian"),
	
	/**
	 * Discharge by hospital-based dietitian
	 */
	_Dischargebyhospitalbaseddietitian ("306541008", "Discharge by hospital-based dietitian"),
	
	/**
	 * Discharge by community-based dietitian
	 */
	_Dischargebycommunitybaseddietitian ("306542001", "Discharge by community-based dietitian"),
	
	/**
	 * Discharge by occupational therapist
	 */
	_Dischargebyoccupationaltherapist ("306543006", "Discharge by occupational therapist"),
	
	/**
	 * Discharge by community-based occupational therapist
	 */
	_Dischargebycommunitybasedoccupationaltherapist ("306544000", "Discharge by community-based occupational therapist"),
	
	/**
	 * Discharge by social services department occupational therapist
	 */
	_Dischargebysocialservicesdepartmentoccupationaltherapist ("306545004", "Discharge by social services department occupational therapist"),
	
	/**
	 * Discharge by hospital-based occupational therapist
	 */
	_Dischargebyhospitalbasedoccupationaltherapist ("306546003", "Discharge by hospital-based occupational therapist"),
	
	/**
	 * Discharge by optometrist
	 */
	_Dischargebyoptometrist ("306547007", "Discharge by optometrist"),
	
	/**
	 * Discharge by orthoptist
	 */
	_Dischargebyorthoptist ("306548002", "Discharge by orthoptist"),
	
	/**
	 * Discharge by orthotist
	 */
	_Dischargebyorthotist ("306549005", "Discharge by orthotist"),
	
	/**
	 * Discharge by surgical fitter
	 */
	_Dischargebysurgicalfitter ("306550005", "Discharge by surgical fitter"),
	
	/**
	 * Discharge by physiotherapist
	 */
	_Dischargebyphysiotherapist ("306551009", "Discharge by physiotherapist"),
	
	/**
	 * Discharge by community-based physiotherapist
	 */
	_Dischargebycommunitybasedphysiotherapist ("306552002", "Discharge by community-based physiotherapist"),
	
	/**
	 * Discharge by hospital-based physiotherapist
	 */
	_Dischargebyhospitalbasedphysiotherapist ("306553007", "Discharge by hospital-based physiotherapist"),
	
	/**
	 * Discharge by radiographer
	 */
	_Dischargebyradiographer ("306554001", "Discharge by radiographer"),
	
	/**
	 * Discharge by diagnostic radiographer
	 */
	_Dischargebydiagnosticradiographer ("306555000", "Discharge by diagnostic radiographer"),
	
	/**
	 * Discharge by therapeutic radiographer
	 */
	_Dischargebytherapeuticradiographer ("306556004", "Discharge by therapeutic radiographer"),
	
	/**
	 * Discharge by speech and language therapist
	 */
	_Dischargebyspeechandlanguagetherapist ("306557008", "Discharge by speech and language therapist"),
	
	/**
	 * Discharge by community-based speech and language therapist
	 */
	_Dischargebycommunitybasedspeechandlanguagetherapist ("306558003", "Discharge by community-based speech and language therapist"),
	
	/**
	 * Discharge by hospital-based speech and language therapist
	 */
	_Dischargebyhospitalbasedspeechandlanguagetherapist ("306559006", "Discharge by hospital-based speech and language therapist"),
	
	/**
	 * Self-discharge
	 */
	_Selfdischarge ("306560001", "Self-discharge"),
	
	/**
	 * Discharge from establishment
	 */
	_Dischargefromestablishment ("306561002", "Discharge from establishment"),
	
	/**
	 * Discharge from service
	 */
	_Dischargefromservice ("306562009", "Discharge from service"),
	
	/**
	 * Discharge from Accident and Emergency service
	 */
	_DischargefromAccidentandEmergencyservice ("306563004", "Discharge from Accident and Emergency service"),
	
	/**
	 * Discharge from anaesthetic service
	 */
	_Dischargefromanaestheticservice ("306564005", "Discharge from anaesthetic service"),
	
	/**
	 * Discharge from clinical oncology service
	 */
	_Dischargefromclinicaloncologyservice ("306565006", "Discharge from clinical oncology service"),
	
	/**
	 * Discharge from radiotherapy service
	 */
	_Dischargefromradiotherapyservice ("306566007", "Discharge from radiotherapy service"),
	
	/**
	 * Discharge from family planning service
	 */
	_Dischargefromfamilyplanningservice ("306567003", "Discharge from family planning service"),
	
	/**
	 * Discharge from intensive care service
	 */
	_Dischargefromintensivecareservice ("306568008", "Discharge from intensive care service"),
	
	/**
	 * Discharge from adult intensive care service
	 */
	_Dischargefromadultintensivecareservice ("306569000", "Discharge from adult intensive care service"),
	
	/**
	 * Discharge from paediatric intensive care service
	 */
	_Dischargefrompaediatricintensivecareservice ("306570004", "Discharge from paediatric intensive care service"),
	
	/**
	 * Discharge from medical service
	 */
	_Dischargefrommedicalservice ("306571000", "Discharge from medical service"),
	
	/**
	 * Discharge from clinical allergy service
	 */
	_Dischargefromclinicalallergyservice ("306572007", "Discharge from clinical allergy service"),
	
	/**
	 * Discharge from audiology service
	 */
	_Dischargefromaudiologyservice ("306573002", "Discharge from audiology service"),
	
	/**
	 * Discharge from cardiology service
	 */
	_Dischargefromcardiologyservice ("306574008", "Discharge from cardiology service"),
	
	/**
	 * Discharge from chest medicine service
	 */
	_Dischargefromchestmedicineservice ("306575009", "Discharge from chest medicine service"),
	
	/**
	 * Discharge from respiratory medicine service
	 */
	_Dischargefromrespiratorymedicineservice ("306576005", "Discharge from respiratory medicine service"),
	
	/**
	 * Discharge from thoracic medicine service
	 */
	_Dischargefromthoracicmedicineservice ("306577001", "Discharge from thoracic medicine service"),
	
	/**
	 * Discharge from clinical immunology service
	 */
	_Dischargefromclinicalimmunologyservice ("306578006", "Discharge from clinical immunology service"),
	
	/**
	 * Discharge from clinical neurophysiology service
	 */
	_Dischargefromclinicalneurophysiologyservice ("306579003", "Discharge from clinical neurophysiology service"),
	
	/**
	 * Discharge from clinical pharmacology service
	 */
	_Dischargefromclinicalpharmacologyservice ("306580000", "Discharge from clinical pharmacology service"),
	
	/**
	 * Discharge from clinical physiology service
	 */
	_Dischargefromclinicalphysiologyservice ("306581001", "Discharge from clinical physiology service"),
	
	/**
	 * Discharge from dermatology service
	 */
	_Dischargefromdermatologyservice ("306582008", "Discharge from dermatology service"),
	
	/**
	 * Discharge from endocrinology service
	 */
	_Dischargefromendocrinologyservice ("306583003", "Discharge from endocrinology service"),
	
	/**
	 * Discharge from gastroenterology service
	 */
	_Dischargefromgastroenterologyservice ("306584009", "Discharge from gastroenterology service"),
	
	/**
	 * Discharge from general medical service
	 */
	_Dischargefromgeneralmedicalservice ("306585005", "Discharge from general medical service"),
	
	/**
	 * Discharge from genetics service
	 */
	_Dischargefromgeneticsservice ("306586006", "Discharge from genetics service"),
	
	/**
	 * Discharge from clinical genetics service
	 */
	_Dischargefromclinicalgeneticsservice ("306588007", "Discharge from clinical genetics service"),
	
	/**
	 * Discharge from clinical cytogenetics service
	 */
	_Dischargefromclinicalcytogeneticsservice ("306589004", "Discharge from clinical cytogenetics service"),
	
	/**
	 * Discharge from clinical molecular genetics service
	 */
	_Dischargefromclinicalmoleculargeneticsservice ("306590008", "Discharge from clinical molecular genetics service"),
	
	/**
	 * Discharge from genitourinary medicine service
	 */
	_Dischargefromgenitourinarymedicineservice ("306591007", "Discharge from genitourinary medicine service"),
	
	/**
	 * Discharge from care of the elderly service
	 */
	_Dischargefromcareoftheelderlyservice ("306592000", "Discharge from care of the elderly service"),
	
	/**
	 * Discharge from infectious diseases service
	 */
	_Dischargefrominfectiousdiseasesservice ("306593005", "Discharge from infectious diseases service"),
	
	/**
	 * Discharge from nephrology service
	 */
	_Dischargefromnephrologyservice ("306594004", "Discharge from nephrology service"),
	
	/**
	 * Discharge from neurology service
	 */
	_Dischargefromneurologyservice ("306595003", "Discharge from neurology service"),
	
	/**
	 * Discharge from nuclear medicine service
	 */
	_Dischargefromnuclearmedicineservice ("306596002", "Discharge from nuclear medicine service"),
	
	/**
	 * Discharge from palliative care service
	 */
	_Dischargefrompalliativecareservice ("306597006", "Discharge from palliative care service"),
	
	/**
	 * Discharge from rheumatology service
	 */
	_Dischargefromrheumatologyservice ("306598001", "Discharge from rheumatology service"),
	
	/**
	 * Discharge from paediatric service
	 */
	_Dischargefrompaediatricservice ("306599009", "Discharge from paediatric service"),
	
	/**
	 * Discharge from community paediatric service
	 */
	_Dischargefromcommunitypaediatricservice ("306600007", "Discharge from community paediatric service"),
	
	/**
	 * Discharge from paediatric neurology service
	 */
	_Dischargefrompaediatricneurologyservice ("306601006", "Discharge from paediatric neurology service"),
	
	/**
	 * Discharge from paediatric oncology service
	 */
	_Dischargefrompaediatriconcologyservice ("306602004", "Discharge from paediatric oncology service"),
	
	/**
	 * Discharge from special care baby service
	 */
	_Dischargefromspecialcarebabyservice ("306603009", "Discharge from special care baby service"),
	
	/**
	 * Discharge from obstetrics and gynaecology service
	 */
	_Dischargefromobstetricsandgynaecologyservice ("306604003", "Discharge from obstetrics and gynaecology service"),
	
	/**
	 * Discharge from obstetrics service
	 */
	_Dischargefromobstetricsservice ("306605002", "Discharge from obstetrics service"),
	
	/**
	 * Discharge from gynaecology service
	 */
	_Dischargefromgynaecologyservice ("306606001", "Discharge from gynaecology service"),
	
	/**
	 * Discharge from psychiatry service
	 */
	_Dischargefrompsychiatryservice ("306607005", "Discharge from psychiatry service"),
	
	/**
	 * Discharge from child and adolescent psychiatry service
	 */
	_Dischargefromchildandadolescentpsychiatryservice ("306608000", "Discharge from child and adolescent psychiatry service"),
	
	/**
	 * Discharge from forensic psychiatry service
	 */
	_Dischargefromforensicpsychiatryservice ("306609008", "Discharge from forensic psychiatry service"),
	
	/**
	 * Discharge from liaison psychiatry service
	 */
	_Dischargefromliaisonpsychiatryservice ("306610003", "Discharge from liaison psychiatry service"),
	
	/**
	 * Discharge from mental handicap psychiatry service
	 */
	_Dischargefrommentalhandicappsychiatryservice ("306611004", "Discharge from mental handicap psychiatry service"),
	
	/**
	 * Discharge from psychogeriatrician service
	 */
	_Dischargefrompsychogeriatricianservice ("306612006", "Discharge from psychogeriatrician service"),
	
	/**
	 * Discharge from rehabilitation psychiatry service
	 */
	_Dischargefromrehabilitationpsychiatryservice ("306613001", "Discharge from rehabilitation psychiatry service"),
	
	/**
	 * Discharge from pathology service
	 */
	_Dischargefrompathologyservice ("306614007", "Discharge from pathology service"),
	
	/**
	 * Discharge from blood transfusion service
	 */
	_Dischargefrombloodtransfusionservice ("306615008", "Discharge from blood transfusion service"),
	
	/**
	 * Discharge from chemical pathology service
	 */
	_Dischargefromchemicalpathologyservice ("306616009", "Discharge from chemical pathology service"),
	
	/**
	 * Discharge from haematology service
	 */
	_Dischargefromhaematologyservice ("306617000", "Discharge from haematology service"),
	
	/**
	 * Discharge from pain management service
	 */
	_Dischargefrompainmanagementservice ("306618005", "Discharge from pain management service"),
	
	/**
	 * Discharge from occupational health service
	 */
	_Dischargefromoccupationalhealthservice ("306619002", "Discharge from occupational health service"),
	
	/**
	 * Discharge from psychotherapy service
	 */
	_Dischargefrompsychotherapyservice ("306620008", "Discharge from psychotherapy service"),
	
	/**
	 * Discharge from professionals allied to medicine service
	 */
	_Dischargefromprofessionalsalliedtomedicineservice ("306621007", "Discharge from professionals allied to medicine service"),
	
	/**
	 * Discharge from arts therapy service
	 */
	_Dischargefromartstherapyservice ("306622000", "Discharge from arts therapy service"),
	
	/**
	 * Discharge from podiatry service
	 */
	_Dischargefrompodiatryservice ("306623005", "Discharge from podiatry service"),
	
	/**
	 * Discharge from community podiatry service
	 */
	_Dischargefromcommunitypodiatryservice ("306624004", "Discharge from community podiatry service"),
	
	/**
	 * Discharge from hospital podiatry service
	 */
	_Dischargefromhospitalpodiatryservice ("306625003", "Discharge from hospital podiatry service"),
	
	/**
	 * Discharge from dietetics service
	 */
	_Dischargefromdieteticsservice ("306626002", "Discharge from dietetics service"),
	
	/**
	 * Discharge from hospital dietetics service
	 */
	_Dischargefromhospitaldieteticsservice ("306627006", "Discharge from hospital dietetics service"),
	
	/**
	 * Discharge from community dietetics service
	 */
	_Dischargefromcommunitydieteticsservice ("306628001", "Discharge from community dietetics service"),
	
	/**
	 * Discharge from occupational therapy service
	 */
	_Dischargefromoccupationaltherapyservice ("306629009", "Discharge from occupational therapy service"),
	
	/**
	 * Discharge from hospital occupational therapy service
	 */
	_Dischargefromhospitaloccupationaltherapyservice ("306630004", "Discharge from hospital occupational therapy service"),
	
	/**
	 * Discharge from community occupational therapy service
	 */
	_Dischargefromcommunityoccupationaltherapyservice ("306631000", "Discharge from community occupational therapy service"),
	
	/**
	 * Discharge from orthoptics service
	 */
	_Dischargefromorthopticsservice ("306632007", "Discharge from orthoptics service"),
	
	/**
	 * Discharge from hospital orthoptics service
	 */
	_Dischargefromhospitalorthopticsservice ("306633002", "Discharge from hospital orthoptics service"),
	
	/**
	 * Discharge from community orthoptics service
	 */
	_Dischargefromcommunityorthopticsservice ("306634008", "Discharge from community orthoptics service"),
	
	/**
	 * Discharge from physiotherapy service
	 */
	_Dischargefromphysiotherapyservice ("306635009", "Discharge from physiotherapy service"),
	
	/**
	 * Discharge from hospital physiotherapy service
	 */
	_Dischargefromhospitalphysiotherapyservice ("306636005", "Discharge from hospital physiotherapy service"),
	
	/**
	 * Discharge from community physiotherapy service
	 */
	_Dischargefromcommunityphysiotherapyservice ("306637001", "Discharge from community physiotherapy service"),
	
	/**
	 * Discharge from speech and language therapy service
	 */
	_Dischargefromspeechandlanguagetherapyservice ("306638006", "Discharge from speech and language therapy service"),
	
	/**
	 * Discharge from hospital speech and language therapy service
	 */
	_Dischargefromhospitalspeechandlanguagetherapyservice ("306639003", "Discharge from hospital speech and language therapy service"),
	
	/**
	 * Discharge from community speech and language therapy service
	 */
	_Dischargefromcommunityspeechandlanguagetherapyservice ("306640001", "Discharge from community speech and language therapy service"),
	
	/**
	 * Discharge from orthotics service
	 */
	_Dischargefromorthoticsservice ("306641002", "Discharge from orthotics service"),
	
	/**
	 * Discharge from hospital orthotics service
	 */
	_Dischargefromhospitalorthoticsservice ("306642009", "Discharge from hospital orthotics service"),
	
	/**
	 * Discharge from community orthotics service
	 */
	_Dischargefromcommunityorthoticsservice ("306643004", "Discharge from community orthotics service"),
	
	/**
	 * Discharge from surgical fitting service
	 */
	_Dischargefromsurgicalfittingservice ("306644005", "Discharge from surgical fitting service"),
	
	/**
	 * Discharge from hospital surgical fitting service
	 */
	_Dischargefromhospitalsurgicalfittingservice ("306645006", "Discharge from hospital surgical fitting service"),
	
	/**
	 * Discharge from community surgical fitting service
	 */
	_Dischargefromcommunitysurgicalfittingservice ("306646007", "Discharge from community surgical fitting service"),
	
	/**
	 * Discharge from radiology service
	 */
	_Dischargefromradiologyservice ("306647003", "Discharge from radiology service"),
	
	/**
	 * Discharge from surgical service
	 */
	_Dischargefromsurgicalservice ("306648008", "Discharge from surgical service"),
	
	/**
	 * Discharge from breast surgery service
	 */
	_Dischargefrombreastsurgeryservice ("306649000", "Discharge from breast surgery service"),
	
	/**
	 * Discharge from cardiothoracic surgery service
	 */
	_Dischargefromcardiothoracicsurgeryservice ("306650000", "Discharge from cardiothoracic surgery service"),
	
	/**
	 * Discharge from thoracic surgery service
	 */
	_Dischargefromthoracicsurgeryservice ("306651001", "Discharge from thoracic surgery service"),
	
	/**
	 * Discharge from cardiac surgery service
	 */
	_Dischargefromcardiacsurgeryservice ("306652008", "Discharge from cardiac surgery service"),
	
	/**
	 * Discharge from dental surgery service
	 */
	_Dischargefromdentalsurgeryservice ("306653003", "Discharge from dental surgery service"),
	
	/**
	 * Discharge from orthodontics service
	 */
	_Dischargefromorthodonticsservice ("306654009", "Discharge from orthodontics service"),
	
	/**
	 * Discharge from paediatric dentistry service
	 */
	_Dischargefrompaediatricdentistryservice ("306655005", "Discharge from paediatric dentistry service"),
	
	/**
	 * Discharge from restorative dentistry service
	 */
	_Dischargefromrestorativedentistryservice ("306656006", "Discharge from restorative dentistry service"),
	
	/**
	 * Discharge from ear, nose and throat service
	 */
	_Dischargefromearnoseandthroatservice ("306657002", "Discharge from ear, nose and throat service"),
	
	/**
	 * Discharge from endocrine surgery service
	 */
	_Dischargefromendocrinesurgeryservice ("306658007", "Discharge from endocrine surgery service"),
	
	/**
	 * Discharge from gastrointestinal surgical service
	 */
	_Dischargefromgastrointestinalsurgicalservice ("306659004", "Discharge from gastrointestinal surgical service"),
	
	/**
	 * Discharge from general gastrointestinal surgical service
	 */
	_Dischargefromgeneralgastrointestinalsurgicalservice ("306660009", "Discharge from general gastrointestinal surgical service"),
	
	/**
	 * Discharge from upper gastrointestinal surgical service
	 */
	_Dischargefromuppergastrointestinalsurgicalservice ("306661008", "Discharge from upper gastrointestinal surgical service"),
	
	/**
	 * Discharge from colorectal surgery service
	 */
	_Dischargefromcolorectalsurgeryservice ("306662001", "Discharge from colorectal surgery service"),
	
	/**
	 * Discharge from general surgical service
	 */
	_Dischargefromgeneralsurgicalservice ("306663006", "Discharge from general surgical service"),
	
	/**
	 * Discharge from hand surgery service
	 */
	_Dischargefromhandsurgeryservice ("306664000", "Discharge from hand surgery service"),
	
	/**
	 * Discharge from neurosurgical service
	 */
	_Dischargefromneurosurgicalservice ("306665004", "Discharge from neurosurgical service"),
	
	/**
	 * Discharge from ophthalmology service
	 */
	_Dischargefromophthalmologyservice ("306666003", "Discharge from ophthalmology service"),
	
	/**
	 * Discharge from oral surgery service
	 */
	_Dischargefromoralsurgeryservice ("306667007", "Discharge from oral surgery service"),
	
	/**
	 * Discharge from orthopaedic service
	 */
	_Dischargefromorthopaedicservice ("306668002", "Discharge from orthopaedic service"),
	
	/**
	 * Discharge from pancreatic surgery service
	 */
	_Dischargefrompancreaticsurgeryservice ("306669005", "Discharge from pancreatic surgery service"),
	
	/**
	 * Discharge from paediatric surgical service
	 */
	_Dischargefrompaediatricsurgicalservice ("306670006", "Discharge from paediatric surgical service"),
	
	/**
	 * Discharge from plastic surgery service
	 */
	_Dischargefromplasticsurgeryservice ("306671005", "Discharge from plastic surgery service"),
	
	/**
	 * Discharge from transplant surgery service
	 */
	_Dischargefromtransplantsurgeryservice ("306672003", "Discharge from transplant surgery service"),
	
	/**
	 * Discharge from trauma service
	 */
	_Dischargefromtraumaservice ("306673008", "Discharge from trauma service"),
	
	/**
	 * Discharge from urology service
	 */
	_Dischargefromurologyservice ("306674002", "Discharge from urology service"),
	
	/**
	 * Discharge from vascular surgery service
	 */
	_Dischargefromvascularsurgeryservice ("306675001", "Discharge from vascular surgery service"),
	
	/**
	 * Discharge from hospice
	 */
	_Dischargefromhospice ("306676000", "Discharge from hospice"),
	
	/**
	 * Discharge from day hospital
	 */
	_Dischargefromdayhospital ("306677009", "Discharge from day hospital"),
	
	/**
	 * Discharge from psychiatry day hospital
	 */
	_Dischargefrompsychiatrydayhospital ("306678004", "Discharge from psychiatry day hospital"),
	
	/**
	 * Discharge from psychogeriatric day hospital
	 */
	_Dischargefrompsychogeriatricdayhospital ("306679007", "Discharge from psychogeriatric day hospital"),
	
	/**
	 * Discharge from care of the elderly day hospital
	 */
	_Dischargefromcareoftheelderlydayhospital ("306680005", "Discharge from care of the elderly day hospital"),
	
	/**
	 * Discharge from hospice day hospital
	 */
	_Dischargefromhospicedayhospital ("306681009", "Discharge from hospice day hospital"),
	
	/**
	 * Discharge from ward
	 */
	_Dischargefromward ("306682002", "Discharge from ward"),
	
	/**
	 * Discharge from day ward
	 */
	_Dischargefromdayward ("306683007", "Discharge from day ward"),
	
	/**
	 * Case closure by social services department
	 */
	_Caseclosurebysocialservicesdepartment ("306684001", "Case closure by social services department"),
	
	/**
	 * Discharge to establishment
	 */
	_Dischargetoestablishment ("306685000", "Discharge to establishment"),
	
	/**
	 * Discharge to home
	 */
	_Dischargetohome ("306689006", "Discharge to home"),
	
	/**
	 * Discharge to relatives home
	 */
	_Dischargetorelativeshome ("306690002", "Discharge to relative's home"),
	
	/**
	 * Discharge to residential home
	 */
	_Dischargetoresidentialhome ("306691003", "Discharge to residential home"),
	
	/**
	 * Discharge to private residential home
	 */
	_Dischargetoprivateresidentialhome ("306692005", "Discharge to private residential home"),
	
	/**
	 * Discharge to part III residential home
	 */
	_DischargetopartIIIresidentialhome ("306693000", "Discharge to part III residential home"),
	
	/**
	 * Discharge to nursing home
	 */
	_Dischargetonursinghome ("306694006", "Discharge to nursing home"),
	
	/**
	 * Discharge to private nursing home
	 */
	_Dischargetoprivatenursinghome ("306695007", "Discharge to private nursing home"),
	
	/**
	 * Discharge to part III accommodation
	 */
	_DischargetopartIIIaccommodation ("306696008", "Discharge to part III accommodation"),
	
	/**
	 * Discharge to sheltered housing
	 */
	_Dischargetoshelteredhousing ("306697004", "Discharge to sheltered housing"),
	
	/**
	 * Discharge to warden controlled accommodation
	 */
	_Dischargetowardencontrolledaccommodation ("306698009", "Discharge to warden controlled accommodation"),
	
	/**
	 * Discharge to hospital
	 */
	_Dischargetohospital ("306699001", "Discharge to hospital"),
	
	/**
	 * Discharge to long stay hospital
	 */
	_Dischargetolongstayhospital ("306700000", "Discharge to long stay hospital"),
	
	/**
	 * Discharge to community hospital
	 */
	_Dischargetocommunityhospital ("306701001", "Discharge to community hospital"),
	
	/**
	 * Discharge to tertiary referral hospital
	 */
	_Dischargetotertiaryreferralhospital ("306703003", "Discharge to tertiary referral hospital"),
	
	/**
	 * Discharge to police custody
	 */
	_Dischargetopolicecustody ("306705005", "Discharge to police custody"),
	
	/**
	 * Discharge to ward
	 */
	_Dischargetoward ("306706006", "Discharge to ward"),
	
	/**
	 * Discharge to day ward
	 */
	_Dischargetodayward ("306707002", "Discharge to day ward"),
	
	/**
	 * Case closure by social worker
	 */
	_Caseclosurebysocialworker ("306729003", "Case closure by social worker"),
	
	/**
	 * Admission by general dental surgeon
	 */
	_Admissionbygeneraldentalsurgeon ("306731007", "Admission by general dental surgeon"),
	
	/**
	 * Admission to general dental surgery department
	 */
	_Admissiontogeneraldentalsurgerydepartment ("306732000", "Admission to general dental surgery department"),
	
	/**
	 * Discharge by general dental surgeon
	 */
	_Dischargebygeneraldentalsurgeon ("306733005", "Discharge by general dental surgeon"),
	
	/**
	 * Admission to stroke unit
	 */
	_Admissiontostrokeunit ("306803007", "Admission to stroke unit"),
	
	/**
	 * Admission to young disabled unit
	 */
	_Admissiontoyoungdisabledunit ("306804001", "Admission to young disabled unit"),
	
	/**
	 * Discharge from stroke service
	 */
	_Dischargefromstrokeservice ("306808003", "Discharge from stroke service"),
	
	/**
	 * Discharge from young disabled service
	 */
	_Dischargefromyoungdisabledservice ("306809006", "Discharge from young disabled service"),
	
	/**
	 * Discharge by audiological physician
	 */
	_Dischargebyaudiologicalphysician ("306858005", "Discharge by audiological physician"),
	
	/**
	 * Discharge from dance therapy service
	 */
	_Dischargefromdancetherapyservice ("306862004", "Discharge from dance therapy service"),
	
	/**
	 * Discharge from drama therapy service
	 */
	_Dischargefromdramatherapyservice ("306863009", "Discharge from drama therapy service"),
	
	/**
	 * Discharge from music therapy service
	 */
	_Dischargefrommusictherapyservice ("306864003", "Discharge from music therapy service"),
	
	/**
	 * Admission to hand surgery department
	 */
	_Admissiontohandsurgerydepartment ("306967009", "Admission to hand surgery department"),
	
	/**
	 * Admission by clinical haematologist
	 */
	_Admissionbyclinicalhaematologist ("307064007", "Admission by clinical haematologist"),
	
	/**
	 * Discharge from head injury rehabilitation service
	 */
	_Dischargefromheadinjuryrehabilitationservice ("307382004", "Discharge from head injury rehabilitation service"),
	
	/**
	 * Discharge from community rehabilitation service
	 */
	_Dischargefromcommunityrehabilitationservice ("307383009", "Discharge from community rehabilitation service"),
	
	/**
	 * Discharge by person
	 */
	_Dischargebyperson ("307838002", "Discharge by person"),
	
	/**
	 * Discharge from rehabilitation service
	 */
	_Dischargefromrehabilitationservice ("308018004", "Discharge from rehabilitation service"),
	
	/**
	 * Admission to clinical oncology department
	 */
	_Admissiontoclinicaloncologydepartment ("308251003", "Admission to clinical oncology department"),
	
	/**
	 * Admission to radiotherapy department
	 */
	_Admissiontoradiotherapydepartment ("308252005", "Admission to radiotherapy department"),
	
	/**
	 * Admission to diabetic department
	 */
	_Admissiontodiabeticdepartment ("308253000", "Admission to diabetic department"),
	
	/**
	 * Discharge from hospital
	 */
	_Dischargefromhospital ("308283009", "Discharge from hospital"),
	
	/**
	 * Delayed discharge - social services
	 */
	_Delayeddischargesocialservices ("309568009", "Delayed discharge - social services"),
	
	/**
	 * Non-urgent cardiological admission
	 */
	_Nonurgentcardiologicaladmission ("310361003", "Non-urgent cardiological admission"),
	
	/**
	 * Discharge by medical oncologist
	 */
	_Dischargebymedicaloncologist ("310513006", "Discharge by medical oncologist"),
	
	/**
	 * Admission by medical oncologist
	 */
	_Admissionbymedicaloncologist ("310518002", "Admission by medical oncologist"),
	
	/**
	 * Admission for social reasons
	 */
	_Admissionforsocialreasons ("313331005", "Admission for social reasons"),
	
	/**
	 * Admit cardiology emergency
	 */
	_Admitcardiologyemergency ("313385005", "Admit cardiology emergency"),
	
	/**
	 * Unexpected admission to high dependency unit
	 */
	_Unexpectedadmissiontohighdependencyunit ("397769005", "Unexpected admission to high dependency unit"),
	
	/**
	 * Unexpected admission to intensive care unit
	 */
	_Unexpectedadmissiontointensivecareunit ("397945004", "Unexpected admission to intensive care unit"),
	
	/**
	 * Admission to high dependency unit
	 */
	_Admissiontohighdependencyunit ("398162007", "Admission to high dependency unit"),
	
	/**
	 * Unexpected hospital admission
	 */
	_Unexpectedhospitaladmission ("405614004", "Unexpected hospital admission"),
	
	/**
	 * Discharge to convalescence
	 */
	_Dischargetoconvalescence ("405807003", "Discharge to convalescence"),
	
	/**
	 * Admit respiratory emergency
	 */
	_Admitrespiratoryemergency ("408489005", "Admit respiratory emergency"),
	
	/**
	 * Admit COPD emergency
	 */
	_AdmitCOPDemergency ("408501008", "Admit COPD emergency"),
	
	/**
	 * Discharge from learning disability team
	 */
	_Dischargefromlearningdisabilityteam ("413128002", "Discharge from learning disability team"),
	
	/**
	 * Admit heart failure emergency
	 */
	_Admitheartfailureemergency ("416683003", "Admit heart failure emergency"),
	
	/**
	 * Admission from
	 */
	_Admissionfrom ("427675001", "Admission from"),
	
	/**
	 * Discharge from intermediate care
	 */
	_Dischargefromintermediatecare ("25131000000105", "Discharge from intermediate care"),
	
	/**
	 * Discharge from care
	 */
	_Dischargefromcare ("25241000000106", "Discharge from care"),
	
	/**
	 * Admission to high dependency unit or intensive therapy unit following anaesthetic adverse event
	 */
	_Admissiontohighdependencyunitorintensivetherapyunitfollowinganaestheticadverseevent ("84901000000108", "Admission to high dependency unit or intensive therapy unit following anaesthetic adverse event"),
	
	/**
	 * Discharge from care against professional advice
	 */
	_Dischargefromcareagainstprofessionaladvice ("88861000000105", "Discharge from care against professional advice"),
	
	/**
	 * Discharge from care in accordance with professional advice
	 */
	_Dischargefromcareinaccordancewithprofessionaladvice ("88881000000101", "Discharge from care in accordance with professional advice"),
	
	/**
	 * Admission to regional secure unit
	 */
	_Admissiontoregionalsecureunit ("91981000000104", "Admission to regional secure unit"),
	
	/**
	 * Admission to medium secure unit
	 */
	_Admissiontomediumsecureunit ("91991000000102", "Admission to medium secure unit"),
	
	/**
	 * Admission to psychiatric intensive care unit
	 */
	_Admissiontopsychiatricintensivecareunit ("92231000000101", "Admission to psychiatric intensive care unit"),
	
	/**
	 * Admission to high secure unit
	 */
	_Admissiontohighsecureunit ("108171000000102", "Admission to high secure unit"),
	
	/**
	 * Admission to mental health specialist services
	 */
	_Admissiontomentalhealthspecialistservices ("163781000000106", "Admission to mental health specialist services"),
	
	/**
	 * Discharge from practice nurse heart failure clinic
	 */
	_Dischargefrompracticenurseheartfailureclinic ("200201000000101", "Discharge from practice nurse heart failure clinic"),
	
	/**
	 * Antenatal admission
	 */
	_Antenataladmission ("203221000000100", "Antenatal admission"),
	
	/**
	 * Admission under the Mental Health Act
	 */
	_AdmissionundertheMentalHealthAct ("242171000000106", "Admission under the Mental Health Act"),
	
	/**
	 * Admission by co-operative general practitioner
	 */
	_Admissionbycooperativegeneralpractitioner ("388751000000101", "Admission by co-operative general practitioner"),
	
	/**
	 * Admission by deputising general practitioner
	 */
	_Admissionbydeputisinggeneralpractitioner ("395581000000108", "Admission by deputising general practitioner"),
	
	/**
	 * Emergency psychiatric admission under Mental Health Act 1983 England and Wales
	 */
	_EmergencypsychiatricadmissionunderMentalHealthAct1983EnglandandWales ("395661000000100", "Emergency psychiatric admission under Mental Health Act 1983 (England and Wales)"),
	
	/**
	 * Discharge by general practice registrar
	 */
	_Dischargebygeneralpracticeregistrar ("401751000000103", "Discharge by general practice registrar"),
	
	/**
	 * Discharge by co-operative GP general practitioner
	 */
	_DischargebycooperativeGPgeneralpractitioner ("424191000000103", "Discharge by co-operative GP (general practitioner)"),
	
	/**
	 * Admission by GP general practitioner registrar
	 */
	_AdmissionbyGPgeneralpractitionerregistrar ("424211000000104", "Admission by GP (general practitioner) registrar"),
	
	/**
	 * Discharge by deputising general practitioner
	 */
	_Dischargebydeputisinggeneralpractitioner ("431341000000109", "Discharge by deputising general practitioner"),
	
	/**
	 * Admission by associate GP general practitioner
	 */
	_AdmissionbyassociateGPgeneralpractitioner ("437171000000107", "Admission by associate GP (general practitioner)"),
	
	/**
	 * Discharge by associate general practitioner
	 */
	_Dischargebyassociategeneralpractitioner ("443291000000109", "Discharge by associate general practitioner"),
	
	/**
	 * Admission by assistant GP general practitioner
	 */
	_AdmissionbyassistantGPgeneralpractitioner ("449201000000100", "Admission by assistant GP (general practitioner)"),
	
	/**
	 * Discharge by assistant general practitioner
	 */
	_Dischargebyassistantgeneralpractitioner ("455271000000109", "Discharge by assistant general practitioner"),
	
	/**
	 * Discharge by locum general practitioner
	 */
	_Dischargebylocumgeneralpractitioner ("466881000000106", "Discharge by locum general practitioner"),
	
	/**
	 * Admission by locum general practitioner
	 */
	_Admissionbylocumgeneralpractitioner ("476981000000106", "Admission by locum general practitioner"),
	
	/**
	 * Discharge from primary healthcare team
	 */
	_Dischargefromprimaryhealthcareteam ("782351000000102", "Discharge from primary healthcare team"),
	
	/**
	 * Discharge from care following non-attendance
	 */
	_Dischargefromcarefollowingnonattendance ("790241000000105", "Discharge from care following non-attendance"),
	
	/**
	 * Discharge from community specialist palliative care
	 */
	_Dischargefromcommunityspecialistpalliativecare ("818361000000102", "Discharge from community specialist palliative care"),
	
	/**
	 * Discharge from advanced primary nurse care
	 */
	_Dischargefromadvancedprimarynursecare ("818941000000102", "Discharge from advanced primary nurse care"),
	
	/**
	 * 111 contact disposition to general practitioner
	 */
	_111contactdispositiontogeneralpractitioner ("864811000000104", "111 contact disposition to general practitioner"),
	
	/**
	 * 111 contact disposition to pharmacist
	 */
	_111contactdispositiontopharmacist ("864831000000107", "111 contact disposition to pharmacist"),
	
	/**
	 * 111 contact disposition to out of hours service
	 */
	_111contactdispositiontooutofhoursservice ("864851000000100", "111 contact disposition to out of hours service"),
	
	/**
	 * 111 contact disposition to community service
	 */
	_111contactdispositiontocommunityservice ("864871000000109", "111 contact disposition to community service"),
	
	/**
	 * 111 contact disposition to mental health service
	 */
	_111contactdispositiontomentalhealthservice ("864891000000108", "111 contact disposition to mental health service"),
	
	/**
	 * 111 contact disposition to Social Services
	 */
	_111contactdispositiontoSocialServices ("864911000000106", "111 contact disposition to Social Services"),
	
	/**
	 * 111 contact disposition to 999 transfer
	 */
	_111contactdispositionto999transfer ("864931000000103", "111 contact disposition to 999 transfer"),
	
	/**
	 * 111 contact disposition to accident and emergency department
	 */
	_111contactdispositiontoaccidentandemergencydepartment ("864951000000105", "111 contact disposition to accident and emergency department"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	Encounterdisposition(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static Encounterdisposition getByCode(String code) {
		Encounterdisposition[] vals = values();
		for (Encounterdisposition val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(Encounterdisposition other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	