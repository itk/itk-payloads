/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the CorrespondenceDocumenttype vocabulary:
 * <ul>
 *   <li>_229059009 : Report</li>
 *   <li>_270370004 : Driving licence fitness report</li>
 *   <li>_270372007 : Disabled driver orange badge report</li>
 *   <li>_307881004 : DSS RMO RM2 report</li>
 *   <li>_307930005 : Death certificate</li>
 *   <li>_308575004 : Employment report</li>
 *   <li>_308584004 : DLA 370 report</li>
 *   <li>_308585003 : DS 1500 report</li>
 *   <li>_308619006 : War pensions report</li>
 *   <li>_308621001 : RM10-DHSS DMO report</li>
 *   <li>_310854009 : Housing report</li>
 *   <li>_371527006 : Radiology report</li>
 *   <li>_373942005 : Discharge summary</li>
 *   <li>_408403008 : Patient held record</li>
 *   <li>_416779005 : Anaesthesia record</li>
 *   <li>_445300006 : Emergency department record</li>
 *   <li>_24641000000107 : Investigation result</li>
 *   <li>_24711000000100 : Mail to patient</li>
 *   <li>_25611000000107 : Referral letter</li>
 *   <li>_25731000000109 : Mail from patient</li>
 *   <li>_25831000000103 : Image document</li>
 *   <li>_37341000000109 : Alert note</li>
 *   <li>_163111000000100 : Medication Record</li>
 *   <li>_163221000000102 : Allergies and Adverse Reactions</li>
 *   <li>_325661000000106 : Care plan</li>
 *   <li>_325931000000109 : Assessment</li>
 *   <li>_526591000000108 : Record of fluid balance</li>
 *   <li>_716921000000105 : Fulfiller request</li>
 *   <li>_717321000000108 : Diagnostic imaging report</li>
 *   <li>_819981000000101 : Nursing assessment record</li>
 *   <li>_819991000000104 : Allied health professional assessment record</li>
 *   <li>_820011000000105 : CAF common assessment framework assessment record</li>
 *   <li>_820021000000104 : SSA - single shared assessment record</li>
 *   <li>_820031000000102 : CPA care programme approach assessment record</li>
 *   <li>_820041000000106 : Multidisciplinary assessment record</li>
 *   <li>_820071000000100 : Pre-admission assessment record</li>
 *   <li>_820081000000103 : Self assessment record</li>
 *   <li>_820091000000101 : Medical assessment record</li>
 *   <li>_820101000000109 : Social services assessment record</li>
 *   <li>_820121000000100 : Discharge care plan</li>
 *   <li>_820141000000107 : Record of fundal height measurement</li>
 *   <li>_820161000000108 : Growth chart</li>
 *   <li>_820191000000102 : Partogram</li>
 *   <li>_820211000000103 : Patient safety checklist</li>
 *   <li>_820221000000109 : Inpatient medical note</li>
 *   <li>_820241000000102 : Medical photograph</li>
 *   <li>_820251000000104 : Interventional radiology record</li>
 *   <li>_820261000000101 : Controlled drug dispensing record</li>
 *   <li>_820271000000108 : Chemotherapy record</li>
 *   <li>_820291000000107 : Infectious disease notification</li>
 *   <li>_820301000000106 : Adoption report</li>
 *   <li>_820441000000103 : Weight chart</li>
 *   <li>_820451000000100 : Medical note</li>
 *   <li>_820461000000102 : Multidisciplinary note</li>
 *   <li>_820471000000109 : Nursing note</li>
 *   <li>_820481000000106 : Outpatient nursing note</li>
 *   <li>_820491000000108 : Outpatient medical note</li>
 *   <li>_820501000000102 : Nutritional record</li>
 *   <li>_820511000000100 : Endoscopy record</li>
 *   <li>_822751000000105 : Organ donor card</li>
 *   <li>_822761000000108 : Patient preferences record</li>
 *   <li>_822771000000101 : Pulmonary investigation record</li>
 *   <li>_822781000000104 : Vascular investigation record</li>
 *   <li>_822791000000102 : Cardiac investigation record</li>
 *   <li>_822801000000103 : Gastroenterology investigation record</li>
 *   <li>_822811000000101 : Neurological investigation record</li>
 *   <li>_822821000000107 : Echography report</li>
 *   <li>_822831000000109 : Audiology investigation record</li>
 *   <li>_823561000000105 : Pre-operative assessment record</li>
 *   <li>_823571000000103 : Scored assessment record</li>
 *   <li>_823581000000101 : Multidisciplinary care plan</li>
 *   <li>_823591000000104 : Operating theatre patient checklist</li>
 *   <li>_823601000000105 : Critical care chart</li>
 *   <li>_823611000000107 : Vital signs chart</li>
 *   <li>_823621000000101 : Observation chart</li>
 *   <li>_823631000000104 : OOH out of hours note</li>
 *   <li>_823641000000108 : AHP allied health professional note</li>
 *   <li>_823651000000106 : Clinical note</li>
 *   <li>_823661000000109 : Operation note</li>
 *   <li>_823681000000100 : Outpatient letter</li>
 *   <li>_823691000000103 : Clinical letter</li>
 *   <li>_823701000000103 : Discharge letter</li>
 *   <li>_823721000000107 : Social Services letter</li>
 *   <li>_823731000000109 : Transfer of care letter</li>
 *   <li>_823741000000100 : Admission letter</li>
 *   <li>_823751000000102 : Letter</li>
 *   <li>_823761000000104 : Administrative letter</li>
 *   <li>_823771000000106 : Unscheduled care letter</li>
 *   <li>_823781000000108 : MDT multidisciplinary team letter</li>
 *   <li>_823831000000103 : AHP allied health professional therapy record</li>
 *   <li>_823841000000107 : Radiotherapy record</li>
 *   <li>_823871000000101 : Forensic autopsy report</li>
 *   <li>_823881000000104 : Do not attempt cardiopulmonary resuscitation order</li>
 *   <li>_823891000000102 : Allied health professional investigation record</li>
 *   <li>_823901000000101 : Non-statutory provider document</li>
 *   <li>_823931000000107 : Private provider note</li>
 *   <li>_823941000000103 : Third party document</li>
 *   <li>_823951000000100 : Adult incapacity report</li>
 *   <li>_824231000000100 : Temperature chart</li>
 *   <li>_824321000000109 : Summary record</li>
 *   <li>_824331000000106 : Inpatient final discharge letter</li>
 *   <li>_824341000000102 : Immediate inpatient discharge letter</li>
 *   <li>_824781000000106 : Drug administration chart</li>
 *   <li>_824791000000108 : Prescription and administration record</li>
 *   <li>_824801000000107 : Patient master index sheet</li>
 *   <li>_824831000000101 : Consent form</li>
 *   <li>_824861000000106 : Ambulance service patient report form</li>
 *   <li>_826491000000106 : Intervention record</li>
 *   <li>_826501000000100 : Miscellaneous record</li>
 *   <li>_826511000000103 : Exemption form</li>
 *   <li>_826521000000109 : Refusal form</li>
 *   <li>_826541000000102 : Power of attorney and legal guardianship record</li>
 *   <li>_826621000000105 : Legal notice</li>
 *   <li>_826631000000107 : Mental Health Act notice</li>
 *   <li>_826651000000100 : Notification and legal document</li>
 *   <li>_827701000000106 : Living will and advance directive record</li>
 *   <li>_827711000000108 : Electrocardiograph</li>
 *   <li>_827721000000102 : Urodynamics record</li>
 *   <li>_829201000000105 : Inpatient nursing note</li>
 *   <li>_829581000000103 : Life insurance report</li>
 *   <li>_866371000000107 : Child Screening Report</li>
 *   <li>_908621000000106 : End of life care report</li>
 *   <li>_909031000000105 : Adult safeguarding report</li>
 *   <li>_909041000000101 : Child safeguarding report</li>
 *   <li>_909921000000109 : Did not attend letter</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum CorrespondenceDocumenttype implements VocabularyEntry {
	
	
	/**
	 * Report
	 */
	_229059009 ("229059009", "Report"),
	
	/**
	 * Driving licence fitness report
	 */
	_270370004 ("270370004", "Driving licence fitness report"),
	
	/**
	 * Disabled driver orange badge report
	 */
	_270372007 ("270372007", "Disabled driver orange badge report"),
	
	/**
	 * DSS RMO RM2 report
	 */
	_307881004 ("307881004", "DSS RMO RM2 report"),
	
	/**
	 * Death certificate
	 */
	_307930005 ("307930005", "Death certificate"),
	
	/**
	 * Employment report
	 */
	_308575004 ("308575004", "Employment report"),
	
	/**
	 * DLA 370 report
	 */
	_308584004 ("308584004", "DLA 370 report"),
	
	/**
	 * DS 1500 report
	 */
	_308585003 ("308585003", "DS 1500 report"),
	
	/**
	 * War pensions report
	 */
	_308619006 ("308619006", "War pensions report"),
	
	/**
	 * RM10-DHSS DMO report
	 */
	_308621001 ("308621001", "RM10-DHSS DMO report"),
	
	/**
	 * Housing report
	 */
	_310854009 ("310854009", "Housing report"),
	
	/**
	 * Radiology report
	 */
	_371527006 ("371527006", "Radiology report"),
	
	/**
	 * Discharge summary
	 */
	_373942005 ("373942005", "Discharge summary"),
	
	/**
	 * Patient held record
	 */
	_408403008 ("408403008", "Patient held record"),
	
	/**
	 * Anaesthesia record
	 */
	_416779005 ("416779005", "Anaesthesia record"),
	
	/**
	 * Emergency department record
	 */
	_445300006 ("445300006", "Emergency department record"),
	
	/**
	 * Investigation result
	 */
	_24641000000107 ("24641000000107", "Investigation result"),
	
	/**
	 * Mail to patient
	 */
	_24711000000100 ("24711000000100", "Mail to patient"),
	
	/**
	 * Referral letter
	 */
	_25611000000107 ("25611000000107", "Referral letter"),
	
	/**
	 * Mail from patient
	 */
	_25731000000109 ("25731000000109", "Mail from patient"),
	
	/**
	 * Image document
	 */
	_25831000000103 ("25831000000103", "Image (document)"),
	
	/**
	 * Alert note
	 */
	_37341000000109 ("37341000000109", "Alert note"),
	
	/**
	 * Medication Record
	 */
	_163111000000100 ("163111000000100", "Medication Record"),
	
	/**
	 * Allergies and Adverse Reactions
	 */
	_163221000000102 ("163221000000102", "Allergies and Adverse Reactions"),
	
	/**
	 * Care plan
	 */
	_325661000000106 ("325661000000106", "Care plan"),
	
	/**
	 * Assessment
	 */
	_325931000000109 ("325931000000109", "Assessment"),
	
	/**
	 * Record of fluid balance
	 */
	_526591000000108 ("526591000000108", "Record of fluid balance"),
	
	/**
	 * Fulfiller request
	 */
	_716921000000105 ("716921000000105", "Fulfiller request"),
	
	/**
	 * Diagnostic imaging report
	 */
	_717321000000108 ("717321000000108", "Diagnostic imaging report"),
	
	/**
	 * Nursing assessment record
	 */
	_819981000000101 ("819981000000101", "Nursing assessment record"),
	
	/**
	 * Allied health professional assessment record
	 */
	_819991000000104 ("819991000000104", "Allied health professional assessment record"),
	
	/**
	 * CAF common assessment framework assessment record
	 */
	_820011000000105 ("820011000000105", "CAF (common assessment framework) assessment record"),
	
	/**
	 * SSA - single shared assessment record
	 */
	_820021000000104 ("820021000000104", "SSA - single shared assessment record"),
	
	/**
	 * CPA care programme approach assessment record
	 */
	_820031000000102 ("820031000000102", "CPA (care programme approach) assessment record"),
	
	/**
	 * Multidisciplinary assessment record
	 */
	_820041000000106 ("820041000000106", "Multidisciplinary assessment record"),
	
	/**
	 * Pre-admission assessment record
	 */
	_820071000000100 ("820071000000100", "Pre-admission assessment record"),
	
	/**
	 * Self assessment record
	 */
	_820081000000103 ("820081000000103", "Self assessment record"),
	
	/**
	 * Medical assessment record
	 */
	_820091000000101 ("820091000000101", "Medical assessment record"),
	
	/**
	 * Social services assessment record
	 */
	_820101000000109 ("820101000000109", "Social services assessment record"),
	
	/**
	 * Discharge care plan
	 */
	_820121000000100 ("820121000000100", "Discharge care plan"),
	
	/**
	 * Record of fundal height measurement
	 */
	_820141000000107 ("820141000000107", "Record of fundal height measurement"),
	
	/**
	 * Growth chart
	 */
	_820161000000108 ("820161000000108", "Growth chart"),
	
	/**
	 * Partogram
	 */
	_820191000000102 ("820191000000102", "Partogram"),
	
	/**
	 * Patient safety checklist
	 */
	_820211000000103 ("820211000000103", "Patient safety checklist"),
	
	/**
	 * Inpatient medical note
	 */
	_820221000000109 ("820221000000109", "Inpatient medical note"),
	
	/**
	 * Medical photograph
	 */
	_820241000000102 ("820241000000102", "Medical photograph"),
	
	/**
	 * Interventional radiology record
	 */
	_820251000000104 ("820251000000104", "Interventional radiology record"),
	
	/**
	 * Controlled drug dispensing record
	 */
	_820261000000101 ("820261000000101", "Controlled drug dispensing record"),
	
	/**
	 * Chemotherapy record
	 */
	_820271000000108 ("820271000000108", "Chemotherapy record"),
	
	/**
	 * Infectious disease notification
	 */
	_820291000000107 ("820291000000107", "Infectious disease notification"),
	
	/**
	 * Adoption report
	 */
	_820301000000106 ("820301000000106", "Adoption report"),
	
	/**
	 * Weight chart
	 */
	_820441000000103 ("820441000000103", "Weight chart"),
	
	/**
	 * Medical note
	 */
	_820451000000100 ("820451000000100", "Medical note"),
	
	/**
	 * Multidisciplinary note
	 */
	_820461000000102 ("820461000000102", "Multidisciplinary note"),
	
	/**
	 * Nursing note
	 */
	_820471000000109 ("820471000000109", "Nursing note"),
	
	/**
	 * Outpatient nursing note
	 */
	_820481000000106 ("820481000000106", "Outpatient nursing note"),
	
	/**
	 * Outpatient medical note
	 */
	_820491000000108 ("820491000000108", "Outpatient medical note"),
	
	/**
	 * Nutritional record
	 */
	_820501000000102 ("820501000000102", "Nutritional record"),
	
	/**
	 * Endoscopy record
	 */
	_820511000000100 ("820511000000100", "Endoscopy record"),
	
	/**
	 * Organ donor card
	 */
	_822751000000105 ("822751000000105", "Organ donor card"),
	
	/**
	 * Patient preferences record
	 */
	_822761000000108 ("822761000000108", "Patient preferences record"),
	
	/**
	 * Pulmonary investigation record
	 */
	_822771000000101 ("822771000000101", "Pulmonary investigation record"),
	
	/**
	 * Vascular investigation record
	 */
	_822781000000104 ("822781000000104", "Vascular investigation record"),
	
	/**
	 * Cardiac investigation record
	 */
	_822791000000102 ("822791000000102", "Cardiac investigation record"),
	
	/**
	 * Gastroenterology investigation record
	 */
	_822801000000103 ("822801000000103", "Gastroenterology investigation record"),
	
	/**
	 * Neurological investigation record
	 */
	_822811000000101 ("822811000000101", "Neurological investigation record"),
	
	/**
	 * Echography report
	 */
	_822821000000107 ("822821000000107", "Echography report"),
	
	/**
	 * Audiology investigation record
	 */
	_822831000000109 ("822831000000109", "Audiology investigation record"),
	
	/**
	 * Pre-operative assessment record
	 */
	_823561000000105 ("823561000000105", "Pre-operative assessment record"),
	
	/**
	 * Scored assessment record
	 */
	_823571000000103 ("823571000000103", "Scored assessment record"),
	
	/**
	 * Multidisciplinary care plan
	 */
	_823581000000101 ("823581000000101", "Multidisciplinary care plan"),
	
	/**
	 * Operating theatre patient checklist
	 */
	_823591000000104 ("823591000000104", "Operating theatre patient checklist"),
	
	/**
	 * Critical care chart
	 */
	_823601000000105 ("823601000000105", "Critical care chart"),
	
	/**
	 * Vital signs chart
	 */
	_823611000000107 ("823611000000107", "Vital signs chart"),
	
	/**
	 * Observation chart
	 */
	_823621000000101 ("823621000000101", "Observation chart"),
	
	/**
	 * OOH out of hours note
	 */
	_823631000000104 ("823631000000104", "OOH (out of hours) note"),
	
	/**
	 * AHP allied health professional note
	 */
	_823641000000108 ("823641000000108", "AHP (allied health professional) note"),
	
	/**
	 * Clinical note
	 */
	_823651000000106 ("823651000000106", "Clinical note"),
	
	/**
	 * Operation note
	 */
	_823661000000109 ("823661000000109", "Operation note"),
	
	/**
	 * Outpatient letter
	 */
	_823681000000100 ("823681000000100", "Outpatient letter"),
	
	/**
	 * Clinical letter
	 */
	_823691000000103 ("823691000000103", "Clinical letter"),
	
	/**
	 * Discharge letter
	 */
	_823701000000103 ("823701000000103", "Discharge letter"),
	
	/**
	 * Social Services letter
	 */
	_823721000000107 ("823721000000107", "Social Services letter"),
	
	/**
	 * Transfer of care letter
	 */
	_823731000000109 ("823731000000109", "Transfer of care letter"),
	
	/**
	 * Admission letter
	 */
	_823741000000100 ("823741000000100", "Admission letter"),
	
	/**
	 * Letter
	 */
	_823751000000102 ("823751000000102", "Letter"),
	
	/**
	 * Administrative letter
	 */
	_823761000000104 ("823761000000104", "Administrative letter"),
	
	/**
	 * Unscheduled care letter
	 */
	_823771000000106 ("823771000000106", "Unscheduled care letter"),
	
	/**
	 * MDT multidisciplinary team letter
	 */
	_823781000000108 ("823781000000108", "MDT (multidisciplinary team) letter"),
	
	/**
	 * AHP allied health professional therapy record
	 */
	_823831000000103 ("823831000000103", "AHP (allied health professional) therapy record"),
	
	/**
	 * Radiotherapy record
	 */
	_823841000000107 ("823841000000107", "Radiotherapy record"),
	
	/**
	 * Forensic autopsy report
	 */
	_823871000000101 ("823871000000101", "Forensic autopsy report"),
	
	/**
	 * Do not attempt cardiopulmonary resuscitation order
	 */
	_823881000000104 ("823881000000104", "Do not attempt cardiopulmonary resuscitation order"),
	
	/**
	 * Allied health professional investigation record
	 */
	_823891000000102 ("823891000000102", "Allied health professional investigation record"),
	
	/**
	 * Non-statutory provider document
	 */
	_823901000000101 ("823901000000101", "Non-statutory provider document"),
	
	/**
	 * Private provider note
	 */
	_823931000000107 ("823931000000107", "Private provider note"),
	
	/**
	 * Third party document
	 */
	_823941000000103 ("823941000000103", "Third party document"),
	
	/**
	 * Adult incapacity report
	 */
	_823951000000100 ("823951000000100", "Adult incapacity report"),
	
	/**
	 * Temperature chart
	 */
	_824231000000100 ("824231000000100", "Temperature chart"),
	
	/**
	 * Summary record
	 */
	_824321000000109 ("824321000000109", "Summary record"),
	
	/**
	 * Inpatient final discharge letter
	 */
	_824331000000106 ("824331000000106", "Inpatient final discharge letter"),
	
	/**
	 * Immediate inpatient discharge letter
	 */
	_824341000000102 ("824341000000102", "Immediate inpatient discharge letter"),
	
	/**
	 * Drug administration chart
	 */
	_824781000000106 ("824781000000106", "Drug administration chart"),
	
	/**
	 * Prescription and administration record
	 */
	_824791000000108 ("824791000000108", "Prescription and administration record"),
	
	/**
	 * Patient master index sheet
	 */
	_824801000000107 ("824801000000107", "Patient master index sheet"),
	
	/**
	 * Consent form
	 */
	_824831000000101 ("824831000000101", "Consent form"),
	
	/**
	 * Ambulance service patient report form
	 */
	_824861000000106 ("824861000000106", "Ambulance service patient report form"),
	
	/**
	 * Intervention record
	 */
	_826491000000106 ("826491000000106", "Intervention record"),
	
	/**
	 * Miscellaneous record
	 */
	_826501000000100 ("826501000000100", "Miscellaneous record"),
	
	/**
	 * Exemption form
	 */
	_826511000000103 ("826511000000103", "Exemption form"),
	
	/**
	 * Refusal form
	 */
	_826521000000109 ("826521000000109", "Refusal form"),
	
	/**
	 * Power of attorney and legal guardianship record
	 */
	_826541000000102 ("826541000000102", "Power of attorney and legal guardianship record"),
	
	/**
	 * Legal notice
	 */
	_826621000000105 ("826621000000105", "Legal notice"),
	
	/**
	 * Mental Health Act notice
	 */
	_826631000000107 ("826631000000107", "Mental Health Act notice"),
	
	/**
	 * Notification and legal document
	 */
	_826651000000100 ("826651000000100", "Notification and legal document"),
	
	/**
	 * Living will and advance directive record
	 */
	_827701000000106 ("827701000000106", "Living will and advance directive record"),
	
	/**
	 * Electrocardiograph
	 */
	_827711000000108 ("827711000000108", "Electrocardiograph"),
	
	/**
	 * Urodynamics record
	 */
	_827721000000102 ("827721000000102", "Urodynamics record"),
	
	/**
	 * Inpatient nursing note
	 */
	_829201000000105 ("829201000000105", "Inpatient nursing note"),
	
	/**
	 * Life insurance report
	 */
	_829581000000103 ("829581000000103", "Life insurance report"),
	
	/**
	 * Child Screening Report
	 */
	_866371000000107 ("866371000000107", "Child Screening Report"),
	
	/**
	 * End of life care report
	 */
	_908621000000106 ("908621000000106", "End of life care report"),
	
	/**
	 * Adult safeguarding report
	 */
	_909031000000105 ("909031000000105", "Adult safeguarding report"),
	
	/**
	 * Child safeguarding report
	 */
	_909041000000101 ("909041000000101", "Child safeguarding report"),
	
	/**
	 * Did not attend letter
	 */
	_909921000000109 ("909921000000109", "Did not attend letter"),
	
	/**
	 * Report
	 */
	_Report ("229059009", "Report"),
	
	/**
	 * Driving licence fitness report
	 */
	_Drivinglicencefitnessreport ("270370004", "Driving licence fitness report"),
	
	/**
	 * Disabled driver orange badge report
	 */
	_Disableddriverorangebadgereport ("270372007", "Disabled driver orange badge report"),
	
	/**
	 * DSS RMO RM2 report
	 */
	_DSSRMORM2report ("307881004", "DSS RMO RM2 report"),
	
	/**
	 * Death certificate
	 */
	_Deathcertificate ("307930005", "Death certificate"),
	
	/**
	 * Employment report
	 */
	_Employmentreport ("308575004", "Employment report"),
	
	/**
	 * DLA 370 report
	 */
	_DLA370report ("308584004", "DLA 370 report"),
	
	/**
	 * DS 1500 report
	 */
	_DS1500report ("308585003", "DS 1500 report"),
	
	/**
	 * War pensions report
	 */
	_Warpensionsreport ("308619006", "War pensions report"),
	
	/**
	 * RM10-DHSS DMO report
	 */
	_RM10DHSSDMOreport ("308621001", "RM10-DHSS DMO report"),
	
	/**
	 * Housing report
	 */
	_Housingreport ("310854009", "Housing report"),
	
	/**
	 * Radiology report
	 */
	_Radiologyreport ("371527006", "Radiology report"),
	
	/**
	 * Discharge summary
	 */
	_Dischargesummary ("373942005", "Discharge summary"),
	
	/**
	 * Patient held record
	 */
	_Patientheldrecord ("408403008", "Patient held record"),
	
	/**
	 * Anaesthesia record
	 */
	_Anaesthesiarecord ("416779005", "Anaesthesia record"),
	
	/**
	 * Emergency department record
	 */
	_Emergencydepartmentrecord ("445300006", "Emergency department record"),
	
	/**
	 * Investigation result
	 */
	_Investigationresult ("24641000000107", "Investigation result"),
	
	/**
	 * Mail to patient
	 */
	_Mailtopatient ("24711000000100", "Mail to patient"),
	
	/**
	 * Referral letter
	 */
	_Referralletter ("25611000000107", "Referral letter"),
	
	/**
	 * Mail from patient
	 */
	_Mailfrompatient ("25731000000109", "Mail from patient"),
	
	/**
	 * Image document
	 */
	_Imagedocument ("25831000000103", "Image (document)"),
	
	/**
	 * Alert note
	 */
	_Alertnote ("37341000000109", "Alert note"),
	
	/**
	 * Medication Record
	 */
	_MedicationRecord ("163111000000100", "Medication Record"),
	
	/**
	 * Allergies and Adverse Reactions
	 */
	_AllergiesandAdverseReactions ("163221000000102", "Allergies and Adverse Reactions"),
	
	/**
	 * Care plan
	 */
	_Careplan ("325661000000106", "Care plan"),
	
	/**
	 * Assessment
	 */
	_Assessment ("325931000000109", "Assessment"),
	
	/**
	 * Record of fluid balance
	 */
	_Recordoffluidbalance ("526591000000108", "Record of fluid balance"),
	
	/**
	 * Fulfiller request
	 */
	_Fulfillerrequest ("716921000000105", "Fulfiller request"),
	
	/**
	 * Diagnostic imaging report
	 */
	_Diagnosticimagingreport ("717321000000108", "Diagnostic imaging report"),
	
	/**
	 * Nursing assessment record
	 */
	_Nursingassessmentrecord ("819981000000101", "Nursing assessment record"),
	
	/**
	 * Allied health professional assessment record
	 */
	_Alliedhealthprofessionalassessmentrecord ("819991000000104", "Allied health professional assessment record"),
	
	/**
	 * CAF common assessment framework assessment record
	 */
	_CAFcommonassessmentframeworkassessmentrecord ("820011000000105", "CAF (common assessment framework) assessment record"),
	
	/**
	 * SSA - single shared assessment record
	 */
	_SSAsinglesharedassessmentrecord ("820021000000104", "SSA - single shared assessment record"),
	
	/**
	 * CPA care programme approach assessment record
	 */
	_CPAcareprogrammeapproachassessmentrecord ("820031000000102", "CPA (care programme approach) assessment record"),
	
	/**
	 * Multidisciplinary assessment record
	 */
	_Multidisciplinaryassessmentrecord ("820041000000106", "Multidisciplinary assessment record"),
	
	/**
	 * Pre-admission assessment record
	 */
	_Preadmissionassessmentrecord ("820071000000100", "Pre-admission assessment record"),
	
	/**
	 * Self assessment record
	 */
	_Selfassessmentrecord ("820081000000103", "Self assessment record"),
	
	/**
	 * Medical assessment record
	 */
	_Medicalassessmentrecord ("820091000000101", "Medical assessment record"),
	
	/**
	 * Social services assessment record
	 */
	_Socialservicesassessmentrecord ("820101000000109", "Social services assessment record"),
	
	/**
	 * Discharge care plan
	 */
	_Dischargecareplan ("820121000000100", "Discharge care plan"),
	
	/**
	 * Record of fundal height measurement
	 */
	_Recordoffundalheightmeasurement ("820141000000107", "Record of fundal height measurement"),
	
	/**
	 * Growth chart
	 */
	_Growthchart ("820161000000108", "Growth chart"),
	
	/**
	 * Partogram
	 */
	_Partogram ("820191000000102", "Partogram"),
	
	/**
	 * Patient safety checklist
	 */
	_Patientsafetychecklist ("820211000000103", "Patient safety checklist"),
	
	/**
	 * Inpatient medical note
	 */
	_Inpatientmedicalnote ("820221000000109", "Inpatient medical note"),
	
	/**
	 * Medical photograph
	 */
	_Medicalphotograph ("820241000000102", "Medical photograph"),
	
	/**
	 * Interventional radiology record
	 */
	_Interventionalradiologyrecord ("820251000000104", "Interventional radiology record"),
	
	/**
	 * Controlled drug dispensing record
	 */
	_Controlleddrugdispensingrecord ("820261000000101", "Controlled drug dispensing record"),
	
	/**
	 * Chemotherapy record
	 */
	_Chemotherapyrecord ("820271000000108", "Chemotherapy record"),
	
	/**
	 * Infectious disease notification
	 */
	_Infectiousdiseasenotification ("820291000000107", "Infectious disease notification"),
	
	/**
	 * Adoption report
	 */
	_Adoptionreport ("820301000000106", "Adoption report"),
	
	/**
	 * Weight chart
	 */
	_Weightchart ("820441000000103", "Weight chart"),
	
	/**
	 * Medical note
	 */
	_Medicalnote ("820451000000100", "Medical note"),
	
	/**
	 * Multidisciplinary note
	 */
	_Multidisciplinarynote ("820461000000102", "Multidisciplinary note"),
	
	/**
	 * Nursing note
	 */
	_Nursingnote ("820471000000109", "Nursing note"),
	
	/**
	 * Outpatient nursing note
	 */
	_Outpatientnursingnote ("820481000000106", "Outpatient nursing note"),
	
	/**
	 * Outpatient medical note
	 */
	_Outpatientmedicalnote ("820491000000108", "Outpatient medical note"),
	
	/**
	 * Nutritional record
	 */
	_Nutritionalrecord ("820501000000102", "Nutritional record"),
	
	/**
	 * Endoscopy record
	 */
	_Endoscopyrecord ("820511000000100", "Endoscopy record"),
	
	/**
	 * Organ donor card
	 */
	_Organdonorcard ("822751000000105", "Organ donor card"),
	
	/**
	 * Patient preferences record
	 */
	_Patientpreferencesrecord ("822761000000108", "Patient preferences record"),
	
	/**
	 * Pulmonary investigation record
	 */
	_Pulmonaryinvestigationrecord ("822771000000101", "Pulmonary investigation record"),
	
	/**
	 * Vascular investigation record
	 */
	_Vascularinvestigationrecord ("822781000000104", "Vascular investigation record"),
	
	/**
	 * Cardiac investigation record
	 */
	_Cardiacinvestigationrecord ("822791000000102", "Cardiac investigation record"),
	
	/**
	 * Gastroenterology investigation record
	 */
	_Gastroenterologyinvestigationrecord ("822801000000103", "Gastroenterology investigation record"),
	
	/**
	 * Neurological investigation record
	 */
	_Neurologicalinvestigationrecord ("822811000000101", "Neurological investigation record"),
	
	/**
	 * Echography report
	 */
	_Echographyreport ("822821000000107", "Echography report"),
	
	/**
	 * Audiology investigation record
	 */
	_Audiologyinvestigationrecord ("822831000000109", "Audiology investigation record"),
	
	/**
	 * Pre-operative assessment record
	 */
	_Preoperativeassessmentrecord ("823561000000105", "Pre-operative assessment record"),
	
	/**
	 * Scored assessment record
	 */
	_Scoredassessmentrecord ("823571000000103", "Scored assessment record"),
	
	/**
	 * Multidisciplinary care plan
	 */
	_Multidisciplinarycareplan ("823581000000101", "Multidisciplinary care plan"),
	
	/**
	 * Operating theatre patient checklist
	 */
	_Operatingtheatrepatientchecklist ("823591000000104", "Operating theatre patient checklist"),
	
	/**
	 * Critical care chart
	 */
	_Criticalcarechart ("823601000000105", "Critical care chart"),
	
	/**
	 * Vital signs chart
	 */
	_Vitalsignschart ("823611000000107", "Vital signs chart"),
	
	/**
	 * Observation chart
	 */
	_Observationchart ("823621000000101", "Observation chart"),
	
	/**
	 * OOH out of hours note
	 */
	_OOHoutofhoursnote ("823631000000104", "OOH (out of hours) note"),
	
	/**
	 * AHP allied health professional note
	 */
	_AHPalliedhealthprofessionalnote ("823641000000108", "AHP (allied health professional) note"),
	
	/**
	 * Clinical note
	 */
	_Clinicalnote ("823651000000106", "Clinical note"),
	
	/**
	 * Operation note
	 */
	_Operationnote ("823661000000109", "Operation note"),
	
	/**
	 * Outpatient letter
	 */
	_Outpatientletter ("823681000000100", "Outpatient letter"),
	
	/**
	 * Clinical letter
	 */
	_Clinicalletter ("823691000000103", "Clinical letter"),
	
	/**
	 * Discharge letter
	 */
	_Dischargeletter ("823701000000103", "Discharge letter"),
	
	/**
	 * Social Services letter
	 */
	_SocialServicesletter ("823721000000107", "Social Services letter"),
	
	/**
	 * Transfer of care letter
	 */
	_Transferofcareletter ("823731000000109", "Transfer of care letter"),
	
	/**
	 * Admission letter
	 */
	_Admissionletter ("823741000000100", "Admission letter"),
	
	/**
	 * Letter
	 */
	_Letter ("823751000000102", "Letter"),
	
	/**
	 * Administrative letter
	 */
	_Administrativeletter ("823761000000104", "Administrative letter"),
	
	/**
	 * Unscheduled care letter
	 */
	_Unscheduledcareletter ("823771000000106", "Unscheduled care letter"),
	
	/**
	 * MDT multidisciplinary team letter
	 */
	_MDTmultidisciplinaryteamletter ("823781000000108", "MDT (multidisciplinary team) letter"),
	
	/**
	 * AHP allied health professional therapy record
	 */
	_AHPalliedhealthprofessionaltherapyrecord ("823831000000103", "AHP (allied health professional) therapy record"),
	
	/**
	 * Radiotherapy record
	 */
	_Radiotherapyrecord ("823841000000107", "Radiotherapy record"),
	
	/**
	 * Forensic autopsy report
	 */
	_Forensicautopsyreport ("823871000000101", "Forensic autopsy report"),
	
	/**
	 * Do not attempt cardiopulmonary resuscitation order
	 */
	_Donotattemptcardiopulmonaryresuscitationorder ("823881000000104", "Do not attempt cardiopulmonary resuscitation order"),
	
	/**
	 * Allied health professional investigation record
	 */
	_Alliedhealthprofessionalinvestigationrecord ("823891000000102", "Allied health professional investigation record"),
	
	/**
	 * Non-statutory provider document
	 */
	_Nonstatutoryproviderdocument ("823901000000101", "Non-statutory provider document"),
	
	/**
	 * Private provider note
	 */
	_Privateprovidernote ("823931000000107", "Private provider note"),
	
	/**
	 * Third party document
	 */
	_Thirdpartydocument ("823941000000103", "Third party document"),
	
	/**
	 * Adult incapacity report
	 */
	_Adultincapacityreport ("823951000000100", "Adult incapacity report"),
	
	/**
	 * Temperature chart
	 */
	_Temperaturechart ("824231000000100", "Temperature chart"),
	
	/**
	 * Summary record
	 */
	_Summaryrecord ("824321000000109", "Summary record"),
	
	/**
	 * Inpatient final discharge letter
	 */
	_Inpatientfinaldischargeletter ("824331000000106", "Inpatient final discharge letter"),
	
	/**
	 * Immediate inpatient discharge letter
	 */
	_Immediateinpatientdischargeletter ("824341000000102", "Immediate inpatient discharge letter"),
	
	/**
	 * Drug administration chart
	 */
	_Drugadministrationchart ("824781000000106", "Drug administration chart"),
	
	/**
	 * Prescription and administration record
	 */
	_Prescriptionandadministrationrecord ("824791000000108", "Prescription and administration record"),
	
	/**
	 * Patient master index sheet
	 */
	_Patientmasterindexsheet ("824801000000107", "Patient master index sheet"),
	
	/**
	 * Consent form
	 */
	_Consentform ("824831000000101", "Consent form"),
	
	/**
	 * Ambulance service patient report form
	 */
	_Ambulanceservicepatientreportform ("824861000000106", "Ambulance service patient report form"),
	
	/**
	 * Intervention record
	 */
	_Interventionrecord ("826491000000106", "Intervention record"),
	
	/**
	 * Miscellaneous record
	 */
	_Miscellaneousrecord ("826501000000100", "Miscellaneous record"),
	
	/**
	 * Exemption form
	 */
	_Exemptionform ("826511000000103", "Exemption form"),
	
	/**
	 * Refusal form
	 */
	_Refusalform ("826521000000109", "Refusal form"),
	
	/**
	 * Power of attorney and legal guardianship record
	 */
	_Powerofattorneyandlegalguardianshiprecord ("826541000000102", "Power of attorney and legal guardianship record"),
	
	/**
	 * Legal notice
	 */
	_Legalnotice ("826621000000105", "Legal notice"),
	
	/**
	 * Mental Health Act notice
	 */
	_MentalHealthActnotice ("826631000000107", "Mental Health Act notice"),
	
	/**
	 * Notification and legal document
	 */
	_Notificationandlegaldocument ("826651000000100", "Notification and legal document"),
	
	/**
	 * Living will and advance directive record
	 */
	_Livingwillandadvancedirectiverecord ("827701000000106", "Living will and advance directive record"),
	
	/**
	 * Electrocardiograph
	 */
	_Electrocardiograph ("827711000000108", "Electrocardiograph"),
	
	/**
	 * Urodynamics record
	 */
	_Urodynamicsrecord ("827721000000102", "Urodynamics record"),
	
	/**
	 * Inpatient nursing note
	 */
	_Inpatientnursingnote ("829201000000105", "Inpatient nursing note"),
	
	/**
	 * Life insurance report
	 */
	_Lifeinsurancereport ("829581000000103", "Life insurance report"),
	
	/**
	 * Child Screening Report
	 */
	_ChildScreeningReport ("866371000000107", "Child Screening Report"),
	
	/**
	 * End of life care report
	 */
	_Endoflifecarereport ("908621000000106", "End of life care report"),
	
	/**
	 * Adult safeguarding report
	 */
	_Adultsafeguardingreport ("909031000000105", "Adult safeguarding report"),
	
	/**
	 * Child safeguarding report
	 */
	_Childsafeguardingreport ("909041000000101", "Child safeguarding report"),
	
	/**
	 * Did not attend letter
	 */
	_Didnotattendletter ("909921000000109", "Did not attend letter"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	CorrespondenceDocumenttype(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static CorrespondenceDocumenttype getByCode(String code) {
		CorrespondenceDocumenttype[] vals = values();
		for (CorrespondenceDocumenttype val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(CorrespondenceDocumenttype other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	