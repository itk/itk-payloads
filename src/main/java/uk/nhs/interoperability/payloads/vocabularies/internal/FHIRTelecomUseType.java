/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

public enum FHIRTelecomUseType implements VocabularyEntry {
	/** A communication contact at a home; attempted contacts for business purposes might intrude
	  * privacy and chances are one will contact family or other household members instead of the
	  * person one wishes to call. Typically used with urgent cases, or if no other contacts are available. */
	home("home"),
	/** An office contact. First choice for business related contacts during business hours. */
	work("work"),
	/** A temporary contact. The period can provide more detailed information. */
	temp("temp"),
	/** This contact is no longer in use (or was never correct, but retained for records). */
	old("old"),
	/** A telecommunication device that moves and stays with its owner. May have characteristics of
	  * all other use codes, suitable for urgent matters, not the first choice for routine business. */
	mobile("mobile");	
	
	public String code;
	
	private FHIRTelecomUseType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static FHIRTelecomUseType getByCode(String code) {
		FHIRTelecomUseType[] vals = values();
		for (FHIRTelecomUseType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(FHIRTelecomUseType other) {
		return (other.getCode().equals(code));
	}
}
