/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Enumeration of the levels of precision within a date object.
 * <br>Examples:
 * <ul>
 * <li>2013 = DatePrecision.Years</li>
 * <li>201301 = DatePrecision.Months</li>
 * <li>20130101 = DatePrecision.Days</li>
 * <li>2013010112 = DatePrecision.Hours</li>
 * <li>2013010112+0 = DatePrecision.HoursWithTimezone</li>
 * <li>201301011201 = DatePrecision.Minutes</li>
 * <li>201301011201+0 = DatePrecision.MinutesWithTimezone</li>
 * <li>20130101120101 = DatePrecision.Seconds</li>
 * <li>20130101120101+0 = DatePrecision.SecondsWithTimezone</li>
 * <li>20130101120101.01 = DatePrecision.Milliseconds</li>
 * <li>20130101120101.01+0 = DatePrecision.MillisecondsWithTimezone</li>
 * </ul>
 * @author Adam Hatherly
 */
public enum DatePrecision {
	/** Milliseconds: yyyyMMddHHmms.S */
	Milliseconds("yyyyMMddHHmmss.S", "dd-MMM-yyyy HH:mm:ss", "([0-9]{14,14}\\.[0-9]+)"),
	/** Milliseconds with Timezone: yyyyMMddHHmms.SZ */
	MillisecondsWithTimezone("yyyyMMddHHmmss.SZ", "dd-MMM-yyyy HH:mm:ss", "([0-9]{14,14}\\.[0-9]+)([+\\-][0-9]{1,4})"),
	/** Seconds: yyyyMMddHHmms */
	Seconds("yyyyMMddHHmmss", "dd-MMM-yyyy HH:mm:ss", "([0-9]{14,14})"),
	/** Seconds with Timezone: yyyyMMddHHmmsZ */
	SecondsWithTimezone("yyyyMMddHHmmssZ", "dd-MMM-yyyy HH:mm:ss", "([0-9]{14,14})([+\\-][0-9]{1,4})"),
	/** Minutes: yyyyMMddHHmm */
	Minutes("yyyyMMddHHmm", "dd-MMM-yyyy HH:mm", "([0-9]{12,12})"),
	/** Minutes with Timezone: yyyyMMddHHmmZ */
	MinutesWithTimezone("yyyyMMddHHmmZ", "dd-MMM-yyyy HH:mm", "([0-9]{12,12})([+\\-][0-9]{1,4})"),
	/** Hours: yyyyMMddHH */
	Hours("yyyyMMddHH", "dd-MMM-yyyy HH", "([0-9]{10,10})"),
	/** Hours with Timezone: yyyyMMddHHZ */
	HoursWithTimezone("yyyyMMddHHZ", "dd-MMM-yyyy HH", "([0-9]{10,10})([+\\-][0-9]{1,4})"),
	/** Days: yyyyMMdd */
	Days("yyyyMMdd", "dd-MMM-yyyy", "[0-9]{8}"),
	/** Months: yyyyMM */
	Months("yyyyMM", "MMM-yyyy", "[0-9]{6}"),
	/** Years: yyyy */
	Years("yyyy", "yyyy", "[0-9]{4}");
	
	private static final Logger logger = LoggerFactory.getLogger(DatePrecision.class);
	public String formatString;
	public ThreadLocal<SimpleDateFormat> formatter;
	public ThreadLocal<SimpleDateFormat> displayFormatter;
	public Pattern regEx;
	
	private DatePrecision(final String formatString, final String displayFormatString, String regEx) {
		this.formatString = formatString;
		formatter = new ThreadLocal<SimpleDateFormat>() {
			protected SimpleDateFormat initialValue() {
				return new SimpleDateFormat(formatString);
			}
		};
		displayFormatter = new ThreadLocal<SimpleDateFormat>() {
			protected SimpleDateFormat initialValue() {
				return new SimpleDateFormat(displayFormatString);
			}
		};
		this.regEx = Pattern.compile(regEx);
	}

	public static DatePrecision getPrecision(String pattern) {
		for (DatePrecision p : DatePrecision.values()) {
			if(p.regEx.matcher(pattern).matches()) {
				return p;
			}
		}
		return null;
	}
	
	public String makeString(Date date) {
		return formatter.get().format(date);
	}
	
	public String makeDisplayString(Date date) {
		return displayFormatter.get().format(date);
	}
	
	public Date makeDate(String val) {
		try {
			return formatter.get().parse(val);
		} catch (ParseException e) {
			logger.error("Cannot convert '{}' to a date", val, e);
			return null;
		}
	}

	public String getFormatString() {
		return this.formatString;
	}
}
