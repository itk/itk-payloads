/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the ActStatus vocabulary:
 * <ul>
 *   <li>_aborted : aborted</li>
 *   <li>_active : active</li>
 *   <li>_cancelled : cancelled</li>
 *   <li>_completed : completed</li>
 *   <li>_held : held</li>
 *   <li>_new : new</li>
 *   <li>_normal : normal</li>
 *   <li>_nullified : nullified</li>
 *   <li>_obsolete : obsolete</li>
 *   <li>_suspended : suspended</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum ActStatus implements VocabularyEntry {
	
	
	/**
	 * aborted
	 */
	_aborted ("aborted", "aborted"),
	
	/**
	 * active
	 */
	_active ("active", "active"),
	
	/**
	 * cancelled
	 */
	_cancelled ("cancelled", "cancelled"),
	
	/**
	 * completed
	 */
	_completed ("completed", "completed"),
	
	/**
	 * held
	 */
	_held ("held", "held"),
	
	/**
	 * new
	 */
	_new ("new", "new"),
	
	/**
	 * normal
	 */
	_normal ("normal", "normal"),
	
	/**
	 * nullified
	 */
	_nullified ("nullified", "nullified"),
	
	/**
	 * obsolete
	 */
	_obsolete ("obsolete", "obsolete"),
	
	/**
	 * suspended
	 */
	_suspended ("suspended", "suspended"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.5.14";

	ActStatus(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static ActStatus getByCode(String code) {
		ActStatus[] vals = values();
		for (ActStatus val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(ActStatus other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	