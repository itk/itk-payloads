/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the PDSMiniServiceResponseCode vocabulary:
 * <ul>
 *   <li>_DEMOG0001 : No Match</li>
 *   <li>_DEMOG0007 : Multiple matches found</li>
 *   <li>_DEMOG0017 : Superceding NHS number returned</li>
 *   <li>_DEMOG0022 : NHS Number of response record has been invalidated</li>
 *   <li>_DEMOG0040 : NHS number not verified</li>
 *   <li>_DEMOG0042 : NHS Number is not a new style number</li>
 *   <li>_DEMOG9999 : Generic Spine Service Error</li>
 *   <li>_SMSP0000 : Success</li>
 *   <li>_SMSP0001 : Input message validation error</li>
 *   <li>_SMSP0002 : Response message validation error</li>
 *   <li>_SMSP9999 : Generic Spine Mini Service Provider software failure</li>
 *   <li>_SMSP0003 : Data returned from local store, Spine unavailable</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum PDSMiniServiceResponseCode implements VocabularyEntry {
	
	
	/**
	 * No Match
	 */
	_DEMOG0001 ("DEMOG0001", "No Match"),
	
	/**
	 * Multiple matches found
	 */
	_DEMOG0007 ("DEMOG0007", "Multiple matches found"),
	
	/**
	 * Superceding NHS number returned
	 */
	_DEMOG0017 ("DEMOG0017", "Superceding NHS number returned"),
	
	/**
	 * NHS Number of response record has been invalidated
	 */
	_DEMOG0022 ("DEMOG0022", "NHS Number of response record has been invalidated"),
	
	/**
	 * NHS number not verified
	 */
	_DEMOG0040 ("DEMOG0040", "NHS number not verified"),
	
	/**
	 * NHS Number is not a new style number
	 */
	_DEMOG0042 ("DEMOG0042", "NHS Number is not a new style number"),
	
	/**
	 * Generic Spine Service Error
	 */
	_DEMOG9999 ("DEMOG9999", "Generic Spine Service Error"),
	
	/**
	 * Success
	 */
	_SMSP0000 ("SMSP0000", "Success"),
	
	/**
	 * Input message validation error
	 */
	_SMSP0001 ("SMSP0001", "Input message validation error"),
	
	/**
	 * Response message validation error
	 */
	_SMSP0002 ("SMSP0002", "Response message validation error"),
	
	/**
	 * Generic Spine Mini Service Provider software failure
	 */
	_SMSP9999 ("SMSP9999", "Generic Spine Mini Service Provider software failure"),
	
	/**
	 * Data returned from local store, Spine unavailable
	 */
	_SMSP0003 ("SMSP0003", "Data returned from local store, Spine unavailable"),
	
	/**
	 * No Match
	 */
	_NoMatch ("DEMOG-0001", "No Match"),
	
	/**
	 * Multiple matches found
	 */
	_Multiplematchesfound ("DEMOG-0007", "Multiple matches found"),
	
	/**
	 * Superceding NHS number returned
	 */
	_SupercedingNHSnumberreturned ("DEMOG-0017", "Superceding NHS number returned"),
	
	/**
	 * NHS Number of response record has been invalidated
	 */
	_NHSNumberofresponserecordhasbeeninvalidated ("DEMOG-0022", "NHS Number of response record has been invalidated"),
	
	/**
	 * NHS number not verified
	 */
	_NHSnumbernotverified ("DEMOG-0040", "NHS number not verified"),
	
	/**
	 * NHS Number is not a new style number
	 */
	_NHSNumberisnotanewstylenumber ("DEMOG-0042", "NHS Number is not a new style number"),
	
	/**
	 * Generic Spine Service Error
	 */
	_GenericSpineServiceError ("DEMOG-9999", "Generic Spine Service Error"),
	
	/**
	 * Success
	 */
	_Success ("SMSP-0000", "Success"),
	
	/**
	 * Input message validation error
	 */
	_Inputmessagevalidationerror ("SMSP-0001", "Input message validation error"),
	
	/**
	 * Response message validation error
	 */
	_Responsemessagevalidationerror ("SMSP-0002", "Response message validation error"),
	
	/**
	 * Generic Spine Mini Service Provider software failure
	 */
	_GenericSpineMiniServiceProvidersoftwarefailure ("SMSP-9999", "Generic Spine Mini Service Provider software failure"),
	
	/**
	 * Data returned from local store, Spine unavailable
	 */
	_DatareturnedfromlocalstoreSpineunavailable ("SMSP-0003", "Data returned from local store, Spine unavailable"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.285";

	PDSMiniServiceResponseCode(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static PDSMiniServiceResponseCode getByCode(String code) {
		PDSMiniServiceResponseCode[] vals = values();
		for (PDSMiniServiceResponseCode val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(PDSMiniServiceResponseCode other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	