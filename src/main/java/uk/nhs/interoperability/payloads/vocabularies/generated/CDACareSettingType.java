/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the CDACareSettingType vocabulary:
 * <ul>
 *   <li>_331006 : Hospital-based outpatient rheumatology clinic</li>
 *   <li>_901005 : Helicopter-based care</li>
 *   <li>_1348009 : Day care centre</li>
 *   <li>_1526002 : Free-standing breast clinic</li>
 *   <li>_1773006 : Free-standing radiology facility</li>
 *   <li>_1814000 : Hospital-based outpatient geriatric clinic</li>
 *   <li>_2081004 : Hospital ship</li>
 *   <li>_2849009 : Hospital-based outpatient infectious disease clinic</li>
 *   <li>_2961002 : Aid car-based care</li>
 *   <li>_3729002 : Hospital-based outpatient paediatric clinic</li>
 *   <li>_3768009 : Free-standing haematology clinic</li>
 *   <li>_4322002 : Military field hospital</li>
 *   <li>_5584006 : Hospital-based outpatient peripheral vascular clinic</li>
 *   <li>_6827000 : Local community health centre</li>
 *   <li>_10206005 : Hospital-based outpatient dental clinic</li>
 *   <li>_10531005 : Free-standing ambulatory surgery facility</li>
 *   <li>_11119007 : Free-standing peripheral vascular clinic</li>
 *   <li>_11424001 : Ambulance-based care</li>
 *   <li>_13015006 : Free-standing gastroenterology clinic</li>
 *   <li>_13656005 : Free-standing physical medicine clinic</li>
 *   <li>_14866005 : Hospital-based outpatient mental health clinic</li>
 *   <li>_16384001 : Skilled nursing facility, level 2 care</li>
 *   <li>_16597003 : Free-standing ophthalmology clinic</li>
 *   <li>_16879004 : Free-standing neurology clinic</li>
 *   <li>_18095007 : Intermediate care hospital for mentally retarded</li>
 *   <li>_19602009 : Fee-for-service private physicians group office</li>
 *   <li>_20078004 : Substance abuse treatment centre</li>
 *   <li>_20316001 : Tertiary care hospital</li>
 *   <li>_21700006 : Free-standing dermatology clinic</li>
 *   <li>_22201008 : Free-standing dental clinic</li>
 *   <li>_22232009 : Hospital</li>
 *   <li>_22357001 : Skilled nursing facility, level 1 care</li>
 *   <li>_22549003 : Hospital-based outpatient gynaecology clinic</li>
 *   <li>_23392004 : Hospital-based outpatient otorhinolaryngology clinic</li>
 *   <li>_25567001 : Hospital-based outpatient breast clinic</li>
 *   <li>_25681007 : Sexually transmitted disease clinic</li>
 *   <li>_30629002 : Retirement home</li>
 *   <li>_31154006 : Prepaid private physicians group office</li>
 *   <li>_31628002 : Hospital-based outpatient family medicine clinic</li>
 *   <li>_32074000 : Long term care hospital</li>
 *   <li>_32193000 : Free-standing immunology clinic</li>
 *   <li>_33022008 : Hospital-based outpatient department</li>
 *   <li>_34304006 : Burn centre</li>
 *   <li>_35313001 : Skilled nursing facility, level 4 care</li>
 *   <li>_35971002 : Ambulatory care site</li>
 *   <li>_36125001 : Trauma centre</li>
 *   <li>_36293008 : Hospital-based outpatient pain clinic</li>
 *   <li>_36490008 : Free-standing general surgery clinic</li>
 *   <li>_37546005 : Hospital-based outpatient rehabilitation clinic</li>
 *   <li>_37550003 : Hospital-based outpatient dermatology clinic</li>
 *   <li>_38238005 : Hospital-based outpatient neurology clinic</li>
 *   <li>_38426004 : Treatment centre for emotionally disturbed children</li>
 *   <li>_39176004 : Aeroplane-based care</li>
 *   <li>_39216000 : School infirmary</li>
 *   <li>_39350007 : Private physicians group office</li>
 *   <li>_39750005 : Free-standing pain clinic</li>
 *   <li>_39913001 : Residential school infirmary</li>
 *   <li>_41844007 : Free-standing geriatric clinic</li>
 *   <li>_42168000 : Hospital-based radiotherapy facility</li>
 *   <li>_42665001 : Nursing home</li>
 *   <li>_43741000 : Site of care</li>
 *   <li>_44860005 : Free-standing rehabilitation clinic</li>
 *   <li>_45131006 : Primary care hospital</li>
 *   <li>_45288004 : Free-standing urology clinic</li>
 *   <li>_45618002 : Skilled nursing facility</li>
 *   <li>_45899008 : Free-standing laboratory facility</li>
 *   <li>_46111000 : Secondary care hospital</li>
 *   <li>_46224007 : Vaccination clinic</li>
 *   <li>_48120004 : Cancer hospital</li>
 *   <li>_48311003 : Veterans administration hospital</li>
 *   <li>_50569004 : Hospital-based outpatient urology clinic</li>
 *   <li>_51563005 : Free-standing mental health clinic</li>
 *   <li>_52589007 : Free-standing orthopaedics clinic</li>
 *   <li>_52668009 : Hospital-based birthing centre</li>
 *   <li>_54372004 : Geriatric hospital</li>
 *   <li>_55948006 : Free-standing family medicine clinic</li>
 *   <li>_56109004 : Intermediate care hospital</li>
 *   <li>_56189001 : Hospital-based outpatient obstetrical clinic</li>
 *   <li>_56217002 : Free-standing obstetrical clinic</li>
 *   <li>_56293002 : Hospital-based outpatient haematology clinic</li>
 *   <li>_56775002 : Free-standing respiratory disease clinic</li>
 *   <li>_57159002 : Hospital-based outpatient respiratory disease clinic</li>
 *   <li>_58482006 : Hospital-based outpatient gastroenterology clinic</li>
 *   <li>_59374000 : Travelers aid clinic</li>
 *   <li>_62264003 : Free-standing otorhinolaryngology clinic</li>
 *   <li>_62480006 : Psychiatric hospital</li>
 *   <li>_63997002 : Skilled nursing facility, level 3 care</li>
 *   <li>_66280005 : Private home-based care</li>
 *   <li>_67190003 : Free-standing clinic</li>
 *   <li>_67236001 : Hospital-based outpatient physical medicine clinic</li>
 *   <li>_69135000 : Free-standing infectious disease clinic</li>
 *   <li>_69362002 : Hospital-based ambulatory surgery facility</li>
 *   <li>_69859008 : Free-standing rheumatology clinic</li>
 *   <li>_70547005 : Inner city health centre</li>
 *   <li>_72311000 : Health maintenance organisation</li>
 *   <li>_73588009 : Hospital-based laboratory facility</li>
 *   <li>_73644007 : Hospital-based outpatient endocrinology clinic</li>
 *   <li>_73770003 : Hospital-based outpatient emergency care centre</li>
 *   <li>_74018000 : Free-standing emergency care centre</li>
 *   <li>_74056004 : Orphanage</li>
 *   <li>_77500004 : Vocational rehabilitation centre</li>
 *   <li>_77931003 : Rural health centre</li>
 *   <li>_78001009 : Hospital-based outpatient orthopaedics clinic</li>
 *   <li>_78088001 : Hospital-based outpatient ophthalmology clinic</li>
 *   <li>_79491001 : Hospital-based radiology facility</li>
 *   <li>_79993009 : Government hospital</li>
 *   <li>_80522000 : Rehabilitation hospital</li>
 *   <li>_81234003 : Walk-in clinic</li>
 *   <li>_82242000 : Childrens hospital</li>
 *   <li>_82455001 : Free-standing paediatric clinic</li>
 *   <li>_83891005 : Solo practice private office</li>
 *   <li>_84643003 : Free-standing endocrinology clinic</li>
 *   <li>_89972002 : Hospital-based outpatient oncology clinic</li>
 *   <li>_90484001 : Hospital-based outpatient general surgery clinic</li>
 *   <li>_91154008 : Free-standing birthing centre</li>
 *   <li>_91655008 : Free-standing gynaecology clinic</li>
 *   <li>_108343000 : Hospital ANDOR institution</li>
 *   <li>_108344006 : Nursing home ANDOR ambulatory care site</li>
 *   <li>_113172002 : Free-standing oncology clinic</li>
 *   <li>_113173007 : Domiciliary</li>
 *   <li>_182718004 : Mountain resort sanatorium</li>
 *   <li>_185483006 : Outreach clinic</li>
 *   <li>_185484000 : Dermatology outreach clinic</li>
 *   <li>_185485004 : Orthopaedic outreach clinic</li>
 *   <li>_185486003 : Ophthalmology outreach clinic</li>
 *   <li>_185487007 : Podiatry outreach clinic</li>
 *   <li>_185920002 : Cardiovascular clinic</li>
 *   <li>_224687002 : Prison hospital</li>
 *   <li>_224880002 : Location within general practice premises</li>
 *   <li>_224881003 : Charitable nursing home</li>
 *   <li>_224882005 : Cheshire nursing home</li>
 *   <li>_224883000 : St Dunstans</li>
 *   <li>_224884006 : Location within hospital premises</li>
 *   <li>_224885007 : Hospital consulting room</li>
 *   <li>_224886008 : Treatment room</li>
 *   <li>_224887004 : Hospital waiting room</li>
 *   <li>_224888009 : Health education room</li>
 *   <li>_224889001 : Toilet on hospital premises</li>
 *   <li>_224890005 : Hospital reception area</li>
 *   <li>_224924009 : Cottage hospital</li>
 *   <li>_224927002 : Medium secure unit</li>
 *   <li>_224928007 : Regional secure unit</li>
 *   <li>_224929004 : Secure hospital</li>
 *   <li>_225728007 : Accident and Emergency department</li>
 *   <li>_225729004 : Bathing station</li>
 *   <li>_225732001 : Community hospital</li>
 *   <li>_225734000 : Holding room</li>
 *   <li>_225737007 : Mortuary</li>
 *   <li>_225738002 : Operating theatre</li>
 *   <li>_225739005 : Clients room</li>
 *   <li>_225740007 : Regional centre</li>
 *   <li>_225741006 : Resuscitation room</li>
 *   <li>_225743009 : Seclusion room</li>
 *   <li>_225745002 : Stripped room</li>
 *   <li>_225746001 : Ward</li>
 *   <li>_225747005 : X-ray department</li>
 *   <li>_257585005 : Clinic</li>
 *   <li>_257622000 : Healthcare facility</li>
 *   <li>_264358009 : General practice premises</li>
 *   <li>_264361005 : Health centre</li>
 *   <li>_264372000 : Pharmacy</li>
 *   <li>_274409007 : Special care unit</li>
 *   <li>_274516006 : Day hospital</li>
 *   <li>_274517002 : Alcoholism detoxication centre</li>
 *   <li>_275576008 : Elderly assessment clinic</li>
 *   <li>_275692008 : Wart clinic</li>
 *   <li>_282086004 : Dental surgery premises</li>
 *   <li>_282087008 : Dental laboratory</li>
 *   <li>_284455000 : Location within secure unit</li>
 *   <li>_284548004 : Hospital department</li>
 *   <li>_288561005 : Special hospital</li>
 *   <li>_288562003 : Secure unit</li>
 *   <li>_288565001 : Medical centre</li>
 *   <li>_288566000 : General practice examination room</li>
 *   <li>_288567009 : General practice treatment room</li>
 *   <li>_288568004 : General practice consulting room</li>
 *   <li>_288569007 : General practice reception area</li>
 *   <li>_288570008 : General practice waiting room</li>
 *   <li>_288572000 : Toilet on general practice premises</li>
 *   <li>_288573005 : Community health care environment</li>
 *   <li>_309895006 : Private hospital</li>
 *   <li>_309896007 : Tertiary referral hospital</li>
 *   <li>_309897003 : Psychiatric day care hospital</li>
 *   <li>_309898008 : Psychogeriatric day hospital</li>
 *   <li>_309899000 : Elderly severely mentally ill day hospital</li>
 *   <li>_309900005 : Care of the elderly day hospital</li>
 *   <li>_309901009 : Anaesthetic department</li>
 *   <li>_309902002 : Clinical oncology department</li>
 *   <li>_309903007 : Radiotherapy department</li>
 *   <li>_309904001 : Intensive care unit</li>
 *   <li>_309905000 : Adult intensive care unit</li>
 *   <li>_309906004 : Burns unit</li>
 *   <li>_309907008 : Cardiac intensive care unit</li>
 *   <li>_309908003 : Metabolic intensive care unit</li>
 *   <li>_309909006 : Neurological intensive care unit</li>
 *   <li>_309910001 : Paediatric intensive care unit</li>
 *   <li>_309911002 : Respiratory intensive care unit</li>
 *   <li>_309912009 : Medical department</li>
 *   <li>_309913004 : Clinical allergy department</li>
 *   <li>_309914005 : Audiology department</li>
 *   <li>_309915006 : Cardiology department</li>
 *   <li>_309916007 : Chest medicine department</li>
 *   <li>_309917003 : Thoracic medicine department</li>
 *   <li>_309918008 : Respiratory medicine department</li>
 *   <li>_309919000 : Clinical immunology department</li>
 *   <li>_309920006 : Clinical neurophysiology department</li>
 *   <li>_309921005 : Clinical pharmacology department</li>
 *   <li>_309922003 : Clinical physiology department</li>
 *   <li>_309923008 : Dermatology department</li>
 *   <li>_309924002 : Diabetic department</li>
 *   <li>_309925001 : Endocrinology department</li>
 *   <li>_309926000 : Gastroenterology department</li>
 *   <li>_309927009 : General medical department</li>
 *   <li>_309928004 : Genetics department</li>
 *   <li>_309929007 : Clinical genetics department</li>
 *   <li>_309930002 : Clinical cytogenetics department</li>
 *   <li>_309931003 : Clinical molecular genetics department</li>
 *   <li>_309932005 : Genitourinary medicine department</li>
 *   <li>_309933000 : Care of the elderly department</li>
 *   <li>_309934006 : Infectious diseases department</li>
 *   <li>_309935007 : Medical ophthalmology department</li>
 *   <li>_309936008 : Nephrology department</li>
 *   <li>_309937004 : Neurology department</li>
 *   <li>_309938009 : Nuclear medicine department</li>
 *   <li>_309939001 : Palliative care department</li>
 *   <li>_309940004 : Rehabilitation department</li>
 *   <li>_309941000 : Rheumatology department</li>
 *   <li>_309942007 : Obstetrics and gynaecology department</li>
 *   <li>_309943002 : Gynaecology department</li>
 *   <li>_309944008 : Obstetrics department</li>
 *   <li>_309945009 : Paediatric department</li>
 *   <li>_309946005 : Special care baby unit</li>
 *   <li>_309947001 : Paediatric neurology department</li>
 *   <li>_309948006 : Paediatric oncology department</li>
 *   <li>_309949003 : Pain management department</li>
 *   <li>_309950003 : Pathology department</li>
 *   <li>_309951004 : Blood transfusion department</li>
 *   <li>_309952006 : Chemical pathology department</li>
 *   <li>_309953001 : General pathology department</li>
 *   <li>_309954007 : Haematology department</li>
 *   <li>_309956009 : Medical microbiology department</li>
 *   <li>_309957000 : Neuropathology department</li>
 *   <li>_309958005 : Psychiatry department</li>
 *   <li>_309959002 : Child and adolescent psychiatry department</li>
 *   <li>_309960007 : Forensic psychiatry department</li>
 *   <li>_309961006 : Psychogeriatric department</li>
 *   <li>_309962004 : Mental handicap psychiatry department</li>
 *   <li>_309963009 : Rehabilitation psychiatry department</li>
 *   <li>_309964003 : Radiology department</li>
 *   <li>_309965002 : Occupational health department</li>
 *   <li>_309966001 : Stroke unit</li>
 *   <li>_309967005 : Surgical department</li>
 *   <li>_309968000 : Breast surgery department</li>
 *   <li>_309969008 : Cardiothoracic surgery department</li>
 *   <li>_309970009 : Thoracic surgery department</li>
 *   <li>_309971008 : Cardiac surgery department</li>
 *   <li>_309972001 : Dental surgery department</li>
 *   <li>_309973006 : General dental surgery department</li>
 *   <li>_309974000 : Oral surgery department</li>
 *   <li>_309975004 : Orthodontics department</li>
 *   <li>_309976003 : Paediatric dentistry department</li>
 *   <li>_309977007 : Restorative dentistry department</li>
 *   <li>_309978002 : Ear, nose and throat department</li>
 *   <li>_309979005 : Endocrine surgery department</li>
 *   <li>_309980008 : Gastrointestinal surgery department</li>
 *   <li>_309981007 : General gastrointestinal surgery department</li>
 *   <li>_309982000 : Upper gastrointestinal surgery department</li>
 *   <li>_309983005 : Colorectal surgery department</li>
 *   <li>_309984004 : General surgical department</li>
 *   <li>_309985003 : Hand surgery department</li>
 *   <li>_309986002 : Hepatobiliary surgical department</li>
 *   <li>_309987006 : Neurosurgical department</li>
 *   <li>_309988001 : Ophthalmology department</li>
 *   <li>_309989009 : Orthopaedic department</li>
 *   <li>_309990000 : Pancreatic surgery department</li>
 *   <li>_309991001 : Paediatric surgical department</li>
 *   <li>_309992008 : Plastic surgery department</li>
 *   <li>_309993003 : Surgical transplant department</li>
 *   <li>_309994009 : Trauma surgery department</li>
 *   <li>_309995005 : Urology department</li>
 *   <li>_309996006 : Vascular surgery department</li>
 *   <li>_309997002 : Young disabled unit</li>
 *   <li>_309998007 : Day ward</li>
 *   <li>_310203004 : Paediatric Accident and Emergency department</li>
 *   <li>_310204005 : Ophthalmology Accident and Emergency department</li>
 *   <li>_310206007 : Private nursing home</li>
 *   <li>_310390009 : Hospital clinic</li>
 *   <li>_310391008 : Community clinic</li>
 *   <li>_310464005 : Physiotherapy department</li>
 *   <li>_360957003 : Hospital-based outpatient allergy clinic</li>
 *   <li>_360966004 : Hospital-based outpatient immunology clinic</li>
 *   <li>_394573001 : Maternity clinic</li>
 *   <li>_394574007 : Antenatal clinic</li>
 *   <li>_394575008 : Postnatal clinic</li>
 *   <li>_394750006 : Primary Care Trust site</li>
 *   <li>_394759007 : Independent provider site</li>
 *   <li>_394761003 : GP practice site</li>
 *   <li>_394777002 : Health encounter sites</li>
 *   <li>_394778007 : Clients or patients home</li>
 *   <li>_394783004 : Group home managed by local authority</li>
 *   <li>_394784005 : Group home managed by voluntary or private agents</li>
 *   <li>_394789000 : Day centre managed by local authority</li>
 *   <li>_394790009 : Day centre managed by voluntary or private agents</li>
 *   <li>_394794000 : Health clinic managed by voluntary or private agents</li>
 *   <li>_394797007 : Resource centre managed by local authority</li>
 *   <li>_394798002 : Resource centre managed by voluntary or private agents</li>
 *   <li>_394865001 : Joint consultant clinic</li>
 *   <li>_397784001 : Other location within hospital premises</li>
 *   <li>_397872006 : Preoperative screening clinic</li>
 *   <li>_397983003 : Preoperative holding area</li>
 *   <li>_398145002 : Unspecified location within hospital premises</li>
 *   <li>_398156002 : Medical or surgical floor</li>
 *   <li>_398161000 : Postoperative anaesthesia care unit</li>
 *   <li>_404821007 : Psychiatric intensive care unit</li>
 *   <li>_405269005 : Neonatal intensive care unit</li>
 *   <li>_405606005 : Labour and delivery unit</li>
 *   <li>_405607001 : Ambulatory surgery centre</li>
 *   <li>_405608006 : Academic medical centre</li>
 *   <li>_409518000 : Mass casualty setting</li>
 *   <li>_409519008 : Contained casualty setting</li>
 *   <li>_409685000 : Hospital decontamination room</li>
 *   <li>_409688003 : Hospital isolation room</li>
 *   <li>_413456002 : Adult day care centre</li>
 *   <li>_413817003 : Child day care centre</li>
 *   <li>_413964002 : Delivery suite</li>
 *   <li>_414485004 : Induction room</li>
 *   <li>_415142008 : PACU Phase 1</li>
 *   <li>_415143003 : PACU Phase 2</li>
 *   <li>_416957006 : Community medical centre</li>
 *   <li>_418028002 : Surgical stepdown unit</li>
 *   <li>_418433008 : Surgical intensive care unit</li>
 *   <li>_418518002 : Dialysis unit</li>
 *   <li>_418649002 : Cardiac stepdown unit</li>
 *   <li>_419390002 : Medical stepdown unit</li>
 *   <li>_419590001 : Stepdown unit</li>
 *   <li>_420223003 : Paediatric medicine department</li>
 *   <li>_420280003 : Obstetrical stepdown unit</li>
 *   <li>_422798006 : Telemetry unit</li>
 *   <li>_426439001 : Burns intensive care unit</li>
 *   <li>_427695007 : Newborn nursery unit</li>
 *   <li>_431654006 : Genitourinary medicine clinic</li>
 *   <li>_441480003 : Primary care department</li>
 *   <li>_441548002 : Tropical medicine department</li>
 *   <li>_441662001 : Diagnostic imaging department</li>
 *   <li>_441950002 : Histopathology department</li>
 *   <li>_441994008 : Medical intensive care unit</li>
 *   <li>_443621004 : Sleep apnoea clinic</li>
 *   <li>_443750004 : Nonacute hospital</li>
 *   <li>_444766008 : Colposcopy clinic</li>
 *   <li>_448399001 : Dental hospital</li>
 *   <li>_702706001 : Diabetes clinic</li>
 *   <li>_702812003 : Acute pain clinic</li>
 *   <li>_702813008 : Acquired immune deficiency syndrome clinic</li>
 *   <li>_702814002 : Allergy clinic</li>
 *   <li>_702819007 : After hours clinic</li>
 *   <li>_702821002 : Amputee clinic</li>
 *   <li>_702822009 : Anticoagulation clinic</li>
 *   <li>_702823004 : Asthma clinic</li>
 *   <li>_702824005 : Audiology clinic</li>
 *   <li>_702825006 : Autism clinic</li>
 *   <li>_702826007 : Bipolar clinic</li>
 *   <li>_702827003 : Bone marrow transplant clinic</li>
 *   <li>_702828008 : Breast clinic</li>
 *   <li>_702830005 : Cardiology clinic</li>
 *   <li>_702831009 : Child and adolescent medicine clinic</li>
 *   <li>_702832002 : Child and adolescent neurology clinic</li>
 *   <li>_702833007 : Child and adolescent psychiatry clinic</li>
 *   <li>_702834001 : Child speech and language therapy clinic</li>
 *   <li>_702839006 : Chronic obstructive pulmonary disease clinic</li>
 *   <li>_702840008 : Chronic pain clinic</li>
 *   <li>_702841007 : Cleft palate clinic</li>
 *   <li>_702842000 : Congenital heart disease clinic</li>
 *   <li>_702843005 : Congestive heart failure clinic</li>
 *   <li>_702844004 : Cystic fibrosis clinic</li>
 *   <li>_702845003 : Deep vein thrombosis clinic</li>
 *   <li>_702846002 : Dental clinic</li>
 *   <li>_702847006 : Dermatology clinic</li>
 *   <li>_702848001 : Diabetes foot care clinic</li>
 *   <li>_702849009 : Diabetes in pregnancy clinic</li>
 *   <li>_702850009 : Diabetic retinopathy clinic</li>
 *   <li>_702851008 : Down syndrome clinic</li>
 *   <li>_702852001 : Ear, nose and throat clinic</li>
 *   <li>_702853006 : Eating disorder clinic</li>
 *   <li>_702854000 : Endocrinology clinic</li>
 *   <li>_702855004 : Family medicine clinic</li>
 *   <li>_702856003 : Family planning clinic</li>
 *   <li>_702857007 : Fetal alcohol clinic</li>
 *   <li>_702858002 : Fracture clinic</li>
 *   <li>_702859005 : Gastroenterology clinic</li>
 *   <li>_702860000 : General surgery clinic</li>
 *   <li>_702861001 : Genetics clinic</li>
 *   <li>_702862008 : Gynaecology clinic</li>
 *   <li>_702863003 : Hand clinic</li>
 *   <li>_702864009 : Headache clinic</li>
 *   <li>_702865005 : Haematology clinic</li>
 *   <li>_702866006 : Haemophilia clinic</li>
 *   <li>_702867002 : Hypertension clinic</li>
 *   <li>_702868007 : Immunology clinic</li>
 *   <li>_702870003 : Infectious disease clinic</li>
 *   <li>_702871004 : Infertility clinic</li>
 *   <li>_702877000 : Internal medicine clinic</li>
 *   <li>_702878005 : Land ambulance</li>
 *   <li>_702879002 : Laser eye clinic</li>
 *   <li>_702880004 : Lymphoedema clinic</li>
 *   <li>_702881000 : Medical day care</li>
 *   <li>_702882007 : Mental health clinic</li>
 *   <li>_702883002 : Mood or anxiety disorder clinic</li>
 *   <li>_702884008 : Movement disorder clinic</li>
 *   <li>_702885009 : Multiple sclerosis clinic</li>
 *   <li>_702886005 : Nephrology clinic</li>
 *   <li>_702887001 : Neurology clinic</li>
 *   <li>_702888006 : Neuromuscular clinic</li>
 *   <li>_702889003 : Neurophysiology clinic</li>
 *   <li>_702890007 : Neuropsychology clinic</li>
 *   <li>_702891006 : Neurosurgery clinic</li>
 *   <li>_702892004 : Obstetrics and gynaecology clinic</li>
 *   <li>_702893009 : Occupational therapy clinic</li>
 *   <li>_702894003 : Ophthalmology clinic</li>
 *   <li>_702895002 : Osteoporosis clinic</li>
 *   <li>_702896001 : Pacemaker clinic</li>
 *   <li>_702897005 : Pain clinic</li>
 *   <li>_702898000 : Parkinsons disease clinic</li>
 *   <li>_702899008 : Paediatric cardiology clinic</li>
 *   <li>_702900003 : Paediatric clinic</li>
 *   <li>_702901004 : Paediatric dermatology clinic</li>
 *   <li>_702902006 : Paediatric ear, nose and throat clinic</li>
 *   <li>_702903001 : Paediatric endocrinology clinic</li>
 *   <li>_702904007 : Paediatric gastroenterology clinic</li>
 *   <li>_702905008 : Paediatric haematology clinic</li>
 *   <li>_702906009 : Paediatric infectious disease clinic</li>
 *   <li>_702907000 : Paediatric rheumatology clinic</li>
 *   <li>_702908005 : Paediatric urology clinic</li>
 *   <li>_702909002 : Paediatric oncology clinic</li>
 *   <li>_702910007 : Pelvic floor clinic</li>
 *   <li>_702911006 : Podiatry clinic</li>
 *   <li>_702912004 : Preventive medicine clinic</li>
 *   <li>_702913009 : Proctology clinic</li>
 *   <li>_702914003 : Psychiatry clinic</li>
 *   <li>_702915002 : Psychology clinic</li>
 *   <li>_702916001 : Rehabilitation clinic</li>
 *   <li>_702917005 : Respiratory disease clinic</li>
 *   <li>_702918000 : Rheumatology clinic</li>
 *   <li>_702919008 : Schizophrenia clinic</li>
 *   <li>_702920002 : Seizure clinic</li>
 *   <li>_702921003 : Sexual health clinic</li>
 *   <li>_702922005 : Speech and language therapy clinic</li>
 *   <li>_702923000 : Sports medicine clinic</li>
 *   <li>_702924006 : Stroke clinic</li>
 *   <li>_702925007 : Swallow clinic</li>
 *   <li>_702926008 : Tourette syndrome clinic</li>
 *   <li>_702927004 : Urgent care clinic</li>
 *   <li>_702928009 : Urology clinic</li>
 *   <li>_702929001 : Vertigo clinic</li>
 *   <li>_702930006 : Plastic surgery clinic</li>
 *   <li>_702931005 : Oncology clinic</li>
 *   <li>_702932003 : Oral and maxillofacial surgery clinic</li>
 *   <li>_702933008 : Orthopaedic clinic</li>
 *   <li>_702934002 : Paediatric surgery clinic</li>
 *   <li>_702935001 : Physiotherapy clinic</li>
 *   <li>_703069008 : Nurse practitioner clinic</li>
 *   <li>_198931000000104 : Gynaecology outreach clinic</li>
 *   <li>_198941000000108 : General surgery outreach clinic</li>
 *   <li>_198951000000106 : Psychiatric outreach clinic</li>
 *   <li>_198961000000109 : General medicine outreach clinic</li>
 *   <li>_198971000000102 : Asthma outreach clinic</li>
 *   <li>_203951000000101 : Diabetic antenatal clinic</li>
 *   <li>_312351000000101 : Ambulance service care setting</li>
 *   <li>_312361000000103 : Primary care out of hours service care setting</li>
 *   <li>_312371000000105 : Social care setting</li>
 *   <li>_312391000000109 : NHS Direct Contact Centre</li>
 *   <li>_312401000000107 : Urgent care centre</li>
 *   <li>_312411000000109 : Minor injuries unit</li>
 *   <li>_312471000000104 : Community mental health care setting</li>
 *   <li>_313161000000107 : Out of hours service care setting</li>
 *   <li>_506341000000101 : Specialist treatment area</li>
 *   <li>_930711000000104 : Group home managed by NHS</li>
 *   <li>_930721000000105 : NHS day care facility on NHS hospital site</li>
 *   <li>_930731000000107 : NHS day care facility on other site</li>
 *   <li>_930741000000103 : NHS consultant clinic premises on NHS hospital site</li>
 *   <li>_930751000000100 : NHS consultant clinic premises off NHS hospital site</li>
 *   <li>_930761000000102 : Health clinic managed by NHS</li>
 *   <li>_930771000000109 : Resource centre on NHS hospital site</li>
 *   <li>_930781000000106 : Resource centre managed by the NHS off NHS hospital site</li>
 *   <li>_930791000000108 : Professional staff group department on NHS hospital site</li>
 *   <li>_930801000000107 : Professional staff group department managed by the NHS off NHS hospital site</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum CDACareSettingType implements VocabularyEntry {
	
	
	/**
	 * Hospital-based outpatient rheumatology clinic
	 */
	_331006 ("331006", "Hospital-based outpatient rheumatology clinic"),
	
	/**
	 * Helicopter-based care
	 */
	_901005 ("901005", "Helicopter-based care"),
	
	/**
	 * Day care centre
	 */
	_1348009 ("1348009", "Day care centre"),
	
	/**
	 * Free-standing breast clinic
	 */
	_1526002 ("1526002", "Free-standing breast clinic"),
	
	/**
	 * Free-standing radiology facility
	 */
	_1773006 ("1773006", "Free-standing radiology facility"),
	
	/**
	 * Hospital-based outpatient geriatric clinic
	 */
	_1814000 ("1814000", "Hospital-based outpatient geriatric clinic"),
	
	/**
	 * Hospital ship
	 */
	_2081004 ("2081004", "Hospital ship"),
	
	/**
	 * Hospital-based outpatient infectious disease clinic
	 */
	_2849009 ("2849009", "Hospital-based outpatient infectious disease clinic"),
	
	/**
	 * Aid car-based care
	 */
	_2961002 ("2961002", "Aid car-based care"),
	
	/**
	 * Hospital-based outpatient paediatric clinic
	 */
	_3729002 ("3729002", "Hospital-based outpatient paediatric clinic"),
	
	/**
	 * Free-standing haematology clinic
	 */
	_3768009 ("3768009", "Free-standing haematology clinic"),
	
	/**
	 * Military field hospital
	 */
	_4322002 ("4322002", "Military field hospital"),
	
	/**
	 * Hospital-based outpatient peripheral vascular clinic
	 */
	_5584006 ("5584006", "Hospital-based outpatient peripheral vascular clinic"),
	
	/**
	 * Local community health centre
	 */
	_6827000 ("6827000", "Local community health centre"),
	
	/**
	 * Hospital-based outpatient dental clinic
	 */
	_10206005 ("10206005", "Hospital-based outpatient dental clinic"),
	
	/**
	 * Free-standing ambulatory surgery facility
	 */
	_10531005 ("10531005", "Free-standing ambulatory surgery facility"),
	
	/**
	 * Free-standing peripheral vascular clinic
	 */
	_11119007 ("11119007", "Free-standing peripheral vascular clinic"),
	
	/**
	 * Ambulance-based care
	 */
	_11424001 ("11424001", "Ambulance-based care"),
	
	/**
	 * Free-standing gastroenterology clinic
	 */
	_13015006 ("13015006", "Free-standing gastroenterology clinic"),
	
	/**
	 * Free-standing physical medicine clinic
	 */
	_13656005 ("13656005", "Free-standing physical medicine clinic"),
	
	/**
	 * Hospital-based outpatient mental health clinic
	 */
	_14866005 ("14866005", "Hospital-based outpatient mental health clinic"),
	
	/**
	 * Skilled nursing facility, level 2 care
	 */
	_16384001 ("16384001", "Skilled nursing facility, level 2 care"),
	
	/**
	 * Free-standing ophthalmology clinic
	 */
	_16597003 ("16597003", "Free-standing ophthalmology clinic"),
	
	/**
	 * Free-standing neurology clinic
	 */
	_16879004 ("16879004", "Free-standing neurology clinic"),
	
	/**
	 * Intermediate care hospital for mentally retarded
	 */
	_18095007 ("18095007", "Intermediate care hospital for mentally retarded"),
	
	/**
	 * Fee-for-service private physicians group office
	 */
	_19602009 ("19602009", "Fee-for-service private physicians' group office"),
	
	/**
	 * Substance abuse treatment centre
	 */
	_20078004 ("20078004", "Substance abuse treatment centre"),
	
	/**
	 * Tertiary care hospital
	 */
	_20316001 ("20316001", "Tertiary care hospital"),
	
	/**
	 * Free-standing dermatology clinic
	 */
	_21700006 ("21700006", "Free-standing dermatology clinic"),
	
	/**
	 * Free-standing dental clinic
	 */
	_22201008 ("22201008", "Free-standing dental clinic"),
	
	/**
	 * Hospital
	 */
	_22232009 ("22232009", "Hospital"),
	
	/**
	 * Skilled nursing facility, level 1 care
	 */
	_22357001 ("22357001", "Skilled nursing facility, level 1 care"),
	
	/**
	 * Hospital-based outpatient gynaecology clinic
	 */
	_22549003 ("22549003", "Hospital-based outpatient gynaecology clinic"),
	
	/**
	 * Hospital-based outpatient otorhinolaryngology clinic
	 */
	_23392004 ("23392004", "Hospital-based outpatient otorhinolaryngology clinic"),
	
	/**
	 * Hospital-based outpatient breast clinic
	 */
	_25567001 ("25567001", "Hospital-based outpatient breast clinic"),
	
	/**
	 * Sexually transmitted disease clinic
	 */
	_25681007 ("25681007", "Sexually transmitted disease clinic"),
	
	/**
	 * Retirement home
	 */
	_30629002 ("30629002", "Retirement home"),
	
	/**
	 * Prepaid private physicians group office
	 */
	_31154006 ("31154006", "Prepaid private physicians' group office"),
	
	/**
	 * Hospital-based outpatient family medicine clinic
	 */
	_31628002 ("31628002", "Hospital-based outpatient family medicine clinic"),
	
	/**
	 * Long term care hospital
	 */
	_32074000 ("32074000", "Long term care hospital"),
	
	/**
	 * Free-standing immunology clinic
	 */
	_32193000 ("32193000", "Free-standing immunology clinic"),
	
	/**
	 * Hospital-based outpatient department
	 */
	_33022008 ("33022008", "Hospital-based outpatient department"),
	
	/**
	 * Burn centre
	 */
	_34304006 ("34304006", "Burn centre"),
	
	/**
	 * Skilled nursing facility, level 4 care
	 */
	_35313001 ("35313001", "Skilled nursing facility, level 4 care"),
	
	/**
	 * Ambulatory care site
	 */
	_35971002 ("35971002", "Ambulatory care site"),
	
	/**
	 * Trauma centre
	 */
	_36125001 ("36125001", "Trauma centre"),
	
	/**
	 * Hospital-based outpatient pain clinic
	 */
	_36293008 ("36293008", "Hospital-based outpatient pain clinic"),
	
	/**
	 * Free-standing general surgery clinic
	 */
	_36490008 ("36490008", "Free-standing general surgery clinic"),
	
	/**
	 * Hospital-based outpatient rehabilitation clinic
	 */
	_37546005 ("37546005", "Hospital-based outpatient rehabilitation clinic"),
	
	/**
	 * Hospital-based outpatient dermatology clinic
	 */
	_37550003 ("37550003", "Hospital-based outpatient dermatology clinic"),
	
	/**
	 * Hospital-based outpatient neurology clinic
	 */
	_38238005 ("38238005", "Hospital-based outpatient neurology clinic"),
	
	/**
	 * Treatment centre for emotionally disturbed children
	 */
	_38426004 ("38426004", "Treatment centre for emotionally disturbed children"),
	
	/**
	 * Aeroplane-based care
	 */
	_39176004 ("39176004", "Aeroplane-based care"),
	
	/**
	 * School infirmary
	 */
	_39216000 ("39216000", "School infirmary"),
	
	/**
	 * Private physicians group office
	 */
	_39350007 ("39350007", "Private physicians' group office"),
	
	/**
	 * Free-standing pain clinic
	 */
	_39750005 ("39750005", "Free-standing pain clinic"),
	
	/**
	 * Residential school infirmary
	 */
	_39913001 ("39913001", "Residential school infirmary"),
	
	/**
	 * Free-standing geriatric clinic
	 */
	_41844007 ("41844007", "Free-standing geriatric clinic"),
	
	/**
	 * Hospital-based radiotherapy facility
	 */
	_42168000 ("42168000", "Hospital-based radiotherapy facility"),
	
	/**
	 * Nursing home
	 */
	_42665001 ("42665001", "Nursing home"),
	
	/**
	 * Site of care
	 */
	_43741000 ("43741000", "Site of care"),
	
	/**
	 * Free-standing rehabilitation clinic
	 */
	_44860005 ("44860005", "Free-standing rehabilitation clinic"),
	
	/**
	 * Primary care hospital
	 */
	_45131006 ("45131006", "Primary care hospital"),
	
	/**
	 * Free-standing urology clinic
	 */
	_45288004 ("45288004", "Free-standing urology clinic"),
	
	/**
	 * Skilled nursing facility
	 */
	_45618002 ("45618002", "Skilled nursing facility"),
	
	/**
	 * Free-standing laboratory facility
	 */
	_45899008 ("45899008", "Free-standing laboratory facility"),
	
	/**
	 * Secondary care hospital
	 */
	_46111000 ("46111000", "Secondary care hospital"),
	
	/**
	 * Vaccination clinic
	 */
	_46224007 ("46224007", "Vaccination clinic"),
	
	/**
	 * Cancer hospital
	 */
	_48120004 ("48120004", "Cancer hospital"),
	
	/**
	 * Veterans administration hospital
	 */
	_48311003 ("48311003", "Veteran's administration hospital"),
	
	/**
	 * Hospital-based outpatient urology clinic
	 */
	_50569004 ("50569004", "Hospital-based outpatient urology clinic"),
	
	/**
	 * Free-standing mental health clinic
	 */
	_51563005 ("51563005", "Free-standing mental health clinic"),
	
	/**
	 * Free-standing orthopaedics clinic
	 */
	_52589007 ("52589007", "Free-standing orthopaedics clinic"),
	
	/**
	 * Hospital-based birthing centre
	 */
	_52668009 ("52668009", "Hospital-based birthing centre"),
	
	/**
	 * Geriatric hospital
	 */
	_54372004 ("54372004", "Geriatric hospital"),
	
	/**
	 * Free-standing family medicine clinic
	 */
	_55948006 ("55948006", "Free-standing family medicine clinic"),
	
	/**
	 * Intermediate care hospital
	 */
	_56109004 ("56109004", "Intermediate care hospital"),
	
	/**
	 * Hospital-based outpatient obstetrical clinic
	 */
	_56189001 ("56189001", "Hospital-based outpatient obstetrical clinic"),
	
	/**
	 * Free-standing obstetrical clinic
	 */
	_56217002 ("56217002", "Free-standing obstetrical clinic"),
	
	/**
	 * Hospital-based outpatient haematology clinic
	 */
	_56293002 ("56293002", "Hospital-based outpatient haematology clinic"),
	
	/**
	 * Free-standing respiratory disease clinic
	 */
	_56775002 ("56775002", "Free-standing respiratory disease clinic"),
	
	/**
	 * Hospital-based outpatient respiratory disease clinic
	 */
	_57159002 ("57159002", "Hospital-based outpatient respiratory disease clinic"),
	
	/**
	 * Hospital-based outpatient gastroenterology clinic
	 */
	_58482006 ("58482006", "Hospital-based outpatient gastroenterology clinic"),
	
	/**
	 * Travelers aid clinic
	 */
	_59374000 ("59374000", "Traveler's aid clinic"),
	
	/**
	 * Free-standing otorhinolaryngology clinic
	 */
	_62264003 ("62264003", "Free-standing otorhinolaryngology clinic"),
	
	/**
	 * Psychiatric hospital
	 */
	_62480006 ("62480006", "Psychiatric hospital"),
	
	/**
	 * Skilled nursing facility, level 3 care
	 */
	_63997002 ("63997002", "Skilled nursing facility, level 3 care"),
	
	/**
	 * Private home-based care
	 */
	_66280005 ("66280005", "Private home-based care"),
	
	/**
	 * Free-standing clinic
	 */
	_67190003 ("67190003", "Free-standing clinic"),
	
	/**
	 * Hospital-based outpatient physical medicine clinic
	 */
	_67236001 ("67236001", "Hospital-based outpatient physical medicine clinic"),
	
	/**
	 * Free-standing infectious disease clinic
	 */
	_69135000 ("69135000", "Free-standing infectious disease clinic"),
	
	/**
	 * Hospital-based ambulatory surgery facility
	 */
	_69362002 ("69362002", "Hospital-based ambulatory surgery facility"),
	
	/**
	 * Free-standing rheumatology clinic
	 */
	_69859008 ("69859008", "Free-standing rheumatology clinic"),
	
	/**
	 * Inner city health centre
	 */
	_70547005 ("70547005", "Inner city health centre"),
	
	/**
	 * Health maintenance organisation
	 */
	_72311000 ("72311000", "Health maintenance organisation"),
	
	/**
	 * Hospital-based laboratory facility
	 */
	_73588009 ("73588009", "Hospital-based laboratory facility"),
	
	/**
	 * Hospital-based outpatient endocrinology clinic
	 */
	_73644007 ("73644007", "Hospital-based outpatient endocrinology clinic"),
	
	/**
	 * Hospital-based outpatient emergency care centre
	 */
	_73770003 ("73770003", "Hospital-based outpatient emergency care centre"),
	
	/**
	 * Free-standing emergency care centre
	 */
	_74018000 ("74018000", "Free-standing emergency care centre"),
	
	/**
	 * Orphanage
	 */
	_74056004 ("74056004", "Orphanage"),
	
	/**
	 * Vocational rehabilitation centre
	 */
	_77500004 ("77500004", "Vocational rehabilitation centre"),
	
	/**
	 * Rural health centre
	 */
	_77931003 ("77931003", "Rural health centre"),
	
	/**
	 * Hospital-based outpatient orthopaedics clinic
	 */
	_78001009 ("78001009", "Hospital-based outpatient orthopaedics clinic"),
	
	/**
	 * Hospital-based outpatient ophthalmology clinic
	 */
	_78088001 ("78088001", "Hospital-based outpatient ophthalmology clinic"),
	
	/**
	 * Hospital-based radiology facility
	 */
	_79491001 ("79491001", "Hospital-based radiology facility"),
	
	/**
	 * Government hospital
	 */
	_79993009 ("79993009", "Government hospital"),
	
	/**
	 * Rehabilitation hospital
	 */
	_80522000 ("80522000", "Rehabilitation hospital"),
	
	/**
	 * Walk-in clinic
	 */
	_81234003 ("81234003", "Walk-in clinic"),
	
	/**
	 * Childrens hospital
	 */
	_82242000 ("82242000", "Children's hospital"),
	
	/**
	 * Free-standing paediatric clinic
	 */
	_82455001 ("82455001", "Free-standing paediatric clinic"),
	
	/**
	 * Solo practice private office
	 */
	_83891005 ("83891005", "Solo practice private office"),
	
	/**
	 * Free-standing endocrinology clinic
	 */
	_84643003 ("84643003", "Free-standing endocrinology clinic"),
	
	/**
	 * Hospital-based outpatient oncology clinic
	 */
	_89972002 ("89972002", "Hospital-based outpatient oncology clinic"),
	
	/**
	 * Hospital-based outpatient general surgery clinic
	 */
	_90484001 ("90484001", "Hospital-based outpatient general surgery clinic"),
	
	/**
	 * Free-standing birthing centre
	 */
	_91154008 ("91154008", "Free-standing birthing centre"),
	
	/**
	 * Free-standing gynaecology clinic
	 */
	_91655008 ("91655008", "Free-standing gynaecology clinic"),
	
	/**
	 * Hospital ANDOR institution
	 */
	_108343000 ("108343000", "Hospital AND/OR institution"),
	
	/**
	 * Nursing home ANDOR ambulatory care site
	 */
	_108344006 ("108344006", "Nursing home AND/OR ambulatory care site"),
	
	/**
	 * Free-standing oncology clinic
	 */
	_113172002 ("113172002", "Free-standing oncology clinic"),
	
	/**
	 * Domiciliary
	 */
	_113173007 ("113173007", "Domiciliary"),
	
	/**
	 * Mountain resort sanatorium
	 */
	_182718004 ("182718004", "Mountain resort sanatorium"),
	
	/**
	 * Outreach clinic
	 */
	_185483006 ("185483006", "Outreach clinic"),
	
	/**
	 * Dermatology outreach clinic
	 */
	_185484000 ("185484000", "Dermatology outreach clinic"),
	
	/**
	 * Orthopaedic outreach clinic
	 */
	_185485004 ("185485004", "Orthopaedic outreach clinic"),
	
	/**
	 * Ophthalmology outreach clinic
	 */
	_185486003 ("185486003", "Ophthalmology outreach clinic"),
	
	/**
	 * Podiatry outreach clinic
	 */
	_185487007 ("185487007", "Podiatry outreach clinic"),
	
	/**
	 * Cardiovascular clinic
	 */
	_185920002 ("185920002", "Cardiovascular clinic"),
	
	/**
	 * Prison hospital
	 */
	_224687002 ("224687002", "Prison hospital"),
	
	/**
	 * Location within general practice premises
	 */
	_224880002 ("224880002", "Location within general practice premises"),
	
	/**
	 * Charitable nursing home
	 */
	_224881003 ("224881003", "Charitable nursing home"),
	
	/**
	 * Cheshire nursing home
	 */
	_224882005 ("224882005", "Cheshire nursing home"),
	
	/**
	 * St Dunstans
	 */
	_224883000 ("224883000", "St Dunstan's"),
	
	/**
	 * Location within hospital premises
	 */
	_224884006 ("224884006", "Location within hospital premises"),
	
	/**
	 * Hospital consulting room
	 */
	_224885007 ("224885007", "Hospital consulting room"),
	
	/**
	 * Treatment room
	 */
	_224886008 ("224886008", "Treatment room"),
	
	/**
	 * Hospital waiting room
	 */
	_224887004 ("224887004", "Hospital waiting room"),
	
	/**
	 * Health education room
	 */
	_224888009 ("224888009", "Health education room"),
	
	/**
	 * Toilet on hospital premises
	 */
	_224889001 ("224889001", "Toilet on hospital premises"),
	
	/**
	 * Hospital reception area
	 */
	_224890005 ("224890005", "Hospital reception area"),
	
	/**
	 * Cottage hospital
	 */
	_224924009 ("224924009", "Cottage hospital"),
	
	/**
	 * Medium secure unit
	 */
	_224927002 ("224927002", "Medium secure unit"),
	
	/**
	 * Regional secure unit
	 */
	_224928007 ("224928007", "Regional secure unit"),
	
	/**
	 * Secure hospital
	 */
	_224929004 ("224929004", "Secure hospital"),
	
	/**
	 * Accident and Emergency department
	 */
	_225728007 ("225728007", "Accident and Emergency department"),
	
	/**
	 * Bathing station
	 */
	_225729004 ("225729004", "Bathing station"),
	
	/**
	 * Community hospital
	 */
	_225732001 ("225732001", "Community hospital"),
	
	/**
	 * Holding room
	 */
	_225734000 ("225734000", "Holding room"),
	
	/**
	 * Mortuary
	 */
	_225737007 ("225737007", "Mortuary"),
	
	/**
	 * Operating theatre
	 */
	_225738002 ("225738002", "Operating theatre"),
	
	/**
	 * Clients room
	 */
	_225739005 ("225739005", "Client's room"),
	
	/**
	 * Regional centre
	 */
	_225740007 ("225740007", "Regional centre"),
	
	/**
	 * Resuscitation room
	 */
	_225741006 ("225741006", "Resuscitation room"),
	
	/**
	 * Seclusion room
	 */
	_225743009 ("225743009", "Seclusion room"),
	
	/**
	 * Stripped room
	 */
	_225745002 ("225745002", "Stripped room"),
	
	/**
	 * Ward
	 */
	_225746001 ("225746001", "Ward"),
	
	/**
	 * X-ray department
	 */
	_225747005 ("225747005", "X-ray department"),
	
	/**
	 * Clinic
	 */
	_257585005 ("257585005", "Clinic"),
	
	/**
	 * Healthcare facility
	 */
	_257622000 ("257622000", "Healthcare facility"),
	
	/**
	 * General practice premises
	 */
	_264358009 ("264358009", "General practice premises"),
	
	/**
	 * Health centre
	 */
	_264361005 ("264361005", "Health centre"),
	
	/**
	 * Pharmacy
	 */
	_264372000 ("264372000", "Pharmacy"),
	
	/**
	 * Special care unit
	 */
	_274409007 ("274409007", "Special care unit"),
	
	/**
	 * Day hospital
	 */
	_274516006 ("274516006", "Day hospital"),
	
	/**
	 * Alcoholism detoxication centre
	 */
	_274517002 ("274517002", "Alcoholism detoxication centre"),
	
	/**
	 * Elderly assessment clinic
	 */
	_275576008 ("275576008", "Elderly assessment clinic"),
	
	/**
	 * Wart clinic
	 */
	_275692008 ("275692008", "Wart clinic"),
	
	/**
	 * Dental surgery premises
	 */
	_282086004 ("282086004", "Dental surgery premises"),
	
	/**
	 * Dental laboratory
	 */
	_282087008 ("282087008", "Dental laboratory"),
	
	/**
	 * Location within secure unit
	 */
	_284455000 ("284455000", "Location within secure unit"),
	
	/**
	 * Hospital department
	 */
	_284548004 ("284548004", "Hospital department"),
	
	/**
	 * Special hospital
	 */
	_288561005 ("288561005", "Special hospital"),
	
	/**
	 * Secure unit
	 */
	_288562003 ("288562003", "Secure unit"),
	
	/**
	 * Medical centre
	 */
	_288565001 ("288565001", "Medical centre"),
	
	/**
	 * General practice examination room
	 */
	_288566000 ("288566000", "General practice examination room"),
	
	/**
	 * General practice treatment room
	 */
	_288567009 ("288567009", "General practice treatment room"),
	
	/**
	 * General practice consulting room
	 */
	_288568004 ("288568004", "General practice consulting room"),
	
	/**
	 * General practice reception area
	 */
	_288569007 ("288569007", "General practice reception area"),
	
	/**
	 * General practice waiting room
	 */
	_288570008 ("288570008", "General practice waiting room"),
	
	/**
	 * Toilet on general practice premises
	 */
	_288572000 ("288572000", "Toilet on general practice premises"),
	
	/**
	 * Community health care environment
	 */
	_288573005 ("288573005", "Community health care environment"),
	
	/**
	 * Private hospital
	 */
	_309895006 ("309895006", "Private hospital"),
	
	/**
	 * Tertiary referral hospital
	 */
	_309896007 ("309896007", "Tertiary referral hospital"),
	
	/**
	 * Psychiatric day care hospital
	 */
	_309897003 ("309897003", "Psychiatric day care hospital"),
	
	/**
	 * Psychogeriatric day hospital
	 */
	_309898008 ("309898008", "Psychogeriatric day hospital"),
	
	/**
	 * Elderly severely mentally ill day hospital
	 */
	_309899000 ("309899000", "Elderly severely mentally ill day hospital"),
	
	/**
	 * Care of the elderly day hospital
	 */
	_309900005 ("309900005", "Care of the elderly day hospital"),
	
	/**
	 * Anaesthetic department
	 */
	_309901009 ("309901009", "Anaesthetic department"),
	
	/**
	 * Clinical oncology department
	 */
	_309902002 ("309902002", "Clinical oncology department"),
	
	/**
	 * Radiotherapy department
	 */
	_309903007 ("309903007", "Radiotherapy department"),
	
	/**
	 * Intensive care unit
	 */
	_309904001 ("309904001", "Intensive care unit"),
	
	/**
	 * Adult intensive care unit
	 */
	_309905000 ("309905000", "Adult intensive care unit"),
	
	/**
	 * Burns unit
	 */
	_309906004 ("309906004", "Burns unit"),
	
	/**
	 * Cardiac intensive care unit
	 */
	_309907008 ("309907008", "Cardiac intensive care unit"),
	
	/**
	 * Metabolic intensive care unit
	 */
	_309908003 ("309908003", "Metabolic intensive care unit"),
	
	/**
	 * Neurological intensive care unit
	 */
	_309909006 ("309909006", "Neurological intensive care unit"),
	
	/**
	 * Paediatric intensive care unit
	 */
	_309910001 ("309910001", "Paediatric intensive care unit"),
	
	/**
	 * Respiratory intensive care unit
	 */
	_309911002 ("309911002", "Respiratory intensive care unit"),
	
	/**
	 * Medical department
	 */
	_309912009 ("309912009", "Medical department"),
	
	/**
	 * Clinical allergy department
	 */
	_309913004 ("309913004", "Clinical allergy department"),
	
	/**
	 * Audiology department
	 */
	_309914005 ("309914005", "Audiology department"),
	
	/**
	 * Cardiology department
	 */
	_309915006 ("309915006", "Cardiology department"),
	
	/**
	 * Chest medicine department
	 */
	_309916007 ("309916007", "Chest medicine department"),
	
	/**
	 * Thoracic medicine department
	 */
	_309917003 ("309917003", "Thoracic medicine department"),
	
	/**
	 * Respiratory medicine department
	 */
	_309918008 ("309918008", "Respiratory medicine department"),
	
	/**
	 * Clinical immunology department
	 */
	_309919000 ("309919000", "Clinical immunology department"),
	
	/**
	 * Clinical neurophysiology department
	 */
	_309920006 ("309920006", "Clinical neurophysiology department"),
	
	/**
	 * Clinical pharmacology department
	 */
	_309921005 ("309921005", "Clinical pharmacology department"),
	
	/**
	 * Clinical physiology department
	 */
	_309922003 ("309922003", "Clinical physiology department"),
	
	/**
	 * Dermatology department
	 */
	_309923008 ("309923008", "Dermatology department"),
	
	/**
	 * Diabetic department
	 */
	_309924002 ("309924002", "Diabetic department"),
	
	/**
	 * Endocrinology department
	 */
	_309925001 ("309925001", "Endocrinology department"),
	
	/**
	 * Gastroenterology department
	 */
	_309926000 ("309926000", "Gastroenterology department"),
	
	/**
	 * General medical department
	 */
	_309927009 ("309927009", "General medical department"),
	
	/**
	 * Genetics department
	 */
	_309928004 ("309928004", "Genetics department"),
	
	/**
	 * Clinical genetics department
	 */
	_309929007 ("309929007", "Clinical genetics department"),
	
	/**
	 * Clinical cytogenetics department
	 */
	_309930002 ("309930002", "Clinical cytogenetics department"),
	
	/**
	 * Clinical molecular genetics department
	 */
	_309931003 ("309931003", "Clinical molecular genetics department"),
	
	/**
	 * Genitourinary medicine department
	 */
	_309932005 ("309932005", "Genitourinary medicine department"),
	
	/**
	 * Care of the elderly department
	 */
	_309933000 ("309933000", "Care of the elderly department"),
	
	/**
	 * Infectious diseases department
	 */
	_309934006 ("309934006", "Infectious diseases department"),
	
	/**
	 * Medical ophthalmology department
	 */
	_309935007 ("309935007", "Medical ophthalmology department"),
	
	/**
	 * Nephrology department
	 */
	_309936008 ("309936008", "Nephrology department"),
	
	/**
	 * Neurology department
	 */
	_309937004 ("309937004", "Neurology department"),
	
	/**
	 * Nuclear medicine department
	 */
	_309938009 ("309938009", "Nuclear medicine department"),
	
	/**
	 * Palliative care department
	 */
	_309939001 ("309939001", "Palliative care department"),
	
	/**
	 * Rehabilitation department
	 */
	_309940004 ("309940004", "Rehabilitation department"),
	
	/**
	 * Rheumatology department
	 */
	_309941000 ("309941000", "Rheumatology department"),
	
	/**
	 * Obstetrics and gynaecology department
	 */
	_309942007 ("309942007", "Obstetrics and gynaecology department"),
	
	/**
	 * Gynaecology department
	 */
	_309943002 ("309943002", "Gynaecology department"),
	
	/**
	 * Obstetrics department
	 */
	_309944008 ("309944008", "Obstetrics department"),
	
	/**
	 * Paediatric department
	 */
	_309945009 ("309945009", "Paediatric department"),
	
	/**
	 * Special care baby unit
	 */
	_309946005 ("309946005", "Special care baby unit"),
	
	/**
	 * Paediatric neurology department
	 */
	_309947001 ("309947001", "Paediatric neurology department"),
	
	/**
	 * Paediatric oncology department
	 */
	_309948006 ("309948006", "Paediatric oncology department"),
	
	/**
	 * Pain management department
	 */
	_309949003 ("309949003", "Pain management department"),
	
	/**
	 * Pathology department
	 */
	_309950003 ("309950003", "Pathology department"),
	
	/**
	 * Blood transfusion department
	 */
	_309951004 ("309951004", "Blood transfusion department"),
	
	/**
	 * Chemical pathology department
	 */
	_309952006 ("309952006", "Chemical pathology department"),
	
	/**
	 * General pathology department
	 */
	_309953001 ("309953001", "General pathology department"),
	
	/**
	 * Haematology department
	 */
	_309954007 ("309954007", "Haematology department"),
	
	/**
	 * Medical microbiology department
	 */
	_309956009 ("309956009", "Medical microbiology department"),
	
	/**
	 * Neuropathology department
	 */
	_309957000 ("309957000", "Neuropathology department"),
	
	/**
	 * Psychiatry department
	 */
	_309958005 ("309958005", "Psychiatry department"),
	
	/**
	 * Child and adolescent psychiatry department
	 */
	_309959002 ("309959002", "Child and adolescent psychiatry department"),
	
	/**
	 * Forensic psychiatry department
	 */
	_309960007 ("309960007", "Forensic psychiatry department"),
	
	/**
	 * Psychogeriatric department
	 */
	_309961006 ("309961006", "Psychogeriatric department"),
	
	/**
	 * Mental handicap psychiatry department
	 */
	_309962004 ("309962004", "Mental handicap psychiatry department"),
	
	/**
	 * Rehabilitation psychiatry department
	 */
	_309963009 ("309963009", "Rehabilitation psychiatry department"),
	
	/**
	 * Radiology department
	 */
	_309964003 ("309964003", "Radiology department"),
	
	/**
	 * Occupational health department
	 */
	_309965002 ("309965002", "Occupational health department"),
	
	/**
	 * Stroke unit
	 */
	_309966001 ("309966001", "Stroke unit"),
	
	/**
	 * Surgical department
	 */
	_309967005 ("309967005", "Surgical department"),
	
	/**
	 * Breast surgery department
	 */
	_309968000 ("309968000", "Breast surgery department"),
	
	/**
	 * Cardiothoracic surgery department
	 */
	_309969008 ("309969008", "Cardiothoracic surgery department"),
	
	/**
	 * Thoracic surgery department
	 */
	_309970009 ("309970009", "Thoracic surgery department"),
	
	/**
	 * Cardiac surgery department
	 */
	_309971008 ("309971008", "Cardiac surgery department"),
	
	/**
	 * Dental surgery department
	 */
	_309972001 ("309972001", "Dental surgery department"),
	
	/**
	 * General dental surgery department
	 */
	_309973006 ("309973006", "General dental surgery department"),
	
	/**
	 * Oral surgery department
	 */
	_309974000 ("309974000", "Oral surgery department"),
	
	/**
	 * Orthodontics department
	 */
	_309975004 ("309975004", "Orthodontics department"),
	
	/**
	 * Paediatric dentistry department
	 */
	_309976003 ("309976003", "Paediatric dentistry department"),
	
	/**
	 * Restorative dentistry department
	 */
	_309977007 ("309977007", "Restorative dentistry department"),
	
	/**
	 * Ear, nose and throat department
	 */
	_309978002 ("309978002", "Ear, nose and throat department"),
	
	/**
	 * Endocrine surgery department
	 */
	_309979005 ("309979005", "Endocrine surgery department"),
	
	/**
	 * Gastrointestinal surgery department
	 */
	_309980008 ("309980008", "Gastrointestinal surgery department"),
	
	/**
	 * General gastrointestinal surgery department
	 */
	_309981007 ("309981007", "General gastrointestinal surgery department"),
	
	/**
	 * Upper gastrointestinal surgery department
	 */
	_309982000 ("309982000", "Upper gastrointestinal surgery department"),
	
	/**
	 * Colorectal surgery department
	 */
	_309983005 ("309983005", "Colorectal surgery department"),
	
	/**
	 * General surgical department
	 */
	_309984004 ("309984004", "General surgical department"),
	
	/**
	 * Hand surgery department
	 */
	_309985003 ("309985003", "Hand surgery department"),
	
	/**
	 * Hepatobiliary surgical department
	 */
	_309986002 ("309986002", "Hepatobiliary surgical department"),
	
	/**
	 * Neurosurgical department
	 */
	_309987006 ("309987006", "Neurosurgical department"),
	
	/**
	 * Ophthalmology department
	 */
	_309988001 ("309988001", "Ophthalmology department"),
	
	/**
	 * Orthopaedic department
	 */
	_309989009 ("309989009", "Orthopaedic department"),
	
	/**
	 * Pancreatic surgery department
	 */
	_309990000 ("309990000", "Pancreatic surgery department"),
	
	/**
	 * Paediatric surgical department
	 */
	_309991001 ("309991001", "Paediatric surgical department"),
	
	/**
	 * Plastic surgery department
	 */
	_309992008 ("309992008", "Plastic surgery department"),
	
	/**
	 * Surgical transplant department
	 */
	_309993003 ("309993003", "Surgical transplant department"),
	
	/**
	 * Trauma surgery department
	 */
	_309994009 ("309994009", "Trauma surgery department"),
	
	/**
	 * Urology department
	 */
	_309995005 ("309995005", "Urology department"),
	
	/**
	 * Vascular surgery department
	 */
	_309996006 ("309996006", "Vascular surgery department"),
	
	/**
	 * Young disabled unit
	 */
	_309997002 ("309997002", "Young disabled unit"),
	
	/**
	 * Day ward
	 */
	_309998007 ("309998007", "Day ward"),
	
	/**
	 * Paediatric Accident and Emergency department
	 */
	_310203004 ("310203004", "Paediatric Accident and Emergency department"),
	
	/**
	 * Ophthalmology Accident and Emergency department
	 */
	_310204005 ("310204005", "Ophthalmology Accident and Emergency department"),
	
	/**
	 * Private nursing home
	 */
	_310206007 ("310206007", "Private nursing home"),
	
	/**
	 * Hospital clinic
	 */
	_310390009 ("310390009", "Hospital clinic"),
	
	/**
	 * Community clinic
	 */
	_310391008 ("310391008", "Community clinic"),
	
	/**
	 * Physiotherapy department
	 */
	_310464005 ("310464005", "Physiotherapy department"),
	
	/**
	 * Hospital-based outpatient allergy clinic
	 */
	_360957003 ("360957003", "Hospital-based outpatient allergy clinic"),
	
	/**
	 * Hospital-based outpatient immunology clinic
	 */
	_360966004 ("360966004", "Hospital-based outpatient immunology clinic"),
	
	/**
	 * Maternity clinic
	 */
	_394573001 ("394573001", "Maternity clinic"),
	
	/**
	 * Antenatal clinic
	 */
	_394574007 ("394574007", "Antenatal clinic"),
	
	/**
	 * Postnatal clinic
	 */
	_394575008 ("394575008", "Postnatal clinic"),
	
	/**
	 * Primary Care Trust site
	 */
	_394750006 ("394750006", "Primary Care Trust site"),
	
	/**
	 * Independent provider site
	 */
	_394759007 ("394759007", "Independent provider site"),
	
	/**
	 * GP practice site
	 */
	_394761003 ("394761003", "GP practice site"),
	
	/**
	 * Health encounter sites
	 */
	_394777002 ("394777002", "Health encounter sites"),
	
	/**
	 * Clients or patients home
	 */
	_394778007 ("394778007", "Client's or patient's home"),
	
	/**
	 * Group home managed by local authority
	 */
	_394783004 ("394783004", "Group home managed by local authority"),
	
	/**
	 * Group home managed by voluntary or private agents
	 */
	_394784005 ("394784005", "Group home managed by voluntary or private agents"),
	
	/**
	 * Day centre managed by local authority
	 */
	_394789000 ("394789000", "Day centre managed by local authority"),
	
	/**
	 * Day centre managed by voluntary or private agents
	 */
	_394790009 ("394790009", "Day centre managed by voluntary or private agents"),
	
	/**
	 * Health clinic managed by voluntary or private agents
	 */
	_394794000 ("394794000", "Health clinic managed by voluntary or private agents"),
	
	/**
	 * Resource centre managed by local authority
	 */
	_394797007 ("394797007", "Resource centre managed by local authority"),
	
	/**
	 * Resource centre managed by voluntary or private agents
	 */
	_394798002 ("394798002", "Resource centre managed by voluntary or private agents"),
	
	/**
	 * Joint consultant clinic
	 */
	_394865001 ("394865001", "Joint consultant clinic"),
	
	/**
	 * Other location within hospital premises
	 */
	_397784001 ("397784001", "Other location within hospital premises"),
	
	/**
	 * Preoperative screening clinic
	 */
	_397872006 ("397872006", "Preoperative screening clinic"),
	
	/**
	 * Preoperative holding area
	 */
	_397983003 ("397983003", "Preoperative holding area"),
	
	/**
	 * Unspecified location within hospital premises
	 */
	_398145002 ("398145002", "Unspecified location within hospital premises"),
	
	/**
	 * Medical or surgical floor
	 */
	_398156002 ("398156002", "Medical or surgical floor"),
	
	/**
	 * Postoperative anaesthesia care unit
	 */
	_398161000 ("398161000", "Postoperative anaesthesia care unit"),
	
	/**
	 * Psychiatric intensive care unit
	 */
	_404821007 ("404821007", "Psychiatric intensive care unit"),
	
	/**
	 * Neonatal intensive care unit
	 */
	_405269005 ("405269005", "Neonatal intensive care unit"),
	
	/**
	 * Labour and delivery unit
	 */
	_405606005 ("405606005", "Labour and delivery unit"),
	
	/**
	 * Ambulatory surgery centre
	 */
	_405607001 ("405607001", "Ambulatory surgery centre"),
	
	/**
	 * Academic medical centre
	 */
	_405608006 ("405608006", "Academic medical centre"),
	
	/**
	 * Mass casualty setting
	 */
	_409518000 ("409518000", "Mass casualty setting"),
	
	/**
	 * Contained casualty setting
	 */
	_409519008 ("409519008", "Contained casualty setting"),
	
	/**
	 * Hospital decontamination room
	 */
	_409685000 ("409685000", "Hospital decontamination room"),
	
	/**
	 * Hospital isolation room
	 */
	_409688003 ("409688003", "Hospital isolation room"),
	
	/**
	 * Adult day care centre
	 */
	_413456002 ("413456002", "Adult day care centre"),
	
	/**
	 * Child day care centre
	 */
	_413817003 ("413817003", "Child day care centre"),
	
	/**
	 * Delivery suite
	 */
	_413964002 ("413964002", "Delivery suite"),
	
	/**
	 * Induction room
	 */
	_414485004 ("414485004", "Induction room"),
	
	/**
	 * PACU Phase 1
	 */
	_415142008 ("415142008", "PACU Phase 1"),
	
	/**
	 * PACU Phase 2
	 */
	_415143003 ("415143003", "PACU Phase 2"),
	
	/**
	 * Community medical centre
	 */
	_416957006 ("416957006", "Community medical centre"),
	
	/**
	 * Surgical stepdown unit
	 */
	_418028002 ("418028002", "Surgical stepdown unit"),
	
	/**
	 * Surgical intensive care unit
	 */
	_418433008 ("418433008", "Surgical intensive care unit"),
	
	/**
	 * Dialysis unit
	 */
	_418518002 ("418518002", "Dialysis unit"),
	
	/**
	 * Cardiac stepdown unit
	 */
	_418649002 ("418649002", "Cardiac stepdown unit"),
	
	/**
	 * Medical stepdown unit
	 */
	_419390002 ("419390002", "Medical stepdown unit"),
	
	/**
	 * Stepdown unit
	 */
	_419590001 ("419590001", "Stepdown unit"),
	
	/**
	 * Paediatric medicine department
	 */
	_420223003 ("420223003", "Paediatric medicine department"),
	
	/**
	 * Obstetrical stepdown unit
	 */
	_420280003 ("420280003", "Obstetrical stepdown unit"),
	
	/**
	 * Telemetry unit
	 */
	_422798006 ("422798006", "Telemetry unit"),
	
	/**
	 * Burns intensive care unit
	 */
	_426439001 ("426439001", "Burns intensive care unit"),
	
	/**
	 * Newborn nursery unit
	 */
	_427695007 ("427695007", "Newborn nursery unit"),
	
	/**
	 * Genitourinary medicine clinic
	 */
	_431654006 ("431654006", "Genitourinary medicine clinic"),
	
	/**
	 * Primary care department
	 */
	_441480003 ("441480003", "Primary care department"),
	
	/**
	 * Tropical medicine department
	 */
	_441548002 ("441548002", "Tropical medicine department"),
	
	/**
	 * Diagnostic imaging department
	 */
	_441662001 ("441662001", "Diagnostic imaging department"),
	
	/**
	 * Histopathology department
	 */
	_441950002 ("441950002", "Histopathology department"),
	
	/**
	 * Medical intensive care unit
	 */
	_441994008 ("441994008", "Medical intensive care unit"),
	
	/**
	 * Sleep apnoea clinic
	 */
	_443621004 ("443621004", "Sleep apnoea clinic"),
	
	/**
	 * Nonacute hospital
	 */
	_443750004 ("443750004", "Nonacute hospital"),
	
	/**
	 * Colposcopy clinic
	 */
	_444766008 ("444766008", "Colposcopy clinic"),
	
	/**
	 * Dental hospital
	 */
	_448399001 ("448399001", "Dental hospital"),
	
	/**
	 * Diabetes clinic
	 */
	_702706001 ("702706001", "Diabetes clinic"),
	
	/**
	 * Acute pain clinic
	 */
	_702812003 ("702812003", "Acute pain clinic"),
	
	/**
	 * Acquired immune deficiency syndrome clinic
	 */
	_702813008 ("702813008", "Acquired immune deficiency syndrome clinic"),
	
	/**
	 * Allergy clinic
	 */
	_702814002 ("702814002", "Allergy clinic"),
	
	/**
	 * After hours clinic
	 */
	_702819007 ("702819007", "After hours clinic"),
	
	/**
	 * Amputee clinic
	 */
	_702821002 ("702821002", "Amputee clinic"),
	
	/**
	 * Anticoagulation clinic
	 */
	_702822009 ("702822009", "Anticoagulation clinic"),
	
	/**
	 * Asthma clinic
	 */
	_702823004 ("702823004", "Asthma clinic"),
	
	/**
	 * Audiology clinic
	 */
	_702824005 ("702824005", "Audiology clinic"),
	
	/**
	 * Autism clinic
	 */
	_702825006 ("702825006", "Autism clinic"),
	
	/**
	 * Bipolar clinic
	 */
	_702826007 ("702826007", "Bipolar clinic"),
	
	/**
	 * Bone marrow transplant clinic
	 */
	_702827003 ("702827003", "Bone marrow transplant clinic"),
	
	/**
	 * Breast clinic
	 */
	_702828008 ("702828008", "Breast clinic"),
	
	/**
	 * Cardiology clinic
	 */
	_702830005 ("702830005", "Cardiology clinic"),
	
	/**
	 * Child and adolescent medicine clinic
	 */
	_702831009 ("702831009", "Child and adolescent medicine clinic"),
	
	/**
	 * Child and adolescent neurology clinic
	 */
	_702832002 ("702832002", "Child and adolescent neurology clinic"),
	
	/**
	 * Child and adolescent psychiatry clinic
	 */
	_702833007 ("702833007", "Child and adolescent psychiatry clinic"),
	
	/**
	 * Child speech and language therapy clinic
	 */
	_702834001 ("702834001", "Child speech and language therapy clinic"),
	
	/**
	 * Chronic obstructive pulmonary disease clinic
	 */
	_702839006 ("702839006", "Chronic obstructive pulmonary disease clinic"),
	
	/**
	 * Chronic pain clinic
	 */
	_702840008 ("702840008", "Chronic pain clinic"),
	
	/**
	 * Cleft palate clinic
	 */
	_702841007 ("702841007", "Cleft palate clinic"),
	
	/**
	 * Congenital heart disease clinic
	 */
	_702842000 ("702842000", "Congenital heart disease clinic"),
	
	/**
	 * Congestive heart failure clinic
	 */
	_702843005 ("702843005", "Congestive heart failure clinic"),
	
	/**
	 * Cystic fibrosis clinic
	 */
	_702844004 ("702844004", "Cystic fibrosis clinic"),
	
	/**
	 * Deep vein thrombosis clinic
	 */
	_702845003 ("702845003", "Deep vein thrombosis clinic"),
	
	/**
	 * Dental clinic
	 */
	_702846002 ("702846002", "Dental clinic"),
	
	/**
	 * Dermatology clinic
	 */
	_702847006 ("702847006", "Dermatology clinic"),
	
	/**
	 * Diabetes foot care clinic
	 */
	_702848001 ("702848001", "Diabetes foot care clinic"),
	
	/**
	 * Diabetes in pregnancy clinic
	 */
	_702849009 ("702849009", "Diabetes in pregnancy clinic"),
	
	/**
	 * Diabetic retinopathy clinic
	 */
	_702850009 ("702850009", "Diabetic retinopathy clinic"),
	
	/**
	 * Down syndrome clinic
	 */
	_702851008 ("702851008", "Down syndrome clinic"),
	
	/**
	 * Ear, nose and throat clinic
	 */
	_702852001 ("702852001", "Ear, nose and throat clinic"),
	
	/**
	 * Eating disorder clinic
	 */
	_702853006 ("702853006", "Eating disorder clinic"),
	
	/**
	 * Endocrinology clinic
	 */
	_702854000 ("702854000", "Endocrinology clinic"),
	
	/**
	 * Family medicine clinic
	 */
	_702855004 ("702855004", "Family medicine clinic"),
	
	/**
	 * Family planning clinic
	 */
	_702856003 ("702856003", "Family planning clinic"),
	
	/**
	 * Fetal alcohol clinic
	 */
	_702857007 ("702857007", "Fetal alcohol clinic"),
	
	/**
	 * Fracture clinic
	 */
	_702858002 ("702858002", "Fracture clinic"),
	
	/**
	 * Gastroenterology clinic
	 */
	_702859005 ("702859005", "Gastroenterology clinic"),
	
	/**
	 * General surgery clinic
	 */
	_702860000 ("702860000", "General surgery clinic"),
	
	/**
	 * Genetics clinic
	 */
	_702861001 ("702861001", "Genetics clinic"),
	
	/**
	 * Gynaecology clinic
	 */
	_702862008 ("702862008", "Gynaecology clinic"),
	
	/**
	 * Hand clinic
	 */
	_702863003 ("702863003", "Hand clinic"),
	
	/**
	 * Headache clinic
	 */
	_702864009 ("702864009", "Headache clinic"),
	
	/**
	 * Haematology clinic
	 */
	_702865005 ("702865005", "Haematology clinic"),
	
	/**
	 * Haemophilia clinic
	 */
	_702866006 ("702866006", "Haemophilia clinic"),
	
	/**
	 * Hypertension clinic
	 */
	_702867002 ("702867002", "Hypertension clinic"),
	
	/**
	 * Immunology clinic
	 */
	_702868007 ("702868007", "Immunology clinic"),
	
	/**
	 * Infectious disease clinic
	 */
	_702870003 ("702870003", "Infectious disease clinic"),
	
	/**
	 * Infertility clinic
	 */
	_702871004 ("702871004", "Infertility clinic"),
	
	/**
	 * Internal medicine clinic
	 */
	_702877000 ("702877000", "Internal medicine clinic"),
	
	/**
	 * Land ambulance
	 */
	_702878005 ("702878005", "Land ambulance"),
	
	/**
	 * Laser eye clinic
	 */
	_702879002 ("702879002", "Laser eye clinic"),
	
	/**
	 * Lymphoedema clinic
	 */
	_702880004 ("702880004", "Lymphoedema clinic"),
	
	/**
	 * Medical day care
	 */
	_702881000 ("702881000", "Medical day care"),
	
	/**
	 * Mental health clinic
	 */
	_702882007 ("702882007", "Mental health clinic"),
	
	/**
	 * Mood or anxiety disorder clinic
	 */
	_702883002 ("702883002", "Mood or anxiety disorder clinic"),
	
	/**
	 * Movement disorder clinic
	 */
	_702884008 ("702884008", "Movement disorder clinic"),
	
	/**
	 * Multiple sclerosis clinic
	 */
	_702885009 ("702885009", "Multiple sclerosis clinic"),
	
	/**
	 * Nephrology clinic
	 */
	_702886005 ("702886005", "Nephrology clinic"),
	
	/**
	 * Neurology clinic
	 */
	_702887001 ("702887001", "Neurology clinic"),
	
	/**
	 * Neuromuscular clinic
	 */
	_702888006 ("702888006", "Neuromuscular clinic"),
	
	/**
	 * Neurophysiology clinic
	 */
	_702889003 ("702889003", "Neurophysiology clinic"),
	
	/**
	 * Neuropsychology clinic
	 */
	_702890007 ("702890007", "Neuropsychology clinic"),
	
	/**
	 * Neurosurgery clinic
	 */
	_702891006 ("702891006", "Neurosurgery clinic"),
	
	/**
	 * Obstetrics and gynaecology clinic
	 */
	_702892004 ("702892004", "Obstetrics and gynaecology clinic"),
	
	/**
	 * Occupational therapy clinic
	 */
	_702893009 ("702893009", "Occupational therapy clinic"),
	
	/**
	 * Ophthalmology clinic
	 */
	_702894003 ("702894003", "Ophthalmology clinic"),
	
	/**
	 * Osteoporosis clinic
	 */
	_702895002 ("702895002", "Osteoporosis clinic"),
	
	/**
	 * Pacemaker clinic
	 */
	_702896001 ("702896001", "Pacemaker clinic"),
	
	/**
	 * Pain clinic
	 */
	_702897005 ("702897005", "Pain clinic"),
	
	/**
	 * Parkinsons disease clinic
	 */
	_702898000 ("702898000", "Parkinson's disease clinic"),
	
	/**
	 * Paediatric cardiology clinic
	 */
	_702899008 ("702899008", "Paediatric cardiology clinic"),
	
	/**
	 * Paediatric clinic
	 */
	_702900003 ("702900003", "Paediatric clinic"),
	
	/**
	 * Paediatric dermatology clinic
	 */
	_702901004 ("702901004", "Paediatric dermatology clinic"),
	
	/**
	 * Paediatric ear, nose and throat clinic
	 */
	_702902006 ("702902006", "Paediatric ear, nose and throat clinic"),
	
	/**
	 * Paediatric endocrinology clinic
	 */
	_702903001 ("702903001", "Paediatric endocrinology clinic"),
	
	/**
	 * Paediatric gastroenterology clinic
	 */
	_702904007 ("702904007", "Paediatric gastroenterology clinic"),
	
	/**
	 * Paediatric haematology clinic
	 */
	_702905008 ("702905008", "Paediatric haematology clinic"),
	
	/**
	 * Paediatric infectious disease clinic
	 */
	_702906009 ("702906009", "Paediatric infectious disease clinic"),
	
	/**
	 * Paediatric rheumatology clinic
	 */
	_702907000 ("702907000", "Paediatric rheumatology clinic"),
	
	/**
	 * Paediatric urology clinic
	 */
	_702908005 ("702908005", "Paediatric urology clinic"),
	
	/**
	 * Paediatric oncology clinic
	 */
	_702909002 ("702909002", "Paediatric oncology clinic"),
	
	/**
	 * Pelvic floor clinic
	 */
	_702910007 ("702910007", "Pelvic floor clinic"),
	
	/**
	 * Podiatry clinic
	 */
	_702911006 ("702911006", "Podiatry clinic"),
	
	/**
	 * Preventive medicine clinic
	 */
	_702912004 ("702912004", "Preventive medicine clinic"),
	
	/**
	 * Proctology clinic
	 */
	_702913009 ("702913009", "Proctology clinic"),
	
	/**
	 * Psychiatry clinic
	 */
	_702914003 ("702914003", "Psychiatry clinic"),
	
	/**
	 * Psychology clinic
	 */
	_702915002 ("702915002", "Psychology clinic"),
	
	/**
	 * Rehabilitation clinic
	 */
	_702916001 ("702916001", "Rehabilitation clinic"),
	
	/**
	 * Respiratory disease clinic
	 */
	_702917005 ("702917005", "Respiratory disease clinic"),
	
	/**
	 * Rheumatology clinic
	 */
	_702918000 ("702918000", "Rheumatology clinic"),
	
	/**
	 * Schizophrenia clinic
	 */
	_702919008 ("702919008", "Schizophrenia clinic"),
	
	/**
	 * Seizure clinic
	 */
	_702920002 ("702920002", "Seizure clinic"),
	
	/**
	 * Sexual health clinic
	 */
	_702921003 ("702921003", "Sexual health clinic"),
	
	/**
	 * Speech and language therapy clinic
	 */
	_702922005 ("702922005", "Speech and language therapy clinic"),
	
	/**
	 * Sports medicine clinic
	 */
	_702923000 ("702923000", "Sports medicine clinic"),
	
	/**
	 * Stroke clinic
	 */
	_702924006 ("702924006", "Stroke clinic"),
	
	/**
	 * Swallow clinic
	 */
	_702925007 ("702925007", "Swallow clinic"),
	
	/**
	 * Tourette syndrome clinic
	 */
	_702926008 ("702926008", "Tourette syndrome clinic"),
	
	/**
	 * Urgent care clinic
	 */
	_702927004 ("702927004", "Urgent care clinic"),
	
	/**
	 * Urology clinic
	 */
	_702928009 ("702928009", "Urology clinic"),
	
	/**
	 * Vertigo clinic
	 */
	_702929001 ("702929001", "Vertigo clinic"),
	
	/**
	 * Plastic surgery clinic
	 */
	_702930006 ("702930006", "Plastic surgery clinic"),
	
	/**
	 * Oncology clinic
	 */
	_702931005 ("702931005", "Oncology clinic"),
	
	/**
	 * Oral and maxillofacial surgery clinic
	 */
	_702932003 ("702932003", "Oral and maxillofacial surgery clinic"),
	
	/**
	 * Orthopaedic clinic
	 */
	_702933008 ("702933008", "Orthopaedic clinic"),
	
	/**
	 * Paediatric surgery clinic
	 */
	_702934002 ("702934002", "Paediatric surgery clinic"),
	
	/**
	 * Physiotherapy clinic
	 */
	_702935001 ("702935001", "Physiotherapy clinic"),
	
	/**
	 * Nurse practitioner clinic
	 */
	_703069008 ("703069008", "Nurse practitioner clinic"),
	
	/**
	 * Gynaecology outreach clinic
	 */
	_198931000000104 ("198931000000104", "Gynaecology outreach clinic"),
	
	/**
	 * General surgery outreach clinic
	 */
	_198941000000108 ("198941000000108", "General surgery outreach clinic"),
	
	/**
	 * Psychiatric outreach clinic
	 */
	_198951000000106 ("198951000000106", "Psychiatric outreach clinic"),
	
	/**
	 * General medicine outreach clinic
	 */
	_198961000000109 ("198961000000109", "General medicine outreach clinic"),
	
	/**
	 * Asthma outreach clinic
	 */
	_198971000000102 ("198971000000102", "Asthma outreach clinic"),
	
	/**
	 * Diabetic antenatal clinic
	 */
	_203951000000101 ("203951000000101", "Diabetic antenatal clinic"),
	
	/**
	 * Ambulance service care setting
	 */
	_312351000000101 ("312351000000101", "Ambulance service care setting"),
	
	/**
	 * Primary care out of hours service care setting
	 */
	_312361000000103 ("312361000000103", "Primary care out of hours service care setting"),
	
	/**
	 * Social care setting
	 */
	_312371000000105 ("312371000000105", "Social care setting"),
	
	/**
	 * NHS Direct Contact Centre
	 */
	_312391000000109 ("312391000000109", "NHS Direct Contact Centre"),
	
	/**
	 * Urgent care centre
	 */
	_312401000000107 ("312401000000107", "Urgent care centre"),
	
	/**
	 * Minor injuries unit
	 */
	_312411000000109 ("312411000000109", "Minor injuries unit"),
	
	/**
	 * Community mental health care setting
	 */
	_312471000000104 ("312471000000104", "Community mental health care setting"),
	
	/**
	 * Out of hours service care setting
	 */
	_313161000000107 ("313161000000107", "Out of hours service care setting"),
	
	/**
	 * Specialist treatment area
	 */
	_506341000000101 ("506341000000101", "Specialist treatment area"),
	
	/**
	 * Group home managed by NHS
	 */
	_930711000000104 ("930711000000104", "Group home managed by NHS"),
	
	/**
	 * NHS day care facility on NHS hospital site
	 */
	_930721000000105 ("930721000000105", "NHS day care facility on NHS hospital site"),
	
	/**
	 * NHS day care facility on other site
	 */
	_930731000000107 ("930731000000107", "NHS day care facility on other site"),
	
	/**
	 * NHS consultant clinic premises on NHS hospital site
	 */
	_930741000000103 ("930741000000103", "NHS consultant clinic premises on NHS hospital site"),
	
	/**
	 * NHS consultant clinic premises off NHS hospital site
	 */
	_930751000000100 ("930751000000100", "NHS consultant clinic premises off NHS hospital site"),
	
	/**
	 * Health clinic managed by NHS
	 */
	_930761000000102 ("930761000000102", "Health clinic managed by NHS"),
	
	/**
	 * Resource centre on NHS hospital site
	 */
	_930771000000109 ("930771000000109", "Resource centre on NHS hospital site"),
	
	/**
	 * Resource centre managed by the NHS off NHS hospital site
	 */
	_930781000000106 ("930781000000106", "Resource centre managed by the NHS off NHS hospital site"),
	
	/**
	 * Professional staff group department on NHS hospital site
	 */
	_930791000000108 ("930791000000108", "Professional staff group department on NHS hospital site"),
	
	/**
	 * Professional staff group department managed by the NHS off NHS hospital site
	 */
	_930801000000107 ("930801000000107", "Professional staff group department managed by the NHS off NHS hospital site"),
	
	/**
	 * Hospital-based outpatient rheumatology clinic
	 */
	_Hospitalbasedoutpatientrheumatologyclinic ("331006", "Hospital-based outpatient rheumatology clinic"),
	
	/**
	 * Helicopter-based care
	 */
	_Helicopterbasedcare ("901005", "Helicopter-based care"),
	
	/**
	 * Day care centre
	 */
	_Daycarecentre ("1348009", "Day care centre"),
	
	/**
	 * Free-standing breast clinic
	 */
	_Freestandingbreastclinic ("1526002", "Free-standing breast clinic"),
	
	/**
	 * Free-standing radiology facility
	 */
	_Freestandingradiologyfacility ("1773006", "Free-standing radiology facility"),
	
	/**
	 * Hospital-based outpatient geriatric clinic
	 */
	_Hospitalbasedoutpatientgeriatricclinic ("1814000", "Hospital-based outpatient geriatric clinic"),
	
	/**
	 * Hospital ship
	 */
	_Hospitalship ("2081004", "Hospital ship"),
	
	/**
	 * Hospital-based outpatient infectious disease clinic
	 */
	_Hospitalbasedoutpatientinfectiousdiseaseclinic ("2849009", "Hospital-based outpatient infectious disease clinic"),
	
	/**
	 * Aid car-based care
	 */
	_Aidcarbasedcare ("2961002", "Aid car-based care"),
	
	/**
	 * Hospital-based outpatient paediatric clinic
	 */
	_Hospitalbasedoutpatientpaediatricclinic ("3729002", "Hospital-based outpatient paediatric clinic"),
	
	/**
	 * Free-standing haematology clinic
	 */
	_Freestandinghaematologyclinic ("3768009", "Free-standing haematology clinic"),
	
	/**
	 * Military field hospital
	 */
	_Militaryfieldhospital ("4322002", "Military field hospital"),
	
	/**
	 * Hospital-based outpatient peripheral vascular clinic
	 */
	_Hospitalbasedoutpatientperipheralvascularclinic ("5584006", "Hospital-based outpatient peripheral vascular clinic"),
	
	/**
	 * Local community health centre
	 */
	_Localcommunityhealthcentre ("6827000", "Local community health centre"),
	
	/**
	 * Hospital-based outpatient dental clinic
	 */
	_Hospitalbasedoutpatientdentalclinic ("10206005", "Hospital-based outpatient dental clinic"),
	
	/**
	 * Free-standing ambulatory surgery facility
	 */
	_Freestandingambulatorysurgeryfacility ("10531005", "Free-standing ambulatory surgery facility"),
	
	/**
	 * Free-standing peripheral vascular clinic
	 */
	_Freestandingperipheralvascularclinic ("11119007", "Free-standing peripheral vascular clinic"),
	
	/**
	 * Ambulance-based care
	 */
	_Ambulancebasedcare ("11424001", "Ambulance-based care"),
	
	/**
	 * Free-standing gastroenterology clinic
	 */
	_Freestandinggastroenterologyclinic ("13015006", "Free-standing gastroenterology clinic"),
	
	/**
	 * Free-standing physical medicine clinic
	 */
	_Freestandingphysicalmedicineclinic ("13656005", "Free-standing physical medicine clinic"),
	
	/**
	 * Hospital-based outpatient mental health clinic
	 */
	_Hospitalbasedoutpatientmentalhealthclinic ("14866005", "Hospital-based outpatient mental health clinic"),
	
	/**
	 * Skilled nursing facility, level 2 care
	 */
	_Skillednursingfacilitylevel2care ("16384001", "Skilled nursing facility, level 2 care"),
	
	/**
	 * Free-standing ophthalmology clinic
	 */
	_Freestandingophthalmologyclinic ("16597003", "Free-standing ophthalmology clinic"),
	
	/**
	 * Free-standing neurology clinic
	 */
	_Freestandingneurologyclinic ("16879004", "Free-standing neurology clinic"),
	
	/**
	 * Intermediate care hospital for mentally retarded
	 */
	_Intermediatecarehospitalformentallyretarded ("18095007", "Intermediate care hospital for mentally retarded"),
	
	/**
	 * Fee-for-service private physicians group office
	 */
	_Feeforserviceprivatephysiciansgroupoffice ("19602009", "Fee-for-service private physicians' group office"),
	
	/**
	 * Substance abuse treatment centre
	 */
	_Substanceabusetreatmentcentre ("20078004", "Substance abuse treatment centre"),
	
	/**
	 * Tertiary care hospital
	 */
	_Tertiarycarehospital ("20316001", "Tertiary care hospital"),
	
	/**
	 * Free-standing dermatology clinic
	 */
	_Freestandingdermatologyclinic ("21700006", "Free-standing dermatology clinic"),
	
	/**
	 * Free-standing dental clinic
	 */
	_Freestandingdentalclinic ("22201008", "Free-standing dental clinic"),
	
	/**
	 * Hospital
	 */
	_Hospital ("22232009", "Hospital"),
	
	/**
	 * Skilled nursing facility, level 1 care
	 */
	_Skillednursingfacilitylevel1care ("22357001", "Skilled nursing facility, level 1 care"),
	
	/**
	 * Hospital-based outpatient gynaecology clinic
	 */
	_Hospitalbasedoutpatientgynaecologyclinic ("22549003", "Hospital-based outpatient gynaecology clinic"),
	
	/**
	 * Hospital-based outpatient otorhinolaryngology clinic
	 */
	_Hospitalbasedoutpatientotorhinolaryngologyclinic ("23392004", "Hospital-based outpatient otorhinolaryngology clinic"),
	
	/**
	 * Hospital-based outpatient breast clinic
	 */
	_Hospitalbasedoutpatientbreastclinic ("25567001", "Hospital-based outpatient breast clinic"),
	
	/**
	 * Sexually transmitted disease clinic
	 */
	_Sexuallytransmitteddiseaseclinic ("25681007", "Sexually transmitted disease clinic"),
	
	/**
	 * Retirement home
	 */
	_Retirementhome ("30629002", "Retirement home"),
	
	/**
	 * Prepaid private physicians group office
	 */
	_Prepaidprivatephysiciansgroupoffice ("31154006", "Prepaid private physicians' group office"),
	
	/**
	 * Hospital-based outpatient family medicine clinic
	 */
	_Hospitalbasedoutpatientfamilymedicineclinic ("31628002", "Hospital-based outpatient family medicine clinic"),
	
	/**
	 * Long term care hospital
	 */
	_Longtermcarehospital ("32074000", "Long term care hospital"),
	
	/**
	 * Free-standing immunology clinic
	 */
	_Freestandingimmunologyclinic ("32193000", "Free-standing immunology clinic"),
	
	/**
	 * Hospital-based outpatient department
	 */
	_Hospitalbasedoutpatientdepartment ("33022008", "Hospital-based outpatient department"),
	
	/**
	 * Burn centre
	 */
	_Burncentre ("34304006", "Burn centre"),
	
	/**
	 * Skilled nursing facility, level 4 care
	 */
	_Skillednursingfacilitylevel4care ("35313001", "Skilled nursing facility, level 4 care"),
	
	/**
	 * Ambulatory care site
	 */
	_Ambulatorycaresite ("35971002", "Ambulatory care site"),
	
	/**
	 * Trauma centre
	 */
	_Traumacentre ("36125001", "Trauma centre"),
	
	/**
	 * Hospital-based outpatient pain clinic
	 */
	_Hospitalbasedoutpatientpainclinic ("36293008", "Hospital-based outpatient pain clinic"),
	
	/**
	 * Free-standing general surgery clinic
	 */
	_Freestandinggeneralsurgeryclinic ("36490008", "Free-standing general surgery clinic"),
	
	/**
	 * Hospital-based outpatient rehabilitation clinic
	 */
	_Hospitalbasedoutpatientrehabilitationclinic ("37546005", "Hospital-based outpatient rehabilitation clinic"),
	
	/**
	 * Hospital-based outpatient dermatology clinic
	 */
	_Hospitalbasedoutpatientdermatologyclinic ("37550003", "Hospital-based outpatient dermatology clinic"),
	
	/**
	 * Hospital-based outpatient neurology clinic
	 */
	_Hospitalbasedoutpatientneurologyclinic ("38238005", "Hospital-based outpatient neurology clinic"),
	
	/**
	 * Treatment centre for emotionally disturbed children
	 */
	_Treatmentcentreforemotionallydisturbedchildren ("38426004", "Treatment centre for emotionally disturbed children"),
	
	/**
	 * Aeroplane-based care
	 */
	_Aeroplanebasedcare ("39176004", "Aeroplane-based care"),
	
	/**
	 * School infirmary
	 */
	_Schoolinfirmary ("39216000", "School infirmary"),
	
	/**
	 * Private physicians group office
	 */
	_Privatephysiciansgroupoffice ("39350007", "Private physicians' group office"),
	
	/**
	 * Free-standing pain clinic
	 */
	_Freestandingpainclinic ("39750005", "Free-standing pain clinic"),
	
	/**
	 * Residential school infirmary
	 */
	_Residentialschoolinfirmary ("39913001", "Residential school infirmary"),
	
	/**
	 * Free-standing geriatric clinic
	 */
	_Freestandinggeriatricclinic ("41844007", "Free-standing geriatric clinic"),
	
	/**
	 * Hospital-based radiotherapy facility
	 */
	_Hospitalbasedradiotherapyfacility ("42168000", "Hospital-based radiotherapy facility"),
	
	/**
	 * Nursing home
	 */
	_Nursinghome ("42665001", "Nursing home"),
	
	/**
	 * Site of care
	 */
	_Siteofcare ("43741000", "Site of care"),
	
	/**
	 * Free-standing rehabilitation clinic
	 */
	_Freestandingrehabilitationclinic ("44860005", "Free-standing rehabilitation clinic"),
	
	/**
	 * Primary care hospital
	 */
	_Primarycarehospital ("45131006", "Primary care hospital"),
	
	/**
	 * Free-standing urology clinic
	 */
	_Freestandingurologyclinic ("45288004", "Free-standing urology clinic"),
	
	/**
	 * Skilled nursing facility
	 */
	_Skillednursingfacility ("45618002", "Skilled nursing facility"),
	
	/**
	 * Free-standing laboratory facility
	 */
	_Freestandinglaboratoryfacility ("45899008", "Free-standing laboratory facility"),
	
	/**
	 * Secondary care hospital
	 */
	_Secondarycarehospital ("46111000", "Secondary care hospital"),
	
	/**
	 * Vaccination clinic
	 */
	_Vaccinationclinic ("46224007", "Vaccination clinic"),
	
	/**
	 * Cancer hospital
	 */
	_Cancerhospital ("48120004", "Cancer hospital"),
	
	/**
	 * Veterans administration hospital
	 */
	_Veteransadministrationhospital ("48311003", "Veteran's administration hospital"),
	
	/**
	 * Hospital-based outpatient urology clinic
	 */
	_Hospitalbasedoutpatienturologyclinic ("50569004", "Hospital-based outpatient urology clinic"),
	
	/**
	 * Free-standing mental health clinic
	 */
	_Freestandingmentalhealthclinic ("51563005", "Free-standing mental health clinic"),
	
	/**
	 * Free-standing orthopaedics clinic
	 */
	_Freestandingorthopaedicsclinic ("52589007", "Free-standing orthopaedics clinic"),
	
	/**
	 * Hospital-based birthing centre
	 */
	_Hospitalbasedbirthingcentre ("52668009", "Hospital-based birthing centre"),
	
	/**
	 * Geriatric hospital
	 */
	_Geriatrichospital ("54372004", "Geriatric hospital"),
	
	/**
	 * Free-standing family medicine clinic
	 */
	_Freestandingfamilymedicineclinic ("55948006", "Free-standing family medicine clinic"),
	
	/**
	 * Intermediate care hospital
	 */
	_Intermediatecarehospital ("56109004", "Intermediate care hospital"),
	
	/**
	 * Hospital-based outpatient obstetrical clinic
	 */
	_Hospitalbasedoutpatientobstetricalclinic ("56189001", "Hospital-based outpatient obstetrical clinic"),
	
	/**
	 * Free-standing obstetrical clinic
	 */
	_Freestandingobstetricalclinic ("56217002", "Free-standing obstetrical clinic"),
	
	/**
	 * Hospital-based outpatient haematology clinic
	 */
	_Hospitalbasedoutpatienthaematologyclinic ("56293002", "Hospital-based outpatient haematology clinic"),
	
	/**
	 * Free-standing respiratory disease clinic
	 */
	_Freestandingrespiratorydiseaseclinic ("56775002", "Free-standing respiratory disease clinic"),
	
	/**
	 * Hospital-based outpatient respiratory disease clinic
	 */
	_Hospitalbasedoutpatientrespiratorydiseaseclinic ("57159002", "Hospital-based outpatient respiratory disease clinic"),
	
	/**
	 * Hospital-based outpatient gastroenterology clinic
	 */
	_Hospitalbasedoutpatientgastroenterologyclinic ("58482006", "Hospital-based outpatient gastroenterology clinic"),
	
	/**
	 * Travelers aid clinic
	 */
	_Travelersaidclinic ("59374000", "Traveler's aid clinic"),
	
	/**
	 * Free-standing otorhinolaryngology clinic
	 */
	_Freestandingotorhinolaryngologyclinic ("62264003", "Free-standing otorhinolaryngology clinic"),
	
	/**
	 * Psychiatric hospital
	 */
	_Psychiatrichospital ("62480006", "Psychiatric hospital"),
	
	/**
	 * Skilled nursing facility, level 3 care
	 */
	_Skillednursingfacilitylevel3care ("63997002", "Skilled nursing facility, level 3 care"),
	
	/**
	 * Private home-based care
	 */
	_Privatehomebasedcare ("66280005", "Private home-based care"),
	
	/**
	 * Free-standing clinic
	 */
	_Freestandingclinic ("67190003", "Free-standing clinic"),
	
	/**
	 * Hospital-based outpatient physical medicine clinic
	 */
	_Hospitalbasedoutpatientphysicalmedicineclinic ("67236001", "Hospital-based outpatient physical medicine clinic"),
	
	/**
	 * Free-standing infectious disease clinic
	 */
	_Freestandinginfectiousdiseaseclinic ("69135000", "Free-standing infectious disease clinic"),
	
	/**
	 * Hospital-based ambulatory surgery facility
	 */
	_Hospitalbasedambulatorysurgeryfacility ("69362002", "Hospital-based ambulatory surgery facility"),
	
	/**
	 * Free-standing rheumatology clinic
	 */
	_Freestandingrheumatologyclinic ("69859008", "Free-standing rheumatology clinic"),
	
	/**
	 * Inner city health centre
	 */
	_Innercityhealthcentre ("70547005", "Inner city health centre"),
	
	/**
	 * Health maintenance organisation
	 */
	_Healthmaintenanceorganisation ("72311000", "Health maintenance organisation"),
	
	/**
	 * Hospital-based laboratory facility
	 */
	_Hospitalbasedlaboratoryfacility ("73588009", "Hospital-based laboratory facility"),
	
	/**
	 * Hospital-based outpatient endocrinology clinic
	 */
	_Hospitalbasedoutpatientendocrinologyclinic ("73644007", "Hospital-based outpatient endocrinology clinic"),
	
	/**
	 * Hospital-based outpatient emergency care centre
	 */
	_Hospitalbasedoutpatientemergencycarecentre ("73770003", "Hospital-based outpatient emergency care centre"),
	
	/**
	 * Free-standing emergency care centre
	 */
	_Freestandingemergencycarecentre ("74018000", "Free-standing emergency care centre"),
	
	/**
	 * Orphanage
	 */
	_Orphanage ("74056004", "Orphanage"),
	
	/**
	 * Vocational rehabilitation centre
	 */
	_Vocationalrehabilitationcentre ("77500004", "Vocational rehabilitation centre"),
	
	/**
	 * Rural health centre
	 */
	_Ruralhealthcentre ("77931003", "Rural health centre"),
	
	/**
	 * Hospital-based outpatient orthopaedics clinic
	 */
	_Hospitalbasedoutpatientorthopaedicsclinic ("78001009", "Hospital-based outpatient orthopaedics clinic"),
	
	/**
	 * Hospital-based outpatient ophthalmology clinic
	 */
	_Hospitalbasedoutpatientophthalmologyclinic ("78088001", "Hospital-based outpatient ophthalmology clinic"),
	
	/**
	 * Hospital-based radiology facility
	 */
	_Hospitalbasedradiologyfacility ("79491001", "Hospital-based radiology facility"),
	
	/**
	 * Government hospital
	 */
	_Governmenthospital ("79993009", "Government hospital"),
	
	/**
	 * Rehabilitation hospital
	 */
	_Rehabilitationhospital ("80522000", "Rehabilitation hospital"),
	
	/**
	 * Walk-in clinic
	 */
	_Walkinclinic ("81234003", "Walk-in clinic"),
	
	/**
	 * Childrens hospital
	 */
	_Childrenshospital ("82242000", "Children's hospital"),
	
	/**
	 * Free-standing paediatric clinic
	 */
	_Freestandingpaediatricclinic ("82455001", "Free-standing paediatric clinic"),
	
	/**
	 * Solo practice private office
	 */
	_Solopracticeprivateoffice ("83891005", "Solo practice private office"),
	
	/**
	 * Free-standing endocrinology clinic
	 */
	_Freestandingendocrinologyclinic ("84643003", "Free-standing endocrinology clinic"),
	
	/**
	 * Hospital-based outpatient oncology clinic
	 */
	_Hospitalbasedoutpatientoncologyclinic ("89972002", "Hospital-based outpatient oncology clinic"),
	
	/**
	 * Hospital-based outpatient general surgery clinic
	 */
	_Hospitalbasedoutpatientgeneralsurgeryclinic ("90484001", "Hospital-based outpatient general surgery clinic"),
	
	/**
	 * Free-standing birthing centre
	 */
	_Freestandingbirthingcentre ("91154008", "Free-standing birthing centre"),
	
	/**
	 * Free-standing gynaecology clinic
	 */
	_Freestandinggynaecologyclinic ("91655008", "Free-standing gynaecology clinic"),
	
	/**
	 * Hospital ANDOR institution
	 */
	_HospitalANDORinstitution ("108343000", "Hospital AND/OR institution"),
	
	/**
	 * Nursing home ANDOR ambulatory care site
	 */
	_NursinghomeANDORambulatorycaresite ("108344006", "Nursing home AND/OR ambulatory care site"),
	
	/**
	 * Free-standing oncology clinic
	 */
	_Freestandingoncologyclinic ("113172002", "Free-standing oncology clinic"),
	
	/**
	 * Domiciliary
	 */
	_Domiciliary ("113173007", "Domiciliary"),
	
	/**
	 * Mountain resort sanatorium
	 */
	_Mountainresortsanatorium ("182718004", "Mountain resort sanatorium"),
	
	/**
	 * Outreach clinic
	 */
	_Outreachclinic ("185483006", "Outreach clinic"),
	
	/**
	 * Dermatology outreach clinic
	 */
	_Dermatologyoutreachclinic ("185484000", "Dermatology outreach clinic"),
	
	/**
	 * Orthopaedic outreach clinic
	 */
	_Orthopaedicoutreachclinic ("185485004", "Orthopaedic outreach clinic"),
	
	/**
	 * Ophthalmology outreach clinic
	 */
	_Ophthalmologyoutreachclinic ("185486003", "Ophthalmology outreach clinic"),
	
	/**
	 * Podiatry outreach clinic
	 */
	_Podiatryoutreachclinic ("185487007", "Podiatry outreach clinic"),
	
	/**
	 * Cardiovascular clinic
	 */
	_Cardiovascularclinic ("185920002", "Cardiovascular clinic"),
	
	/**
	 * Prison hospital
	 */
	_Prisonhospital ("224687002", "Prison hospital"),
	
	/**
	 * Location within general practice premises
	 */
	_Locationwithingeneralpracticepremises ("224880002", "Location within general practice premises"),
	
	/**
	 * Charitable nursing home
	 */
	_Charitablenursinghome ("224881003", "Charitable nursing home"),
	
	/**
	 * Cheshire nursing home
	 */
	_Cheshirenursinghome ("224882005", "Cheshire nursing home"),
	
	/**
	 * St Dunstans
	 */
	_StDunstans ("224883000", "St Dunstan's"),
	
	/**
	 * Location within hospital premises
	 */
	_Locationwithinhospitalpremises ("224884006", "Location within hospital premises"),
	
	/**
	 * Hospital consulting room
	 */
	_Hospitalconsultingroom ("224885007", "Hospital consulting room"),
	
	/**
	 * Treatment room
	 */
	_Treatmentroom ("224886008", "Treatment room"),
	
	/**
	 * Hospital waiting room
	 */
	_Hospitalwaitingroom ("224887004", "Hospital waiting room"),
	
	/**
	 * Health education room
	 */
	_Healtheducationroom ("224888009", "Health education room"),
	
	/**
	 * Toilet on hospital premises
	 */
	_Toiletonhospitalpremises ("224889001", "Toilet on hospital premises"),
	
	/**
	 * Hospital reception area
	 */
	_Hospitalreceptionarea ("224890005", "Hospital reception area"),
	
	/**
	 * Cottage hospital
	 */
	_Cottagehospital ("224924009", "Cottage hospital"),
	
	/**
	 * Medium secure unit
	 */
	_Mediumsecureunit ("224927002", "Medium secure unit"),
	
	/**
	 * Regional secure unit
	 */
	_Regionalsecureunit ("224928007", "Regional secure unit"),
	
	/**
	 * Secure hospital
	 */
	_Securehospital ("224929004", "Secure hospital"),
	
	/**
	 * Accident and Emergency department
	 */
	_AccidentandEmergencydepartment ("225728007", "Accident and Emergency department"),
	
	/**
	 * Bathing station
	 */
	_Bathingstation ("225729004", "Bathing station"),
	
	/**
	 * Community hospital
	 */
	_Communityhospital ("225732001", "Community hospital"),
	
	/**
	 * Holding room
	 */
	_Holdingroom ("225734000", "Holding room"),
	
	/**
	 * Mortuary
	 */
	_Mortuary ("225737007", "Mortuary"),
	
	/**
	 * Operating theatre
	 */
	_Operatingtheatre ("225738002", "Operating theatre"),
	
	/**
	 * Clients room
	 */
	_Clientsroom ("225739005", "Client's room"),
	
	/**
	 * Regional centre
	 */
	_Regionalcentre ("225740007", "Regional centre"),
	
	/**
	 * Resuscitation room
	 */
	_Resuscitationroom ("225741006", "Resuscitation room"),
	
	/**
	 * Seclusion room
	 */
	_Seclusionroom ("225743009", "Seclusion room"),
	
	/**
	 * Stripped room
	 */
	_Strippedroom ("225745002", "Stripped room"),
	
	/**
	 * Ward
	 */
	_Ward ("225746001", "Ward"),
	
	/**
	 * X-ray department
	 */
	_Xraydepartment ("225747005", "X-ray department"),
	
	/**
	 * Clinic
	 */
	_Clinic ("257585005", "Clinic"),
	
	/**
	 * Healthcare facility
	 */
	_Healthcarefacility ("257622000", "Healthcare facility"),
	
	/**
	 * General practice premises
	 */
	_Generalpracticepremises ("264358009", "General practice premises"),
	
	/**
	 * Health centre
	 */
	_Healthcentre ("264361005", "Health centre"),
	
	/**
	 * Pharmacy
	 */
	_Pharmacy ("264372000", "Pharmacy"),
	
	/**
	 * Special care unit
	 */
	_Specialcareunit ("274409007", "Special care unit"),
	
	/**
	 * Day hospital
	 */
	_Dayhospital ("274516006", "Day hospital"),
	
	/**
	 * Alcoholism detoxication centre
	 */
	_Alcoholismdetoxicationcentre ("274517002", "Alcoholism detoxication centre"),
	
	/**
	 * Elderly assessment clinic
	 */
	_Elderlyassessmentclinic ("275576008", "Elderly assessment clinic"),
	
	/**
	 * Wart clinic
	 */
	_Wartclinic ("275692008", "Wart clinic"),
	
	/**
	 * Dental surgery premises
	 */
	_Dentalsurgerypremises ("282086004", "Dental surgery premises"),
	
	/**
	 * Dental laboratory
	 */
	_Dentallaboratory ("282087008", "Dental laboratory"),
	
	/**
	 * Location within secure unit
	 */
	_Locationwithinsecureunit ("284455000", "Location within secure unit"),
	
	/**
	 * Hospital department
	 */
	_Hospitaldepartment ("284548004", "Hospital department"),
	
	/**
	 * Special hospital
	 */
	_Specialhospital ("288561005", "Special hospital"),
	
	/**
	 * Secure unit
	 */
	_Secureunit ("288562003", "Secure unit"),
	
	/**
	 * Medical centre
	 */
	_Medicalcentre ("288565001", "Medical centre"),
	
	/**
	 * General practice examination room
	 */
	_Generalpracticeexaminationroom ("288566000", "General practice examination room"),
	
	/**
	 * General practice treatment room
	 */
	_Generalpracticetreatmentroom ("288567009", "General practice treatment room"),
	
	/**
	 * General practice consulting room
	 */
	_Generalpracticeconsultingroom ("288568004", "General practice consulting room"),
	
	/**
	 * General practice reception area
	 */
	_Generalpracticereceptionarea ("288569007", "General practice reception area"),
	
	/**
	 * General practice waiting room
	 */
	_Generalpracticewaitingroom ("288570008", "General practice waiting room"),
	
	/**
	 * Toilet on general practice premises
	 */
	_Toiletongeneralpracticepremises ("288572000", "Toilet on general practice premises"),
	
	/**
	 * Community health care environment
	 */
	_Communityhealthcareenvironment ("288573005", "Community health care environment"),
	
	/**
	 * Private hospital
	 */
	_Privatehospital ("309895006", "Private hospital"),
	
	/**
	 * Tertiary referral hospital
	 */
	_Tertiaryreferralhospital ("309896007", "Tertiary referral hospital"),
	
	/**
	 * Psychiatric day care hospital
	 */
	_Psychiatricdaycarehospital ("309897003", "Psychiatric day care hospital"),
	
	/**
	 * Psychogeriatric day hospital
	 */
	_Psychogeriatricdayhospital ("309898008", "Psychogeriatric day hospital"),
	
	/**
	 * Elderly severely mentally ill day hospital
	 */
	_Elderlyseverelymentallyilldayhospital ("309899000", "Elderly severely mentally ill day hospital"),
	
	/**
	 * Care of the elderly day hospital
	 */
	_Careoftheelderlydayhospital ("309900005", "Care of the elderly day hospital"),
	
	/**
	 * Anaesthetic department
	 */
	_Anaestheticdepartment ("309901009", "Anaesthetic department"),
	
	/**
	 * Clinical oncology department
	 */
	_Clinicaloncologydepartment ("309902002", "Clinical oncology department"),
	
	/**
	 * Radiotherapy department
	 */
	_Radiotherapydepartment ("309903007", "Radiotherapy department"),
	
	/**
	 * Intensive care unit
	 */
	_Intensivecareunit ("309904001", "Intensive care unit"),
	
	/**
	 * Adult intensive care unit
	 */
	_Adultintensivecareunit ("309905000", "Adult intensive care unit"),
	
	/**
	 * Burns unit
	 */
	_Burnsunit ("309906004", "Burns unit"),
	
	/**
	 * Cardiac intensive care unit
	 */
	_Cardiacintensivecareunit ("309907008", "Cardiac intensive care unit"),
	
	/**
	 * Metabolic intensive care unit
	 */
	_Metabolicintensivecareunit ("309908003", "Metabolic intensive care unit"),
	
	/**
	 * Neurological intensive care unit
	 */
	_Neurologicalintensivecareunit ("309909006", "Neurological intensive care unit"),
	
	/**
	 * Paediatric intensive care unit
	 */
	_Paediatricintensivecareunit ("309910001", "Paediatric intensive care unit"),
	
	/**
	 * Respiratory intensive care unit
	 */
	_Respiratoryintensivecareunit ("309911002", "Respiratory intensive care unit"),
	
	/**
	 * Medical department
	 */
	_Medicaldepartment ("309912009", "Medical department"),
	
	/**
	 * Clinical allergy department
	 */
	_Clinicalallergydepartment ("309913004", "Clinical allergy department"),
	
	/**
	 * Audiology department
	 */
	_Audiologydepartment ("309914005", "Audiology department"),
	
	/**
	 * Cardiology department
	 */
	_Cardiologydepartment ("309915006", "Cardiology department"),
	
	/**
	 * Chest medicine department
	 */
	_Chestmedicinedepartment ("309916007", "Chest medicine department"),
	
	/**
	 * Thoracic medicine department
	 */
	_Thoracicmedicinedepartment ("309917003", "Thoracic medicine department"),
	
	/**
	 * Respiratory medicine department
	 */
	_Respiratorymedicinedepartment ("309918008", "Respiratory medicine department"),
	
	/**
	 * Clinical immunology department
	 */
	_Clinicalimmunologydepartment ("309919000", "Clinical immunology department"),
	
	/**
	 * Clinical neurophysiology department
	 */
	_Clinicalneurophysiologydepartment ("309920006", "Clinical neurophysiology department"),
	
	/**
	 * Clinical pharmacology department
	 */
	_Clinicalpharmacologydepartment ("309921005", "Clinical pharmacology department"),
	
	/**
	 * Clinical physiology department
	 */
	_Clinicalphysiologydepartment ("309922003", "Clinical physiology department"),
	
	/**
	 * Dermatology department
	 */
	_Dermatologydepartment ("309923008", "Dermatology department"),
	
	/**
	 * Diabetic department
	 */
	_Diabeticdepartment ("309924002", "Diabetic department"),
	
	/**
	 * Endocrinology department
	 */
	_Endocrinologydepartment ("309925001", "Endocrinology department"),
	
	/**
	 * Gastroenterology department
	 */
	_Gastroenterologydepartment ("309926000", "Gastroenterology department"),
	
	/**
	 * General medical department
	 */
	_Generalmedicaldepartment ("309927009", "General medical department"),
	
	/**
	 * Genetics department
	 */
	_Geneticsdepartment ("309928004", "Genetics department"),
	
	/**
	 * Clinical genetics department
	 */
	_Clinicalgeneticsdepartment ("309929007", "Clinical genetics department"),
	
	/**
	 * Clinical cytogenetics department
	 */
	_Clinicalcytogeneticsdepartment ("309930002", "Clinical cytogenetics department"),
	
	/**
	 * Clinical molecular genetics department
	 */
	_Clinicalmoleculargeneticsdepartment ("309931003", "Clinical molecular genetics department"),
	
	/**
	 * Genitourinary medicine department
	 */
	_Genitourinarymedicinedepartment ("309932005", "Genitourinary medicine department"),
	
	/**
	 * Care of the elderly department
	 */
	_Careoftheelderlydepartment ("309933000", "Care of the elderly department"),
	
	/**
	 * Infectious diseases department
	 */
	_Infectiousdiseasesdepartment ("309934006", "Infectious diseases department"),
	
	/**
	 * Medical ophthalmology department
	 */
	_Medicalophthalmologydepartment ("309935007", "Medical ophthalmology department"),
	
	/**
	 * Nephrology department
	 */
	_Nephrologydepartment ("309936008", "Nephrology department"),
	
	/**
	 * Neurology department
	 */
	_Neurologydepartment ("309937004", "Neurology department"),
	
	/**
	 * Nuclear medicine department
	 */
	_Nuclearmedicinedepartment ("309938009", "Nuclear medicine department"),
	
	/**
	 * Palliative care department
	 */
	_Palliativecaredepartment ("309939001", "Palliative care department"),
	
	/**
	 * Rehabilitation department
	 */
	_Rehabilitationdepartment ("309940004", "Rehabilitation department"),
	
	/**
	 * Rheumatology department
	 */
	_Rheumatologydepartment ("309941000", "Rheumatology department"),
	
	/**
	 * Obstetrics and gynaecology department
	 */
	_Obstetricsandgynaecologydepartment ("309942007", "Obstetrics and gynaecology department"),
	
	/**
	 * Gynaecology department
	 */
	_Gynaecologydepartment ("309943002", "Gynaecology department"),
	
	/**
	 * Obstetrics department
	 */
	_Obstetricsdepartment ("309944008", "Obstetrics department"),
	
	/**
	 * Paediatric department
	 */
	_Paediatricdepartment ("309945009", "Paediatric department"),
	
	/**
	 * Special care baby unit
	 */
	_Specialcarebabyunit ("309946005", "Special care baby unit"),
	
	/**
	 * Paediatric neurology department
	 */
	_Paediatricneurologydepartment ("309947001", "Paediatric neurology department"),
	
	/**
	 * Paediatric oncology department
	 */
	_Paediatriconcologydepartment ("309948006", "Paediatric oncology department"),
	
	/**
	 * Pain management department
	 */
	_Painmanagementdepartment ("309949003", "Pain management department"),
	
	/**
	 * Pathology department
	 */
	_Pathologydepartment ("309950003", "Pathology department"),
	
	/**
	 * Blood transfusion department
	 */
	_Bloodtransfusiondepartment ("309951004", "Blood transfusion department"),
	
	/**
	 * Chemical pathology department
	 */
	_Chemicalpathologydepartment ("309952006", "Chemical pathology department"),
	
	/**
	 * General pathology department
	 */
	_Generalpathologydepartment ("309953001", "General pathology department"),
	
	/**
	 * Haematology department
	 */
	_Haematologydepartment ("309954007", "Haematology department"),
	
	/**
	 * Medical microbiology department
	 */
	_Medicalmicrobiologydepartment ("309956009", "Medical microbiology department"),
	
	/**
	 * Neuropathology department
	 */
	_Neuropathologydepartment ("309957000", "Neuropathology department"),
	
	/**
	 * Psychiatry department
	 */
	_Psychiatrydepartment ("309958005", "Psychiatry department"),
	
	/**
	 * Child and adolescent psychiatry department
	 */
	_Childandadolescentpsychiatrydepartment ("309959002", "Child and adolescent psychiatry department"),
	
	/**
	 * Forensic psychiatry department
	 */
	_Forensicpsychiatrydepartment ("309960007", "Forensic psychiatry department"),
	
	/**
	 * Psychogeriatric department
	 */
	_Psychogeriatricdepartment ("309961006", "Psychogeriatric department"),
	
	/**
	 * Mental handicap psychiatry department
	 */
	_Mentalhandicappsychiatrydepartment ("309962004", "Mental handicap psychiatry department"),
	
	/**
	 * Rehabilitation psychiatry department
	 */
	_Rehabilitationpsychiatrydepartment ("309963009", "Rehabilitation psychiatry department"),
	
	/**
	 * Radiology department
	 */
	_Radiologydepartment ("309964003", "Radiology department"),
	
	/**
	 * Occupational health department
	 */
	_Occupationalhealthdepartment ("309965002", "Occupational health department"),
	
	/**
	 * Stroke unit
	 */
	_Strokeunit ("309966001", "Stroke unit"),
	
	/**
	 * Surgical department
	 */
	_Surgicaldepartment ("309967005", "Surgical department"),
	
	/**
	 * Breast surgery department
	 */
	_Breastsurgerydepartment ("309968000", "Breast surgery department"),
	
	/**
	 * Cardiothoracic surgery department
	 */
	_Cardiothoracicsurgerydepartment ("309969008", "Cardiothoracic surgery department"),
	
	/**
	 * Thoracic surgery department
	 */
	_Thoracicsurgerydepartment ("309970009", "Thoracic surgery department"),
	
	/**
	 * Cardiac surgery department
	 */
	_Cardiacsurgerydepartment ("309971008", "Cardiac surgery department"),
	
	/**
	 * Dental surgery department
	 */
	_Dentalsurgerydepartment ("309972001", "Dental surgery department"),
	
	/**
	 * General dental surgery department
	 */
	_Generaldentalsurgerydepartment ("309973006", "General dental surgery department"),
	
	/**
	 * Oral surgery department
	 */
	_Oralsurgerydepartment ("309974000", "Oral surgery department"),
	
	/**
	 * Orthodontics department
	 */
	_Orthodonticsdepartment ("309975004", "Orthodontics department"),
	
	/**
	 * Paediatric dentistry department
	 */
	_Paediatricdentistrydepartment ("309976003", "Paediatric dentistry department"),
	
	/**
	 * Restorative dentistry department
	 */
	_Restorativedentistrydepartment ("309977007", "Restorative dentistry department"),
	
	/**
	 * Ear, nose and throat department
	 */
	_Earnoseandthroatdepartment ("309978002", "Ear, nose and throat department"),
	
	/**
	 * Endocrine surgery department
	 */
	_Endocrinesurgerydepartment ("309979005", "Endocrine surgery department"),
	
	/**
	 * Gastrointestinal surgery department
	 */
	_Gastrointestinalsurgerydepartment ("309980008", "Gastrointestinal surgery department"),
	
	/**
	 * General gastrointestinal surgery department
	 */
	_Generalgastrointestinalsurgerydepartment ("309981007", "General gastrointestinal surgery department"),
	
	/**
	 * Upper gastrointestinal surgery department
	 */
	_Uppergastrointestinalsurgerydepartment ("309982000", "Upper gastrointestinal surgery department"),
	
	/**
	 * Colorectal surgery department
	 */
	_Colorectalsurgerydepartment ("309983005", "Colorectal surgery department"),
	
	/**
	 * General surgical department
	 */
	_Generalsurgicaldepartment ("309984004", "General surgical department"),
	
	/**
	 * Hand surgery department
	 */
	_Handsurgerydepartment ("309985003", "Hand surgery department"),
	
	/**
	 * Hepatobiliary surgical department
	 */
	_Hepatobiliarysurgicaldepartment ("309986002", "Hepatobiliary surgical department"),
	
	/**
	 * Neurosurgical department
	 */
	_Neurosurgicaldepartment ("309987006", "Neurosurgical department"),
	
	/**
	 * Ophthalmology department
	 */
	_Ophthalmologydepartment ("309988001", "Ophthalmology department"),
	
	/**
	 * Orthopaedic department
	 */
	_Orthopaedicdepartment ("309989009", "Orthopaedic department"),
	
	/**
	 * Pancreatic surgery department
	 */
	_Pancreaticsurgerydepartment ("309990000", "Pancreatic surgery department"),
	
	/**
	 * Paediatric surgical department
	 */
	_Paediatricsurgicaldepartment ("309991001", "Paediatric surgical department"),
	
	/**
	 * Plastic surgery department
	 */
	_Plasticsurgerydepartment ("309992008", "Plastic surgery department"),
	
	/**
	 * Surgical transplant department
	 */
	_Surgicaltransplantdepartment ("309993003", "Surgical transplant department"),
	
	/**
	 * Trauma surgery department
	 */
	_Traumasurgerydepartment ("309994009", "Trauma surgery department"),
	
	/**
	 * Urology department
	 */
	_Urologydepartment ("309995005", "Urology department"),
	
	/**
	 * Vascular surgery department
	 */
	_Vascularsurgerydepartment ("309996006", "Vascular surgery department"),
	
	/**
	 * Young disabled unit
	 */
	_Youngdisabledunit ("309997002", "Young disabled unit"),
	
	/**
	 * Day ward
	 */
	_Dayward ("309998007", "Day ward"),
	
	/**
	 * Paediatric Accident and Emergency department
	 */
	_PaediatricAccidentandEmergencydepartment ("310203004", "Paediatric Accident and Emergency department"),
	
	/**
	 * Ophthalmology Accident and Emergency department
	 */
	_OphthalmologyAccidentandEmergencydepartment ("310204005", "Ophthalmology Accident and Emergency department"),
	
	/**
	 * Private nursing home
	 */
	_Privatenursinghome ("310206007", "Private nursing home"),
	
	/**
	 * Hospital clinic
	 */
	_Hospitalclinic ("310390009", "Hospital clinic"),
	
	/**
	 * Community clinic
	 */
	_Communityclinic ("310391008", "Community clinic"),
	
	/**
	 * Physiotherapy department
	 */
	_Physiotherapydepartment ("310464005", "Physiotherapy department"),
	
	/**
	 * Hospital-based outpatient allergy clinic
	 */
	_Hospitalbasedoutpatientallergyclinic ("360957003", "Hospital-based outpatient allergy clinic"),
	
	/**
	 * Hospital-based outpatient immunology clinic
	 */
	_Hospitalbasedoutpatientimmunologyclinic ("360966004", "Hospital-based outpatient immunology clinic"),
	
	/**
	 * Maternity clinic
	 */
	_Maternityclinic ("394573001", "Maternity clinic"),
	
	/**
	 * Antenatal clinic
	 */
	_Antenatalclinic ("394574007", "Antenatal clinic"),
	
	/**
	 * Postnatal clinic
	 */
	_Postnatalclinic ("394575008", "Postnatal clinic"),
	
	/**
	 * Primary Care Trust site
	 */
	_PrimaryCareTrustsite ("394750006", "Primary Care Trust site"),
	
	/**
	 * Independent provider site
	 */
	_Independentprovidersite ("394759007", "Independent provider site"),
	
	/**
	 * GP practice site
	 */
	_GPpracticesite ("394761003", "GP practice site"),
	
	/**
	 * Health encounter sites
	 */
	_Healthencountersites ("394777002", "Health encounter sites"),
	
	/**
	 * Clients or patients home
	 */
	_Clientsorpatientshome ("394778007", "Client's or patient's home"),
	
	/**
	 * Group home managed by local authority
	 */
	_Grouphomemanagedbylocalauthority ("394783004", "Group home managed by local authority"),
	
	/**
	 * Group home managed by voluntary or private agents
	 */
	_Grouphomemanagedbyvoluntaryorprivateagents ("394784005", "Group home managed by voluntary or private agents"),
	
	/**
	 * Day centre managed by local authority
	 */
	_Daycentremanagedbylocalauthority ("394789000", "Day centre managed by local authority"),
	
	/**
	 * Day centre managed by voluntary or private agents
	 */
	_Daycentremanagedbyvoluntaryorprivateagents ("394790009", "Day centre managed by voluntary or private agents"),
	
	/**
	 * Health clinic managed by voluntary or private agents
	 */
	_Healthclinicmanagedbyvoluntaryorprivateagents ("394794000", "Health clinic managed by voluntary or private agents"),
	
	/**
	 * Resource centre managed by local authority
	 */
	_Resourcecentremanagedbylocalauthority ("394797007", "Resource centre managed by local authority"),
	
	/**
	 * Resource centre managed by voluntary or private agents
	 */
	_Resourcecentremanagedbyvoluntaryorprivateagents ("394798002", "Resource centre managed by voluntary or private agents"),
	
	/**
	 * Joint consultant clinic
	 */
	_Jointconsultantclinic ("394865001", "Joint consultant clinic"),
	
	/**
	 * Other location within hospital premises
	 */
	_Otherlocationwithinhospitalpremises ("397784001", "Other location within hospital premises"),
	
	/**
	 * Preoperative screening clinic
	 */
	_Preoperativescreeningclinic ("397872006", "Preoperative screening clinic"),
	
	/**
	 * Preoperative holding area
	 */
	_Preoperativeholdingarea ("397983003", "Preoperative holding area"),
	
	/**
	 * Unspecified location within hospital premises
	 */
	_Unspecifiedlocationwithinhospitalpremises ("398145002", "Unspecified location within hospital premises"),
	
	/**
	 * Medical or surgical floor
	 */
	_Medicalorsurgicalfloor ("398156002", "Medical or surgical floor"),
	
	/**
	 * Postoperative anaesthesia care unit
	 */
	_Postoperativeanaesthesiacareunit ("398161000", "Postoperative anaesthesia care unit"),
	
	/**
	 * Psychiatric intensive care unit
	 */
	_Psychiatricintensivecareunit ("404821007", "Psychiatric intensive care unit"),
	
	/**
	 * Neonatal intensive care unit
	 */
	_Neonatalintensivecareunit ("405269005", "Neonatal intensive care unit"),
	
	/**
	 * Labour and delivery unit
	 */
	_Labouranddeliveryunit ("405606005", "Labour and delivery unit"),
	
	/**
	 * Ambulatory surgery centre
	 */
	_Ambulatorysurgerycentre ("405607001", "Ambulatory surgery centre"),
	
	/**
	 * Academic medical centre
	 */
	_Academicmedicalcentre ("405608006", "Academic medical centre"),
	
	/**
	 * Mass casualty setting
	 */
	_Masscasualtysetting ("409518000", "Mass casualty setting"),
	
	/**
	 * Contained casualty setting
	 */
	_Containedcasualtysetting ("409519008", "Contained casualty setting"),
	
	/**
	 * Hospital decontamination room
	 */
	_Hospitaldecontaminationroom ("409685000", "Hospital decontamination room"),
	
	/**
	 * Hospital isolation room
	 */
	_Hospitalisolationroom ("409688003", "Hospital isolation room"),
	
	/**
	 * Adult day care centre
	 */
	_Adultdaycarecentre ("413456002", "Adult day care centre"),
	
	/**
	 * Child day care centre
	 */
	_Childdaycarecentre ("413817003", "Child day care centre"),
	
	/**
	 * Delivery suite
	 */
	_Deliverysuite ("413964002", "Delivery suite"),
	
	/**
	 * Induction room
	 */
	_Inductionroom ("414485004", "Induction room"),
	
	/**
	 * PACU Phase 1
	 */
	_PACUPhase1 ("415142008", "PACU Phase 1"),
	
	/**
	 * PACU Phase 2
	 */
	_PACUPhase2 ("415143003", "PACU Phase 2"),
	
	/**
	 * Community medical centre
	 */
	_Communitymedicalcentre ("416957006", "Community medical centre"),
	
	/**
	 * Surgical stepdown unit
	 */
	_Surgicalstepdownunit ("418028002", "Surgical stepdown unit"),
	
	/**
	 * Surgical intensive care unit
	 */
	_Surgicalintensivecareunit ("418433008", "Surgical intensive care unit"),
	
	/**
	 * Dialysis unit
	 */
	_Dialysisunit ("418518002", "Dialysis unit"),
	
	/**
	 * Cardiac stepdown unit
	 */
	_Cardiacstepdownunit ("418649002", "Cardiac stepdown unit"),
	
	/**
	 * Medical stepdown unit
	 */
	_Medicalstepdownunit ("419390002", "Medical stepdown unit"),
	
	/**
	 * Stepdown unit
	 */
	_Stepdownunit ("419590001", "Stepdown unit"),
	
	/**
	 * Paediatric medicine department
	 */
	_Paediatricmedicinedepartment ("420223003", "Paediatric medicine department"),
	
	/**
	 * Obstetrical stepdown unit
	 */
	_Obstetricalstepdownunit ("420280003", "Obstetrical stepdown unit"),
	
	/**
	 * Telemetry unit
	 */
	_Telemetryunit ("422798006", "Telemetry unit"),
	
	/**
	 * Burns intensive care unit
	 */
	_Burnsintensivecareunit ("426439001", "Burns intensive care unit"),
	
	/**
	 * Newborn nursery unit
	 */
	_Newbornnurseryunit ("427695007", "Newborn nursery unit"),
	
	/**
	 * Genitourinary medicine clinic
	 */
	_Genitourinarymedicineclinic ("431654006", "Genitourinary medicine clinic"),
	
	/**
	 * Primary care department
	 */
	_Primarycaredepartment ("441480003", "Primary care department"),
	
	/**
	 * Tropical medicine department
	 */
	_Tropicalmedicinedepartment ("441548002", "Tropical medicine department"),
	
	/**
	 * Diagnostic imaging department
	 */
	_Diagnosticimagingdepartment ("441662001", "Diagnostic imaging department"),
	
	/**
	 * Histopathology department
	 */
	_Histopathologydepartment ("441950002", "Histopathology department"),
	
	/**
	 * Medical intensive care unit
	 */
	_Medicalintensivecareunit ("441994008", "Medical intensive care unit"),
	
	/**
	 * Sleep apnoea clinic
	 */
	_Sleepapnoeaclinic ("443621004", "Sleep apnoea clinic"),
	
	/**
	 * Nonacute hospital
	 */
	_Nonacutehospital ("443750004", "Nonacute hospital"),
	
	/**
	 * Colposcopy clinic
	 */
	_Colposcopyclinic ("444766008", "Colposcopy clinic"),
	
	/**
	 * Dental hospital
	 */
	_Dentalhospital ("448399001", "Dental hospital"),
	
	/**
	 * Diabetes clinic
	 */
	_Diabetesclinic ("702706001", "Diabetes clinic"),
	
	/**
	 * Acute pain clinic
	 */
	_Acutepainclinic ("702812003", "Acute pain clinic"),
	
	/**
	 * Acquired immune deficiency syndrome clinic
	 */
	_Acquiredimmunedeficiencysyndromeclinic ("702813008", "Acquired immune deficiency syndrome clinic"),
	
	/**
	 * Allergy clinic
	 */
	_Allergyclinic ("702814002", "Allergy clinic"),
	
	/**
	 * After hours clinic
	 */
	_Afterhoursclinic ("702819007", "After hours clinic"),
	
	/**
	 * Amputee clinic
	 */
	_Amputeeclinic ("702821002", "Amputee clinic"),
	
	/**
	 * Anticoagulation clinic
	 */
	_Anticoagulationclinic ("702822009", "Anticoagulation clinic"),
	
	/**
	 * Asthma clinic
	 */
	_Asthmaclinic ("702823004", "Asthma clinic"),
	
	/**
	 * Audiology clinic
	 */
	_Audiologyclinic ("702824005", "Audiology clinic"),
	
	/**
	 * Autism clinic
	 */
	_Autismclinic ("702825006", "Autism clinic"),
	
	/**
	 * Bipolar clinic
	 */
	_Bipolarclinic ("702826007", "Bipolar clinic"),
	
	/**
	 * Bone marrow transplant clinic
	 */
	_Bonemarrowtransplantclinic ("702827003", "Bone marrow transplant clinic"),
	
	/**
	 * Breast clinic
	 */
	_Breastclinic ("702828008", "Breast clinic"),
	
	/**
	 * Cardiology clinic
	 */
	_Cardiologyclinic ("702830005", "Cardiology clinic"),
	
	/**
	 * Child and adolescent medicine clinic
	 */
	_Childandadolescentmedicineclinic ("702831009", "Child and adolescent medicine clinic"),
	
	/**
	 * Child and adolescent neurology clinic
	 */
	_Childandadolescentneurologyclinic ("702832002", "Child and adolescent neurology clinic"),
	
	/**
	 * Child and adolescent psychiatry clinic
	 */
	_Childandadolescentpsychiatryclinic ("702833007", "Child and adolescent psychiatry clinic"),
	
	/**
	 * Child speech and language therapy clinic
	 */
	_Childspeechandlanguagetherapyclinic ("702834001", "Child speech and language therapy clinic"),
	
	/**
	 * Chronic obstructive pulmonary disease clinic
	 */
	_Chronicobstructivepulmonarydiseaseclinic ("702839006", "Chronic obstructive pulmonary disease clinic"),
	
	/**
	 * Chronic pain clinic
	 */
	_Chronicpainclinic ("702840008", "Chronic pain clinic"),
	
	/**
	 * Cleft palate clinic
	 */
	_Cleftpalateclinic ("702841007", "Cleft palate clinic"),
	
	/**
	 * Congenital heart disease clinic
	 */
	_Congenitalheartdiseaseclinic ("702842000", "Congenital heart disease clinic"),
	
	/**
	 * Congestive heart failure clinic
	 */
	_Congestiveheartfailureclinic ("702843005", "Congestive heart failure clinic"),
	
	/**
	 * Cystic fibrosis clinic
	 */
	_Cysticfibrosisclinic ("702844004", "Cystic fibrosis clinic"),
	
	/**
	 * Deep vein thrombosis clinic
	 */
	_Deepveinthrombosisclinic ("702845003", "Deep vein thrombosis clinic"),
	
	/**
	 * Dental clinic
	 */
	_Dentalclinic ("702846002", "Dental clinic"),
	
	/**
	 * Dermatology clinic
	 */
	_Dermatologyclinic ("702847006", "Dermatology clinic"),
	
	/**
	 * Diabetes foot care clinic
	 */
	_Diabetesfootcareclinic ("702848001", "Diabetes foot care clinic"),
	
	/**
	 * Diabetes in pregnancy clinic
	 */
	_Diabetesinpregnancyclinic ("702849009", "Diabetes in pregnancy clinic"),
	
	/**
	 * Diabetic retinopathy clinic
	 */
	_Diabeticretinopathyclinic ("702850009", "Diabetic retinopathy clinic"),
	
	/**
	 * Down syndrome clinic
	 */
	_Downsyndromeclinic ("702851008", "Down syndrome clinic"),
	
	/**
	 * Ear, nose and throat clinic
	 */
	_Earnoseandthroatclinic ("702852001", "Ear, nose and throat clinic"),
	
	/**
	 * Eating disorder clinic
	 */
	_Eatingdisorderclinic ("702853006", "Eating disorder clinic"),
	
	/**
	 * Endocrinology clinic
	 */
	_Endocrinologyclinic ("702854000", "Endocrinology clinic"),
	
	/**
	 * Family medicine clinic
	 */
	_Familymedicineclinic ("702855004", "Family medicine clinic"),
	
	/**
	 * Family planning clinic
	 */
	_Familyplanningclinic ("702856003", "Family planning clinic"),
	
	/**
	 * Fetal alcohol clinic
	 */
	_Fetalalcoholclinic ("702857007", "Fetal alcohol clinic"),
	
	/**
	 * Fracture clinic
	 */
	_Fractureclinic ("702858002", "Fracture clinic"),
	
	/**
	 * Gastroenterology clinic
	 */
	_Gastroenterologyclinic ("702859005", "Gastroenterology clinic"),
	
	/**
	 * General surgery clinic
	 */
	_Generalsurgeryclinic ("702860000", "General surgery clinic"),
	
	/**
	 * Genetics clinic
	 */
	_Geneticsclinic ("702861001", "Genetics clinic"),
	
	/**
	 * Gynaecology clinic
	 */
	_Gynaecologyclinic ("702862008", "Gynaecology clinic"),
	
	/**
	 * Hand clinic
	 */
	_Handclinic ("702863003", "Hand clinic"),
	
	/**
	 * Headache clinic
	 */
	_Headacheclinic ("702864009", "Headache clinic"),
	
	/**
	 * Haematology clinic
	 */
	_Haematologyclinic ("702865005", "Haematology clinic"),
	
	/**
	 * Haemophilia clinic
	 */
	_Haemophiliaclinic ("702866006", "Haemophilia clinic"),
	
	/**
	 * Hypertension clinic
	 */
	_Hypertensionclinic ("702867002", "Hypertension clinic"),
	
	/**
	 * Immunology clinic
	 */
	_Immunologyclinic ("702868007", "Immunology clinic"),
	
	/**
	 * Infectious disease clinic
	 */
	_Infectiousdiseaseclinic ("702870003", "Infectious disease clinic"),
	
	/**
	 * Infertility clinic
	 */
	_Infertilityclinic ("702871004", "Infertility clinic"),
	
	/**
	 * Internal medicine clinic
	 */
	_Internalmedicineclinic ("702877000", "Internal medicine clinic"),
	
	/**
	 * Land ambulance
	 */
	_Landambulance ("702878005", "Land ambulance"),
	
	/**
	 * Laser eye clinic
	 */
	_Lasereyeclinic ("702879002", "Laser eye clinic"),
	
	/**
	 * Lymphoedema clinic
	 */
	_Lymphoedemaclinic ("702880004", "Lymphoedema clinic"),
	
	/**
	 * Medical day care
	 */
	_Medicaldaycare ("702881000", "Medical day care"),
	
	/**
	 * Mental health clinic
	 */
	_Mentalhealthclinic ("702882007", "Mental health clinic"),
	
	/**
	 * Mood or anxiety disorder clinic
	 */
	_Moodoranxietydisorderclinic ("702883002", "Mood or anxiety disorder clinic"),
	
	/**
	 * Movement disorder clinic
	 */
	_Movementdisorderclinic ("702884008", "Movement disorder clinic"),
	
	/**
	 * Multiple sclerosis clinic
	 */
	_Multiplesclerosisclinic ("702885009", "Multiple sclerosis clinic"),
	
	/**
	 * Nephrology clinic
	 */
	_Nephrologyclinic ("702886005", "Nephrology clinic"),
	
	/**
	 * Neurology clinic
	 */
	_Neurologyclinic ("702887001", "Neurology clinic"),
	
	/**
	 * Neuromuscular clinic
	 */
	_Neuromuscularclinic ("702888006", "Neuromuscular clinic"),
	
	/**
	 * Neurophysiology clinic
	 */
	_Neurophysiologyclinic ("702889003", "Neurophysiology clinic"),
	
	/**
	 * Neuropsychology clinic
	 */
	_Neuropsychologyclinic ("702890007", "Neuropsychology clinic"),
	
	/**
	 * Neurosurgery clinic
	 */
	_Neurosurgeryclinic ("702891006", "Neurosurgery clinic"),
	
	/**
	 * Obstetrics and gynaecology clinic
	 */
	_Obstetricsandgynaecologyclinic ("702892004", "Obstetrics and gynaecology clinic"),
	
	/**
	 * Occupational therapy clinic
	 */
	_Occupationaltherapyclinic ("702893009", "Occupational therapy clinic"),
	
	/**
	 * Ophthalmology clinic
	 */
	_Ophthalmologyclinic ("702894003", "Ophthalmology clinic"),
	
	/**
	 * Osteoporosis clinic
	 */
	_Osteoporosisclinic ("702895002", "Osteoporosis clinic"),
	
	/**
	 * Pacemaker clinic
	 */
	_Pacemakerclinic ("702896001", "Pacemaker clinic"),
	
	/**
	 * Pain clinic
	 */
	_Painclinic ("702897005", "Pain clinic"),
	
	/**
	 * Parkinsons disease clinic
	 */
	_Parkinsonsdiseaseclinic ("702898000", "Parkinson's disease clinic"),
	
	/**
	 * Paediatric cardiology clinic
	 */
	_Paediatriccardiologyclinic ("702899008", "Paediatric cardiology clinic"),
	
	/**
	 * Paediatric clinic
	 */
	_Paediatricclinic ("702900003", "Paediatric clinic"),
	
	/**
	 * Paediatric dermatology clinic
	 */
	_Paediatricdermatologyclinic ("702901004", "Paediatric dermatology clinic"),
	
	/**
	 * Paediatric ear, nose and throat clinic
	 */
	_Paediatricearnoseandthroatclinic ("702902006", "Paediatric ear, nose and throat clinic"),
	
	/**
	 * Paediatric endocrinology clinic
	 */
	_Paediatricendocrinologyclinic ("702903001", "Paediatric endocrinology clinic"),
	
	/**
	 * Paediatric gastroenterology clinic
	 */
	_Paediatricgastroenterologyclinic ("702904007", "Paediatric gastroenterology clinic"),
	
	/**
	 * Paediatric haematology clinic
	 */
	_Paediatrichaematologyclinic ("702905008", "Paediatric haematology clinic"),
	
	/**
	 * Paediatric infectious disease clinic
	 */
	_Paediatricinfectiousdiseaseclinic ("702906009", "Paediatric infectious disease clinic"),
	
	/**
	 * Paediatric rheumatology clinic
	 */
	_Paediatricrheumatologyclinic ("702907000", "Paediatric rheumatology clinic"),
	
	/**
	 * Paediatric urology clinic
	 */
	_Paediatricurologyclinic ("702908005", "Paediatric urology clinic"),
	
	/**
	 * Paediatric oncology clinic
	 */
	_Paediatriconcologyclinic ("702909002", "Paediatric oncology clinic"),
	
	/**
	 * Pelvic floor clinic
	 */
	_Pelvicfloorclinic ("702910007", "Pelvic floor clinic"),
	
	/**
	 * Podiatry clinic
	 */
	_Podiatryclinic ("702911006", "Podiatry clinic"),
	
	/**
	 * Preventive medicine clinic
	 */
	_Preventivemedicineclinic ("702912004", "Preventive medicine clinic"),
	
	/**
	 * Proctology clinic
	 */
	_Proctologyclinic ("702913009", "Proctology clinic"),
	
	/**
	 * Psychiatry clinic
	 */
	_Psychiatryclinic ("702914003", "Psychiatry clinic"),
	
	/**
	 * Psychology clinic
	 */
	_Psychologyclinic ("702915002", "Psychology clinic"),
	
	/**
	 * Rehabilitation clinic
	 */
	_Rehabilitationclinic ("702916001", "Rehabilitation clinic"),
	
	/**
	 * Respiratory disease clinic
	 */
	_Respiratorydiseaseclinic ("702917005", "Respiratory disease clinic"),
	
	/**
	 * Rheumatology clinic
	 */
	_Rheumatologyclinic ("702918000", "Rheumatology clinic"),
	
	/**
	 * Schizophrenia clinic
	 */
	_Schizophreniaclinic ("702919008", "Schizophrenia clinic"),
	
	/**
	 * Seizure clinic
	 */
	_Seizureclinic ("702920002", "Seizure clinic"),
	
	/**
	 * Sexual health clinic
	 */
	_Sexualhealthclinic ("702921003", "Sexual health clinic"),
	
	/**
	 * Speech and language therapy clinic
	 */
	_Speechandlanguagetherapyclinic ("702922005", "Speech and language therapy clinic"),
	
	/**
	 * Sports medicine clinic
	 */
	_Sportsmedicineclinic ("702923000", "Sports medicine clinic"),
	
	/**
	 * Stroke clinic
	 */
	_Strokeclinic ("702924006", "Stroke clinic"),
	
	/**
	 * Swallow clinic
	 */
	_Swallowclinic ("702925007", "Swallow clinic"),
	
	/**
	 * Tourette syndrome clinic
	 */
	_Tourettesyndromeclinic ("702926008", "Tourette syndrome clinic"),
	
	/**
	 * Urgent care clinic
	 */
	_Urgentcareclinic ("702927004", "Urgent care clinic"),
	
	/**
	 * Urology clinic
	 */
	_Urologyclinic ("702928009", "Urology clinic"),
	
	/**
	 * Vertigo clinic
	 */
	_Vertigoclinic ("702929001", "Vertigo clinic"),
	
	/**
	 * Plastic surgery clinic
	 */
	_Plasticsurgeryclinic ("702930006", "Plastic surgery clinic"),
	
	/**
	 * Oncology clinic
	 */
	_Oncologyclinic ("702931005", "Oncology clinic"),
	
	/**
	 * Oral and maxillofacial surgery clinic
	 */
	_Oralandmaxillofacialsurgeryclinic ("702932003", "Oral and maxillofacial surgery clinic"),
	
	/**
	 * Orthopaedic clinic
	 */
	_Orthopaedicclinic ("702933008", "Orthopaedic clinic"),
	
	/**
	 * Paediatric surgery clinic
	 */
	_Paediatricsurgeryclinic ("702934002", "Paediatric surgery clinic"),
	
	/**
	 * Physiotherapy clinic
	 */
	_Physiotherapyclinic ("702935001", "Physiotherapy clinic"),
	
	/**
	 * Nurse practitioner clinic
	 */
	_Nursepractitionerclinic ("703069008", "Nurse practitioner clinic"),
	
	/**
	 * Gynaecology outreach clinic
	 */
	_Gynaecologyoutreachclinic ("198931000000104", "Gynaecology outreach clinic"),
	
	/**
	 * General surgery outreach clinic
	 */
	_Generalsurgeryoutreachclinic ("198941000000108", "General surgery outreach clinic"),
	
	/**
	 * Psychiatric outreach clinic
	 */
	_Psychiatricoutreachclinic ("198951000000106", "Psychiatric outreach clinic"),
	
	/**
	 * General medicine outreach clinic
	 */
	_Generalmedicineoutreachclinic ("198961000000109", "General medicine outreach clinic"),
	
	/**
	 * Asthma outreach clinic
	 */
	_Asthmaoutreachclinic ("198971000000102", "Asthma outreach clinic"),
	
	/**
	 * Diabetic antenatal clinic
	 */
	_Diabeticantenatalclinic ("203951000000101", "Diabetic antenatal clinic"),
	
	/**
	 * Ambulance service care setting
	 */
	_Ambulanceservicecaresetting ("312351000000101", "Ambulance service care setting"),
	
	/**
	 * Primary care out of hours service care setting
	 */
	_Primarycareoutofhoursservicecaresetting ("312361000000103", "Primary care out of hours service care setting"),
	
	/**
	 * Social care setting
	 */
	_Socialcaresetting ("312371000000105", "Social care setting"),
	
	/**
	 * NHS Direct Contact Centre
	 */
	_NHSDirectContactCentre ("312391000000109", "NHS Direct Contact Centre"),
	
	/**
	 * Urgent care centre
	 */
	_Urgentcarecentre ("312401000000107", "Urgent care centre"),
	
	/**
	 * Minor injuries unit
	 */
	_Minorinjuriesunit ("312411000000109", "Minor injuries unit"),
	
	/**
	 * Community mental health care setting
	 */
	_Communitymentalhealthcaresetting ("312471000000104", "Community mental health care setting"),
	
	/**
	 * Out of hours service care setting
	 */
	_Outofhoursservicecaresetting ("313161000000107", "Out of hours service care setting"),
	
	/**
	 * Specialist treatment area
	 */
	_Specialisttreatmentarea ("506341000000101", "Specialist treatment area"),
	
	/**
	 * Group home managed by NHS
	 */
	_GrouphomemanagedbyNHS ("930711000000104", "Group home managed by NHS"),
	
	/**
	 * NHS day care facility on NHS hospital site
	 */
	_NHSdaycarefacilityonNHShospitalsite ("930721000000105", "NHS day care facility on NHS hospital site"),
	
	/**
	 * NHS day care facility on other site
	 */
	_NHSdaycarefacilityonothersite ("930731000000107", "NHS day care facility on other site"),
	
	/**
	 * NHS consultant clinic premises on NHS hospital site
	 */
	_NHSconsultantclinicpremisesonNHShospitalsite ("930741000000103", "NHS consultant clinic premises on NHS hospital site"),
	
	/**
	 * NHS consultant clinic premises off NHS hospital site
	 */
	_NHSconsultantclinicpremisesoffNHShospitalsite ("930751000000100", "NHS consultant clinic premises off NHS hospital site"),
	
	/**
	 * Health clinic managed by NHS
	 */
	_HealthclinicmanagedbyNHS ("930761000000102", "Health clinic managed by NHS"),
	
	/**
	 * Resource centre on NHS hospital site
	 */
	_ResourcecentreonNHShospitalsite ("930771000000109", "Resource centre on NHS hospital site"),
	
	/**
	 * Resource centre managed by the NHS off NHS hospital site
	 */
	_ResourcecentremanagedbytheNHSoffNHShospitalsite ("930781000000106", "Resource centre managed by the NHS off NHS hospital site"),
	
	/**
	 * Professional staff group department on NHS hospital site
	 */
	_ProfessionalstaffgroupdepartmentonNHShospitalsite ("930791000000108", "Professional staff group department on NHS hospital site"),
	
	/**
	 * Professional staff group department managed by the NHS off NHS hospital site
	 */
	_ProfessionalstaffgroupdepartmentmanagedbytheNHSoffNHShospitalsite ("930801000000107", "Professional staff group department managed by the NHS off NHS hospital site"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	CDACareSettingType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static CDACareSettingType getByCode(String code) {
		CDACareSettingType[] vals = values();
		for (CDACareSettingType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(CDACareSettingType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	