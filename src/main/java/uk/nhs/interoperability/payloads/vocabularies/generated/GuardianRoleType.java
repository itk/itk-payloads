/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the GuardianRoleType vocabulary:
 * <ul>
 *   <li>_06 : Foster parent</li>
 *   <li>_07 : Step parent</li>
 *   <li>_12 : Mother</li>
 *   <li>_13 : Father</li>
 *   <li>_14 : Sister</li>
 *   <li>_15 : Brother</li>
 *   <li>_16 : Relative</li>
 *   <li>_20 : Carer</li>
 *   <li>_98 : Not Known</li>
 *   <li>_99 : Not specified</li>
 *   <li>_24 : Maternal Grand-Father</li>
 *   <li>_25 : Maternal Grand-Mother</li>
 *   <li>_26 : Paternal Grand-Father</li>
 *   <li>_27 : Paternal Grand-Mother</li>
 *   <li>_30 : Aunt</li>
 *   <li>_31 : Uncle</li>
 *   <li>_32 : Niece</li>
 *   <li>_33 : Nephew</li>
 *   <li>_34 : Step-Mother</li>
 *   <li>_35 : Step-Father</li>
 *   <li>_44 : Grandparent</li>
 *   <li>_49 : Person with Parental Responsibility</li>
 *   <li>_61 : Step parent duplicate</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum GuardianRoleType implements VocabularyEntry {
	
	
	/**
	 * Foster parent
	 */
	_06 ("06", "Foster parent"),
	
	/**
	 * Step parent
	 */
	_07 ("07", "Step parent"),
	
	/**
	 * Mother
	 */
	_12 ("12", "Mother"),
	
	/**
	 * Father
	 */
	_13 ("13", "Father"),
	
	/**
	 * Sister
	 */
	_14 ("14", "Sister"),
	
	/**
	 * Brother
	 */
	_15 ("15", "Brother"),
	
	/**
	 * Relative
	 */
	_16 ("16", "Relative"),
	
	/**
	 * Carer
	 */
	_20 ("20", "Carer"),
	
	/**
	 * Not Known
	 */
	_98 ("98", "Not Known"),
	
	/**
	 * Not specified
	 */
	_99 ("99", "Not specified"),
	
	/**
	 * Maternal Grand-Father
	 */
	_24 ("24", "Maternal Grand-Father"),
	
	/**
	 * Maternal Grand-Mother
	 */
	_25 ("25", "Maternal Grand-Mother"),
	
	/**
	 * Paternal Grand-Father
	 */
	_26 ("26", "Paternal Grand-Father"),
	
	/**
	 * Paternal Grand-Mother
	 */
	_27 ("27", "Paternal Grand-Mother"),
	
	/**
	 * Aunt
	 */
	_30 ("30", "Aunt"),
	
	/**
	 * Uncle
	 */
	_31 ("31", "Uncle"),
	
	/**
	 * Niece
	 */
	_32 ("32", "Niece"),
	
	/**
	 * Nephew
	 */
	_33 ("33", "Nephew"),
	
	/**
	 * Step-Mother
	 */
	_34 ("34", "Step-Mother"),
	
	/**
	 * Step-Father
	 */
	_35 ("35", "Step-Father"),
	
	/**
	 * Grandparent
	 */
	_44 ("44", "Grandparent"),
	
	/**
	 * Person with Parental Responsibility
	 */
	_49 ("49", "Person with Parental Responsibility"),
	
	/**
	 * Step parent duplicate
	 */
	_61 ("61", "Step parent (duplicate)"),
	
	/**
	 * Foster parent
	 */
	_Fosterparent ("06", "Foster parent"),
	
	/**
	 * Step parent
	 */
	_Stepparent ("07", "Step parent"),
	
	/**
	 * Mother
	 */
	_Mother ("12", "Mother"),
	
	/**
	 * Father
	 */
	_Father ("13", "Father"),
	
	/**
	 * Sister
	 */
	_Sister ("14", "Sister"),
	
	/**
	 * Brother
	 */
	_Brother ("15", "Brother"),
	
	/**
	 * Relative
	 */
	_Relative ("16", "Relative"),
	
	/**
	 * Carer
	 */
	_Carer ("20", "Carer"),
	
	/**
	 * Not Known
	 */
	_NotKnown ("98", "Not Known"),
	
	/**
	 * Not specified
	 */
	_Notspecified ("99", "Not specified"),
	
	/**
	 * Maternal Grand-Father
	 */
	_MaternalGrandFather ("24", "Maternal Grand-Father"),
	
	/**
	 * Maternal Grand-Mother
	 */
	_MaternalGrandMother ("25", "Maternal Grand-Mother"),
	
	/**
	 * Paternal Grand-Father
	 */
	_PaternalGrandFather ("26", "Paternal Grand-Father"),
	
	/**
	 * Paternal Grand-Mother
	 */
	_PaternalGrandMother ("27", "Paternal Grand-Mother"),
	
	/**
	 * Aunt
	 */
	_Aunt ("30", "Aunt"),
	
	/**
	 * Uncle
	 */
	_Uncle ("31", "Uncle"),
	
	/**
	 * Niece
	 */
	_Niece ("32", "Niece"),
	
	/**
	 * Nephew
	 */
	_Nephew ("33", "Nephew"),
	
	/**
	 * Step-Mother
	 */
	_StepMother ("34", "Step-Mother"),
	
	/**
	 * Step-Father
	 */
	_StepFather ("35", "Step-Father"),
	
	/**
	 * Grandparent
	 */
	_Grandparent ("44", "Grandparent"),
	
	/**
	 * Person with Parental Responsibility
	 */
	_PersonwithParentalResponsibility ("49", "Person with Parental Responsibility"),
	
	/**
	 * Step parent duplicate
	 */
	_Stepparentduplicate ("61", "Step parent (duplicate)"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.16.454";

	GuardianRoleType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static GuardianRoleType getByCode(String code) {
		GuardianRoleType[] vals = values();
		for (GuardianRoleType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(GuardianRoleType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	