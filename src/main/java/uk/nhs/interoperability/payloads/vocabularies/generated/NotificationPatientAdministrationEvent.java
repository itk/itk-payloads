/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the NotificationPatientAdministrationEvent vocabulary:
 * <ul>
 *   <li>_01 : Change of Address</li>
 *   <li>_02 : Death Notification</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum NotificationPatientAdministrationEvent implements VocabularyEntry {
	
	
	/**
	 * Change of Address
	 */
	_01 ("01", "Change of Address"),
	
	/**
	 * Death Notification
	 */
	_02 ("02", "Death Notification"),
	
	/**
	 * Change of Address
	 */
	_ChangeofAddress ("01", "Change of Address"),
	
	/**
	 * Death Notification
	 */
	_DeathNotification ("02", "Death Notification"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.453";

	NotificationPatientAdministrationEvent(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static NotificationPatientAdministrationEvent getByCode(String code) {
		NotificationPatientAdministrationEvent[] vals = values();
		for (NotificationPatientAdministrationEvent val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(NotificationPatientAdministrationEvent other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	