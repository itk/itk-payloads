/**
 * These are java enumerations representing various vocabularies that
 * can be populated into coded fields in various payloads. A field will
 * indicate (in it's javadoc comment) when a specific vocabulary should
 * be used.
 * <br>"Internal" vocabularies are those not formally defined as
 * "vocabularies" in the domain message specifications, but are used for
 * fields that nevertheless have a fixed set of values.
 */
package uk.nhs.interoperability.payloads.vocabularies.internal;