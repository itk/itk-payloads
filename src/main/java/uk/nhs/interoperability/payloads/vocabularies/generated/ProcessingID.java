/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the ProcessingID vocabulary:
 * <ul>
 *   <li>_D : Debugging</li>
 *   <li>_P : Production</li>
 *   <li>_T : Training</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum ProcessingID implements VocabularyEntry {
	
	
	/**
	 * Debugging
	 */
	_D ("D", "Debugging"),
	
	/**
	 * Production
	 */
	_P ("P", "Production"),
	
	/**
	 * Training
	 */
	_T ("T", "Training"),
	
	/**
	 * Debugging
	 */
	_Debugging ("D", "Debugging"),
	
	/**
	 * Production
	 */
	_Production ("P", "Production"),
	
	/**
	 * Training
	 */
	_Training ("T", "Training"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.11.103";

	ProcessingID(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static ProcessingID getByCode(String code) {
		ProcessingID[] vals = values();
		for (ProcessingID val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(ProcessingID other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	