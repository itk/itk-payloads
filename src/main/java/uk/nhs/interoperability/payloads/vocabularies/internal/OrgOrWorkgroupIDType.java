/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values types of "ID"
 * <ul>
 *   <li>SDSOrgID : SDS organisation identifier</li>
 *   <li>SDSWorkgroupID : SDS workgroup identifier</li>
 * </ul>
 * This is an internal enumeration for this Java library and is not a standard ITK vocabulary
 */
public enum OrgOrWorkgroupIDType implements VocabularyEntry {

	/** ODS Organisation Code */
	SDSOrgID,
	/** ODS Workgroup Code */
	SDSWorkgroupID;
	
	public String code;
	
	private OrgOrWorkgroupIDType() {
		this.code = this.toString();
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static OrgOrWorkgroupIDType getByCode(String code) {
		OrgOrWorkgroupIDType[] vals = values();
		for (OrgOrWorkgroupIDType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(OrgOrWorkgroupIDType other) {
		return (other.getCode().equals(code));
	}
	
}