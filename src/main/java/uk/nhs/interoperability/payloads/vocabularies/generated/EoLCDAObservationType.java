/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the EoLCDAObservationType vocabulary:
 * <ul>
 *   <li>_EOLCP : EOL Care Plan</li>
 *   <li>_EOLTLU : EOL Tool Used</li>
 *   <li>_EOLTST : EOL Current Stage</li>
 *   <li>_EOLPRD : EOL Planned Review Date</li>
 *   <li>_EOLPR : EOL Patient Relationships</li>
 *   <li>_EOLADP : EOL Patient ADRT</li>
 *   <li>_EOLADL : EOL Patient ADRT Docs Location</li>
 *   <li>_EOLADS : EOL Patient ADRT Discussed</li>
 *   <li>_EOLBLC : EOL Anticipatory Medicine Box Location</li>
 *   <li>_EOLDCR : EOL DNACPR by Senior Responsible Clinician</li>
 *   <li>_EOLDDL : EOL DNACPR Docs Location</li>
 *   <li>_EOLAOP : EOL Aware of Prognosis</li>
 *   <li>_EOLLPA : EOL Authority to LPA</li>
 *   <li>_EOLDRD : EOL DNACPR Review Date</li>
 *   <li>_EOLRST : EOL Resuscitation Status</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum EoLCDAObservationType implements VocabularyEntry {
	
	
	/**
	 * EOL Care Plan
	 */
	_EOLCP ("EOLCP", "EOL Care Plan"),
	
	/**
	 * EOL Tool Used
	 */
	_EOLTLU ("EOLTLU", "EOL Tool Used"),
	
	/**
	 * EOL Current Stage
	 */
	_EOLTST ("EOLTST", "EOL Current Stage"),
	
	/**
	 * EOL Planned Review Date
	 */
	_EOLPRD ("EOLPRD", "EOL Planned Review Date"),
	
	/**
	 * EOL Patient Relationships
	 */
	_EOLPR ("EOLPR", "EOL Patient Relationships"),
	
	/**
	 * EOL Patient ADRT
	 */
	_EOLADP ("EOLADP", "EOL Patient ADRT"),
	
	/**
	 * EOL Patient ADRT Docs Location
	 */
	_EOLADL ("EOLADL", "EOL Patient ADRT Docs Location"),
	
	/**
	 * EOL Patient ADRT Discussed
	 */
	_EOLADS ("EOLADS", "EOL Patient ADRT Discussed"),
	
	/**
	 * EOL Anticipatory Medicine Box Location
	 */
	_EOLBLC ("EOLBLC", "EOL Anticipatory Medicine Box Location"),
	
	/**
	 * EOL DNACPR by Senior Responsible Clinician
	 */
	_EOLDCR ("EOLDCR", "EOL DNACPR by Senior Responsible Clinician"),
	
	/**
	 * EOL DNACPR Docs Location
	 */
	_EOLDDL ("EOLDDL", "EOL DNACPR Docs Location"),
	
	/**
	 * EOL Aware of Prognosis
	 */
	_EOLAOP ("EOLAOP", "EOL Aware of Prognosis"),
	
	/**
	 * EOL Authority to LPA
	 */
	_EOLLPA ("EOLLPA", "EOL Authority to LPA"),
	
	/**
	 * EOL DNACPR Review Date
	 */
	_EOLDRD ("EOLDRD", "EOL DNACPR Review Date"),
	
	/**
	 * EOL Resuscitation Status
	 */
	_EOLRST ("EOLRST", "EOL Resuscitation Status"),
	
	/**
	 * EOL Care Plan
	 */
	_EOLCarePlan ("EOLCP", "EOL Care Plan"),
	
	/**
	 * EOL Tool Used
	 */
	_EOLToolUsed ("EOLTLU", "EOL Tool Used"),
	
	/**
	 * EOL Current Stage
	 */
	_EOLCurrentStage ("EOLTST", "EOL Current Stage"),
	
	/**
	 * EOL Planned Review Date
	 */
	_EOLPlannedReviewDate ("EOLPRD", "EOL Planned Review Date"),
	
	/**
	 * EOL Patient Relationships
	 */
	_EOLPatientRelationships ("EOLPR", "EOL Patient Relationships"),
	
	/**
	 * EOL Patient ADRT
	 */
	_EOLPatientADRT ("EOLADP", "EOL Patient ADRT"),
	
	/**
	 * EOL Patient ADRT Docs Location
	 */
	_EOLPatientADRTDocsLocation ("EOLADL", "EOL Patient ADRT Docs Location"),
	
	/**
	 * EOL Patient ADRT Discussed
	 */
	_EOLPatientADRTDiscussed ("EOLADS", "EOL Patient ADRT Discussed"),
	
	/**
	 * EOL Anticipatory Medicine Box Location
	 */
	_EOLAnticipatoryMedicineBoxLocation ("EOLBLC", "EOL Anticipatory Medicine Box Location"),
	
	/**
	 * EOL DNACPR by Senior Responsible Clinician
	 */
	_EOLDNACPRbySeniorResponsibleClinician ("EOLDCR", "EOL DNACPR by Senior Responsible Clinician"),
	
	/**
	 * EOL DNACPR Docs Location
	 */
	_EOLDNACPRDocsLocation ("EOLDDL", "EOL DNACPR Docs Location"),
	
	/**
	 * EOL Aware of Prognosis
	 */
	_EOLAwareofPrognosis ("EOLAOP", "EOL Aware of Prognosis"),
	
	/**
	 * EOL Authority to LPA
	 */
	_EOLAuthoritytoLPA ("EOLLPA", "EOL Authority to LPA"),
	
	/**
	 * EOL DNACPR Review Date
	 */
	_EOLDNACPRReviewDate ("EOLDRD", "EOL DNACPR Review Date"),
	
	/**
	 * EOL Resuscitation Status
	 */
	_EOLResuscitationStatus ("EOLRST", "EOL Resuscitation Status"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.435";

	EoLCDAObservationType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static EoLCDAObservationType getByCode(String code) {
		EoLCDAObservationType[] vals = values();
		for (EoLCDAObservationType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(EoLCDAObservationType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	