/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT;

public enum HL7FunctionType implements VocabularyEntry {
	/** A person who actually and principally carries out an action. Need not be the principal responsible actor, e.g. a surgery resident operating under supervision of attending surgeon, and may be the patient in self-care, e.g. fingerstick blood sugar. The traditional order filler is a performer. This information should accompany every service event. */
	Performer("PRF"),
	/** A primary performer is a type of performer that represents the principal or primary performer of an act that involves other performers or assistants. The primary performer is the person who undertakes the primary activity not the most senior person involved. */
	PrimaryPerformer("PPRF"),
	/** A secondary performer is a type of performer that represents a person who assists in the performance of the act. A secondary performer must be directly involved in the act. The term secondary performer relates only to the nature of participation in the act. Neither job title nor seniority has any bearing on whether a participant is a primary performer or secondary performer. */
	SecondaryPerformer("SPRF");
	
	public String code;
	
	private HL7FunctionType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}

	public static HL7FunctionType getByCode(String code) {
		HL7FunctionType[] vals = values();
		for (HL7FunctionType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(HL7FunctionType other) {
		return (other.getCode().equals(code));
	}
	
}
