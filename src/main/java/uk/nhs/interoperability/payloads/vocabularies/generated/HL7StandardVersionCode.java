/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the HL7StandardVersionCode vocabulary:
 * <ul>
 *   <li>_V3NPfIT20 : Overall version description for UK use</li>
 *   <li>_V3NPfIT21 : Overall version description for UK use</li>
 *   <li>_V3NPfIT22 : Overall version description for UK use</li>
 *   <li>_V3NPfIT23 : Overall version description for UK use</li>
 *   <li>_V3NPfIT30 : Overall version description for UK use</li>
 *   <li>_V3NPfIT31 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3101 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3102 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3103 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3104 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3105 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3106 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3107 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3108 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3109 : Overall version description for UK use</li>
 *   <li>_V3NPfIT3110 : Overall version description for UK use</li>
 *   <li>_V3NPfIT400 : Overall version description for UK use</li>
 *   <li>_V3NPfIT41 : Overall version description for UK use</li>
 *   <li>_V3NPfIT4101 : Overall version description for UK use</li>
 *   <li>_V3NPfIT4102 : Overall version description for UK use</li>
 *   <li>_V3NPfIT4103 : Overall version description for UK use</li>
 *   <li>_V3NPfIT4104 : Overall version description for UK use</li>
 *   <li>_V3NPfIT4200 : Overall version description for UK use</li>
 *   <li>_V3NPfIT50 : Overall version description for UK use</li>
 *   <li>_V3NPfIT51 : Overall version description for UK use</li>
 *   <li>_V3NPfIT52 : Overall version description for UK use</li>
 *   <li>_V3NPfIT53 : Overall version description for UK use</li>
 *   <li>_V3NPfIT5400 : Overall version description for UK use</li>
 *   <li>_3NPfIT5500 : Overall version description for UK use</li>
 *   <li>_3NPfIT6100 : Overall version description for UK use</li>
 *   <li>_3NPfIT6101 : Overall version description for UK use</li>
 *   <li>_3NPfIT5501 : Overall version description for UK use</li>
 *   <li>_3NPfIT6102 : Overall version description for UK use</li>
 *   <li>_3NPfIT6200 : Overall version description for UK use</li>
 *   <li>_3NPfIT6201 : Overall version description for UK use</li>
 *   <li>_3NPfIT6202 : Overall version description for UK use</li>
 *   <li>_3NPfIT6300 : Overall version description for UK use</li>
 *   <li>_3NPfIT7000 : Overall version description for UK use</li>
 *   <li>_3NPfIT7100 : Overall version description for UK use</li>
 *   <li>_3NPfIT6301 : Overall version description for UK use</li>
 *   <li>_3NPfIT7200 : Overall version description for UK use</li>
 *   <li>_3NPfIT7201 : Overall version description for UK use</li>
 *   <li>_3NPfIT7202 : Overall version description for UK use</li>
 *   <li>_3NPfIT8100 : Overall version description for UK use</li>
 *   <li>_3HSCIDMS20 : Overall version description for UK use</li>
 *   <li>_3HSCIDMS31 : Overall version description for UK use</li>
 *   <li>_3NCDDMS10 : Overall version description for UK use</li>
 *   <li>_3OOHDMS20 : Overall version description for UK use</li>
 *   <li>_3DISDMS40 : Overall version description for UK use</li>
 *   <li>_3EDTDMS40 : Overall version description for UK use</li>
 *   <li>_3OPSDMS20 : Overall version description for UK use</li>
 *   <li>_3ABMDMS20 : Overall version description for UK use</li>
 *   <li>_3HSCIDMS30 : Overall version description for UK use</li>
 *   <li>_3HSCIDMS32 : Overall version description for UK use</li>
 *   <li>_3GPSDMS10 : Overall version description for UK use</li>
 *   <li>_v3ABM_DMSv20RC2 : Overall version description for UK use</li>
 *   <li>_v3DIS_DMSv40RC2 : Overall version description for UK use</li>
 *   <li>_v3EDT_DMSv40RC2 : Overall version description for UK use</li>
 *   <li>_v3NCD_DMSv10RC2 : Overall version description for UK use</li>
 *   <li>_v3GPS_DMSv10RC1 : Overall version description for UK use</li>
 *   <li>_v3HSC_DMSv32RC2 : Overall version description for UK use</li>
 *   <li>_v3TEL_DMSv20RC1 : Overall version description for UK use</li>
 *   <li>_v3OOH_DMSv20RC2 : Overall version description for UK use</li>
 *   <li>_v3OPS_DMSv20RC2 : Overall version description for UK use</li>
 *   <li>_v3NHP_DMSv10RC2 : Overall version description for UK use</li>
 *   <li>_v3CPI_DMSv10RC1 : Overall version description for UK use</li>
 *   <li>_v3CPI_DMSv10RC2 : Overall version description for UK use</li>
 *   <li>_v3INF_DMSv57FIN : Overall version description for UK use</li>
 *   <li>_v3ABM_DMSv20FIN : Overall version description for UK use</li>
 *   <li>_v3ABM_DMSv21FIN : Overall version description for UK use</li>
 *   <li>_v3CPI_DMSv10RC3 : Overall version description for UK use</li>
 *   <li>_v3DIS_DMSv40FIN : Overall version description for UK use</li>
 *   <li>_v3DIS_DMSv41FIN : Overall version description for UK use</li>
 *   <li>_v3EPS_DMSv231FIN : Overall version description for UK use</li>
 *   <li>_v3EPS_DMSv330RC1 : Overall version description for UK use</li>
 *   <li>_v3EPS_DMSv330RC2 : Overall version description for UK use</li>
 *   <li>_v3EPS_DMSv330FIN : Overall version description for UK use</li>
 *   <li>_v3EDT_DMSv40FIN : Overall version description for UK use</li>
 *   <li>_v3EDT_DMSv41FIN : Overall version description for UK use</li>
 *   <li>_v3G2G_DMSv10RC1 : Overall version description for UK use</li>
 *   <li>_v3HSC_DMSv40FIN : Overall version description for UK use</li>
 *   <li>_v3HSC_DMSv41FIN : Overall version description for UK use</li>
 *   <li>_v3NCD_DMSv10FIN : Overall version description for UK use</li>
 *   <li>_v3NCD_DMSv11FIN : Overall version description for UK use</li>
 *   <li>_v3OOH_DMSv20FIN : Overall version description for UK use</li>
 *   <li>_v3OOH_DMSv21FIN : Overall version description for UK use</li>
 *   <li>_v3OPS_DMSv20FIN : Overall version description for UK use</li>
 *   <li>_v3OPS_DMSv21FIN : Overall version description for UK use</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum HL7StandardVersionCode implements VocabularyEntry {
	
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT20 ("V3NPfIT2.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT21 ("V3NPfIT2.1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT22 ("V3NPfIT2.2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT23 ("V3NPfIT2.3", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT30 ("V3NPfIT3.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT31 ("V3NPfIT3.1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3101 ("V3NPfIT3.1.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3102 ("V3NPfIT3.1.02", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3103 ("V3NPfIT3.1.03", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3104 ("V3NPfIT3.1.04", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3105 ("V3NPfIT3.1.05", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3106 ("V3NPfIT3.1.06", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3107 ("V3NPfIT3.1.07", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3108 ("V3NPfIT3.1.08", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3109 ("V3NPfIT3.1.09", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT3110 ("V3NPfIT3.1.10", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT400 ("V3NPfIT4.0.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT41 ("V3NPfIT4.1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT4101 ("V3NPfIT4.1.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT4102 ("V3NPfIT4.1.02", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT4103 ("V3NPfIT4.1.03", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT4104 ("V3NPfIT4.1.04", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT4200 ("V3NPfIT4.2.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT50 ("V3NPfIT5.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT51 ("V3NPfIT5.1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT52 ("V3NPfIT5.2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT53 ("V3NPfIT5.3", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_V3NPfIT5400 ("V3NPfIT5.4.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT5500 ("3NPfIT5.5.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6100 ("3NPfIT6.1.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6101 ("3NPfIT6.1.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT5501 ("3NPfIT5.5.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6102 ("3NPfIT6.1.02", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6200 ("3NPfIT6.2.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6201 ("3NPfIT6.2.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6202 ("3NPfIT6.2.02", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6300 ("3NPfIT6.3.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT7000 ("3NPfIT7.0.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT7100 ("3NPfIT7.1.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT6301 ("3NPfIT6.3.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT7200 ("3NPfIT7.2.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT7201 ("3NPfIT7.2.01", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT7202 ("3NPfIT7.2.02", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NPfIT8100 ("3NPfIT8.1.00", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3HSCIDMS20 ("3HSCIDMS2.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3HSCIDMS31 ("3HSCIDMS3.1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3NCDDMS10 ("3NCDDMS1.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3OOHDMS20 ("3OOHDMS2.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3DISDMS40 ("3DISDMS4.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3EDTDMS40 ("3EDTDMS4.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3OPSDMS20 ("3OPSDMS2.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3ABMDMS20 ("3ABMDMS2.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3HSCIDMS30 ("3HSCIDMS3.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3HSCIDMS32 ("3HSCIDMS3.2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_3GPSDMS10 ("3GPSDMS1.0", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3ABM_DMSv20RC2 ("v3ABM_DMSv2.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3DIS_DMSv40RC2 ("v3DIS_DMSv4.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EDT_DMSv40RC2 ("v3EDT_DMSv4.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3NCD_DMSv10RC2 ("v3NCD_DMSv1.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3GPS_DMSv10RC1 ("v3GPS_DMSv1.0:RC1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3HSC_DMSv32RC2 ("v3HSC_DMSv3.2:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3TEL_DMSv20RC1 ("v3TEL_DMSv2.0:RC1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3OOH_DMSv20RC2 ("v3OOH_DMSv2.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3OPS_DMSv20RC2 ("v3OPS_DMSv2.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3NHP_DMSv10RC2 ("v3NHP_DMSv1.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3CPI_DMSv10RC1 ("v3CPI_DMSv1.0:RC1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3CPI_DMSv10RC2 ("v3CPI_DMSv1.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3INF_DMSv57FIN ("v3INF_DMSv5.7:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3ABM_DMSv20FIN ("v3ABM_DMSv2.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3ABM_DMSv21FIN ("v3ABM_DMSv2.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3CPI_DMSv10RC3 ("v3CPI_DMSv1.0:RC3", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3DIS_DMSv40FIN ("v3DIS_DMSv4.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3DIS_DMSv41FIN ("v3DIS_DMSv4.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EPS_DMSv231FIN ("v3EPS_DMSv2.3.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EPS_DMSv330RC1 ("v3EPS_DMSv3.3.0:RC1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EPS_DMSv330RC2 ("v3EPS_DMSv3.3.0:RC2", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EPS_DMSv330FIN ("v3EPS_DMSv3.3.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EDT_DMSv40FIN ("v3EDT_DMSv4.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3EDT_DMSv41FIN ("v3EDT_DMSv4.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3G2G_DMSv10RC1 ("v3G2G_DMSv1.0:RC1", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3HSC_DMSv40FIN ("v3HSC_DMSv4.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3HSC_DMSv41FIN ("v3HSC_DMSv4.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3NCD_DMSv10FIN ("v3NCD_DMSv1.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3NCD_DMSv11FIN ("v3NCD_DMSv1.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3OOH_DMSv20FIN ("v3OOH_DMSv2.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3OOH_DMSv21FIN ("v3OOH_DMSv2.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3OPS_DMSv20FIN ("v3OPS_DMSv2.0:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_v3OPS_DMSv21FIN ("v3OPS_DMSv2.1:FIN", "Overall version description for UK use"),
	
	/**
	 * Overall version description for UK use
	 */
	_OverallversiondescriptionforUKuse ("V3NPfIT2.0", "Overall version description for UK use"),
	
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	// Duplicate display name omitted: Overall version description for UK use
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.1.5.1097.11.1";

	HL7StandardVersionCode(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static HL7StandardVersionCode getByCode(String code) {
		HL7StandardVersionCode[] vals = values();
		for (HL7StandardVersionCode val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(HL7StandardVersionCode other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	