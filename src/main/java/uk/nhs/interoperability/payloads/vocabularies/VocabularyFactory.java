/**
 * Note: This class is generated automatically when GenerateVocabularies is run
 * @author Adam Hatherly
 */
package uk.nhs.interoperability.payloads.vocabularies;

public class VocabularyFactory {
	public static VocabularyEntry getVocab(String name, String oid, String code) {
		if (oid==null) oid = "";
		if (name.equals("ActStatus")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ActStatus.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.5.14")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ActStatus.getByCode(code);
		}
		if (name.equals("CDAOrganizationProviderType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.289")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType.getByCode(code);
		}
		if (name.equals("CDAOrganizationType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.191")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationType.getByCode(code);
		}
		if (name.equals("DocumentConsentSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT.getByCode(code);
		}
		if (name.equals("HumanLanguage")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.70")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage.getByCode(code);
		}
		if (name.equals("LanguageAbilityMode")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.5.60")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityMode.getByCode(code);
		}
		if (name.equals("ReferenceURLType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.336")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ReferenceURLType.getByCode(code);
		}
		if (name.equals("Sex")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Sex.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.16.25")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Sex.getByCode(code);
		}
		if (name.equals("TargetAwareness")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.5.137")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.TargetAwareness.getByCode(code);
		}
		if (name.equals("PersonalRelationshipRoleType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.1.11.19563")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PersonalRelationshipRoleType.getByCode(code);
		}
		if (name.equals("ParticipationFunction")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.5.88")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction.getByCode(code);
		}
		if (name.equals("x_EncounterParticipant")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.x_EncounterParticipant.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.1.11.19600")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.x_EncounterParticipant.getByCode(code);
		}
		if (name.equals("ParticipationType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.1.5.90.11.1")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType.getByCode(code);
		}
		if (name.equals("GuardianOrganisationType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.478")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.GuardianOrganisationType.getByCode(code);
		}
		if (name.equals("AudiologyTestingOutcomeStatus")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.466")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AudiologyTestingOutcomeStatus.getByCode(code);
		}
		if (name.equals("PKUScreeningResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.461")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PKUScreeningResult.getByCode(code);
		}
		if (name.equals("BSLaboratoryInterpretationCode")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.490")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode.getByCode(code);
		}
		if (name.equals("BSScreeningLocStatus")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.486")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningLocStatus.getByCode(code);
		}
		if (name.equals("BSScreeningStatusSubCodes")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.487")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.BSScreeningStatusSubCodes.getByCode(code);
		}
		if (name.equals("CFScreeningResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.463")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CFScreeningResult.getByCode(code);
		}
		if (name.equals("MCADDScreeningResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.465")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.MCADDScreeningResult.getByCode(code);
		}
		if (name.equals("SCDScreeningResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.462")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.SCDScreeningResult.getByCode(code);
		}
		if (name.equals("CHTScreeningResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.464")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CHTScreeningResult.getByCode(code);
		}
		if (name.equals("NewBornHearingScreeningOutcomeSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NewBornHearingScreeningOutcomeSnCT.getByCode(code);
		}
		if (name.equals("HipsExaminationResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.446")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HipsExaminationResult.getByCode(code);
		}
		if (name.equals("HipsUltraSoundOutcomeDecision")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.467")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HipsUltraSoundOutcomeDecision.getByCode(code);
		}
		if (name.equals("HipsExpertManagementPlanType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.468")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HipsExpertManagementPlanType.getByCode(code);
		}
		if (name.equals("HeartExaminationResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.447")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HeartExaminationResult.getByCode(code);
		}
		if (name.equals("EyesExaminationResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.448")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EyesExaminationResult.getByCode(code);
		}
		if (name.equals("TestesExaminationResult")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.449")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.TestesExaminationResult.getByCode(code);
		}
		if (name.equals("PDSMiniServiceResponseCode")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.285")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode.getByCode(code);
		}
		if (name.equals("InformationSensitivity")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.16.1")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity.getByCode(code);
		}
		if (name.equals("PreviousNhsContact")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.38")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact.getByCode(code);
		}
		if (name.equals("DeathNotification")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.16.5")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification.getByCode(code);
		}
		if (name.equals("HL7StandardVersionCode")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.1.5.1097.11.1")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode.getByCode(code);
		}
		if (name.equals("ProcessingMode")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.11.207")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode.getByCode(code);
		}
		if (name.equals("ProcessingID")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.11.103")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID.getByCode(code);
		}
		if (name.equals("AcknowledgementDetailType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.1.5.1082.11.1")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType.getByCode(code);
		}
		if (name.equals("AcknowledgementType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.1.5.18.11.1")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType.getByCode(code);
		}
		if (name.equals("QueryResponse")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.1.5.101.11.1")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse.getByCode(code);
		}
		if (name.equals("DetectedIssueQualifier")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.104")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier.getByCode(code);
		}
		if (name.equals("Ethnicity")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.16.81")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Ethnicity.getByCode(code);
		}
		if (name.equals("RoleClassAssociative")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.1.11.19313")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative.getByCode(code);
		}
		if (name.equals("CDAEncounterType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAEncounterType.getByCode(code);
		}
		if (name.equals("Encounterdisposition")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Encounterdisposition.getByCode(code);
		}
		if (name.equals("Documenttype")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype.getByCode(code);
		}
		if (name.equals("CorrespondenceDocumenttype")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceDocumenttype.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceDocumenttype.getByCode(code);
		}
		if (name.equals("CorrespondenceCaresettingtype")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceCaresettingtype.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceCaresettingtype.getByCode(code);
		}
		if (name.equals("ADRTDiscussedSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT.getByCode(code);
		}
		if (name.equals("AuthorFunctionType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.178")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType.getByCode(code);
		}
		if (name.equals("AuthoritytoLPASnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT.getByCode(code);
		}
		if (name.equals("DNACPRprefSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT.getByCode(code);
		}
		if (name.equals("EoLADRTprefSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT.getByCode(code);
		}
		if (name.equals("EoLAnticipatoryMedicineBoxIssueSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT.getByCode(code);
		}
		if (name.equals("EoLCDAObservationType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLCDAObservationType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.435")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLCDAObservationType.getByCode(code);
		}
		if (name.equals("EoLDocumentTypeSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT.getByCode(code);
		}
		if (name.equals("EoLToolSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT.getByCode(code);
		}
		if (name.equals("JobRoleName")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.124")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName.getByCode(code);
		}
		if (name.equals("LanguageAbilityProficiency")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.5.61")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency.getByCode(code);
		}
		if (name.equals("PrognosisAwarenessSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT.getByCode(code);
		}
		if (name.equals("ResuscitationStatusSnCT")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ResuscitationStatusSnCT.getByCode(code);
		}
		if (name.equals("x_BasicConfidentialityKind")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.1.11.16926")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind.getByCode(code);
		}
		if (name.equals("NotificationPatientEncounterEvent")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientEncounterEvent.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.452")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientEncounterEvent.getByCode(code);
		}
		if (name.equals("NotificationDocumentEvent")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentEvent.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.451")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentEvent.getByCode(code);
		}
		if (name.equals("NotificationDocumentType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.438")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType.getByCode(code);
		}
		if (name.equals("NotificationEventType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.437")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType.getByCode(code);
		}
		if (name.equals("NotificationPatientAdministrationEvent")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientAdministrationEvent.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.453")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientAdministrationEvent.getByCode(code);
		}
		if (name.equals("CDAPersonRelationshipType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.16.45")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType.getByCode(code);
		}
		if (name.equals("RoleCode")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.5.111")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode.getByCode(code);
		}
		if (name.equals("DocumentResponseType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.17.457")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType.getByCode(code);
		}
		if (name.equals("GuardianRoleType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.16.454")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.GuardianRoleType.getByCode(code);
		}
		if (name.equals("CDACareSettingType")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType.getByCode(code);
		}
		if (oid.equals("2.16.840.1.113883.2.1.3.2.4.15")) {
			return uk.nhs.interoperability.payloads.vocabularies.generated.CDACareSettingType.getByCode(code);
		}
		if (name.equals("TelecomUseType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType.getByCode(code);
		}
		if (name.equals("HL7ActType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType.getByCode(code);
		}
		if (name.equals("SystemIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType.getByCode(code);
		}
		if (name.equals("AttachmentType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType.getByCode(code);
		}
		if (name.equals("WorkgroupIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.WorkgroupIDType.getByCode(code);
		}
		if (name.equals("FHIRAddressType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.FHIRAddressType.getByCode(code);
		}
		if (name.equals("OrgOrWorkgroupIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.OrgOrWorkgroupIDType.getByCode(code);
		}
		if (name.equals("IDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.IDType.getByCode(code);
		}
		if (name.equals("PersonIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType.getByCode(code);
		}
		if (name.equals("PersonNameType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType.getByCode(code);
		}
		if (name.equals("HL7FunctionType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.HL7FunctionType.getByCode(code);
		}
		if (name.equals("FHIRTelecomUseType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.FHIRTelecomUseType.getByCode(code);
		}
		if (name.equals("AddressType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AddressType.getByCode(code);
		}
		if (name.equals("PDSPatientIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType.getByCode(code);
		}
		if (name.equals("HL7PerformerType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType.getByCode(code);
		}
		if (name.equals("PatientIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType.getByCode(code);
		}
		if (name.equals("OrgIDType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType.getByCode(code);
		}
		if (name.equals("NullFlavour")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode(code);
		}
		if (name.equals("FHIRPersonNameType")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.FHIRPersonNameType.getByCode(code);
		}
		if (name.equals("DocumentTypes")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.DocumentTypes.getByCode(code);
		}
		if (name.equals("NHSNumberTraceStatus")) {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus.getByCode(code);
		}
		return null;
	}
}
