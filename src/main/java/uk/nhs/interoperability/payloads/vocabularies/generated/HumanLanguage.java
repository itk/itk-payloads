/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the HumanLanguage vocabulary:
 * <ul>
 *   <li>_q1 : Braille</li>
 *   <li>_q2 : American Sign Language</li>
 *   <li>_q3 : AustralianSign Language</li>
 *   <li>_q4 : British Sign Language</li>
 *   <li>_q5 : Makaton</li>
 *   <li>_aa : Afar</li>
 *   <li>_ab : Abkhazian</li>
 *   <li>_af : Afrikaans</li>
 *   <li>_ak : Akan</li>
 *   <li>_sq : Albanian</li>
 *   <li>_am : Amharic</li>
 *   <li>_ar : Arabic</li>
 *   <li>_an : Aragonese</li>
 *   <li>_hy : Armenian</li>
 *   <li>_as : Assamese</li>
 *   <li>_av : Avaric</li>
 *   <li>_ae : Avestan</li>
 *   <li>_ay : Aymara</li>
 *   <li>_az : Azerbaijani</li>
 *   <li>_ba : Bashkir</li>
 *   <li>_bm : Bambara</li>
 *   <li>_eu : Basque</li>
 *   <li>_be : Belarusian</li>
 *   <li>_bn : Bengali</li>
 *   <li>_bh : Bihari</li>
 *   <li>_bi : Bislama</li>
 *   <li>_bo : Tibetan</li>
 *   <li>_bs : Bosnian</li>
 *   <li>_br : Breton</li>
 *   <li>_bg : Bulgarian</li>
 *   <li>_my : Burmese</li>
 *   <li>_ca : Catalan; Valencian</li>
 *   <li>_cs : Czech</li>
 *   <li>_ch : Chamorro</li>
 *   <li>_ce : Chechen</li>
 *   <li>_zh : Chinese</li>
 *   <li>_cu : Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic</li>
 *   <li>_cv : Chuvash</li>
 *   <li>_kw : Cornish</li>
 *   <li>_co : Corsican</li>
 *   <li>_cr : Cree</li>
 *   <li>_cy : Welsh</li>
 *   <li>_da : Danish</li>
 *   <li>_de : German</li>
 *   <li>_dv : Divehi; Dhivehi; Maldivian</li>
 *   <li>_nl : Dutch; Flemish</li>
 *   <li>_dz : Dzongkha</li>
 *   <li>_el : Greek, Modern 1453-</li>
 *   <li>_en : English</li>
 *   <li>_eo : Esperanto</li>
 *   <li>_et : Estonian</li>
 *   <li>_ee : Ewe</li>
 *   <li>_fo : Faroese</li>
 *   <li>_fa : Persian</li>
 *   <li>_fj : Fijian</li>
 *   <li>_fi : Finnish</li>
 *   <li>_fr : French</li>
 *   <li>_fy : Western Frisian</li>
 *   <li>_ff : Fulah</li>
 *   <li>_ka : Georgian</li>
 *   <li>_gd : Gaelic; Scottish Gaelic</li>
 *   <li>_ga : Irish</li>
 *   <li>_gl : Galician</li>
 *   <li>_gv : Manx</li>
 *   <li>_gn : Guarani</li>
 *   <li>_gu : Gujarati</li>
 *   <li>_ht : Haitian; Haitian Creole</li>
 *   <li>_ha : Hausa</li>
 *   <li>_he : Hebrew</li>
 *   <li>_hz : Herero</li>
 *   <li>_hi : Hindi</li>
 *   <li>_ho : Hiri Motu</li>
 *   <li>_hr : Croatian</li>
 *   <li>_hu : Hungarian</li>
 *   <li>_ig : Igbo</li>
 *   <li>_is : Icelandic</li>
 *   <li>_io : Ido</li>
 *   <li>_ii : Sichuan Yi</li>
 *   <li>_iu : Inuktitut</li>
 *   <li>_ie : Interlingue</li>
 *   <li>_ia : Interlingua International Auxiliary Language Association</li>
 *   <li>_id : Indonesian</li>
 *   <li>_ik : Inupiaq</li>
 *   <li>_it : Italian</li>
 *   <li>_jv : Javanese</li>
 *   <li>_ja : Japanese</li>
 *   <li>_kl : Kalaallisut; Greenlandic</li>
 *   <li>_kn : Kannada</li>
 *   <li>_ks : Kashmiri</li>
 *   <li>_kr : Kanuri</li>
 *   <li>_kk : Kazakh</li>
 *   <li>_km : Central Khmer</li>
 *   <li>_ki : Kikuyu; Gikuyu</li>
 *   <li>_rw : Kinyarwanda</li>
 *   <li>_ky : Kirghiz; Kyrgyz</li>
 *   <li>_kv : Komi</li>
 *   <li>_kg : Kongo</li>
 *   <li>_ko : Korean</li>
 *   <li>_kj : Kuanyama; Kwanyama</li>
 *   <li>_ku : Kurdish</li>
 *   <li>_lo : Lao</li>
 *   <li>_la : Latin</li>
 *   <li>_lv : Latvian</li>
 *   <li>_li : Limburgan; Limburger; Limburgish</li>
 *   <li>_ln : Lingala</li>
 *   <li>_lt : Lithuanian</li>
 *   <li>_lb : Luxembourgish; Letzeburgesch</li>
 *   <li>_lu : Luba-Katanga</li>
 *   <li>_lg : Ganda</li>
 *   <li>_mk : Macedonian</li>
 *   <li>_mh : Marshallese</li>
 *   <li>_ml : Malayalam</li>
 *   <li>_mi : Maori</li>
 *   <li>_mr : Marathi</li>
 *   <li>_ms : Malay</li>
 *   <li>_mg : Malagasy</li>
 *   <li>_mt : Maltese</li>
 *   <li>_mo : Moldavian</li>
 *   <li>_mn : Mongolian</li>
 *   <li>_na : Nauru</li>
 *   <li>_nv : Navajo; Navaho</li>
 *   <li>_nr : Ndebele, South; South Ndebele</li>
 *   <li>_nd : Ndebele, North; North Ndebele</li>
 *   <li>_ng : Ndonga</li>
 *   <li>_ne : Nepali</li>
 *   <li>_nn : Norwegian Nynorsk; Nynorsk, Norwegian</li>
 *   <li>_nb : Bokml, Norwegian; Norwegian Bokml</li>
 *   <li>_no : Norwegian</li>
 *   <li>_ny : Chichewa; Chewa; Nyanja</li>
 *   <li>_oc : Occitan post1500; Provenal</li>
 *   <li>_oj : Ojibwa</li>
 *   <li>_or : Oriya</li>
 *   <li>_om : Oromo</li>
 *   <li>_os : Ossetian; Ossetic</li>
 *   <li>_pa : Panjabi; Punjabi</li>
 *   <li>_pi : Pali</li>
 *   <li>_pl : Polish</li>
 *   <li>_pt : Portuguese</li>
 *   <li>_ps : Pushto</li>
 *   <li>_qu : Quechua</li>
 *   <li>_rm : Romansh</li>
 *   <li>_ro : Romanian</li>
 *   <li>_rn : Rundi</li>
 *   <li>_ru : Russian</li>
 *   <li>_sg : Sango</li>
 *   <li>_sa : Sanskrit</li>
 *   <li>_sr : Serbian</li>
 *   <li>_si : Sinhala; Sinhalese</li>
 *   <li>_sk : Slovak</li>
 *   <li>_sl : Slovenian</li>
 *   <li>_se : Northern Sami</li>
 *   <li>_sm : Samoan</li>
 *   <li>_sn : Shona</li>
 *   <li>_sd : Sindhi</li>
 *   <li>_so : Somali</li>
 *   <li>_st : Sotho, Southern</li>
 *   <li>_es : Spanish; Castilian</li>
 *   <li>_sc : Sardinian</li>
 *   <li>_ss : Swati</li>
 *   <li>_su : Sundanese</li>
 *   <li>_sw : Swahili</li>
 *   <li>_sv : Swedish</li>
 *   <li>_ty : Tahitian</li>
 *   <li>_ta : Tamil</li>
 *   <li>_tt : Tatar</li>
 *   <li>_te : Telugu</li>
 *   <li>_tg : Tajik</li>
 *   <li>_tl : Tagalog</li>
 *   <li>_th : Thai</li>
 *   <li>_ti : Tigrinya</li>
 *   <li>_to : Tonga Tonga Islands</li>
 *   <li>_tn : Tswana</li>
 *   <li>_ts : Tsonga</li>
 *   <li>_tk : Turkmen</li>
 *   <li>_tr : Turkish</li>
 *   <li>_tw : Twi</li>
 *   <li>_ug : Uighur; Uyghur</li>
 *   <li>_uk : Ukrainian</li>
 *   <li>_ur : Urdu</li>
 *   <li>_uz : Uzbek</li>
 *   <li>_ve : Venda</li>
 *   <li>_vi : Vietnamese</li>
 *   <li>_vo : Volapk</li>
 *   <li>_wa : Walloon</li>
 *   <li>_wo : Wolof</li>
 *   <li>_xh : Xhosa</li>
 *   <li>_yi : Yiddish</li>
 *   <li>_yo : Yoruba</li>
 *   <li>_za : Zhuang; Chuang</li>
 *   <li>_zu : Zulu</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum HumanLanguage implements VocabularyEntry {
	
	
	/**
	 * Braille
	 */
	_q1 ("q1", "Braille"),
	
	/**
	 * American Sign Language
	 */
	_q2 ("q2", "American Sign Language"),
	
	/**
	 * AustralianSign Language
	 */
	_q3 ("q3", "Australian.Sign Language"),
	
	/**
	 * British Sign Language
	 */
	_q4 ("q4", "British Sign Language"),
	
	/**
	 * Makaton
	 */
	_q5 ("q5", "Makaton"),
	
	/**
	 * Afar
	 */
	_aa ("aa", "Afar"),
	
	/**
	 * Abkhazian
	 */
	_ab ("ab", "Abkhazian"),
	
	/**
	 * Afrikaans
	 */
	_af ("af", "Afrikaans"),
	
	/**
	 * Akan
	 */
	_ak ("ak", "Akan"),
	
	/**
	 * Albanian
	 */
	_sq ("sq", "Albanian"),
	
	/**
	 * Amharic
	 */
	_am ("am", "Amharic"),
	
	/**
	 * Arabic
	 */
	_ar ("ar", "Arabic"),
	
	/**
	 * Aragonese
	 */
	_an ("an", "Aragonese"),
	
	/**
	 * Armenian
	 */
	_hy ("hy", "Armenian"),
	
	/**
	 * Assamese
	 */
	_as ("as", "Assamese"),
	
	/**
	 * Avaric
	 */
	_av ("av", "Avaric"),
	
	/**
	 * Avestan
	 */
	_ae ("ae", "Avestan"),
	
	/**
	 * Aymara
	 */
	_ay ("ay", "Aymara"),
	
	/**
	 * Azerbaijani
	 */
	_az ("az", "Azerbaijani"),
	
	/**
	 * Bashkir
	 */
	_ba ("ba", "Bashkir"),
	
	/**
	 * Bambara
	 */
	_bm ("bm", "Bambara"),
	
	/**
	 * Basque
	 */
	_eu ("eu", "Basque"),
	
	/**
	 * Belarusian
	 */
	_be ("be", "Belarusian"),
	
	/**
	 * Bengali
	 */
	_bn ("bn", "Bengali"),
	
	/**
	 * Bihari
	 */
	_bh ("bh", "Bihari"),
	
	/**
	 * Bislama
	 */
	_bi ("bi", "Bislama"),
	
	/**
	 * Tibetan
	 */
	_bo ("bo", "Tibetan"),
	
	/**
	 * Bosnian
	 */
	_bs ("bs", "Bosnian"),
	
	/**
	 * Breton
	 */
	_br ("br", "Breton"),
	
	/**
	 * Bulgarian
	 */
	_bg ("bg", "Bulgarian"),
	
	/**
	 * Burmese
	 */
	_my ("my", "Burmese"),
	
	/**
	 * Catalan; Valencian
	 */
	_ca ("ca", "Catalan; Valencian"),
	
	/**
	 * Czech
	 */
	_cs ("cs", "Czech"),
	
	/**
	 * Chamorro
	 */
	_ch ("ch", "Chamorro"),
	
	/**
	 * Chechen
	 */
	_ce ("ce", "Chechen"),
	
	/**
	 * Chinese
	 */
	_zh ("zh", "Chinese"),
	
	/**
	 * Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic
	 */
	_cu ("cu", "Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic"),
	
	/**
	 * Chuvash
	 */
	_cv ("cv", "Chuvash"),
	
	/**
	 * Cornish
	 */
	_kw ("kw", "Cornish"),
	
	/**
	 * Corsican
	 */
	_co ("co", "Corsican"),
	
	/**
	 * Cree
	 */
	_cr ("cr", "Cree"),
	
	/**
	 * Welsh
	 */
	_cy ("cy", "Welsh"),
	
	/**
	 * Danish
	 */
	_da ("da", "Danish"),
	
	/**
	 * German
	 */
	_de ("de", "German"),
	
	/**
	 * Divehi; Dhivehi; Maldivian
	 */
	_dv ("dv", "Divehi; Dhivehi; Maldivian"),
	
	/**
	 * Dutch; Flemish
	 */
	_nl ("nl", "Dutch; Flemish"),
	
	/**
	 * Dzongkha
	 */
	_dz ("dz", "Dzongkha"),
	
	/**
	 * Greek, Modern 1453-
	 */
	_el ("el", "Greek, Modern (1453-)"),
	
	/**
	 * English
	 */
	_en ("en", "English"),
	
	/**
	 * Esperanto
	 */
	_eo ("eo", "Esperanto"),
	
	/**
	 * Estonian
	 */
	_et ("et", "Estonian"),
	
	/**
	 * Ewe
	 */
	_ee ("ee", "Ewe"),
	
	/**
	 * Faroese
	 */
	_fo ("fo", "Faroese"),
	
	/**
	 * Persian
	 */
	_fa ("fa", "Persian"),
	
	/**
	 * Fijian
	 */
	_fj ("fj", "Fijian"),
	
	/**
	 * Finnish
	 */
	_fi ("fi", "Finnish"),
	
	/**
	 * French
	 */
	_fr ("fr", "French"),
	
	/**
	 * Western Frisian
	 */
	_fy ("fy", "Western Frisian"),
	
	/**
	 * Fulah
	 */
	_ff ("ff", "Fulah"),
	
	/**
	 * Georgian
	 */
	_ka ("ka", "Georgian"),
	
	/**
	 * Gaelic; Scottish Gaelic
	 */
	_gd ("gd", "Gaelic; Scottish Gaelic"),
	
	/**
	 * Irish
	 */
	_ga ("ga", "Irish"),
	
	/**
	 * Galician
	 */
	_gl ("gl", "Galician"),
	
	/**
	 * Manx
	 */
	_gv ("gv", "Manx"),
	
	/**
	 * Guarani
	 */
	_gn ("gn", "Guarani"),
	
	/**
	 * Gujarati
	 */
	_gu ("gu", "Gujarati"),
	
	/**
	 * Haitian; Haitian Creole
	 */
	_ht ("ht", "Haitian; Haitian Creole"),
	
	/**
	 * Hausa
	 */
	_ha ("ha", "Hausa"),
	
	/**
	 * Hebrew
	 */
	_he ("he", "Hebrew"),
	
	/**
	 * Herero
	 */
	_hz ("hz", "Herero"),
	
	/**
	 * Hindi
	 */
	_hi ("hi", "Hindi"),
	
	/**
	 * Hiri Motu
	 */
	_ho ("ho", "Hiri Motu"),
	
	/**
	 * Croatian
	 */
	_hr ("hr", "Croatian"),
	
	/**
	 * Hungarian
	 */
	_hu ("hu", "Hungarian"),
	
	/**
	 * Igbo
	 */
	_ig ("ig", "Igbo"),
	
	/**
	 * Icelandic
	 */
	_is ("is", "Icelandic"),
	
	/**
	 * Ido
	 */
	_io ("io", "Ido"),
	
	/**
	 * Sichuan Yi
	 */
	_ii ("ii", "Sichuan Yi"),
	
	/**
	 * Inuktitut
	 */
	_iu ("iu", "Inuktitut"),
	
	/**
	 * Interlingue
	 */
	_ie ("ie", "Interlingue"),
	
	/**
	 * Interlingua International Auxiliary Language Association
	 */
	_ia ("ia", "Interlingua (International Auxiliary Language Association)"),
	
	/**
	 * Indonesian
	 */
	_id ("id", "Indonesian"),
	
	/**
	 * Inupiaq
	 */
	_ik ("ik", "Inupiaq"),
	
	/**
	 * Italian
	 */
	_it ("it", "Italian"),
	
	/**
	 * Javanese
	 */
	_jv ("jv", "Javanese"),
	
	/**
	 * Japanese
	 */
	_ja ("ja", "Japanese"),
	
	/**
	 * Kalaallisut; Greenlandic
	 */
	_kl ("kl", "Kalaallisut; Greenlandic"),
	
	/**
	 * Kannada
	 */
	_kn ("kn", "Kannada"),
	
	/**
	 * Kashmiri
	 */
	_ks ("ks", "Kashmiri"),
	
	/**
	 * Kanuri
	 */
	_kr ("kr", "Kanuri"),
	
	/**
	 * Kazakh
	 */
	_kk ("kk", "Kazakh"),
	
	/**
	 * Central Khmer
	 */
	_km ("km", "Central Khmer"),
	
	/**
	 * Kikuyu; Gikuyu
	 */
	_ki ("ki", "Kikuyu; Gikuyu"),
	
	/**
	 * Kinyarwanda
	 */
	_rw ("rw", "Kinyarwanda"),
	
	/**
	 * Kirghiz; Kyrgyz
	 */
	_ky ("ky", "Kirghiz; Kyrgyz"),
	
	/**
	 * Komi
	 */
	_kv ("kv", "Komi"),
	
	/**
	 * Kongo
	 */
	_kg ("kg", "Kongo"),
	
	/**
	 * Korean
	 */
	_ko ("ko", "Korean"),
	
	/**
	 * Kuanyama; Kwanyama
	 */
	_kj ("kj", "Kuanyama; Kwanyama"),
	
	/**
	 * Kurdish
	 */
	_ku ("ku", "Kurdish"),
	
	/**
	 * Lao
	 */
	_lo ("lo", "Lao"),
	
	/**
	 * Latin
	 */
	_la ("la", "Latin"),
	
	/**
	 * Latvian
	 */
	_lv ("lv", "Latvian"),
	
	/**
	 * Limburgan; Limburger; Limburgish
	 */
	_li ("li", "Limburgan; Limburger; Limburgish"),
	
	/**
	 * Lingala
	 */
	_ln ("ln", "Lingala"),
	
	/**
	 * Lithuanian
	 */
	_lt ("lt", "Lithuanian"),
	
	/**
	 * Luxembourgish; Letzeburgesch
	 */
	_lb ("lb", "Luxembourgish; Letzeburgesch"),
	
	/**
	 * Luba-Katanga
	 */
	_lu ("lu", "Luba-Katanga"),
	
	/**
	 * Ganda
	 */
	_lg ("lg", "Ganda"),
	
	/**
	 * Macedonian
	 */
	_mk ("mk", "Macedonian"),
	
	/**
	 * Marshallese
	 */
	_mh ("mh", "Marshallese"),
	
	/**
	 * Malayalam
	 */
	_ml ("ml", "Malayalam"),
	
	/**
	 * Maori
	 */
	_mi ("mi", "Maori"),
	
	/**
	 * Marathi
	 */
	_mr ("mr", "Marathi"),
	
	/**
	 * Malay
	 */
	_ms ("ms", "Malay"),
	
	/**
	 * Malagasy
	 */
	_mg ("mg", "Malagasy"),
	
	/**
	 * Maltese
	 */
	_mt ("mt", "Maltese"),
	
	/**
	 * Moldavian
	 */
	_mo ("mo", "Moldavian"),
	
	/**
	 * Mongolian
	 */
	_mn ("mn", "Mongolian"),
	
	/**
	 * Nauru
	 */
	_na ("na", "Nauru"),
	
	/**
	 * Navajo; Navaho
	 */
	_nv ("nv", "Navajo; Navaho"),
	
	/**
	 * Ndebele, South; South Ndebele
	 */
	_nr ("nr", "Ndebele, South; South Ndebele"),
	
	/**
	 * Ndebele, North; North Ndebele
	 */
	_nd ("nd", "Ndebele, North; North Ndebele"),
	
	/**
	 * Ndonga
	 */
	_ng ("ng", "Ndonga"),
	
	/**
	 * Nepali
	 */
	_ne ("ne", "Nepali"),
	
	/**
	 * Norwegian Nynorsk; Nynorsk, Norwegian
	 */
	_nn ("nn", "Norwegian Nynorsk; Nynorsk, Norwegian"),
	
	/**
	 * Bokml, Norwegian; Norwegian Bokml
	 */
	_nb ("nb", "Bokmål, Norwegian; Norwegian Bokmål"),
	
	/**
	 * Norwegian
	 */
	_no ("no", "Norwegian"),
	
	/**
	 * Chichewa; Chewa; Nyanja
	 */
	_ny ("ny", "Chichewa; Chewa; Nyanja"),
	
	/**
	 * Occitan post1500; Provenal
	 */
	_oc ("oc", "Occitan (post1500); Provençal"),
	
	/**
	 * Ojibwa
	 */
	_oj ("oj", "Ojibwa"),
	
	/**
	 * Oriya
	 */
	_or ("or", "Oriya"),
	
	/**
	 * Oromo
	 */
	_om ("om", "Oromo"),
	
	/**
	 * Ossetian; Ossetic
	 */
	_os ("os", "Ossetian; Ossetic"),
	
	/**
	 * Panjabi; Punjabi
	 */
	_pa ("pa", "Panjabi; Punjabi"),
	
	/**
	 * Pali
	 */
	_pi ("pi", "Pali"),
	
	/**
	 * Polish
	 */
	_pl ("pl", "Polish"),
	
	/**
	 * Portuguese
	 */
	_pt ("pt", "Portuguese"),
	
	/**
	 * Pushto
	 */
	_ps ("ps", "Pushto"),
	
	/**
	 * Quechua
	 */
	_qu ("qu", "Quechua"),
	
	/**
	 * Romansh
	 */
	_rm ("rm", "Romansh"),
	
	/**
	 * Romanian
	 */
	_ro ("ro", "Romanian"),
	
	/**
	 * Rundi
	 */
	_rn ("rn", "Rundi"),
	
	/**
	 * Russian
	 */
	_ru ("ru", "Russian"),
	
	/**
	 * Sango
	 */
	_sg ("sg", "Sango"),
	
	/**
	 * Sanskrit
	 */
	_sa ("sa", "Sanskrit"),
	
	/**
	 * Serbian
	 */
	_sr ("sr", "Serbian"),
	
	/**
	 * Sinhala; Sinhalese
	 */
	_si ("si", "Sinhala; Sinhalese"),
	
	/**
	 * Slovak
	 */
	_sk ("sk", "Slovak"),
	
	/**
	 * Slovenian
	 */
	_sl ("sl", "Slovenian"),
	
	/**
	 * Northern Sami
	 */
	_se ("se", "Northern Sami"),
	
	/**
	 * Samoan
	 */
	_sm ("sm", "Samoan"),
	
	/**
	 * Shona
	 */
	_sn ("sn", "Shona"),
	
	/**
	 * Sindhi
	 */
	_sd ("sd", "Sindhi"),
	
	/**
	 * Somali
	 */
	_so ("so", "Somali"),
	
	/**
	 * Sotho, Southern
	 */
	_st ("st", "Sotho, Southern"),
	
	/**
	 * Spanish; Castilian
	 */
	_es ("es", "Spanish; Castilian"),
	
	/**
	 * Sardinian
	 */
	_sc ("sc", "Sardinian"),
	
	/**
	 * Swati
	 */
	_ss ("ss", "Swati"),
	
	/**
	 * Sundanese
	 */
	_su ("su", "Sundanese"),
	
	/**
	 * Swahili
	 */
	_sw ("sw", "Swahili"),
	
	/**
	 * Swedish
	 */
	_sv ("sv", "Swedish"),
	
	/**
	 * Tahitian
	 */
	_ty ("ty", "Tahitian"),
	
	/**
	 * Tamil
	 */
	_ta ("ta", "Tamil"),
	
	/**
	 * Tatar
	 */
	_tt ("tt", "Tatar"),
	
	/**
	 * Telugu
	 */
	_te ("te", "Telugu"),
	
	/**
	 * Tajik
	 */
	_tg ("tg", "Tajik"),
	
	/**
	 * Tagalog
	 */
	_tl ("tl", "Tagalog"),
	
	/**
	 * Thai
	 */
	_th ("th", "Thai"),
	
	/**
	 * Tigrinya
	 */
	_ti ("ti", "Tigrinya"),
	
	/**
	 * Tonga Tonga Islands
	 */
	_to ("to", "Tonga (Tonga Islands)"),
	
	/**
	 * Tswana
	 */
	_tn ("tn", "Tswana"),
	
	/**
	 * Tsonga
	 */
	_ts ("ts", "Tsonga"),
	
	/**
	 * Turkmen
	 */
	_tk ("tk", "Turkmen"),
	
	/**
	 * Turkish
	 */
	_tr ("tr", "Turkish"),
	
	/**
	 * Twi
	 */
	_tw ("tw", "Twi"),
	
	/**
	 * Uighur; Uyghur
	 */
	_ug ("ug", "Uighur; Uyghur"),
	
	/**
	 * Ukrainian
	 */
	_uk ("uk", "Ukrainian"),
	
	/**
	 * Urdu
	 */
	_ur ("ur", "Urdu"),
	
	/**
	 * Uzbek
	 */
	_uz ("uz", "Uzbek"),
	
	/**
	 * Venda
	 */
	_ve ("ve", "Venda"),
	
	/**
	 * Vietnamese
	 */
	_vi ("vi", "Vietnamese"),
	
	/**
	 * Volapk
	 */
	_vo ("vo", "Volapük"),
	
	/**
	 * Walloon
	 */
	_wa ("wa", "Walloon"),
	
	/**
	 * Wolof
	 */
	_wo ("wo", "Wolof"),
	
	/**
	 * Xhosa
	 */
	_xh ("xh", "Xhosa"),
	
	/**
	 * Yiddish
	 */
	_yi ("yi", "Yiddish"),
	
	/**
	 * Yoruba
	 */
	_yo ("yo", "Yoruba"),
	
	/**
	 * Zhuang; Chuang
	 */
	_za ("za", "Zhuang; Chuang"),
	
	/**
	 * Zulu
	 */
	_zu ("zu", "Zulu"),
	
	/**
	 * Braille
	 */
	_Braille ("q1", "Braille"),
	
	/**
	 * American Sign Language
	 */
	_AmericanSignLanguage ("q2", "American Sign Language"),
	
	/**
	 * AustralianSign Language
	 */
	_AustralianSignLanguage ("q3", "Australian.Sign Language"),
	
	/**
	 * British Sign Language
	 */
	_BritishSignLanguage ("q4", "British Sign Language"),
	
	/**
	 * Makaton
	 */
	_Makaton ("q5", "Makaton"),
	
	/**
	 * Afar
	 */
	_Afar ("aa", "Afar"),
	
	/**
	 * Abkhazian
	 */
	_Abkhazian ("ab", "Abkhazian"),
	
	/**
	 * Afrikaans
	 */
	_Afrikaans ("af", "Afrikaans"),
	
	/**
	 * Akan
	 */
	_Akan ("ak", "Akan"),
	
	/**
	 * Albanian
	 */
	_Albanian ("sq", "Albanian"),
	
	/**
	 * Amharic
	 */
	_Amharic ("am", "Amharic"),
	
	/**
	 * Arabic
	 */
	_Arabic ("ar", "Arabic"),
	
	/**
	 * Aragonese
	 */
	_Aragonese ("an", "Aragonese"),
	
	/**
	 * Armenian
	 */
	_Armenian ("hy", "Armenian"),
	
	/**
	 * Assamese
	 */
	_Assamese ("as", "Assamese"),
	
	/**
	 * Avaric
	 */
	_Avaric ("av", "Avaric"),
	
	/**
	 * Avestan
	 */
	_Avestan ("ae", "Avestan"),
	
	/**
	 * Aymara
	 */
	_Aymara ("ay", "Aymara"),
	
	/**
	 * Azerbaijani
	 */
	_Azerbaijani ("az", "Azerbaijani"),
	
	/**
	 * Bashkir
	 */
	_Bashkir ("ba", "Bashkir"),
	
	/**
	 * Bambara
	 */
	_Bambara ("bm", "Bambara"),
	
	/**
	 * Basque
	 */
	_Basque ("eu", "Basque"),
	
	/**
	 * Belarusian
	 */
	_Belarusian ("be", "Belarusian"),
	
	/**
	 * Bengali
	 */
	_Bengali ("bn", "Bengali"),
	
	/**
	 * Bihari
	 */
	_Bihari ("bh", "Bihari"),
	
	/**
	 * Bislama
	 */
	_Bislama ("bi", "Bislama"),
	
	/**
	 * Tibetan
	 */
	_Tibetan ("bo", "Tibetan"),
	
	/**
	 * Bosnian
	 */
	_Bosnian ("bs", "Bosnian"),
	
	/**
	 * Breton
	 */
	_Breton ("br", "Breton"),
	
	/**
	 * Bulgarian
	 */
	_Bulgarian ("bg", "Bulgarian"),
	
	/**
	 * Burmese
	 */
	_Burmese ("my", "Burmese"),
	
	/**
	 * Catalan; Valencian
	 */
	_CatalanValencian ("ca", "Catalan; Valencian"),
	
	/**
	 * Czech
	 */
	_Czech ("cs", "Czech"),
	
	/**
	 * Chamorro
	 */
	_Chamorro ("ch", "Chamorro"),
	
	/**
	 * Chechen
	 */
	_Chechen ("ce", "Chechen"),
	
	/**
	 * Chinese
	 */
	_Chinese ("zh", "Chinese"),
	
	/**
	 * Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic
	 */
	_ChurchSlavicOldSlavonicChurchSlavonicOldBulgarianOldChurchSlavonic ("cu", "Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic"),
	
	/**
	 * Chuvash
	 */
	_Chuvash ("cv", "Chuvash"),
	
	/**
	 * Cornish
	 */
	_Cornish ("kw", "Cornish"),
	
	/**
	 * Corsican
	 */
	_Corsican ("co", "Corsican"),
	
	/**
	 * Cree
	 */
	_Cree ("cr", "Cree"),
	
	/**
	 * Welsh
	 */
	_Welsh ("cy", "Welsh"),
	
	/**
	 * Danish
	 */
	_Danish ("da", "Danish"),
	
	/**
	 * German
	 */
	_German ("de", "German"),
	
	/**
	 * Divehi; Dhivehi; Maldivian
	 */
	_DivehiDhivehiMaldivian ("dv", "Divehi; Dhivehi; Maldivian"),
	
	/**
	 * Dutch; Flemish
	 */
	_DutchFlemish ("nl", "Dutch; Flemish"),
	
	/**
	 * Dzongkha
	 */
	_Dzongkha ("dz", "Dzongkha"),
	
	/**
	 * Greek, Modern 1453-
	 */
	_GreekModern1453 ("el", "Greek, Modern (1453-)"),
	
	/**
	 * English
	 */
	_English ("en", "English"),
	
	/**
	 * Esperanto
	 */
	_Esperanto ("eo", "Esperanto"),
	
	/**
	 * Estonian
	 */
	_Estonian ("et", "Estonian"),
	
	/**
	 * Ewe
	 */
	_Ewe ("ee", "Ewe"),
	
	/**
	 * Faroese
	 */
	_Faroese ("fo", "Faroese"),
	
	/**
	 * Persian
	 */
	_Persian ("fa", "Persian"),
	
	/**
	 * Fijian
	 */
	_Fijian ("fj", "Fijian"),
	
	/**
	 * Finnish
	 */
	_Finnish ("fi", "Finnish"),
	
	/**
	 * French
	 */
	_French ("fr", "French"),
	
	/**
	 * Western Frisian
	 */
	_WesternFrisian ("fy", "Western Frisian"),
	
	/**
	 * Fulah
	 */
	_Fulah ("ff", "Fulah"),
	
	/**
	 * Georgian
	 */
	_Georgian ("ka", "Georgian"),
	
	/**
	 * Gaelic; Scottish Gaelic
	 */
	_GaelicScottishGaelic ("gd", "Gaelic; Scottish Gaelic"),
	
	/**
	 * Irish
	 */
	_Irish ("ga", "Irish"),
	
	/**
	 * Galician
	 */
	_Galician ("gl", "Galician"),
	
	/**
	 * Manx
	 */
	_Manx ("gv", "Manx"),
	
	/**
	 * Guarani
	 */
	_Guarani ("gn", "Guarani"),
	
	/**
	 * Gujarati
	 */
	_Gujarati ("gu", "Gujarati"),
	
	/**
	 * Haitian; Haitian Creole
	 */
	_HaitianHaitianCreole ("ht", "Haitian; Haitian Creole"),
	
	/**
	 * Hausa
	 */
	_Hausa ("ha", "Hausa"),
	
	/**
	 * Hebrew
	 */
	_Hebrew ("he", "Hebrew"),
	
	/**
	 * Herero
	 */
	_Herero ("hz", "Herero"),
	
	/**
	 * Hindi
	 */
	_Hindi ("hi", "Hindi"),
	
	/**
	 * Hiri Motu
	 */
	_HiriMotu ("ho", "Hiri Motu"),
	
	/**
	 * Croatian
	 */
	_Croatian ("hr", "Croatian"),
	
	/**
	 * Hungarian
	 */
	_Hungarian ("hu", "Hungarian"),
	
	/**
	 * Igbo
	 */
	_Igbo ("ig", "Igbo"),
	
	/**
	 * Icelandic
	 */
	_Icelandic ("is", "Icelandic"),
	
	/**
	 * Ido
	 */
	_Ido ("io", "Ido"),
	
	/**
	 * Sichuan Yi
	 */
	_SichuanYi ("ii", "Sichuan Yi"),
	
	/**
	 * Inuktitut
	 */
	_Inuktitut ("iu", "Inuktitut"),
	
	/**
	 * Interlingue
	 */
	_Interlingue ("ie", "Interlingue"),
	
	/**
	 * Interlingua International Auxiliary Language Association
	 */
	_InterlinguaInternationalAuxiliaryLanguageAssociation ("ia", "Interlingua (International Auxiliary Language Association)"),
	
	/**
	 * Indonesian
	 */
	_Indonesian ("id", "Indonesian"),
	
	/**
	 * Inupiaq
	 */
	_Inupiaq ("ik", "Inupiaq"),
	
	/**
	 * Italian
	 */
	_Italian ("it", "Italian"),
	
	/**
	 * Javanese
	 */
	_Javanese ("jv", "Javanese"),
	
	/**
	 * Japanese
	 */
	_Japanese ("ja", "Japanese"),
	
	/**
	 * Kalaallisut; Greenlandic
	 */
	_KalaallisutGreenlandic ("kl", "Kalaallisut; Greenlandic"),
	
	/**
	 * Kannada
	 */
	_Kannada ("kn", "Kannada"),
	
	/**
	 * Kashmiri
	 */
	_Kashmiri ("ks", "Kashmiri"),
	
	/**
	 * Kanuri
	 */
	_Kanuri ("kr", "Kanuri"),
	
	/**
	 * Kazakh
	 */
	_Kazakh ("kk", "Kazakh"),
	
	/**
	 * Central Khmer
	 */
	_CentralKhmer ("km", "Central Khmer"),
	
	/**
	 * Kikuyu; Gikuyu
	 */
	_KikuyuGikuyu ("ki", "Kikuyu; Gikuyu"),
	
	/**
	 * Kinyarwanda
	 */
	_Kinyarwanda ("rw", "Kinyarwanda"),
	
	/**
	 * Kirghiz; Kyrgyz
	 */
	_KirghizKyrgyz ("ky", "Kirghiz; Kyrgyz"),
	
	/**
	 * Komi
	 */
	_Komi ("kv", "Komi"),
	
	/**
	 * Kongo
	 */
	_Kongo ("kg", "Kongo"),
	
	/**
	 * Korean
	 */
	_Korean ("ko", "Korean"),
	
	/**
	 * Kuanyama; Kwanyama
	 */
	_KuanyamaKwanyama ("kj", "Kuanyama; Kwanyama"),
	
	/**
	 * Kurdish
	 */
	_Kurdish ("ku", "Kurdish"),
	
	/**
	 * Lao
	 */
	_Lao ("lo", "Lao"),
	
	/**
	 * Latin
	 */
	_Latin ("la", "Latin"),
	
	/**
	 * Latvian
	 */
	_Latvian ("lv", "Latvian"),
	
	/**
	 * Limburgan; Limburger; Limburgish
	 */
	_LimburganLimburgerLimburgish ("li", "Limburgan; Limburger; Limburgish"),
	
	/**
	 * Lingala
	 */
	_Lingala ("ln", "Lingala"),
	
	/**
	 * Lithuanian
	 */
	_Lithuanian ("lt", "Lithuanian"),
	
	/**
	 * Luxembourgish; Letzeburgesch
	 */
	_LuxembourgishLetzeburgesch ("lb", "Luxembourgish; Letzeburgesch"),
	
	/**
	 * Luba-Katanga
	 */
	_LubaKatanga ("lu", "Luba-Katanga"),
	
	/**
	 * Ganda
	 */
	_Ganda ("lg", "Ganda"),
	
	/**
	 * Macedonian
	 */
	_Macedonian ("mk", "Macedonian"),
	
	/**
	 * Marshallese
	 */
	_Marshallese ("mh", "Marshallese"),
	
	/**
	 * Malayalam
	 */
	_Malayalam ("ml", "Malayalam"),
	
	/**
	 * Maori
	 */
	_Maori ("mi", "Maori"),
	
	/**
	 * Marathi
	 */
	_Marathi ("mr", "Marathi"),
	
	/**
	 * Malay
	 */
	_Malay ("ms", "Malay"),
	
	/**
	 * Malagasy
	 */
	_Malagasy ("mg", "Malagasy"),
	
	/**
	 * Maltese
	 */
	_Maltese ("mt", "Maltese"),
	
	/**
	 * Moldavian
	 */
	_Moldavian ("mo", "Moldavian"),
	
	/**
	 * Mongolian
	 */
	_Mongolian ("mn", "Mongolian"),
	
	/**
	 * Nauru
	 */
	_Nauru ("na", "Nauru"),
	
	/**
	 * Navajo; Navaho
	 */
	_NavajoNavaho ("nv", "Navajo; Navaho"),
	
	/**
	 * Ndebele, South; South Ndebele
	 */
	_NdebeleSouthSouthNdebele ("nr", "Ndebele, South; South Ndebele"),
	
	/**
	 * Ndebele, North; North Ndebele
	 */
	_NdebeleNorthNorthNdebele ("nd", "Ndebele, North; North Ndebele"),
	
	/**
	 * Ndonga
	 */
	_Ndonga ("ng", "Ndonga"),
	
	/**
	 * Nepali
	 */
	_Nepali ("ne", "Nepali"),
	
	/**
	 * Norwegian Nynorsk; Nynorsk, Norwegian
	 */
	_NorwegianNynorskNynorskNorwegian ("nn", "Norwegian Nynorsk; Nynorsk, Norwegian"),
	
	/**
	 * Bokml, Norwegian; Norwegian Bokml
	 */
	_BokmlNorwegianNorwegianBokml ("nb", "Bokmål, Norwegian; Norwegian Bokmål"),
	
	/**
	 * Norwegian
	 */
	_Norwegian ("no", "Norwegian"),
	
	/**
	 * Chichewa; Chewa; Nyanja
	 */
	_ChichewaChewaNyanja ("ny", "Chichewa; Chewa; Nyanja"),
	
	/**
	 * Occitan post1500; Provenal
	 */
	_Occitanpost1500Provenal ("oc", "Occitan (post1500); Provençal"),
	
	/**
	 * Ojibwa
	 */
	_Ojibwa ("oj", "Ojibwa"),
	
	/**
	 * Oriya
	 */
	_Oriya ("or", "Oriya"),
	
	/**
	 * Oromo
	 */
	_Oromo ("om", "Oromo"),
	
	/**
	 * Ossetian; Ossetic
	 */
	_OssetianOssetic ("os", "Ossetian; Ossetic"),
	
	/**
	 * Panjabi; Punjabi
	 */
	_PanjabiPunjabi ("pa", "Panjabi; Punjabi"),
	
	/**
	 * Pali
	 */
	_Pali ("pi", "Pali"),
	
	/**
	 * Polish
	 */
	_Polish ("pl", "Polish"),
	
	/**
	 * Portuguese
	 */
	_Portuguese ("pt", "Portuguese"),
	
	/**
	 * Pushto
	 */
	_Pushto ("ps", "Pushto"),
	
	/**
	 * Quechua
	 */
	_Quechua ("qu", "Quechua"),
	
	/**
	 * Romansh
	 */
	_Romansh ("rm", "Romansh"),
	
	/**
	 * Romanian
	 */
	_Romanian ("ro", "Romanian"),
	
	/**
	 * Rundi
	 */
	_Rundi ("rn", "Rundi"),
	
	/**
	 * Russian
	 */
	_Russian ("ru", "Russian"),
	
	/**
	 * Sango
	 */
	_Sango ("sg", "Sango"),
	
	/**
	 * Sanskrit
	 */
	_Sanskrit ("sa", "Sanskrit"),
	
	/**
	 * Serbian
	 */
	_Serbian ("sr", "Serbian"),
	
	/**
	 * Sinhala; Sinhalese
	 */
	_SinhalaSinhalese ("si", "Sinhala; Sinhalese"),
	
	/**
	 * Slovak
	 */
	_Slovak ("sk", "Slovak"),
	
	/**
	 * Slovenian
	 */
	_Slovenian ("sl", "Slovenian"),
	
	/**
	 * Northern Sami
	 */
	_NorthernSami ("se", "Northern Sami"),
	
	/**
	 * Samoan
	 */
	_Samoan ("sm", "Samoan"),
	
	/**
	 * Shona
	 */
	_Shona ("sn", "Shona"),
	
	/**
	 * Sindhi
	 */
	_Sindhi ("sd", "Sindhi"),
	
	/**
	 * Somali
	 */
	_Somali ("so", "Somali"),
	
	/**
	 * Sotho, Southern
	 */
	_SothoSouthern ("st", "Sotho, Southern"),
	
	/**
	 * Spanish; Castilian
	 */
	_SpanishCastilian ("es", "Spanish; Castilian"),
	
	/**
	 * Sardinian
	 */
	_Sardinian ("sc", "Sardinian"),
	
	/**
	 * Swati
	 */
	_Swati ("ss", "Swati"),
	
	/**
	 * Sundanese
	 */
	_Sundanese ("su", "Sundanese"),
	
	/**
	 * Swahili
	 */
	_Swahili ("sw", "Swahili"),
	
	/**
	 * Swedish
	 */
	_Swedish ("sv", "Swedish"),
	
	/**
	 * Tahitian
	 */
	_Tahitian ("ty", "Tahitian"),
	
	/**
	 * Tamil
	 */
	_Tamil ("ta", "Tamil"),
	
	/**
	 * Tatar
	 */
	_Tatar ("tt", "Tatar"),
	
	/**
	 * Telugu
	 */
	_Telugu ("te", "Telugu"),
	
	/**
	 * Tajik
	 */
	_Tajik ("tg", "Tajik"),
	
	/**
	 * Tagalog
	 */
	_Tagalog ("tl", "Tagalog"),
	
	/**
	 * Thai
	 */
	_Thai ("th", "Thai"),
	
	/**
	 * Tigrinya
	 */
	_Tigrinya ("ti", "Tigrinya"),
	
	/**
	 * Tonga Tonga Islands
	 */
	_TongaTongaIslands ("to", "Tonga (Tonga Islands)"),
	
	/**
	 * Tswana
	 */
	_Tswana ("tn", "Tswana"),
	
	/**
	 * Tsonga
	 */
	_Tsonga ("ts", "Tsonga"),
	
	/**
	 * Turkmen
	 */
	_Turkmen ("tk", "Turkmen"),
	
	/**
	 * Turkish
	 */
	_Turkish ("tr", "Turkish"),
	
	/**
	 * Twi
	 */
	_Twi ("tw", "Twi"),
	
	/**
	 * Uighur; Uyghur
	 */
	_UighurUyghur ("ug", "Uighur; Uyghur"),
	
	/**
	 * Ukrainian
	 */
	_Ukrainian ("uk", "Ukrainian"),
	
	/**
	 * Urdu
	 */
	_Urdu ("ur", "Urdu"),
	
	/**
	 * Uzbek
	 */
	_Uzbek ("uz", "Uzbek"),
	
	/**
	 * Venda
	 */
	_Venda ("ve", "Venda"),
	
	/**
	 * Vietnamese
	 */
	_Vietnamese ("vi", "Vietnamese"),
	
	/**
	 * Volapk
	 */
	_Volapk ("vo", "Volapük"),
	
	/**
	 * Walloon
	 */
	_Walloon ("wa", "Walloon"),
	
	/**
	 * Wolof
	 */
	_Wolof ("wo", "Wolof"),
	
	/**
	 * Xhosa
	 */
	_Xhosa ("xh", "Xhosa"),
	
	/**
	 * Yiddish
	 */
	_Yiddish ("yi", "Yiddish"),
	
	/**
	 * Yoruba
	 */
	_Yoruba ("yo", "Yoruba"),
	
	/**
	 * Zhuang; Chuang
	 */
	_ZhuangChuang ("za", "Zhuang; Chuang"),
	
	/**
	 * Zulu
	 */
	_Zulu ("zu", "Zulu"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.70";

	HumanLanguage(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static HumanLanguage getByCode(String code) {
		HumanLanguage[] vals = values();
		for (HumanLanguage val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(HumanLanguage other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	