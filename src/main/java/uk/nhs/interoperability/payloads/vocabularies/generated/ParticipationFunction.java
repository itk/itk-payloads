/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the ParticipationFunction vocabulary:
 * <ul>
 *   <li>_ADMPHYS : admitting physician</li>
 *   <li>_ANEST : anesthesist</li>
 *   <li>_ANRS : anesthesia nurse</li>
 *   <li>_ATTPHYS : attending physician</li>
 *   <li>_DISPHYS : discharging physician</li>
 *   <li>_FASST : first assistant surgeon</li>
 *   <li>_MDWF : midwife</li>
 *   <li>_NASST : nurse assistant</li>
 *   <li>_PCP : primary care physician</li>
 *   <li>_PRISURG : primary surgeon</li>
 *   <li>_RNDPHYS : rounding physician</li>
 *   <li>_SASST : second assistant surgeon</li>
 *   <li>_SNRS : scrub nurse</li>
 *   <li>_TASST : third assistant</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum ParticipationFunction implements VocabularyEntry {
	
	
	/**
	 * admitting physician
	 */
	_ADMPHYS ("ADMPHYS", "admitting physician"),
	
	/**
	 * anesthesist
	 */
	_ANEST ("ANEST", "anesthesist"),
	
	/**
	 * anesthesia nurse
	 */
	_ANRS ("ANRS", "anesthesia nurse"),
	
	/**
	 * attending physician
	 */
	_ATTPHYS ("ATTPHYS", "attending physician"),
	
	/**
	 * discharging physician
	 */
	_DISPHYS ("DISPHYS", "discharging physician"),
	
	/**
	 * first assistant surgeon
	 */
	_FASST ("FASST", "first assistant surgeon"),
	
	/**
	 * midwife
	 */
	_MDWF ("MDWF", "midwife"),
	
	/**
	 * nurse assistant
	 */
	_NASST ("NASST", "nurse assistant"),
	
	/**
	 * primary care physician
	 */
	_PCP ("PCP", "primary care physician"),
	
	/**
	 * primary surgeon
	 */
	_PRISURG ("PRISURG", "primary surgeon"),
	
	/**
	 * rounding physician
	 */
	_RNDPHYS ("RNDPHYS", "rounding physician"),
	
	/**
	 * second assistant surgeon
	 */
	_SASST ("SASST", "second assistant surgeon"),
	
	/**
	 * scrub nurse
	 */
	_SNRS ("SNRS", "scrub nurse"),
	
	/**
	 * third assistant
	 */
	_TASST ("TASST", "third assistant"),
	
	/**
	 * admitting physician
	 */
	_admittingphysician ("ADMPHYS", "admitting physician"),
	
	/**
	 * anesthesist
	 */
	_anesthesist ("ANEST", "anesthesist"),
	
	/**
	 * anesthesia nurse
	 */
	_anesthesianurse ("ANRS", "anesthesia nurse"),
	
	/**
	 * attending physician
	 */
	_attendingphysician ("ATTPHYS", "attending physician"),
	
	/**
	 * discharging physician
	 */
	_dischargingphysician ("DISPHYS", "discharging physician"),
	
	/**
	 * first assistant surgeon
	 */
	_firstassistantsurgeon ("FASST", "first assistant surgeon"),
	
	/**
	 * midwife
	 */
	_midwife ("MDWF", "midwife"),
	
	/**
	 * nurse assistant
	 */
	_nurseassistant ("NASST", "nurse assistant"),
	
	/**
	 * primary care physician
	 */
	_primarycarephysician ("PCP", "primary care physician"),
	
	/**
	 * primary surgeon
	 */
	_primarysurgeon ("PRISURG", "primary surgeon"),
	
	/**
	 * rounding physician
	 */
	_roundingphysician ("RNDPHYS", "rounding physician"),
	
	/**
	 * second assistant surgeon
	 */
	_secondassistantsurgeon ("SASST", "second assistant surgeon"),
	
	/**
	 * scrub nurse
	 */
	_scrubnurse ("SNRS", "scrub nurse"),
	
	/**
	 * third assistant
	 */
	_thirdassistant ("TASST", "third assistant"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.5.88";

	ParticipationFunction(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static ParticipationFunction getByCode(String code) {
		ParticipationFunction[] vals = values();
		for (ParticipationFunction val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(ParticipationFunction other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	