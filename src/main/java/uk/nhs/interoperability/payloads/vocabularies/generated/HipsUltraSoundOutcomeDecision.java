/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the HipsUltraSoundOutcomeDecision vocabulary:
 * <ul>
 *   <li>_01 : Refer for expert opinion</li>
 *   <li>_02 : Arrange for follow up 4 weeks later within 8 weeks</li>
 *   <li>_03 : Arrange for follow up 4 weeks later within 12 weeks</li>
 *   <li>_04 : Discharged</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum HipsUltraSoundOutcomeDecision implements VocabularyEntry {
	
	
	/**
	 * Refer for expert opinion
	 */
	_01 ("01", "Refer for expert opinion"),
	
	/**
	 * Arrange for follow up 4 weeks later within 8 weeks
	 */
	_02 ("02", "Arrange for follow up 4 weeks later within 8 weeks"),
	
	/**
	 * Arrange for follow up 4 weeks later within 12 weeks
	 */
	_03 ("03", "Arrange for follow up 4 weeks later within 12 weeks"),
	
	/**
	 * Discharged
	 */
	_04 ("04", "Discharged"),
	
	/**
	 * Refer for expert opinion
	 */
	_Referforexpertopinion ("01", "Refer for expert opinion"),
	
	/**
	 * Arrange for follow up 4 weeks later within 8 weeks
	 */
	_Arrangeforfollowup4weekslaterwithin8weeks ("02", "Arrange for follow up 4 weeks later within 8 weeks"),
	
	/**
	 * Arrange for follow up 4 weeks later within 12 weeks
	 */
	_Arrangeforfollowup4weekslaterwithin12weeks ("03", "Arrange for follow up 4 weeks later within 12 weeks"),
	
	/**
	 * Discharged
	 */
	_Discharged ("04", "Discharged"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.467";

	HipsUltraSoundOutcomeDecision(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static HipsUltraSoundOutcomeDecision getByCode(String code) {
		HipsUltraSoundOutcomeDecision[] vals = values();
		for (HipsUltraSoundOutcomeDecision val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(HipsUltraSoundOutcomeDecision other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	