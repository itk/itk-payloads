/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the NewBornHearingScreeningOutcomeSnCT vocabulary:
 * <ul>
 *   <li>_276781000000109 : Clear response no follow up required</li>
 *   <li>_276791000000106 : Clear response targeted follow-up required</li>
 *   <li>_276811000000107 : No clear response bilateral referral</li>
 *   <li>_276801000000105 : No clear response unilateral referral</li>
 *   <li>_279591000000106 : Incomplete screening contraindicated</li>
 *   <li>_276851000000106 : Incomplete deceased</li>
 *   <li>_279611000000103 : Incomplete baby unsettled</li>
 *   <li>_276821000000101 : Incomplete declined screen</li>
 *   <li>_276831000000104 : Incomplete appointment missed</li>
 *   <li>_279601000000100 : Incomplete lack of service capacity</li>
 *   <li>_276841000000108 : Incomplete lost contact</li>
 *   <li>_276861000000109 : Incomplete out of screening coverage</li>
 *   <li>_276881000000100 : Incomplete late entry</li>
 *   <li>_276891000000103 : Incomplete equipment malfunction</li>
 *   <li>_276901000000102 : Incomplete equipment not available</li>
 *   <li>_276871000000102 : Incomplete withdrew consent</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum NewBornHearingScreeningOutcomeSnCT implements VocabularyEntry {
	
	
	/**
	 * Clear response no follow up required
	 */
	_276781000000109 ("276781000000109", "Clear response no follow up required"),
	
	/**
	 * Clear response targeted follow-up required
	 */
	_276791000000106 ("276791000000106", "Clear response targeted follow-up required"),
	
	/**
	 * No clear response bilateral referral
	 */
	_276811000000107 ("276811000000107", "No clear response bilateral referral"),
	
	/**
	 * No clear response unilateral referral
	 */
	_276801000000105 ("276801000000105", "No clear response unilateral referral"),
	
	/**
	 * Incomplete screening contraindicated
	 */
	_279591000000106 ("279591000000106", "Incomplete screening contraindicated"),
	
	/**
	 * Incomplete deceased
	 */
	_276851000000106 ("276851000000106", "Incomplete deceased"),
	
	/**
	 * Incomplete baby unsettled
	 */
	_279611000000103 ("279611000000103", "Incomplete baby unsettled"),
	
	/**
	 * Incomplete declined screen
	 */
	_276821000000101 ("276821000000101", "Incomplete declined screen"),
	
	/**
	 * Incomplete appointment missed
	 */
	_276831000000104 ("276831000000104", "Incomplete appointment missed"),
	
	/**
	 * Incomplete lack of service capacity
	 */
	_279601000000100 ("279601000000100", "Incomplete lack of service capacity"),
	
	/**
	 * Incomplete lost contact
	 */
	_276841000000108 ("276841000000108", "Incomplete lost contact"),
	
	/**
	 * Incomplete out of screening coverage
	 */
	_276861000000109 ("276861000000109", "Incomplete out of screening coverage"),
	
	/**
	 * Incomplete late entry
	 */
	_276881000000100 ("276881000000100", "Incomplete late entry"),
	
	/**
	 * Incomplete equipment malfunction
	 */
	_276891000000103 ("276891000000103", "Incomplete equipment malfunction"),
	
	/**
	 * Incomplete equipment not available
	 */
	_276901000000102 ("276901000000102", "Incomplete equipment not available"),
	
	/**
	 * Incomplete withdrew consent
	 */
	_276871000000102 ("276871000000102", "Incomplete withdrew consent"),
	
	/**
	 * Clear response no follow up required
	 */
	_Clearresponsenofollowuprequired ("276781000000109", "Clear response no follow up required"),
	
	/**
	 * Clear response targeted follow-up required
	 */
	_Clearresponsetargetedfollowuprequired ("276791000000106", "Clear response targeted follow-up required"),
	
	/**
	 * No clear response bilateral referral
	 */
	_Noclearresponsebilateralreferral ("276811000000107", "No clear response bilateral referral"),
	
	/**
	 * No clear response unilateral referral
	 */
	_Noclearresponseunilateralreferral ("276801000000105", "No clear response unilateral referral"),
	
	/**
	 * Incomplete screening contraindicated
	 */
	_Incompletescreeningcontraindicated ("279591000000106", "Incomplete screening contraindicated"),
	
	/**
	 * Incomplete deceased
	 */
	_Incompletedeceased ("276851000000106", "Incomplete deceased"),
	
	/**
	 * Incomplete baby unsettled
	 */
	_Incompletebabyunsettled ("279611000000103", "Incomplete baby unsettled"),
	
	/**
	 * Incomplete declined screen
	 */
	_Incompletedeclinedscreen ("276821000000101", "Incomplete declined screen"),
	
	/**
	 * Incomplete appointment missed
	 */
	_Incompleteappointmentmissed ("276831000000104", "Incomplete appointment missed"),
	
	/**
	 * Incomplete lack of service capacity
	 */
	_Incompletelackofservicecapacity ("279601000000100", "Incomplete lack of service capacity"),
	
	/**
	 * Incomplete lost contact
	 */
	_Incompletelostcontact ("276841000000108", "Incomplete lost contact"),
	
	/**
	 * Incomplete out of screening coverage
	 */
	_Incompleteoutofscreeningcoverage ("276861000000109", "Incomplete out of screening coverage"),
	
	/**
	 * Incomplete late entry
	 */
	_Incompletelateentry ("276881000000100", "Incomplete late entry"),
	
	/**
	 * Incomplete equipment malfunction
	 */
	_Incompleteequipmentmalfunction ("276891000000103", "Incomplete equipment malfunction"),
	
	/**
	 * Incomplete equipment not available
	 */
	_Incompleteequipmentnotavailable ("276901000000102", "Incomplete equipment not available"),
	
	/**
	 * Incomplete withdrew consent
	 */
	_Incompletewithdrewconsent ("276871000000102", "Incomplete withdrew consent"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	NewBornHearingScreeningOutcomeSnCT(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static NewBornHearingScreeningOutcomeSnCT getByCode(String code) {
		NewBornHearingScreeningOutcomeSnCT[] vals = values();
		for (NewBornHearingScreeningOutcomeSnCT val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(NewBornHearingScreeningOutcomeSnCT other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	