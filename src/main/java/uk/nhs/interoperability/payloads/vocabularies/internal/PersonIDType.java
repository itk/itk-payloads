/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values types of "ID"
 * <ul>
 *   <li>SDSID : SDS user identifier</li>
 *   <li>SDSRoleProfile : SDS Role Profile identifier</li>
 *   <li>LocalID : Locally assigned identifier</li>
 * </ul>
 * This is an internal enumeration for this Java library and is not a standard ITK vocabulary
 */
public enum PersonIDType implements VocabularyEntry {

	/** Locally assigned identifier */
	LocalPersonID,
	/** SDS user identifier */
	SDSID,
	/** SDS Role Profile identifier */
	SDSRoleProfile;
	
	public String code;
	
	private PersonIDType() {
		this.code = this.toString();
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static PersonIDType getByCode(String code) {
		PersonIDType[] vals = values();
		for (PersonIDType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(PersonIDType other) {
		return (other.getCode().equals(code));
	}
	
}