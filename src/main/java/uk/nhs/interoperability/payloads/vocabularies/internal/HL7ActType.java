/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthorFunctionType;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLDocumentTypeSnCT;

public enum HL7ActType implements VocabularyEntry {
	/** This code is used when the CDA document is about a medical procedure */
	Procedure("PROC"),
	/** This code is used when the document is about a clinical trial to assess the effectiveness and/or safety of a biopharmaceutical product (food,drug,device, etc). */
	ClinicalTrial("CLNTRL"),
	/** This code is used for service events related to giving information about a topic to the patient. */
	Inform("INFRM"),
	/** This code is used when the CDA document is about a transfer of care. */
	PatientCareProvision("PCPR"),
	/** This code is used for CDA documents related to laboratory reports. */
	SpecimenObservation("SPCOBS"),
	/** This code is used for a CDA document about observation series, for example ECG reports. */
	ObservationSeries("OBSSER"),
	/** This code is used when the document is about a substance administration, which is a type of act that involves using, introducing or otherwise applying a substance to the patient. */
	SubstanceAdministration("SBADM");
	
	public String code;
	
	private HL7ActType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}

	public static HL7ActType getByCode(String code) {
		HL7ActType[] vals = values();
		for (HL7ActType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(HL7ActType other) {
		return (other.getCode().equals(code));
	}
	
}
