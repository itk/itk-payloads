/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the HipsExaminationResult vocabulary:
 * <ul>
 *   <li>_00 : Not tested</li>
 *   <li>_01 : No abnormalities and no risk factors</li>
 *   <li>_02 : No abnormalities with risk factors</li>
 *   <li>_03 : Unilateral abnormality suspected Right with risk factors</li>
 *   <li>_04 : Unilateral abnormality suspected Right with no risk</li>
 *   <li>_05 : Unilateral abnormality suspected Left with risk factors</li>
 *   <li>_06 : Unilateral abnormality suspected Left with no risk factors</li>
 *   <li>_07 : Bi-lateral abnormality suspected with risk factors</li>
 *   <li>_08 : Bi-lateral abnormality suspected with no risk factors</li>
 *   <li>_09 : Screen declined</li>
 *   <li>_98 : Incomplete</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum HipsExaminationResult implements VocabularyEntry {
	
	
	/**
	 * Not tested
	 */
	_00 ("00", "Not tested"),
	
	/**
	 * No abnormalities and no risk factors
	 */
	_01 ("01", "No abnormalities and no risk factors"),
	
	/**
	 * No abnormalities with risk factors
	 */
	_02 ("02", "No abnormalities with risk factors"),
	
	/**
	 * Unilateral abnormality suspected Right with risk factors
	 */
	_03 ("03", "Unilateral abnormality suspected (Right) with risk factors"),
	
	/**
	 * Unilateral abnormality suspected Right with no risk
	 */
	_04 ("04", "Unilateral abnormality suspected (Right) with no risk"),
	
	/**
	 * Unilateral abnormality suspected Left with risk factors
	 */
	_05 ("05", "Unilateral abnormality suspected (Left) with risk factors"),
	
	/**
	 * Unilateral abnormality suspected Left with no risk factors
	 */
	_06 ("06", "Unilateral abnormality suspected (Left) with no risk factors"),
	
	/**
	 * Bi-lateral abnormality suspected with risk factors
	 */
	_07 ("07", "Bi-lateral abnormality suspected with risk factors"),
	
	/**
	 * Bi-lateral abnormality suspected with no risk factors
	 */
	_08 ("08", "Bi-lateral abnormality suspected with no risk factors"),
	
	/**
	 * Screen declined
	 */
	_09 ("09", "Screen declined"),
	
	/**
	 * Incomplete
	 */
	_98 ("98", "Incomplete"),
	
	/**
	 * Not tested
	 */
	_Nottested ("00", "Not tested"),
	
	/**
	 * No abnormalities and no risk factors
	 */
	_Noabnormalitiesandnoriskfactors ("01", "No abnormalities and no risk factors"),
	
	/**
	 * No abnormalities with risk factors
	 */
	_Noabnormalitieswithriskfactors ("02", "No abnormalities with risk factors"),
	
	/**
	 * Unilateral abnormality suspected Right with risk factors
	 */
	_UnilateralabnormalitysuspectedRightwithriskfactors ("03", "Unilateral abnormality suspected (Right) with risk factors"),
	
	/**
	 * Unilateral abnormality suspected Right with no risk
	 */
	_UnilateralabnormalitysuspectedRightwithnorisk ("04", "Unilateral abnormality suspected (Right) with no risk"),
	
	/**
	 * Unilateral abnormality suspected Left with risk factors
	 */
	_UnilateralabnormalitysuspectedLeftwithriskfactors ("05", "Unilateral abnormality suspected (Left) with risk factors"),
	
	/**
	 * Unilateral abnormality suspected Left with no risk factors
	 */
	_UnilateralabnormalitysuspectedLeftwithnoriskfactors ("06", "Unilateral abnormality suspected (Left) with no risk factors"),
	
	/**
	 * Bi-lateral abnormality suspected with risk factors
	 */
	_Bilateralabnormalitysuspectedwithriskfactors ("07", "Bi-lateral abnormality suspected with risk factors"),
	
	/**
	 * Bi-lateral abnormality suspected with no risk factors
	 */
	_Bilateralabnormalitysuspectedwithnoriskfactors ("08", "Bi-lateral abnormality suspected with no risk factors"),
	
	/**
	 * Screen declined
	 */
	_Screendeclined ("09", "Screen declined"),
	
	/**
	 * Incomplete
	 */
	_Incomplete ("98", "Incomplete"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.17.446";

	HipsExaminationResult(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static HipsExaminationResult getByCode(String code) {
		HipsExaminationResult[] vals = values();
		for (HipsExaminationResult val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(HipsExaminationResult other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	