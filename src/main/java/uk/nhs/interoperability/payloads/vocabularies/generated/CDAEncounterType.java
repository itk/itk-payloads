/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the CDAEncounterType vocabulary:
 * <ul>
 *   <li>_680007 : Radiation physics consultation</li>
 *   <li>_726007 : Pathology consultation, comprehensive, records and specimen with report</li>
 *   <li>_2530001 : Chiropractic visit</li>
 *   <li>_4525004 : Emergency department patient visit</li>
 *   <li>_5947002 : Consultation for hearing andor speech problem</li>
 *   <li>_8502007 : Radiation physics consultation with therapeutic radiologist</li>
 *   <li>_11429006 : Consultation</li>
 *   <li>_11797002 : Telephone call by physician to patient or for consultation</li>
 *   <li>_12566000 : Consultation in computer dosimetry and isodose chart, teletherapy</li>
 *   <li>_12586001 : Physician direction of emergency medical systems</li>
 *   <li>_12843005 : Subsequent hospital visit by physician</li>
 *   <li>_14736009 : Patient evaluation and management</li>
 *   <li>_15301000 : Consultation in chemotherapy</li>
 *   <li>_17436001 : Medical consultation with outpatient</li>
 *   <li>_24882007 : Medical consultation on hospital inpatient</li>
 *   <li>_25028003 : Dermatology consultation and report, comprehensive</li>
 *   <li>_28191001 : Consultation and report by radiologist</li>
 *   <li>_30078008 : Dermatology consultation and report, brief</li>
 *   <li>_30274002 : Chiropractic consultation</li>
 *   <li>_30346009 : Evaluation and management of established outpatient in office or other outpatient facility</li>
 *   <li>_31108002 : Consultation for laboratory medicine</li>
 *   <li>_34043003 : Dental consultation and report</li>
 *   <li>_35437002 : Consultation in computer dosimetry and isodose chart for brachytherapy</li>
 *   <li>_35755007 : Chiropractic interpretation of x-rays</li>
 *   <li>_37894004 : Evaluation and management of new outpatient in office or other outpatient facility</li>
 *   <li>_42423000 : Chiropractic consultation, history and examination</li>
 *   <li>_44340006 : Prosthodontic dental consultation and report</li>
 *   <li>_48550003 : Medical consultation on nursing facility inpatient</li>
 *   <li>_49463003 : Consultation for paternity case</li>
 *   <li>_49569001 : Consultation in teletherapy</li>
 *   <li>_50687007 : Digestive tract consultation and report</li>
 *   <li>_53923005 : Medical consultation on inpatient</li>
 *   <li>_58400001 : Chiropractic examination</li>
 *   <li>_59000001 : Surgical pathology consultation and report on referred slides prepared elsewhere</li>
 *   <li>_65981005 : Consultation in laboratory medicine for test interpretation</li>
 *   <li>_69399002 : Initial hospital visit by physician</li>
 *   <li>_70495004 : Limited consultation</li>
 *   <li>_71318009 : Physical medicine consultation and report</li>
 *   <li>_71387007 : Endodontic dental consultation and report</li>
 *   <li>_73007006 : Dermatology consultation and report</li>
 *   <li>_73575006 : Chiropractic consultation with history</li>
 *   <li>_77406008 : Confirmatory medical consultation</li>
 *   <li>_77965002 : Consultation in brachytherapy</li>
 *   <li>_83362003 : Final inpatient visit with instructions at discharge</li>
 *   <li>_84251009 : Comprehensive consultation</li>
 *   <li>_86181006 : Evaluation and management of inpatient</li>
 *   <li>_87790002 : Follow-up inpatient consultation visit</li>
 *   <li>_89291005 : Orthodontic dental consultation and report</li>
 *   <li>_108220007 : Evaluation ANDOR management - new patient</li>
 *   <li>_108221006 : Evaluation ANDOR management - established patient</li>
 *   <li>_185202000 : Seen in GPs surgery</li>
 *   <li>_185208001 : Seen in work place</li>
 *   <li>_185218006 : Seen in nursing home</li>
 *   <li>_185235005 : Site of encounter street</li>
 *   <li>_185317003 : Telephone encounter</li>
 *   <li>_185320006 : Encounter by computer link</li>
 *   <li>_185387006 : New patient consultation</li>
 *   <li>_209099002 : History and physical examination with management of domiciliary or rest home patient</li>
 *   <li>_270420001 : Seen in own home</li>
 *   <li>_281036007 : Follow-up consultation</li>
 *   <li>_288836004 : Agreeing on elements of the care plan</li>
 *   <li>_308021002 : Seen in clinic</li>
 *   <li>_308720009 : Letter encounter</li>
 *   <li>_313183009 : Inappropriate use of out of hours service</li>
 *   <li>_314849005 : Telephone contact by consultant</li>
 *   <li>_370838009 : Obtains consultation from the appropriate health care providers</li>
 *   <li>_386472008 : Telephone consultation</li>
 *   <li>_388970003 : Weight maintenance consultation</li>
 *   <li>_388975008 : Weight reduction consultation</li>
 *   <li>_388977000 : Weight increase consultation</li>
 *   <li>_398228004 : Anaesthesia consultation</li>
 *   <li>_400979004 : Homoeopathic consultation</li>
 *   <li>_416520008 : Consultation for unaccompanied minor</li>
 *   <li>_420454001 : Consultation for chronic pain</li>
 *   <li>_420650002 : Consultation for pain</li>
 *   <li>_421946003 : Consultation for acute pain</li>
 *   <li>_423861007 : Consultation for restraint debriefing</li>
 *   <li>_448337001 : Telemedicine consultation with patient</li>
 *   <li>_698307003 : Consultation for benign neoplasm disease</li>
 *   <li>_698308008 : Consultation for malignant neoplasm disease</li>
 *   <li>_698309000 : Consultation for endoscopic procedure</li>
 *   <li>_698310005 : Consultation for colposcopy related procedure</li>
 *   <li>_698311009 : Consultation for assisted reproductive procedure</li>
 *   <li>_698312002 : Consultation for minor abdominal procedure</li>
 *   <li>_698313007 : Consultation for minor operation</li>
 *   <li>_698314001 : Consultation for treatment</li>
 *   <li>_14161000000107 : Out of hours consultation at surgery</li>
 *   <li>_14351000000106 : Weekend consultation at surgery</li>
 *   <li>_14601000000102 : Bank holiday surgery consultation</li>
 *   <li>_113011000000100 : Consultation for minor injury</li>
 *   <li>_239441000000103 : Emergency ambulance call</li>
 *   <li>_239451000000100 : Routine ambulance call</li>
 *   <li>_239461000000102 : Special planned ambulance call</li>
 *   <li>_239471000000109 : Urgent ambulance call</li>
 *   <li>_248491000000101 : Follow-up consultation for minor injury</li>
 *   <li>_514501000000101 : Agreeing on proposed care outcomes</li>
 *   <li>_514601000000102 : Agreeing follow up appointment</li>
 *   <li>_520141000000108 : Telephone consultation for suspected influenza A virus subtype H1N1</li>
 *   <li>_839551000000107 : Consultation for complex sexual health need</li>
 *   <li>_844571000000103 : Agreeing on health professional actions in care plan</li>
 *   <li>_844591000000104 : Agreeing on patient actions in care plan</li>
 *   <li>_903001000000102 : Joint consultation</li>
 *   <li>_903041000000104 : Joint consultation - General Practitioner registrar and General Practitioner trainer</li>
 *   <li>_904931000000107 : Agreeing on needs to be met by care plan</li>
 *   <li>_905131000000106 : Agreeing on needs</li>
 *   <li>_905541000000105 : Agreeing on needs not to be met by care plan</li>
 *   <li>_956781000000107 : Joint consultation with practice nurse and community diabetes specialist nurse</li>
 *   <li>_956921000000109 : Joint consultation with General Practitioner and community diabetes specialist nurse</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum CDAEncounterType implements VocabularyEntry {
	
	
	/**
	 * Radiation physics consultation
	 */
	_680007 ("680007", "Radiation physics consultation"),
	
	/**
	 * Pathology consultation, comprehensive, records and specimen with report
	 */
	_726007 ("726007", "Pathology consultation, comprehensive, records and specimen with report"),
	
	/**
	 * Chiropractic visit
	 */
	_2530001 ("2530001", "Chiropractic visit"),
	
	/**
	 * Emergency department patient visit
	 */
	_4525004 ("4525004", "Emergency department patient visit"),
	
	/**
	 * Consultation for hearing andor speech problem
	 */
	_5947002 ("5947002", "Consultation for hearing and/or speech problem"),
	
	/**
	 * Radiation physics consultation with therapeutic radiologist
	 */
	_8502007 ("8502007", "Radiation physics consultation with therapeutic radiologist"),
	
	/**
	 * Consultation
	 */
	_11429006 ("11429006", "Consultation"),
	
	/**
	 * Telephone call by physician to patient or for consultation
	 */
	_11797002 ("11797002", "Telephone call by physician to patient or for consultation"),
	
	/**
	 * Consultation in computer dosimetry and isodose chart, teletherapy
	 */
	_12566000 ("12566000", "Consultation in computer dosimetry and isodose chart, teletherapy"),
	
	/**
	 * Physician direction of emergency medical systems
	 */
	_12586001 ("12586001", "Physician direction of emergency medical systems"),
	
	/**
	 * Subsequent hospital visit by physician
	 */
	_12843005 ("12843005", "Subsequent hospital visit by physician"),
	
	/**
	 * Patient evaluation and management
	 */
	_14736009 ("14736009", "Patient evaluation and management"),
	
	/**
	 * Consultation in chemotherapy
	 */
	_15301000 ("15301000", "Consultation in chemotherapy"),
	
	/**
	 * Medical consultation with outpatient
	 */
	_17436001 ("17436001", "Medical consultation with outpatient"),
	
	/**
	 * Medical consultation on hospital inpatient
	 */
	_24882007 ("24882007", "Medical consultation on hospital inpatient"),
	
	/**
	 * Dermatology consultation and report, comprehensive
	 */
	_25028003 ("25028003", "Dermatology consultation and report, comprehensive"),
	
	/**
	 * Consultation and report by radiologist
	 */
	_28191001 ("28191001", "Consultation and report by radiologist"),
	
	/**
	 * Dermatology consultation and report, brief
	 */
	_30078008 ("30078008", "Dermatology consultation and report, brief"),
	
	/**
	 * Chiropractic consultation
	 */
	_30274002 ("30274002", "Chiropractic consultation"),
	
	/**
	 * Evaluation and management of established outpatient in office or other outpatient facility
	 */
	_30346009 ("30346009", "Evaluation and management of established outpatient in office or other outpatient facility"),
	
	/**
	 * Consultation for laboratory medicine
	 */
	_31108002 ("31108002", "Consultation for laboratory medicine"),
	
	/**
	 * Dental consultation and report
	 */
	_34043003 ("34043003", "Dental consultation and report"),
	
	/**
	 * Consultation in computer dosimetry and isodose chart for brachytherapy
	 */
	_35437002 ("35437002", "Consultation in computer dosimetry and isodose chart for brachytherapy"),
	
	/**
	 * Chiropractic interpretation of x-rays
	 */
	_35755007 ("35755007", "Chiropractic interpretation of x-rays"),
	
	/**
	 * Evaluation and management of new outpatient in office or other outpatient facility
	 */
	_37894004 ("37894004", "Evaluation and management of new outpatient in office or other outpatient facility"),
	
	/**
	 * Chiropractic consultation, history and examination
	 */
	_42423000 ("42423000", "Chiropractic consultation, history and examination"),
	
	/**
	 * Prosthodontic dental consultation and report
	 */
	_44340006 ("44340006", "Prosthodontic dental consultation and report"),
	
	/**
	 * Medical consultation on nursing facility inpatient
	 */
	_48550003 ("48550003", "Medical consultation on nursing facility inpatient"),
	
	/**
	 * Consultation for paternity case
	 */
	_49463003 ("49463003", "Consultation for paternity case"),
	
	/**
	 * Consultation in teletherapy
	 */
	_49569001 ("49569001", "Consultation in teletherapy"),
	
	/**
	 * Digestive tract consultation and report
	 */
	_50687007 ("50687007", "Digestive tract consultation and report"),
	
	/**
	 * Medical consultation on inpatient
	 */
	_53923005 ("53923005", "Medical consultation on inpatient"),
	
	/**
	 * Chiropractic examination
	 */
	_58400001 ("58400001", "Chiropractic examination"),
	
	/**
	 * Surgical pathology consultation and report on referred slides prepared elsewhere
	 */
	_59000001 ("59000001", "Surgical pathology consultation and report on referred slides prepared elsewhere"),
	
	/**
	 * Consultation in laboratory medicine for test interpretation
	 */
	_65981005 ("65981005", "Consultation in laboratory medicine for test interpretation"),
	
	/**
	 * Initial hospital visit by physician
	 */
	_69399002 ("69399002", "Initial hospital visit by physician"),
	
	/**
	 * Limited consultation
	 */
	_70495004 ("70495004", "Limited consultation"),
	
	/**
	 * Physical medicine consultation and report
	 */
	_71318009 ("71318009", "Physical medicine consultation and report"),
	
	/**
	 * Endodontic dental consultation and report
	 */
	_71387007 ("71387007", "Endodontic dental consultation and report"),
	
	/**
	 * Dermatology consultation and report
	 */
	_73007006 ("73007006", "Dermatology consultation and report"),
	
	/**
	 * Chiropractic consultation with history
	 */
	_73575006 ("73575006", "Chiropractic consultation with history"),
	
	/**
	 * Confirmatory medical consultation
	 */
	_77406008 ("77406008", "Confirmatory medical consultation"),
	
	/**
	 * Consultation in brachytherapy
	 */
	_77965002 ("77965002", "Consultation in brachytherapy"),
	
	/**
	 * Final inpatient visit with instructions at discharge
	 */
	_83362003 ("83362003", "Final inpatient visit with instructions at discharge"),
	
	/**
	 * Comprehensive consultation
	 */
	_84251009 ("84251009", "Comprehensive consultation"),
	
	/**
	 * Evaluation and management of inpatient
	 */
	_86181006 ("86181006", "Evaluation and management of inpatient"),
	
	/**
	 * Follow-up inpatient consultation visit
	 */
	_87790002 ("87790002", "Follow-up inpatient consultation visit"),
	
	/**
	 * Orthodontic dental consultation and report
	 */
	_89291005 ("89291005", "Orthodontic dental consultation and report"),
	
	/**
	 * Evaluation ANDOR management - new patient
	 */
	_108220007 ("108220007", "Evaluation AND/OR management - new patient"),
	
	/**
	 * Evaluation ANDOR management - established patient
	 */
	_108221006 ("108221006", "Evaluation AND/OR management - established patient"),
	
	/**
	 * Seen in GPs surgery
	 */
	_185202000 ("185202000", "Seen in GP's surgery"),
	
	/**
	 * Seen in work place
	 */
	_185208001 ("185208001", "Seen in work place"),
	
	/**
	 * Seen in nursing home
	 */
	_185218006 ("185218006", "Seen in nursing home"),
	
	/**
	 * Site of encounter street
	 */
	_185235005 ("185235005", "Site of encounter: street"),
	
	/**
	 * Telephone encounter
	 */
	_185317003 ("185317003", "Telephone encounter"),
	
	/**
	 * Encounter by computer link
	 */
	_185320006 ("185320006", "Encounter by computer link"),
	
	/**
	 * New patient consultation
	 */
	_185387006 ("185387006", "New patient consultation"),
	
	/**
	 * History and physical examination with management of domiciliary or rest home patient
	 */
	_209099002 ("209099002", "History and physical examination with management of domiciliary or rest home patient"),
	
	/**
	 * Seen in own home
	 */
	_270420001 ("270420001", "Seen in own home"),
	
	/**
	 * Follow-up consultation
	 */
	_281036007 ("281036007", "Follow-up consultation"),
	
	/**
	 * Agreeing on elements of the care plan
	 */
	_288836004 ("288836004", "Agreeing on elements of the care plan"),
	
	/**
	 * Seen in clinic
	 */
	_308021002 ("308021002", "Seen in clinic"),
	
	/**
	 * Letter encounter
	 */
	_308720009 ("308720009", "Letter encounter"),
	
	/**
	 * Inappropriate use of out of hours service
	 */
	_313183009 ("313183009", "Inappropriate use of out of hours service"),
	
	/**
	 * Telephone contact by consultant
	 */
	_314849005 ("314849005", "Telephone contact by consultant"),
	
	/**
	 * Obtains consultation from the appropriate health care providers
	 */
	_370838009 ("370838009", "Obtains consultation from the appropriate health care providers"),
	
	/**
	 * Telephone consultation
	 */
	_386472008 ("386472008", "Telephone consultation"),
	
	/**
	 * Weight maintenance consultation
	 */
	_388970003 ("388970003", "Weight maintenance consultation"),
	
	/**
	 * Weight reduction consultation
	 */
	_388975008 ("388975008", "Weight reduction consultation"),
	
	/**
	 * Weight increase consultation
	 */
	_388977000 ("388977000", "Weight increase consultation"),
	
	/**
	 * Anaesthesia consultation
	 */
	_398228004 ("398228004", "Anaesthesia consultation"),
	
	/**
	 * Homoeopathic consultation
	 */
	_400979004 ("400979004", "Homoeopathic consultation"),
	
	/**
	 * Consultation for unaccompanied minor
	 */
	_416520008 ("416520008", "Consultation for unaccompanied minor"),
	
	/**
	 * Consultation for chronic pain
	 */
	_420454001 ("420454001", "Consultation for chronic pain"),
	
	/**
	 * Consultation for pain
	 */
	_420650002 ("420650002", "Consultation for pain"),
	
	/**
	 * Consultation for acute pain
	 */
	_421946003 ("421946003", "Consultation for acute pain"),
	
	/**
	 * Consultation for restraint debriefing
	 */
	_423861007 ("423861007", "Consultation for restraint debriefing"),
	
	/**
	 * Telemedicine consultation with patient
	 */
	_448337001 ("448337001", "Telemedicine consultation with patient"),
	
	/**
	 * Consultation for benign neoplasm disease
	 */
	_698307003 ("698307003", "Consultation for benign neoplasm disease"),
	
	/**
	 * Consultation for malignant neoplasm disease
	 */
	_698308008 ("698308008", "Consultation for malignant neoplasm disease"),
	
	/**
	 * Consultation for endoscopic procedure
	 */
	_698309000 ("698309000", "Consultation for endoscopic procedure"),
	
	/**
	 * Consultation for colposcopy related procedure
	 */
	_698310005 ("698310005", "Consultation for colposcopy related procedure"),
	
	/**
	 * Consultation for assisted reproductive procedure
	 */
	_698311009 ("698311009", "Consultation for assisted reproductive procedure"),
	
	/**
	 * Consultation for minor abdominal procedure
	 */
	_698312002 ("698312002", "Consultation for minor abdominal procedure"),
	
	/**
	 * Consultation for minor operation
	 */
	_698313007 ("698313007", "Consultation for minor operation"),
	
	/**
	 * Consultation for treatment
	 */
	_698314001 ("698314001", "Consultation for treatment"),
	
	/**
	 * Out of hours consultation at surgery
	 */
	_14161000000107 ("14161000000107", "Out of hours consultation at surgery"),
	
	/**
	 * Weekend consultation at surgery
	 */
	_14351000000106 ("14351000000106", "Weekend consultation at surgery"),
	
	/**
	 * Bank holiday surgery consultation
	 */
	_14601000000102 ("14601000000102", "Bank holiday surgery consultation"),
	
	/**
	 * Consultation for minor injury
	 */
	_113011000000100 ("113011000000100", "Consultation for minor injury"),
	
	/**
	 * Emergency ambulance call
	 */
	_239441000000103 ("239441000000103", "Emergency ambulance call"),
	
	/**
	 * Routine ambulance call
	 */
	_239451000000100 ("239451000000100", "Routine ambulance call"),
	
	/**
	 * Special planned ambulance call
	 */
	_239461000000102 ("239461000000102", "Special planned ambulance call"),
	
	/**
	 * Urgent ambulance call
	 */
	_239471000000109 ("239471000000109", "Urgent ambulance call"),
	
	/**
	 * Follow-up consultation for minor injury
	 */
	_248491000000101 ("248491000000101", "Follow-up consultation for minor injury"),
	
	/**
	 * Agreeing on proposed care outcomes
	 */
	_514501000000101 ("514501000000101", "Agreeing on proposed care outcomes"),
	
	/**
	 * Agreeing follow up appointment
	 */
	_514601000000102 ("514601000000102", "Agreeing follow up appointment"),
	
	/**
	 * Telephone consultation for suspected influenza A virus subtype H1N1
	 */
	_520141000000108 ("520141000000108", "Telephone consultation for suspected influenza A virus subtype H1N1"),
	
	/**
	 * Consultation for complex sexual health need
	 */
	_839551000000107 ("839551000000107", "Consultation for complex sexual health need"),
	
	/**
	 * Agreeing on health professional actions in care plan
	 */
	_844571000000103 ("844571000000103", "Agreeing on health professional actions in care plan"),
	
	/**
	 * Agreeing on patient actions in care plan
	 */
	_844591000000104 ("844591000000104", "Agreeing on patient actions in care plan"),
	
	/**
	 * Joint consultation
	 */
	_903001000000102 ("903001000000102", "Joint consultation"),
	
	/**
	 * Joint consultation - General Practitioner registrar and General Practitioner trainer
	 */
	_903041000000104 ("903041000000104", "Joint consultation - General Practitioner registrar and General Practitioner trainer"),
	
	/**
	 * Agreeing on needs to be met by care plan
	 */
	_904931000000107 ("904931000000107", "Agreeing on needs to be met by care plan"),
	
	/**
	 * Agreeing on needs
	 */
	_905131000000106 ("905131000000106", "Agreeing on needs"),
	
	/**
	 * Agreeing on needs not to be met by care plan
	 */
	_905541000000105 ("905541000000105", "Agreeing on needs not to be met by care plan"),
	
	/**
	 * Joint consultation with practice nurse and community diabetes specialist nurse
	 */
	_956781000000107 ("956781000000107", "Joint consultation with practice nurse and community diabetes specialist nurse"),
	
	/**
	 * Joint consultation with General Practitioner and community diabetes specialist nurse
	 */
	_956921000000109 ("956921000000109", "Joint consultation with General Practitioner and community diabetes specialist nurse"),
	
	/**
	 * Radiation physics consultation
	 */
	_Radiationphysicsconsultation ("680007", "Radiation physics consultation"),
	
	/**
	 * Pathology consultation, comprehensive, records and specimen with report
	 */
	_Pathologyconsultationcomprehensiverecordsandspecimenwithreport ("726007", "Pathology consultation, comprehensive, records and specimen with report"),
	
	/**
	 * Chiropractic visit
	 */
	_Chiropracticvisit ("2530001", "Chiropractic visit"),
	
	/**
	 * Emergency department patient visit
	 */
	_Emergencydepartmentpatientvisit ("4525004", "Emergency department patient visit"),
	
	/**
	 * Consultation for hearing andor speech problem
	 */
	_Consultationforhearingandorspeechproblem ("5947002", "Consultation for hearing and/or speech problem"),
	
	/**
	 * Radiation physics consultation with therapeutic radiologist
	 */
	_Radiationphysicsconsultationwiththerapeuticradiologist ("8502007", "Radiation physics consultation with therapeutic radiologist"),
	
	/**
	 * Consultation
	 */
	_Consultation ("11429006", "Consultation"),
	
	/**
	 * Telephone call by physician to patient or for consultation
	 */
	_Telephonecallbyphysiciantopatientorforconsultation ("11797002", "Telephone call by physician to patient or for consultation"),
	
	/**
	 * Consultation in computer dosimetry and isodose chart, teletherapy
	 */
	_Consultationincomputerdosimetryandisodosechartteletherapy ("12566000", "Consultation in computer dosimetry and isodose chart, teletherapy"),
	
	/**
	 * Physician direction of emergency medical systems
	 */
	_Physiciandirectionofemergencymedicalsystems ("12586001", "Physician direction of emergency medical systems"),
	
	/**
	 * Subsequent hospital visit by physician
	 */
	_Subsequenthospitalvisitbyphysician ("12843005", "Subsequent hospital visit by physician"),
	
	/**
	 * Patient evaluation and management
	 */
	_Patientevaluationandmanagement ("14736009", "Patient evaluation and management"),
	
	/**
	 * Consultation in chemotherapy
	 */
	_Consultationinchemotherapy ("15301000", "Consultation in chemotherapy"),
	
	/**
	 * Medical consultation with outpatient
	 */
	_Medicalconsultationwithoutpatient ("17436001", "Medical consultation with outpatient"),
	
	/**
	 * Medical consultation on hospital inpatient
	 */
	_Medicalconsultationonhospitalinpatient ("24882007", "Medical consultation on hospital inpatient"),
	
	/**
	 * Dermatology consultation and report, comprehensive
	 */
	_Dermatologyconsultationandreportcomprehensive ("25028003", "Dermatology consultation and report, comprehensive"),
	
	/**
	 * Consultation and report by radiologist
	 */
	_Consultationandreportbyradiologist ("28191001", "Consultation and report by radiologist"),
	
	/**
	 * Dermatology consultation and report, brief
	 */
	_Dermatologyconsultationandreportbrief ("30078008", "Dermatology consultation and report, brief"),
	
	/**
	 * Chiropractic consultation
	 */
	_Chiropracticconsultation ("30274002", "Chiropractic consultation"),
	
	/**
	 * Evaluation and management of established outpatient in office or other outpatient facility
	 */
	_Evaluationandmanagementofestablishedoutpatientinofficeorotheroutpatientfacility ("30346009", "Evaluation and management of established outpatient in office or other outpatient facility"),
	
	/**
	 * Consultation for laboratory medicine
	 */
	_Consultationforlaboratorymedicine ("31108002", "Consultation for laboratory medicine"),
	
	/**
	 * Dental consultation and report
	 */
	_Dentalconsultationandreport ("34043003", "Dental consultation and report"),
	
	/**
	 * Consultation in computer dosimetry and isodose chart for brachytherapy
	 */
	_Consultationincomputerdosimetryandisodosechartforbrachytherapy ("35437002", "Consultation in computer dosimetry and isodose chart for brachytherapy"),
	
	/**
	 * Chiropractic interpretation of x-rays
	 */
	_Chiropracticinterpretationofxrays ("35755007", "Chiropractic interpretation of x-rays"),
	
	/**
	 * Evaluation and management of new outpatient in office or other outpatient facility
	 */
	_Evaluationandmanagementofnewoutpatientinofficeorotheroutpatientfacility ("37894004", "Evaluation and management of new outpatient in office or other outpatient facility"),
	
	/**
	 * Chiropractic consultation, history and examination
	 */
	_Chiropracticconsultationhistoryandexamination ("42423000", "Chiropractic consultation, history and examination"),
	
	/**
	 * Prosthodontic dental consultation and report
	 */
	_Prosthodonticdentalconsultationandreport ("44340006", "Prosthodontic dental consultation and report"),
	
	/**
	 * Medical consultation on nursing facility inpatient
	 */
	_Medicalconsultationonnursingfacilityinpatient ("48550003", "Medical consultation on nursing facility inpatient"),
	
	/**
	 * Consultation for paternity case
	 */
	_Consultationforpaternitycase ("49463003", "Consultation for paternity case"),
	
	/**
	 * Consultation in teletherapy
	 */
	_Consultationinteletherapy ("49569001", "Consultation in teletherapy"),
	
	/**
	 * Digestive tract consultation and report
	 */
	_Digestivetractconsultationandreport ("50687007", "Digestive tract consultation and report"),
	
	/**
	 * Medical consultation on inpatient
	 */
	_Medicalconsultationoninpatient ("53923005", "Medical consultation on inpatient"),
	
	/**
	 * Chiropractic examination
	 */
	_Chiropracticexamination ("58400001", "Chiropractic examination"),
	
	/**
	 * Surgical pathology consultation and report on referred slides prepared elsewhere
	 */
	_Surgicalpathologyconsultationandreportonreferredslidespreparedelsewhere ("59000001", "Surgical pathology consultation and report on referred slides prepared elsewhere"),
	
	/**
	 * Consultation in laboratory medicine for test interpretation
	 */
	_Consultationinlaboratorymedicinefortestinterpretation ("65981005", "Consultation in laboratory medicine for test interpretation"),
	
	/**
	 * Initial hospital visit by physician
	 */
	_Initialhospitalvisitbyphysician ("69399002", "Initial hospital visit by physician"),
	
	/**
	 * Limited consultation
	 */
	_Limitedconsultation ("70495004", "Limited consultation"),
	
	/**
	 * Physical medicine consultation and report
	 */
	_Physicalmedicineconsultationandreport ("71318009", "Physical medicine consultation and report"),
	
	/**
	 * Endodontic dental consultation and report
	 */
	_Endodonticdentalconsultationandreport ("71387007", "Endodontic dental consultation and report"),
	
	/**
	 * Dermatology consultation and report
	 */
	_Dermatologyconsultationandreport ("73007006", "Dermatology consultation and report"),
	
	/**
	 * Chiropractic consultation with history
	 */
	_Chiropracticconsultationwithhistory ("73575006", "Chiropractic consultation with history"),
	
	/**
	 * Confirmatory medical consultation
	 */
	_Confirmatorymedicalconsultation ("77406008", "Confirmatory medical consultation"),
	
	/**
	 * Consultation in brachytherapy
	 */
	_Consultationinbrachytherapy ("77965002", "Consultation in brachytherapy"),
	
	/**
	 * Final inpatient visit with instructions at discharge
	 */
	_Finalinpatientvisitwithinstructionsatdischarge ("83362003", "Final inpatient visit with instructions at discharge"),
	
	/**
	 * Comprehensive consultation
	 */
	_Comprehensiveconsultation ("84251009", "Comprehensive consultation"),
	
	/**
	 * Evaluation and management of inpatient
	 */
	_Evaluationandmanagementofinpatient ("86181006", "Evaluation and management of inpatient"),
	
	/**
	 * Follow-up inpatient consultation visit
	 */
	_Followupinpatientconsultationvisit ("87790002", "Follow-up inpatient consultation visit"),
	
	/**
	 * Orthodontic dental consultation and report
	 */
	_Orthodonticdentalconsultationandreport ("89291005", "Orthodontic dental consultation and report"),
	
	/**
	 * Evaluation ANDOR management - new patient
	 */
	_EvaluationANDORmanagementnewpatient ("108220007", "Evaluation AND/OR management - new patient"),
	
	/**
	 * Evaluation ANDOR management - established patient
	 */
	_EvaluationANDORmanagementestablishedpatient ("108221006", "Evaluation AND/OR management - established patient"),
	
	/**
	 * Seen in GPs surgery
	 */
	_SeeninGPssurgery ("185202000", "Seen in GP's surgery"),
	
	/**
	 * Seen in work place
	 */
	_Seeninworkplace ("185208001", "Seen in work place"),
	
	/**
	 * Seen in nursing home
	 */
	_Seeninnursinghome ("185218006", "Seen in nursing home"),
	
	/**
	 * Site of encounter street
	 */
	_Siteofencounterstreet ("185235005", "Site of encounter: street"),
	
	/**
	 * Telephone encounter
	 */
	_Telephoneencounter ("185317003", "Telephone encounter"),
	
	/**
	 * Encounter by computer link
	 */
	_Encounterbycomputerlink ("185320006", "Encounter by computer link"),
	
	/**
	 * New patient consultation
	 */
	_Newpatientconsultation ("185387006", "New patient consultation"),
	
	/**
	 * History and physical examination with management of domiciliary or rest home patient
	 */
	_Historyandphysicalexaminationwithmanagementofdomiciliaryorresthomepatient ("209099002", "History and physical examination with management of domiciliary or rest home patient"),
	
	/**
	 * Seen in own home
	 */
	_Seeninownhome ("270420001", "Seen in own home"),
	
	/**
	 * Follow-up consultation
	 */
	_Followupconsultation ("281036007", "Follow-up consultation"),
	
	/**
	 * Agreeing on elements of the care plan
	 */
	_Agreeingonelementsofthecareplan ("288836004", "Agreeing on elements of the care plan"),
	
	/**
	 * Seen in clinic
	 */
	_Seeninclinic ("308021002", "Seen in clinic"),
	
	/**
	 * Letter encounter
	 */
	_Letterencounter ("308720009", "Letter encounter"),
	
	/**
	 * Inappropriate use of out of hours service
	 */
	_Inappropriateuseofoutofhoursservice ("313183009", "Inappropriate use of out of hours service"),
	
	/**
	 * Telephone contact by consultant
	 */
	_Telephonecontactbyconsultant ("314849005", "Telephone contact by consultant"),
	
	/**
	 * Obtains consultation from the appropriate health care providers
	 */
	_Obtainsconsultationfromtheappropriatehealthcareproviders ("370838009", "Obtains consultation from the appropriate health care providers"),
	
	/**
	 * Telephone consultation
	 */
	_Telephoneconsultation ("386472008", "Telephone consultation"),
	
	/**
	 * Weight maintenance consultation
	 */
	_Weightmaintenanceconsultation ("388970003", "Weight maintenance consultation"),
	
	/**
	 * Weight reduction consultation
	 */
	_Weightreductionconsultation ("388975008", "Weight reduction consultation"),
	
	/**
	 * Weight increase consultation
	 */
	_Weightincreaseconsultation ("388977000", "Weight increase consultation"),
	
	/**
	 * Anaesthesia consultation
	 */
	_Anaesthesiaconsultation ("398228004", "Anaesthesia consultation"),
	
	/**
	 * Homoeopathic consultation
	 */
	_Homoeopathicconsultation ("400979004", "Homoeopathic consultation"),
	
	/**
	 * Consultation for unaccompanied minor
	 */
	_Consultationforunaccompaniedminor ("416520008", "Consultation for unaccompanied minor"),
	
	/**
	 * Consultation for chronic pain
	 */
	_Consultationforchronicpain ("420454001", "Consultation for chronic pain"),
	
	/**
	 * Consultation for pain
	 */
	_Consultationforpain ("420650002", "Consultation for pain"),
	
	/**
	 * Consultation for acute pain
	 */
	_Consultationforacutepain ("421946003", "Consultation for acute pain"),
	
	/**
	 * Consultation for restraint debriefing
	 */
	_Consultationforrestraintdebriefing ("423861007", "Consultation for restraint debriefing"),
	
	/**
	 * Telemedicine consultation with patient
	 */
	_Telemedicineconsultationwithpatient ("448337001", "Telemedicine consultation with patient"),
	
	/**
	 * Consultation for benign neoplasm disease
	 */
	_Consultationforbenignneoplasmdisease ("698307003", "Consultation for benign neoplasm disease"),
	
	/**
	 * Consultation for malignant neoplasm disease
	 */
	_Consultationformalignantneoplasmdisease ("698308008", "Consultation for malignant neoplasm disease"),
	
	/**
	 * Consultation for endoscopic procedure
	 */
	_Consultationforendoscopicprocedure ("698309000", "Consultation for endoscopic procedure"),
	
	/**
	 * Consultation for colposcopy related procedure
	 */
	_Consultationforcolposcopyrelatedprocedure ("698310005", "Consultation for colposcopy related procedure"),
	
	/**
	 * Consultation for assisted reproductive procedure
	 */
	_Consultationforassistedreproductiveprocedure ("698311009", "Consultation for assisted reproductive procedure"),
	
	/**
	 * Consultation for minor abdominal procedure
	 */
	_Consultationforminorabdominalprocedure ("698312002", "Consultation for minor abdominal procedure"),
	
	/**
	 * Consultation for minor operation
	 */
	_Consultationforminoroperation ("698313007", "Consultation for minor operation"),
	
	/**
	 * Consultation for treatment
	 */
	_Consultationfortreatment ("698314001", "Consultation for treatment"),
	
	/**
	 * Out of hours consultation at surgery
	 */
	_Outofhoursconsultationatsurgery ("14161000000107", "Out of hours consultation at surgery"),
	
	/**
	 * Weekend consultation at surgery
	 */
	_Weekendconsultationatsurgery ("14351000000106", "Weekend consultation at surgery"),
	
	/**
	 * Bank holiday surgery consultation
	 */
	_Bankholidaysurgeryconsultation ("14601000000102", "Bank holiday surgery consultation"),
	
	/**
	 * Consultation for minor injury
	 */
	_Consultationforminorinjury ("113011000000100", "Consultation for minor injury"),
	
	/**
	 * Emergency ambulance call
	 */
	_Emergencyambulancecall ("239441000000103", "Emergency ambulance call"),
	
	/**
	 * Routine ambulance call
	 */
	_Routineambulancecall ("239451000000100", "Routine ambulance call"),
	
	/**
	 * Special planned ambulance call
	 */
	_Specialplannedambulancecall ("239461000000102", "Special planned ambulance call"),
	
	/**
	 * Urgent ambulance call
	 */
	_Urgentambulancecall ("239471000000109", "Urgent ambulance call"),
	
	/**
	 * Follow-up consultation for minor injury
	 */
	_Followupconsultationforminorinjury ("248491000000101", "Follow-up consultation for minor injury"),
	
	/**
	 * Agreeing on proposed care outcomes
	 */
	_Agreeingonproposedcareoutcomes ("514501000000101", "Agreeing on proposed care outcomes"),
	
	/**
	 * Agreeing follow up appointment
	 */
	_Agreeingfollowupappointment ("514601000000102", "Agreeing follow up appointment"),
	
	/**
	 * Telephone consultation for suspected influenza A virus subtype H1N1
	 */
	_TelephoneconsultationforsuspectedinfluenzaAvirussubtypeH1N1 ("520141000000108", "Telephone consultation for suspected influenza A virus subtype H1N1"),
	
	/**
	 * Consultation for complex sexual health need
	 */
	_Consultationforcomplexsexualhealthneed ("839551000000107", "Consultation for complex sexual health need"),
	
	/**
	 * Agreeing on health professional actions in care plan
	 */
	_Agreeingonhealthprofessionalactionsincareplan ("844571000000103", "Agreeing on health professional actions in care plan"),
	
	/**
	 * Agreeing on patient actions in care plan
	 */
	_Agreeingonpatientactionsincareplan ("844591000000104", "Agreeing on patient actions in care plan"),
	
	/**
	 * Joint consultation
	 */
	_Jointconsultation ("903001000000102", "Joint consultation"),
	
	/**
	 * Joint consultation - General Practitioner registrar and General Practitioner trainer
	 */
	_JointconsultationGeneralPractitionerregistrarandGeneralPractitionertrainer ("903041000000104", "Joint consultation - General Practitioner registrar and General Practitioner trainer"),
	
	/**
	 * Agreeing on needs to be met by care plan
	 */
	_Agreeingonneedstobemetbycareplan ("904931000000107", "Agreeing on needs to be met by care plan"),
	
	/**
	 * Agreeing on needs
	 */
	_Agreeingonneeds ("905131000000106", "Agreeing on needs"),
	
	/**
	 * Agreeing on needs not to be met by care plan
	 */
	_Agreeingonneedsnottobemetbycareplan ("905541000000105", "Agreeing on needs not to be met by care plan"),
	
	/**
	 * Joint consultation with practice nurse and community diabetes specialist nurse
	 */
	_Jointconsultationwithpracticenurseandcommunitydiabetesspecialistnurse ("956781000000107", "Joint consultation with practice nurse and community diabetes specialist nurse"),
	
	/**
	 * Joint consultation with General Practitioner and community diabetes specialist nurse
	 */
	_JointconsultationwithGeneralPractitionerandcommunitydiabetesspecialistnurse ("956921000000109", "Joint consultation with General Practitioner and community diabetes specialist nurse"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	CDAEncounterType(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static CDAEncounterType getByCode(String code) {
		CDAEncounterType[] vals = values();
		for (CDAEncounterType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(CDAEncounterType other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	