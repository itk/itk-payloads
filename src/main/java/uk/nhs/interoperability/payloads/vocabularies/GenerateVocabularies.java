/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.metadata.VocabClassInfo;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.util.TransformManager;

/**
 * Nationally agreed vocabularies are published on TRUD within the
 * NHS Interoperability Specifications Reference Pack bundle:
 * https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases
 *
 * @author adha3
 */
public class GenerateVocabularies implements VocabProcessor {
	private static final Logger logger = LoggerFactory.getLogger(GenerateVocabularies.class);

	/**
	 * @param args Command line arguments (ignored)
	 */
	public static void main(String[] args) {
		GenerateVocabularies processor = new GenerateVocabularies();
		VocabularyLoader.loadVocabs(processor);
		logger.info("Vocabulary generation complete.");
	}
	
	@Override
	public VocabClassInfo processVocab(File file) {
		return processVocab(file, null);
	}
	
	@Override
	public VocabClassInfo processVocab(File vocabFile, String oid) {
		VocabClassInfo vocabInfo = null;
		String outputPath = PropertyReader.getProperty("generatedClassesPath");
	    
	    try {
			String output = TransformManager.doTransform("codegeneratortransforms/vocabCodeGenerationTranform.xsl", vocabFile, null);
			
			// Bit of a hack, but the first line of the generated output is the filename, terminated by "@@@@"
			String[] outputSplit = output.split("@@@@");
			String vocabName = outputSplit[0];
			String originalOID = outputSplit[1];
			String classFileContent = outputSplit[2];
			
			if (oid != null) {
				// Overwrite the ID we got from the main vocab file with the provided OID
				classFileContent = classFileContent.replaceAll("oid=\".*\"", "oid=\""+oid+"\"");
			} else {
				oid = originalOID;
			}
			
			try {
				FileWriter.writeFile(outputPath + "/" + vocabName + ".java", classFileContent.getBytes("UTF-8"));
				vocabInfo = new VocabClassInfo();
				vocabInfo.setVocabName(vocabName);
				vocabInfo.setOid(oid);
			} catch (UnsupportedEncodingException e) {
				logger.error("Unable to write vocabularies using UTF-8 encoding", e);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    return vocabInfo;
	}
}
