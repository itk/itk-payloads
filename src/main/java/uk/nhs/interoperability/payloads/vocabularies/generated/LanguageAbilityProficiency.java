/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the LanguageAbilityProficiency vocabulary:
 * <ul>
 *   <li>_E : Excellent</li>
 *   <li>_F : Fair</li>
 *   <li>_G : Good</li>
 *   <li>_P : Poor</li>
 *   <li>_0 : Interpreter not required</li>
 *   <li>_1 : Interpreter required</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum LanguageAbilityProficiency implements VocabularyEntry {
	
	
	/**
	 * Excellent
	 */
	_E ("E", "Excellent"),
	
	/**
	 * Fair
	 */
	_F ("F", "Fair"),
	
	/**
	 * Good
	 */
	_G ("G", "Good"),
	
	/**
	 * Poor
	 */
	_P ("P", "Poor"),
	
	/**
	 * Interpreter not required
	 */
	_0 ("0", "Interpreter not required"),
	
	/**
	 * Interpreter required
	 */
	_1 ("1", "Interpreter required"),
	
	/**
	 * Excellent
	 */
	_Excellent ("E", "Excellent"),
	
	/**
	 * Fair
	 */
	_Fair ("F", "Fair"),
	
	/**
	 * Good
	 */
	_Good ("G", "Good"),
	
	/**
	 * Poor
	 */
	_Poor ("P", "Poor"),
	
	/**
	 * Interpreter not required
	 */
	_Interpreternotrequired ("0", "Interpreter not required"),
	
	/**
	 * Interpreter required
	 */
	_Interpreterrequired ("1", "Interpreter required"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.5.61";

	LanguageAbilityProficiency(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static LanguageAbilityProficiency getByCode(String code) {
		LanguageAbilityProficiency[] vals = values();
		for (LanguageAbilityProficiency val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(LanguageAbilityProficiency other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	