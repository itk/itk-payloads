/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values types of "ID"
 * <ul>
 *   <li>Local : Local Identifier</li>
 *   <li>National : National Identifier</li>
 * </ul>
 * This is an internal enumeration for this Java library and is not a standard ITK vocabulary
 */
public enum IDType implements VocabularyEntry {

	/** Local Identifier */
	Local,
	/** National Identifier */
	National;
	
	public String code;
	
	private IDType() {
		this.code = this.toString();
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}
	
	public static IDType getByCode(String code) {
		IDType[] vals = values();
		for (IDType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(IDType other) {
		return (other.getCode().equals(code));
	}
	
}