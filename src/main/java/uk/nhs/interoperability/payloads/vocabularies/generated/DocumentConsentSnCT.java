/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.generated;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is an enumeration of the allowable values within the DocumentConsentSnCT vocabulary:
 * <ul>
 *   <li>_425691002 : Consent given for electronic record sharing</li>
 *   <li>_342591000000108 : Consent given for electronic record sharing with carer</li>
 *   <li>_417528008 : Consent given for upload to local shared electronic record</li>
 *   <li>_417370002 : Consent given for upload to national shared electronic record</li>
 *   <li>_319951000000105 : Consent given to share patient data with specified third party</li>
 *   <li>_320011000000108 : Declined consent to share patient data with specified third party</li>
 *   <li>_416409005 : Refused consent for upload to local shared electronic record</li>
 *   <li>_416308001 : Refused consent for upload to national shared electronic record</li>

 * </ul>
 * This enum was generated from XML files that define the vocabularies, the national vocabularies are available
 * from <a href="https://www.uktcregistration.nss.cfh.nhs.uk/trud3/user/guest/group/0/pack/11/subpack/61/releases">TRUD</a>.
 * @see uk.nhs.interoperability.payloads.vocabularies.GenerateVocabularies
 */
public enum DocumentConsentSnCT implements VocabularyEntry {
	
	
	/**
	 * Consent given for electronic record sharing
	 */
	_425691002 ("425691002", "Consent given for electronic record sharing"),
	
	/**
	 * Consent given for electronic record sharing with carer
	 */
	_342591000000108 ("342591000000108", "Consent given for electronic record sharing with carer"),
	
	/**
	 * Consent given for upload to local shared electronic record
	 */
	_417528008 ("417528008", "Consent given for upload to local shared electronic record"),
	
	/**
	 * Consent given for upload to national shared electronic record
	 */
	_417370002 ("417370002", "Consent given for upload to national shared electronic record"),
	
	/**
	 * Consent given to share patient data with specified third party
	 */
	_319951000000105 ("319951000000105", "Consent given to share patient data with specified third party"),
	
	/**
	 * Declined consent to share patient data with specified third party
	 */
	_320011000000108 ("320011000000108", "Declined consent to share patient data with specified third party"),
	
	/**
	 * Refused consent for upload to local shared electronic record
	 */
	_416409005 ("416409005", "Refused consent for upload to local shared electronic record"),
	
	/**
	 * Refused consent for upload to national shared electronic record
	 */
	_416308001 ("416308001", "Refused consent for upload to national shared electronic record"),
	
	/**
	 * Consent given for electronic record sharing
	 */
	_Consentgivenforelectronicrecordsharing ("425691002", "Consent given for electronic record sharing"),
	
	/**
	 * Consent given for electronic record sharing with carer
	 */
	_Consentgivenforelectronicrecordsharingwithcarer ("342591000000108", "Consent given for electronic record sharing with carer"),
	
	/**
	 * Consent given for upload to local shared electronic record
	 */
	_Consentgivenforuploadtolocalsharedelectronicrecord ("417528008", "Consent given for upload to local shared electronic record"),
	
	/**
	 * Consent given for upload to national shared electronic record
	 */
	_Consentgivenforuploadtonationalsharedelectronicrecord ("417370002", "Consent given for upload to national shared electronic record"),
	
	/**
	 * Consent given to share patient data with specified third party
	 */
	_Consentgiventosharepatientdatawithspecifiedthirdparty ("319951000000105", "Consent given to share patient data with specified third party"),
	
	/**
	 * Declined consent to share patient data with specified third party
	 */
	_Declinedconsenttosharepatientdatawithspecifiedthirdparty ("320011000000108", "Declined consent to share patient data with specified third party"),
	
	/**
	 * Refused consent for upload to local shared electronic record
	 */
	_Refusedconsentforuploadtolocalsharedelectronicrecord ("416409005", "Refused consent for upload to local shared electronic record"),
	
	/**
	 * Refused consent for upload to national shared electronic record
	 */
	_Refusedconsentforuploadtonationalsharedelectronicrecord ("416308001", "Refused consent for upload to national shared electronic record"),
	
	;
	
	public final String code;
	public final String displayName;
	public final String oid="2.16.840.1.113883.2.1.3.2.4.15";

	DocumentConsentSnCT(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	public String getOID() {
		return oid;
	}
	
	public static DocumentConsentSnCT getByCode(String code) {
		DocumentConsentSnCT[] vals = values();
		for (DocumentConsentSnCT val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(DocumentConsentSnCT other) {
		return (other.getCode().equals(code) && other.getOID().equals(oid));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
	