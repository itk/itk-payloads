/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.vocabularies.internal;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

public enum FHIRPersonNameType implements VocabularyEntry {

	/** Known as/conventional/the one you normally use. */
	usual("usual"),
	/** The formal name as registered in an official (government) registry, but which name might not be commonly used. May
	  * be called "legal name". */
	official("official"),
	/** A temporary name. Name.period can provide more detailed information. This may also be used for temporary names
	  * assigned at birth or in emergency situations. */
	temp("temp"),
	/** A name that is used to address the person in an informal manner, but is not part of their formal or usual name. */
	nickname("nickname"),
	/** Anonymous assigned name, alias, or pseudonym (used to protect a person's identity for privacy reasons). */
	anonymous("anonymous"),
	/** This name is no longer in use (or was never correct, but retained for records). */
	old("old"),
	/** A name used prior to marriage. Marriage naming customs vary greatly around the world. This name use is for use by
	  * applications that collect and store "maiden" names. Though the concept of maiden name is often gender specific, the
	  * use of this term is not gender specific. The use of this term does not imply any particular history for a person's
	  * name, nor should the maiden name be determined algorithmically. */
	maiden("maiden");
	
	public String code;
	
	private FHIRPersonNameType(String code) {
		this.code = code;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getOID() {
		return null;
	}

	public static FHIRPersonNameType getByCode(String code) {
		FHIRPersonNameType[] vals = values();
		for (FHIRPersonNameType val : vals) {
			if (val.getCode().equals(code)) {
				return val;
			}
		}
		return null;
	}
	
	public boolean sameAs(FHIRPersonNameType other) {
		return (other.getCode().equals(code));
	}
	
}
