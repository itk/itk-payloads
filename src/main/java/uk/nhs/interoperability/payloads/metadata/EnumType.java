package uk.nhs.interoperability.payloads.metadata;

public enum EnumType {
	internal, generated;
}
