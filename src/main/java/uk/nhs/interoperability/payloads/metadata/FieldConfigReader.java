/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.metadata;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.xml.ParseUtils;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public class FieldConfigReader extends DefaultHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(FieldConfigReader.class);
	
	private Map<String, Field> fieldList = new LinkedHashMap<String, Field>();
	private Field fieldTemp;
	private String className;
	private String classNameTemp;
	private String fieldNameTemp;
	private Set<String> xpaths = new HashSet<String>();
	private XMLNamespaceContext namespaces;
	private StringBuilder currentText;
	
	private FieldConfigReader(XMLNamespaceContext namespaces, String className) {
		this.className = className;
		this.namespaces = namespaces;
	}
	
	public static Map<String, Field> loadConfig(XMLNamespaceContext namespaces, String filename, String className) {
		logger.trace("Loading message metadata from file: {}", filename);

		String fileContent = FileLoader.loadFileOnClasspath("/"+filename);

		// Add any "included" xml into the config
		fileContent = ParseUtils.addIncludes(fileContent);
		FileWriter.writeFile("fullConfigTEMP.xml", fileContent.getBytes());
		ByteArrayInputStream bis = new ByteArrayInputStream(fileContent.getBytes());
		
		FieldConfigReader reader = new FieldConfigReader(namespaces, className);
		SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser;
		try {
			parser = factory.newSAXParser();
			parser.parse(bis, reader);
		} catch (ParserConfigurationException e) {
			logger.error("Error parsing message metadata from file: {}", filename, e);
		} catch (SAXException e) {
			logger.error("Error parsing message metadata from file: {}", filename, e);
		} catch (IOException e) {
			logger.error("Error parsing message metadata from file: {}", filename, e);
		}
		
		setDependentFixedFields(reader.getFieldList());
		
		return reader.getFieldList();
	}
	
	private Map<String, Field> getFieldList() {
		return fieldList;
	}
	
	@Override
    public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {
		if (elementName.equalsIgnoreCase("class")) {
        	classNameTemp = attributes.getValue("name");
        }
		if (elementName.equalsIgnoreCase("ifValue")) {
        	fieldNameTemp = attributes.getValue("field");
        }
		if (elementName.equalsIgnoreCase("field")) {
        	fieldTemp = new Field();
        }
    }
    @Override
    public void endElement(String s, String s1, String element) throws SAXException {

    	/*Logger.trace("FIELD VALUE = '" + ((currentText==null)?"null":currentText.toString()) +
    				"' for element name '" + element +
    				"' for class name: '" + classNameTemp +
    				"' and field name: " + ((fieldTemp==null)?"null":fieldTemp.getName()));
    	*/
    	
    	if (element.equals("field")) {
    		if (classNameTemp.equals(className)) {
    			// Do check for duplicate xpaths
    			if (xpaths.contains(fieldTemp.getXpath())) {
            		fieldTemp.setDuplicateXPath(true);
            	} else {
            		xpaths.add(fieldTemp.getXpath());
            	}
    			if (fieldList.containsKey(fieldTemp.getName())) {
    				logger.error("The field name {} already exists - IGNORING - please fix configuration file!!", fieldTemp.getName());
    				throw new SAXException("The field name " + fieldTemp.getName() + " already exists - IGNORING - please fix configuration file!!");
    			} else {
    				fieldList.put(fieldTemp.getName(), fieldTemp);
    			}
    			fieldTemp = null;
    		} else {
    			//Logger.trace(" Ignoring field for class " + classNameTemp + " - we want class " + className);
    		}
    		currentText = null;
        }
    	
    	// The remainder relate to fields, so if this element is not part of a field, exit now
    	if (fieldTemp == null || currentText == null) {
    		currentText = null;
    		return;
    	}
    	
    	if (element.equalsIgnoreCase("xpath")) {
        	fieldTemp.setXpath(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("name")) {
        	fieldTemp.setName(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("description")) {
   			fieldTemp.setDescription(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("mandatory")) {
        	if (currentText.toString().equals("true")) {
        		fieldTemp.setMandatory(true);
        	} else {
        		fieldTemp.setMandatory(false);
        	}
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("addAtStart")) {
        	if (currentText.toString().equals("true")) {
        		fieldTemp.setAddAtStart(true);
        	}
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("vocabulary")) {
        	fieldTemp.setVocabulary(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("type")) {
        	fieldTemp.setType(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("maxOccurs")) {
    		fieldTemp.setMaxOccurs(Integer.parseInt(currentText.toString()));
    		currentText = null;
        }
    	if (element.equalsIgnoreCase("format")) {
        	fieldTemp.setFormat(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("fixed")) {
        	fieldTemp.setFixed(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("ifExists")) {
        	fieldTemp.setIfExists(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("ifNotExists")) {
        	fieldTemp.setIfNotExists(currentText.toString());
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("ifValue")) {
        	fieldTemp.setIfValue(currentText.toString());
        	fieldTemp.setIfValueField(fieldNameTemp);
        	currentText = null;
        }
    	if (element.equalsIgnoreCase("localCodeOID")) {
    		fieldTemp.setLocalCodeOID(currentText.toString());
    		//fieldTemp.generateXPaths(namespaces);
    		currentText = null;
    	}
    	if (element.equalsIgnoreCase("deriveOIDFrom")) {
    		fieldTemp.setDeriveOIDFrom(currentText.toString());
    		currentText = null;
    	}
    	if (element.equalsIgnoreCase("deriveValueFromTemplateNameUsedInField")) {
    		fieldTemp.setDeriveValueFromTemplateNameUsedInField(currentText.toString());
    		currentText = null;
    	}
    	if (element.equalsIgnoreCase("versionedIdentifierXPath")) {
    		fieldTemp.setVersionedIdentifierXPath(currentText.toString());
    		currentText = null;
    	}
    	//if (element.equalsIgnoreCase("nullFlavour")) {
    	//	currentText = null;
    	//}
    	if (element.equalsIgnoreCase("suppressCodeSystem")) {
        	fieldTemp.setSuppressCodeSystem(currentText.toString().equalsIgnoreCase("true"));
        	currentText = null;
        }

    	currentText = null;
    }
    
    /*@Override
    public void characters(char[] ac, int i, int j) throws SAXException {
        currentText = new String(ac, i, j);
    }*/
    @Override
    public void characters(char[] ch, int start, int length)
    {
       if(length == 0) return;
       int end = (start + length) - 1;
       while(ch[start] <= '\u0020')
       {
           if(start == end) return;
           start++;
           length--;
       }
       while(ch[end] <= '\u0020')
       {
           if(end == start) return;
           length--;
           end--;
       }
       if (currentText == null)
		{
			currentText = new StringBuilder(length);
		}
       currentText.append(ch, start, length);
    }
    /*@Override
	public void characters(@SuppressWarnings("StandardVariableNames") final char[] ch, final int start, final int length) {
		if (currentText == null)
		{
			currentText = new StringBuilder(length);
		}
		currentText.append(ch, start, length);
	}*/
    
    private static void setDependentFixedFields(Map<String, Field> fieldList) {
    	for (String fieldName: fieldList.keySet()) {
    		Field f = fieldList.get(fieldName);
    		if (f.isDuplicateXPath()) {
    			// This is a duplicate, so see if we have a fixed field that depends on it, so we can
    			// decide whether to initialise it with a value when parsing
    			for (String fName2: fieldList.keySet()) {
	    			Field f2 = fieldList.get(fName2);
					if (f2.getIfExists() != null) {
	    				if (f2.getIfExists().equalsIgnoreCase(fieldName)) {
	    					f.setDependentFixedField(fName2);
	    				}
					}
    			}
    		}
    	}
    }
}
