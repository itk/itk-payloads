/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.metadata;

public class PayloadClassInfo {
	private String packageName;
	private String className;
	private String versionedName;
	private String rootNode;
	private String[] interfaces;
	
	public PayloadClassInfo(String packageName, String className, String versionedName, String rootNode, String[] interfaces) {
		this.packageName = packageName;
		this.className = className;
		this.versionedName = versionedName;
		this.rootNode = rootNode;
		this.interfaces = interfaces;
	}
	
	public String getPackageName() {
		return packageName;
	}
	public String getClassName() {
		return className;
	}
	public String getVersionedName() {
		return versionedName;
	}
	public String getRootNode() {
		return rootNode;
	}
	public String[] getInterfaces() {
		return interfaces;
	}
}
