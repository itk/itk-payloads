/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.metadata;

import java.util.HashSet;
import java.util.Map;

import javax.xml.xpath.XPathExpression;

import uk.nhs.interoperability.payloads.Code;
import uk.nhs.interoperability.payloads.FieldType;
import uk.nhs.interoperability.payloads.util.FieldHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.DOMFieldHandler;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

public class Field {
	private String name;
	private String xpath;
	private String description;
	private boolean mandatory;
	private boolean addAtStart = false;
	private String vocabulary;
	private String type;
	private String typePackage;
	private int maxOccurs = 1;
	private String format;
	private String fixed;
	private String ifExists;
	private String ifNotExists;
	private String ifValueField;
	private String ifValue;
	private XPathExpression compiledXpath;
	private boolean isDuplicateXPath = false;
	private String dependentFixedField;
	private String localCodeOID;
	private String deriveOIDFrom;
	private String deriveValueFromTemplateNameUsedInField;
	private String versionedIdentifierXPath;
	private String versionedIdentifierField;
	private boolean suppressCodeSystem = false;
	private boolean suppressDisplayName = false;
	private String expectedExpressionCode = null;
	private String expectedExpressionDisplayName = null;
	
	/**
	 * Constructor for non-fixed fields
	 * @param name Field Name
	 * @param xpath Field xpath
	 * @param description Field description
	 * @param mandatory Is mandatory
	 * @param addAtStart Should be added at start of parent element
	 * @param vocabulary Vocab to populate field with
	 * @param type Field type
	 * @param typePackage Package field type is in
	 * @param maxOccurs Multiplicity
	 * @param localCodeOID OID to use for local codes
	 * @param deriveOIDFrom Child template to use to select appropriate OID
	 * @param versionedIdentifierXPath Xpath to identify child template when several can be used
	 * @param versionedIdentifierField Field to identify child template when several can be used
	 * @param suppressCodeSystem Flag to indicate that the code system should be suppressed
	 * @param suppressDisplayName Flag to indicate that the display name should be suppressed
	 * @param expectedExpressionCode Expected top level code expected in SNOMED compositional statement
	 * @param expectedExpressionDisplayName Display name for expected top level code in SNOMED compositional statement 
	 */
	public Field(String name, String xpath, String description,
			String mandatory, String addAtStart, String vocabulary,
			String type, String typePackage, String maxOccurs,
			String localCodeOID, String deriveOIDFrom,
			String versionedIdentifierXPath, String versionedIdentifierField,
			String suppressCodeSystem, String suppressDisplayName,
			String expectedExpressionCode, String expectedExpressionDisplayName) {
		if (name.length()>0) setName(name);
		if (xpath.length()>0) setXpath(xpath);
		if (description.length()>0) setDescription(description);
		if (mandatory.length()>0) setMandatory(Boolean.parseBoolean(mandatory));
		if (addAtStart.length()>0) setAddAtStart(Boolean.parseBoolean(addAtStart));
		if (vocabulary.length()>0) setVocabulary(vocabulary);
		if (type.length()>0) setType(type);
		if (typePackage.length()>0) setTypePackage(typePackage);
		if (maxOccurs.length()>0) setMaxOccurs(Integer.parseInt(maxOccurs));
		if (localCodeOID.length()>0) setLocalCodeOID(localCodeOID);
		if (deriveOIDFrom.length()>0) setDeriveOIDFrom(deriveOIDFrom);
		if (versionedIdentifierXPath.length()>0) setVersionedIdentifierXPath(versionedIdentifierXPath);
		if (versionedIdentifierField.length()>0) setVersionedIdentifierField(versionedIdentifierField);
		if (suppressCodeSystem.length()>0) setSuppressCodeSystem(Boolean.parseBoolean(suppressCodeSystem));
		if (suppressDisplayName.length()>0) setSuppressDisplayName(Boolean.parseBoolean(suppressDisplayName));
		if (expectedExpressionCode.length()>0) setExpectedExpressionCode(expectedExpressionCode);
		if (expectedExpressionDisplayName.length()>0) setExpectedExpressionDisplayName(expectedExpressionDisplayName);
	}

	/**
	 * Constructor for fixed fields 
	 * @param name Field name
	 * @param xpath Field xpath
	 * @param fixed Fixed field
	 * @param ifExists If exists condition
	 * @param ifNotExists If not exists condition
	 * @param ifValueField If value condition
	 * @param ifValue If value
	 * @param deriveValueFromTemplateNameUsedInField Field to use to derive value from
	 * @param addAtStart Element should be added as first child of parent element
	 */
	public Field(String name, String xpath, String fixed, String ifExists,
			String ifNotExists, String ifValueField, String ifValue,
			String deriveValueFromTemplateNameUsedInField, String addAtStart) {
		if (name.length()>0) setName(name);
		if (xpath.length()>0) setXpath(xpath);
		if (fixed.length()>0) setFixed(fixed);
		if (ifExists.length()>0) setIfExists(ifExists);
		if (ifNotExists.length()>0) setIfNotExists(ifNotExists);
		if (ifValueField.length()>0) setIfValueField(ifValueField);
		if (ifValue.length()>0) setIfValue(ifValue);
		if (deriveValueFromTemplateNameUsedInField.length()>0) setDeriveValueFromTemplateNameUsedInField(deriveValueFromTemplateNameUsedInField);
		if (addAtStart.length()>0) setAddAtStart(Boolean.parseBoolean(addAtStart));
	}
	
	/**
	 * Default constructor
	 */
	public Field() {
	}
	
	public boolean isSuppressCodeSystem() {
		return suppressCodeSystem;
	}
	public void setSuppressCodeSystem(boolean suppressCodeSystem) {
		this.suppressCodeSystem = suppressCodeSystem;
	}
	// Derived Fields
	private XPathExpression compiledXpathCodeSystem;
	private XPathExpression compiledXpathCode;
	private XPathExpression compiledXpathDisplayName;
	private XPathExpression compiledXpathReference;
	private XPathExpression compiledVersionedIdentifierXPath;
	
	private FieldType typeEnum = null;

	public FieldType getTypeEnum() {
		return typeEnum;
	}
	public String getVersionedIdentifierXPath() {
		return versionedIdentifierXPath;
	}
	public XPathExpression getCompiledVersionedIdentifierXPath(XMLNamespaceContext namespaces) {
		if (compiledVersionedIdentifierXPath == null) {
			compiledVersionedIdentifierXPath = XPaths.compileXpath(namespaces, versionedIdentifierXPath);
		}
		return compiledVersionedIdentifierXPath;
	}
	public void setVersionedIdentifierXPath(String versionedIdentifierXPath) {
		this.versionedIdentifierXPath = versionedIdentifierXPath;
		// If we are setting this value, this must be a templated field
		this.typeEnum = FieldType.Templated;
	}
	public String getDeriveValueFromTemplateNameUsedInField() {
		return deriveValueFromTemplateNameUsedInField;
	}
	public void setDeriveValueFromTemplateNameUsedInField(
			String deriveValueFromTemplateNameUsedInField) {
		this.deriveValueFromTemplateNameUsedInField = deriveValueFromTemplateNameUsedInField;
	}
	public String getDeriveOIDFrom() {
		return deriveOIDFrom;
	}
	public void setDeriveOIDFrom(String deriveOIDFrom) {
		this.deriveOIDFrom = deriveOIDFrom;
	}
	public String getLocalCodeOID() {
		return localCodeOID;
	}
	public void setLocalCodeOID(String localCodeOID) {
		this.localCodeOID = localCodeOID;
	}
	public String getDependentFixedField() {
		return dependentFixedField;
	}
	public void setDependentFixedField(String dependentFixedField) {
		this.dependentFixedField = dependentFixedField;
	}
	public boolean isDuplicateXPath() {
		return isDuplicateXPath;
	}
	public void setDuplicateXPath(boolean isDuplicateXPath) {
		this.isDuplicateXPath = isDuplicateXPath;
	}
	public String getIfNotExists() {
		return ifNotExists;
	}
	public void setIfNotExists(String ifNotExists) {
		this.ifNotExists = ifNotExists;
	}
	public String getIfValueField() {
		return ifValueField;
	}
	public void setIfValueField(String ifValueField) {
		this.ifValueField = ifValueField;
	}
	public String getIfValue() {
		return ifValue;
	}
	public void setIfValue(String ifValue) {
		this.ifValue = ifValue;
	}
	public XPathExpression getCompiledXpath(XMLNamespaceContext namespaces) {
		if (compiledXpath == null) {
			compiledXpath = XPaths.compileXpath(namespaces, xpath);
		}
		return compiledXpath;
	}
	public String getIfExists() {
		return ifExists;
	}
	public void setIfExists(String ifExists) {
		this.ifExists = ifExists;
	}
	public String getFixed() {
		return fixed;
	}
	public void setFixed(String fixed) {
		this.fixed = fixed;
		this.typeEnum = FieldType.Fixed;
		this.type = "Fixed";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getXpath() {
		return xpath;
	}
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isMandatory() {
		return mandatory;
	}
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	public boolean isAddAtStart() {
		return addAtStart;
	}
	public void setAddAtStart(boolean addAtStart) {
		this.addAtStart = addAtStart;
	}
	public String getVocabulary() {
		return vocabulary;
	}
	public void setVocabulary(String vocabulary) {
		this.vocabulary = vocabulary;
	}
	public String getTypeName() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
		if (type != null && (typeEnum == null)) {
			if (type.equalsIgnoreCase("String")) {
				typeEnum = FieldType.String;
			} else if (type.equalsIgnoreCase("XML")) {
				typeEnum = FieldType.XML;
			} else if (type.equalsIgnoreCase("HL7Date")) {
				typeEnum = FieldType.HL7Date;
			} else if (type.equalsIgnoreCase("CodedValue")) { 
				typeEnum = FieldType.CodedValue;
			} else if (type.equalsIgnoreCase("FHIRCodedValue")) { 
				typeEnum = FieldType.FHIRCodedValue;
			} else if (type.equalsIgnoreCase("CompositionalStatement")) { 
				typeEnum = FieldType.CompositionalStatement;
			} else {
				typeEnum = FieldType.Other;
			}
		}
	}
	public String getTypePackage() {
		return typePackage;
	}
	public void setTypePackage(String typePackage) {
		this.typePackage = typePackage;
	}
	public int getMaxOccurs() {
		return maxOccurs;
	}
	public void setMaxOccurs(int maxOccurs) {
		this.maxOccurs = maxOccurs;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public XPathExpression getCompiledXpathCodeSystem(XMLNamespaceContext namespaces) {
		if (compiledXpathCodeSystem == null) {
			this.compiledXpathCodeSystem = XPaths.compileXpath(namespaces, "@codeSystem");
		}
		return compiledXpathCodeSystem;
	}
	public XPathExpression getCompiledXpathCode(XMLNamespaceContext namespaces) {
		if (compiledXpathCode == null) {
			this.compiledXpathCode = XPaths.compileXpath(namespaces, "@code");
		}
		return compiledXpathCode;
	}
	public XPathExpression getCompiledXpathDisplayName(XMLNamespaceContext namespaces) {
		if (compiledXpathDisplayName == null) {
			this.compiledXpathDisplayName = XPaths.compileXpath(namespaces, "@displayName");
		}
		return compiledXpathDisplayName;
	}
	public XPathExpression getCompiledXpathReference(XMLNamespaceContext namespaces) {
		if (compiledXpathReference == null) {
			this.compiledXpathReference = XPaths.compileXpath(namespaces, "x:originalText/x:reference/@value");
		}
		return compiledXpathReference;
	}
	public DOMFieldHandler getHandler() {
		return (DOMFieldHandler)this.typeEnum.getHandler();
	}
	
	public static void setDependentFixedFields(Map<String, Field> fieldList) {
    	// Check for duplicate xpaths
		HashSet<String> keys = new HashSet<String>();
		for (String fieldName: fieldList.keySet()) {
			Field f = fieldList.get(fieldName);
			if (keys.contains(f.getXpath())) {
				f.setDuplicateXPath(true);
    			// This is a duplicate, so see if we have a fixed field that depends on it, so we can
    			// decide whether to initialise it with a value when parsing
    			for (String fName2: fieldList.keySet()) {
	    			Field f2 = fieldList.get(fName2);
					if (f2.getIfExists() != null) {
	    				if (f2.getIfExists().equalsIgnoreCase(fieldName)) {
	    					f.setDependentFixedField(fName2);
	    				}
					}
    			}
			}
			keys.add(f.getXpath());
		}
    }

	public boolean isSuppressDisplayName() {
		return suppressDisplayName;
	}

	public void setSuppressDisplayName(boolean suppressDisplayName) {
		this.suppressDisplayName = suppressDisplayName;
	}

	public String getVersionedIdentifierField() {
		return versionedIdentifierField;
	}

	public void setVersionedIdentifierField(String versionedIdentifierField) {
		this.versionedIdentifierField = versionedIdentifierField;
	}

	public String getExpectedExpressionCode() {
		return expectedExpressionCode;
	}

	public void setExpectedExpressionCode(String expectedExpressionCode) {
		this.expectedExpressionCode = expectedExpressionCode;
	}

	public String getExpectedExpressionDisplayName() {
		return expectedExpressionDisplayName;
	}

	public void setExpectedExpressionDisplayName(
			String expectedExpressionDisplayName) {
		this.expectedExpressionDisplayName = expectedExpressionDisplayName;
	}
}
