package uk.nhs.interoperability.payloads.metadata;


public class VocabClassInfo {
	private String vocabName;
	private String oid;
	private boolean internal = false;
	
	public String getVocabName() {
		return vocabName;
	}
	public void setVocabName(String vocabName) {
		this.vocabName = vocabName;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public boolean isInternal() {
		return internal;
	}
	public void setInternal(boolean internal) {
		this.internal = internal;
	}
}
