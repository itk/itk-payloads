/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import static uk.nhs.interoperability.payloads.util.Emptiables.isNullOrEmpty;

import java.util.LinkedHashMap;

import uk.nhs.interoperability.payloads.util.Emptiable;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * This is a simple implementation of a SNOMED CT "compositional grammar" as defined in certain
 * ITK domain message specifications.
 * NOTE: This is not a full implementation of the compositional grammar specification from IHTSDO, rather
 * it is sufficient to meet the requirements specific in the ITK domain message specifications only.
 * @author Adam Hatherly
 */
public class CompositionalStatement implements Emptiable {
	
	// All SNOMED CT codes within a SNOMED CT compositional statement will have the SNOMED CT OID
	public static final String OID = PropertyReader.getProperty("nationalSnomedOID");
	public static final String COMPOSITIONAL_OID = PropertyReader.getProperty("nationalSnomedCompositionalStatementOID");
	
	private Code focusExpression;
	private LinkedHashMap<Code, Code> codes;
	
	/**
	 * Initialise with a list of codes to form a compositional statement
	 * @param focusExpression Main code for the statement
	 * @param refinementAttributes One or more refinement attributes
	 */
	public CompositionalStatement(Code focusExpression, LinkedHashMap<Code, Code> refinementAttributes) {
		this.focusExpression = focusExpression;
		this.codes = refinementAttributes;
	}
	
	/**
	 * Initialise with just the focus expression (add the additional codes afterwards).
	 * @param focusExpression Main code for the statement
	 */
	public CompositionalStatement(Code focusExpression) {
		this.focusExpression = focusExpression;
	}
	
	/**
	 * Initialise with just the focus expression taken from a vocabulary (add the additional codes afterwards).
	 * @param focusExpression Main vocabulary entry for the statement
	 */
	public CompositionalStatement(VocabularyEntry focusExpression) {
		this.focusExpression = new CodedValue(focusExpression);
	}
	
	/**
	 * Default Constructor
	 */
	public CompositionalStatement() {
	}
	
	/**
	 * Add another attribute to this compositional statement.
	 * @param attributeCode Code identifying the attribute
	 * @param valueCode Code with the value for the attribute
	 * @return this to allow the builder pattern to be used
	 */
	public CompositionalStatement addAttributeCodeAndValueCode(Code attributeCode, Code valueCode) {
		if (codes == null) {
			codes = new LinkedHashMap<Code, Code>();
		}
		codes.put(attributeCode, valueCode);
		return this;
	}
	
	/**
	 * Add another attribute to this compositional statement.
	 * @param attributeCode Code identifying the attribute
	 * @param valueCode Code with the value for the attribute
	 * @return this to allow the builder pattern to be used
	 */
	public CompositionalStatement addAttributeCodeAndValueCode(VocabularyEntry attributeCode, Code valueCode) {
		if (codes == null) {
			codes = new LinkedHashMap<Code, Code>();
		}
		codes.put(new CodedValue(attributeCode), valueCode);
		return this;
	}
	
	/**
	 * Add another attribute to this compositional statement.
	 * @param attributeCode Code identifying the attribute
	 * @param valueCode Code with the value for the attribute
	 * @return this to allow the builder pattern to be used
	 */
	public CompositionalStatement addAttributeCodeAndValueCode(VocabularyEntry attributeCode, VocabularyEntry valueCode) {
		if (codes == null) {
			codes = new LinkedHashMap<Code, Code>();
		}
		codes.put(new CodedValue(attributeCode), new CodedValue(valueCode));
		return this;
	}
	
	/**
	 * Add a list of codes to this compositional statement
	 * @return this to allow the builder pattern to be used
	 */
	public CompositionalStatement addCodes(LinkedHashMap<Code, Code> refinementAttributes) {
		if (this.codes == null) {
			this.codes = new LinkedHashMap<Code, Code>();
		}
		codes.putAll(refinementAttributes);
		return this;
	}
	
	/**
	 * Get the ordered list of codes in this compositional statement
	 * @return the ordered list of codes
	 */
	public LinkedHashMap<Code, Code> getRefinementCodes() {
		return this.codes;
	}
	
	/**
	 * Get the value code for a specific attribute code (if it exists in the compositional statement)
	 * @param attributeCode Attribute code to look for
	 * @return Value code or null if the attribute code doesn't exist in the compositional statement 
	 */
	public Code getValueForAttributeCode(Code attributeCode) {
		for (Code code : this.codes.keySet()) {
			if (code.sameAs((CodedValue)attributeCode)) {
				return this.codes.get(code);
			}
		}
		return null;
	}
	
	/**
	 * Get the value code for a specific vocabulary entry (if it exists in the compositional statement)
	 * @param attributeCode Attribute code to look for
	 * @return Value code or null if the attribute code doesn't exist in the compositional statement
	 */
	public Code getValueForAttributeCode(VocabularyEntry attributeCode) {
		for (Code code : this.codes.keySet()) {
			if (code.sameAs(attributeCode)) {
				return this.codes.get(code);
			}
		}
		return null;
	}

	@Override
	public boolean isEmpty() {
		return isNullOrEmpty(codes);
	}

	/**
	 * Return the main "focus" expression code for this compositional grammar
	 * @return the coded item
	 */
	public Code getFocusExpression() {
		return focusExpression;
	}

	/**
	 * Set the main "focus" expression code for this compositional grammar
	 * @param focusExpression the main code to set
	 */
	public void setFocusExpression(Code focusExpression) {
		this.focusExpression = focusExpression;
	}
	
	/**
	 * Set the main "focus" expression code for this compositional grammar using a vocab entry
	 * @param focusExpression the main code to set
	 */
	public void setFocusExpression(VocabularyEntry focusExpression) {
		this.focusExpression = new CodedValue(focusExpression);
	}
	
	/**
	 * Codes are serialised as follows:
	 * expression:attributeCode=valueCode[,attributeCode=valueCode]*
	 * 
	 * Display names are serialised as follows:
	 * expression|displayName|:attributeCode|displayName|=valueCode|displayName|[,attributeCode|displayName|=valueCode|displayName|]*
	 */
	@Override
	public String toString() {
		return displayString();
	}
	
	/**
	 * Outputs a human-readable version of the compositional statement - useful for debugging
	 * @return Human readable version of statement content
	 */
	public String displayString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Compositional Statement - Focus Concept: ")
			.append(this.focusExpression.toString())
			.append("\n   - Attributes [");
		
		for (Code attributeName : this.codes.keySet()) {
			Code attributeValue = this.codes.get(attributeName);
			sb.append("\n       ");
			printCode(sb, attributeName);
			sb.append(" = ");
			printCode(sb, attributeValue);
		}
		sb.append("]");
		return sb.toString();
	}
	
	private static void printCode(StringBuilder sb, Code c) {
		sb.append("{").append(c.getCode()).append("|").append(c.getDisplayName()).append("}");
	}
	
}
