/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Recipient object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} RecipientAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} JobRoleName</li>
 * <li>String Telephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} Name</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgID</li>
 * <li>String OrgName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Recipient extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "notificationFieldConfig";
		protected static final String name = "Recipient";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.notification";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param RecipientAddress RecipientAddress
		 * @param JobRoleName JobRoleName
		 * @param Telephone Telephone
		 * @param Name Name
		 * @param OrgID OrgID
		 * @param OrgName OrgName
		 */
	    public Recipient(uk.nhs.interoperability.payloads.commontypes.Address RecipientAddress, CodedValue JobRoleName, String Telephone, uk.nhs.interoperability.payloads.commontypes.PersonName Name, uk.nhs.interoperability.payloads.commontypes.OrgID OrgID, String OrgName) {
			fields = new LinkedHashMap<String, Object>();
			
			setRecipientAddress(RecipientAddress);
			setJobRoleName(JobRoleName);
			setTelephone(Telephone);
			setName(Name);
			setOrgID(OrgID);
			setOrgName(OrgName);
		}
	
		/**
		 * Recipient address
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getRecipientAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("RecipientAddress");
		}
		
		
		
		
		/**
		 * Recipient address
		 * <br><br>This field is MANDATORY
		 * @param RecipientAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setRecipientAddress(uk.nhs.interoperability.payloads.commontypes.Address RecipientAddress) {
			setValue("RecipientAddress", RecipientAddress);
			return this;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return CodedValue object
		 */	
		public CodedValue getJobRoleName() {
			return (CodedValue)getValue("JobRoleName");
		}
		
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("JobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * @param JobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setJobRoleName(CodedValue JobRoleName) {
			setValue("JobRoleName", JobRoleName);
			return this;
		}
		
		
		/**
		 * This is an identifier for the Job Role of the Recipient
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param JobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setJobRoleName(VocabularyEntry JobRoleName) {
			Code c = new CodedValue(JobRoleName);
			setValue("JobRoleName", c);
			return this;
		}
		
		/**
		 * Telephone number of the recipient
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getTelephone() {
			return (String)getValue("Telephone");
		}
		
		
		
		
		/**
		 * Telephone number of the recipient
		 * <br><br>This field is MANDATORY
		 * @param Telephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setTelephone(String Telephone) {
			setValue("Telephone", Telephone);
			return this;
		}
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * ID of contacts organisation (ODS Code)
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgID");
		}
		
		
		
		
		/**
		 * ID of contacts organisation (ODS Code)
		 * @param OrgID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setOrgID(uk.nhs.interoperability.payloads.commontypes.OrgID OrgID) {
			setValue("OrgID", OrgID);
			return this;
		}
		
		
		/**
		 * Name of contacts organisation
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * Name of contacts organisation
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Recipient setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("RecipientTypeCode", new Field(
												"RecipientTypeCode",
												"@typeCode",
												"IRCP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecipientContentIDRoot", new Field(
												"RecipientContentIDRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecipientContentIDExtension", new Field(
												"RecipientContentIDExtension",
												"npfitlc:contentId/@extension",
												"COCD_TP145218GB01#IntendedRecipient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecipientClassCode", new Field(
												"RecipientClassCode",
												"x:COCD_TP145218GB01.IntendedRecipient/@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecipientAddress", new Field(
												"RecipientAddress",
												"x:COCD_TP145218GB01.IntendedRecipient/x:addr",
												"Recipient address",
												"true",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("JobRoleName", new Field(
												"JobRoleName",
												"x:COCD_TP145218GB01.IntendedRecipient/x:code",
												"This is an identifier for the Job Role of the Recipient",
												"false",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Telephone", new Field(
												"Telephone",
												"x:COCD_TP145218GB01.IntendedRecipient/x:telecom/@value",
												"Telephone number of the recipient",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecipientTemplateIdRoot", new Field(
												"RecipientTemplateIdRoot",
												"x:COCD_TP145218GB01.IntendedRecipient/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RecipientTemplateIdExtension", new Field(
												"RecipientTemplateIdExtension",
												"x:COCD_TP145218GB01.IntendedRecipient/x:templateId/@extension",
												"COCD_TP145218GB01#IntendedRecipient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameClassCode", new Field(
												"PersonNameClassCode",
												"x:COCD_TP145218GB01.IntendedRecipient/x:assignedPerson/@classCode",
												"PSN",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameDeterminerCode", new Field(
												"PersonNameDeterminerCode",
												"x:COCD_TP145218GB01.IntendedRecipient/x:assignedPerson/@determinerCode",
												"INSTANCE",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:COCD_TP145218GB01.IntendedRecipient/x:assignedPerson/x:name",
												"Patient Name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameTemplateRoot", new Field(
												"PersonNameTemplateRoot",
												"x:COCD_TP145218GB01.IntendedRecipient/x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameTemplateExtension", new Field(
												"PersonNameTemplateExtension",
												"x:COCD_TP145218GB01.IntendedRecipient/x:assignedPerson/x:templateId/@extension",
												"COCD_TP145218GB01#assignedPerson",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgClassCode", new Field(
												"ContactPersonOrgClassCode",
												"x:COCD_TP145218GB01.IntendedRecipient/x:representedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgDeterminerCode", new Field(
												"ContactPersonOrgDeterminerCode",
												"x:COCD_TP145218GB01.IntendedRecipient/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgID", new Field(
												"OrgID",
												"x:COCD_TP145218GB01.IntendedRecipient/x:representedOrganization/x:id",
												"ID of contacts organisation (ODS Code)",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:COCD_TP145218GB01.IntendedRecipient/x:representedOrganization/x:name",
												"Name of contacts organisation",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgTemplateRoot", new Field(
												"ContactPersonOrgTemplateRoot",
												"x:COCD_TP145218GB01.IntendedRecipient/x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgTemplateExtension", new Field(
												"ContactPersonOrgTemplateExtension",
												"x:COCD_TP145218GB01.IntendedRecipient/x:representedOrganization/x:templateId/@extension",
												"COCD_TP145218GB01#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Recipient() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Recipient(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	