/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the OriginatingSystem object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SystemID SystemID} SystemID</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgID</li>
 * <li>String OrgName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class OriginatingSystem extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "notificationFieldConfig";
		protected static final String name = "OriginatingSystem";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.notification";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param SystemID SystemID
		 * @param OrgID OrgID
		 * @param OrgName OrgName
		 */
	    public OriginatingSystem(uk.nhs.interoperability.payloads.commontypes.SystemID SystemID, uk.nhs.interoperability.payloads.commontypes.OrgID OrgID, String OrgName) {
			fields = new LinkedHashMap<String, Object>();
			
			setSystemID(SystemID);
			setOrgID(OrgID);
			setOrgName(OrgName);
		}
	
		/**
		 * The ID of the System that generated the event
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.SystemID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SystemID getSystemID() {
			return (uk.nhs.interoperability.payloads.commontypes.SystemID)getValue("SystemID");
		}
		
		
		
		
		/**
		 * The ID of the System that generated the event
		 * <br><br>This field is MANDATORY
		 * @param SystemID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public OriginatingSystem setSystemID(uk.nhs.interoperability.payloads.commontypes.SystemID SystemID) {
			setValue("SystemID", SystemID);
			return this;
		}
		
		
		/**
		 * The ODS Code of the organisation hosting the system that generated the event
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgID");
		}
		
		
		
		
		/**
		 * The ODS Code of the organisation hosting the system that generated the event
		 * <br><br>This field is MANDATORY
		 * @param OrgID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public OriginatingSystem setOrgID(uk.nhs.interoperability.payloads.commontypes.OrgID OrgID) {
			setValue("OrgID", OrgID);
			return this;
		}
		
		
		/**
		 * The name of the organisation hosting the system that generated the event
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * The name of the organisation hosting the system that generated the event
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public OriginatingSystem setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("OriginatingSystemTypeCode", new Field(
												"OriginatingSystemTypeCode",
												"@typeCode",
												"AUT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemContentIDRoot", new Field(
												"OriginatingSystemContentIDRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemContentIDExtension", new Field(
												"OriginatingSystemContentIDExtension",
												"npfitlc:contentId/@extension",
												"COCD_TP145231GB01#OriginatingSystem",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemClassCode", new Field(
												"OriginatingSystemClassCode",
												"x:COCD_TP145231GB01.OriginatingSystem/@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemCode", new Field(
												"OriginatingSystemCode",
												"x:COCD_TP145231GB01.OriginatingSystem/x:code/@code",
												"OSY",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemCodeSystem", new Field(
												"OriginatingSystemCodeSystem",
												"x:COCD_TP145231GB01.OriginatingSystem/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.205",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemCodeDisplayName", new Field(
												"OriginatingSystemCodeDisplayName",
												"x:COCD_TP145231GB01.OriginatingSystem/x:code/@displayName",
												"Originating System",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SystemID", new Field(
												"SystemID",
												"x:COCD_TP145231GB01.OriginatingSystem/x:id",
												"The ID of the System that generated the event",
												"true",
												"",
												"",
												"SystemID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemTemplateIDRoot", new Field(
												"OriginatingSystemTemplateIDRoot",
												"x:COCD_TP145231GB01.OriginatingSystem/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemTemplateIDExtension", new Field(
												"OriginatingSystemTemplateIDExtension",
												"x:COCD_TP145231GB01.OriginatingSystem/x:templateId/@extension",
												"COCD_TP145231GB01#OriginatingSystem",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOrgClassCode", new Field(
												"OriginatingSystemOrgClassCode",
												"x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/@classCode",
												"ORG",
												"OrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOrgDeterminerCode", new Field(
												"OriginatingSystemOrgDeterminerCode",
												"x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"OrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgID", new Field(
												"OrgID",
												"x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:id",
												"The ODS Code of the organisation hosting the system that generated the event",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:name",
												"The name of the organisation hosting the system that generated the event",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOrgTemplateIDRoot", new Field(
												"OriginatingSystemOrgTemplateIDRoot",
												"x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOrgTemplateIDExtension", new Field(
												"OriginatingSystemOrgTemplateIDExtension",
												"x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:templateId/@extension",
												"COCD_TP145231GB01#representedOrganization",
												"OrgID",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public OriginatingSystem() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public OriginatingSystem(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	