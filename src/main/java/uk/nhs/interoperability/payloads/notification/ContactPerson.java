/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ContactPerson object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>This is the contact to speak to for more information about the event. Any of the following "IDs" can be provided for the person:
				<br><br>If you are using SDS identifiers, provide both of:
					<ul><li>An SDS identifier for the person - populate this into the ContactPersonID field</li>
					<li>An SDS Role profile identifier for the person - populate this into the ContactPersonRoleProfileID field</li></ul>
				If you are using a local ID that has an OID:
					<ul><li>Populate ther ID into the ContactPersonLocalID field; AND either:
						<ul><li>If you have an OID, populate it into the ContactPersonLocalIDOID field, OR</li>
						<li>Populate an identifier for the organisation that assigned the ID into the ContactPersonLocalIDAssigningAuthority field in the form: ODSCode:ODSName, for example 'RA8:St Elsewhere's Hospital'</li>
						</ul></li>
					</ul>
		
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} PersonAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} JobRoleName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; ID</li>
 * <li>String Telephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DOB</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} Name</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OrgID</li>
 * <li>String OrgName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ContactPerson extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "notificationFieldConfig";
		protected static final String name = "ContactPerson";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.notification";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param PersonAddress PersonAddress
		 * @param JobRoleName JobRoleName
		 * @param ID ID
		 * @param Telephone Telephone
		 * @param DOB DOB
		 * @param Name Name
		 * @param OrgID OrgID
		 * @param OrgName OrgName
		 */
	    public ContactPerson(uk.nhs.interoperability.payloads.commontypes.Address PersonAddress, CodedValue JobRoleName, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> ID, String Telephone, HL7Date DOB, uk.nhs.interoperability.payloads.commontypes.PersonName Name, uk.nhs.interoperability.payloads.commontypes.OrgID OrgID, String OrgName) {
			fields = new LinkedHashMap<String, Object>();
			
			setPersonAddress(PersonAddress);
			setJobRoleName(JobRoleName);
			setID(ID);
			setTelephone(Telephone);
			setDOB(DOB);
			setName(Name);
			setOrgID(OrgID);
			setOrgName(OrgName);
		}
	
		/**
		 * Contact address lines
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getPersonAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("PersonAddress");
		}
		
		
		
		
		/**
		 * Contact address lines
		 * @param PersonAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setPersonAddress(uk.nhs.interoperability.payloads.commontypes.Address PersonAddress) {
			setValue("PersonAddress", PersonAddress);
			return this;
		}
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getJobRoleName() {
			return (CodedValue)getValue("JobRoleName");
		}
		
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("JobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @param JobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setJobRoleName(CodedValue JobRoleName) {
			setValue("JobRoleName", JobRoleName);
			return this;
		}
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param JobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setJobRoleName(VocabularyEntry JobRoleName) {
			Code c = new CodedValue(JobRoleName);
			setValue("JobRoleName", c);
			return this;
		}
		
		/**
		 * Unique ID(s) for the contact person
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getID() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("ID");
		}
		
		/**
		 * Unique ID(s) for the contact person
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setID(List ID) {
			setValue("ID", ID);
			return this;
		}
		
		/**
		 * Unique ID(s) for the contact person
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param ID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson addID(uk.nhs.interoperability.payloads.commontypes.PersonID ID) {
			addMultivalue("ID", ID);
			return this;
		}
		
		
		/**
		 * Contact telephone number
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getTelephone() {
			return (String)getValue("Telephone");
		}
		
		
		
		
		/**
		 * Contact telephone number
		 * <br><br>This field is MANDATORY
		 * @param Telephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setTelephone(String Telephone) {
			setValue("Telephone", Telephone);
			return this;
		}
		
		
		/**
		 * Patient date of birth
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getDOB() {
			return (HL7Date)getValue("DOB");
		}
		
		
		
		
		/**
		 * Patient date of birth
		 * <br><br>This field is MANDATORY
		 * @param DOB value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setDOB(HL7Date DOB) {
			setValue("DOB", DOB);
			return this;
		}
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setName(uk.nhs.interoperability.payloads.commontypes.PersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * ID of contacts organisation (ODS Code)
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOrgID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OrgID");
		}
		
		
		
		
		/**
		 * ID of contacts organisation (ODS Code)
		 * @param OrgID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setOrgID(uk.nhs.interoperability.payloads.commontypes.OrgID OrgID) {
			setValue("OrgID", OrgID);
			return this;
		}
		
		
		/**
		 * Name of contacts organisation
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getOrgName() {
			return (String)getValue("OrgName");
		}
		
		
		
		
		/**
		 * Name of contacts organisation
		 * <br><br>This field is MANDATORY
		 * @param OrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ContactPerson setOrgName(String OrgName) {
			setValue("OrgName", OrgName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ContactPersonTypeCode", new Field(
												"ContactPersonTypeCode",
												"@typeCode",
												"PRF",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonContentIDRoot", new Field(
												"ContactPersonContentIDRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonContentIDExtension", new Field(
												"ContactPersonContentIDExtension",
												"npfitlc:contentId/@extension",
												"COCD_TP145217GB01#ContactPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonClassCode", new Field(
												"ContactPersonClassCode",
												"x:COCD_TP145217GB01.ContactPerson/@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonAddress", new Field(
												"PersonAddress",
												"x:COCD_TP145217GB01.ContactPerson/x:addr",
												"Contact address lines",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("JobRoleName", new Field(
												"JobRoleName",
												"x:COCD_TP145217GB01.ContactPerson/x:code",
												"The type of role the role the contact is in",
												"true",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:COCD_TP145217GB01.ContactPerson/x:id",
												"Unique ID(s) for the contact person",
												"false",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"2",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Telephone", new Field(
												"Telephone",
												"x:COCD_TP145217GB01.ContactPerson/x:telecom/@value",
												"Contact telephone number",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonTemplateRoot", new Field(
												"ContactPersonTemplateRoot",
												"x:COCD_TP145217GB01.ContactPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonTemplateExtension", new Field(
												"ContactPersonTemplateExtension",
												"x:COCD_TP145217GB01.ContactPerson/x:templateId/@extension",
												"COCD_TP145217GB01#ContactPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameClassCode", new Field(
												"PersonNameClassCode",
												"x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameDeterminerCode", new Field(
												"PersonNameDeterminerCode",
												"x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DOB", new Field(
												"DOB",
												"x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:birthTime/@value",
												"Patient date of birth",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:name",
												"Patient Name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameTemplateRoot", new Field(
												"PersonNameTemplateRoot",
												"x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonNameTemplateExtension", new Field(
												"PersonNameTemplateExtension",
												"x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:templateId/@extension",
												"COCD_TP145217GB01#assignedPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgClassCode", new Field(
												"ContactPersonOrgClassCode",
												"x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgDeterminerCode", new Field(
												"ContactPersonOrgDeterminerCode",
												"x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgID", new Field(
												"OrgID",
												"x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:id",
												"ID of contacts organisation (ODS Code)",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgName", new Field(
												"OrgName",
												"x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:name",
												"Name of contacts organisation",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgTemplateRoot", new Field(
												"ContactPersonOrgTemplateRoot",
												"x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPersonOrgTemplateExtension", new Field(
												"ContactPersonOrgTemplateExtension",
												"x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:templateId/@extension",
												"COCD_TP145217GB01#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ContactPerson() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ContactPerson(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

