/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Patient object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} PatientAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom} PatientTelephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PatientID PatientID} PatientNhsNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} PatientDOB</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; PatientName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Patient extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "notificationFieldConfig";
		protected static final String name = "Patient";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.notification";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param PatientAddress PatientAddress
		 * @param PatientTelephone PatientTelephone
		 * @param PatientNhsNumber PatientNhsNumber
		 * @param PatientDOB PatientDOB
		 * @param PatientName PatientName
		 */
	    public Patient(uk.nhs.interoperability.payloads.commontypes.Address PatientAddress, uk.nhs.interoperability.payloads.commontypes.Telecom PatientTelephone, uk.nhs.interoperability.payloads.commontypes.PatientID PatientNhsNumber, HL7Date PatientDOB, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> PatientName) {
			fields = new LinkedHashMap<String, Object>();
			
			setPatientAddress(PatientAddress);
			setPatientTelephone(PatientTelephone);
			setPatientNhsNumber(PatientNhsNumber);
			setPatientDOB(PatientDOB);
			setPatientName(PatientName);
		}
	
		/**
		 * Patient address
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getPatientAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("PatientAddress");
		}
		
		
		
		
		/**
		 * Patient address
		 * @param PatientAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patient setPatientAddress(uk.nhs.interoperability.payloads.commontypes.Address PatientAddress) {
			setValue("PatientAddress", PatientAddress);
			return this;
		}
		
		
		/**
		 * Patient telephone number
		 * @return uk.nhs.interoperability.payloads.commontypes.Telecom object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Telecom getPatientTelephone() {
			return (uk.nhs.interoperability.payloads.commontypes.Telecom)getValue("PatientTelephone");
		}
		
		
		
		
		/**
		 * Patient telephone number
		 * @param PatientTelephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patient setPatientTelephone(uk.nhs.interoperability.payloads.commontypes.Telecom PatientTelephone) {
			setValue("PatientTelephone", PatientTelephone);
			return this;
		}
		
		
		/**
		 * NHS number of the patient
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PatientID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PatientID getPatientNhsNumber() {
			return (uk.nhs.interoperability.payloads.commontypes.PatientID)getValue("PatientNhsNumber");
		}
		
		
		
		
		/**
		 * NHS number of the patient
		 * <br><br>This field is MANDATORY
		 * @param PatientNhsNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patient setPatientNhsNumber(uk.nhs.interoperability.payloads.commontypes.PatientID PatientNhsNumber) {
			setValue("PatientNhsNumber", PatientNhsNumber);
			return this;
		}
		
		
		/**
		 * Patient date of birth
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getPatientDOB() {
			return (HL7Date)getValue("PatientDOB");
		}
		
		
		
		
		/**
		 * Patient date of birth
		 * <br><br>This field is MANDATORY
		 * @param PatientDOB value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patient setPatientDOB(HL7Date PatientDOB) {
			setValue("PatientDOB", PatientDOB);
			return this;
		}
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getPatientName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("PatientName");
		}
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @param PatientName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Patient setPatientName(List PatientName) {
			setValue("PatientName", PatientName);
			return this;
		}
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param PatientName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Patient addPatientName(uk.nhs.interoperability.payloads.commontypes.PersonName PatientName) {
			addMultivalue("PatientName", PatientName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TypeCode", new Field(
												"TypeCode",
												"@typeCode",
												"RCT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIDRoot", new Field(
												"ContentIDRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIDExtension", new Field(
												"ContentIDExtension",
												"npfitlc:contentId/@extension",
												"COCD_TP145216GB01#PatientRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientClassCode", new Field(
												"PatientClassCode",
												"x:COCD_TP145216GB01.PatientRole/@classCode",
												"PAT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientAddress", new Field(
												"PatientAddress",
												"x:COCD_TP145216GB01.PatientRole/x:addr",
												"Patient address",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTelephone", new Field(
												"PatientTelephone",
												"x:COCD_TP145216GB01.PatientRole/x:telecom",
												"Patient telephone number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientNhsNumber", new Field(
												"PatientNhsNumber",
												"x:COCD_TP145216GB01.PatientRole/x:id",
												"NHS number of the patient",
												"true",
												"",
												"",
												"PatientID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTemplateIDRoot", new Field(
												"PatientTemplateIDRoot",
												"x:COCD_TP145216GB01.PatientRole/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTemplateIDExtension", new Field(
												"PatientTemplateIDExtension",
												"x:COCD_TP145216GB01.PatientRole/x:templateId/@extension",
												"COCD_TP145216GB01#PatientRole",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonNameClassCode", new Field(
												"PatientPersonNameClassCode",
												"x:COCD_TP145216GB01.PatientRole/x:patientPatient/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonNameDeterminerCode", new Field(
												"PatientPersonNameDeterminerCode",
												"x:COCD_TP145216GB01.PatientRole/x:patientPatient/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientDOB", new Field(
												"PatientDOB",
												"x:COCD_TP145216GB01.PatientRole/x:patientPatient/x:birthTime/@value",
												"Patient date of birth",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientName", new Field(
												"PatientName",
												"x:COCD_TP145216GB01.PatientRole/x:patientPatient/x:name",
												"Patient Name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"4",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonNameTemplateRoot", new Field(
												"PatientPersonNameTemplateRoot",
												"x:COCD_TP145216GB01.PatientRole/x:patientPatient/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonNameTemplateExtension", new Field(
												"PatientPersonNameTemplateExtension",
												"x:COCD_TP145216GB01.PatientRole/x:patientPatient/x:templateId/@extension",
												"COCD_TP145216GB01#patientPatient",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Patient() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Patient(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	