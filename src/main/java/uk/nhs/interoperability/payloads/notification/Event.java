/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Event object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EventType</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EventTime</li>
 * <li>String EventID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} DocumentType</li>
 * <li>String DocumentFormat</li>
 * <li>String DocumentProfileID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} EventSubtype</li>
 * <li>{@link ContactPerson ContactPerson} ContactPerson</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Event extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "notificationFieldConfig";
		protected static final String name = "Event";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.notification";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param EventType EventType
		 * @param EventTime EventTime
		 * @param EventID EventID
		 * @param DocumentType DocumentType
		 * @param DocumentFormat DocumentFormat
		 * @param DocumentProfileID DocumentProfileID
		 * @param EventSubtype EventSubtype
		 * @param ContactPerson ContactPerson
		 */
	    public Event(CodedValue EventType, HL7Date EventTime, String EventID, CodedValue DocumentType, String DocumentFormat, String DocumentProfileID, CodedValue EventSubtype, ContactPerson ContactPerson) {
			fields = new LinkedHashMap<String, Object>();
			
			setEventType(EventType);
			setEventTime(EventTime);
			setEventID(EventID);
			setDocumentType(DocumentType);
			setDocumentFormat(DocumentFormat);
			setDocumentProfileID(DocumentProfileID);
			setEventSubtype(EventSubtype);
			setContactPerson(ContactPerson);
		}
	
		/**
		 * Type of event that is being notified (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationEventType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getEventType() {
			return (CodedValue)getValue("EventType");
		}
		
		
		
		/**
		 * Type of event that is being notified (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationEventType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType
		 * <br><br>This field is MANDATORY
		 * @return NotificationEventType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType getEventTypeEnum() {
			CodedValue cv = (CodedValue)getValue("EventType");
			VocabularyEntry entry = VocabularyFactory.getVocab("NotificationEventType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType)entry;
		}
		
		
		/**
		 * Type of event that is being notified (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationEventType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType
		 * <br><br>This field is MANDATORY
		 * @param EventType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setEventType(CodedValue EventType) {
			setValue("EventType", EventType);
			return this;
		}
		
		
		/**
		 * Type of event that is being notified (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationEventType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationEventType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EventType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Event setEventType(VocabularyEntry EventType) {
			Code c = new CodedValue(EventType);
			setValue("EventType", c);
			return this;
		}
		
		/**
		 * The date and time that the event occurred
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEventTime() {
			return (HL7Date)getValue("EventTime");
		}
		
		
		
		
		/**
		 * The date and time that the event occurred
		 * <br><br>This field is MANDATORY
		 * @param EventTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setEventTime(HL7Date EventTime) {
			setValue("EventTime", EventTime);
			return this;
		}
		
		
		/**
		 * Unique ID (UUID) for notification
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getEventID() {
			return (String)getValue("EventID");
		}
		
		
		
		
		/**
		 * Unique ID (UUID) for notification
		 * <br><br>This field is MANDATORY
		 * @param EventID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setEventID(String EventID) {
			setValue("EventID", EventID);
			return this;
		}
		
		
		/**
		 * Type of document that is being notified about (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType
		 * @return CodedValue object
		 */	
		public CodedValue getDocumentType() {
			return (CodedValue)getValue("DocumentType");
		}
		
		
		
		/**
		 * Type of document that is being notified about (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType
		 * @return NotificationDocumentType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType getDocumentTypeEnum() {
			CodedValue cv = (CodedValue)getValue("DocumentType");
			VocabularyEntry entry = VocabularyFactory.getVocab("NotificationDocumentType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType)entry;
		}
		
		
		/**
		 * Type of document that is being notified about (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType
		 * @param DocumentType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setDocumentType(CodedValue DocumentType) {
			setValue("DocumentType", DocumentType);
			return this;
		}
		
		
		/**
		 * Type of document that is being notified about (ID)
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentType
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param DocumentType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Event setDocumentType(VocabularyEntry DocumentType) {
			Code c = new CodedValue(DocumentType);
			setValue("DocumentType", c);
			return this;
		}
		
		/**
		 * Format (mime type) of document that is being notified about
		 * @return String object
		 */	
		public String getDocumentFormat() {
			return (String)getValue("DocumentFormat");
		}
		
		
		
		
		/**
		 * Format (mime type) of document that is being notified about
		 * @param DocumentFormat value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setDocumentFormat(String DocumentFormat) {
			setValue("DocumentFormat", DocumentFormat);
			return this;
		}
		
		
		/**
		 * Document Profile ID of document that is being notified about
		 * @return String object
		 */	
		public String getDocumentProfileID() {
			return (String)getValue("DocumentProfileID");
		}
		
		
		
		
		/**
		 * Document Profile ID of document that is being notified about
		 * @param DocumentProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setDocumentProfileID(String DocumentProfileID) {
			setValue("DocumentProfileID", DocumentProfileID);
			return this;
		}
		
		
		/**
		 * The Subtype of the event that is being notified
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentEvent
  		 * <br>NOTE: This field should be populated using the "NotificationPatientEncounterEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientEncounterEvent
  		 * <br>NOTE: This field should be populated using the "NotificationPatientAdministrationEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientAdministrationEvent
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getEventSubtype() {
			return (CodedValue)getValue("EventSubtype");
		}
		
		
		
		
		/**
		 * The Subtype of the event that is being notified
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentEvent
  		 * <br>NOTE: This field should be populated using the "NotificationPatientEncounterEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientEncounterEvent
  		 * <br>NOTE: This field should be populated using the "NotificationPatientAdministrationEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientAdministrationEvent
		 * <br><br>This field is MANDATORY
		 * @param EventSubtype value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setEventSubtype(CodedValue EventSubtype) {
			setValue("EventSubtype", EventSubtype);
			return this;
		}
		
		
		/**
		 * The Subtype of the event that is being notified
  		 * <br>NOTE: This field should be populated using the "NotificationDocumentEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationDocumentEvent
  		 * <br>NOTE: This field should be populated using the "NotificationPatientEncounterEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientEncounterEvent
  		 * <br>NOTE: This field should be populated using the "NotificationPatientAdministrationEvent" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.NotificationPatientAdministrationEvent
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param EventSubtype value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Event setEventSubtype(VocabularyEntry EventSubtype) {
			Code c = new CodedValue(EventSubtype);
			setValue("EventSubtype", c);
			return this;
		}
		
		/**
		 * The contact person for the event
		 * <br><br>This field is MANDATORY
		 * @return ContactPerson object
		 */	
		public ContactPerson getContactPerson() {
			return (ContactPerson)getValue("ContactPerson");
		}
		
		
		
		
		/**
		 * The contact person for the event
		 * <br><br>This field is MANDATORY
		 * @param ContactPerson value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Event setContactPerson(ContactPerson ContactPerson) {
			setValue("ContactPerson", ContactPerson);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("EventClassCode", new Field(
												"EventClassCode",
												"@classCode",
												"INFRM",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventMoodCode", new Field(
												"EventMoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventType", new Field(
												"EventType",
												"x:code",
												"Type of event that is being notified (ID)",
												"true",
												"",
												"NotificationEventType",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.455",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventTime", new Field(
												"EventTime",
												"x:effectiveTime/@value",
												"The date and time that the event occurred",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventID", new Field(
												"EventID",
												"x:id/@root",
												"Unique ID (UUID) for notification",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTypeTypeCode", new Field(
												"DocumentTypeTypeCode",
												"x:component/@typeCode",
												"COMP",
												"DocumentType",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTypeClassCode", new Field(
												"DocumentTypeClassCode",
												"x:component/x:documentDetails/@classCode",
												"DOCCLIN",
												"DocumentType",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTypeMoodCode", new Field(
												"DocumentTypeMoodCode",
												"x:component/x:documentDetails/@moodCode",
												"EVN",
												"DocumentType",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentType", new Field(
												"DocumentType",
												"x:component/x:documentDetails/x:code",
												"Type of document that is being notified about (ID)",
												"false",
												"",
												"NotificationDocumentType",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.450",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatTypeCode", new Field(
												"DocumentFormatTypeCode",
												"x:component2/@typeCode",
												"COMP",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatClassCode", new Field(
												"DocumentFormatClassCode",
												"x:component2/x:documentFormat/@classCode",
												"OBS",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatMoodCode", new Field(
												"DocumentFormatMoodCode",
												"x:component2/x:documentFormat/@moodCode",
												"EVN",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatCode", new Field(
												"DocumentFormatCode",
												"x:component2/x:documentFormat/x:code/@code",
												"01",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatCodeSystem", new Field(
												"DocumentFormatCodeSystem",
												"x:component2/x:documentFormat/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.440",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatCodeSystemDisplayName", new Field(
												"DocumentFormatCodeSystemDisplayName",
												"x:component2/x:documentFormat/x:code/@displayName",
												"Document Format",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormat", new Field(
												"DocumentFormat",
												"x:component2/x:documentFormat/x:value/@code",
												"Format (mime type) of document that is being notified about",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentFormatValueCodeSystem", new Field(
												"DocumentFormatValueCodeSystem",
												"x:component2/x:documentFormat/x:value/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.441",
												"DocumentFormat",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileTypeCode", new Field(
												"DocumentProfileTypeCode",
												"x:component3/@typeCode",
												"COMP",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileClassCode", new Field(
												"DocumentProfileClassCode",
												"x:component3/x:profileID/@classCode",
												"OBS",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileMoodCode", new Field(
												"DocumentProfileMoodCode",
												"x:component3/x:profileID/@moodCode",
												"EVN",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileCode", new Field(
												"DocumentProfileCode",
												"x:component3/x:profileID/x:code/@code",
												"02",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileCodeSystem", new Field(
												"DocumentProfileCodeSystem",
												"x:component3/x:profileID/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.440",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileDisplayName", new Field(
												"DocumentProfileDisplayName",
												"x:component3/x:profileID/x:code/@displayName",
												"Profile ID",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileID", new Field(
												"DocumentProfileID",
												"x:component3/x:profileID/x:value/@extension",
												"Document Profile ID of document that is being notified about",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentProfileValue", new Field(
												"DocumentProfileValue",
												"x:component3/x:profileID/x:value/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.28",
												"DocumentProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtypeTypeCode", new Field(
												"EventSubtypeTypeCode",
												"x:component4/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtypeClassCode", new Field(
												"EventSubtypeClassCode",
												"x:component4/x:eventSubtype/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtypeMoodCode", new Field(
												"EventSubtypeMoodCode",
												"x:component4/x:eventSubtype/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtypeCode", new Field(
												"EventSubtypeCode",
												"x:component4/x:eventSubtype/x:code/@code",
												"03",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtypeCodeSystem", new Field(
												"EventSubtypeCodeSystem",
												"x:component4/x:eventSubtype/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.440",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtypeCodeDisplayName", new Field(
												"EventSubtypeCodeDisplayName",
												"x:component4/x:eventSubtype/x:code/@displayName",
												"Document Subtype",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EventSubtype", new Field(
												"EventSubtype",
												"x:component4/x:eventSubtype/x:value",
												"The Subtype of the event that is being notified",
												"true",
												"",
												"NotificationDocumentEvent",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.456",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContactPerson", new Field(
												"ContactPerson",
												"x:performer",
												"The contact person for the event",
												"true",
												"",
												"",
												"ContactPerson",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Event() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Event(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	