/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.notification;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the EventNotification object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} Time</li>
 * <li>String NotificationID</li>
 * <li>{@link OriginatingSystem OriginatingSystem} OriginatingSystem</li>
 * <li>{@link Event Event} Event</li>
 * <li>List&lt;{@link Recipient Recipient}&gt; Recipient</li>
 * <li>{@link Patient Patient} Patient</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class EventNotification extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "notificationFieldConfig";
		protected static final String name = "EventNotification";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.notification";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Time Time
		 * @param NotificationID NotificationID
		 * @param OriginatingSystem OriginatingSystem
		 * @param Event Event
		 * @param Recipient Recipient
		 * @param Patient Patient
		 */
	    public EventNotification(HL7Date Time, String NotificationID, OriginatingSystem OriginatingSystem, Event Event, 
		List<Recipient> Recipient, Patient Patient) {
			fields = new LinkedHashMap<String, Object>();
			
			setTime(Time);
			setNotificationID(NotificationID);
			setOriginatingSystem(OriginatingSystem);
			setEvent(Event);
			setRecipient(Recipient);
			setPatient(Patient);
		}
	
		/**
		 * Date and Time the notification was generated
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTime() {
			return (HL7Date)getValue("Time");
		}
		
		
		
		
		/**
		 * Date and Time the notification was generated
		 * <br><br>This field is MANDATORY
		 * @param Time value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification setTime(HL7Date Time) {
			setValue("Time", Time);
			return this;
		}
		
		
		/**
		 * Unique ID (UUID) for notification
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getNotificationID() {
			return (String)getValue("NotificationID");
		}
		
		
		
		
		/**
		 * Unique ID (UUID) for notification
		 * <br><br>This field is MANDATORY
		 * @param NotificationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification setNotificationID(String NotificationID) {
			setValue("NotificationID", NotificationID);
			return this;
		}
		
		
		/**
		 * The system that generated the event
		 * <br><br>This field is MANDATORY
		 * @return OriginatingSystem object
		 */	
		public OriginatingSystem getOriginatingSystem() {
			return (OriginatingSystem)getValue("OriginatingSystem");
		}
		
		
		
		
		/**
		 * The system that generated the event
		 * <br><br>This field is MANDATORY
		 * @param OriginatingSystem value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification setOriginatingSystem(OriginatingSystem OriginatingSystem) {
			setValue("OriginatingSystem", OriginatingSystem);
			return this;
		}
		
		
		/**
		 * The event being notified
		 * <br><br>This field is MANDATORY
		 * @return Event object
		 */	
		public Event getEvent() {
			return (Event)getValue("Event");
		}
		
		
		
		
		/**
		 * The event being notified
		 * <br><br>This field is MANDATORY
		 * @param Event value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification setEvent(Event Event) {
			setValue("Event", Event);
			return this;
		}
		
		
		/**
		 * A recipient of the notification
		 * <br><br>This field is MANDATORY
		 * @return List of Recipient objects
		 */
		public List<Recipient> getRecipient() {
			return (List<Recipient>)getValue("Recipient");
		}
		
		/**
		 * A recipient of the notification
		 * <br><br>This field is MANDATORY
		 * @param Recipient value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification setRecipient(List Recipient) {
			setValue("Recipient", Recipient);
			return this;
		}
		
		/**
		 * A recipient of the notification
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a Recipient object, but this method can be called
		 * multiple times to add additional Recipient objects.
		 * @param Recipient value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification addRecipient(Recipient Recipient) {
			addMultivalue("Recipient", Recipient);
			return this;
		}
		
		
		/**
		 * The patient that is the subject of the notification
		 * <br><br>This field is MANDATORY
		 * @return Patient object
		 */	
		public Patient getPatient() {
			return (Patient)getValue("Patient");
		}
		
		
		
		
		/**
		 * The patient that is the subject of the notification
		 * <br><br>This field is MANDATORY
		 * @param Patient value to set
		 * @return this To allow the use of the builder pattern
		 */
		public EventNotification setPatient(Patient Patient) {
			setValue("Patient", Patient);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("NotificationClassCode", new Field(
												"NotificationClassCode",
												"@classCode",
												"INFRM",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NotificationMoodCode", new Field(
												"NotificationMoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NotificationCode", new Field(
												"NotificationCode",
												"x:code/@code",
												"01",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NotificationCodeSystem", new Field(
												"NotificationCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.436",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NotificationCodeDisplayName", new Field(
												"NotificationCodeDisplayName",
												"x:code/@displayName",
												"Event Notification",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Time", new Field(
												"Time",
												"x:effectiveTime/@value",
												"Date and Time the notification was generated",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NotificationID", new Field(
												"NotificationID",
												"x:id/@root",
												"Unique ID (UUID) for notification",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystem", new Field(
												"OriginatingSystem",
												"x:author",
												"The system that generated the event",
												"true",
												"",
												"",
												"OriginatingSystem",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NotificationComponentTypeCode", new Field(
												"NotificationComponentTypeCode",
												"x:component/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Event", new Field(
												"Event",
												"x:component/x:eventInformation",
												"The event being notified",
												"true",
												"",
												"",
												"Event",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Recipient", new Field(
												"Recipient",
												"x:informationRecipient",
												"A recipient of the notification",
												"true",
												"",
												"",
												"Recipient",
												"",
												"20",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Patient", new Field(
												"Patient",
												"x:recordTarget",
												"The patient that is the subject of the notification",
												"true",
												"",
												"",
												"Patient",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public EventNotification() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public EventNotification(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	