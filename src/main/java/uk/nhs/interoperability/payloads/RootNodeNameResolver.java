/**
 * Note: This class is generated automatically when GenerateDomainObjects is run
 * @author Adam Hatherly
 */
package uk.nhs.interoperability.payloads;

public class RootNodeNameResolver {
	public static String getRootNodeName(String versionedName, String packg) {
   	if (packg.endsWith(".")) {
			packg = packg.substring(0, packg.length()-1);
   	};		if (versionedName.equals("COCD_TP145018UK03#AssignedCustodian") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:assignedCustodian";
		}
		if (versionedName.equals("COCD_TP145200GB01#AssignedAuthor") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:assignedAuthor";
		}
		if (versionedName.equals("COCD_TP145201GB01#PatientRole") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:patientRole";
		}
		if (versionedName.equals("COCD_TP145202GB01#IntendedRecipient") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:intendedRecipient";
		}
		if (versionedName.equals("COCD_TP145202GB02#IntendedRecipient") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:intendedRecipient";
		}
		if (versionedName.equals("COCD_TP145203GB02#IntendedRecipient") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:intendedRecipient";
		}
		if (versionedName.equals("COCD_TP145203GB03#IntendedRecipient") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:intendedRecipient";
		}
		if (versionedName.equals("COCD_TP145204GB02#RecipientWorkgroup") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:intendedRecipient";
		}
		if (versionedName.equals("COCD_TP145204GB03#RecipientWorkgroup") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:intendedRecipient";
		}
		if (versionedName.equals("COCD_TP145207GB01#AssignedAuthorDevice") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:assignedAuthor";
		}
		if (versionedName.equals("COCD_TP145208GB01#AssignedAuthor") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:assignedAuthor";
		}
		if (versionedName.equals("COCD_TP145007UK03#RelatedEntity") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:relatedEntity";
		}
		if (versionedName.equals("COCD_TP145007UK03#RelatedEntity") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:relatedEntity";
		}
		if (versionedName.equals("COCD_TP145205GB01#AssignedEntity") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:assignedEntity";
		}
		if (versionedName.equals("COCD_TP145211GB01#HealthCareFacility") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:healthCareFacility";
		}
		if (versionedName.equals("COCD_TP145214GB01#AssociatedEntity") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:associatedEntity";
		}
		if (versionedName.equals("COCD_TP145213GB01#RelatedSubject") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:relatedSubject";
		}
		if (versionedName.equals("COCD_TP146224GB02#ObservationMedia") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observationMedia";
		}
		if (versionedName.equals("COCD_TP146227GB02#ServiceEvent") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:serviceEvent";
		}
		if (versionedName.equals("COCD_TP146228GB01#EncompassingEncounter") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:encompassingEncounter";
		}
		if (versionedName.equals("COCD_TP146248GB01#ReferenceURL") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP146414GB01#PrognosisAwareness") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP146417GB01#AuthorityToLPA") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP146418GB01#AMBoxIssueProcedure") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:procedure";
		}
		if (versionedName.equals("COCD_TP146419GB01#EoLCarePlan") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:act";
		}
		if (versionedName.equals("COCD_TP146420GB01#PatientADRT") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP146422GB01#DNACPRbySRClinician") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP000028GB01#NewBornBirthDetails") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP000029GB01#ParticipantRole") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:participantRole";
		}
		if (versionedName.equals("COCD_TP145230GB02#PatientRole") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:patientRole";
		}
		if (versionedName.equals("COCD_TP145230GB02#guardianOrganization") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:guardianOrganization";
		}
		if (versionedName.equals("COCD_TP145230GB02#guardianPerson") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:guardianPerson";
		}
		if (versionedName.equals("COCD_TP146003GB01#NewBornHearingScreening") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:observation";
		}
		if (versionedName.equals("COCD_TP146005GB02#BloodSpotScreening") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:organizer";
		}
		if (versionedName.equals("COCD_TP146004GB01#NewBornPhysicalExamination") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:procedure";
		}
		if (versionedName.equals("COCD_TP145201GB02#PatientRole") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:patientRole";
		}
		if (versionedName.equals("COCD_TP145201GB02#guardianOrganization") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:guardianOrganization";
		}
		if (versionedName.equals("COCD_TP145201GB02#guardianPerson") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return "x:guardianPerson";
		}
		if (versionedName.equals("Template1#Template1") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return "x:typeone";
		}
		if (versionedName.equals("Template2#Template2") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return "x:typetwo";
		}
		return null;
	}
}