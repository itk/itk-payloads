/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import java.util.Date;

import uk.nhs.interoperability.payloads.util.Emptiable;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

/**
 * Interface for HL7 dates
 * @see uk.nhs.interoperability.payloads.DateValue
 * @author adha3
 *
 */
public interface HL7Date extends Emptiable {
	public DatePrecision getPrecision();
	public Date getDate();
	public String asString();
}
