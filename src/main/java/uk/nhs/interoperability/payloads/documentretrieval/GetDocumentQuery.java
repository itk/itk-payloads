/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.documentretrieval;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the GetDocumentQuery object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} Time</li>
 * <li>String RequestID</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Address Address} AuthorPersonAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} AuthorJobRoleName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID}&gt; AuthorID</li>
 * <li>String AuthorTelephone</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} AuthorDOB</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} AuthorName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} AuthorOrgID</li>
 * <li>String AuthorOrgName</li>
 * <li>String DocumentID</li>
 * <li>String DocumentSetID</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class GetDocumentQuery extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "documentretrievalFieldConfig";
		protected static final String name = "GetDocumentQuery";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.documentretrieval";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Time Time
		 * @param RequestID RequestID
		 * @param AuthorPersonAddress AuthorPersonAddress
		 * @param AuthorJobRoleName AuthorJobRoleName
		 * @param AuthorID AuthorID
		 * @param AuthorTelephone AuthorTelephone
		 * @param AuthorDOB AuthorDOB
		 * @param AuthorName AuthorName
		 * @param AuthorOrgID AuthorOrgID
		 * @param AuthorOrgName AuthorOrgName
		 * @param DocumentID DocumentID
		 * @param DocumentSetID DocumentSetID
		 */
	    public GetDocumentQuery(HL7Date Time, String RequestID, uk.nhs.interoperability.payloads.commontypes.Address AuthorPersonAddress, CodedValue AuthorJobRoleName, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonID> AuthorID, String AuthorTelephone, HL7Date AuthorDOB, uk.nhs.interoperability.payloads.commontypes.PersonName AuthorName, uk.nhs.interoperability.payloads.commontypes.OrgID AuthorOrgID, String AuthorOrgName, String DocumentID, String DocumentSetID) {
			fields = new LinkedHashMap<String, Object>();
			
			setTime(Time);
			setRequestID(RequestID);
			setAuthorPersonAddress(AuthorPersonAddress);
			setAuthorJobRoleName(AuthorJobRoleName);
			setAuthorID(AuthorID);
			setAuthorTelephone(AuthorTelephone);
			setAuthorDOB(AuthorDOB);
			setAuthorName(AuthorName);
			setAuthorOrgID(AuthorOrgID);
			setAuthorOrgName(AuthorOrgName);
			setDocumentID(DocumentID);
			setDocumentSetID(DocumentSetID);
		}
	
		/**
		 * Date and Time when the query for the document was made
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTime() {
			return (HL7Date)getValue("Time");
		}
		
		
		
		
		/**
		 * Date and Time when the query for the document was made
		 * <br><br>This field is MANDATORY
		 * @param Time value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setTime(HL7Date Time) {
			setValue("Time", Time);
			return this;
		}
		
		
		/**
		 * Unique ID (UUID) for the request
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getRequestID() {
			return (String)getValue("RequestID");
		}
		
		
		
		
		/**
		 * Unique ID (UUID) for the request
		 * <br><br>This field is MANDATORY
		 * @param RequestID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setRequestID(String RequestID) {
			setValue("RequestID", RequestID);
			return this;
		}
		
		
		/**
		 * Contact address lines
		 * @return uk.nhs.interoperability.payloads.commontypes.Address object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Address getAuthorPersonAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.Address)getValue("AuthorPersonAddress");
		}
		
		
		
		
		/**
		 * Contact address lines
		 * @param AuthorPersonAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorPersonAddress(uk.nhs.interoperability.payloads.commontypes.Address AuthorPersonAddress) {
			setValue("AuthorPersonAddress", AuthorPersonAddress);
			return this;
		}
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getAuthorJobRoleName() {
			return (CodedValue)getValue("AuthorJobRoleName");
		}
		
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getAuthorJobRoleNameEnum() {
			CodedValue cv = (CodedValue)getValue("AuthorJobRoleName");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @param AuthorJobRoleName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorJobRoleName(CodedValue AuthorJobRoleName) {
			setValue("AuthorJobRoleName", AuthorJobRoleName);
			return this;
		}
		
		
		/**
		 * The type of role the role the contact is in
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param AuthorJobRoleName value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorJobRoleName(VocabularyEntry AuthorJobRoleName) {
			Code c = new CodedValue(AuthorJobRoleName);
			setValue("AuthorJobRoleName", c);
			return this;
		}
		
		/**
		 * Unique ID(s) for the contact person
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonID> getAuthorID() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonID>)getValue("AuthorID");
		}
		
		/**
		 * Unique ID(s) for the contact person
		 * @param AuthorID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorID(List AuthorID) {
			setValue("AuthorID", AuthorID);
			return this;
		}
		
		/**
		 * Unique ID(s) for the contact person
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonID objects.
		 * @param AuthorID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery addAuthorID(uk.nhs.interoperability.payloads.commontypes.PersonID AuthorID) {
			addMultivalue("AuthorID", AuthorID);
			return this;
		}
		
		
		/**
		 * Contact telephone number
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getAuthorTelephone() {
			return (String)getValue("AuthorTelephone");
		}
		
		
		
		
		/**
		 * Contact telephone number
		 * <br><br>This field is MANDATORY
		 * @param AuthorTelephone value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorTelephone(String AuthorTelephone) {
			setValue("AuthorTelephone", AuthorTelephone);
			return this;
		}
		
		
		/**
		 * Patient date of birth
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getAuthorDOB() {
			return (HL7Date)getValue("AuthorDOB");
		}
		
		
		
		
		/**
		 * Patient date of birth
		 * <br><br>This field is MANDATORY
		 * @param AuthorDOB value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorDOB(HL7Date AuthorDOB) {
			setValue("AuthorDOB", AuthorDOB);
			return this;
		}
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getAuthorName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("AuthorName");
		}
		
		
		
		
		/**
		 * Patient Name
		 * <br><br>This field is MANDATORY
		 * @param AuthorName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorName(uk.nhs.interoperability.payloads.commontypes.PersonName AuthorName) {
			setValue("AuthorName", AuthorName);
			return this;
		}
		
		
		/**
		 * ID of contacts organisation (ODS Code)
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getAuthorOrgID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("AuthorOrgID");
		}
		
		
		
		
		/**
		 * ID of contacts organisation (ODS Code)
		 * @param AuthorOrgID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorOrgID(uk.nhs.interoperability.payloads.commontypes.OrgID AuthorOrgID) {
			setValue("AuthorOrgID", AuthorOrgID);
			return this;
		}
		
		
		/**
		 * Name of contacts organisation
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getAuthorOrgName() {
			return (String)getValue("AuthorOrgName");
		}
		
		
		
		
		/**
		 * Name of contacts organisation
		 * <br><br>This field is MANDATORY
		 * @param AuthorOrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setAuthorOrgName(String AuthorOrgName) {
			setValue("AuthorOrgName", AuthorOrgName);
			return this;
		}
		
		
		/**
		 * Unique ID for the document being requested
		 * @return String object
		 */	
		public String getDocumentID() {
			return (String)getValue("DocumentID");
		}
		
		
		
		
		/**
		 * Unique ID for the document being requested
		 * @param DocumentID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setDocumentID(String DocumentID) {
			setValue("DocumentID", DocumentID);
			return this;
		}
		
		
		/**
		 * Unique ID for the document being requested
		 * @return String object
		 */	
		public String getDocumentSetID() {
			return (String)getValue("DocumentSetID");
		}
		
		
		
		
		/**
		 * Unique ID for the document being requested
		 * @param DocumentSetID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentQuery setDocumentSetID(String DocumentSetID) {
			setValue("DocumentSetID", DocumentSetID);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GetDocumentRequestCode", new Field(
												"GetDocumentRequestCode",
												"x:code/@code",
												"01",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GetDocumentRequestCodeSystem", new Field(
												"GetDocumentRequestCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.455",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GetDocumentRequestCodeDisplayName", new Field(
												"GetDocumentRequestCodeDisplayName",
												"x:code/@displayName",
												"GetDocumentQuery",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Time", new Field(
												"Time",
												"x:effectiveTime/@value",
												"Date and Time when the query for the document was made",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RequestID", new Field(
												"RequestID",
												"x:id/@root",
												"Unique ID (UUID) for the request",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeCode", new Field(
												"TypeCode",
												"x:author/@typeCode",
												"AUT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentId", new Field(
												"ContentId",
												"x:author/npfitlc:contentId/@extension",
												"COCD_TP145217GB01#ContactPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdRoot", new Field(
												"ContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonClassCode", new Field(
												"AuthorContactPersonClassCode",
												"x:author/x:COCD_TP145217GB01.ContactPerson/@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonAddress", new Field(
												"AuthorPersonAddress",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:addr",
												"Contact address lines",
												"false",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorJobRoleName", new Field(
												"AuthorJobRoleName",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:code",
												"The type of role the role the contact is in",
												"true",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorID", new Field(
												"AuthorID",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:id",
												"Unique ID(s) for the contact person",
												"false",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"2",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTelephone", new Field(
												"AuthorTelephone",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:telecom/@value",
												"Contact telephone number",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonTemplateRoot", new Field(
												"AuthorContactPersonTemplateRoot",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonTemplateExtension", new Field(
												"AuthorContactPersonTemplateExtension",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:templateId/@extension",
												"COCD_TP145217GB01#ContactPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonNameClassCode", new Field(
												"AuthorPersonNameClassCode",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/@classCode",
												"PSN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonNameDeterminerCode", new Field(
												"AuthorPersonNameDeterminerCode",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorDOB", new Field(
												"AuthorDOB",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:birthTime/@value",
												"Patient date of birth",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorName", new Field(
												"AuthorName",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:name",
												"Patient Name",
												"true",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonNameTemplateRoot", new Field(
												"AuthorPersonNameTemplateRoot",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonNameTemplateExtension", new Field(
												"AuthorPersonNameTemplateExtension",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:assignedPerson/x:templateId/@extension",
												"COCD_TP145217GB01#assignedPerson",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonOrgClassCode", new Field(
												"AuthorContactPersonOrgClassCode",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/@classCode",
												"ORG",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonOrgDeterminerCode", new Field(
												"AuthorContactPersonOrgDeterminerCode",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorOrgID", new Field(
												"AuthorOrgID",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:id",
												"ID of contacts organisation (ODS Code)",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorOrgName", new Field(
												"AuthorOrgName",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:name",
												"Name of contacts organisation",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonOrgTemplateRoot", new Field(
												"AuthorContactPersonOrgTemplateRoot",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContactPersonOrgTemplateExtension", new Field(
												"AuthorContactPersonOrgTemplateExtension",
												"x:author/x:COCD_TP145217GB01.ContactPerson/x:representedOrganization/x:templateId/@extension",
												"COCD_TP145217GB01#representedOrganization",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SemanticsTextDocID", new Field(
												"SemanticsTextDocID",
												"x:query/x:document.id/x:semanticsText",
												"Document.id",
												"DocumentID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentID", new Field(
												"DocumentID",
												"x:query/x:document.id/x:value/@extension",
												"Unique ID for the document being requested",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentIDOID", new Field(
												"DocumentIDOID",
												"x:query/x:document.id/x:value/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.21",
												"DocumentID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SemanticsTextDocSetID", new Field(
												"SemanticsTextDocSetID",
												"x:query/x:document.setId/x:semanticsText",
												"Document.setId",
												"DocumentSetID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentSetID", new Field(
												"DocumentSetID",
												"x:query/x:document.setId/x:value/@extension",
												"Unique ID for the document being requested",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentSetIDOID", new Field(
												"DocumentSetIDOID",
												"x:query/x:document.setId/x:value/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.45",
												"DocumentSetID",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public GetDocumentQuery() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public GetDocumentQuery(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}



	
	