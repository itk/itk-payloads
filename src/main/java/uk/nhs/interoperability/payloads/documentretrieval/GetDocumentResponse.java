/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.documentretrieval;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the GetDocumentResponse object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} Time</li>
 * <li>String ResponseID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ResponseDocumentMessageType</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SystemID SystemID} OriginatingSystemSystemID</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} OriginatingSystemOrgID</li>
 * <li>String OriginatingSystemOrgName</li>
 * <li>String RequestID</li>
 * <li>String ResponseDocument</li>
 * <li>String ResponseDocumentType</li>
 * <li>String ResponseDocumentMediaType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class GetDocumentResponse extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "documentretrievalFieldConfig";
		protected static final String name = "GetDocumentResponse";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.documentretrieval";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Time Time
		 * @param ResponseID ResponseID
		 * @param ResponseDocumentMessageType ResponseDocumentMessageType
		 * @param OriginatingSystemSystemID OriginatingSystemSystemID
		 * @param OriginatingSystemOrgID OriginatingSystemOrgID
		 * @param OriginatingSystemOrgName OriginatingSystemOrgName
		 * @param RequestID RequestID
		 * @param ResponseDocument ResponseDocument
		 * @param ResponseDocumentType ResponseDocumentType
		 * @param ResponseDocumentMediaType ResponseDocumentMediaType
		 */
	    public GetDocumentResponse(HL7Date Time, String ResponseID, CodedValue ResponseDocumentMessageType, uk.nhs.interoperability.payloads.commontypes.SystemID OriginatingSystemSystemID, uk.nhs.interoperability.payloads.commontypes.OrgID OriginatingSystemOrgID, String OriginatingSystemOrgName, String RequestID, String ResponseDocument, String ResponseDocumentType, String ResponseDocumentMediaType) {
			fields = new LinkedHashMap<String, Object>();
			
			setTime(Time);
			setResponseID(ResponseID);
			setResponseDocumentMessageType(ResponseDocumentMessageType);
			setOriginatingSystemSystemID(OriginatingSystemSystemID);
			setOriginatingSystemOrgID(OriginatingSystemOrgID);
			setOriginatingSystemOrgName(OriginatingSystemOrgName);
			setRequestID(RequestID);
			setResponseDocument(ResponseDocument);
			setResponseDocumentType(ResponseDocumentType);
			setResponseDocumentMediaType(ResponseDocumentMediaType);
		}
	
		/**
		 * Date and Time when the get document response was made
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTime() {
			return (HL7Date)getValue("Time");
		}
		
		
		
		
		/**
		 * Date and Time when the get document response was made
		 * <br><br>This field is MANDATORY
		 * @param Time value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setTime(HL7Date Time) {
			setValue("Time", Time);
			return this;
		}
		
		
		/**
		 * Unique ID (UUID) for the request
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getResponseID() {
			return (String)getValue("ResponseID");
		}
		
		
		
		
		/**
		 * Unique ID (UUID) for the request
		 * <br><br>This field is MANDATORY
		 * @param ResponseID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setResponseID(String ResponseID) {
			setValue("ResponseID", ResponseID);
			return this;
		}
		
		
		/**
		 * A code from the DocumentResponseType vocabulary to describe the type of document being returned.
  		 * <br>NOTE: This field should be populated using the "DocumentResponseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getResponseDocumentMessageType() {
			return (CodedValue)getValue("ResponseDocumentMessageType");
		}
		
		
		
		/**
		 * A code from the DocumentResponseType vocabulary to describe the type of document being returned.
  		 * <br>NOTE: This field should be populated using the "DocumentResponseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType
		 * <br><br>This field is MANDATORY
		 * @return DocumentResponseType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType getResponseDocumentMessageTypeEnum() {
			CodedValue cv = (CodedValue)getValue("ResponseDocumentMessageType");
			VocabularyEntry entry = VocabularyFactory.getVocab("DocumentResponseType", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType)entry;
		}
		
		
		/**
		 * A code from the DocumentResponseType vocabulary to describe the type of document being returned.
  		 * <br>NOTE: This field should be populated using the "DocumentResponseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType
		 * <br><br>This field is MANDATORY
		 * @param ResponseDocumentMessageType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setResponseDocumentMessageType(CodedValue ResponseDocumentMessageType) {
			setValue("ResponseDocumentMessageType", ResponseDocumentMessageType);
			return this;
		}
		
		
		/**
		 * A code from the DocumentResponseType vocabulary to describe the type of document being returned.
  		 * <br>NOTE: This field should be populated using the "DocumentResponseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DocumentResponseType
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ResponseDocumentMessageType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setResponseDocumentMessageType(VocabularyEntry ResponseDocumentMessageType) {
			Code c = new CodedValue(ResponseDocumentMessageType);
			setValue("ResponseDocumentMessageType", c);
			return this;
		}
		
		/**
		 * The ID of the System that generated the event
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.SystemID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SystemID getOriginatingSystemSystemID() {
			return (uk.nhs.interoperability.payloads.commontypes.SystemID)getValue("OriginatingSystemSystemID");
		}
		
		
		
		
		/**
		 * The ID of the System that generated the event
		 * <br><br>This field is MANDATORY
		 * @param OriginatingSystemSystemID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setOriginatingSystemSystemID(uk.nhs.interoperability.payloads.commontypes.SystemID OriginatingSystemSystemID) {
			setValue("OriginatingSystemSystemID", OriginatingSystemSystemID);
			return this;
		}
		
		
		/**
		 * The ODS Code of the organisation hosting the system that generated the event
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getOriginatingSystemOrgID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("OriginatingSystemOrgID");
		}
		
		
		
		
		/**
		 * The ODS Code of the organisation hosting the system that generated the event
		 * <br><br>This field is MANDATORY
		 * @param OriginatingSystemOrgID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setOriginatingSystemOrgID(uk.nhs.interoperability.payloads.commontypes.OrgID OriginatingSystemOrgID) {
			setValue("OriginatingSystemOrgID", OriginatingSystemOrgID);
			return this;
		}
		
		
		/**
		 * The name of the organisation hosting the system that generated the event
		 * @return String object
		 */	
		public String getOriginatingSystemOrgName() {
			return (String)getValue("OriginatingSystemOrgName");
		}
		
		
		
		
		/**
		 * The name of the organisation hosting the system that generated the event
		 * @param OriginatingSystemOrgName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setOriginatingSystemOrgName(String OriginatingSystemOrgName) {
			setValue("OriginatingSystemOrgName", OriginatingSystemOrgName);
			return this;
		}
		
		
		/**
		 * Unique ID (UUID) for the request that this response relates to
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getRequestID() {
			return (String)getValue("RequestID");
		}
		
		
		
		
		/**
		 * Unique ID (UUID) for the request that this response relates to
		 * <br><br>This field is MANDATORY
		 * @param RequestID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setRequestID(String RequestID) {
			setValue("RequestID", RequestID);
			return this;
		}
		
		
		/**
		 * Actual document. Can be plain text or base64 encoded. Please set ResponseDocumentType accordingly.
		 * @return String object
		 */	
		public String getResponseDocument() {
			return (String)getValue("ResponseDocument");
		}
		
		
		
		
		/**
		 * Actual document. Can be plain text or base64 encoded. Please set ResponseDocumentType accordingly.
		 * @param ResponseDocument value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setResponseDocument(String ResponseDocument) {
			setValue("ResponseDocument", ResponseDocument);
			return this;
		}
		
		
		/**
		 * Type of document - must be either Text or Base64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * @return String object
		 */	
		public String getResponseDocumentType() {
			return (String)getValue("ResponseDocumentType");
		}
		
		
		
		/**
		 * Type of document - must be either Text or Base64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * @return AttachmentType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType getResponseDocumentTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType.getByCode((String)getValue("ResponseDocumentType"));
		}
		
		
		/**
		 * Type of document - must be either Text or Base64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * @param ResponseDocumentType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setResponseDocumentType(String ResponseDocumentType) {
			setValue("ResponseDocumentType", ResponseDocumentType);
			return this;
		}
		
		
		/**
		 * Media type of attachment (e.g. text/xml or application/pdf)
		 * @return String object
		 */	
		public String getResponseDocumentMediaType() {
			return (String)getValue("ResponseDocumentMediaType");
		}
		
		
		
		
		/**
		 * Media type of attachment (e.g. text/xml or application/pdf)
		 * @param ResponseDocumentMediaType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetDocumentResponse setResponseDocumentMediaType(String ResponseDocumentMediaType) {
			setValue("ResponseDocumentMediaType", ResponseDocumentMediaType);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GetDocumentRequestCode", new Field(
												"GetDocumentRequestCode",
												"x:code/@code",
												"02",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GetDocumentRequestCodeSystem", new Field(
												"GetDocumentRequestCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.455",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GetDocumentRequestCodeDisplayName", new Field(
												"GetDocumentRequestCodeDisplayName",
												"x:code/@displayName",
												"GetDocumentResponse",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Time", new Field(
												"Time",
												"x:effectiveTime/@value",
												"Date and Time when the get document response was made",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseID", new Field(
												"ResponseID",
												"x:id/@root",
												"Unique ID (UUID) for the request",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseDocumentMessageType", new Field(
												"ResponseDocumentMessageType",
												"x:value",
												"A code from the DocumentResponseType vocabulary to describe the type of document being returned.",
												"true",
												"",
												"DocumentResponseType",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeCode", new Field(
												"TypeCode",
												"x:author/@typeCode",
												"AUT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentId", new Field(
												"ContentId",
												"x:author/npfitlc:contentId/@extension",
												"COCD_TP145231GB01#OriginatingSystem",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ContentIdRoot", new Field(
												"ContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemClassCode", new Field(
												"OriginatingSystemOriginatingSystemClassCode",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/@classCode",
												"ASSIGNED",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemCode", new Field(
												"OriginatingSystemOriginatingSystemCode",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:code/@code",
												"OSY",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemCodeSystem", new Field(
												"OriginatingSystemOriginatingSystemCodeSystem",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.205",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemCodeDisplayName", new Field(
												"OriginatingSystemOriginatingSystemCodeDisplayName",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:code/@displayName",
												"Originating System",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemSystemID", new Field(
												"OriginatingSystemSystemID",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:id",
												"The ID of the System that generated the event",
												"true",
												"",
												"",
												"SystemID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemTemplateIDRoot", new Field(
												"OriginatingSystemOriginatingSystemTemplateIDRoot",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemTemplateIDExtension", new Field(
												"OriginatingSystemOriginatingSystemTemplateIDExtension",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:templateId/@extension",
												"COCD_TP145231GB01#OriginatingSystem",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemOrgClassCode", new Field(
												"OriginatingSystemOriginatingSystemOrgClassCode",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/@classCode",
												"ORG",
												"OriginatingSystemOrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemOrgDeterminerCode", new Field(
												"OriginatingSystemOriginatingSystemOrgDeterminerCode",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"OriginatingSystemOrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOrgID", new Field(
												"OriginatingSystemOrgID",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:id",
												"The ODS Code of the organisation hosting the system that generated the event",
												"true",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOrgName", new Field(
												"OriginatingSystemOrgName",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:name",
												"The name of the organisation hosting the system that generated the event",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemOrgTemplateIDRoot", new Field(
												"OriginatingSystemOriginatingSystemOrgTemplateIDRoot",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:templateId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.2",
												"OriginatingSystemOrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OriginatingSystemOriginatingSystemOrgTemplateIDExtension", new Field(
												"OriginatingSystemOriginatingSystemOrgTemplateIDExtension",
												"x:author/x:COCD_TP145231GB01.OriginatingSystem/x:representedOrganization/x:templateId/@extension",
												"COCD_TP145231GB01#representedOrganization",
												"OriginatingSystemOrgID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferenceTypeCode", new Field(
												"ReferenceTypeCode",
												"x:reference/@typeCode",
												"REFR",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferenceContextConductionInd", new Field(
												"ReferenceContextConductionInd",
												"x:reference/@contextConductionInd",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferenceSeparatableInd", new Field(
												"ReferenceSeparatableInd",
												"x:reference/x:seperatableInd/@value",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferredToRequestClassCode", new Field(
												"ReferredToRequestClassCode",
												"x:reference/x:referredToRequest/@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReferredToRequestMoodCode", new Field(
												"ReferredToRequestMoodCode",
												"x:reference/x:referredToRequest/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RequestID", new Field(
												"RequestID",
												"x:reference/x:referredToRequest/x:id/@root",
												"Unique ID (UUID) for the request that this response relates to",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseTypeCode", new Field(
												"ResponseTypeCode",
												"x:subject/@typeCode",
												"SUBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseContextConductionInd", new Field(
												"ResponseContextConductionInd",
												"x:subject/@contextConductionInd",
												"false",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseSeparatableInd", new Field(
												"ResponseSeparatableInd",
												"x:subject/x:seperatableInd/@value",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseClassCode", new Field(
												"ResponseClassCode",
												"x:subject/x:responsePayload/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseMoodCode", new Field(
												"ResponseMoodCode",
												"x:subject/x:responsePayload/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocRetrievalObservationCode", new Field(
												"DocRetrievalObservationCode",
												"x:subject/x:responsePayload/x:code/@code",
												"01",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocRetrievalObservationCodeSystem", new Field(
												"DocRetrievalObservationCodeSystem",
												"x:subject/x:responsePayload/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.456",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocRetrievalObservationDisplayName", new Field(
												"DocRetrievalObservationDisplayName",
												"x:subject/x:responsePayload/x:code/@codeSystem",
												"ResponsePayload",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseDocument", new Field(
												"ResponseDocument",
												"x:subject/x:responsePayload/x:value",
												"Actual document. Can be plain text or base64 encoded. Please set ResponseDocumentType accordingly.",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseDocumentType", new Field(
												"ResponseDocumentType",
												"",
												"Type of document - must be either Text or Base64. Please use the AttachmentType internal vocab.",
												"false",
												"",
												"AttachmentType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseDocumentTypeTXT", new Field(
												"ResponseDocumentTypeTXT",
												"x:subject/x:responsePayload/x:value/@representation",
												"TXT",
												"",
												"",
												"ResponseDocumentType",
												"Text",
												"",
												""
												));
	
		put("ResponseDocumentTypeB64", new Field(
												"ResponseDocumentTypeB64",
												"x:subject/x:responsePayload/x:value/@representation",
												"B64",
												"",
												"",
												"ResponseDocumentType",
												"Base64",
												"",
												""
												));
	
		put("ResponseDocumentMediaType", new Field(
												"ResponseDocumentMediaType",
												"x:subject/x:responsePayload/x:value/@mediaType",
												"Media type of attachment (e.g. text/xml or application/pdf)",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public GetDocumentResponse() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public GetDocumentResponse(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

