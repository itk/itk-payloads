/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.CompositionalStatement;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentSection;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceCaresettingtype;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceDocumenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.DocumentTypes;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType;

/**
 * This is a class to help simplify the creation of the non-coded CDA Document, and hide some of
 * the complexity of the underlying document. Once created using this helper, the document
 * can still be fine-tuned using the methods in objects created.
 * 
 * @author Adam Hatherly
 *
 */
public class TransferOfCareFields extends CDACommonFields {
	private CorrespondenceCaresettingtype careSetting;
	
	private String admissionDetails = null;
	private String allergies = null;
	private String assessments = null;
	private String clinicalSummary = null;
	private String diagnoses = null;
	private String dischargeDetails = null;
	private String informationGiven = null;
	private String investigations = null;
	private String legal = null;
	private String medications = null;
	private String research = null;
	private String concerns = null;
	private String personCompletingRecord = null;
	private String plan = null;
	private String procedures = null;
	private String alerts = null;
	private String socialContext = null;
	private PersonName medicationsAuthorName = null;
	private String medicationsAuthorTelephone = null;
	private DateValue medicationsAuthorDate = null;
	private String medicationsAuthorOrgName = null;
	private String medicationsAuthorODSCode = null;
	private JobRoleName medicationsAuthorRole = null;

	public String getAdmissionDetails() {
		return admissionDetails;
	}
	public void setAdmissionDetails(String admissionDetails) {
		this.admissionDetails = admissionDetails;
	}
	public String getAllergies() {
		return allergies;
	}
	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}
	public String getAssessments() {
		return assessments;
	}
	public void setAssessments(String assessments) {
		this.assessments = assessments;
	}
	public String getClinicalSummary() {
		return clinicalSummary;
	}
	public void setClinicalSummary(String clinicalSummary) {
		this.clinicalSummary = clinicalSummary;
	}
	public String getDiagnoses() {
		return diagnoses;
	}
	public void setDiagnoses(String diagnoses) {
		this.diagnoses = diagnoses;
	}
	public String getDischargeDetails() {
		return dischargeDetails;
	}
	public void setDischargeDetails(String dischargeDetails) {
		this.dischargeDetails = dischargeDetails;
	}
	public String getInformationGiven() {
		return informationGiven;
	}
	public void setInformationGiven(String informationGiven) {
		this.informationGiven = informationGiven;
	}
	public String getInvestigations() {
		return investigations;
	}
	public void setInvestigations(String investigations) {
		this.investigations = investigations;
	}
	public String getLegal() {
		return legal;
	}
	public void setLegal(String legal) {
		this.legal = legal;
	}
	public String getMedications() {
		return medications;
	}
	public void setMedications(String medications) {
		this.medications = medications;
	}
	public String getResearch() {
		return research;
	}
	public void setResearch(String research) {
		this.research = research;
	}
	public String getConcerns() {
		return concerns;
	}
	public void setConcerns(String concerns) {
		this.concerns = concerns;
	}
	public String getPersonCompletingRecord() {
		return personCompletingRecord;
	}
	public void setPersonCompletingRecord(String personCompletingRecord) {
		this.personCompletingRecord = personCompletingRecord;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getProcedures() {
		return procedures;
	}
	public void setProcedures(String procedures) {
		this.procedures = procedures;
	}
	public String getAlerts() {
		return alerts;
	}
	public void setAlerts(String alerts) {
		this.alerts = alerts;
	}
	public String getSocialContext() {
		return socialContext;
	}
	public void setSocialContext(String socialContext) {
		this.socialContext = socialContext;
	}
	public CorrespondenceCaresettingtype getCareSetting() {
		return careSetting;
	}
	public void setCareSetting(CorrespondenceCaresettingtype careSetting) {
		this.careSetting = careSetting;
	}
	public PersonName getMedicationsAuthorName() {
		return medicationsAuthorName;
	}
	public void setMedicationsAuthorName(
			PersonName medicationsAuthorName) {
		this.medicationsAuthorName = medicationsAuthorName;
	}
	public String getMedicationsAuthorTelephone() {
		return medicationsAuthorTelephone;
	}
	public void setMedicationsAuthorTelephone(
			String medicationsAuthorTelephone) {
		this.medicationsAuthorTelephone = medicationsAuthorTelephone;
	}
	public DateValue getMedicationsAuthorDate() {
		return medicationsAuthorDate;
	}
	public void setMedicationsAuthorDate(
			DateValue medicationsAuthorDate) {
		this.medicationsAuthorDate = medicationsAuthorDate;
	}
	public String getMedicationsAuthorOrgName() {
		return medicationsAuthorOrgName;
	}
	public void setMedicationsAuthorOrgName(
			String medicationsAuthorOrgName) {
		this.medicationsAuthorOrgName = medicationsAuthorOrgName;
	}
	public String getMedicationsAuthorODSCode() {
		return medicationsAuthorODSCode;
	}
	public void setMedicationsAuthorODSCode(
			String medicationsAuthorODSCode) {
		this.medicationsAuthorODSCode = medicationsAuthorODSCode;
	}
	public JobRoleName getMedicationsAuthorRole() {
		return medicationsAuthorRole;
	}
	public void setMedicationsAuthorRole(JobRoleName medicationsAuthorRole) {
		this.medicationsAuthorRole = medicationsAuthorRole;
	}
	
}
