/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createAuthenticator;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createAuthorPerson;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createCustodian;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createDataEnterer;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createEncompassingEncounter;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createParticipant;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createPatient;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createRecipient;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createServiceEvent;

import java.util.Date;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.ConsentID;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.noncodedcdav2.ClinicalDocument;
import uk.nhs.interoperability.payloads.noncodedcdav2.DocumentParticipant;
import uk.nhs.interoperability.payloads.noncodedcdav2.DocumentationOf;
import uk.nhs.interoperability.payloads.noncodedcdav2.InformationOnlyRecipient;
import uk.nhs.interoperability.payloads.noncodedcdav2.PrimaryRecipient;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.Consent;
import uk.nhs.interoperability.payloads.templates.Participant;
import uk.nhs.interoperability.payloads.templates.PatientUniversal;
import uk.nhs.interoperability.payloads.templates.Recipient;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

/**
 * This helper class takes a set of fields in the form of a NonCodedCDACommonFields object, and uses them to
 * create the various Java objects that represent the document. The helper makes a number of assumptions in
 * order to simplify the process of creating a Non-Coded CDA document. For example it assumes that all
 * patients will be identified using an NHS number, and that all staff will be identified using an SDS ID. These
 * assumptions may not fit the specific needs of teams implementing this library in their solution. Developers
 * are encouraged to use this class as a starting point to build on/tweak as required 
 * @author Adam Hatherly
 */
public class NonCodedCDADocumentCreationHelper {

	public static ClinicalDocument addNonXMLBody(ClinicalDocument document, AttachmentType encoding, String mimeType, String body) {
		document.setNonXMLBodyMediaType(mimeType);
		document.setNonXMLBodyType(encoding.code);
		document.setNonXMLBodyText(body);
		return document;
	}
	
	public static ClinicalDocument createDocument(NonCodedCDACommonFields commonFields) throws MissingMandatoryFieldException {
		
		ClinicalDocument template = new ClinicalDocument();
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		DateValue currentDateTime = new DateValue(new Date(), DatePrecision.Minutes);
		
		// ==== We will assume some things and set them accordingly ====
		template.setDocumentId(CDAUUID.generateUUIDString());
		template.setConfidentialityCode(x_BasicConfidentialityKind._N);
		
		// Title
		if (commonFields.getDocumentTitle() != null) {
			template.setDocumentTitle(commonFields.getDocumentTitle());
		} else {
			missingFields.addMissingField("documentTitle", "The document title must be provided");
		}
		
		// Document Type
		if (commonFields.getDocumentType() != null) {
			template.setDocumentType(commonFields.getDocumentType());
		} else {
			missingFields.addMissingField("documentType", "The document type must be provided");
		}
		
		// If no record effective date/time specified, assume the current date/time
		if (commonFields.getDocumentEffectiveTime() == null) {
			template.setEffectiveTime(currentDateTime);
		} else {
			template.setEffectiveTime(commonFields.getDocumentEffectiveTime());
		}
		
		// If no document set ID provided, generate a new one
		if (commonFields.getDocumentSetID() != null) {
			template.setDocumentSetId(commonFields.getDocumentSetID());
		} else {
			template.setDocumentSetId(CDAUUID.generateUUIDString());
		}
		
		// Version defaults to 1 unless set to a different integer value
		template.setDocumentVersionNumber(String.valueOf(commonFields.getDocumentVersionNumber()));
		
		// Patient
		try {
			PatientUniversal patient = createPatient(commonFields);
			template.setPatient(patient);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Author
		if (commonFields.getTimeAuthored() == null) {
			template.setTimeAuthored(currentDateTime);
		} else {
			template.setTimeAuthored(commonFields.getTimeAuthored());
		}
		
		try {
			AuthorPersonUniversal author = createAuthorPerson(commonFields);
			template.setAuthor(author);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Data Enterer (optional)
		if (commonFields.getDataEntererName() != null) {
			try {
				template.setDataEnterer(createDataEnterer(commonFields));
			} catch (MissingMandatoryFieldException e) {
				missingFields.addMissingFields(e);
			}
		}
		
		// Custodian
		try {
			template.setCustodianOrganisation(createCustodian(commonFields));
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Recipients
		
		// Having at least one recipient is mandatory
		if (commonFields.getRecipients()==null) {
			missingFields.addMissingField("recipients", "At least one recipient must be provided");
		} else if (commonFields.getRecipients().size()==0) {
			missingFields.addMissingField("recipients", "At least one recipient must be provided");
		} else {
			// Primary Recipients
			for (DocumentRecipient recipient : commonFields.getRecipients()) {
				try {
					Recipient r = createRecipient(recipient);
					template.addPrimaryRecipients(
								new PrimaryRecipient().setRecipient(r));
				} catch (MissingMandatoryFieldException e) {
					missingFields.addMissingFields(e);
				}
			}
			// Copy Recipients
			if (commonFields.getCopyRecipients() != null) {
				for (DocumentRecipient recipient : commonFields.getCopyRecipients()) {
					try {
						Recipient r = createRecipient(recipient);
						template.addInformationOnlyRecipients(
									new InformationOnlyRecipient().setRecipient(r));
					} catch (MissingMandatoryFieldException e) {
						missingFields.addMissingFields(e);
					}
				}
			}
		}
		
		// Authenticator
		if (commonFields.getAuthenticatorName() != null) {
			if (commonFields.getAuthenticatedTime() == null) {
				missingFields.addMissingField("authenticatedTime", "The time the document was authenticated must be provided");
			} else {
				template.setTimeAuthenticated(commonFields.getAuthenticatedTime());
			}
			try {
				template.setAuthenticator(createAuthenticator(commonFields));
			} catch (MissingMandatoryFieldException e) {
				missingFields.addMissingFields(e);
			}
		}
		
		// Participants
		if (commonFields.getParticipants() != null) {
			for (CDADocumentParticipant participant : commonFields.getParticipants()) {
				if (participant.getParticipantType() == null) {
					missingFields.addMissingField("participantType", "The participant type must be provided");
				}
				try {
					Participant p = createParticipant(participant);
					template.addParticipant((
								new DocumentParticipant()
											.setParticipant(p)
											.setParticipantTypeCode(participant.getParticipantType().code)));
				} catch (MissingMandatoryFieldException e) {
					missingFields.addMissingFields(e);
				}
			}
		}
		
		// DocumentationOf
		if (commonFields.getEventCode() != null) {
			try {
				template.addDocumentationOf(new DocumentationOf().setServiceEvent(createServiceEvent(commonFields)));
			} catch (MissingMandatoryFieldException e) {
				missingFields.addMissingFields(e);
			}
		}
		
		// Consent
		if (commonFields.getConsent() != null) {
			template.setAuthorizingConsent(new Consent()
													.setConsentCode(commonFields.getConsent())
													.addID(new ConsentID(CDAUUID.generateUUIDString())));
		}
		
		// Encompassing Encounter
		if (commonFields.getEncounterType() != null) {
			template.setEncompassingEncounter(createEncompassingEncounter(commonFields));
		}

		// We have done all the checks on mandatory fields, so if there are any
		// errors, throw them up to the caller
		if (missingFields.hasEntries()) {
			throw missingFields;
		}		
		
		return template; 
	}
}
