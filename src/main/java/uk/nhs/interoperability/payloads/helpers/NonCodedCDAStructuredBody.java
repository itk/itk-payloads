/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;
import java.util.List;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.vocabularies.generated.*;

/**
 * This is a class to help simplify the creation of the non-coded CDA Document, and hide some of
 * the complexity of the underlying document. Once created using this helper, the document
 * can still be fine-tuned using the methods in objects created.
 * 
 * @author Adam Hatherly
 *
 */
public class NonCodedCDAStructuredBody {
	
}
