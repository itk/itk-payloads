package uk.nhs.interoperability.payloads.helpers;

import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;

public class DocumentRecipient implements Cloneable {
	
	private PersonName recipientName;
	private Address recipientAddress;
	private String recipientTelephone;
	private JobRoleName recipientJobRole;
	private String recipientODSCode;
	private String recipientOrganisationName;
	private String recipientSDSID;
	private String recipientSDSRoleID;
	
	@Override
	public DocumentRecipient clone() {
		try {
			return (DocumentRecipient)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public PersonName getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(PersonName recipientName) {
		this.recipientName = recipientName;
	}
	public Address getRecipientAddress() {
		return recipientAddress;
	}
	public void setRecipientAddress(Address recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	public String getRecipientTelephone() {
		return recipientTelephone;
	}
	public void setRecipientTelephone(String recipientTelephone) {
		this.recipientTelephone = recipientTelephone;
	}
	public JobRoleName getRecipientJobRole() {
		return recipientJobRole;
	}
	public void setRecipientJobRole(JobRoleName recipientJobRole) {
		this.recipientJobRole = recipientJobRole;
	}
	public String getRecipientODSCode() {
		return recipientODSCode;
	}
	public void setRecipientODSCode(String recipientODSCode) {
		this.recipientODSCode = recipientODSCode;
	}
	public String getRecipientOrganisationName() {
		return recipientOrganisationName;
	}
	public void setRecipientOrganisationName(String recipientOrganisationName) {
		this.recipientOrganisationName = recipientOrganisationName;
	}
	public String getRecipientSDSID() {
		return recipientSDSID;
	}
	public void setRecipientSDSID(String recipientSDSID) {
		this.recipientSDSID = recipientSDSID;
	}
	public String getRecipientSDSRoleID() {
		return recipientSDSRoleID;
	}
	public void setRecipientSDSRoleID(String recipientSDSRoleID) {
		this.recipientSDSRoleID = recipientSDSRoleID;
	}

}
