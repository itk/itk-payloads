/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Date;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.childscreeningv2.ClinicalDocument;
import uk.nhs.interoperability.payloads.childscreeningv2.CodedSections;
import uk.nhs.interoperability.payloads.childscreeningv2.InformationOnlyRecipient;
import uk.nhs.interoperability.payloads.childscreeningv2.PrimaryRecipient;
import uk.nhs.interoperability.payloads.childscreeningv2.TextSections;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.ConsentID;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.BloodSpotScreening;
import uk.nhs.interoperability.payloads.templates.ChildPatientGuardian;
import uk.nhs.interoperability.payloads.templates.ChildPatientGuardianPerson;
import uk.nhs.interoperability.payloads.templates.ChildPatientOrganisationPartOf;
import uk.nhs.interoperability.payloads.templates.ChildPatientUniversal;
import uk.nhs.interoperability.payloads.templates.Consent;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.NewBornBirthDetails;
import uk.nhs.interoperability.payloads.templates.NewBornHearingScreening;
import uk.nhs.interoperability.payloads.templates.NewBornPhysicalExamination;
import uk.nhs.interoperability.payloads.templates.Recipient;
import uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal;
import uk.nhs.interoperability.payloads.templates.Section2;
import uk.nhs.interoperability.payloads.templates.Section3;
import uk.nhs.interoperability.payloads.templates.TextSection;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.generated.BSLaboratoryInterpretationCode;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;
import uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

/**
 * This helper class takes a set of fields in the form of a ChildScreeningFields object, and uses them to
 * create the various Java objects that represent the document. The helper makes a number of assumptions in
 * order to simplify the process of creating a Child Screening document. For example it assumes that all
 * patients will be identified using an NHS number, and that all staff will be identified using an SDS ID. These
 * assumptions may not fit the specific needs of teams implementing this library in their solution. Developers
 * are encouraged to use this class as a starting point to build on/tweak as required 
 * @author Adam Hatherly
 */
public class ChildScreeningDocumentCreationHelper {

	public static ClinicalDocument createDocument(ChildScreeningFields childScreeningFields) throws MissingMandatoryFieldException {
		ClinicalDocument template = new ClinicalDocument();
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		DateValue currentDateTime = new DateValue(new Date(), DatePrecision.Minutes);
		
		// ==== We will assume some things and set them accordingly ====
		template.setDocumentId(CDAUUID.generateUUIDString());
		template.setConfidentialityCode(x_BasicConfidentialityKind._N);
		
		// If no record effective date/time specified, assume the current date/time
		if (childScreeningFields.getDocumentCreationDate() == null) {
			template.setEffectiveTime(currentDateTime);
		} else {
			template.setEffectiveTime(childScreeningFields.getDocumentCreationDate());
		}
		
		// If no document set ID provided, generate a new one
		if (childScreeningFields.getDocumentSetId() != null) {
			template.setDocumentSetId(childScreeningFields.getDocumentSetId());
		} else {
			template.setDocumentSetId(CDAUUID.generateUUIDString());
		}
		
		// Version defaults to 1 unless set to a different integer value
		template.setDocumentVersionNumber(String.valueOf(childScreeningFields.getDocumentVersionNumber()));
		
		
		// Child Patient
		try {
			ChildPatientUniversal patient = createPatient(childScreeningFields);
			template.setChildPatient(patient);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Author
		if (childScreeningFields.getDocumentAuthoredTime() == null) {
			template.setTimeAuthored(currentDateTime);
		} else {
			template.setTimeAuthored(childScreeningFields.getDocumentAuthoredTime());
		}
		
		try {
			AuthorPersonUniversal author = createAuthor(childScreeningFields);
			template.setAuthor(author);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Custodian (Organisation hosting the EPaCCS)
		try {
			CustodianOrganizationUniversal custodian = createCustodian(childScreeningFields);
			template.setCustodianOrganisation(custodian);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Recipients
		
		// Having at least one recipient is mandatory
		if (childScreeningFields.getRecipients()==null) {
			missingFields.addMissingField("recipients", "At least one recipient must be provided");
		} else if (childScreeningFields.getRecipients().size()==0) {
			missingFields.addMissingField("recipients", "At least one recipient must be provided");
		} else {
			// Primary Recipients
			for (DocumentRecipient recipient : childScreeningFields.getRecipients()) {
				try {
					Recipient r = createRecipient(recipient);
					template.addPrimaryRecipients(
								new PrimaryRecipient().setRecipient(r));
				} catch (MissingMandatoryFieldException e) {
					missingFields.addMissingFields(e);
				}
			}
			// Copy Recipients
			if (childScreeningFields.getCopyRecipients() != null) {
				for (DocumentRecipient recipient : childScreeningFields.getCopyRecipients()) {
					try {
						Recipient r = createRecipient(recipient);
						template.addInformationOnlyRecipients(
									new InformationOnlyRecipient().setRecipient(r));
					} catch (MissingMandatoryFieldException e) {
						missingFields.addMissingFields(e);
					}
				}
			}
		}
		
		// Consent
		if (childScreeningFields.getConsent() != null) {
			template.setAuthorizingConsent(new Consent()
													.setConsentCode(childScreeningFields.getConsent())
													.addID(new ConsentID(CDAUUID.generateUUIDString())));
		}

		// ==== Now create the coded sections ====
		
		// Blood Spot Screening
		try {
			BloodSpotScreening bloodspot = createBloodSpotScreening(childScreeningFields);
			if (bloodspot != null) {
				template.addCodedSections(new CodedSections(bloodspot));
			}
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// New Born Birth Details
		try {
			NewBornBirthDetails birthdetails = createBirthDetails(childScreeningFields);
			if (birthdetails != null) {
				template.addCodedSections(new CodedSections(birthdetails));
			}
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Hearing Screening
		try {
			NewBornHearingScreening hearing = createHearingScreening(childScreeningFields);
			if (hearing != null) {
				template.addCodedSections(new CodedSections(hearing));
			}
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Hearing Screening
		try {
			NewBornPhysicalExamination physical = createPhysicalExam(childScreeningFields);
			if (physical != null) {
				template.addCodedSections(new CodedSections(physical));
			}
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// We have done all the checks on mandatory fields, so if there are any
		// errors, throw them up to the caller
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		// ==== Now create the text sections ====
		template.setMainDocumentSectionID(CDAUUID.generateUUIDString());
		
		TextSection ts1 = createTextSection_Guardian(childScreeningFields);
		if (ts1 != null) template.addTextSections(new TextSections(ts1));
		
		TextSection ts2 = createTextSection_NewBornBirthDetails(childScreeningFields);
		if (ts2 != null) template.addTextSections(new TextSections(ts2));

		TextSection ts3 = createTextSection_BloodSpotScreening(childScreeningFields);
		if (ts3 != null) template.addTextSections(new TextSections(ts3));
		
		TextSection ts4 = createTextSection_HearingScreening(childScreeningFields);
		if (ts4 != null) template.addTextSections(new TextSections(ts4));
		
		TextSection ts5 = createTextSection_PhysicalExamination(childScreeningFields);
		if (ts5 != null) template.addTextSections(new TextSections(ts5));
		
		return template; 
	}

	public static ChildPatientUniversal createPatient(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		
		// Null checks for mandatory fields
		if (fields.getPatientNHSNoTraceStatus() == null) {
			missingFields.addMissingField("PatientNHSNoTraceStatus", "The tracing status for the NHS number must be provided");
		}
		if (fields.getPatientNHSNo() == null) {
			missingFields.addMissingField("PatientNHSNo", "The patient's NHS number must be provided");
		}
		if (fields.getPatientName() == null) {
			missingFields.addMissingField("PatientName", "The patient's name must be provided");
		}
		if (fields.getPatientGender() == null) {
			missingFields.addMissingField("PatientGender", "The patient's gender must be provided");
		}
		if (fields.getPatientBirthDate() == null) {
			missingFields.addMissingField("PatientBirthDate", "The patient's date of birth must be provided");
		}
		
		// Check if a guardian has been provided, and if so, check all the fields are there
		boolean guardianName = fields.getGuardianName() != null;
		boolean guardianNHSNo = fields.getGuardianNHSNo() != null;
		boolean guardianNHSNoTraceStatus = fields.getGuardianNHSNoTraceStatus() != null;
		boolean guardianAddress = fields.getGuardianAddress() != null;
		boolean guardianTelephone = fields.getGuardianTelephone() != null;
		boolean guardianRole = fields.getGuardianRole() != null;
		boolean guardian = (guardianName || guardianNHSNo || guardianNHSNoTraceStatus || guardianAddress || guardianTelephone || guardianRole);
		if (guardian) {
			if (!guardianName) {
				missingFields.addMissingField("GuardianName", "When a guardian is specified, a name must be included");
			}
			if (!guardianNHSNo) {
				missingFields.addMissingField("GuardianNHSNo", "When a guardian is specified, an NHS number must be included");
			}
			if (!guardianNHSNoTraceStatus) {
				missingFields.addMissingField("GuardianNHSNoTraceStatus", "When a guardian is specified, an NHS number trace status must be included");
			}
			if (!guardianRole) {
				missingFields.addMissingField("GuardianRole", "When a guardian is specified, a guardian role must be included");
			}
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		// Check if a provider Org is provided
		boolean providerOrganisationODSID = fields.getProviderOrganisationODSID() != null;
		boolean providerOrganisation = fields.getProviderOrganisation() != null;
		boolean providerOrganisationAddress = fields.getProviderOrganisationAddress() != null;
		boolean providerOrganisationTelephone = fields.getProviderOrganisationTelephone() != null;
		boolean providerOrganisationType = fields.getProviderOrganisationType() != null;
		boolean providerParentOrganisationODSID = fields.getProviderParentOrganisationODSID() != null;
		boolean provider = (providerOrganisationODSID || providerOrganisation || providerOrganisationAddress || providerOrganisationTelephone || providerOrganisationType || providerParentOrganisationODSID);
		if (provider) {
			if (!providerOrganisationODSID) {
				missingFields.addMissingField("ProviderOrganisationODSID", "When a provider organisation is specified for a child patient, an ODS code must be included");
			}
			if (!providerOrganisation) {
				missingFields.addMissingField("ProviderOrganisation", "When a provider organisation is specified for a child patient, an Org Name must be included");
			}
			if (!providerOrganisationType) {
				missingFields.addMissingField("ProviderOrganisationType", "When a provider organisation is specified for a child patient, a provider org type must be included");
			}
		}
		
		ChildPatientUniversal template = new ChildPatientUniversal();
		
		String idType = null;
		if (fields.getPatientNHSNoTraceStatus().sameAs(NHSNumberTraceStatus.Traced)) {
			idType = PatientIDType.VerifiedNHSNumber.code;
		} else {
			idType = PatientIDType.UnverifiedNHSNumber.code;
		}
		
		template.addPatientID(new PatientIDWithTraceStatuses()
								.setPatientID(fields.getPatientNHSNo())
								.setPatientIDType(idType)
								.setNHSNoTraceStatus(fields.getPatientNHSNoTraceStatus().code));
		
		if (fields.getPatientAddress() != null) {
			template.addAddress(fields.getPatientAddress());
		}
		template.addName(fields.getPatientName());
		template.setGender(fields.getPatientGender());
		template.setDateOfBirth(fields.getPatientBirthDate());
		
		// Guardian
		if (guardian) {
			ChildPatientGuardian guardianFields = new ChildPatientGuardian();
			
			String guardianIdType = null;
			if (fields.getGuardianNHSNoTraceStatus().sameAs(NHSNumberTraceStatus.Traced)) {
				guardianIdType = PatientIDType.VerifiedNHSNumber.code;
			} else {
				guardianIdType = PatientIDType.UnverifiedNHSNumber.code;
			}
			guardianFields.addId(new PatientIDWithTraceStatuses()
										.setPatientID(fields.getGuardianNHSNo())
										.setPatientIDType(guardianIdType)
										.setNHSNoTraceStatus(fields.getGuardianNHSNoTraceStatus().code));
			guardianFields.setGuardianDetails(new ChildPatientGuardianPerson().setGuardianName(fields.getGuardianName()));
			guardianFields.setRole(fields.getGuardianRole());
			
			if (guardianAddress) {
				guardianFields.addAddress(fields.getGuardianAddress());
			}
			if (guardianTelephone) {
				guardianFields.addTelephoneNumber(new Telecom()
											.setTelecom("tel:" + fields.getGuardianTelephone())
											.setTelecomType(TelecomUseType.HomeAddress.code));
			}
			template.addGuardian(guardianFields);
		}
		
		// Provider Organisation
		if (provider) {
	 		template.setOrganisationId(new OrgID(OrgIDType.ODSOrgID.code, fields.getProviderOrganisationODSID()));
			template.setOrganisationName(fields.getProviderOrganisation());
			template.setOrganisationType(fields.getProviderOrganisationType());
			
			if (providerOrganisationTelephone) {
				template.addOrganisationTelephone(new Telecom()
													.setTelecom("tel:" + fields.getProviderOrganisationTelephone())
													.setTelecomType(TelecomUseType.WorkPlace.code));
			}
			
			if (providerOrganisationAddress) {
				template.setOrganisationAddress(fields.getProviderOrganisationAddress());
			}
			
			if (providerParentOrganisationODSID) {
				template.addOrganisationPartOf(new ChildPatientOrganisationPartOf()
													.addOrganisationId(new OrgID(OrgIDType.ODSOrgID.code,
																					fields.getProviderParentOrganisationODSID())));
			}
		}
		return template;
	}
	
	public static AuthorPersonUniversal createAuthor(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		
		// Null checks for mandatory fields
		if (fields.getDocumentAuthorSDSID() == null) {
			missingFields.addMissingField("DocumentAuthorSDSID", "The SDS ID of the document author must be provided");
		}
		if (fields.getDocumentAuthorRole() == null) {
			missingFields.addMissingField("DocumentAuthorRole", "The job role of the document author must be provided");
		}
		if (fields.getDocumentAuthorName() == null) {
			missingFields.addMissingField("DocumentAuthorName", "The name of the document author must be provided");
		}
		if (fields.getDocumentAuthorOrganisationODSID() == null) {
			missingFields.addMissingField("DocumentAuthorOrganisationODSID", "The ID of the organisation the document author belongs to must be provided");
		}
		if (fields.getDocumentAuthorOrganisationName() == null) {
			missingFields.addMissingField("DocumentAuthorOrganisationName", "The name of the organisation the document author belongs to must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		// Author SID ID
		template.addId(new PersonID()
 						.setType(PersonIDType.SDSID.code)
 						.setID(fields.getDocumentAuthorSDSID()));
		
		// Author Job Role
 		template.setJobRoleName(fields.getDocumentAuthorRole());
 		
 		// Author Address
 		if (fields.getDocumentAuthorAddress() != null) {
 			Address add = fields.getDocumentAuthorAddress();
 			add.setAddressUse(AddressType.WorkPlace.code);
 			template.addAddress(add);
 		}
 		
 		// Author telephone number
 		if (fields.getDocumentAuthorTelephone() != null) {
 			template.addTelephoneNumber(new Telecom("tel:" + fields.getDocumentAuthorTelephone()));
 		}
 		
 		// Author Name
 		template.setName(fields.getDocumentAuthorName());
 		
 		// Author ORG ID
		template.setOrganisationId(new OrgID()
									.setID(fields.getDocumentAuthorOrganisationODSID())
									.setType(OrgIDType.ODSOrgID.code));
		
		// Author ORG Name
		template.setOrganisationName(fields.getDocumentAuthorOrganisationName());
		
		return template;
	}
	
	public static CustodianOrganizationUniversal createCustodian(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		if (fields.getCustodianOrganisationODSID() == null) {
			missingFields.addMissingField("CustodianOrganisationODSID", "The ODS ID of the custodian organisation must be provided");
		}
		if (fields.getCustodianOrganisation() == null) {
			missingFields.addMissingField("CustodianOrganisation", "The name of the custodian organisation must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		CustodianOrganizationUniversal template = new CustodianOrganizationUniversal();
		
		// Custodian Org ID
		template.setId(new OrgID(OrgIDType.ODSOrgID.code, fields.getCustodianOrganisationODSID()));
		
		// Custodian Org Name
		template.setName(fields.getCustodianOrganisation());
		
		return template;
	}
	
	public static Recipient createRecipient(DocumentRecipient recipient) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		if (recipient.getRecipientName() == null) {
			missingFields.addMissingField("recipientName", "The name of the recipient must be provided");
		}
		if (recipient.getRecipientODSCode() == null) {
			missingFields.addMissingField("recipientODSCode", "The ODS Code for the organisation of the recipient must be provided");
		}
		if (recipient.getRecipientOrganisationName() == null) {
			missingFields.addMissingField("recipientOrganisationName", "The organisation name for the recipient must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		RecipientPersonUniversal template = new RecipientPersonUniversal();
		// ID (NULL)
		template.addId(new RoleID().setNullFlavour(NullFlavour.NA.code));
		// recipientName
		template.setName(recipient.getRecipientName());
		// recipientAddress
		if (recipient.getRecipientAddress() != null) {
			template.setAddress(recipient.getRecipientAddress());
		}
		// recipientTelephone
 		if (recipient.getRecipientTelephone() != null) {
 			template.addTelephoneNumber(new Telecom("tel:" + recipient.getRecipientTelephone()));
 		}
		// recipientJobRole
 		if (recipient.getRecipientJobRole() != null) {
 			template.setJobRoleName(recipient.getRecipientJobRole());
 		}
		// recipientODSCode
 		template.setOrgId(new OrgID()
 								.setID(recipient.getRecipientODSCode())
 								.setType(OrgIDType.ODSOrgID.code));
		// recipientOrganisationName
 		template.setOrgName(recipient.getRecipientOrganisationName());
		return template;
	}

	public static BloodSpotScreening createBloodSpotScreening(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		boolean pkuResult = fields.getPKUScreeningValue() != null;		
		boolean pkuLabConfirmed = fields.getLaboratoryConfirmPKUasCorrect();
		boolean pkuSubStatus = fields.getPKUScreeningSubStatus() != null;
		boolean pkuReason = fields.getPKUReasonText() != null;
		boolean pkuSupplementary = fields.getPKUSupplementaryText() != null;
		
		boolean scdResult = fields.getSCDScreeningValue() != null;		
		boolean scdLabConfirmed = fields.getLaboratoryConfirmSCDasCorrect();
		boolean scdSubStatus = fields.getSCDScreeningSubStatus() != null;
		boolean scdReason = fields.getSCDReasonText() != null;
		boolean scdSupplementary = fields.getSCDSupplementaryText() != null;
		
		boolean cfResult = fields.getCFScreeningValue() != null;		
		boolean cfLabConfirmed = fields.getLaboratoryConfirmCFasCorrect();
		boolean cfSubStatus = fields.getCFScreeningSubStatus() != null;
		boolean cfReason = fields.getCFReasonText() != null;
		boolean cfSupplementary = fields.getCFSupplementaryText() != null;
		
		boolean chtResult = fields.getCHTScreeningValue() != null;		
		boolean chtLabConfirmed = fields.getLaboratoryConfirmCHTasCorrect();
		boolean chtSubStatus = fields.getCHTScreeningSubStatus() != null;
		boolean chtReason = fields.getCHTReasonText() != null;
		boolean chtSupplementary = fields.getCHTSupplementaryText() != null;
		
		boolean mcaddResult = fields.getMCADDScreeningValue() != null;		
		boolean mcaddLabConfirmed = fields.getLaboratoryConfirmMCADDasCorrect();
		boolean mcaddSubStatus = fields.getMCADDScreeningSubStatus() != null;
		boolean mcaddReason = fields.getMCADDReasonText() != null;
		boolean mcaddSupplementary = fields.getMCADDSupplementaryText() != null;
		
		if ((pkuLabConfirmed || pkuSubStatus || pkuReason || pkuSupplementary) && !pkuResult) {
			missingFields.addMissingField("PKUScreeningValue", "Details cannot be provided for a test unless there is a test result included");
		}
		if ((scdLabConfirmed || scdSubStatus || scdReason || scdSupplementary) && !scdResult) {
			missingFields.addMissingField("SCDScreeningValue", "Details cannot be provided for a test unless there is a test result included");
		}
		if ((cfLabConfirmed || cfSubStatus || cfReason || cfSupplementary) && !cfResult) {
			missingFields.addMissingField("CFScreeningValue", "Details cannot be provided for a test unless there is a test result included");
		}
		if ((chtLabConfirmed || chtSubStatus || chtReason || chtSupplementary) && !chtResult) {
			missingFields.addMissingField("CHTScreeningValue", "Details cannot be provided for a test unless there is a test result included");
		}
		if ((mcaddLabConfirmed || mcaddSubStatus || mcaddReason || mcaddSupplementary) && !mcaddResult) {
			missingFields.addMissingField("MCADDScreeningValue", "Details cannot be provided for a test unless there is a test result included");
		}
		
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
				
		if (fields.getDateOfBloodSpotScreening() == null) {
			return null;
		}
		
		BloodSpotScreening template = new BloodSpotScreening();
		
		template.setId(CDAUUID.generateUUIDString());
		template.setEffectiveTime(fields.getDateOfBloodSpotScreening());
		// Performer (Sample collected by)
		template.setSampleCollectedTime(fields.getBloodSpotSampleCollectedTime());
		template.addPerformerPersonId(new PersonID().setID(fields.getBloodSpotPerformerPersonSDSID()).setType(PersonIDType.SDSID.code));
		template.setPerformerPersonName(fields.getBloodSpotPerformerPersonName());
		template.setPerformerOrgId(new OrgID()
											.setID(fields.getBloodSpotPerformerOrganisationODSID())
											.setType(OrgIDType.ODSOrgID.code));
		template.setPerformerOrgName(fields.getBloodSpotPerformerOrganisationName());
		// Lab (Secondary Performer)
		template.setTimeReceivedAtLab(fields.getBloodSpotTimeReceivedAtLab());
		template.setLabOrganisationId(new OrgID()
											.setID(fields.getBloodSpotLabOrganisationODSID())
											.setType(OrgIDType.ODSOrgID.code));
		template.setLabOrganisationDescription(fields.getBloodSpotLabOrganisationName());
		
		// Screening results
		if (pkuResult) {
			template.setPKUScreeningValue(fields.getPKUScreeningValue());
			if (fields.getLaboratoryConfirmPKUasCorrect()) {
				template.setPKUBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
			}
			template.setPKUScreeningSubStatus(fields.getPKUScreeningSubStatus());
			template.setPKUReasonText(fields.getPKUReasonText());
			template.setPKUSupplementaryText(fields.getPKUSupplementaryText());
		}
		
		if (scdResult) {
			template.setSCDScreeningValue(fields.getSCDScreeningValue());
			if (fields.getLaboratoryConfirmSCDasCorrect()) {
				template.setSCDBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
			}
			template.setSCDScreeningSubStatus(fields.getSCDScreeningSubStatus());
			template.setSCDReasonText(fields.getSCDReasonText());
			template.setSCDSupplementaryText(fields.getSCDSupplementaryText());
		}
		
		if (cfResult) {
			template.setCFScreeningValue(fields.getCFScreeningValue());
			if (fields.getLaboratoryConfirmCFasCorrect()) {
				template.setCFBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
			}
			template.setCFScreeningSubStatus(fields.getCFScreeningSubStatus());
			template.setCFReasonText(fields.getCFReasonText());
			template.setCFSupplementaryText(fields.getCFSupplementaryText());
		}
		
		if (chtResult) {
			template.setCHTScreeningValue(fields.getCHTScreeningValue());
			if (fields.getLaboratoryConfirmCHTasCorrect()) {
				template.setCHTBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
			}
			template.setCHTScreeningSubStatus(fields.getCHTScreeningSubStatus());
			template.setCHTReasonText(fields.getCHTReasonText());
			template.setCHTSupplementaryText(fields.getCHTSupplementaryText());
		}
		
		if (mcaddResult) {
			template.setMCADDScreeningValue(fields.getMCADDScreeningValue());
			if (fields.getLaboratoryConfirmMCADDasCorrect()) {
				template.setMCADDBSLaboratoryInterpretationCode(BSLaboratoryInterpretationCode._ReevaluatedResultLaboratoryConfirmasCorrect);
			}
			template.setMCADDScreeningSubStatus(fields.getMCADDScreeningSubStatus());
			template.setMCADDReasonText(fields.getMCADDReasonText());
			template.setMCADDSupplementaryText(fields.getMCADDSupplementaryText());
		}
		
		// Other observations
		template.setScreeningLocationStatus(fields.getScreeningLocationStatus());
		template.setLaboratoryCardSerialNumber(fields.getLaboratoryCardSerialNumber());
		template.setPreviousLaboratoryCardSerialNumber(fields.getPreviousLaboratoryCardSerialNumber());
		return template;
	}
	
	public static NewBornBirthDetails createBirthDetails(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		if (fields.getDateBirthDetailsRecorded() == null) {
			return null;
		}
		
		NewBornBirthDetails template = new NewBornBirthDetails();

		template.setId(CDAUUID.generateUUIDString());
		template.setEffectiveTime(fields.getDateBirthDetailsRecorded());
		template.setGestationalAgeInWeeks(fields.getGestationalAgeInWeeks());
		if (fields.getBirthOrder() != null) {
			template.setBirthOrder(Integer.toString(fields.getBirthOrder()));
		}
		if (fields.getNoOfFoetusInConfinement() != null) {
			template.setNoOfFoetusInConfinement(Integer.toString(fields.getNoOfFoetusInConfinement()));
		}
		template.setBirthWeightInGrams(fields.getBirthWeightInGrams());
		
		return template;
	}
	
	public static NewBornHearingScreening createHearingScreening(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		boolean finding = fields.getAudiologyTestFinding() != null;
		boolean findingTime = fields.getAudiologyTestFindingEffectiveTime() != null;
		
		if (finding && !findingTime) {
			missingFields.addMissingField("AudiologyTestFindingEffectiveTime", "When providing an audiology test finding, an effective time is also required");
		}
		if (!finding && findingTime) {
			missingFields.addMissingField("AudiologyTestFinding", "An effective time has been provided for an audiology test, but no test result was provided");
		}
		
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		if (fields.getHearingScreeningOutcome() == null) {
			return null;
		}
		
		NewBornHearingScreening template = new NewBornHearingScreening();
		template.setId(CDAUUID.generateUUIDString());
		template.setScreeningOutcome(fields.getHearingScreeningOutcome());
		template.setAudiologyTestFinding(fields.getAudiologyTestFinding());
		template.setAudiologyTestFindingEffectiveTime(fields.getAudiologyTestFindingEffectiveTime());
		template.setAudiologyReferralTime(fields.getAudiologyReferralTime());
		return template;
	}
	
	public static NewBornPhysicalExamination createPhysicalExam(ChildScreeningFields fields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		if (fields.getDateOfPhysicalExamination() == null) {
			return null;
		}
		
		NewBornPhysicalExamination template = new NewBornPhysicalExamination();
		template.setId(CDAUUID.generateUUIDString());
		template.setEffectiveTime(fields.getDateOfPhysicalExamination());
		if (fields.getGestationalAgeInDays() != null) {
			template.setGestationalAgeInDays(Integer.toString(fields.getGestationalAgeInDays()));
		}
		template.setHipsExamination(fields.getHipsExamination());
		template.setUltraSoundDecision(fields.getUltraSoundDecision());
		template.setHipsUltraSoundEffectiveTime(fields.getDateOfHipsUltrasound());
		template.setExpertManagementPlan(fields.getExpertManagementPlan());
		template.setExpertManagementPlanEffectiveTime(fields.getDateHipsExpertManagementPlanCreated());
		template.setHeartExamination(fields.getHeartExamination());
		template.setEyesExamination(fields.getEyesExamination());
		template.setTestesExamination(fields.getTestesExamination());
		return template;
	}
	
	public static TextSection createTextSection_Guardian(ChildScreeningFields fields) {
		if (fields.getGuardianName() == null) {
			return null;
		}
		TextSection template = new TextSection();
		template.setSectionId(CDAUUID.generateUUIDString());
		template.setTitle("Guardian Details");
		template.setText("<table width=\"100%\"><tbody>"
				+ "<tr align=\"left\" valign=\"top\"><td>Relationship</td><td>" + fields.getGuardianRole().displayName + "</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>NHS Number</td><td>" + fields.getGuardianNHSNo() + "</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Name</td><td>" + HumanReadableFormatter.makeHumanReadablePersonName(fields.getGuardianName()) + "</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td>Address</td><td>" + HumanReadableFormatter.makeHumanReadableAddress(fields.getGuardianAddress(), "<br/>") + "</td></tr>"
				+ "</tbody></table>");
		return template;
	}
	
	public static TextSection createTextSection_NewBornBirthDetails(ChildScreeningFields fields) {
		if (fields.getDateBirthDetailsRecorded() == null) {
			return null;
		}
		TextSection template = new TextSection();
		template.setSectionId(CDAUUID.generateUUIDString());
		template.setTitle("New Born Birth Details");
		template.setText("<table width=\"100%\"><tbody>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a1\">Gestational age</content></td><td>" + fields.getGestationalAgeInWeeks() + " Weeks</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a2\">Birth order</content></td><td>" + fields.getBirthOrder() + "</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a3\">Number of births in confinement</content></td><td>" + fields.getNoOfFoetusInConfinement() + "</td></tr>"
				+ "<tr align=\"left\" valign=\"top\"><td><content ID=\"a4\">Birth weight</content></td><td>" + fields.getBirthWeightInGrams() + " grams</td></tr>"
				+ "</tbody></table>");
		return template;
	}
	
	public static TextSection createTextSection_BloodSpotScreening(ChildScreeningFields fields) {
		if (fields.getDateOfBloodSpotScreening() == null) {
			return null;
		}
		TextSection template = new TextSection();
		template.setSectionId(CDAUUID.generateUUIDString());
		template.setTitle("Blood Spot Screening");
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<table width=\"100%\"><tbody><tr align=\"left\" valign=\"top\">");
		sb.append("<th>Blood spot screening tests</th>");
		sb.append("<th>Results</th>");
		sb.append("<th>Re-evaluated Result: Laboratory Confirm as Correct</th>");
		sb.append("<th>Status Sub Code</th>");
		sb.append("<th>Reason Text</th>");
		sb.append("<th>Supplementary Text</th></tr>");
				
		// PKU
		outputScreeningTableRow(sb,"Phenylketonuria screening test",
									fields.getPKUScreeningValue(),
									fields.getLaboratoryConfirmPKUasCorrect(),
									fields.getPKUScreeningSubStatus(),
									fields.getPKUReasonText(),
									fields.getPKUSupplementaryText());
		
		// SCD
		outputScreeningTableRow(sb,"Screening for sickle cell disease",
									fields.getSCDScreeningValue(),
									fields.getLaboratoryConfirmSCDasCorrect(),
									fields.getSCDScreeningSubStatus(),
									fields.getSCDReasonText(),
									fields.getSCDSupplementaryText());
		
		// CF
		outputScreeningTableRow(sb,"Cystic fibrosis screening test",
									fields.getCFScreeningValue(),
									fields.getLaboratoryConfirmCFasCorrect(),
									fields.getCFScreeningSubStatus(),
									fields.getCFReasonText(),
									fields.getCFSupplementaryText());
		
		// CHT
		outputScreeningTableRow(sb,"Congenital hypothyroidism screening test",
									fields.getCHTScreeningValue(),
									fields.getLaboratoryConfirmCHTasCorrect(),
									fields.getCHTScreeningSubStatus(),
									fields.getCHTReasonText(),
									fields.getCHTSupplementaryText());
		
		// MCADD
		outputScreeningTableRow(sb,"Medium-chain acyl-coenzyme A dehydrogenase deficiency screening test",
									fields.getMCADDScreeningValue(),
									fields.getLaboratoryConfirmMCADDasCorrect(),
									fields.getMCADDScreeningSubStatus(),
									fields.getMCADDReasonText(),
									fields.getMCADDSupplementaryText());
		
		sb.append("</tbody></table>");
		template.setText(sb.toString());
		
		// Lab Details
		boolean timeReceived = fields.getBloodSpotTimeReceivedAtLab() != null;
		boolean labName = fields.getBloodSpotLabOrganisationName() != null;
		boolean cardNumber = fields.getLaboratoryCardSerialNumber() != null;
		boolean previous = fields.getPreviousLaboratoryCardSerialNumber() != null;
		
		if (timeReceived || labName || cardNumber || previous) {		
			Section2 s2 = new Section2();
			s2.setSectionId(CDAUUID.generateUUIDString());
			s2.setTitle("Laboratory Details");
			
			if (labName)
				s2.setText(fields.getBloodSpotLabOrganisationName());
			if (timeReceived)
				addSection3(s2, "Date of Receipt at Lab", fields.getBloodSpotTimeReceivedAtLab().getDisplayString());
			if (cardNumber)
				addSection3(s2, "Lab Card Serial Number", fields.getLaboratoryCardSerialNumber());
			if (previous)
				addSection3(s2, "Previous Lab Card Serial Number", fields.getPreviousLaboratoryCardSerialNumber());
			
			template.addSection2(s2);
		}
		
		// Specimen Collection Details
		boolean orgName = fields.getBloodSpotPerformerOrganisationName() != null;
		boolean collected = fields.getBloodSpotSampleCollectedTime() != null;
		boolean collectedBy = fields.getBloodSpotPerformerPersonName() != null;
		
		if (orgName || collected || collectedBy) {
			Section2 s2b = new Section2();
			s2b.setSectionId(CDAUUID.generateUUIDString());
			s2b.setTitle("Specimen Collection Details");
			if (orgName)
				s2b.setText("Blood specimen collected at " + fields.getBloodSpotPerformerOrganisationName());
			if (collected)
				addSection3(s2b, "Specimen Collection Time", fields.getBloodSpotSampleCollectedTime().getDisplayString());
			if (collectedBy)
				addSection3(s2b, "Specimen Collected by", 
						HumanReadableFormatter.makeHumanReadablePersonName(fields.getBloodSpotPerformerPersonName()));
			template.addSection2(s2b);
		}
		
		return template;
	}
	
	private static Section3 addSection3(Section2 s2, String title, String text) {
		if (text == null) {
			return null;
		}
		Section3 s3 = new Section3();
		s3.setSectionId(CDAUUID.generateUUIDString());
		s3.setTitle(title);
		s3.setText(text);
		s2.addSection3(s3);
		return s3;
	}
	
	private static Section2 addSection2(TextSection s1, String title, String text) {
		if (text == null) {
			return null;
		}
		Section2 s2 = new Section2();
		s2.setSectionId(CDAUUID.generateUUIDString());
		s2.setTitle(title);
		s2.setText(text);
		s1.addSection2(s2);
		return s2;
	}
	
	private static void outputScreeningTableRow(StringBuilder sb, String name, VocabularyEntry result,
					boolean reevaluated, VocabularyEntry substatus, String reason, String supplementary) {
		if (result == null) {
			return;
		}
		sb.append("<tr align=\"left\" valign=\"top\"><td>").append(name).append("</td><td>");
		sb.append(result.getDisplayName()).append("</td><td>");
		if (reevaluated) {
			sb.append("Yes");
		}
		sb.append("</td><td>");
		if (substatus != null) {
			sb.append(substatus.getDisplayName());
		}
		sb.append("</td><td>").append(reason);
		sb.append("</td><td>").append(supplementary).append("</td></tr>");
	}
	
	public static TextSection createTextSection_HearingScreening(ChildScreeningFields fields) {
		if (fields.getHearingScreeningOutcome() == null) {
			return null;
		}
		TextSection template = new TextSection();
		template.setSectionId(CDAUUID.generateUUIDString());
		template.setTitle("Hearing Screening");
		template.setText(fields.getHearingScreeningOutcome().getDisplayName());
		if (fields.getAudiologyTestFindingEffectiveTime() != null)
			addSection2(template, "Date of Audiology Test", fields.getAudiologyTestFindingEffectiveTime().getDisplayString());
		if (fields.getAudiologyTestFinding() != null)
			addSection2(template, "Audiology Test Showed Impairement", fields.getAudiologyTestFinding().getDisplayName());
		if (fields.getAudiologyReferralTime() != null)
			addSection2(template, "Audiology Referral Time", fields.getAudiologyReferralTime().getDisplayString());
		return template;
	}
	
	public static TextSection createTextSection_PhysicalExamination(ChildScreeningFields fields) {
		if (fields.getDateOfPhysicalExamination() == null) {
			return null;
		}
		TextSection template = new TextSection();
		template.setSectionId(CDAUUID.generateUUIDString());
		template.setTitle("Physical Examination");
		
		addSection2(template,"Date of Examination", fields.getDateOfPhysicalExamination().getDisplayString());
		
		if (fields.getGestationalAgeInDays() != null)
			addSection2(template,"Gestational Age", fields.getGestationalAgeInDays() + " Days");
		
		if (fields.getHipsExamination() != null) {
			Section2 hips = addSection2(template,"Hips Examination", fields.getHipsExamination().getDisplayName());
			if (fields.getUltraSoundDecision() != null) {
				addSection3(hips, "Ultra sound outcome decision", fields.getUltraSoundDecision().getDisplayName());
			}
			if (fields.getExpertManagementPlan() != null) {
				addSection3(hips, "Expert management plan", fields.getExpertManagementPlan().getDisplayName());
			}
		}
		if (fields.getHeartExamination() != null)
			addSection2(template,"Heart Examination", fields.getHeartExamination().getDisplayName());
		if (fields.getEyesExamination() != null)
			addSection2(template,"Eyes Examination", fields.getEyesExamination().getDisplayName());
		if (fields.getTestesExamination() != null)
			addSection2(template,"Testes Examination", fields.getTestesExamination().getDisplayName());
		
		return template;
	}
	
}
