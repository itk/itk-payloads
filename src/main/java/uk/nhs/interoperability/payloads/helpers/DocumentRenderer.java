/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;
import uk.nhs.interoperability.payloads.util.TransformManager;

public class DocumentRenderer {

	/**
	 * Will be very slow as it has to serialise to XML and transform to generate the rendered version - could
	 * look into reproducing the logic in the stylesheet directly in code to make this faster and more flexible
	 * @param xmlDocument Java object structure representing CDA document content
	 * @return String HTML rendered representation of document
	 */
	public static String generateHTMLDocument(Payload xmlDocument) {
		String documentStr = xmlDocument.serialise();
		return generateHTMLDocument(documentStr);
	}
	
	/**
	 * Simply takes an XML string and passes it through the standard rendering XSL template
	 * @param xmlDocument String containing CDA document
	 * @return String HTML rendered representation of document
	 */
	public static String generateHTMLDocument(String xmlDocument) {
		String html = TransformManager.doTransform("othertransforms/nhs_CDA_Document_Renderer.xsl", xmlDocument, null);
		return html;
	}
}
