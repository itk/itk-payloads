/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;
import java.util.List;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.vocabularies.generated.*;

/**
 * This is a class to help simplify the creation of the EOLC CDA Document, and hide some of
 * the complexity of the underlying document. Once created using this helper, the document
 * can still be fine-tuned using the methods in objects created.
 * 
 * @author Adam Hatherly
 *
 */
public class EndOfLifeCareISBFields implements PatientFields {
	private DateValue epaccsRecordCreationDate;
	private DateValue epaccsRecordAuthoredDate;
	private Address epaccsRecordAuthorAddress;
	private JobRoleName epaccsRecordAuthorRole;
	private String epaccsRecordAuthorSDSID;
	private String epaccsRecordAuthorTelephone;
	private PersonName epaccsRecordAuthorName;
	private String epaccsRecordAuthorOrganisationODSID;
	private String epaccsRecordAuthorOrganisationName;
	private DateValue epaccsRecordReviewDate;
	private DateValue epaccsRecordUpdatedDate;
	private Address epaccsRecordUpdateAuthorAddress;
	private JobRoleName epaccsRecordUpdateAuthorRole;
	private String epaccsRecordUpdateAuthorSDSID;
	private String epaccsRecordUpdateAuthorTelephone;
	private PersonName epaccsRecordUpdateAuthorName;
	private String epaccsRecordUpdateAuthorOrganisationODSID;
	private String epaccsRecordUpdateAuthorOrganisationName;
	private PersonName patientName;
	private PersonName patientPreferredName;
	private DateValue patientBirthDate;
	private String patientNHSNo;
	private Boolean patientNHSNoIsTraced;
	private Sex patientGender;
	private Boolean patientInterpreterNeeded;
	private HumanLanguage patientPreferredSpokenLanguage;
	private String patientDisability;
	private Address patientAddress;
	private String patientTelephone;
	private String patientMobile;
	private PersonName mainInformalCarerName;
	private String mainInformalCarerTel;
	private PrognosisAwarenessSnCT mainInformalCarerAwareOfPrognosis;
	private DateValue prognosisAwarenessRecordedDate;
	private PersonName usualGPName;
	private String usualGPOrgName;
	private String usualGPODSCode;
	private String usualGPTelephone;
	private String usualGPFax;
	private Address usualGPAddress;
	private PersonName keyWorkerName;
	private String keyWorkerTelephone;
	private Address keyWorkerAddress;
	private JobRoleName keyWorkerJobRole;
	private String keyWorkerSDSID;
	private String keyWorkerOrgID;
	private String keyWorkerOrgName;
	private List<EndOfLifeCareDocumentFormalCarer> formalCarers;
	private String primaryEOLCDiagnosis;
	private String otherRelevantDiagnoses;
	private String allergiesAndAdverseReactions;
	private boolean anticipatoryMedicinesIssued;
	private String anticipatoryMedicinesLocation;
	private DateValue anticipatoryMedicinesDateIssued;
	private EoLToolSnCT eolcTool;
	private String eolcPathwayStageNONISB;
	private String advanceStatements;
	private String preferredPlaceOfDeath;
	private String preferredPlaceOfDeathOrganisation;
	private Address preferredPlaceOfDeathAddress;
	private String preferredPlaceOfDeathIsUPR;
	private String preferredPlaceOfDeath2;
	private String preferredPlaceOfDeath2Organisation;
	private Address preferredPlaceOfDeath2Address;
	private String preferredPlaceOfDeath2IsUPR;
	private DNACPRprefSnCT DNACPR;
	private DateValue DNACPRDate;
	private DateValue DNACPRReviewDate;
	private DateValue DNACPRCreatedDate;
	private String DNACPRLocation;
	private EoLADRTprefSnCT ADRT;
	private Boolean ADRTDiscussedWithClinicianNONISB;
	private String ADRTDocumentLocation;
	private DateValue ADRTRecordedDate;
	private PersonName LPAName;
	private AuthoritytoLPASnCT LPAAuthority;
	private DateValue LPADate;
	private String LPATelephone;
	private PersonName additionalPersonToInvolve;
	private String additionalPersonToInvolveTel;
	private PersonName additionalPersonToInvolve2;
	private String additionalPersonToInvolve2Tel;
	private String otherRelevantInformation;
	private DateValue documentCreationDate;
	private String documentId;
	private String documentSetId;
	private int documentVersionNumber = 1;
	private Address documentAuthorAddress;
	private JobRoleName documentAuthorRole;
	private String documentAuthorSDSID;
	private String documentAuthorTelephone;
	private PersonName documentAuthorName;
	private String documentAuthorOrganisationODSID;
	private String documentAuthorOrganisationName;
	private DateValue documentAuthoredTime;
	private String EPACCSOrganisationODSID;
	private String EPACCSOrganisation;
	private PersonName SeniorResponsibleClinicianName;
	private String SeniorResponsibleClinicianSDSID;
	private String SeniorResponsibleClinicianORGID;
	private String SeniorResponsibleClinicianORGName;
	private JobRoleName SeniorResponsibleClinicianJobRole;
	private String SeniorResponsibleClinicianTelephone;
	private Address SeniorResponsibleClinicianAddress;
	private String EPaCCSURL;

	// Helper methods
	public ClinicalDocument createDocument() throws MissingMandatoryFieldException {
		return EndOfLifeCareDocumentCreationHelper.createDocument(this);
	}

	
	
	// Getters and Setters
	public DateValue getEpaccsRecordCreationDate() {
		return epaccsRecordCreationDate;
	}
	public void setEpaccsRecordCreationDate(DateValue recordCreationDate) {
		this.epaccsRecordCreationDate = recordCreationDate;
	}
	public DateValue getEpaccsRecordAuthoredDate() {
		return epaccsRecordAuthoredDate;
	}

	public void setEpaccsRecordAuthoredDate(DateValue epaccsRecordAuthoredDate) {
		this.epaccsRecordAuthoredDate = epaccsRecordAuthoredDate;
	}

	public Address getEpaccsRecordAuthorAddress() {
		return epaccsRecordAuthorAddress;
	}

	public void setEpaccsRecordAuthorAddress(Address epaccsRecordAuthorAddress) {
		this.epaccsRecordAuthorAddress = epaccsRecordAuthorAddress;
	}

	public JobRoleName getEpaccsRecordAuthorRole() {
		return epaccsRecordAuthorRole;
	}

	public void setEpaccsRecordAuthorRole(JobRoleName epaccsRecordAuthorRole) {
		this.epaccsRecordAuthorRole = epaccsRecordAuthorRole;
	}

	public String getEpaccsRecordAuthorSDSID() {
		return epaccsRecordAuthorSDSID;
	}

	public void setEpaccsRecordAuthorSDSID(String epaccsRecordAuthorSDSID) {
		this.epaccsRecordAuthorSDSID = epaccsRecordAuthorSDSID;
	}

	public String getEpaccsRecordAuthorTelephone() {
		return epaccsRecordAuthorTelephone;
	}

	public void setEpaccsRecordAuthorTelephone(String epaccsRecordAuthorTelephone) {
		this.epaccsRecordAuthorTelephone = epaccsRecordAuthorTelephone;
	}

	public PersonName getEpaccsRecordAuthorName() {
		return epaccsRecordAuthorName;
	}

	public void setEpaccsRecordAuthorName(PersonName epaccsRecordAuthorName) {
		this.epaccsRecordAuthorName = epaccsRecordAuthorName;
	}

	public String getEpaccsRecordAuthorOrganisationODSID() {
		return epaccsRecordAuthorOrganisationODSID;
	}

	public void setEpaccsRecordAuthorOrganisationODSID(
			String epaccsRecordAuthorOrganisationODSID) {
		this.epaccsRecordAuthorOrganisationODSID = epaccsRecordAuthorOrganisationODSID;
	}

	public String getEpaccsRecordAuthorOrganisationName() {
		return epaccsRecordAuthorOrganisationName;
	}

	public void setEpaccsRecordAuthorOrganisationName(
			String epaccsRecordAuthorOrganisationName) {
		this.epaccsRecordAuthorOrganisationName = epaccsRecordAuthorOrganisationName;
	}

	public DateValue getEpaccsRecordReviewDate() {
		return epaccsRecordReviewDate;
	}
	public void setEpaccsRecordReviewDate(DateValue recordReviewDate) {
		this.epaccsRecordReviewDate = recordReviewDate;
	}
	public DateValue getEpaccsRecordUpdatedDate() {
		return epaccsRecordUpdatedDate;
	}
	public void setEpaccsRecordUpdatedDate(DateValue recordUpdatedDate) {
		this.epaccsRecordUpdatedDate = recordUpdatedDate;
	}
	public Address getEpaccsRecordUpdateAuthorAddress() {
		return epaccsRecordUpdateAuthorAddress;
	}

	public void setEpaccsRecordUpdateAuthorAddress(
			Address epaccsRecordUpdateAuthorAddress) {
		this.epaccsRecordUpdateAuthorAddress = epaccsRecordUpdateAuthorAddress;
	}

	public JobRoleName getEpaccsRecordUpdateAuthorRole() {
		return epaccsRecordUpdateAuthorRole;
	}

	public void setEpaccsRecordUpdateAuthorRole(
			JobRoleName epaccsRecordUpdateAuthorRole) {
		this.epaccsRecordUpdateAuthorRole = epaccsRecordUpdateAuthorRole;
	}

	public String getEpaccsRecordUpdateAuthorSDSID() {
		return epaccsRecordUpdateAuthorSDSID;
	}

	public void setEpaccsRecordUpdateAuthorSDSID(
			String epaccsRecordUpdateAuthorSDSID) {
		this.epaccsRecordUpdateAuthorSDSID = epaccsRecordUpdateAuthorSDSID;
	}

	public String getEpaccsRecordUpdateAuthorTelephone() {
		return epaccsRecordUpdateAuthorTelephone;
	}

	public void setEpaccsRecordUpdateAuthorTelephone(
			String epaccsRecordUpdateAuthorTelephone) {
		this.epaccsRecordUpdateAuthorTelephone = epaccsRecordUpdateAuthorTelephone;
	}

	public PersonName getEpaccsRecordUpdateAuthorName() {
		return epaccsRecordUpdateAuthorName;
	}

	public void setEpaccsRecordUpdateAuthorName(
			PersonName epaccsRecordUpdateAuthorName) {
		this.epaccsRecordUpdateAuthorName = epaccsRecordUpdateAuthorName;
	}

	public String getEpaccsRecordUpdateAuthorOrganisationODSID() {
		return epaccsRecordUpdateAuthorOrganisationODSID;
	}

	public void setEpaccsRecordUpdateAuthorOrganisationODSID(
			String epaccsRecordUpdateAuthorOrganisationODSID) {
		this.epaccsRecordUpdateAuthorOrganisationODSID = epaccsRecordUpdateAuthorOrganisationODSID;
	}

	public String getEpaccsRecordUpdateAuthorOrganisationName() {
		return epaccsRecordUpdateAuthorOrganisationName;
	}

	public void setEpaccsRecordUpdateAuthorOrganisationName(
			String epaccsRecordUpdateAuthorOrganisationName) {
		this.epaccsRecordUpdateAuthorOrganisationName = epaccsRecordUpdateAuthorOrganisationName;
	}

	public PersonName getPatientName() {
		return patientName;
	}
	public void setPatientName(PersonName patientName) {
		this.patientName = patientName;
	}
	public PersonName getPatientPreferredName() {
		return patientPreferredName;
	}
	public void setPatientPreferredName(PersonName patientPreferredName) {
		this.patientPreferredName = patientPreferredName;
	}
	public DateValue getPatientBirthDate() {
		return patientBirthDate;
	}
	public void setPatientBirthDate(DateValue patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}
	public String getPatientNHSNo() {
		return patientNHSNo;
	}
	public void setPatientNHSNo(String patientNHSNo) {
		this.patientNHSNo = patientNHSNo;
	}
	public Boolean getPatientNHSNoIsTraced() {
		return patientNHSNoIsTraced;
	}
	public void setPatientNHSNoIsTraced(Boolean patientNHSNoIsTraced) {
		this.patientNHSNoIsTraced = patientNHSNoIsTraced;
	}
	public Sex getPatientGender() {
		return patientGender;
	}
	public void setPatientGender(Sex patientGender) {
		this.patientGender = patientGender;
	}
	public boolean isPatientInterpreterNeeded() {
		if (patientInterpreterNeeded == null) {
			return false;
		} else {
			return patientInterpreterNeeded;
		}
	}
	public void setPatientInterpreterNeeded(boolean patientInterpreterNeeded) {
		this.patientInterpreterNeeded = patientInterpreterNeeded;
	}
	public HumanLanguage getPatientPreferredSpokenLanguage() {
		return patientPreferredSpokenLanguage;
	}
	public void setPatientPreferredSpokenLanguage(
			HumanLanguage patientPreferredSpokenLanguage) {
		this.patientPreferredSpokenLanguage = patientPreferredSpokenLanguage;
	}
	public String getPatientDisability() {
		return patientDisability;
	}
	public void setPatientDisability(String patientDisability) {
		this.patientDisability = patientDisability;
	}
	public Address getPatientAddress() {
		return patientAddress;
	}
	public void setPatientAddress(Address patientAddress) {
		this.patientAddress = patientAddress;
	}
	public String getPatientTelephone() {
		return patientTelephone;
	}
	public void setPatientTelephone(String patientTelephone) {
		this.patientTelephone = patientTelephone;
	}
	public String getPatientMobile() {
		return patientMobile;
	}

	public void setPatientMobile(String patientMobile) {
		this.patientMobile = patientMobile;
	}

	public PersonName getMainInformalCarerName() {
		return mainInformalCarerName;
	}
	public void setMainInformalCarerName(PersonName mainInformalCarerName) {
		this.mainInformalCarerName = mainInformalCarerName;
	}
	public String getMainInformalCarerTel() {
		return mainInformalCarerTel;
	}
	public void setMainInformalCarerTel(String mainInformalCarerTel) {
		this.mainInformalCarerTel = mainInformalCarerTel;
	}
	public PrognosisAwarenessSnCT getMainInformalCarerAwareOfPrognosis() {
		return mainInformalCarerAwareOfPrognosis;
	}
	public void setMainInformalCarerAwareOfPrognosis(
			PrognosisAwarenessSnCT mainInformalCarerAwareOfPrognosis) {
		this.mainInformalCarerAwareOfPrognosis = mainInformalCarerAwareOfPrognosis;
	}
	public DateValue getPrognosisAwarenessRecordedDate() {
		return prognosisAwarenessRecordedDate;
	}

	public void setPrognosisAwarenessRecordedDate(
			DateValue prognosisAwarenessRecordedDate) {
		this.prognosisAwarenessRecordedDate = prognosisAwarenessRecordedDate;
	}

	public PersonName getUsualGPName() {
		return usualGPName;
	}

	public void setUsualGPName(PersonName usualGPName) {
		this.usualGPName = usualGPName;
	}

	public String getUsualGPOrgName() {
		return usualGPOrgName;
	}
	public void setUsualGPOrgName(String usualGPOrgName) {
		this.usualGPOrgName = usualGPOrgName;
	}
	public String getUsualGPODSCode() {
		return usualGPODSCode;
	}
	public void setUsualGPODSCode(String usualGPODSCode) {
		this.usualGPODSCode = usualGPODSCode;
	}
	public String getUsualGPTelephone() {
		return usualGPTelephone;
	}
	public void setUsualGPTelephone(String usualGPTelephone) {
		this.usualGPTelephone = usualGPTelephone;
	}
	public String getUsualGPFax() {
		return usualGPFax;
	}
	public void setUsualGPFax(String usualGPFax) {
		this.usualGPFax = usualGPFax;
	}
	public Address getUsualGPAddress() {
		return usualGPAddress;
	}
	public void setUsualGPAddress(Address usualGPAddress) {
		this.usualGPAddress = usualGPAddress;
	}
	public PersonName getKeyWorkerName() {
		return keyWorkerName;
	}
	public void setKeyWorkerName(PersonName keyWorkerName) {
		this.keyWorkerName = keyWorkerName;
	}
	public String getKeyWorkerTelephone() {
		return keyWorkerTelephone;
	}
	public void setKeyWorkerTelephone(String keyWorkerTelephone) {
		this.keyWorkerTelephone = keyWorkerTelephone;
	}
	public Address getKeyWorkerAddress() {
		return keyWorkerAddress;
	}

	public void setKeyWorkerAddress(Address keyWorkerAddress) {
		this.keyWorkerAddress = keyWorkerAddress;
	}

	public JobRoleName getKeyWorkerJobRole() {
		return keyWorkerJobRole;
	}

	public void setKeyWorkerJobRole(JobRoleName keyWorkerJobRole) {
		this.keyWorkerJobRole = keyWorkerJobRole;
	}

	public String getKeyWorkerSDSID() {
		return keyWorkerSDSID;
	}

	public void setKeyWorkerSDSID(String keyWorkerSDSID) {
		this.keyWorkerSDSID = keyWorkerSDSID;
	}

	public String getKeyWorkerOrgID() {
		return keyWorkerOrgID;
	}

	public void setKeyWorkerOrgID(String keyWorkerOrgID) {
		this.keyWorkerOrgID = keyWorkerOrgID;
	}

	public String getKeyWorkerOrgName() {
		return keyWorkerOrgName;
	}

	public void setKeyWorkerOrgName(String keyWorkerOrgName) {
		this.keyWorkerOrgName = keyWorkerOrgName;
	}

	public List<EndOfLifeCareDocumentFormalCarer> getFormalCarers() {
		return formalCarers;
	}
	public void setFormalCarers(List<EndOfLifeCareDocumentFormalCarer> formalCarers) {
		this.formalCarers = formalCarers;
	}
	public void addFormalCarer(EndOfLifeCareDocumentFormalCarer carer) {
		if (this.formalCarers == null) {
			this.formalCarers = new ArrayList<EndOfLifeCareDocumentFormalCarer>();
		}
		this.formalCarers.add(carer);
	}
	public String getPrimaryEOLCDiagnosis() {
		return primaryEOLCDiagnosis;
	}
	public void setPrimaryEOLCDiagnosis(String primaryEOLCDiagnosis) {
		this.primaryEOLCDiagnosis = primaryEOLCDiagnosis;
	}
	public String getOtherRelevantDiagnoses() {
		return otherRelevantDiagnoses;
	}
	public void setOtherRelevantDiagnoses(String otherRelevantDiagnoses) {
		this.otherRelevantDiagnoses = otherRelevantDiagnoses;
	}
	public String getAllergiesAndAdverseReactions() {
		return allergiesAndAdverseReactions;
	}
	public void setAllergiesAndAdverseReactions(
			String allergiesAndAdverseReactions) {
		this.allergiesAndAdverseReactions = allergiesAndAdverseReactions;
	}
	public boolean isAnticipatoryMedicinesIssued() {
		return anticipatoryMedicinesIssued;
	}
	public void setAnticipatoryMedicinesIssued(
			boolean anticipatoryMedicinesIssued) {
		this.anticipatoryMedicinesIssued = anticipatoryMedicinesIssued;
	}
	public String getAnticipatoryMedicinesLocation() {
		return anticipatoryMedicinesLocation;
	}
	public void setAnticipatoryMedicinesLocation(
			String anticipatoryMedicinesLocation) {
		this.anticipatoryMedicinesLocation = anticipatoryMedicinesLocation;
	}
	public DateValue getAnticipatoryMedicinesDateIssued() {
		return anticipatoryMedicinesDateIssued;
	}

	public void setAnticipatoryMedicinesDateIssued(
			DateValue anticipatoryMedicinesDateIssued) {
		this.anticipatoryMedicinesDateIssued = anticipatoryMedicinesDateIssued;
	}

	public EoLToolSnCT getEolcTool() {
		return eolcTool;
	}
	public void setEolcTool(EoLToolSnCT eolcTool) {
		this.eolcTool = eolcTool;
	}
	public String getEolcPathwayStageNONISB() {
		return eolcPathwayStageNONISB;
	}
	public void setEolcPathwayStageNONISB(String eolcPathwayStage) {
		this.eolcPathwayStageNONISB = eolcPathwayStage;
	}
	public String getAdvanceStatements() {
		return advanceStatements;
	}
	public void setAdvanceStatements(String advanceStatements) {
		this.advanceStatements = advanceStatements;
	}
	public String getPreferredPlaceOfDeath() {
		return preferredPlaceOfDeath;
	}
	public void setPreferredPlaceOfDeath(String preferredPlaceOfDeath) {
		this.preferredPlaceOfDeath = preferredPlaceOfDeath;
	}
	public String getPreferredPlaceOfDeathOrganisation() {
		return preferredPlaceOfDeathOrganisation;
	}
	public void setPreferredPlaceOfDeathOrganisation(
			String preferredPlaceOfDeathOrganisation) {
		this.preferredPlaceOfDeathOrganisation = preferredPlaceOfDeathOrganisation;
	}
	public Address getPreferredPlaceOfDeathAddress() {
		return preferredPlaceOfDeathAddress;
	}
	public void setPreferredPlaceOfDeathAddress(
			Address preferredPlaceOfDeathAddress) {
		this.preferredPlaceOfDeathAddress = preferredPlaceOfDeathAddress;
	}
	public String getPreferredPlaceOfDeathIsUPR() {
		return preferredPlaceOfDeathIsUPR;
	}
	public void setPreferredPlaceOfDeathIsUPR(
			String preferredPlaceOfDeathIsUPR) {
		this.preferredPlaceOfDeathIsUPR = preferredPlaceOfDeathIsUPR;
	}
	public String getPreferredPlaceOfDeath2() {
		return preferredPlaceOfDeath2;
	}
	public void setPreferredPlaceOfDeath2(String preferredPlaceOfDeath2) {
		this.preferredPlaceOfDeath2 = preferredPlaceOfDeath2;
	}
	public String getPreferredPlaceOfDeath2Organisation() {
		return preferredPlaceOfDeath2Organisation;
	}
	public void setPreferredPlaceOfDeath2Organisation(
			String preferredPlaceOfDeath2Organisation) {
		this.preferredPlaceOfDeath2Organisation = preferredPlaceOfDeath2Organisation;
	}
	public Address getPreferredPlaceOfDeath2Address() {
		return preferredPlaceOfDeath2Address;
	}
	public void setPreferredPlaceOfDeath2Address(
			Address preferredPlaceOfDeath2Address) {
		this.preferredPlaceOfDeath2Address = preferredPlaceOfDeath2Address;
	}
	public String getPreferredPlaceOfDeath2IsUPR() {
		return preferredPlaceOfDeath2IsUPR;
	}
	public void setPreferredPlaceOfDeath2IsUPR(
			String preferredPlaceOfDeath2IsUPR) {
		this.preferredPlaceOfDeath2IsUPR = preferredPlaceOfDeath2IsUPR;
	}
	public DNACPRprefSnCT getDNACPR() {
		return DNACPR;
	}
	public void setDNACPR(DNACPRprefSnCT dNACPR) {
		DNACPR = dNACPR;
	}
	public DateValue getDNACPRDate() {
		return DNACPRDate;
	}
	public void setDNACPRDate(DateValue dNACPRDate) {
		DNACPRDate = dNACPRDate;
	}
	public DateValue getDNACPRReviewDate() {
		return DNACPRReviewDate;
	}
	public void setDNACPRReviewDate(DateValue dNACPRReviewDate) {
		DNACPRReviewDate = dNACPRReviewDate;
	}
	public DateValue getDNACPRCreatedDate() {
		return DNACPRCreatedDate;
	}

	public void setDNACPRCreatedDate(DateValue dNACPRCreatedDate) {
		DNACPRCreatedDate = dNACPRCreatedDate;
	}

	public String getDNACPRLocation() {
		return DNACPRLocation;
	}
	public void setDNACPRLocation(String dNACPRLocation) {
		DNACPRLocation = dNACPRLocation;
	}
	public EoLADRTprefSnCT getADRT() {
		return ADRT;
	}
	public void setADRT(EoLADRTprefSnCT aDRT) {
		ADRT = aDRT;
	}
	public boolean isADRTDiscussedWithClinicianNONISB() {
		return ADRTDiscussedWithClinicianNONISB;
	}
	public void setADRTDiscussedWithClinicianNONISB(
			boolean aDRTDiscussedWithClinician) {
		ADRTDiscussedWithClinicianNONISB = aDRTDiscussedWithClinician;
	}
	public String getADRTDocumentLocation() {
		return ADRTDocumentLocation;
	}
	public void setADRTDocumentLocation(String aDRTDocumentLocation) {
		ADRTDocumentLocation = aDRTDocumentLocation;
	}
	public DateValue getADRTRecordedDate() {
		return ADRTRecordedDate;
	}

	public void setADRTRecordedDate(DateValue aDRTRecordedDate) {
		ADRTRecordedDate = aDRTRecordedDate;
	}

	public PersonName getLPAName() {
		return LPAName;
	}
	public void setLPAName(PersonName lPAName) {
		LPAName = lPAName;
	}
	public AuthoritytoLPASnCT getLPAAuthority() {
		return LPAAuthority;
	}
	public void setLPAAuthority(AuthoritytoLPASnCT lPAAuthority) {
		LPAAuthority = lPAAuthority;
	}
	public DateValue getLPADate() {
		return LPADate;
	}

	public void setLPADate(DateValue lPADate) {
		LPADate = lPADate;
	}

	public String getLPATelephone() {
		return LPATelephone;
	}
	public void setLPATelephone(String lPATelephone) {
		LPATelephone = lPATelephone;
	}
	public PersonName getAdditionalPersonToInvolve() {
		return additionalPersonToInvolve;
	}
	public void setAdditionalPersonToInvolve(
			PersonName additionalPersonToInvolve) {
		this.additionalPersonToInvolve = additionalPersonToInvolve;
	}
	public String getAdditionalPersonToInvolveTel() {
		return additionalPersonToInvolveTel;
	}
	public void setAdditionalPersonToInvolveTel(
			String additionalPersonToInvolveTel) {
		this.additionalPersonToInvolveTel = additionalPersonToInvolveTel;
	}
	public PersonName getAdditionalPersonToInvolve2() {
		return additionalPersonToInvolve2;
	}
	public void setAdditionalPersonToInvolve2(
			PersonName additionalPersonToInvolve2) {
		this.additionalPersonToInvolve2 = additionalPersonToInvolve2;
	}
	public String getAdditionalPersonToInvolve2Tel() {
		return additionalPersonToInvolve2Tel;
	}
	public void setAdditionalPersonToInvolve2Tel(
			String additionalPersonToInvolve2Tel) {
		this.additionalPersonToInvolve2Tel = additionalPersonToInvolve2Tel;
	}
	public String getOtherRelevantInformation() {
		return otherRelevantInformation;
	}
	public void setOtherRelevantInformation(String otherRelevantInformation) {
		this.otherRelevantInformation = otherRelevantInformation;
	}
	public DateValue getDocumentCreationDate() {
		return documentCreationDate;
	}
	public void setDocumentCreationDate(DateValue documentCreationDate) {
		this.documentCreationDate = documentCreationDate;
	}
	public String getDocumentSetId() {
		return documentSetId;
	}
	public void setDocumentSetId(String documentSetId) {
		this.documentSetId = documentSetId;
	}
	public String getDocumentId() {
		return documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public int getDocumentVersionNumber() {
		return documentVersionNumber;
	}
	public void setDocumentVersionNumber(int documentVersionNumber) {
		this.documentVersionNumber = documentVersionNumber;
	}
	public Address getDocumentAuthorAddress() {
		return documentAuthorAddress;
	}
	public void setDocumentAuthorAddress(Address documentAuthorAddress) {
		this.documentAuthorAddress = documentAuthorAddress;
	}
	public JobRoleName getDocumentAuthorRole() {
		return documentAuthorRole;
	}
	public void setDocumentAuthorRole(JobRoleName documentAuthorRole) {
		this.documentAuthorRole = documentAuthorRole;
	}
	public String getDocumentAuthorSDSID() {
		return documentAuthorSDSID;
	}
	public void setDocumentAuthorSDSID(String documentAuthorSDSID) {
		this.documentAuthorSDSID = documentAuthorSDSID;
	}
	public String getDocumentAuthorTelephone() {
		return documentAuthorTelephone;
	}
	public void setDocumentAuthorTelephone(String documentAuthorTelephone) {
		this.documentAuthorTelephone = documentAuthorTelephone;
	}
	public PersonName getDocumentAuthorName() {
		return documentAuthorName;
	}
	public void setDocumentAuthorName(PersonName documentAuthorName) {
		this.documentAuthorName = documentAuthorName;
	}
	public String getDocumentAuthorOrganisationODSID() {
		return documentAuthorOrganisationODSID;
	}
	public void setDocumentAuthorOrganisationODSID(
			String documentAuthorOrganisationODSID) {
		this.documentAuthorOrganisationODSID = documentAuthorOrganisationODSID;
	}
	public String getDocumentAuthorOrganisationName() {
		return documentAuthorOrganisationName;
	}
	public void setDocumentAuthorOrganisationName(
			String documentAuthorOrganisationName) {
		this.documentAuthorOrganisationName = documentAuthorOrganisationName;
	}
	public DateValue getDocumentAuthoredTime() {
		return documentAuthoredTime;
	}

	public void setDocumentAuthoredTime(DateValue documentAuthoredTime) {
		this.documentAuthoredTime = documentAuthoredTime;
	}

	public String getEPACCSOrganisationODSID() {
		return EPACCSOrganisationODSID;
	}
	public void setEPACCSOrganisationODSID(String ePACCSOrganisationODSID) {
		EPACCSOrganisationODSID = ePACCSOrganisationODSID;
	}
	public String getEPACCSOrganisation() {
		return EPACCSOrganisation;
	}
	public void setEPACCSOrganisation(String ePACCSOrganisation) {
		EPACCSOrganisation = ePACCSOrganisation;
	}

	public PersonName getSeniorResponsibleClinicianName() {
		return SeniorResponsibleClinicianName;
	}

	public void setSeniorResponsibleClinicianName(
			PersonName seniorResponsibleClinicianName) {
		SeniorResponsibleClinicianName = seniorResponsibleClinicianName;
	}

	public String getSeniorResponsibleClinicianSDSID() {
		return SeniorResponsibleClinicianSDSID;
	}

	public void setSeniorResponsibleClinicianSDSID(
			String seniorResponsibleClinicianSDSID) {
		SeniorResponsibleClinicianSDSID = seniorResponsibleClinicianSDSID;
	}

	public String getSeniorResponsibleClinicianORGID() {
		return SeniorResponsibleClinicianORGID;
	}

	public void setSeniorResponsibleClinicianORGID(
			String seniorResponsibleClinicianORGID) {
		SeniorResponsibleClinicianORGID = seniorResponsibleClinicianORGID;
	}

	public String getSeniorResponsibleClinicianORGName() {
		return SeniorResponsibleClinicianORGName;
	}

	public void setSeniorResponsibleClinicianORGName(
			String seniorResponsibleClinicianORGName) {
		SeniorResponsibleClinicianORGName = seniorResponsibleClinicianORGName;
	}

	public JobRoleName getSeniorResponsibleClinicianJobRole() {
		return SeniorResponsibleClinicianJobRole;
	}

	public void setSeniorResponsibleClinicianJobRole(
			JobRoleName seniorResponsibleClinicianJobRole) {
		SeniorResponsibleClinicianJobRole = seniorResponsibleClinicianJobRole;
	}

	public String getSeniorResponsibleClinicianTelephone() {
		return SeniorResponsibleClinicianTelephone;
	}

	public void setSeniorResponsibleClinicianTelephone(
			String seniorResponsibleClinicianTelephone) {
		SeniorResponsibleClinicianTelephone = seniorResponsibleClinicianTelephone;
	}

	public Address getSeniorResponsibleClinicianAddress() {
		return SeniorResponsibleClinicianAddress;
	}

	public void setSeniorResponsibleClinicianAddress(
			Address seniorResponsibleClinicianAddress) {
		SeniorResponsibleClinicianAddress = seniorResponsibleClinicianAddress;
	}

	public String getEPaCCSURL() {
		return EPaCCSURL;
	}

	public void setEPaCCSURL(String ePaCCSURL) {
		EPaCCSURL = ePaCCSURL;
	}



	@Override
	public String toString() {
		return "EndOfLifeCareISBFields [\nepaccsRecordCreationDate="
				+ epaccsRecordCreationDate + ", \nepaccsRecordAuthoredDate="
				+ epaccsRecordAuthoredDate + ", \nepaccsRecordAuthorAddress="
				+ epaccsRecordAuthorAddress + ", \nepaccsRecordAuthorRole="
				+ epaccsRecordAuthorRole + ", \nepaccsRecordAuthorSDSID="
				+ epaccsRecordAuthorSDSID + ", \nepaccsRecordAuthorTelephone="
				+ epaccsRecordAuthorTelephone + ", \nepaccsRecordAuthorName="
				+ epaccsRecordAuthorName
				+ ", \nepaccsRecordAuthorOrganisationODSID="
				+ epaccsRecordAuthorOrganisationODSID
				+ ", \nepaccsRecordAuthorOrganisationName="
				+ epaccsRecordAuthorOrganisationName
				+ ", \nepaccsRecordReviewDate=" + epaccsRecordReviewDate
				+ ", \nepaccsRecordUpdatedDate=" + epaccsRecordUpdatedDate
				+ ", \nepaccsRecordUpdateAuthorAddress="
				+ epaccsRecordUpdateAuthorAddress
				+ ", \nepaccsRecordUpdateAuthorRole="
				+ epaccsRecordUpdateAuthorRole
				+ ", \nepaccsRecordUpdateAuthorSDSID="
				+ epaccsRecordUpdateAuthorSDSID
				+ ", \nepaccsRecordUpdateAuthorTelephone="
				+ epaccsRecordUpdateAuthorTelephone
				+ ", \nepaccsRecordUpdateAuthorName="
				+ epaccsRecordUpdateAuthorName
				+ ", \nepaccsRecordUpdateAuthorOrganisationODSID="
				+ epaccsRecordUpdateAuthorOrganisationODSID
				+ ", \nepaccsRecordUpdateAuthorOrganisationName="
				+ epaccsRecordUpdateAuthorOrganisationName + ", \npatientName="
				+ patientName + ", \npatientPreferredName="
				+ patientPreferredName + ", \npatientBirthDate="
				+ patientBirthDate + ", \npatientNHSNo=" + patientNHSNo
				+ ", \npatientNHSNoIsTraced=" + patientNHSNoIsTraced
				+ ", \npatientGender=" + patientGender
				+ ", \npatientInterpreterNeeded=" + patientInterpreterNeeded
				+ ", \npatientPreferredSpokenLanguage="
				+ patientPreferredSpokenLanguage + ", \npatientDisability="
				+ patientDisability + ", \npatientAddress=" + patientAddress
				+ ", \npatientTelephone=" + patientTelephone + ", \npatientMobile="
				+ patientMobile + ", \nmainInformalCarerName="
				+ mainInformalCarerName + ", \nmainInformalCarerTel="
				+ mainInformalCarerTel + ", \nmainInformalCarerAwareOfPrognosis="
				+ mainInformalCarerAwareOfPrognosis
				+ ", \nprognosisAwarenessRecordedDate="
				+ prognosisAwarenessRecordedDate + ", \nusualGPName="
				+ usualGPName + ", \nusualGPOrgName=" + usualGPOrgName
				+ ", \nusualGPODSCode=" + usualGPODSCode + ", \nusualGPTelephone="
				+ usualGPTelephone + ", \nusualGPFax=" + usualGPFax
				+ ", \nusualGPAddress=" + usualGPAddress + ", \nkeyWorkerName="
				+ keyWorkerName + ", \nkeyWorkerTelephone=" + keyWorkerTelephone
				+ ", \nkeyWorkerAddress=" + keyWorkerAddress
				+ ", \nkeyWorkerJobRole=" + keyWorkerJobRole
				+ ", \nkeyWorkerSDSID=" + keyWorkerSDSID + ", \nkeyWorkerOrgID="
				+ keyWorkerOrgID + ", \nkeyWorkerOrgName=" + keyWorkerOrgName
				+ ", \nformalCarers=" + formalCarers + ", \nprimaryEOLCDiagnosis="
				+ primaryEOLCDiagnosis + ", \notherRelevantDiagnoses="
				+ otherRelevantDiagnoses + ", \nallergiesAndAdverseReactions="
				+ allergiesAndAdverseReactions
				+ ", \nanticipatoryMedicinesIssued="
				+ anticipatoryMedicinesIssued
				+ ", \nanticipatoryMedicinesLocation="
				+ anticipatoryMedicinesLocation
				+ ", \nanticipatoryMedicinesDateIssued="
				+ anticipatoryMedicinesDateIssued + ", \neolcTool=" + eolcTool
				+ ", \neolcPathwayStageNONISB=" + eolcPathwayStageNONISB
				+ ", \nadvanceStatements=" + advanceStatements
				+ ", \npreferredPlaceOfDeath=" + preferredPlaceOfDeath
				+ ", \npreferredPlaceOfDeathOrganisation="
				+ preferredPlaceOfDeathOrganisation
				+ ", \npreferredPlaceOfDeathAddress="
				+ preferredPlaceOfDeathAddress
				+ ", \npreferredPlaceOfDeathIsUPR=" + preferredPlaceOfDeathIsUPR
				+ ", \npreferredPlaceOfDeath2=" + preferredPlaceOfDeath2
				+ ", \npreferredPlaceOfDeath2Organisation="
				+ preferredPlaceOfDeath2Organisation
				+ ", \npreferredPlaceOfDeath2Address="
				+ preferredPlaceOfDeath2Address
				+ ", \npreferredPlaceOfDeath2IsUPR="
				+ preferredPlaceOfDeath2IsUPR + ", \nDNACPR=" + DNACPR
				+ ", \nDNACPRDate=" + DNACPRDate + ", \nDNACPRReviewDate="
				+ DNACPRReviewDate + ", \nDNACPRCreatedDate=" + DNACPRCreatedDate
				+ ", \nDNACPRLocation=" + DNACPRLocation + ", \nADRT=" + ADRT
				+ ", \nADRTDiscussedWithClinicianNONISB="
				+ ADRTDiscussedWithClinicianNONISB + ", \nADRTDocumentLocation="
				+ ADRTDocumentLocation + ", \nADRTRecordedDate="
				+ ADRTRecordedDate + ", \nLPAName=" + LPAName + ", \nLPAAuthority="
				+ LPAAuthority + ", \nLPADate=" + LPADate + ", \nLPATelephone="
				+ LPATelephone + ", \nadditionalPersonToInvolve="
				+ additionalPersonToInvolve + ", \nadditionalPersonToInvolveTel="
				+ additionalPersonToInvolveTel
				+ ", \nadditionalPersonToInvolve2=" + additionalPersonToInvolve2
				+ ", \nadditionalPersonToInvolve2Tel="
				+ additionalPersonToInvolve2Tel + ", \notherRelevantInformation="
				+ otherRelevantInformation + ", \ndocumentCreationDate="
				+ documentCreationDate + ", \ndocumentSetId=" + documentSetId
				+ ", \ndocumentVersionNumber=" + documentVersionNumber
				+ ", \ndocumentAuthorAddress=" + documentAuthorAddress
				+ ", \ndocumentAuthorRole=" + documentAuthorRole
				+ ", \ndocumentAuthorSDSID=" + documentAuthorSDSID
				+ ", \ndocumentAuthorTelephone=" + documentAuthorTelephone
				+ ", \ndocumentAuthorName=" + documentAuthorName
				+ ", \ndocumentAuthorOrganisationODSID="
				+ documentAuthorOrganisationODSID
				+ ", \ndocumentAuthorOrganisationName="
				+ documentAuthorOrganisationName + ", \ndocumentAuthoredTime="
				+ documentAuthoredTime + ", \nEPACCSOrganisationODSID="
				+ EPACCSOrganisationODSID + ", \nEPACCSOrganisation="
				+ EPACCSOrganisation + ", \nSeniorResponsibleClinicianName="
				+ SeniorResponsibleClinicianName
				+ ", \nSeniorResponsibleClinicianSDSID="
				+ SeniorResponsibleClinicianSDSID
				+ ", \nSeniorResponsibleClinicianORGID="
				+ SeniorResponsibleClinicianORGID
				+ ", \nSeniorResponsibleClinicianORGName="
				+ SeniorResponsibleClinicianORGName
				+ ", \nSeniorResponsibleClinicianJobRole="
				+ SeniorResponsibleClinicianJobRole
				+ ", \nSeniorResponsibleClinicianTelephone="
				+ SeniorResponsibleClinicianTelephone
				+ ", \nSeniorResponsibleClinicianAddress="
				+ SeniorResponsibleClinicianAddress + ", \nEPaCCSURL="
				+ EPaCCSURL + "]";
	}
}
