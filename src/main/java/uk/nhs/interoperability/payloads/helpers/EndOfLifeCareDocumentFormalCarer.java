/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Date;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.endoflifecarev1.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.vocabularies.generated.*;
import uk.nhs.interoperability.payloads.vocabularies.internal.*;

public class EndOfLifeCareDocumentFormalCarer {

	private PersonName name;
	private JobRoleName role;
	private String telephone;
	
	public EndOfLifeCareDocumentFormalCarer() {
	}
	
	public EndOfLifeCareDocumentFormalCarer(PersonName name, String tel, JobRoleName role) {
		this.name = name;
		this.telephone = tel;
		this.role = role;
	}
	public EndOfLifeCareDocumentFormalCarer(PersonName name, String tel) {
		this.name = name;
		this.telephone = tel;
	}
	
	public PersonName getName() {
		return name;
	}
	public void setName(PersonName name) {
		this.name = name;
	}
	public JobRoleName getRole() {
		return role;
	}
	public void setRole(JobRoleName role) {
		this.role = role;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "EndOfLifeCareDocumentFormalCarer [name=" + name + ", role="
				+ role + ", telephone=" + telephone + "]";
	}
}
