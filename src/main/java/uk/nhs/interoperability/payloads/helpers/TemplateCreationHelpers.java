package uk.nhs.interoperability.payloads.helpers;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.DateRange;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PatientID;
import uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.commontypes.RoleID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.commontypes.WorkgroupID;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.templates.Author;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.DocumentParticipantUniversal;
import uk.nhs.interoperability.payloads.templates.EncompassingEncounter;
import uk.nhs.interoperability.payloads.templates.Participant;
import uk.nhs.interoperability.payloads.templates.PatientUniversal;
import uk.nhs.interoperability.payloads.templates.PatientUniversalv2;
import uk.nhs.interoperability.payloads.templates.PersonUniversal;
import uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.Recipient;
import uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal;
import uk.nhs.interoperability.payloads.templates.ServiceEvent;
import uk.nhs.interoperability.payloads.templates.ServiceEventPerformer;
import uk.nhs.interoperability.payloads.templates.WorkgroupUniversal;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAOrganizationProviderType;
import uk.nhs.interoperability.payloads.vocabularies.internal.AddressType;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7PerformerType;
import uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;
import uk.nhs.interoperability.payloads.vocabularies.internal.WorkgroupIDType;

public class TemplateCreationHelpers {
	
	public static PatientUniversalv2 createPatientv2(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		
		// Null checks for mandatory fields
		if (commonFields.getPatientNHSNo() == null && commonFields.getPatientLocalID() == null) {
			missingFields.addMissingField("PatientNHSNo", "Either an NHS number or local ID for the patient must be provided");
		}
		
		if (commonFields.getPatientNHSNo() != null && commonFields.getPatientNHSNoIsTraced() == null) {
			missingFields.addMissingField("PatientNHSNoIsTraced", "The tracing status for the NHS number must be provided");
		}
		
		if (commonFields.getPatientLocalID() != null && commonFields.getPatientLocalIDAssigningAuthority() == null) {
			missingFields.addMissingField("PatientLocalIDAssigningAuthority", "When a local patient ID is provided, the assigning authority must also be provided");
		}
		
		if (commonFields.getPatientAddress() == null) {
			missingFields.addMissingField("PatientAddress", "The patient's address must be provided");
		}
		if (commonFields.getPatientName() == null) {
			missingFields.addMissingField("PatientName", "The patient's name must be provided");
		}
		//if (commonFields.getPatientGender() == null) {
		//	missingFields.addMissingField("PatientGender", "The patient's gender must be provided");
		//}
		if (commonFields.getPatientBirthDate() == null) {
			missingFields.addMissingField("PatientBirthDate", "The patient's date of birth must be provided");
		}
		if (commonFields.getUsualGPODSCode() != null) {
			if (commonFields.getUsualGPOrgName() == null) {
				missingFields.addMissingField("UsualGPOrgName", "If a GP ORG ID is provided, an Org name must also be provided");
			}
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		PatientUniversalv2 template = new PatientUniversalv2();
		
		// NHS Number and trace status
		if (commonFields.getPatientNHSNo() != null) {
			if (commonFields.getPatientNHSNoIsTraced().booleanValue()) {
				template.addPatientID(new PatientIDWithTraceStatuses()
											.setPatientID(commonFields.getPatientNHSNo())
											.setPatientIDType(PatientIDType.VerifiedNHSNumber.code)
											.setNHSNoTraceStatus(NHSNumberTraceStatus.Traced.code));
			} else {
				template.addPatientID(new PatientIDWithTraceStatuses()
											.setPatientID(commonFields.getPatientNHSNo())
											.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code)
											.setNHSNoTraceStatus(NHSNumberTraceStatus.PresentButNotTraced.code));
			}
		}
		
		// Local patient ID
		if (commonFields.getPatientLocalID() != null) {
			template.addPatientID(new PatientIDWithTraceStatuses()
										.setPatientID(commonFields.getPatientLocalID())
										.setPatientIDType(PatientIDType.LocalID.code)
										.setAssigningOrganisation(commonFields.getPatientLocalIDAssigningAuthority()));
		}
		
		// Address
		template.addAddress(commonFields.getPatientAddress());
		
		// Telephone
		if (commonFields.getPatientTelephone() != null) {
			template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:" + commonFields.getPatientTelephone()));
		}
		
 		// Mobile
		if (commonFields.getPatientMobile() != null) {
			template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:" + commonFields.getPatientMobile())
									.setTelecomType(TelecomUseType.MobileContact.code));
		}
		
		// Name
		template.addPatientName(commonFields.getPatientName());

		// Gender (actually sex)
		if (commonFields.getPatientGender() != null) {
			template.setSex(commonFields.getPatientGender());
		} else {
			template.setSexNullFlavour(NullFlavour.Unknown.code);
		}

		// Date of birth
		template.setBirthTime(commonFields.getPatientBirthDate());
		
		// Usual GP is no longer mandatory...
		if (commonFields.getUsualGPODSCode() != null) {
			
			// Org type
			template.setOrganisationType(CDAOrganizationProviderType._GPPractice);
			
			// Usual GP ODS Code:
			template.setOrganisationId(new OrgID()
										.setID(commonFields.getUsualGPODSCode())
										.setType(OrgIDType.ODSOrgID.code));
			
			// Usual GP Org Name
			template.setOrganisationName(commonFields.getUsualGPOrgName());
			
			// Usual GP Telephone
			if (commonFields.getUsualGPTelephone() != null) {
				template.addOrganisationTelephone(new Telecom()
										.setTelecom("tel:" + commonFields.getUsualGPTelephone())
										.setTelecomType(TelecomUseType.WorkPlace.code));
			}
			
			// Usual GP Fax
			if (commonFields.getUsualGPFax() != null) {
				template.addOrganisationTelephone(new Telecom()
										.setTelecom("fax:" + commonFields.getUsualGPFax())
										.setTelecomType(TelecomUseType.WorkPlace.code));
			}
			
			// Usual GP Address
			if (commonFields.getUsualGPAddress() != null) {
				Address add = commonFields.getUsualGPAddress();
				add.setAddressUse(AddressType.WorkPlace.code);
				template.setOrganisationAddress(add);
			}
		}
		
		return template;
	}
	
	public static PatientUniversal createPatient(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		
		// Null checks for mandatory fields
		if (commonFields.getPatientNHSNoIsTraced() == null) {
			missingFields.addMissingField("PatientNHSNoIsTraced", "The tracing status for the NHS number must be provided");
		}
		if (commonFields.getPatientNHSNo() == null) {
			missingFields.addMissingField("PatientNHSNo", "The patient's NHS number must be provided");
		}
		if (commonFields.getPatientAddress() == null) {
			missingFields.addMissingField("PatientAddress", "The patient's address must be provided");
		}
		if (commonFields.getPatientName() == null) {
			missingFields.addMissingField("PatientName", "The patient's name must be provided");
		}
		if (commonFields.getPatientGender() == null) {
			missingFields.addMissingField("PatientGender", "The patient's gender must be provided");
		}
		if (commonFields.getPatientBirthDate() == null) {
			missingFields.addMissingField("PatientBirthDate", "The patient's date of birth must be provided");
		}
		if (commonFields.getUsualGPODSCode() == null) {
			missingFields.addMissingField("UsualGPODSCode", "The usual GP's ODS Code must be provided");
		}
		if (commonFields.getUsualGPOrgName() == null) {
			missingFields.addMissingField("UsualGPOrgName", "The usual GP's organisation name must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		PatientUniversal template = new PatientUniversal();
		// NHS Number and trace status
		if (commonFields.getPatientNHSNoIsTraced().booleanValue()) {
			template.addPatientID(new PatientID().setPatientID(commonFields.getPatientNHSNo())
										.setPatientIDType(PatientIDType.VerifiedNHSNumber.code));
		} else {
			template.addPatientID(new PatientID().setPatientID(commonFields.getPatientNHSNo())
					.setPatientIDType(PatientIDType.UnverifiedNHSNumber.code));
		}
		
		// Address
		template.addAddress(commonFields.getPatientAddress());
		
		// Telephone
		if (commonFields.getPatientTelephone() != null) {
			template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:" + commonFields.getPatientTelephone()));
		}
		
		// Mobile
		if (commonFields.getPatientMobile() != null) {
			template.addTelephoneNumber(new Telecom()
									.setTelecom("tel:" + commonFields.getPatientMobile())
									.setTelecomType(TelecomUseType.MobileContact.code));
		}
		
		// Name
		template.addPatientName(commonFields.getPatientName());

		// Gender (actually sex)
		if (commonFields.getPatientGender() != null) {
			template.setSex(commonFields.getPatientGender());
		}

		// Date of birth
		template.setBirthTime(commonFields.getPatientBirthDate());
		
		// Usual GP ODS Code:
		template.setRegisteredGPOrgId(new OrgID()
									.setID(commonFields.getUsualGPODSCode())
									.setType(OrgIDType.ODSOrgID.code));
		
		// Usual GP Org Name
		template.setRegisteredGPOrgName(commonFields.getUsualGPOrgName());
		
		// Usual GP Telephone
		if (commonFields.getUsualGPTelephone() != null) {
			template.addRegisteredGPTelephone(new Telecom()
									.setTelecom("tel:" + commonFields.getUsualGPTelephone())
									.setTelecomType(TelecomUseType.WorkPlace.code));
		}
		
		// Usual GP Fax
		if (commonFields.getUsualGPFax() != null) {
			template.addRegisteredGPTelephone(new Telecom()
									.setTelecom("fax:" + commonFields.getUsualGPFax())
									.setTelecomType(TelecomUseType.WorkPlace.code));
		}
		
		// Usual GP Address
		if (commonFields.getUsualGPAddress() != null) {
			Address add = commonFields.getUsualGPAddress();
			add.setAddressUse(AddressType.WorkPlace.code);
			template.setRegisteredGPAddress(add);
		}
		
		return template;
	}
	
	/**
	 * Selects an appropriate author template to use based on the fields supplied, and creates the relevant
	 * content.
	 * @param commonFields Input fields
	 * @return Author content
	 * @throws MissingMandatoryFieldException
	 */
	public static Author createAuthor(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		if (commonFields.getDocumentAuthorLocalID() != null || commonFields.getDocumentAuthorSDSID() != null) {
			// We have an ID for a person, use the AuthorPersonUniversal template
			return createAuthorPerson(commonFields);
		} else if (commonFields.getDocumentAuthorWorkgroupName() != null) {
			return createAuthorWorkgroup(commonFields);
		} else {
			MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
			missingFields.addMissingField("DocumentAuthorLocalID", "Either a local or SDS ID of the document author, or the name of a workgroup for the author must be provided");
			throw missingFields;
		}
	}
	
	public static AuthorPersonUniversal createAuthorPerson(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		
		// Null checks for mandatory fields
		if (commonFields.getDocumentAuthorSDSID() == null && commonFields.getDocumentAuthorLocalID() == null) {
			missingFields.addMissingField("DocumentAuthorLocalID", "Either a local or SDS ID of the document author must be provided");
		}
		if (commonFields.getDocumentAuthorLocalID() != null && commonFields.getDocumentAuthorLocalIDAssigningAuthority() == null) {
			missingFields.addMissingField("DocumentAuthorLocalIDAssigningAuthority", "When using a local ID for the document author, an assigning authority must be provided");
		}
		if (commonFields.getDocumentAuthorRole() == null) {
			missingFields.addMissingField("DocumentAuthorRole", "The job role of the document author must be provided");
		}
		if (commonFields.getDocumentAuthorName() == null) {
			missingFields.addMissingField("DocumentAuthorName", "The name of the document author must be provided");
		}
		if (commonFields.getDocumentAuthorOrganisationODSID() == null) {
			missingFields.addMissingField("DocumentAuthorOrganisationODSID", "The ID of the organisation the document author belongs to must be provided");
		}
		if (commonFields.getDocumentAuthorOrganisationName() == null) {
			missingFields.addMissingField("DocumentAuthorOrganisationName", "The name of the organisation the document author belongs to must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		AuthorPersonUniversal template = new AuthorPersonUniversal();
 		
		// Author SDS ID
		if (commonFields.getDocumentAuthorSDSID() != null) {
			template.addId(new PersonID()
	 						.setType(PersonIDType.SDSID.code)
	 						.setID(commonFields.getDocumentAuthorSDSID()));
		}
		
		// Author Local ID
		if (commonFields.getDocumentAuthorLocalID() != null) {
			template.addId(new PersonID()
	 						.setType(PersonIDType.LocalPersonID.code)
	 						.setID(commonFields.getDocumentAuthorLocalID())
	 						.setAssigningOrganisation(commonFields.getDocumentAuthorLocalIDAssigningAuthority()));
		}
		
		// Author SID Role ID
		if (commonFields.getDocumentAuthorSDSRoleID() != null) {
			template.addId(new PersonID()
	 						.setType(PersonIDType.SDSRoleProfile.code)
	 						.setID(commonFields.getDocumentAuthorSDSRoleID()));
		}
		
		// Author Job Role
 		template.setJobRoleName(commonFields.getDocumentAuthorRole());
 		
 		// Author Address
 		if (commonFields.getDocumentAuthorAddress() != null) {
 			Address add = commonFields.getDocumentAuthorAddress();
 			add.setAddressUse(AddressType.WorkPlace.code);
 			template.addAddress(add);
 		}
 		
 		// Author telephone number
 		if (commonFields.getDocumentAuthorTelephone() != null) {
 			template.addTelephoneNumber(new Telecom("tel:" + commonFields.getDocumentAuthorTelephone()));
 		}
 		
 		// Author Name
 		template.setName(commonFields.getDocumentAuthorName());
 		
 		// Author ORG ID
		template.setOrganisationId(new OrgID()
									.setID(commonFields.getDocumentAuthorOrganisationODSID())
									.setType(OrgIDType.ODSOrgID.code));
		
		// Author ORG Name
		template.setOrganisationName(commonFields.getDocumentAuthorOrganisationName());
		
		return template;
	}
	
	public static WorkgroupUniversal createAuthorWorkgroup(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		
		// Null checks for mandatory fields
		if (commonFields.getDocumentAuthorWorkgroupLocalID() != null && commonFields.getDocumentAuthorWorkgroupLocalIDAssigningAuthority() == null) {
			missingFields.addMissingField("DocumentAuthorWorkgroupLocalIDAssigningAuthority", "When using a local ID for the document author workgroup ID, an assigning authority must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		WorkgroupUniversal template = new WorkgroupUniversal();
 		
		// Workgroup ID
		if (commonFields.getDocumentAuthorWorkgroupSDSID() != null) {
			template.setID(new WorkgroupID()
	 								.setID(commonFields.getDocumentAuthorWorkgroupSDSID())
	 								.setType(WorkgroupIDType.SDSWorkgroupID.code));
		} else if (commonFields.getDocumentAuthorWorkgroupLocalID() != null) {
			template.setID(new WorkgroupID()
									.setID(commonFields.getDocumentAuthorWorkgroupLocalID())
									.setAssigningOrganisation(commonFields.getDocumentAuthorWorkgroupLocalIDAssigningAuthority())
									.setType(WorkgroupIDType.LocalWorkgroupID.code));
		} else {
			// Null ID
			template.setID(new WorkgroupID().setNullFlavour(NullFlavour.NI.code));
		}
		
		// Workgroup Name
 		template.setWorkgroupName(new CodedValue("01",commonFields.getDocumentAuthorWorkgroupName()));
 		
 		// Author telephone number
 		if (commonFields.getDocumentAuthorTelephone() != null) {
 			template.addTelephoneNumber(new Telecom("tel:" + commonFields.getDocumentAuthorTelephone()));
 		}
 		// Author Name
 		template.setPersonName(commonFields.getDocumentAuthorName());
 		// Author ORG ID
		template.setOrgId(new OrgID()
									.setID(commonFields.getDocumentAuthorOrganisationODSID())
									.setType(OrgIDType.ODSOrgID.code));
		// Author ORG Name
		template.setOrgName(commonFields.getDocumentAuthorOrganisationName());
		return template;
	}
	
	public static PersonUniversal createDataEnterer(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		if (commonFields.getDataEntererSDSID() == null && commonFields.getDataEntererLocalID() == null) {
			missingFields.addMissingField("dataEntererLocalID", "Either a local or SDS ID of the data enterer must be provided");
		}
		if (commonFields.getDataEntererLocalID() != null && commonFields.getDataEntererLocalIDAssigningAuthority() == null) {
			missingFields.addMissingField("dataEntererLocalIDAssigningAuthority", "When using a local ID for the data enterer, an assigning authority must be provided");
		}
		if (commonFields.getDataEntererSDSRoleID() == null) {
			missingFields.addMissingField("dataEntererSDSRoleID", "The SDS Role ID of the data enterer must be provided");
		}
		if (commonFields.getDataEntererName() == null) {
			missingFields.addMissingField("dataEntererName", "The name of the data enterer must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		PersonUniversal template = new PersonUniversal();
		
		// SDS ID
		if (commonFields.getDataEntererSDSID() != null) {
			template.addPersonId(new PersonID()
										.setType(PersonIDType.SDSID.code)
										.setID(commonFields.getDataEntererSDSID()));
		}
		
		// Local ID
		if (commonFields.getDataEntererLocalID() != null) {
			template.addPersonId(new PersonID()
										.setType(PersonIDType.LocalPersonID.code)
										.setID(commonFields.getDataEntererLocalID())
										.setAssigningOrganisation(commonFields.getDataEntererLocalIDAssigningAuthority()));
		}
		
		// SDS Role ID
		template.addPersonId(new PersonID()
									.setType(PersonIDType.SDSRoleProfile.code)
									.setID(commonFields.getDataEntererSDSRoleID()));
		// Name
		template.setPersonName(commonFields.getDataEntererName());
		return template;
	}
	
	public static CustodianOrganizationUniversal createCustodian(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		if (commonFields.getCustodianODSCode() == null) {
			missingFields.addMissingField("custodianODSCode", "The ODS ID of the custodian organisation must be provided");
		}
		if (commonFields.getCustodianOrganisationName() == null) {
			missingFields.addMissingField("custodianOrganisationName", "The name of the custodian organisation must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		CustodianOrganizationUniversal template = new CustodianOrganizationUniversal();
		
		// EPaCCS Hosting Org ID
		template.setId(new OrgID(OrgIDType.ODSOrgID.code, commonFields.getCustodianODSCode()));
		
		// EPaCCS Hosting Org Name
		template.setName(commonFields.getCustodianOrganisationName());
		
		return template;
	}
	
	public static Recipient createRecipient(DocumentRecipient recipient) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		if (recipient.getRecipientName() == null) {
			missingFields.addMissingField("recipientName", "The name of the recipient must be provided");
		}
		if (recipient.getRecipientODSCode() == null) {
			missingFields.addMissingField("recipientODSCode", "The ODS Code for the organisation of the recipient must be provided");
		}
		if (recipient.getRecipientOrganisationName() == null) {
			missingFields.addMissingField("recipientOrganisationName", "The organisation name for the recipient must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		RecipientPersonUniversal template = new RecipientPersonUniversal();
		
		// ID (NULL)
		template.addId(new RoleID().setNullFlavour(NullFlavour.NA.code));
		
		// recipientName
		template.setName(recipient.getRecipientName());
		
		// recipientAddress
		if (recipient.getRecipientAddress() != null) {
			template.setAddress(recipient.getRecipientAddress());
		}
		
		// recipientTelephone
 		if (recipient.getRecipientTelephone() != null) {
 			template.addTelephoneNumber(new Telecom("tel:" + recipient.getRecipientTelephone()));
 		}
		
		// recipientJobRole
 		if (recipient.getRecipientJobRole() != null) {
 			template.setJobRoleName(recipient.getRecipientJobRole());
 		}
 		
		// recipientODSCode
 		template.setOrgId(new OrgID()
 								.setID(recipient.getRecipientODSCode())
 								.setType(OrgIDType.ODSOrgID.code));
 		
		// recipientOrganisationName
 		template.setOrgName(recipient.getRecipientOrganisationName());
		
		return template;
	}
	
	public static Participant createParticipant(CDADocumentParticipant participant) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// No mandatory fields in this object
		
		DocumentParticipantUniversal template = new DocumentParticipantUniversal();
		
		// participantRoleClass
		if (participant.getParticipantRoleClass() != null) {
			template.setRoleClass(participant.getParticipantRoleClass().code);
		}
		
		// participantName
		if (participant.getParticipantName() != null) { 
			template.setName(participant.getParticipantName());
		}
		// participantSDSID
		if (participant.getParticipantSDSID() != null) {
			template.addId(new PersonID()
									.setType(PersonIDType.SDSID.code)
									.setID(participant.getParticipantSDSID()));
			if (participant.getParticipantSDSRoleID() == null) {
				missingFields.addMissingField("participantSDSRoleID", "If a participant SDS ID is provided, then an SDS Role ID must also be provided");
				throw missingFields;
			}
			// participantSDSRoleID
			template.addId(new PersonID()
									.setType(PersonIDType.SDSRoleProfile.code)
									.setID(participant.getParticipantSDSRoleID()));
		}
		// participantAddress
		if (participant.getParticipantAddress() != null) {
			template.setAddress(participant.getParticipantAddress());
		}
		// participantTelephone
 		if (participant.getParticipantTelephone() != null) {
 			template.addTelephoneNumber(new Telecom("tel:" + participant.getParticipantTelephone()));
 		}
 		// participantODSCode
 		if (participant.getParticipantODSCode() != null) {
	 		template.setOrgId(new OrgID()
									.setID(participant.getParticipantODSCode())
									.setType(OrgIDType.ODSOrgID.code));
 		}
 		// participantOrganisationName
 		if (participant.getParticipantOrganisationName() != null) {
 			template.setOrgName(participant.getParticipantOrganisationName());
 		}
		return template;
	}
	
	public static PersonUniversal createAuthenticator(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		if (commonFields.getAuthenticatorName() == null) {
			missingFields.addMissingField("authenticatorName", "The name of the authenticator must be provided");
		}
		if (commonFields.getAuthenticatorSDSID() == null && commonFields.getAuthenticatorLocalID() == null) {
			missingFields.addMissingField("authenticatorLocalID", "Either a local or SDS ID for the authenticator must be provided");
		}
		if (commonFields.getAuthenticatorLocalID() != null && commonFields.getAuthenticatorLocalIDAssigningAuthority() == null) {
			missingFields.addMissingField("authenticatorLocalIDAssigningAuthority", "When using a local ID for the authenticator, an assigning authority must be provided");
		}
		if (commonFields.getAuthenticatorSDSRoleID() == null) {
			missingFields.addMissingField("authenticatorSDSRoleID", "The SDS Role ID for the authenticator must be provided");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		PersonUniversal template = new PersonUniversal();
		
		// authenticatorSDSID
		if (commonFields.getAuthenticatorSDSID() != null) {
			template.addPersonId(new PersonID()
										.setID(commonFields.getAuthenticatorSDSID())
										.setType(PersonIDType.SDSID.code));
		}
		
		if (commonFields.getAuthenticatorLocalID() != null) {
			template.addPersonId(new PersonID()
										.setID(commonFields.getAuthenticatorLocalID())
										.setType(PersonIDType.LocalPersonID.code)
										.setAssigningOrganisation(commonFields.getAuthenticatorLocalIDAssigningAuthority()));
		}
		
		// authenticatorSDSRoleID
		template.addPersonId(new PersonID()
									.setID(commonFields.getAuthenticatorSDSRoleID())
									.setType(PersonIDType.SDSRoleProfile.code));
		
		// authenticatorName
		template.setPersonName(commonFields.getAuthenticatorName());
		
		return template;
	}
	
	public static EncompassingEncounter createEncompassingEncounter(CDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		boolean from = commonFields.getEncounterFromTime()!=null;
		boolean to = commonFields.getEncounterToTime()!=null;
		// Null checks for mandatory fields
		if (!from && !to) {
			missingFields.addMissingField("encounterFromTime", "If an encounter is included, it must have a start and/or end time");
		}
		if (commonFields.getEncounterLocationType() == null) {
			missingFields.addMissingField("encounterLocationType", "If an encounter is included, it must have a location type");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		EncompassingEncounter template = new EncompassingEncounter();
		// ID
		template.setId(CDAUUID.generateUUIDString());
		// encounterFromTime
		// encounterToTime
		if (from || to) {
			DateRange r = new DateRange();
			if (from) r.setLow( commonFields.getEncounterFromTime());
			if (to)   r.setHigh(commonFields.getEncounterToTime());
			template.setEffectiveTime(r);
		}
		// encounterType
		template.setCode(commonFields.getEncounterType());
		// encounterLocationType
		template.setEncounterCareSettingType(commonFields.getEncounterLocationType());
		// encounterLocationName
		if (commonFields.getEncounterLocationName() != null) {
			template.setEncounterPlaceName(commonFields.getEncounterLocationName());
		}
		// encounterLocationAddress
		if (commonFields.getEncounterLocationAddress() != null) {
			template.setEncounterPlaceAddress(commonFields.getEncounterLocationAddress());
		}
		return template;
	}
	
	public static ServiceEvent createServiceEvent(NonCodedCDACommonFields commonFields) throws MissingMandatoryFieldException {
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		// Null checks for mandatory fields
		boolean from = commonFields.getEventEffectiveFromTime()!=null;
		boolean to = commonFields.getEventEffectiveToTime()!=null;
		boolean name = commonFields.getEventPerformerName()!=null;
		boolean ods = commonFields.getEventODSCode()!=null;
		boolean org = commonFields.getEventOrganisatioName()!=null;
		
		if ((name && ods && org) || (!name && !ods && !org))  {
			// OK
		} else {
			missingFields.addMissingField("eventPerformerName", "If an event performer is provided, the name, ODS code and organisation name must all be included");
		}
		if (commonFields.getEventType() == null) {
			missingFields.addMissingField("eventType", "If an event performer is provided, the type of event must also be included");
		}
		if (missingFields.hasEntries()) {
			throw missingFields;
		}
		
		ServiceEvent template = new ServiceEvent();
		
		// ID
		template.setId(CDAUUID.generateUUIDString());
		
		// eventClass
		template.setClassCode(commonFields.getEventType().code);
		
		// eventCode
		template.setEventCode(commonFields.getEventCode());
		
		// eventEffectiveFromTime
		// eventEffectiveToTime
		if (from || to) {
			DateRange r = new DateRange();
			if (from) r.setLow( commonFields.getEventEffectiveFromTime());
			if (to)   r.setHigh(commonFields.getEventEffectiveToTime());
			template.setEffectiveTime(r);
		}
		// eventPerformerName
		if (name) {
			PersonWithOrganizationUniversal performer = new PersonWithOrganizationUniversal();
			
			performer.addPersonId(new PersonID().setNullFlavour(NullFlavour.NI.code));
			
			performer.setPersonName(commonFields.getEventPerformerName());
			
			// eventODSID
			performer.setOrgId(new OrgID()
									.setID(commonFields.getEventODSCode())
									.setType(OrgIDType.ODSOrgID.code));
			// eventOrganisatioName
			performer.setOrgName(commonFields.getEventOrganisatioName());
			
			template.addEventPerformer(new ServiceEventPerformer()
												.setPerformer(performer)
												.setPerformerType(HL7PerformerType.Performer.code));
		}
		return template;
	}

}
