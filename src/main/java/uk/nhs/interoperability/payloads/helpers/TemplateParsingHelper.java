/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.List;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.exceptions.ParsePayloadContentException;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.LanguageAbilityProficiency;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PatientIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonIDType;
import uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType;
import uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType;

/**
 * This class coerces payload objects that are templates into the general template
 * object (rather than the payload-specific instantiation of that object), so that
 * we can do some common processing such as extracting names, addresses, etc. This
 * makes the payload-specific helpers much simpler.
 * @author Adam Hatherly
 */
public class TemplateParsingHelper {
	
	public static void getPatientFields(PatientFields fields, PatientUniversal patient,
			ParsePayloadContentException parseExceptions) {
		
		for (PersonName name : patient.getPatientName()) {
			// Preferred name
			if (name.getNameType() != null && name.getNameType().equals(PersonNameType.Preferred.code)) {
				fields.setPatientPreferredName(name);
			} else {
				// Assume anything that is not the preferred name is just their actual name
				if (fields.getPatientName() == null) {
					fields.setPatientName(name);
				}
			}
		}

		// Date of birth
		fields.setPatientBirthDate((DateValue)patient.getBirthTime());
		
		// NHS Number and trace status
		for (PatientID id : patient.getPatientID()) {
			if (id.getPatientIDType().equals(PatientIDType.UnverifiedNHSNumber.code)) {
				fields.setPatientNHSNo(id.getPatientID());
				fields.setPatientNHSNoIsTraced(false);
			} else if (id.getPatientIDType().equals(PatientIDType.VerifiedNHSNumber.code)) {
				fields.setPatientNHSNo(id.getPatientID());
				fields.setPatientNHSNoIsTraced(true);
			} else {
				parseExceptions.addParseError("PatientNHSNo", "No NHS Number was provided");
			}
		}

		// Gender (actually Sex)
		Sex sex = (Sex)HelperUtils.safelyMapCodedValueToVocabEntry(
				patient.getSex(), "Sex", "PatientGender", parseExceptions, true);
		fields.setPatientGender(sex);

		// Address
		if (patient.getAddress() != null) {
			fields.setPatientAddress(patient.getAddress().get(0));
		}
		
		// Telephone and mobile
		if (patient.getTelephoneNumber() != null) {
			for (Telecom tel : patient.getTelephoneNumber()) {
				String type = tel.getTelecomType();
				if (type != null && type.equals(TelecomUseType.MobileContact.code)) {
					fields.setPatientMobile(HelperUtils.stripPrefixFromTelecom(tel.getTelecom()));
				} else {
					// Assume first non-mobile entry is the telephone number
					if (fields.getPatientTelephone() == null) {
						fields.setPatientTelephone(HelperUtils.stripPrefixFromTelecom(tel.getTelecom()));
					}
				}
			}
		}
		
		// Preferred Spoken Language
		if (patient.getLanguages() != null) {
			for (LanguageCommunication language : patient.getLanguages()) {
				if (language.getPreferenceInd() != null && language.getPreferenceInd().equals("true")) {
					// This one is a bit more awkward to turn back into an enum because we only have the code and no OID
					HumanLanguage languageCode = (HumanLanguage)HelperUtils.safelyMapCodedValueToVocabEntry(
							new CodedValue(language.getLanguage(), "", HumanLanguage._en.getOID()), "HumanLanguage",
							"PreferredSpokenLanguage", parseExceptions, false);
					fields.setPatientPreferredSpokenLanguage(languageCode);
				}
				// Interpreter needed (for English)
				if (language.getLanguage().equals(HumanLanguage._en.code)) {
					if (language.getProficiencyLevel().sameAs(LanguageAbilityProficiency._Interpreterrequired)) {
						fields.setPatientInterpreterNeeded(true);
					}
				}
			}
		}

		// Usual GP Org Name
		fields.setUsualGPOrgName(patient.getRegisteredGPOrgName());
		
		// Usual GP ODS Code
		if (patient.getRegisteredGPOrgId().getType().equals(OrgIDType.ODSOrgID.code)) {
			fields.setUsualGPODSCode(patient.getRegisteredGPOrgId().getID());
		} else {
			parseExceptions.addParseError("UsualGPODSCode", "GP Organisation ID provided was not an ODS ID");
		}
		
		// Usual GP Tel
		if (patient.getRegisteredGPTelephone() != null) {
			for (Telecom tel : patient.getRegisteredGPTelephone()) {
				if (HelperUtils.isTel(tel.getTelecom())) {
					fields.setUsualGPTelephone(HelperUtils.stripPrefixFromTelecom(tel.getTelecom()));
				} else if (HelperUtils.isFax(tel.getTelecom())) {
					fields.setUsualGPFax(HelperUtils.stripPrefixFromTelecom(tel.getTelecom()));
				} 
			}
		}

		// Usual GP Address		
		fields.setUsualGPAddress(patient.getRegisteredGPAddress());
	}
	
	/**
	 * Get first non-fax number only
	 * @param author Author
	 * @param fieldName Field Name
	 * @return Telephone number (with no tel: prefix)
	 */
	protected static String getTel(AuthorPersonUniversal author, String fieldName) {
		if (author.getTelephoneNumber() != null) {
			if (author.getTelephoneNumber().size()>0) {
				return getTel(author.getTelephoneNumber());
			}
		}
		return null;
	}

	/**
	 * Get first non-fax number only
	 * @param person Person
	 * @param fieldName Field Name
	 * @return Telephone number (with no tel: prefix)
	 */
	protected static String getTel(PatientRelationshipParticipantRole person, String fieldName) {
		if (person.getTelephoneNumber() != null) {
			if (person.getTelephoneNumber().size()>0) {
				return getTel(person.getTelephoneNumber());
			}
		}
		return null;
	}
	
	/**
	 * Get first fax number only
	 * @param person Person
	 * @param fieldName Field Name
	 * @return Fax number (with no fax: prefix)
	 */
	protected static String getFax(PatientRelationshipParticipantRole person, String fieldName) {
		if (person.getTelephoneNumber() != null) {
			if (person.getTelephoneNumber().size()>0) {
				return getFax(person.getTelephoneNumber());
			}
		}
		return null;
	}
	
	/**
	 * Get first non-fax number only
	 * @param vals Values
	 * @return Telephone number (with no tel: prefix)
	 */
	protected static String getTel(List<Telecom> vals) {
		String result = null;
		if (vals != null) {
			for (Telecom tel : vals) {
				String telStr = tel.getTelecom();
				if (telStr != null) {
					if (result == null && HelperUtils.isTel(telStr)) {
						result = HelperUtils.stripPrefixFromTelecom(telStr);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Get first fax number only
	 * @param vals Values
	 * @return Fax number (with no fax: prefix)
	 */
	protected static String getFax(List<Telecom> vals) {
		String result = null;
		if (vals != null) {
			for (Telecom tel : vals) {
				String telStr = tel.getTelecom();
				if (telStr != null) {
					if (result == null && HelperUtils.isFax(telStr)) {
						result = HelperUtils.stripPrefixFromTelecom(telStr);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Get the address - If there are multiple addresses, just get the first
	 * @param author Author
	 * @return Structured or Unstructured Address
	 */
	protected static Address getAddress(AuthorPersonUniversal author) {
		if (author.getAddress() != null) {
			if (author.getAddress().size()>0) {
				return author.getAddress().get(0);
			}
		}
		return null;
	}
	
	/**
	 * Get the address - Unusually, this template only allows one address
	 * @param person Person
	 * @return Structured or Unstructured Address
	 */
	protected static Address getAddress(PatientRelationshipParticipantRole person) {
		return person.getAddress();
	}
	
	/**
	 * Get the job role. Note: we need to pass in an example of the right vocab below - it doesn't matter what actual entry from the vocab we pass in 
	 * @param author Author
	 * @param fieldName Field Name
	 * @param parseExceptions Parsing exceptions
	 * @return Job role enum value
	 */
	protected static JobRoleName getJobRole(AuthorPersonUniversal author, String fieldName,
										ParsePayloadContentException parseExceptions) {
		JobRoleName roleName = (JobRoleName)HelperUtils.safelyMapCodedValueToVocabEntry(
				author.getJobRoleName(), "JobRoleName", fieldName, parseExceptions, false);
		return roleName;
	}
		
	/**
	 * Get the SDS ID, or return an error if the ID is a different type (e.g. a local ID)
	 * @param author Author
	 * @param fieldName Field Name
	 * @param parseExceptions Parsing exceptions
	 * @return SDS ID String
	 */
	protected static String getSDSID(AuthorPersonUniversal author, String fieldName,
									ParsePayloadContentException parseExceptions) { 
		for (PersonID personID : author.getId()) {
			if (personID.getType().equals(PersonIDType.SDSID.code)) {
				return personID.getID();
			} else {
				parseExceptions.addParseError(fieldName, "Person ID provided was not an SDS ID");
			}
		}
		return null;
	}
	
	/**
	 * Get the SDS ID, or return an error if the ID is a different type (e.g. a local ID)
	 * @param person Person
	 * @param fieldName Field Name
	 * @param parseExceptions Parsing exceptions
	 * @return SDS ID String
	 */
	protected static String getSDSID(PatientRelationshipParticipantRole person, String fieldName,
									ParsePayloadContentException parseExceptions) { 
		if (person.getID() != null) {
			for (PersonID personID : person.getID()) {
				if (personID != null && personID.getID() != null) {
					if (personID.getType() != null) {
						if (personID.getType().equals(PersonIDType.SDSID.code)) {
							return personID.getID();
						} else {
							parseExceptions.addParseError(fieldName, "Person ID provided was not an SDS ID");
						}
					} else {
						parseExceptions.addParseError(fieldName, "Person ID provided did not have an ID type specified");
					}
				}
			}
		}
		return null;
	}

	/**
	 * Get the persons name
	 * @param author Author
	 * @return Author's Name
	 */
	protected static PersonName getName(AuthorPersonUniversal author) {
		return author.getName();
	}
	
	/**
	 * Get the persons name
	 * @param participant Participant
	 * @return Participant's Name
	 */
	protected static PersonName getName(PatientRelationshipParticipantRole participant) {
		return participant.getPersonName();
	}

	/**
	 * Get the ODS ID, or return an error if the ID is a different type (e.g. a local ID)
	 * @param author Author
	 * @param fieldName Field Name
	 * @param parseExceptions Parsing exceptions
	 * @return ODS ID
	 */
	protected static String getODSID(AuthorPersonUniversal author, String fieldName,
			ParsePayloadContentException parseExceptions) {
		// Author Org ODS ID
		if (author.getOrganisationId() != null) {
			if (author.getOrganisationId().getType().equals(OrgIDType.ODSOrgID.code)) {
				return author.getOrganisationId().getID();
			} else {
				parseExceptions.addParseError(fieldName, "Org ID provided was not an ODS ID");
			}
		}
		return null;
	}
	
	/**
	 * Get the ODS ID, or return an error if the ID is a different type (e.g. a local ID)
	 * @param person Person
	 * @param fieldName Field Name
	 * @param parseExceptions Parsing exceptions
	 * @return ODS ID
	 */
	protected static String getODSID(PatientRelationshipParticipantRole person, String fieldName,
			ParsePayloadContentException parseExceptions) {
		if (person.getOrgId() != null) {
			if (person.getOrgId().getType() != null) {
				if (person.getOrgId().getType().equals(OrgIDType.ODSOrgID.code)) {
					return person.getOrgId().getID();
				} else {
					parseExceptions.addParseError(fieldName, "Org ID provided was not an ODS ID");
				}
			} else {
				parseExceptions.addParseError(fieldName, "Org ID type was not provided");
			}
		}
		return null;
	}
	
	/**
	 * Get the organisation name
	 * @param author Author
	 * @return Org Name
	 */
	protected static String getOrgName(AuthorPersonUniversal author) {
		return author.getOrganisationName();
	}
	
	/**
	 * Get the organisation name
	 * @param person Person
	 * @return Org Name
	 */
	protected static String getOrgName(PatientRelationshipParticipantRole person) {
		return person.getOrgDescription();
	}
}
