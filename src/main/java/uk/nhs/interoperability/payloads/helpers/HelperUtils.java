/*
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
    http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.Date;
import java.util.List;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.HL7Date;
import uk.nhs.interoperability.payloads.commontypes.DateRange;
import uk.nhs.interoperability.payloads.exceptions.ParsePayloadContentException;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;

public class HelperUtils {

	public static VocabularyEntry safelyMapCodedValueToVocabEntry(CodedValue codedEntry, String vocabName) {
		return safelyMapCodedValueToVocabEntry(codedEntry, vocabName, null, null, false);
	}
	
	public static VocabularyEntry safelyMapCodedValueToVocabEntry(CodedValue codedEntry, String vocabName,
										String fieldName, ParsePayloadContentException errors, boolean mandatory) {
		if (codedEntry == null) {
			if (mandatory) {
				if (errors != null) {
					errors.addParseError(fieldName, "This coded entry must be provided");
				}
			}
			return null;
		}
		VocabularyEntry entry = VocabularyFactory.getVocab(vocabName, null, codedEntry.getCode());
		if (entry.getOID().equals(codedEntry.getOID())) {
			return entry;
		} else {
			if (errors != null) {
				errors.addParseError(fieldName, "Coded value does not appear to be from correct vocabulary - expected OID:" + entry.getOID() + " but got OID:" + codedEntry.getOID());
			}
		}
		return null;
	}
	
	/**
	 * Strips the prefix from telephone numbers (tel: or fax:)
	 * @param input Telecom value to strip prefix from
	 * @return Telephone number without prefix
	 */
	public static String stripPrefixFromTelecom(String input) {
		int idx = input.indexOf(':');
		if (idx > -1) {
			return input.substring(idx+1);
		} else {
			return input;
		}
	}
	
	public static boolean isFax(String input) {
		return (input.startsWith("fax:"));
	}
	
	public static boolean isTel(String input) {
		return (input.startsWith("tel:"));
	}
	
	public static boolean isEmail(String input) {
		return (input.startsWith("mailto:"));
	}
	
	public static boolean isTextphone(String input) {
		return (input.startsWith("textphone:"));
	}
	
	/**
	 * When provided with a DateRange, and a date to compare to, this method will return true if
	 * the comparison date falls inside the date range provided (i.e. the range does not start
	 * after the comparison date or end before the comparison date). Any "center" date in the
	 * range is ignored.
	 * @param useablePeriod
	 * @param dateToCompareTo
	 * @return true if the comparison date is within the provided date range
	 */
	public static boolean isCurrent(DateRange useablePeriod, HL7Date dateToCompareTo) {
		Date now;
		// If a reference date is provided, use that, otherwise use the current date
		if (dateToCompareTo == null) {
			now = new Date();
		} else {
			now = dateToCompareTo.getDate();
		}
		if (useablePeriod.getLow() != null) {
			Date low = useablePeriod.getLow().getDate();
			if (low.after(now)) {
				// This address is not valid yet (starts in the future)
				return false;
			}
		}
		if (useablePeriod.getHigh() != null) {
			Date high = useablePeriod.getHigh().getDate();
			if (high.before(now)) {
				// This address is not valid any more (ends in the past)
				return false;
			}
		}
		return true;
	}
}
