/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType;

/**
 * This is a class to help simplify the creation of the non-coded CDA Document, and hide some of
 * the complexity of the underlying document. Once created using this helper, the document
 * can still be fine-tuned using the methods in objects created.
 * 
 * @author Adam Hatherly
 *
 */
public class NonCodedCDACommonFields extends CDACommonFields {
	
	private Documenttype documentType;
	
	// Encompassing Encounter
	private CodedValue eventCode;
	private HL7ActType eventType;
	private DateValue eventEffectiveFromTime;
	private DateValue eventEffectiveToTime;
	private PersonName eventPerformerName;
	private String eventODSCode;
	private String eventOrganisatioName;
	
	
	public Documenttype getDocumentType() {
		return documentType;
	}
	public void setDocumentType(Documenttype documentType) {
		this.documentType = documentType;
	}
	
	public CodedValue getEventCode() {
		return eventCode;
	}
	public void setEventCode(CodedValue eventCode) {
		this.eventCode = eventCode;
	}
	public DateValue getEventEffectiveFromTime() {
		return eventEffectiveFromTime;
	}
	public void setEventEffectiveFromTime(DateValue eventEffectiveFromTime) {
		this.eventEffectiveFromTime = eventEffectiveFromTime;
	}
	public DateValue getEventEffectiveToTime() {
		return eventEffectiveToTime;
	}
	public void setEventEffectiveToTime(DateValue eventEffectiveToTime) {
		this.eventEffectiveToTime = eventEffectiveToTime;
	}
	public PersonName getEventPerformerName() {
		return eventPerformerName;
	}
	public void setEventPerformerName(PersonName eventPerformerName) {
		this.eventPerformerName = eventPerformerName;
	}
	public String getEventODSCode() {
		return eventODSCode;
	}
	public void setEventODSCode(String eventODSCode) {
		this.eventODSCode = eventODSCode;
	}
	public String getEventOrganisatioName() {
		return eventOrganisatioName;
	}
	public void setEventOrganisatioName(String eventOrganisatioName) {
		this.eventOrganisatioName = eventOrganisatioName;
	}
	public HL7ActType getEventType() {
		return eventType;
	}
	public void setEventType(HL7ActType eventType) {
		this.eventType = eventType;
	}
}
