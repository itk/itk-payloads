/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createAuthenticator;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createAuthor;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createCustodian;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createDataEnterer;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createEncompassingEncounter;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createParticipant;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createPatientv2;
import static uk.nhs.interoperability.payloads.helpers.TemplateCreationHelpers.createRecipient;

import java.util.Date;

import uk.nhs.interoperability.payloads.CompositionalStatement;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.ConsentID;
import uk.nhs.interoperability.payloads.commontypes.OrgID;
import uk.nhs.interoperability.payloads.commontypes.PersonID;
import uk.nhs.interoperability.payloads.commontypes.Telecom;
import uk.nhs.interoperability.payloads.exceptions.MissingMandatoryFieldException;
import uk.nhs.interoperability.payloads.prsbsections.AdmissionDetailsSection;
import uk.nhs.interoperability.payloads.prsbsections.AllergiesAndAdverseReactionsSection;
import uk.nhs.interoperability.payloads.prsbsections.AssessmentScalesSection;
import uk.nhs.interoperability.payloads.prsbsections.ClinicalSummarySection;
import uk.nhs.interoperability.payloads.prsbsections.DiagnosesSection;
import uk.nhs.interoperability.payloads.prsbsections.DischargeDetailsSection;
import uk.nhs.interoperability.payloads.prsbsections.InformationGivenSection;
import uk.nhs.interoperability.payloads.prsbsections.InvestigationsAndProceduresRequestedSection;
import uk.nhs.interoperability.payloads.prsbsections.LegalInformationSection;
import uk.nhs.interoperability.payloads.prsbsections.MedicationsAndMedicalDevicesSection;
import uk.nhs.interoperability.payloads.prsbsections.ParticipationInResearchSection;
import uk.nhs.interoperability.payloads.prsbsections.PatientAndCarerConcernsSection;
import uk.nhs.interoperability.payloads.prsbsections.PersonCompletingRecordSection;
import uk.nhs.interoperability.payloads.prsbsections.PlanAndRequestedActionsSection;
import uk.nhs.interoperability.payloads.prsbsections.ProceduresSection;
import uk.nhs.interoperability.payloads.prsbsections.SafetyAlertsSection;
import uk.nhs.interoperability.payloads.prsbsections.SocialContextSection;
import uk.nhs.interoperability.payloads.templates.Author;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.Consent;
import uk.nhs.interoperability.payloads.templates.Participant;
import uk.nhs.interoperability.payloads.templates.PatientUniversalv2;
import uk.nhs.interoperability.payloads.templates.Recipient;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.ClinicalDocument;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentParticipant;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentSection;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.InformationOnlyRecipient;
import uk.nhs.interoperability.payloads.toc_edischarge_draftB.PrimaryRecipient;
import uk.nhs.interoperability.payloads.util.CDAUUID;
import uk.nhs.interoperability.payloads.vocabularies.generated.CorrespondenceDocumenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind;
import uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType;
import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;
import uk.nhs.interoperability.payloads.vocabularies.internal.DocumentTypes;
import uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

/**
 * This helper class takes a set of fields in the form of a TransferOfCareFields object, and uses them to
 * create the various Java objects that represent the document. The helper makes a number of assumptions in
 * order to simplify the process of creating a Non-Coded CDA document. For example it assumes that all
 * patients will be identified using an NHS number, and that all staff will be identified using an SDS ID. These
 * assumptions may not fit the specific needs of teams implementing this library in their solution. Developers
 * are encouraged to use this class as a starting point to build on/tweak as required 
 * @author Adam Hatherly
 */
public class TransferOfCareDraftBDocumentCreationHelper {

	public static ClinicalDocument addNonXMLBody(ClinicalDocument document, AttachmentType encoding, String mimeType, String body) {
		document.setNonXMLBodyMediaType(mimeType);
		document.setNonXMLBodyType(encoding.code);
		document.setNonXMLBodyText(body);
		return document;
	}
	
	public static ClinicalDocument createDocument(TransferOfCareFields commonFields) throws MissingMandatoryFieldException {
		
		ClinicalDocument template = new ClinicalDocument();
		MissingMandatoryFieldException missingFields = new MissingMandatoryFieldException();
		DateValue currentDateTime = new DateValue(new Date(), DatePrecision.Minutes);
		
		// ==== We will assume some things and set them accordingly ====
		template.setDocumentId(CDAUUID.generateUUIDString());
		template.setConfidentialityCode(x_BasicConfidentialityKind._N);
		
		// Title
		if (commonFields.getDocumentTitle() != null) {
			template.setDocumentTitle(commonFields.getDocumentTitle());
		} else {
			missingFields.addMissingField("documentTitle", "The document title must be provided");
		}
		
		// Document Type
		if (commonFields.getCareSetting() != null) {
			CompositionalStatement cs = new CompositionalStatement(DocumentTypes.ClinicalDocumentDescriptor);
			cs.addAttributeCodeAndValueCode(DocumentTypes.DocumentType,
											CorrespondenceDocumenttype._Dischargesummary);
			cs.addAttributeCodeAndValueCode(DocumentTypes.CareSetting,
											commonFields.getCareSetting());
			template.setDocumentType(cs);
		} else {
			missingFields.addMissingField("CareSetting", "The care setting must be provided");
		}
		
		// If no record effective date/time specified, assume the current date/time
		if (commonFields.getDocumentEffectiveTime() == null) {
			template.setEffectiveTime(currentDateTime);
		} else {
			template.setEffectiveTime(commonFields.getDocumentEffectiveTime());
		}
		
		// If no document set ID provided, generate a new one
		if (commonFields.getDocumentSetID() != null) {
			template.setDocumentSetId(commonFields.getDocumentSetID());
		} else {
			template.setDocumentSetId(CDAUUID.generateUUIDString());
		}
		
		// Version defaults to 1 unless set to a different integer value
		template.setDocumentVersionNumber(String.valueOf(commonFields.getDocumentVersionNumber()));
		
		// Patient
		try {
			PatientUniversalv2 patient = createPatientv2(commonFields);
			template.setPatient(patient);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Author
		if (commonFields.getTimeAuthored() == null) {
			template.setTimeAuthored(currentDateTime);
		} else {
			template.setTimeAuthored(commonFields.getTimeAuthored());
		}
		
		try {
			Author author = createAuthor(commonFields);
			template.setAuthor(author);
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Data Enterer (optional)
		if (commonFields.getDataEntererName() != null) {
			try {
				template.setDataEnterer(createDataEnterer(commonFields));
			} catch (MissingMandatoryFieldException e) {
				missingFields.addMissingFields(e);
			}
		}
		
		// Custodian
		try {
			template.setCustodianOrganisation(createCustodian(commonFields));
		} catch (MissingMandatoryFieldException e) {
			missingFields.addMissingFields(e);
		}
		
		// Recipients
		
		// Having at least one recipient is mandatory
		if (commonFields.getRecipients()==null) {
			missingFields.addMissingField("recipients", "At least one recipient must be provided");
		} else if (commonFields.getRecipients().size()==0) {
			missingFields.addMissingField("recipients", "At least one recipient must be provided");
		} else {
			// Primary Recipients
			for (DocumentRecipient recipient : commonFields.getRecipients()) {
				try {
					Recipient r = createRecipient(recipient);
					template.addPrimaryRecipients(
								new PrimaryRecipient().setRecipient(r));
				} catch (MissingMandatoryFieldException e) {
					missingFields.addMissingFields(e);
				}
			}
			// Copy Recipients
			if (commonFields.getCopyRecipients() != null) {
				for (DocumentRecipient recipient : commonFields.getCopyRecipients()) {
					try {
						Recipient r = createRecipient(recipient);
						template.addInformationOnlyRecipients(
									new InformationOnlyRecipient().setRecipient(r));
					} catch (MissingMandatoryFieldException e) {
						missingFields.addMissingFields(e);
					}
				}
			}
		}
		
		// Authenticator
		if (commonFields.getAuthenticatorName() != null) {
			if (commonFields.getAuthenticatedTime() == null) {
				missingFields.addMissingField("authenticatedTime", "The time the document was authenticated must be provided");
			} else {
				template.setTimeAuthenticated(commonFields.getAuthenticatedTime());
			}
			try {
				template.setAuthenticator(createAuthenticator(commonFields));
			} catch (MissingMandatoryFieldException e) {
				missingFields.addMissingFields(e);
			}
		}
		
		// Participants
		if (commonFields.getParticipants() != null) {
			for (CDADocumentParticipant participant : commonFields.getParticipants()) {
				if (participant.getParticipantType() == null) {
					missingFields.addMissingField("participantType", "The participant type must be provided");
				}
				try {
					Participant p = createParticipant(participant);
					template.addParticipant((
								new DocumentParticipant()
											.setParticipant(p)
											.setParticipantTypeCode(participant.getParticipantType().code)));
				} catch (MissingMandatoryFieldException e) {
					missingFields.addMissingFields(e);
				}
			}
		}
		
		// Consent
		if (commonFields.getConsent() != null) {
			template.setAuthorizingConsent(new Consent()
													.setConsentCode(commonFields.getConsent())
													.addID(new ConsentID(CDAUUID.generateUUIDString())));
		}
		
		// Encompassing Encounter
		if (commonFields.getEncounterType() != null) {
			template.setEncompassingEncounter(createEncompassingEncounter(commonFields));
		}

		// We have done all the checks on mandatory fields, so if there are any
		// errors, throw them up to the caller
		if (missingFields.hasEntries()) {
			throw missingFields;
		}		
		
		return template; 
	}
	
	public static void addPRSBSections(TransferOfCareFields commonFields, ClinicalDocument template) {
		
		if (commonFields.getAdmissionDetails() != null) {
		template.addDocumentSections(new DocumentSection(
				new AdmissionDetailsSection(commonFields.getAdmissionDetails())
								.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getAllergies() != null) {
			template.addDocumentSections(new DocumentSection(
					new AllergiesAndAdverseReactionsSection(commonFields.getAllergies())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getAssessments() != null) {
			template.addDocumentSections(new DocumentSection(
					new AssessmentScalesSection(commonFields.getAssessments())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getClinicalSummary() != null) {
			template.addDocumentSections(new DocumentSection(
					new ClinicalSummarySection(commonFields.getClinicalSummary())
									.setSectionId(CDAUUID.generateUUIDString())));
		}

		if (commonFields.getDiagnoses() != null) {
			template.addDocumentSections(new DocumentSection(
					new DiagnosesSection(commonFields.getDiagnoses())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getDischargeDetails() != null) {
			template.addDocumentSections(new DocumentSection(
					new DischargeDetailsSection(commonFields.getDischargeDetails())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getInformationGiven() != null) {
			template.addDocumentSections(new DocumentSection(
					new InformationGivenSection(commonFields.getInformationGiven())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getInvestigations() != null) {
			template.addDocumentSections(new DocumentSection(
					new InvestigationsAndProceduresRequestedSection(commonFields.getInvestigations())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getLegal() != null) {
			template.addDocumentSections(new DocumentSection(
					new LegalInformationSection(commonFields.getLegal())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getMedications() != null) {
			
			MedicationsAndMedicalDevicesSection section =
					new MedicationsAndMedicalDevicesSection(commonFields.getMedications());
			
			section.setSectionId(CDAUUID.generateUUIDString());
			
			if (commonFields.getMedicationsAuthorName() != null) {
				// Author name
				AuthorPersonUniversal author = new AuthorPersonUniversal();
				author.setName(commonFields.getMedicationsAuthorName());
				
				if (commonFields.getMedicationsAuthorRole() != null) {
					author.setJobRoleName(commonFields.getMedicationsAuthorRole());
				} else {
					author.setJobRoleNameNullFlavour(NullFlavour.NI.code);
				}
				
				author.addId(new PersonID().setNullFlavour(NullFlavour.NI.code));
				
				// Author telephone number
		 		if (commonFields.getMedicationsAuthorTelephone() != null) {
		 			author.addTelephoneNumber(new Telecom("tel:" + commonFields.getMedicationsAuthorTelephone()));
		 		}
		 		

		 		// Author Org Name
		 		if (commonFields.getMedicationsAuthorOrgName() != null) {
		 			author.setOrganisationName(commonFields.getMedicationsAuthorOrgName());
		 		}
		 		
		 		// Author Org ID
		 		if (commonFields.getMedicationsAuthorODSCode() != null) {
		 			author.setOrganisationId(new OrgID()
		 											.setID(commonFields.getMedicationsAuthorODSCode())
		 											.setType(OrgIDType.ODSOrgID.code));
		 		}
		 		
		 		section.setAuthor(author);
		 		
		 		// Author date
		 		if (commonFields.getMedicationsAuthorDate() != null) {
		 			section.setTimeAuthored(commonFields.getMedicationsAuthorDate());
		 		}
			}
			
			template.addDocumentSections(new DocumentSection(section));
		}
		
		if (commonFields.getResearch() != null) {
			template.addDocumentSections(new DocumentSection(
					new ParticipationInResearchSection(commonFields.getResearch())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getConcerns() != null) {
			template.addDocumentSections(new DocumentSection(
					new PatientAndCarerConcernsSection(commonFields.getConcerns())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getPersonCompletingRecord() != null) {
			template.addDocumentSections(new DocumentSection(
					new PersonCompletingRecordSection(commonFields.getPersonCompletingRecord())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getPlan() != null) {
			template.addDocumentSections(new DocumentSection(
					new PlanAndRequestedActionsSection(commonFields.getPlan())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getProcedures() != null) {
			template.addDocumentSections(new DocumentSection(
					new ProceduresSection(commonFields.getProcedures())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getAlerts() != null) {
			template.addDocumentSections(new DocumentSection(
					new SafetyAlertsSection(commonFields.getAlerts())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
		
		if (commonFields.getSocialContext() != null) {
			template.addDocumentSections(new DocumentSection(
					new SocialContextSection(commonFields.getSocialContext())
									.setSectionId(CDAUUID.generateUUIDString())));
		}
	}
	
	
}
