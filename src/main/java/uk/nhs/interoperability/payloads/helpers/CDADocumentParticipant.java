package uk.nhs.interoperability.payloads.helpers;

import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.CDAPersonRelationshipType;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.RoleClassAssociative;

public class CDADocumentParticipant {
	private PersonName participantName;
	private String participantSDSID;
	private String participantSDSRoleID;
	private Address participantAddress;
	private String participantTelephone;
	private String participantODSCode;
	private String participantOrganisationName;
	private ParticipationType participantType;
	private RoleClassAssociative participantRoleClass;
	
	public PersonName getParticipantName() {
		return participantName;
	}
	public void setParticipantName(PersonName participantName) {
		this.participantName = participantName;
	}
	public String getParticipantSDSID() {
		return participantSDSID;
	}
	public void setParticipantSDSID(String participantSDSID) {
		this.participantSDSID = participantSDSID;
	}
	public String getParticipantSDSRoleID() {
		return participantSDSRoleID;
	}
	public void setParticipantSDSRoleID(String participantSDSRoleID) {
		this.participantSDSRoleID = participantSDSRoleID;
	}
	public Address getParticipantAddress() {
		return participantAddress;
	}
	public void setParticipantAddress(Address participantAddress) {
		this.participantAddress = participantAddress;
	}
	public String getParticipantTelephone() {
		return participantTelephone;
	}
	public void setParticipantTelephone(String participantTelephone) {
		this.participantTelephone = participantTelephone;
	}
	public String getParticipantODSCode() {
		return participantODSCode;
	}
	public void setParticipantODSCode(String participantODSCode) {
		this.participantODSCode = participantODSCode;
	}
	public String getParticipantOrganisationName() {
		return participantOrganisationName;
	}
	public void setParticipantOrganisationName(String participantOrganisationName) {
		this.participantOrganisationName = participantOrganisationName;
	}
	public ParticipationType getParticipantType() {
		return participantType;
	}
	public void setParticipantType(ParticipationType participantType) {
		this.participantType = participantType;
	}
	public RoleClassAssociative getParticipantRoleClass() {
		return participantRoleClass;
	}
	public void setParticipantRoleClass(RoleClassAssociative participantRoleClass) {
		this.participantRoleClass = participantRoleClass;
	}
	
}
