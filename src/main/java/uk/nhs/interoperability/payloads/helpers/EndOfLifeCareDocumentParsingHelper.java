/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument;
import uk.nhs.interoperability.payloads.endoflifecarev1.CodedSections;
import uk.nhs.interoperability.payloads.endoflifecarev1.TextSections;
import uk.nhs.interoperability.payloads.exceptions.ParsePayloadContentException;
import uk.nhs.interoperability.payloads.templates.AdvanceDecisionToRefuseTreatment;
import uk.nhs.interoperability.payloads.templates.AnticipatoryMedicineBoxIssueProcedure;
import uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal;
import uk.nhs.interoperability.payloads.templates.AuthoritytoLastingPowerofAttorney;
import uk.nhs.interoperability.payloads.templates.CodedSection;
import uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal;
import uk.nhs.interoperability.payloads.templates.DNACPRDecisionbySeniorResponsibleClinician;
import uk.nhs.interoperability.payloads.templates.EOLPatientRelationships;
import uk.nhs.interoperability.payloads.templates.EoLCarePlan;
import uk.nhs.interoperability.payloads.templates.PatientRelationshipParticipantRole;
import uk.nhs.interoperability.payloads.templates.PrognosisAwareness;
import uk.nhs.interoperability.payloads.templates.RelatedPersonDetails;
import uk.nhs.interoperability.payloads.templates.Section2;
import uk.nhs.interoperability.payloads.templates.TextSection;
import uk.nhs.interoperability.payloads.vocabularies.generated.ADRTDiscussedSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.AuthoritytoLPASnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.DNACPRprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLADRTprefSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLAnticipatoryMedicineBoxIssueSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.EoLToolSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.PrognosisAwarenessSnCT;
import uk.nhs.interoperability.payloads.vocabularies.internal.OrgIDType;

public class EndOfLifeCareDocumentParsingHelper {
	private static final Logger logger = LoggerFactory.getLogger(EndOfLifeCareDocumentParsingHelper.class);
	
	public static EndOfLifeCareISBFields getISBFields(ClinicalDocument document) {
		EndOfLifeCareISBFields fields = new EndOfLifeCareISBFields();
		ParsePayloadContentException parseExceptions = new ParsePayloadContentException(); 
		
		// Get overall document fields
		getDocumentFields(fields, document, parseExceptions);
		
		// Patient
		TemplateParsingHelper.getPatientFields(fields, document.getPatient(), parseExceptions);

		// Custodian (Organisation hosting the EPaCCS)
		if (document.getCustodianOrganisation() != null) {
			String className = ((Payload)(document.getCustodianOrganisation())).getClassName();
			if (className.equals("CustodianOrganizationUniversal")) {
				getCustodianFields(fields, (CustodianOrganizationUniversal)document.getCustodianOrganisation(), parseExceptions);
			}
		}

		// Loop through coded sections, extracting values
		List<CodedSections> codedSections = document.getCodedSections();
		for (CodedSections s : codedSections) {
			CodedSection codedSection = s.getCodedEntry();
			String className = ((Payload)codedSection).getClassName();
			
			// End of life care plan
			if (className.equals("EoLCarePlan")) {
				EoLCarePlan eolcp = (EoLCarePlan)codedSection;
				getEoLCPFields(fields, eolcp, parseExceptions);
			}
			
			// Advance decision to refuse treatment
			if (className.equals("AdvanceDecisionToRefuseTreatment")) {
				AdvanceDecisionToRefuseTreatment adrt = (AdvanceDecisionToRefuseTreatment)codedSection;
				getADRTFields(fields, adrt, parseExceptions);
			}
			
			// Anticipatory medicine box
			if (className.equals("AnticipatoryMedicineBoxIssueProcedure")) {
				AnticipatoryMedicineBoxIssueProcedure ambox = (AnticipatoryMedicineBoxIssueProcedure)codedSection;
				getAMBoxFields(fields, ambox, parseExceptions);
			}
			
			// DNACPR Decision
			if (className.equals("DNACPRDecisionbySeniorResponsibleClinician")) {
				DNACPRDecisionbySeniorResponsibleClinician dnacpr = (DNACPRDecisionbySeniorResponsibleClinician)codedSection;
				getDNACPRFields(fields, dnacpr, parseExceptions);
			}
			
			// Main informal carer awareness of prognosis
			if (className.equals("PrognosisAwareness")) {
				PrognosisAwareness aware = (PrognosisAwareness)codedSection;
				getPrognosisAwarenessFields(fields, aware, parseExceptions);
			}
			
			// Authority to Lasting Power of Attorney
			if (className.equals("AuthoritytoLastingPowerofAttorney")) {
				AuthoritytoLastingPowerofAttorney lpa = (AuthoritytoLastingPowerofAttorney)codedSection;
				getLPAFields(fields, lpa, parseExceptions);
			}
		}
		
		// Loop through text sections, extracting values
		List<TextSections> textSections = document.getTextSections();
		for (TextSections s : textSections) {
			TextSection section = s.getTextSection();
			
			// NOTE: Currently we unfortunately have to rely on the titles to find the right sections. This
			// will improve in a later release, which will have reliable identifiers for each text section to
			// allow us to extract the values more safely.
			if (section.getTitle().equals("Patient Choices")) {
				for (Section2 section2 : section.getSection2()) {
					if (section2.getTitle().equals("First preferred place of death")) {
						// ARGH - Can't parse this all back out to separate fields..
					}
					else if (section2.getTitle().equals("Second preferred place of death")) {
						// ARGH - Can't parse this all back out to separate fields..
					}
					else if (section2.getTitle().equals("Other Relevant Issues or Preferences about Provision of Care")) {
						fields.setOtherRelevantInformation(section2.getText());
					}
				}
			}
			else if (section.getTitle().equals("End of Life Care Observations")) {
				for (Section2 section2 : section.getSection2()) {
					if (section2.getTitle().equals("Primary EoLC Diagnosis")) {
						fields.setPrimaryEOLCDiagnosis(section2.getText());
					}
					else if (section2.getTitle().equals("Other EoLC Diagnosis")) {
						fields.setOtherRelevantDiagnoses(section2.getText());
					}
					else if (section2.getTitle().equals("Disabilities")) {
						fields.setPatientDisability(section2.getText());
					}
					else if (section2.getTitle().equals("Allergies and Adverse Reaction Summary")) {
						fields.setAllergiesAndAdverseReactions(section2.getText());
					}
				}
			}
			else if (section.getTitle().equals("EPaCCS Record Location")) {
				fields.setEPaCCSURL(section.getText());
			}
			else if (section.getTitle().equals("Advance Statements")) {
				fields.setAdvanceStatements(section.getText());
			}
		}
		
		if (parseExceptions.hasEntries()) {
			logger.error("Errors: {}", parseExceptions.toString());
		}
		return fields;
		
	}

	private static void getEoLCPFields(EndOfLifeCareISBFields fields, EoLCarePlan eolcp,
			ParsePayloadContentException parseExceptions) {
		
		fields.setEpaccsRecordCreationDate((DateValue)eolcp.getEffectiveFrom());
		fields.setEpaccsRecordAuthoredDate((DateValue)eolcp.getOriginalTimeAuthored());
		fields.setEpaccsRecordUpdatedDate((DateValue)eolcp.getLastAmendedTimeAuthored());

		// EPaCCS Record Original Author
		if (eolcp.getOriginalAuthor() != null) {
			String className = ((Payload)(eolcp.getOriginalAuthor())).getClassName();
			if (className.equals("AuthorPersonUniversal")) {
				getEoLCPAuthorFields(fields, (AuthorPersonUniversal)eolcp.getOriginalAuthor(), parseExceptions);
			}
		}

		// EPaCCS Record Last Amending Author
		if (eolcp.getLastAmendedAuthor() != null) {
			String className = ((Payload)(eolcp.getLastAmendedAuthor())).getClassName();
			if (className.equals("AuthorPersonUniversal")) {
				getEoLCPUpdateAuthorFields(fields, (AuthorPersonUniversal)eolcp.getLastAmendedAuthor(), parseExceptions);
			}
		}

		// EOLC Tool
		EoLToolSnCT tool = (EoLToolSnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				eolcp.getEoLToolSnCT(), "EoLToolSnCT", "EolcTool", parseExceptions, true);
		fields.setEolcTool(tool);
		
		// EOLC Pathway Stage
		fields.setEolcPathwayStageNONISB(eolcp.getEOLToolStage());
		
		// Record Review Date
		fields.setEpaccsRecordReviewDate((DateValue)eolcp.getPlannedReviewDate());
		
		// Related persons
		getPatientRelationshipFields(fields, eolcp.getPatientRelationships(), parseExceptions);
	}
	
	/**
 	 * The main related persons listed in the ISB standard (with a few additional attributes to satisfy CDA spec) are:
	 * <br/>Main informal carer (name, tel)
	 * <br/>Usual GP (name, practice id, practice name, address, tel, fax)
	 * <br/>Key Worker (id, name, role, org id, org name, address, tel)
	 * <br/>Senior Responsible Clinician (name, id, role, org name, org id, add, tel)
	 * <br/>Formal Carers 0..N (name, role, tel)
	 * <br/>Lasting power of attorney (name, tel)
	 * <br/>First additional person to involve in decisions (name, tel)
	 * <br/>Second additional person to involve in decisions (name, tel)
	 * 
	 * @param fields
	 * @param relationships
	 * @param parseExceptions
	 */
	private static void getPatientRelationshipFields(EndOfLifeCareISBFields fields, List<EOLPatientRelationships> relationships,
			ParsePayloadContentException parseExceptions) {
		
		List<EndOfLifeCareDocumentFormalCarer> formalCarers = new ArrayList<EndOfLifeCareDocumentFormalCarer>(); 
		
		for (EOLPatientRelationships relationshipList : relationships) {
			for (RelatedPersonDetails personDetails : relationshipList.getRelationships().getRelatedPerson()) {
				
				PatientRelationshipParticipantRole person = personDetails.getParticipant();
				
				if (person.getPersonRole() != null) {
					JobRoleName role = (JobRoleName)HelperUtils.safelyMapCodedValueToVocabEntry(
						person.getPersonRole(), "JobRoleName", "RelatedPersonJobRole", parseExceptions, true);
					
					JobRoleName associatedRole = null;
					if (person.getAssociatedJobRole() != null) {
						associatedRole = (JobRoleName)HelperUtils.safelyMapCodedValueToVocabEntry(
							person.getAssociatedJobRole(), "JobRoleName", "RelatedPersonAssociatedJobRole", parseExceptions, true);
					}
				
					// Main informal carer (name, tel)
					if (role.sameAs(JobRoleName._MainInformalCarer)) {
						fields.setMainInformalCarerName(person.getPersonName());
						fields.setMainInformalCarerTel(TemplateParsingHelper.getTel(person, "MainInformalCarerTel"));
					}
					
					// Usual GP (name, practice id, practice name, address, tel, fax)
					else if (role.sameAs(JobRoleName._GeneralMedicalPractitioner)) {
						fields.setUsualGPName(person.getPersonName());
						fields.setUsualGPTelephone(TemplateParsingHelper.getTel(person, "UsualGPTelephone"));
						fields.setUsualGPFax(TemplateParsingHelper.getFax(person, "UsualGPFax"));
						fields.setUsualGPAddress(TemplateParsingHelper.getAddress(person));
						fields.setUsualGPODSCode(TemplateParsingHelper.getODSID(person, "UsualGPODSCode", parseExceptions));
						fields.setUsualGPOrgName(TemplateParsingHelper.getOrgName(person));
					}
					
					// Key Worker (id, name, role, org id, org name, address, tel)
					else if (role.sameAs(JobRoleName._KeyWorker)) {
						fields.setKeyWorkerName(person.getPersonName());
						fields.setKeyWorkerSDSID(TemplateParsingHelper.getSDSID(person, "KeyWorkerID", parseExceptions));
						fields.setKeyWorkerTelephone(TemplateParsingHelper.getTel(person, "KeyWorkerTelephone"));
						fields.setKeyWorkerAddress(TemplateParsingHelper.getAddress(person));
						fields.setKeyWorkerOrgID(TemplateParsingHelper.getODSID(person, "KeyWorkerOrgID", parseExceptions));
						fields.setKeyWorkerOrgName(TemplateParsingHelper.getOrgName(person));
						fields.setKeyWorkerJobRole(associatedRole);
					}
					
					// Formal Carers 0..N (name, role, tel)
					else if (role.sameAs(JobRoleName._FormalCarer)) {
						EndOfLifeCareDocumentFormalCarer carerentry = new EndOfLifeCareDocumentFormalCarer();
						carerentry.setName(person.getPersonName());
						carerentry.setTelephone(TemplateParsingHelper.getTel(person, "FormalCarerTelephone"));
						carerentry.setRole(associatedRole);
						formalCarers.add(carerentry);
					}

					// Lasting power of attorney (name, tel)
					// We only need to populate this if it hasn't already been populated from
					// the actual LPA section in the document (which it should have been!)
					else if (role.sameAs(JobRoleName._LPAwithauthority)) {
						if (fields.getLPAName() == null) {
							fields.setLPAName(person.getPersonName());
						}
						if (fields.getLPATelephone() == null) {
							fields.setLPATelephone(TemplateParsingHelper.getTel(person, "LPATelephone"));
						}
					}
					
					// Additional person to involve in decisions (name, tel)
					else if (role.sameAs(JobRoleName._Additionalpersontobeinvolvedindecisions)) {
						if (fields.getAdditionalPersonToInvolve() != null && fields.getAdditionalPersonToInvolve2() != null) {
							// We already appear to have two additional people, so we will have to assum this
							// is a third one, and ignore it
						} else if (fields.getAdditionalPersonToInvolve() == null) {
							// First additional person to involve in decisions (name, tel)
							fields.setAdditionalPersonToInvolve(person.getPersonName());
							fields.setAdditionalPersonToInvolveTel(TemplateParsingHelper.getTel(person, "AdditionalPersonToInvolveTel"));
						} else {
							// Second additional person to involve in decisions (name, tel)
							fields.setAdditionalPersonToInvolve2(person.getPersonName());
							fields.setAdditionalPersonToInvolve2Tel(TemplateParsingHelper.getTel(person, "AdditionalPersonToInvolve2Tel"));
						}
					}
					
					// Senior Responsible Clinician (name, id, role, org name, org id, add, tel)
					// We only need to populate this if it hasn't already been populated from as the author 
					// of the DNACPR section in the document (which it may have been if there is a DNACPR decision recorded)
					else if (role.sameAs(JobRoleName._SeniorResponsibleClinician)) {
						fields.setSeniorResponsibleClinicianName(person.getPersonName());
						fields.setSeniorResponsibleClinicianSDSID(TemplateParsingHelper.getSDSID(person, "SeniorResponsibleClinicianSDSID", parseExceptions));
						fields.setSeniorResponsibleClinicianTelephone(TemplateParsingHelper.getTel(person, "SeniorResponsibleClinicianTelephone"));
						fields.setSeniorResponsibleClinicianAddress(TemplateParsingHelper.getAddress(person));
						fields.setSeniorResponsibleClinicianORGID(TemplateParsingHelper.getODSID(person, "SeniorResponsibleClinicianORGID", parseExceptions));
						fields.setSeniorResponsibleClinicianORGName(TemplateParsingHelper.getOrgName(person));
						fields.setSeniorResponsibleClinicianJobRole(associatedRole);
					}
				}
			}
			
			fields.setFormalCarers(formalCarers);
		}
		
	}
	
	private static void getDocumentFields(EndOfLifeCareISBFields fields, ClinicalDocument document,
			ParsePayloadContentException parseExceptions) {

		fields.setDocumentId(document.getDocumentId());
		fields.setDocumentSetId(document.getDocumentSetId());
		fields.setDocumentVersionNumber(Integer.parseInt(document.getDocumentVersionNumber()));
		fields.setDocumentCreationDate((DateValue)document.getEffectiveTime());

		// Original Author
		if (document.getAuthor() != null) {
			String className = ((Payload)(document.getAuthor())).getClassName();
			if (className.equals("AuthorPersonUniversal")) {
				getDocumentAuthorFields(fields, (AuthorPersonUniversal)document.getAuthor(), parseExceptions);
				// Authored time
				fields.setDocumentAuthoredTime((DateValue)document.getTimeAuthored());
			}
		}
	}

	private static void getDocumentAuthorFields(EndOfLifeCareISBFields fields, AuthorPersonUniversal documentAuthor,
			ParsePayloadContentException parseExceptions) {
		
		fields.setDocumentAuthorAddress(TemplateParsingHelper.getAddress(documentAuthor));
		fields.setDocumentAuthorRole(TemplateParsingHelper.getJobRole(documentAuthor, "DocumentAuthorRole", parseExceptions));
		fields.setDocumentAuthorSDSID(TemplateParsingHelper.getSDSID(documentAuthor, "DocumentAuthorSDSID", parseExceptions));
		fields.setDocumentAuthorTelephone(TemplateParsingHelper.getTel(documentAuthor, "TelephoneNumber"));
		fields.setDocumentAuthorName(TemplateParsingHelper.getName(documentAuthor));
		fields.setDocumentAuthorOrganisationODSID(TemplateParsingHelper.getODSID(documentAuthor, "DocumentAuthorOrganisationODSID", parseExceptions));
		fields.setDocumentAuthorOrganisationName(TemplateParsingHelper.getOrgName(documentAuthor));
	}

	private static void getCustodianFields(EndOfLifeCareISBFields fields, CustodianOrganizationUniversal custodian,
			ParsePayloadContentException parseExceptions) {
		
		// Custodian ORG ID
		if (custodian.getId() != null && custodian.getId().getType().equals(OrgIDType.ODSOrgID.code)) {
			fields.setEPACCSOrganisationODSID(custodian.getId().getID());
		} else {
			parseExceptions.addParseError("EPACCSOrganisationODSID", "Org ID provided was not an ODS ID");
		}
		
		// Custodian Org Name
		fields.setEPACCSOrganisation(custodian.getName());
	}
	
	private static void getEoLCPAuthorFields(EndOfLifeCareISBFields fields, AuthorPersonUniversal originalAuthor,
											ParsePayloadContentException parseExceptions) {
		
		fields.setEpaccsRecordAuthorAddress(TemplateParsingHelper.getAddress(originalAuthor));
		fields.setEpaccsRecordAuthorRole(TemplateParsingHelper.getJobRole(originalAuthor, "EpaccsRecordAuthorRole", parseExceptions));
		fields.setEpaccsRecordAuthorSDSID(TemplateParsingHelper.getSDSID(originalAuthor, "EpaccsRecordAuthorSDSID", parseExceptions));
		fields.setEpaccsRecordAuthorTelephone(TemplateParsingHelper.getTel(originalAuthor, "TelephoneNumber"));
		fields.setEpaccsRecordAuthorName(TemplateParsingHelper.getName(originalAuthor));
		fields.setEpaccsRecordAuthorOrganisationODSID(TemplateParsingHelper.getODSID(originalAuthor, "EpaccsRecordAuthorOrganisationODSID", parseExceptions));
		fields.setEpaccsRecordAuthorOrganisationName(TemplateParsingHelper.getOrgName(originalAuthor));
	}
	
	private static void getEoLCPUpdateAuthorFields(EndOfLifeCareISBFields fields, AuthorPersonUniversal updateAuthor,
			ParsePayloadContentException parseExceptions) {

		fields.setEpaccsRecordUpdateAuthorAddress(TemplateParsingHelper.getAddress(updateAuthor));
		fields.setEpaccsRecordUpdateAuthorRole(TemplateParsingHelper.getJobRole(updateAuthor, "EpaccsRecordUpdateAuthorRole", parseExceptions));
		fields.setEpaccsRecordUpdateAuthorSDSID(TemplateParsingHelper.getSDSID(updateAuthor, "EpaccsRecordUpdateAuthorSDSID", parseExceptions));
		fields.setEpaccsRecordUpdateAuthorTelephone(TemplateParsingHelper.getTel(updateAuthor, "TelephoneNumber"));
		fields.setEpaccsRecordUpdateAuthorName(TemplateParsingHelper.getName(updateAuthor));
		fields.setEpaccsRecordUpdateAuthorOrganisationODSID(TemplateParsingHelper.getODSID(updateAuthor, "EpaccsRecordUpdateAuthorOrganisationODSID", parseExceptions));
		fields.setEpaccsRecordUpdateAuthorOrganisationName(TemplateParsingHelper.getOrgName(updateAuthor));
	}
	
	private static void getADRTFields(EndOfLifeCareISBFields fields, AdvanceDecisionToRefuseTreatment adrt,
			ParsePayloadContentException parseExceptions) {
		
		// ADRT Recorded Date
		fields.setADRTRecordedDate((DateValue)adrt.getEffectiveTime());
		
		// ADRT Preference
		EoLADRTprefSnCT adrtPref = (EoLADRTprefSnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				adrt.getADRTPreference(), "EoLADRTprefSnCT",
				"ADRT", parseExceptions, false);
		fields.setADRT(adrtPref);

		// ADRT Discussed
		ADRTDiscussedSnCT discussed = (ADRTDiscussedSnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				adrt.getADRTDiscussed(), "ADRTDiscussedSnCT",
				"ADRTDiscussedWithClinicianNONISB", parseExceptions, false);
		fields.setADRTDiscussedWithClinicianNONISB(discussed.sameAs(ADRTDiscussedSnCT._820621000000107));
		
		// ADRT Document Location
		fields.setADRTDocumentLocation(adrt.getADRTDocumentLocation());
	}
	
	private static void getAMBoxFields(EndOfLifeCareISBFields fields, AnticipatoryMedicineBoxIssueProcedure ambox,
			ParsePayloadContentException parseExceptions) {

		// Box Issued
		EoLAnticipatoryMedicineBoxIssueSnCT issued = (EoLAnticipatoryMedicineBoxIssueSnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				ambox.getAnticipatoryMedicineBoxIssueCode(), "EoLAnticipatoryMedicineBoxIssueSnCT",
				"AnticipatoryMedicinesIssued", parseExceptions, false);
		fields.setAnticipatoryMedicinesIssued(issued.sameAs(EoLAnticipatoryMedicineBoxIssueSnCT._376201000000102));
		
		// Box location
		fields.setAnticipatoryMedicinesLocation(ambox.getLocationOfBox());
		
		// Time box issued
		fields.setAnticipatoryMedicinesDateIssued((DateValue)ambox.getTimeIssued());
	}
	
	private static void getDNACPRFields(EndOfLifeCareISBFields fields, DNACPRDecisionbySeniorResponsibleClinician dnacpr,
			ParsePayloadContentException parseExceptions) {
		
		// DNACPR Decision date
		fields.setDNACPRDate((DateValue)dnacpr.getEffectiveTime());
		
		// DNACPR Preference
		DNACPRprefSnCT decision = (DNACPRprefSnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				dnacpr.getDNACPRPreference(), "DNACPRprefSnCT",
				"DNACPR", parseExceptions, false);
		fields.setDNACPR(decision);
		
		// DNACPR Decision Originally Recorded Date
		fields.setDNACPRCreatedDate((DateValue)dnacpr.getSeniorResponsibleClinicianTimeAuthored());
		
		// Document Location
		fields.setDNACPRLocation(dnacpr.getDNACPRDocsLocation());
		
		// Review date
		fields.setDNACPRReviewDate((DateValue)dnacpr.getReviewDate());
		
		// Author: Senior Responsible Clinician
		AuthorPersonUniversal src = (AuthorPersonUniversal)dnacpr.getSeniorResponsibleClinicianAuthor();
 		// Name
 		fields.setSeniorResponsibleClinicianName(TemplateParsingHelper.getName(src));
 		// Job Role
 		fields.setSeniorResponsibleClinicianJobRole(TemplateParsingHelper.getJobRole(src, "SeniorResponsibleClinicianJobRole", parseExceptions));
 		// ID
 		fields.setSeniorResponsibleClinicianSDSID(TemplateParsingHelper.getSDSID(src, "SeniorResponsibleClinicianSDSID", parseExceptions));
 		// Tel
 		fields.setSeniorResponsibleClinicianTelephone(TemplateParsingHelper.getTel(src, "TelephoneNumber"));
 		// Address
 		fields.setSeniorResponsibleClinicianAddress(TemplateParsingHelper.getAddress(src));
 		// Org ID
		fields.setSeniorResponsibleClinicianORGID(TemplateParsingHelper.getODSID(src, "SeniorResponsibleClinicianORGID", parseExceptions));
		// Org Name
		fields.setSeniorResponsibleClinicianORGName(TemplateParsingHelper.getOrgName(src));
	}
	
	private static void getPrognosisAwarenessFields(EndOfLifeCareISBFields fields, PrognosisAwareness aware,
			ParsePayloadContentException parseExceptions) {
		
		// Date when made aware of prognosis
		fields.setPrognosisAwarenessRecordedDate((DateValue)aware.getEffectiveTime());
		
		// Awareness of prognosis
		PrognosisAwarenessSnCT awareOfPrognosis = (PrognosisAwarenessSnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				aware.getPrognosisAwareness(), "PrognosisAwarenessSnCT",
				"MainInformalCarerAwareOfPrognosis", parseExceptions, false);
		fields.setMainInformalCarerAwareOfPrognosis(awareOfPrognosis);
				
		// Main informal carer (name, tel)
		PatientRelationshipParticipantRole mic = aware.getMainInformalCarer();
 		//mic.setPersonRole(JobRoleName._NR1980);
 		//mic.setPersonName(isbFields.getMainInformalCarerName());
 		
 		//if (isbFields.getMainInformalCarerTel() != null) {
 		//}
		
		fields.setMainInformalCarerTel(TemplateParsingHelper.getTel(mic, "MainInformalCarerTel"));
		
	}
	
	private static void getLPAFields(EndOfLifeCareISBFields fields, AuthoritytoLastingPowerofAttorney lpa,
			ParsePayloadContentException parseExceptions) {
		
		// Date granted
		fields.setLPADate((DateValue)lpa.getEffectiveTime());
		
		// LPA Authority
		AuthoritytoLPASnCT authority = (AuthoritytoLPASnCT)HelperUtils.safelyMapCodedValueToVocabEntry(
				lpa.getAuthoritytoLPA(), "AuthoritytoLPASnCT",
				"MainInformalCarerAwareOfPrognosis", parseExceptions, false);
		fields.setLPAAuthority(authority);
		fields.setLPAName(lpa.getLPADetails().getPersonName());
		fields.setLPATelephone(TemplateParsingHelper.getTel(lpa.getLPADetails(), "LPATelephone"));
	}

		/*

		// Disability
		fields.setPatientDisability(patient.get);
		fields.setUsualGPName;
		private PersonName mainInformalCarerName;
		private String mainInformalCarerTel;
		private PrognosisAwarenessSnCT mainInformalCarerAwareOfPrognosis;
		private DateValue prognosisAwarenessRecordedDate;
		private PersonName keyWorkerName;
		private String keyWorkerTelephone;
		private Address keyWorkerAddress;
		private JobRoleName keyWorkerJobRole;
		private String keyWorkerSDSID;
		private String keyWorkerOrgID;
		private String keyWorkerOrgName;
		private List<EndOfLifeCareDocumentFormalCarer> formalCarers;
		private String primaryEOLCDiagnosis;
		private String otherRelevantDiagnoses;
		private String allergiesAndAdverseReactions;
		private String advanceStatements;
		private String preferredPlaceOfDeath;
		private String preferredPlaceOfDeathOrganisation;
		private Address preferredPlaceOfDeathAddress;
		private String preferredPlaceOfDeathIsUPR;
		private String preferredPlaceOfDeath2;
		private String preferredPlaceOfDeath2Organisation;
		private Address preferredPlaceOfDeath2Address;
		private String preferredPlaceOfDeath2IsUPR;
		private PersonName LPAName;
		private AuthoritytoLPASnCT LPAAuthority;
		private DateValue LPADate;
		private String LPATelephone;
		private PersonName additionalPersonToInvolve;
		private String additionalPersonToInvolveTel;
		private PersonName additionalPersonToInvolve2;
		private String additionalPersonToInvolve2Tel;
		private String otherRelevantInformation;
		private PersonName SeniorResponsibleClinicianName;
		private String SeniorResponsibleClinicianSDSID;
		private String SeniorResponsibleClinicianORGID;
		private String SeniorResponsibleClinicianORGName;
		private JobRoleName SeniorResponsibleClinicianJobRole;
		private String SeniorResponsibleClinicianTelephone;
		private Address SeniorResponsibleClinicianAddress;
		private String EPaCCSURL;
		
		
		// ==== Now create the text sections ====
		template.setMainDocumentSectionID(CDAUUID.generateUUIDString());
		
		template.addTextSections(new TextSections(createTextSection1_GeneralDocumentInformation(isbFields)));
		template.addTextSections(new TextSections(createTextSection2_PatientRelationships(isbFields)));

		TextSection ts3 = createTextSection_EOLToolUsed(isbFields);
		if (ts3 != null) template.addTextSections(new TextSections(ts3));
		
		TextSection ts4 = createTextSection4_PatientChoices(isbFields);
		if (ts4 != null) template.addTextSections(new TextSections(ts4));
		
		TextSection ts5 = createTextSection5_AdvanceStatements(isbFields);
		if (ts5 != null) template.addTextSections(new TextSections(ts5));
		
		TextSection ts6 = createTextSection6_DNACPR(isbFields);
		if (ts6 != null) template.addTextSections(new TextSections(ts6));
		
		TextSection ts7 = createTextSection7_Observations(isbFields);
		if (ts7 != null) template.addTextSections(new TextSections(ts7));
		
		TextSection ts8 = createTextSection8_AnticipatoryMedication(isbFields);
		if (ts8 != null) template.addTextSections(new TextSections(ts8));
		
		TextSection ts9 = createTextSection9_EPaCCSLink(isbFields);
		if (ts9 != null) template.addTextSections(new TextSections(ts9));
		
		return template; 
	}
	
	
	
	*/
}
