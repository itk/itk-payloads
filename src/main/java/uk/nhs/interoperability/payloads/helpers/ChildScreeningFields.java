package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.*;
import uk.nhs.interoperability.payloads.vocabularies.internal.NHSNumberTraceStatus;

public class ChildScreeningFields {
	private DateValue documentCreationDate;
	private String documentSetId;
	private int documentVersionNumber = 1;
	
	// Author
	private Address documentAuthorAddress;
	private JobRoleName documentAuthorRole;
	private String documentAuthorSDSID;
	private String documentAuthorTelephone;
	private PersonName documentAuthorName;
	private String documentAuthorOrganisationODSID;
	private String documentAuthorOrganisationName;
	private DateValue documentAuthoredTime;
	
	// Custodian
	private String custodianOrganisationODSID;
	private String custodianOrganisation;
	
	// Patient
	private PersonName patientName;
	private DateValue patientBirthDate;
	private String patientNHSNo;
	private NHSNumberTraceStatus patientNHSNoTraceStatus;
	private Sex patientGender;
	private Address patientAddress;
	
	// Guardian
	private PersonName guardianName;
	private String guardianNHSNo;
	private NHSNumberTraceStatus guardianNHSNoTraceStatus;
	private Address guardianAddress;
	private String guardianTelephone;
	private GuardianRoleType guardianRole;
	
	// Provider Organisation
	private String providerOrganisationODSID;
	private String providerOrganisation;
	private Address providerOrganisationAddress;
	private String providerOrganisationTelephone;
	private CDAOrganizationProviderType providerOrganisationType;
	private String providerParentOrganisationODSID;
	
	// Recipients
	private ArrayList<DocumentRecipient> recipients;
	private ArrayList<DocumentRecipient> copyRecipients;
	
	// Consent
	private DocumentConsentSnCT consent;
	
	// Coded Sections
	
	// Blood Spot Screening
	private DateValue dateOfBloodSpotScreening;
	private DateValue bloodSpotSampleCollectedTime;
	private String bloodSpotPerformerPersonSDSID;
	private PersonName bloodSpotPerformerPersonName;
	private String bloodSpotPerformerOrganisationODSID;
	private String bloodSpotPerformerOrganisationName;
	
	private DateValue bloodSpotTimeReceivedAtLab;
	private String bloodSpotLabOrganisationODSID;
	private String bloodSpotLabOrganisationName;
	
	private PKUScreeningResult PKUScreeningValue;
	private boolean laboratoryConfirmPKUasCorrect = false;
	private BSScreeningStatusSubCodes PKUScreeningSubStatus;
	private String PKUReasonText;
	private String PKUSupplementaryText;
	
	private SCDScreeningResult SCDScreeningValue;
	private boolean laboratoryConfirmSCDasCorrect = false;
	private BSScreeningStatusSubCodes SCDScreeningSubStatus;
	private String SCDReasonText;
	private String SCDSupplementaryText;
	
	private CFScreeningResult CFScreeningValue;
	private boolean laboratoryConfirmCFasCorrect = false;
	private BSScreeningStatusSubCodes CFScreeningSubStatus;
	private String CFReasonText;
	private String CFSupplementaryText;
	
	private CHTScreeningResult CHTScreeningValue;
	private boolean laboratoryConfirmCHTasCorrect = false;
	private BSScreeningStatusSubCodes CHTScreeningSubStatus;
	private String CHTReasonText;
	private String CHTSupplementaryText;
	
	private MCADDScreeningResult MCADDScreeningValue;
	private boolean laboratoryConfirmMCADDasCorrect = false;
	private BSScreeningStatusSubCodes MCADDScreeningSubStatus;
	private String MCADDReasonText;
	private String MCADDSupplementaryText;
	
	private BSScreeningLocStatus ScreeningLocationStatus;
	private String LaboratoryCardSerialNumber;
	private String PreviousLaboratoryCardSerialNumber;
	
	// NewBornBirthDetails
	private DateValue dateBirthDetailsRecorded;
	private String GestationalAgeInWeeks;
	private Integer BirthOrder;
	private Integer NoOfFoetusInConfinement;
	private String BirthWeightInGrams;
	
	// NewBornHearingScreening
	private NewBornHearingScreeningOutcomeSnCT HearingScreeningOutcome;
	private AudiologyTestingOutcomeStatus AudiologyTestFinding;
	private DateValue AudiologyTestFindingEffectiveTime;
	private DateValue AudiologyReferralTime;
	
	// NewBornPhysicalExamination
	private DateValue DateOfPhysicalExamination;
	private Integer GestationalAgeInDays;
	private HipsExaminationResult HipsExamination;
	private HipsUltraSoundOutcomeDecision UltraSoundDecision;
	private DateValue DateOfHipsUltrasound;
	private HipsExpertManagementPlanType ExpertManagementPlan;
	private DateValue DateHipsExpertManagementPlanCreated;
	private HeartExaminationResult HeartExamination;
	private EyesExaminationResult EyesExamination;
	private TestesExaminationResult TestesExamination;
	
	public DateValue getDocumentCreationDate() {
		return documentCreationDate;
	}
	public void setDocumentCreationDate(DateValue documentCreationDate) {
		this.documentCreationDate = documentCreationDate;
	}
	public String getDocumentSetId() {
		return documentSetId;
	}
	public void setDocumentSetId(String documentSetId) {
		this.documentSetId = documentSetId;
	}
	public int getDocumentVersionNumber() {
		return documentVersionNumber;
	}
	public void setDocumentVersionNumber(int documentVersionNumber) {
		this.documentVersionNumber = documentVersionNumber;
	}
	public Address getDocumentAuthorAddress() {
		return documentAuthorAddress;
	}
	public void setDocumentAuthorAddress(Address documentAuthorAddress) {
		this.documentAuthorAddress = documentAuthorAddress;
	}
	public JobRoleName getDocumentAuthorRole() {
		return documentAuthorRole;
	}
	public void setDocumentAuthorRole(JobRoleName documentAuthorRole) {
		this.documentAuthorRole = documentAuthorRole;
	}
	public String getDocumentAuthorSDSID() {
		return documentAuthorSDSID;
	}
	public void setDocumentAuthorSDSID(String documentAuthorSDSID) {
		this.documentAuthorSDSID = documentAuthorSDSID;
	}
	public String getDocumentAuthorTelephone() {
		return documentAuthorTelephone;
	}
	public void setDocumentAuthorTelephone(String documentAuthorTelephone) {
		this.documentAuthorTelephone = documentAuthorTelephone;
	}
	public PersonName getDocumentAuthorName() {
		return documentAuthorName;
	}
	public void setDocumentAuthorName(PersonName documentAuthorName) {
		this.documentAuthorName = documentAuthorName;
	}
	public String getDocumentAuthorOrganisationODSID() {
		return documentAuthorOrganisationODSID;
	}
	public void setDocumentAuthorOrganisationODSID(
			String documentAuthorOrganisationODSID) {
		this.documentAuthorOrganisationODSID = documentAuthorOrganisationODSID;
	}
	public String getDocumentAuthorOrganisationName() {
		return documentAuthorOrganisationName;
	}
	public void setDocumentAuthorOrganisationName(
			String documentAuthorOrganisationName) {
		this.documentAuthorOrganisationName = documentAuthorOrganisationName;
	}
	public DateValue getDocumentAuthoredTime() {
		return documentAuthoredTime;
	}
	public void setDocumentAuthoredTime(DateValue documentAuthoredTime) {
		this.documentAuthoredTime = documentAuthoredTime;
	}
	public String getCustodianOrganisationODSID() {
		return custodianOrganisationODSID;
	}
	public void setCustodianOrganisationODSID(String custodianOrganisationODSID) {
		this.custodianOrganisationODSID = custodianOrganisationODSID;
	}
	public String getCustodianOrganisation() {
		return custodianOrganisation;
	}
	public void setCustodianOrganisation(String custodianOrganisation) {
		this.custodianOrganisation = custodianOrganisation;
	}
	public PersonName getPatientName() {
		return patientName;
	}
	public void setPatientName(PersonName patientName) {
		this.patientName = patientName;
	}
	public DateValue getPatientBirthDate() {
		return patientBirthDate;
	}
	public void setPatientBirthDate(DateValue patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}
	public String getPatientNHSNo() {
		return patientNHSNo;
	}
	public void setPatientNHSNo(String patientNHSNo) {
		this.patientNHSNo = patientNHSNo;
	}
	public Sex getPatientGender() {
		return patientGender;
	}
	public void setPatientGender(Sex patientGender) {
		this.patientGender = patientGender;
	}
	public Address getPatientAddress() {
		return patientAddress;
	}
	public void setPatientAddress(Address patientAddress) {
		this.patientAddress = patientAddress;
	}
	public PersonName getGuardianName() {
		return guardianName;
	}
	public void setGuardianName(PersonName guardianName) {
		this.guardianName = guardianName;
	}
	public String getGuardianNHSNo() {
		return guardianNHSNo;
	}
	public void setGuardianNHSNo(String guardianNHSNo) {
		this.guardianNHSNo = guardianNHSNo;
	}
	public Address getGuardianAddress() {
		return guardianAddress;
	}
	public void setGuardianAddress(Address guardianAddress) {
		this.guardianAddress = guardianAddress;
	}
	public String getGuardianTelephone() {
		return guardianTelephone;
	}
	public void setGuardianTelephone(String guardianTelephone) {
		this.guardianTelephone = guardianTelephone;
	}
	public String getProviderOrganisationODSID() {
		return providerOrganisationODSID;
	}
	public void setProviderOrganisationODSID(String providerOrganisationODSID) {
		this.providerOrganisationODSID = providerOrganisationODSID;
	}
	public String getProviderOrganisation() {
		return providerOrganisation;
	}
	public void setProviderOrganisation(String providerOrganisation) {
		this.providerOrganisation = providerOrganisation;
	}
	public Address getProviderOrganisationAddress() {
		return providerOrganisationAddress;
	}
	public void setProviderOrganisationAddress(Address providerOrganisationAddress) {
		this.providerOrganisationAddress = providerOrganisationAddress;
	}
	public String getProviderOrganisationTelephone() {
		return providerOrganisationTelephone;
	}
	public void setProviderOrganisationTelephone(
			String providerOrganisationTelephone) {
		this.providerOrganisationTelephone = providerOrganisationTelephone;
	}
	public CDAOrganizationProviderType getProviderOrganisationType() {
		return providerOrganisationType;
	}
	public void setProviderOrganisationType(
			CDAOrganizationProviderType providerOrganisationType) {
		this.providerOrganisationType = providerOrganisationType;
	}
	public String getProviderParentOrganisationODSID() {
		return providerParentOrganisationODSID;
	}
	public void setProviderParentOrganisationODSID(
			String providerParentOrganisationODSID) {
		this.providerParentOrganisationODSID = providerParentOrganisationODSID;
	}
	public ArrayList<DocumentRecipient> getRecipients() {
		return recipients;
	}
	public void setRecipients(ArrayList<DocumentRecipient> recipients) {
		this.recipients = recipients;
	}
	public void addRecipient(DocumentRecipient recipient) {
		if (this.recipients == null) {
			this.recipients = new ArrayList<DocumentRecipient>();
		}
		this.recipients.add(recipient);
	}
	public ArrayList<DocumentRecipient> getCopyRecipients() {
		return copyRecipients;
	}
	public void setCopyRecipients(ArrayList<DocumentRecipient> copyRecipients) {
		this.copyRecipients = copyRecipients;
	}
	public void addCopyRecipient(DocumentRecipient recipient) {
		if (this.copyRecipients == null) {
			this.copyRecipients = new ArrayList<DocumentRecipient>();
		}
		this.copyRecipients.add(recipient);
	}
	public DocumentConsentSnCT getConsent() {
		return consent;
	}
	public void setConsent(DocumentConsentSnCT consent) {
		this.consent = consent;
	}
	public DateValue getDateOfBloodSpotScreening() {
		return dateOfBloodSpotScreening;
	}
	public void setDateOfBloodSpotScreening(DateValue dateOfBloodSpotScreening) {
		this.dateOfBloodSpotScreening = dateOfBloodSpotScreening;
	}
	public PKUScreeningResult getPKUScreeningValue() {
		return PKUScreeningValue;
	}
	public void setPKUScreeningValue(PKUScreeningResult pKUScreeningValue) {
		PKUScreeningValue = pKUScreeningValue;
	}
	public boolean getLaboratoryConfirmPKUasCorrect() {
		return laboratoryConfirmPKUasCorrect;
	}
	public void setLaboratoryConfirmPKUasCorrect(
			boolean laboratoryConfirmPKUasCorrect) {
		this.laboratoryConfirmPKUasCorrect = laboratoryConfirmPKUasCorrect;
	}
	public BSScreeningStatusSubCodes getPKUScreeningSubStatus() {
		return PKUScreeningSubStatus;
	}
	public void setPKUScreeningSubStatus(
			BSScreeningStatusSubCodes pKUScreeningSubStatus) {
		PKUScreeningSubStatus = pKUScreeningSubStatus;
	}
	public String getPKUReasonText() {
		return PKUReasonText;
	}
	public void setPKUReasonText(String pKUReasonText) {
		PKUReasonText = pKUReasonText;
	}
	public String getPKUSupplementaryText() {
		return PKUSupplementaryText;
	}
	public void setPKUSupplementaryText(String pKUSupplementaryText) {
		PKUSupplementaryText = pKUSupplementaryText;
	}
	public SCDScreeningResult getSCDScreeningValue() {
		return SCDScreeningValue;
	}
	public void setSCDScreeningValue(SCDScreeningResult sCDScreeningValue) {
		SCDScreeningValue = sCDScreeningValue;
	}
	public boolean getLaboratoryConfirmSCDasCorrect() {
		return laboratoryConfirmSCDasCorrect;
	}
	public void setLaboratoryConfirmSCDasCorrect(
			boolean laboratoryConfirmSCDasCorrect) {
		this.laboratoryConfirmSCDasCorrect = laboratoryConfirmSCDasCorrect;
	}
	public BSScreeningStatusSubCodes getSCDScreeningSubStatus() {
		return SCDScreeningSubStatus;
	}
	public void setSCDScreeningSubStatus(
			BSScreeningStatusSubCodes sCDScreeningSubStatus) {
		SCDScreeningSubStatus = sCDScreeningSubStatus;
	}
	public String getSCDReasonText() {
		return SCDReasonText;
	}
	public void setSCDReasonText(String sCDReasonText) {
		SCDReasonText = sCDReasonText;
	}
	public String getSCDSupplementaryText() {
		return SCDSupplementaryText;
	}
	public void setSCDSupplementaryText(String sCDSupplementaryText) {
		SCDSupplementaryText = sCDSupplementaryText;
	}
	public CFScreeningResult getCFScreeningValue() {
		return CFScreeningValue;
	}
	public void setCFScreeningValue(CFScreeningResult cFScreeningValue) {
		CFScreeningValue = cFScreeningValue;
	}
	public boolean getLaboratoryConfirmCFasCorrect() {
		return laboratoryConfirmCFasCorrect;
	}
	public void setLaboratoryConfirmCFasCorrect(boolean laboratoryConfirmCFasCorrect) {
		this.laboratoryConfirmCFasCorrect = laboratoryConfirmCFasCorrect;
	}
	public BSScreeningStatusSubCodes getCFScreeningSubStatus() {
		return CFScreeningSubStatus;
	}
	public void setCFScreeningSubStatus(
			BSScreeningStatusSubCodes cFScreeningSubStatus) {
		CFScreeningSubStatus = cFScreeningSubStatus;
	}
	public String getCFReasonText() {
		return CFReasonText;
	}
	public void setCFReasonText(String cFReasonText) {
		CFReasonText = cFReasonText;
	}
	public String getCFSupplementaryText() {
		return CFSupplementaryText;
	}
	public void setCFSupplementaryText(String cFSupplementaryText) {
		CFSupplementaryText = cFSupplementaryText;
	}
	public CHTScreeningResult getCHTScreeningValue() {
		return CHTScreeningValue;
	}
	public void setCHTScreeningValue(CHTScreeningResult cHTScreeningValue) {
		CHTScreeningValue = cHTScreeningValue;
	}
	public boolean getLaboratoryConfirmCHTasCorrect() {
		return laboratoryConfirmCHTasCorrect;
	}
	public void setLaboratoryConfirmCHTasCorrect(
			boolean laboratoryConfirmCHTasCorrect) {
		this.laboratoryConfirmCHTasCorrect = laboratoryConfirmCHTasCorrect;
	}
	public BSScreeningStatusSubCodes getCHTScreeningSubStatus() {
		return CHTScreeningSubStatus;
	}
	public void setCHTScreeningSubStatus(
			BSScreeningStatusSubCodes cHTScreeningSubStatus) {
		CHTScreeningSubStatus = cHTScreeningSubStatus;
	}
	public String getCHTReasonText() {
		return CHTReasonText;
	}
	public void setCHTReasonText(String cHTReasonText) {
		CHTReasonText = cHTReasonText;
	}
	public String getCHTSupplementaryText() {
		return CHTSupplementaryText;
	}
	public void setCHTSupplementaryText(String cHTSupplementaryText) {
		CHTSupplementaryText = cHTSupplementaryText;
	}
	public MCADDScreeningResult getMCADDScreeningValue() {
		return MCADDScreeningValue;
	}
	public void setMCADDScreeningValue(MCADDScreeningResult mCADDScreeningValue) {
		MCADDScreeningValue = mCADDScreeningValue;
	}
	public Boolean getLaboratoryConfirmMCADDasCorrect() {
		return laboratoryConfirmMCADDasCorrect;
	}
	public void setLaboratoryConfirmMCADDasCorrect(
			Boolean laboratoryConfirmMCADDasCorrect) {
		this.laboratoryConfirmMCADDasCorrect = laboratoryConfirmMCADDasCorrect;
	}
	public BSScreeningStatusSubCodes getMCADDScreeningSubStatus() {
		return MCADDScreeningSubStatus;
	}
	public void setMCADDScreeningSubStatus(
			BSScreeningStatusSubCodes mCADDScreeningSubStatus) {
		MCADDScreeningSubStatus = mCADDScreeningSubStatus;
	}
	public String getMCADDReasonText() {
		return MCADDReasonText;
	}
	public void setMCADDReasonText(String mCADDReasonText) {
		MCADDReasonText = mCADDReasonText;
	}
	public String getMCADDSupplementaryText() {
		return MCADDSupplementaryText;
	}
	public void setMCADDSupplementaryText(String mCADDSupplementaryText) {
		MCADDSupplementaryText = mCADDSupplementaryText;
	}
	public BSScreeningLocStatus getScreeningLocationStatus() {
		return ScreeningLocationStatus;
	}
	public void setScreeningLocationStatus(
			BSScreeningLocStatus screeningLocationStatus) {
		ScreeningLocationStatus = screeningLocationStatus;
	}
	public String getLaboratoryCardSerialNumber() {
		return LaboratoryCardSerialNumber;
	}
	public void setLaboratoryCardSerialNumber(String laboratoryCardSerialNumber) {
		LaboratoryCardSerialNumber = laboratoryCardSerialNumber;
	}
	public String getPreviousLaboratoryCardSerialNumber() {
		return PreviousLaboratoryCardSerialNumber;
	}
	public void setPreviousLaboratoryCardSerialNumber(
			String previousLaboratoryCardSerialNumber) {
		PreviousLaboratoryCardSerialNumber = previousLaboratoryCardSerialNumber;
	}
	public DateValue getDateBirthDetailsRecorded() {
		return dateBirthDetailsRecorded;
	}
	public void setDateBirthDetailsRecorded(DateValue dateBirthDetailsRecorded) {
		this.dateBirthDetailsRecorded = dateBirthDetailsRecorded;
	}
	public String getGestationalAgeInWeeks() {
		return GestationalAgeInWeeks;
	}
	public void setGestationalAgeInWeeks(String gestationalAgeInWeeks) {
		GestationalAgeInWeeks = gestationalAgeInWeeks;
	}
	public String getBirthWeightInGrams() {
		return BirthWeightInGrams;
	}
	public void setBirthWeightInGrams(String birthWeightInGrams) {
		BirthWeightInGrams = birthWeightInGrams;
	}
	public NewBornHearingScreeningOutcomeSnCT getHearingScreeningOutcome() {
		return HearingScreeningOutcome;
	}
	public void setHearingScreeningOutcome(
			NewBornHearingScreeningOutcomeSnCT hearingScreeningOutcome) {
		HearingScreeningOutcome = hearingScreeningOutcome;
	}
	public AudiologyTestingOutcomeStatus getAudiologyTestFinding() {
		return AudiologyTestFinding;
	}
	public void setAudiologyTestFinding(
			AudiologyTestingOutcomeStatus audiologyTestFinding) {
		AudiologyTestFinding = audiologyTestFinding;
	}
	public DateValue getAudiologyReferralTime() {
		return AudiologyReferralTime;
	}
	public void setAudiologyReferralTime(DateValue audiologyReferralTime) {
		AudiologyReferralTime = audiologyReferralTime;
	}
	public DateValue getDateOfPhysicalExamination() {
		return DateOfPhysicalExamination;
	}
	public void setDateOfPhysicalExamination(DateValue dateOfPhysicalExamination) {
		DateOfPhysicalExamination = dateOfPhysicalExamination;
	}
	public Integer getGestationalAgeInDays() {
		return GestationalAgeInDays;
	}
	public void setGestationalAgeInDays(Integer gestationalAgeInDays) {
		GestationalAgeInDays = gestationalAgeInDays;
	}
	public HipsExaminationResult getHipsExamination() {
		return HipsExamination;
	}
	public void setHipsExamination(HipsExaminationResult hipsExamination) {
		HipsExamination = hipsExamination;
	}
	public HipsUltraSoundOutcomeDecision getUltraSoundDecision() {
		return UltraSoundDecision;
	}
	public void setUltraSoundDecision(
			HipsUltraSoundOutcomeDecision ultraSoundDecision) {
		UltraSoundDecision = ultraSoundDecision;
	}
	public HipsExpertManagementPlanType getExpertManagementPlan() {
		return ExpertManagementPlan;
	}
	public void setExpertManagementPlan(
			HipsExpertManagementPlanType expertManagementPlan) {
		ExpertManagementPlan = expertManagementPlan;
	}
	public HeartExaminationResult getHeartExamination() {
		return HeartExamination;
	}
	public void setHeartExamination(HeartExaminationResult heartExamination) {
		HeartExamination = heartExamination;
	}
	public EyesExaminationResult getEyesExamination() {
		return EyesExamination;
	}
	public void setEyesExamination(EyesExaminationResult eyesExamination) {
		EyesExamination = eyesExamination;
	}
	public TestesExaminationResult getTestesExamination() {
		return TestesExamination;
	}
	public void setTestesExamination(TestesExaminationResult testesExamination) {
		TestesExamination = testesExamination;
	}
	public NHSNumberTraceStatus getPatientNHSNoTraceStatus() {
		return patientNHSNoTraceStatus;
	}
	public void setPatientNHSNoTraceStatus(
			NHSNumberTraceStatus patientNHSNoTraceStatus) {
		this.patientNHSNoTraceStatus = patientNHSNoTraceStatus;
	}
	public NHSNumberTraceStatus getGuardianNHSNoTraceStatus() {
		return guardianNHSNoTraceStatus;
	}
	public void setGuardianNHSNoTraceStatus(
			NHSNumberTraceStatus guardianNHSNoTraceStatus) {
		this.guardianNHSNoTraceStatus = guardianNHSNoTraceStatus;
	}
	public GuardianRoleType getGuardianRole() {
		return guardianRole;
	}
	public void setGuardianRole(GuardianRoleType guardianRole) {
		this.guardianRole = guardianRole;
	}
	public DateValue getBloodSpotSampleCollectedTime() {
		return bloodSpotSampleCollectedTime;
	}
	public void setBloodSpotSampleCollectedTime(
			DateValue bloodSpotSampleCollectedTime) {
		this.bloodSpotSampleCollectedTime = bloodSpotSampleCollectedTime;
	}
	public String getBloodSpotPerformerPersonSDSID() {
		return bloodSpotPerformerPersonSDSID;
	}
	public void setBloodSpotPerformerPersonSDSID(
			String bloodSpotPerformerPersonSDSID) {
		this.bloodSpotPerformerPersonSDSID = bloodSpotPerformerPersonSDSID;
	}
	public PersonName getBloodSpotPerformerPersonName() {
		return bloodSpotPerformerPersonName;
	}
	public void setBloodSpotPerformerPersonName(
			PersonName bloodSpotPerformerPersonName) {
		this.bloodSpotPerformerPersonName = bloodSpotPerformerPersonName;
	}
	public String getBloodSpotPerformerOrganisationODSID() {
		return bloodSpotPerformerOrganisationODSID;
	}
	public void setBloodSpotPerformerOrganisationODSID(
			String bloodSpotPerformerOrganisationODSID) {
		this.bloodSpotPerformerOrganisationODSID = bloodSpotPerformerOrganisationODSID;
	}
	public String getBloodSpotPerformerOrganisationName() {
		return bloodSpotPerformerOrganisationName;
	}
	public void setBloodSpotPerformerOrganisationName(
			String bloodSpotPerformerOrganisationName) {
		this.bloodSpotPerformerOrganisationName = bloodSpotPerformerOrganisationName;
	}
	public DateValue getBloodSpotTimeReceivedAtLab() {
		return bloodSpotTimeReceivedAtLab;
	}
	public void setBloodSpotTimeReceivedAtLab(DateValue bloodSpotTimeReceivedAtLab) {
		this.bloodSpotTimeReceivedAtLab = bloodSpotTimeReceivedAtLab;
	}
	public String getBloodSpotLabOrganisationODSID() {
		return bloodSpotLabOrganisationODSID;
	}
	public void setBloodSpotLabOrganisationODSID(
			String bloodSpotLabOrganisationODSID) {
		this.bloodSpotLabOrganisationODSID = bloodSpotLabOrganisationODSID;
	}
	public String getBloodSpotLabOrganisationName() {
		return bloodSpotLabOrganisationName;
	}
	public void setBloodSpotLabOrganisationName(String bloodSpotLabOrganisationName) {
		this.bloodSpotLabOrganisationName = bloodSpotLabOrganisationName;
	}
	public Integer getBirthOrder() {
		return BirthOrder;
	}
	public void setBirthOrder(Integer birthOrder) {
		BirthOrder = birthOrder;
	}
	public Integer getNoOfFoetusInConfinement() {
		return NoOfFoetusInConfinement;
	}
	public void setNoOfFoetusInConfinement(Integer noOfFoetusInConfinement) {
		NoOfFoetusInConfinement = noOfFoetusInConfinement;
	}
	public DateValue getAudiologyTestFindingEffectiveTime() {
		return AudiologyTestFindingEffectiveTime;
	}
	public void setAudiologyTestFindingEffectiveTime(
			DateValue audiologyTestFindingEffectiveTime) {
		AudiologyTestFindingEffectiveTime = audiologyTestFindingEffectiveTime;
	}
	public DateValue getDateOfHipsUltrasound() {
		return DateOfHipsUltrasound;
	}
	public void setDateOfHipsUltrasound(DateValue dateOfHipsUltrasound) {
		DateOfHipsUltrasound = dateOfHipsUltrasound;
	}
	public DateValue getDateHipsExpertManagementPlanCreated() {
		return DateHipsExpertManagementPlanCreated;
	}
	public void setDateHipsExpertManagementPlanCreated(
			DateValue dateHipsExpertManagementPlanCreated) {
		DateHipsExpertManagementPlanCreated = dateHipsExpertManagementPlanCreated;
	}
}
