/**
 * Helper classes are designed to make creating and consuming documents easier
 * by "hiding" some of their flexibility (and therefore complexity). They do
 * this by making certain assumptions (e.g. that patients are identified by
 * NHS number). The output of the helper is the populated document objects, which
 * can then be further modified if required. 
 */
package uk.nhs.interoperability.payloads.helpers;