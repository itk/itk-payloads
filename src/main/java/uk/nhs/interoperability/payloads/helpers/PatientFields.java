package uk.nhs.interoperability.payloads.helpers;

import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.HumanLanguage;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;

public interface PatientFields {
	public PersonName getPatientName();
	public void setPatientName(PersonName patientName);
	public PersonName getPatientPreferredName();
	public void setPatientPreferredName(PersonName patientPreferredName);
	public DateValue getPatientBirthDate();
	public void setPatientBirthDate(DateValue patientBirthDate);
	public String getPatientNHSNo();
	public void setPatientNHSNo(String patientNHSNo);
	public Boolean getPatientNHSNoIsTraced();
	public void setPatientNHSNoIsTraced(Boolean patientNHSNoIsTraced);
	public Sex getPatientGender();
	public void setPatientGender(Sex patientGender);
	public boolean isPatientInterpreterNeeded();
	public void setPatientInterpreterNeeded(boolean patientInterpreterNeeded);
	public HumanLanguage getPatientPreferredSpokenLanguage();
	public void setPatientPreferredSpokenLanguage(HumanLanguage patientPreferredSpokenLanguage);
	public String getPatientDisability();
	public void setPatientDisability(String patientDisability);
	public Address getPatientAddress();
	public void setPatientAddress(Address patientAddress);
	public String getPatientTelephone();
	public void setPatientTelephone(String patientTelephone);
	public String getPatientMobile();
	public void setPatientMobile(String patientMobile);
	public PersonName getUsualGPName();
	public void setUsualGPName(PersonName usualGPName);
	public String getUsualGPOrgName();
	public void setUsualGPOrgName(String usualGPOrgName);
	public String getUsualGPODSCode();
	public void setUsualGPODSCode(String usualGPODSCode);
	public String getUsualGPTelephone();
	public void setUsualGPTelephone(String usualGPTelephone);
	public String getUsualGPFax();
	public void setUsualGPFax(String usualGPFax);
	public Address getUsualGPAddress();
	public void setUsualGPAddress(Address usualGPAddress);
}
