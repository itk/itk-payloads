/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;

public class HumanReadableFormatter {

	/**
	 * This method turns the author fields into a more human readable form to display in the text section - for
	 * example: Ms Niral Singh, Palliative Care Physician at St James Hospital
	 * @param name Person's Name
	 * @param role Person's Role
	 * @param orgName Person's Org Name
	 * @return Human readable name
	 */
	public static String makeHumanReadablePersonDetails(PersonName name, JobRoleName role, String orgName) {
		StringBuilder out = new StringBuilder();
		// First, check for unstructured names
		if (name.getFullName() != null) {
			out.append(name.getFullName()).append(',').append(' ');
		} else {
			// Title
			if (name.getTitle() != null) {
				out.append(name.getTitle()).append(' ');
			}
			// Given Name
			out.append(name.getGivenName()).append(' ');
			// Family Name
			out.append(name.getFamilyName()).append(',').append(' ');
		}
		// Role
		if (role != null) {
			out.append(role.displayName);
			out.append(" at ");
		}
		// Org Name
		out.append(orgName);
		return out.toString();
	}
	
	public static String makeHumanReadablePersonName(PersonName name) {
		StringBuilder out = new StringBuilder();
		// First, check for unstructured names
		if (name.getFullName() != null) {
			out.append(name.getFullName());
		} else {
			// Title
			if (name.getTitle() != null) {
				out.append(name.getTitle()).append(' ');
			}
			// Given Name
			out.append(name.getGivenName()).append(' ');
			// Family Name
			out.append(name.getFamilyName());
		}
		return out.toString();
	}
	
	public static String makeHumanReadableAddress(Address address, String lineSeparator) {
		if (address == null) {
			return "";
		}
		StringBuilder out = new StringBuilder();
		boolean first = true;
		// First, check for unstructured names
		if (address.getFullAddress() != null) {
			out.append(address.getFullAddress());
		} else {
			// Street lines
			for (String line : address.getAddressLine()) {
				if (!first) out.append(lineSeparator);
				first = false;
				out.append(line);
			}
			// City
			if (address.getCity() != null) {
				if (!first) out.append(lineSeparator);
				first = false;
				out.append(address.getCity());
			}
			// Postcode
			if (address.getPostcode() != null) {
				if (!first) out.append(lineSeparator);
				first = false;
				out.append(address.getPostcode());
			}
 		}
		return out.toString();
	}
}
