/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.helpers;

import java.util.ArrayList;

import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.DateValue;
import uk.nhs.interoperability.payloads.commontypes.Address;
import uk.nhs.interoperability.payloads.commontypes.PersonName;
import uk.nhs.interoperability.payloads.vocabularies.generated.DocumentConsentSnCT;
import uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype;
import uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName;
import uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType;
import uk.nhs.interoperability.payloads.vocabularies.generated.Sex;
import uk.nhs.interoperability.payloads.vocabularies.internal.HL7ActType;

/**
 * This is a class to help simplify the creation of the non-coded CDA Document, and hide some of
 * the complexity of the underlying document. Once created using this helper, the document
 * can still be fine-tuned using the methods in objects created.
 * 
 * @author Adam Hatherly
 *
 */
public class CDACommonFields {
	private String documentTitle;
	private DateValue documentEffectiveTime;
	private String documentSetID;
	private int documentVersionNumber = 1;
	private PersonName patientName;
	private DateValue patientBirthDate;
	private String patientNHSNo;
	private Boolean patientNHSNoIsTraced;
	
	// Local ID for the patient (e.g. hospital number)
	private String patientLocalID;
	private String patientLocalIDAssigningAuthority;
	
	private Sex patientGender;
	private Address patientAddress;
	private String patientTelephone;
	private String patientMobile;
	private String usualGPOrgName;
	private String usualGPODSCode;
	private String usualGPTelephone;
	private String usualGPFax;
	private Address usualGPAddress;
	private DateValue timeAuthored;
	private Address documentAuthorAddress;
	private JobRoleName documentAuthorRole;
	
	// SDS IDs
	private String documentAuthorSDSID;
	private String documentAuthorSDSRoleID;
	// Local IDs
	private String documentAuthorLocalID;
	private String documentAuthorLocalIDAssigningAuthority;
	
	private String documentAuthorTelephone;
	private PersonName documentAuthorName;
	private String documentAuthorOrganisationODSID;
	private String documentAuthorOrganisationName;
	
	private String documentAuthorWorkgroupName;
	// SDS ID
	private String documentAuthorWorkgroupSDSID;
	private String documentAuthorWorkgroupLocalID;
	private String documentAuthorWorkgroupLocalIDAssigningAuthority;
	
	// SDS IDs
	private String dataEntererSDSID;
	private String dataEntererSDSRoleID;
	// Local IDs
	private String dataEntererLocalID;
	private String dataEntererLocalIDAssigningAuthority;
	
	private PersonName dataEntererName;
	private String custodianODSCode;
	private String custodianOrganisationName;
	private ArrayList<DocumentRecipient> recipients;
	private ArrayList<DocumentRecipient> copyRecipients;
	
	// SDS IDs
	private String authenticatorSDSID;
	private String authenticatorSDSRoleID;
	// Local IDs
	private String authenticatorLocalID;
	private String authenticatorLocalIDAssigningAuthority;
	
	private PersonName authenticatorName;
	private DateValue authenticatedTime;
	private ArrayList<CDADocumentParticipant> participants;
	
	private DocumentConsentSnCT consent;
	private DateValue encounterFromTime;
	private DateValue encounterToTime;
	private CodedValue encounterType;
	private CodedValue encounterLocationType;
	private String encounterLocationName;
	private Address encounterLocationAddress;
	
	public String getDocumentTitle() {
		return documentTitle;
	}
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}
	public DateValue getDocumentEffectiveTime() {
		return documentEffectiveTime;
	}
	public void setDocumentEffectiveTime(DateValue documentEffectiveTime) {
		this.documentEffectiveTime = documentEffectiveTime;
	}
	public int getDocumentVersionNumber() {
		return documentVersionNumber;
	}
	public void setDocumentVersionNumber(int documentVersionNumber) {
		this.documentVersionNumber = documentVersionNumber;
	}
	public PersonName getPatientName() {
		return patientName;
	}
	public void setPatientName(PersonName patientName) {
		this.patientName = patientName;
	}
	public DateValue getPatientBirthDate() {
		return patientBirthDate;
	}
	public void setPatientBirthDate(DateValue patientBirthDate) {
		this.patientBirthDate = patientBirthDate;
	}
	public String getPatientNHSNo() {
		return patientNHSNo;
	}
	public void setPatientNHSNo(String patientNHSNo) {
		this.patientNHSNo = patientNHSNo;
	}
	public Boolean getPatientNHSNoIsTraced() {
		return patientNHSNoIsTraced;
	}
	public void setPatientNHSNoIsTraced(Boolean patientNHSNoIsTraced) {
		this.patientNHSNoIsTraced = patientNHSNoIsTraced;
	}
	public Sex getPatientGender() {
		return patientGender;
	}
	public void setPatientGender(Sex patientGender) {
		this.patientGender = patientGender;
	}
	public Address getPatientAddress() {
		return patientAddress;
	}
	public void setPatientAddress(Address patientAddress) {
		this.patientAddress = patientAddress;
	}
	public String getPatientTelephone() {
		return patientTelephone;
	}
	public void setPatientTelephone(String patientTelephone) {
		this.patientTelephone = patientTelephone;
	}
	public String getPatientMobile() {
		return patientMobile;
	}
	public void setPatientMobile(String patientMobile) {
		this.patientMobile = patientMobile;
	}
	public String getUsualGPOrgName() {
		return usualGPOrgName;
	}
	public void setUsualGPOrgName(String usualGPOrgName) {
		this.usualGPOrgName = usualGPOrgName;
	}
	public String getUsualGPODSCode() {
		return usualGPODSCode;
	}
	public void setUsualGPODSCode(String usualGPODSCode) {
		this.usualGPODSCode = usualGPODSCode;
	}
	public String getUsualGPTelephone() {
		return usualGPTelephone;
	}
	public void setUsualGPTelephone(String usualGPTelephone) {
		this.usualGPTelephone = usualGPTelephone;
	}
	public String getUsualGPFax() {
		return usualGPFax;
	}
	public void setUsualGPFax(String usualGPFax) {
		this.usualGPFax = usualGPFax;
	}
	public Address getUsualGPAddress() {
		return usualGPAddress;
	}
	public void setUsualGPAddress(Address usualGPAddress) {
		this.usualGPAddress = usualGPAddress;
	}
	public DateValue getTimeAuthored() {
		return timeAuthored;
	}
	public void setTimeAuthored(DateValue timeAuthored) {
		this.timeAuthored = timeAuthored;
	}
	public String getDataEntererSDSID() {
		return dataEntererSDSID;
	}
	public void setDataEntererSDSID(String dataEntererSDSID) {
		this.dataEntererSDSID = dataEntererSDSID;
	}
	public String getDataEntererSDSRoleID() {
		return dataEntererSDSRoleID;
	}
	public void setDataEntererSDSRoleID(String dataEntererSDSRoleID) {
		this.dataEntererSDSRoleID = dataEntererSDSRoleID;
	}
	public PersonName getDataEntererName() {
		return dataEntererName;
	}
	public void setDataEntererName(PersonName dataEntererName) {
		this.dataEntererName = dataEntererName;
	}
	public String getCustodianODSCode() {
		return custodianODSCode;
	}
	public void setCustodianODSCode(String custodianODSCode) {
		this.custodianODSCode = custodianODSCode;
	}
	public String getCustodianOrganisationName() {
		return custodianOrganisationName;
	}
	public void setCustodianOrganisationName(String custodianOrganisationName) {
		this.custodianOrganisationName = custodianOrganisationName;
	}
	public ArrayList<DocumentRecipient> getRecipients() {
		return recipients;
	}
	public void setRecipients(ArrayList<DocumentRecipient> recipients) {
		this.recipients = recipients;
	}
	public void addRecipient(DocumentRecipient recipient) {
		if (this.recipients == null) {
			this.recipients = new ArrayList<DocumentRecipient>();
		}
		this.recipients.add(recipient);
	}
	public ArrayList<DocumentRecipient> getCopyRecipients() {
		return copyRecipients;
	}
	public void setCopyRecipients(ArrayList<DocumentRecipient> copyRecipients) {
		this.copyRecipients = copyRecipients;
	}
	public void addCopyRecipient(DocumentRecipient copyRecipient) {
		if (this.copyRecipients == null) {
			this.copyRecipients = new ArrayList<DocumentRecipient>();
		}
		this.copyRecipients.add(copyRecipient);
	}
	public String getAuthenticatorSDSID() {
		return authenticatorSDSID;
	}
	public void setAuthenticatorSDSID(String authenticatorSDSID) {
		this.authenticatorSDSID = authenticatorSDSID;
	}
	public String getAuthenticatorSDSRoleID() {
		return authenticatorSDSRoleID;
	}
	public void setAuthenticatorSDSRoleID(String authenticatorSDSRoleID) {
		this.authenticatorSDSRoleID = authenticatorSDSRoleID;
	}
	public PersonName getAuthenticatorName() {
		return authenticatorName;
	}
	public void setAuthenticatorName(PersonName authenticatorName) {
		this.authenticatorName = authenticatorName;
	}
	public DateValue getAuthenticatedTime() {
		return authenticatedTime;
	}
	public void setAuthenticatedTime(DateValue authenticatedTime) {
		this.authenticatedTime = authenticatedTime;
	}
	
	public ArrayList<CDADocumentParticipant> getParticipants() {
		return participants;
	}
	public void setParticipants(ArrayList<CDADocumentParticipant> participants) {
		this.participants = participants;
	}
	public void addParticipant(CDADocumentParticipant participant) {
		if (this.participants == null) {
			this.participants = new ArrayList<CDADocumentParticipant>();
		}
		this.participants.add(participant);
	}
	
	public DocumentConsentSnCT getConsent() {
		return consent;
	}
	public void setConsent(DocumentConsentSnCT consent) {
		this.consent = consent;
	}
	public DateValue getEncounterFromTime() {
		return encounterFromTime;
	}
	public void setEncounterFromTime(DateValue encounterFromTime) {
		this.encounterFromTime = encounterFromTime;
	}
	public DateValue getEncounterToTime() {
		return encounterToTime;
	}
	public void setEncounterToTime(DateValue encounterToTime) {
		this.encounterToTime = encounterToTime;
	}
	public CodedValue getEncounterType() {
		return encounterType;
	}
	public void setEncounterType(CodedValue encounterType) {
		this.encounterType = encounterType;
	}
	public CodedValue getEncounterLocationType() {
		return encounterLocationType;
	}
	public void setEncounterLocationType(CodedValue encounterLocationType) {
		this.encounterLocationType = encounterLocationType;
	}
	public String getEncounterLocationName() {
		return encounterLocationName;
	}
	public void setEncounterLocationName(String encounterLocationName) {
		this.encounterLocationName = encounterLocationName;
	}
	public Address getEncounterLocationAddress() {
		return encounterLocationAddress;
	}
	public void setEncounterLocationAddress(Address encounterLocationAddress) {
		this.encounterLocationAddress = encounterLocationAddress;
	}
	public String getDocumentSetID() {
		return documentSetID;
	}
	public void setDocumentSetID(String documentSetID) {
		this.documentSetID = documentSetID;
	}
	public Address getDocumentAuthorAddress() {
		return documentAuthorAddress;
	}
	public void setDocumentAuthorAddress(Address documentAuthorAddress) {
		this.documentAuthorAddress = documentAuthorAddress;
	}
	public JobRoleName getDocumentAuthorRole() {
		return documentAuthorRole;
	}
	public void setDocumentAuthorRole(JobRoleName documentAuthorRole) {
		this.documentAuthorRole = documentAuthorRole;
	}
	public String getDocumentAuthorSDSID() {
		return documentAuthorSDSID;
	}
	public void setDocumentAuthorSDSID(String documentAuthorSDSID) {
		this.documentAuthorSDSID = documentAuthorSDSID;
	}
	public String getDocumentAuthorSDSRoleID() {
		return documentAuthorSDSRoleID;
	}
	public void setDocumentAuthorSDSRoleID(String documentAuthorSDSRoleID) {
		this.documentAuthorSDSRoleID = documentAuthorSDSRoleID;
	}
	public String getDocumentAuthorTelephone() {
		return documentAuthorTelephone;
	}
	public void setDocumentAuthorTelephone(String documentAuthorTelephone) {
		this.documentAuthorTelephone = documentAuthorTelephone;
	}
	public PersonName getDocumentAuthorName() {
		return documentAuthorName;
	}
	public void setDocumentAuthorName(PersonName documentAuthorName) {
		this.documentAuthorName = documentAuthorName;
	}
	public String getDocumentAuthorOrganisationODSID() {
		return documentAuthorOrganisationODSID;
	}
	public void setDocumentAuthorOrganisationODSID(
			String documentAuthorOrganisationODSID) {
		this.documentAuthorOrganisationODSID = documentAuthorOrganisationODSID;
	}
	public String getDocumentAuthorOrganisationName() {
		return documentAuthorOrganisationName;
	}
	public void setDocumentAuthorOrganisationName(
			String documentAuthorOrganisationName) {
		this.documentAuthorOrganisationName = documentAuthorOrganisationName;
	}
	public String getDocumentAuthorLocalID() {
		return documentAuthorLocalID;
	}
	public void setDocumentAuthorLocalID(String documentAuthorLocalID) {
		this.documentAuthorLocalID = documentAuthorLocalID;
	}
	public String getDocumentAuthorLocalIDAssigningAuthority() {
		return documentAuthorLocalIDAssigningAuthority;
	}
	public void setDocumentAuthorLocalIDAssigningAuthority(
			String documentAuthorLocalIDAssigningAuthority) {
		this.documentAuthorLocalIDAssigningAuthority = documentAuthorLocalIDAssigningAuthority;
	}
	public String getDataEntererLocalID() {
		return dataEntererLocalID;
	}
	public void setDataEntererLocalID(String dataEntererLocalID) {
		this.dataEntererLocalID = dataEntererLocalID;
	}
	public String getDataEntererLocalIDAssigningAuthority() {
		return dataEntererLocalIDAssigningAuthority;
	}
	public void setDataEntererLocalIDAssigningAuthority(
			String dataEntererLocalIDAssigningAuthority) {
		this.dataEntererLocalIDAssigningAuthority = dataEntererLocalIDAssigningAuthority;
	}
	public String getAuthenticatorLocalID() {
		return authenticatorLocalID;
	}
	public void setAuthenticatorLocalID(String authenticatorLocalID) {
		this.authenticatorLocalID = authenticatorLocalID;
	}
	public String getAuthenticatorLocalIDAssigningAuthority() {
		return authenticatorLocalIDAssigningAuthority;
	}
	public void setAuthenticatorLocalIDAssigningAuthority(
			String authenticatorLocalIDAssigningAuthority) {
		this.authenticatorLocalIDAssigningAuthority = authenticatorLocalIDAssigningAuthority;
	}
	public String getDocumentAuthorWorkgroupSDSID() {
		return documentAuthorWorkgroupSDSID;
	}
	public void setDocumentAuthorWorkgroupSDSID(String documentAuthorWorkgroupSDSID) {
		this.documentAuthorWorkgroupSDSID = documentAuthorWorkgroupSDSID;
	}
	public String getDocumentAuthorWorkgroupLocalID() {
		return documentAuthorWorkgroupLocalID;
	}
	public void setDocumentAuthorWorkgroupLocalID(
			String documentAuthorWorkgroupLocalID) {
		this.documentAuthorWorkgroupLocalID = documentAuthorWorkgroupLocalID;
	}
	public String getDocumentAuthorWorkgroupLocalIDAssigningAuthority() {
		return documentAuthorWorkgroupLocalIDAssigningAuthority;
	}
	public void setDocumentAuthorWorkgroupLocalIDAssigningAuthority(
			String documentAuthorWorkgroupLocalIDAssigningAuthority) {
		this.documentAuthorWorkgroupLocalIDAssigningAuthority = documentAuthorWorkgroupLocalIDAssigningAuthority;
	}
	public String getDocumentAuthorWorkgroupName() {
		return documentAuthorWorkgroupName;
	}
	public void setDocumentAuthorWorkgroupName(String documentAuthorWorkgroupName) {
		this.documentAuthorWorkgroupName = documentAuthorWorkgroupName;
	}
	public String getPatientLocalID() {
		return patientLocalID;
	}
	public void setPatientLocalID(String patientLocalID) {
		this.patientLocalID = patientLocalID;
	}
	public String getPatientLocalIDAssigningAuthority() {
		return patientLocalIDAssigningAuthority;
	}
	public void setPatientLocalIDAssigningAuthority(
			String patientLocalIDAssigningAuthority) {
		this.patientLocalIDAssigningAuthority = patientLocalIDAssigningAuthority;
	}
}
