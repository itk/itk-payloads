/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import uk.nhs.interoperability.payloads.FieldType;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.documentation.XPathReportCreator;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.DOMFieldHandler;

public class PayloadParser {
	private static final Logger logger = LoggerFactory.getLogger(PayloadParser.class);
	
	public static void parse(String xmlDoc, Payload p, String name) {
		ByteArrayInputStream is = new ByteArrayInputStream(xmlDoc.getBytes());
		parse(is, p, name, null);
	}
	
	public static void parse(String xmlDoc, Payload p, String name, XMLNamespaceContext parentNamespaces) {
		ByteArrayInputStream is = new ByteArrayInputStream(xmlDoc.getBytes());
		parse(is, p, name, parentNamespaces);
	}
	
	public static void parse(InputStream xmlContent, Payload p, String name, XMLNamespaceContext parentNamespaces) {
		try {
			Document xmldoc = generateDOM(xmlContent, true);
			parse(xmldoc, p, name, parentNamespaces);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void parse(Document xmldoc, Payload p, String name) {
		parse(xmldoc, p, name, null);
	}
	
	public static void parse(Document xmldoc, Payload p, String name, XMLNamespaceContext parentNamespaces) {
		XMLNamespaceContext namespaces = p.getNamespaceContext();
		if (parentNamespaces != null) {
			namespaces.inheritNamespaces(parentNamespaces);
		}
		logger.trace("Parsing XML document");
		Element root = xmldoc.getDocumentElement();
		XPathReportCreator.initialise();
		parsePayload(namespaces, p, root, xmldoc, name);
		XPathReportCreator.finalise();
	}
	
	public static Document generateDOM(InputStream xmlContent, boolean namespaceAware) throws ParserConfigurationException, SAXException, IOException, SAXParseException {
		Document result = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(namespaceAware);
		DocumentBuilder builder;
		builder = factory.newDocumentBuilder();
		result = builder.parse(xmlContent);
		return result;
	}
	
	public static void parsePayload(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name) {
		if (parent == null) {
			return;
		}
		Map<String, Field> fieldDefinitions = p.getFieldDefinitions();
		for (String fieldName : fieldDefinitions.keySet()) {
			Field field = fieldDefinitions.get(fieldName);
			FieldType fieldType = field.getTypeEnum();
			processField(namespaces, p, parent, xmldoc, name, field);
		}
	}

	private static void processField(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		String strXpath = field.getXpath();
		FieldType fieldType = field.getTypeEnum();
		DOMFieldHandler handler = fieldType.getHandler();
		
		logger.trace("=== Attempting to find field: {} which is of type {}", field.getName(), field.getTypeName());
		
		boolean continueProcessing = handler.preProcessParse(namespaces, p, parent, xmldoc, name, field);
		
		if (continueProcessing) {
			
			if (strXpath != null) {
				// An xpath of . is used for unstructured names, addresses, etc, but should only be used when there are no structured fields
				if (strXpath.equals(".")) {
					if (p.hasData()) {
						logger.trace("Skipping unstructured data field (with Xpath = .) within payload object: {} as the class already contains structured fields", p.getClassName());
						return;
					}
				}
				
				// If this is a list, return all the matching nodes and add each to the payload object field
				if (field.getMaxOccurs() > 1) {
					XPathExpression xpath = field.getCompiledXpath(namespaces);
					ArrayList<Element> list = XPaths.findListFromXPath(xpath, parent, strXpath);
					if (list != null) {
						for (Element item : list) {
							
							Object elementValueObject = handler.parse(field, item, namespaces, p, xmldoc, parent);
							
							if (elementValueObject != null) {
									p.addMultivalue(field.getName(), elementValueObject, true);
							}
						}
					} else {
						logger.trace("  ** NO MATCH FOUND!");
					}
				} else {
					
					XPathExpression xpath = field.getCompiledXpath(namespaces);
					Element e = handler.getElement(xpath, parent, strXpath);
					
					Object elementValueObject = handler.parse(field, e, namespaces, p, xmldoc, parent);
					
					if (elementValueObject != null) {
						p.setValue(field.getName(), elementValueObject);
					}
				}
			} else {
				// We have a null xpath, which probably means this is a field that is used to drive other field logic
				// the value of this field will be set when parsing the fixed field values (below)
			}
		}
	}
}
