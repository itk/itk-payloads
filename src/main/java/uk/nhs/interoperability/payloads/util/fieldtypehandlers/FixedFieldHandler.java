/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import java.util.ArrayList;

import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.documentation.XPathReportCreator;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

public class FixedFieldHandler extends AbstractFieldHandler implements DOMFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(FixedFieldHandler.class);
	
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		// If enabled, output an entry in an XPath Report
		XPathReportCreator.addField(field, p, field.getFixed(), null);
		// This is a fixed field - check if it is used to drive any other fields (i.e. special fields with no xpath)
		String ifValue = field.getIfValue();
		if (ifValue != null) {
			String strXpath = field.getXpath();
			XPathExpression xpath = field.getCompiledXpath(namespaces);
			// See whether this fixed value exists in the message
			String elementValueStr = XPaths.findStringFromXPath(xpath, parent, strXpath);
			if (elementValueStr != null) {
				if (elementValueStr.equals(field.getFixed())) {
					// Find and set the appropriate variable
					String ifValueField = field.getIfValueField();
					p.setValue(ifValueField, ifValue);
					//Logger.trace("Setting special field value for field " + ifValueField + " to value: " + ifValue);
				}
			}
		}
		//Logger.trace("Fixed field - no further processing");
		return false;
	}
	
	@Override
	public boolean outputField(Field field, Payload p) {
		boolean outputField = true;
		
		// See if there is an ifExists condition attached
		String fieldName = field.getIfExists();
		if (fieldName != null) {
			// See whether the required field does exist - if not, prevent this field being added					
			Object o = p.getValue(fieldName);
			if (o == null) {
				outputField = false;
				logger.trace("  Excluding due to NULL in ifExists check - field: {}", field.getIfExists());
			} else {
				// Now, check if this is an empty payload, which is equivalent to not existing..
				Field defn = p.getFieldDefinitions().get(fieldName);
				DOMFieldHandler handler = defn.getHandler();
				if (handler.isPayload()) {
					boolean isBlank = true;
					if (defn.getMaxOccurs()>1) {
						for (Object fieldListItem : (ArrayList)o) {
							if (!handler.isItemBlank(defn, fieldListItem, true)) {
								isBlank = false; 
							}
						}
					} else { 
						isBlank = handler.isItemBlank(defn, o, false);
					}
					if (isBlank) {
						outputField = false;
						logger.trace("  Excluding due to ifExists check - this is an empty child payload - field: {}", field.getIfExists());
					}
				}
				logger.trace("  NOT EXCLUDING - ifExists check - field: {}", field.getIfExists());
			}
		}
		
		// See of there is an ifNotExists condition attached
		fieldName = field.getIfNotExists();
		if (fieldName != null) {
			// See whether the required field does exist - if it does, prevent this field being added
			Object o = p.getValue(fieldName);
			if (o != null) {
				// Now, check if this is an empty payload, which is equivalent to not existing..
				Field defn = p.getFieldDefinitions().get(fieldName);
				DOMFieldHandler handler = defn.getHandler();
				if (handler.isPayload()) {
					boolean isBlank = handler.isItemBlank(defn, o, false);
					if (!isBlank) {
						outputField = false;
						logger.trace("  Excluding due to ifNotExists check - this is a non-empty child payload - field: {}", field.getIfExists());
					}
				} else {
					outputField = false;
					logger.trace("  Excluding due to ifNotExists check - field: {}", field.getIfNotExists());
				}
			} else {
				logger.trace("  NOT EXCLUDING - ifNotExists check - field: {}", field.getIfNotExists());
			}
		}
		
		
		// See of there is an ifValue condition attached
		//Logger.trace("ifValue check for fixed field " + field.getName() + " - field: " + field.getIfValueField() + " value: " + field.getIfValue());
		if ((field.getIfValue() != null) && (field.getIfValueField() != null)) {
			// See whether the specified field matches the specified value, and if not, prevent the field being added
			Object o = p.getValue(field.getIfValueField());
			if (o != null) {
				if (!(p.getValue(field.getIfValueField()).equals(field.getIfValue()))) {
					outputField = false;
					logger.trace("  Excluding due to ifValue check - field: {} value: {}", field.getIfValueField(), field.getIfValue());
				} else {
					logger.trace("  NOT EXCLUDING - ifValue check - field: {} value: {}", field.getIfValueField(), field.getIfValue());
				}
			} else {
				logger.trace("  NOT EXCLUDING - ifValue check - field: {} value: {}", field.getIfValueField(), field.getIfValue());
			}
			// Note: Even with minimal serialisation, we don't want to exclude fields whose value depends on other
			// fields as these carry information and are not purely boilerplate
		}
		if (outputField) {
			logger.trace("  This field ({}) WILL be output", field.getName());
		} else {
			logger.trace("  This field ({}) will NOT be output", field.getName());
		}
		return outputField;
	}
	
	public static String processDerivedValues(Field field, Payload p) {
		String deriveValueFromTemplateNameUsedInField = field.getDeriveValueFromTemplateNameUsedInField();
		if (deriveValueFromTemplateNameUsedInField != null) {
			logger.trace(" We need to find a template name from the payload in field: {}", deriveValueFromTemplateNameUsedInField);
			Payload childPayloadObject = null;
			if (p.getValue(deriveValueFromTemplateNameUsedInField) != null) {
				childPayloadObject = (Payload)p.getValue(deriveValueFromTemplateNameUsedInField);
				logger.trace(" Template name found - set as: {}", childPayloadObject.getVersionedName());
				return childPayloadObject.getVersionedName();
				//p.setValue(field.getName(), p.getValue(deriveValueFromTemplateNameUsedInField));
			} else {
				logger.trace(" Unable to get the template from the payload field!");
			}
		}
		return null;
	}
	
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object item,
			String xpath, Element parent, Document xmldoc, Field field, Payload payload) {
		// Do nothing
		return true;
	}

	@Override
	public String stringValue(Field definition, Payload p, Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) {
		String derivedVal = FixedFieldHandler.processDerivedValues(definition, p);
		if (derivedVal != null) {
			return derivedVal;
		}
		return definition.getFixed();
	}
	
	@Override
	public boolean isItemBlank(Field defn, Object item, boolean singleItem) {
		return false;
	}
	
	@Override
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent) {
		return null;
	}
}
