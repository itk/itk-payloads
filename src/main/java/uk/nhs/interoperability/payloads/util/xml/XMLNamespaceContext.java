/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLNamespaceContext implements NamespaceContext {
	private static final Logger logger = LoggerFactory.getLogger(XMLNamespaceContext.class);
	
	/*public static final String LOCALISATIONNAMESPACE = "NPFIT:HL7:Localisation";
	public static final String LOCALISATIONNAMESPACE_DEFAULY_PREFIX = "npfitlc";
	
	public static final String SCHEMANAMESPACE = "http://www.w3.org/2001/XMLSchema-instance";
	public static final String SCHEMANAMESPACE_DEFAULY_PREFIX = "xsi";
    
	private static final String DOCUMENT_NAMESPACE = "urn:hl7-org:v3";
	private static final String DOCUMENT_NAMESPACE_PREFIX = "x";
	*/
	
    public Map<String, String> namespaces = new HashMap<String, String>();
    private String documentNamespace="";
    private String documentNamespacePrefix="";
    
    /*static {
    	namespaces.put(LOCALISATIONNAMESPACE_DEFAULY_PREFIX, LOCALISATIONNAMESPACE);
    	namespaces.put(SCHEMANAMESPACE_DEFAULY_PREFIX, SCHEMANAMESPACE);
    	namespaces.put(DOCUMENT_NAMESPACE_PREFIX, DOCUMENT_NAMESPACE);
    }*/

	@Override
	public String getNamespaceURI(String prefix) {
		return namespaces.get(prefix);
	}

	@Override
	public String getPrefix(String namespaceURI) {
		String prefix = null;
		for (Map.Entry<String, String> entry : namespaces.entrySet()) {
			if (entry.getValue().equals(namespaceURI)) {
				if (prefix == null) {
					prefix = entry.getKey();
				} else if (prefix.length()==0) {
					// Prefer non-blank prefix if one exists
					prefix = entry.getKey();
				}
			}
		}
		if (prefix == null) {
			logger.warn("No prefix found for namespace: {}", namespaceURI);
		}
		return prefix;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Iterator getPrefixes(String namespaceURI) {
		List<String> prefixes = new ArrayList<String>();
		for (Map.Entry<String, String> entry : namespaces.entrySet()) {
			if (entry.getValue().equals(namespaceURI)) {
				prefixes.add(entry.getKey());
			}
		}
		return prefixes.iterator();
	}
	
	public Iterator getPrefixes() {
		List<String> prefixes = new ArrayList<String>();
		for (Map.Entry<String, String> entry : namespaces.entrySet()) {
			prefixes.add(entry.getKey());
		}
		return prefixes.iterator();
	}
	
	public Map<String, String> getAllNamespaces() {
		return namespaces;
	}
	
	public String getDocumentNamespace() {
		return documentNamespace;
	}
	
	public String getDocumentNamespacePrefix() {
		return documentNamespacePrefix;
	}
	
	public void addNamespace(String prefix, String url, boolean def) {
		namespaces.put(prefix, url);
		if (def) {
			documentNamespace = url;
			documentNamespacePrefix = prefix;
		}
	}
	
	public void inheritNamespaces(XMLNamespaceContext parentNamespaces) {
		Map<String, String> parentEntries = parentNamespaces.getAllNamespaces();
		for (Map.Entry<String, String> entry : parentEntries.entrySet()) {
			namespaces.put(entry.getKey(), entry.getValue());
		}
		if (parentNamespaces.getDocumentNamespace() != null) {
			if (parentNamespaces.getDocumentNamespace().length()>0) {
				documentNamespace = parentNamespaces.getDocumentNamespace();
				documentNamespacePrefix = parentNamespaces.getDocumentNamespacePrefix();
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Document Namespace - prefix: ").append(documentNamespacePrefix).append(" uri: ").append(documentNamespace);
		for (String prefix : namespaces.keySet()) {
			sb.append("\n - Namespace - prefix: ").append(prefix).append(" uri: ").append(namespaces.get(prefix));
		}
		return sb.toString();
	}
}
