/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public class AbstractFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(AbstractFieldHandler.class);
	
	public boolean outputField(Field field, Payload p) {
		boolean outputField = true;
		if (p.getValue(field.getName()) == null) {
			outputField = false;
		}
		if (field.getXpath() == null) {
			outputField = false;
		}
		//Logger.trace("	Output " + field.getName() + " field: " + outputField);
		return outputField;
	}
	
	public boolean isPayload() {
		return false;
	}
	
	public boolean isItemBlank(Field defn, Object item, boolean singleItem) {
		return (item==null);
	}
	
	public Element getElement(XPathExpression xpath, Element parent, String strXpath) {
		return null;
	}
	
	public Object convertValueToObject(XMLNamespaceContext namespaces,
			Payload p, Field definition, String val, String packg,
			String versionedName) {
		return val;
	}
	
	public void postProcessSerialise(Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) { 
		// Do nothing
	}
}
