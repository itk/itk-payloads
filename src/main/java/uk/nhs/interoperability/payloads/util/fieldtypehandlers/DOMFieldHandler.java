/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Code;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.FieldHandler;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public interface DOMFieldHandler extends FieldHandler {
	// Methods for Serialising fields
	public boolean outputField(Field field, Payload p);
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object item, String xpath, Element parent, Document xmldoc, Field field, Payload payload);
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field);
	public String stringValue(Field definition, Payload p, Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc);
	public boolean isPayload();
	public boolean isItemBlank(Field defn, Object item, boolean singleItem);
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent);
	public Element getElement(XPathExpression xpath, Element parent, String strXpath);
	public Object convertValueToObject(XMLNamespaceContext namespaces, Payload p, Field definition, String val, String packg, String versionedName);
	public void postProcessSerialise(Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc);
}
