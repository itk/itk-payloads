/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.xml.PayloadParser;
import uk.nhs.interoperability.payloads.util.xml.PayloadSerialiser;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

public class XMLFieldHandler extends AbstractFieldHandler implements DOMFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(XMLFieldHandler.class);
	
	@Override
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent) {
		//Element e = XPaths.findNodeFromXPath(field.getCompiledXpath(namespaces), parent, field.getXpath());
		logger.trace("Attempting to adopt child XML nodes and serialise them to a String in field: {}", field.getName());
		//String elementValueStr = XPaths.findStringFromXPath(field.getCompiledXpath(namespaces), item, field.getXpath());
		//Logger.trace("When parsed as a String, the value would have been: " + elementValueStr);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		if (item != null) {
			// We don't want the text node itself, but we do want each of its children
			if (item.hasChildNodes()) {
				NodeList nl = item.getChildNodes();
				int length = nl.getLength();
				for (int n=0; n<length; n++) {
					Node node = nl.item(n);
					PayloadSerialiser.serialiseDOMtoStream(node, out);
				}
			}
			String value = out.toString();
			logger.trace("Final string being set in field: {}", value);
			p.setValue(field.getName(), value);
		}
		return null;
	}
	
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object item, String xpath, Element parent, Document xmldoc, Field field, Payload payload) {
		return true;
	}
	
	@Override
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		// Do nothing
		return true;
	}

	@Override
	public String stringValue(Field definition, Payload p, Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) {
		return null;
	}
	
	@Override
	public void postProcessSerialise(Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) {
		//return null;
		String itemStr = val.toString();
		logger.trace("NESTED XML: {}", itemStr);
		//Logger.trace("XPATH: " + xpath);
		 
		// Because this may not be a single XML document with a root node, we will wrap it in a
		// single node, which will also put all the nested XML into the correct namespace
		ByteArrayInputStream xmlContent = new ByteArrayInputStream(("<x xmlns=\"urn:hl7-org:v3\">"+itemStr+"</x>").getBytes());
		try {
			Document childXMLDoc = PayloadParser.generateDOM(xmlContent, true);
			// Add each of the children of our outer element to the output DOM
			NodeList nl = childXMLDoc.getDocumentElement().getChildNodes();
			int length = nl.getLength();
			for (int n=0; n<length; n++) {
				Node newNode = xmldoc.importNode(nl.item(n), true);
				parent.appendChild(newNode);
			}
		} catch (ParserConfigurationException e1) {
			logger.error("Unable to parse XML in text section", e1);
		} catch (SAXException e1) {
			logger.error("Unable to convert embedded XML content into a DOM - could be badly formed? - XML provided was: {}", itemStr, e1);
		} catch (IOException e1) {
			logger.error("Unable to parse XML in text section", e1);
		}
	}
	
	@Override
	public Element getElement(XPathExpression xpath, Element parent, String strXpath) {
		return XPaths.findNodeFromXPath(xpath, parent, strXpath);
	}
}
