/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.RootNodeNameResolver;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.documentation.XPathReportCreator;
import uk.nhs.interoperability.payloads.util.xml.PayloadParser;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

public class TemplatedFieldHandler extends PayloadFieldHandler implements DOMFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(TemplatedFieldHandler.class);
	
	@Override
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		logger.trace("Pre-process for templated field: {}", field.getName());
		String implementationVersionedName = null;
		
		if (field.getMaxOccurs()>1) {
			logger.error("!!!!! LIST OF TEMPLATED FIELDS !!!!!!!");
		}
		
		// See if we can find the identifier for the template
		if (field.getVersionedIdentifierXPath() != null) {
			logger.trace("Looking for implementationVersionedName in xpath: {}", field.getVersionedIdentifierXPath());
			implementationVersionedName = XPaths.findStringFromXPath(field.getCompiledVersionedIdentifierXPath(namespaces), parent, field.getVersionedIdentifierXPath());
			logger.trace("implementationVersionedName = {}", implementationVersionedName);
		}
		
		if (implementationVersionedName != null) {
			if (implementationVersionedName.length()>0) {
				// Now, to find the correct element name and replace it in the xpath provided if required
				String strXpath = field.getXpath();
				XPathExpression xpath;
				if (strXpath.contains("{")) {
					String packg = p.getPackage();
					if (field.getTypePackage() != null) {
						packg = field.getTypePackage(); 
					}
					String nodeName = RootNodeNameResolver.getRootNodeName(implementationVersionedName, packg);
					strXpath = XPaths.replaceVariableInXPath(nodeName, strXpath);
					xpath = XPaths.compileXpath(namespaces, strXpath);
				} else {
					xpath = field.getCompiledXpath(namespaces);
				}
				
				String elementValueStr = XPaths.findStringFromXPath(xpath, parent, strXpath);
				
				if (elementValueStr != null) {
					//Object elementValueObject = PayloadHelper.getValueAsObject(namespaces, p, field, elementValueStr, p.getPackage(), implementationVersionedName);
					Object elementValueObject = convertValueToObject(namespaces, p, field, elementValueStr, p.getPackage(), implementationVersionedName);
					if (elementValueObject != null) {
						p.setValue(field.getName(), elementValueObject);
						
						logger.trace(" === Attempting to recurse into child Payload object using XPath: {}", strXpath);
						Element childElement = XPaths.findNodeFromXPath(xpath, parent, strXpath);
						if (childElement != null) {
							// Combine our interited namespaces with any additional ones defined in child payload object
							namespaces.inheritNamespaces(((Payload)elementValueObject).getNamespaceContext());
							
							// Add the parent xpath up to this object - for reporting purposes
							XPathReportCreator.addInheritedXPaths(p, strXpath, (Payload)elementValueObject, field, implementationVersionedName);
							XPathReportCreator.addField(field, strXpath, p, "", implementationVersionedName);
							
							PayloadParser.parsePayload(namespaces, (Payload)elementValueObject, childElement, xmldoc, field.getTypeName());
						} else {
							logger.error("Incorrect configuration for templated field: {}", field.getName());
						}
					}
				} else {
					logger.trace("  ** NO MATCH FOUND!");
				}
			}
		}
		return false;
	}
	
	@Override
	public boolean isPayload() {
		return true;
	}
}
