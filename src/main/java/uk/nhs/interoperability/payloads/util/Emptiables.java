package uk.nhs.interoperability.payloads.util;

import java.util.Collection;
import java.util.Map;

import uk.nhs.interoperability.payloads.util.Emptiable;

/**
 * Static helper methods for {@link Emptiable}s.
 */
public final class Emptiables {
	private Emptiables() {
		// Suppress default constructor
	}
	
	/**
	 * Tests if the specified value is either null or an empty instance
	 * 
	 * @param value The value to test
	 * @return true if <code>value</code> is null or empty, false otherwise
	 */
	public static boolean isNullOrEmpty(final Emptiable value) {
		return value == null || value.isEmpty();
	}
	
	/**
	 * Tests if the specified value is either null or an empty instance
	 * 
	 * @param value The value to test
	 * @return true if <code>value</code> is null or empty, false otherwise
	 */
	public static boolean isNullOrEmpty(final String value) {
		return value == null || value.isEmpty();
	}
	
	/**
	 * Tests if the specified value is either null or an empty instance
	 * 
	 * @param value The value to test
	 * @return true if <code>value</code> is null or empty, false otherwise
	 */
	public static boolean isNullOrEmpty(final CharSequence value) {
		return value == null || value.length() == 0;
	}
	
	/**
	 * Tests if the specified value is either null or an empty instance
	 * <p>
	 * This method does <strong>not</strong> check the state of any contained
	 * entries (e.g. if the collection contains a single null entry this method
	 * will return false).
	 * 
	 * @param value The value to test
	 * @return true if <code>value</code> is null or empty, false otherwise
	 */
	public static boolean isNullOrEmpty(final Collection<?> value) {
		return value == null || value.isEmpty();
	}
	
	/**
	 * Tests if the specified value is either null or an empty instance
	 * <p>
	 * This method does <strong>not</strong> check the state of any contained
	 * entries (e.g. if the collection contains a single null value or key
	 * this method will return false).
	 * 
	 * @param value The value to test
	 * @return true if <code>value</code> is null or empty, false otherwise
	 */
	public static boolean isNullOrEmpty(final Map<?, ?> value) {
		return value == null || value.isEmpty();
	}

	/**
	 * Filter to return the specified value if it is non-null and non-empty, or
	 * null otherwise.
	 * 
	 * @param value The value to check
	 * @return <code>value</code> if non-null and non-empty, or null otherwise
	 */
	public static <T extends Emptiable> T emptyToNull(final T value) {
		return isNullOrEmpty(value) ? null : value;
	}
}
