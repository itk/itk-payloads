package uk.nhs.interoperability.payloads.util.documentation;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.FileUtils;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;

public class XPathReportCreator {
	private static final Logger logger = LoggerFactory.getLogger(XPathReportCreator.class);
	private static boolean _enabled = Boolean.parseBoolean(PropertyReader.getProperty("xpathReportEnabled"));
	private static String reportPath = PropertyReader.getProperty("xpathReportPath");
	private static String reportPrefix = PropertyReader.getProperty("xpathReportPrefix");
	private static ArrayList<String> fileList = new ArrayList<String>();
	
	private static String HEADER = "<html>"
						+ "<head><link rel='stylesheet' href='../css/xpathReport.css'></head>"
						+ "<body><table cellpadding=2 cellspacing=0 border=1>"
						+ "<tr><th class='name'>Name</th>"
					    + "<th class='value'>Example Value</th>"
					    + "<th class='type'>Type</th>"
					    + "<th class='cardinality'>Cardinality</th>"
					    + "<th class='description'>Description</th>"
					    + "<th class='xpath'>XPath</th></tr>";
	private static String FOOTER = "</table></body></html>";
	
	public static boolean enabled() {
		return _enabled;
	}
	
	public static void initialise() {
		if (_enabled) {
			logger.info("Creating XPath Reports in path: {}", reportPath);
			FileUtils.createDirectory(reportPath);
			FileUtils.deleteFilesInDir(reportPath);
		}
		fileList.clear();
	}
	
	private static void createOrAppend(String filename, String val) {
		String filePath = reportPath + "/" + reportPrefix + filename + ".html";
		if (FileUtils.fileExists(filePath)) {
			FileWriter.appendToFile(filePath, val.getBytes());
		} else {
			FileWriter.writeFile(filePath, (HEADER + val).getBytes());
			fileList.add(filePath);
		}
	}
	
	public static void finalise() {
		if (_enabled) {
			// Add footer to all the report files
			for (String filePath : fileList) {
				FileWriter.appendToFile(filePath, FOOTER.getBytes());
			}
			fileList.clear();
		}
	}
	
	/**
	 * Combine the Xpath of the parent payload, with the current field, and store it in
	 * the child payload object. This allows us to maintain a full XPath from the root
	 * of the document, for reporting purposes.
	 * @param parentPayload Parent payload object
	 * @param xpath XPath string
	 * @param field Field
	 * @param childPayload Child payload object
	 * @param implementationVersionedName Versioned template name
	 */
	public static void addInheritedXPaths(Payload parentPayload, String xpath, Payload childPayload, Field field, String implementationVersionedName) {
		if (_enabled) {
			// XPaths
			String val = parentPayload.getParentObjectXPath();
			if (val != null) {
				val = val + "/";
			}
			val = val + xpath;
			childPayload.setParentObjectXPath(val);
			
			// Class names
			ArrayList<String> parentObjectNames = new ArrayList<String>();
			parentObjectNames.addAll(parentPayload.getParentObjectNames());
			//if (implementationVersionedName == null) {
				parentObjectNames.add(field.getTypeName());
			/*} else {
				parentObjectNames.add(implementationVersionedName);
			}*/
			childPayload.setParentObjectNames(parentObjectNames);
		}
	}
	
	public static void addField(Field field, Payload p, String code, String displayName, String implementationVersionedName) {
		addField(field, field.getXpath(), p, code + " : " + displayName, implementationVersionedName);
	}
	
	public static void addField(Field field, String xpath, Payload p, String code, String displayName, String implementationVersionedName) {
		addField(field, xpath, p, code + " : " + displayName, implementationVersionedName);
	}
	
	public static void addField(Field field, Payload p, String value, String implementationVersionedName) {
		addField(field, field.getXpath(), p, value, implementationVersionedName);
	}
	public static void addField(Field field, String xpath, Payload p, String value, String implementationVersionedName) {
		if (_enabled) {
			if (value != null) {
				String type = field.getTypeEnum().name();
				StringBuilder val = new StringBuilder();
				val.append("<tr><td class='").append(type).append(" name'>");
				
				// Full Object Name
				String parentObjectName = "";
				ArrayList<String> parentObjectNames = p.getParentObjectNames();
				for (String name : parentObjectNames) {
					parentObjectName = parentObjectName + name;
				}
				String fullObjectName = parentObjectName + field.getName(); 
				
				val.append(field.getName());
				val.append("</td><td class='").append(type).append(" value'>");
				// Value
				val.append(value);
				val.append("</td><td class='").append(type).append(" type'>");
				// Type
				if ((type.equals("Other")) ||
					(type.equals("Templated"))) {
					
					//if (implementationVersionedName == null) {
						val.append("<a href='").append(reportPrefix)
							.append(parentObjectName)
							.append(field.getTypeName()).append(".html'>")
							.append(field.getTypeName()).append("</a>");
					/*} else {
						val.append("<a href='").append(reportPrefix)
							.append(parentObjectName)
							.append(implementationVersionedName).append(".html'>")
							.append(implementationVersionedName).append("</a>");
					}*/
				} else {
					val.append(field.getTypeName());
				}
				val.append("</td><td class='").append(type).append(" cardinality'>");
				// Cardinality
				if (field.getMaxOccurs()>20) {
					val.append("1..*");
				} else {
					val.append("1..").append(field.getMaxOccurs());
				}
				val.append("</td><td class='").append(type).append(" description'>");
				// Description
				if (field.getDescription() != null) {
					val.append(field.getDescription());
				} else {
					val.append("&nbsp;");
				}
				val.append("</td><td class='").append(type).append(" xpath'>");
				// XPath
				if (p.getParentObjectXPath() != null) {
					val.append(p.getParentObjectXPath()).append("/");
				}
				val.append(xpath);
				val.append("</td></tr>");
				
				//FileWriter.appendToFile(reportPath, val.toString().getBytes());
				createOrAppend(parentObjectName, val.toString());
			}
		}
	}
}
