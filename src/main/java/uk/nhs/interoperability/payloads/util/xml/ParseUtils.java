/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.util.FileLoader;

public class ParseUtils {
	private static final Logger logger = LoggerFactory.getLogger(ParseUtils.class);
	public static String addIncludes(String xml) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		int startInc = 0;
		int endInc = 0;
		int prevEnd = 0;
		
		try {
			while (startInc > -1) {
				// Look for includes
				startInc = xml.indexOf("<include", startInc+1);
				if (startInc > -1) {
					// Find the end of the include
					endInc = xml.indexOf('>', startInc)+1;
					String element = xml.substring(startInc, endInc);
					// Find the file attribute
					String file = getAttribute("file", element);
					
					// Put everything before the include into the output
					out.write(xml.substring(prevEnd, startInc).getBytes());
					prevEnd = endInc;
					
					// Now add the included file content
					if (file != null) {
						String incFileContent = FileLoader.loadFileOnClasspath("/config/" + file);
						if (incFileContent == null) {
							logger.error("Unable to load config file: {}", file);
							return null;
						}
						
						// See if we need to recurse (do this first to avoid name clashes in the search and replace)
						if (incFileContent.contains("<include")) {
							incFileContent = addIncludes(incFileContent);
						}
						
						// Now see if we have some search and replace values
						List<String> searchList = getAttributeMultiple("search", element);
						List<String> replaceList = getAttributeMultiple("replace", element);
						// And do the replacement
						for (int n=0; n<searchList.size(); n++) {
							String search = searchList.get(n);
							String replace = replaceList.get(n);
							if (search == null || replace == null) {
								logger.error("Error carrying out search and replace in config file: {}, trying to search for {}", file, search);
								return null;
							}
							incFileContent = incFileContent.replace(search, replace);
						}
						
						// Now see if we have any conditional sections to include/exclude
						List<String> conditionalSectionList = getAttributeMultiple("includeConditionalSection", element);
						
						// And do the replacement
						for (int n=0; n<conditionalSectionList.size(); n++) {
							boolean shouldInclude = Boolean.parseBoolean(conditionalSectionList.get(n));
							String startSectionText = "%CS"+(n+1)+"-START%";
							String endSectionText = "%CS"+(n+1)+"-END%";
							if (!shouldInclude) {
								// We don't want to include this section, so lets remove it
								// e.g. remove everything between %CS1-START% and %CS1-END%
								int startIdx = incFileContent.indexOf(startSectionText);
								int endIdx = incFileContent.indexOf(endSectionText);
								if (startIdx > -1 && endIdx > -1) {
									incFileContent = incFileContent.substring(0, startIdx) +
													incFileContent.substring(endIdx+(endSectionText.length()));
								}
							} else {
								incFileContent = incFileContent.replace(startSectionText, "");
								incFileContent = incFileContent.replace(endSectionText, "");
							}
						}
						
						// Add our included XML into the output
						out.write(incFileContent.getBytes());
					}
				} else {
					// Now write the remainder to the output
					out.write(xml.substring(endInc).getBytes());
				}
			}
		} catch (IOException e) {
			logger.error("Error adding includes to config file", e);
		}
		return out.toString();
	}
	
	
	protected static List<String> getAttributeMultiple(String attribute, String xml) {
		ArrayList<String> values = new ArrayList<String>();
		
		int searchForIndex=1;
		String result = getAttribute(attribute + searchForIndex, xml);
		while (result != null) {
			values.add(result);
			searchForIndex++;
			result = getAttribute(attribute + searchForIndex, xml);
		}
		return values;
	}
	
	protected static String getAttribute(String attribute, String xml) {
		// Look for attribute
		int start = xml.indexOf(attribute + "=\"");
		if (start > -1) {
			start += (attribute.length()+2);
			// Find the end
			int end = xml.indexOf('\"', start);
			return xml.substring(start, end);
		} else {
			return null;
		}
	}

}
