/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransformManager {
	private static final Logger logger = LoggerFactory.getLogger(TransformManager.class);
	
	// This class would benefit from optimisation, but is kept simple for now
	// It will need some rationalisation w.r.t. template locations at least for RI
	
	public static String doTransform(String tname, String input) {
		return doTransform(tname, input, null);
	}
	
	public static String doTransform(String tname, String input, Map<String, String> parameters) {
        return doTransform(tname, new ByteArrayInputStream(input.getBytes()), parameters);
    }
	
	public static String doTransform(String tname, File input, Map<String, String> parameters) throws FileNotFoundException {
        return doTransform(tname, new FileInputStream(input), parameters);
    }
	
	public static String doTransform(String tname, InputStream input, Map<String, String> parameters) {
	    InputStream tis = TransformManager.class.getResourceAsStream("/"+tname);
        return doTransform(tis, input, parameters);
    }
	
	public static String doTransform(InputStream templateInput, InputStream input, Map<String, String> parameters) {
    	String output = "";
    	
	    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    
	    // Use our custom classpath resolver to load all included files from the classpath 
	    transformerFactory.setURIResolver(new ClassPathURIResolver());
	    
        StreamSource s = new StreamSource(templateInput);
        Templates t = null;
        try {
			t = transformerFactory.newTemplates(s);
	        Transformer tf = t.newTransformer();
	        
	        //Set any stylesheet parameters as appropriate
	        if (parameters != null) {
				for (Map.Entry<String, String> entry: parameters.entrySet()) {;
					logger.trace("Found a stylesheet parameter {}", entry);
					tf.setParameter(entry.getKey(), entry.getValue());
				}
			}
	        
	        StreamSource s2 = new StreamSource(input);
	        StringWriter w = new StringWriter();
	        StreamResult r = new StreamResult(w);

	        tf.transform(s2, r);
	        output = w.getBuffer().toString();
	        logger.trace("Transformation complete");
		} catch (TransformerConfigurationException tce) {
			logger.error("Error in XSLT transformation",tce);
			tce.printStackTrace();
		} catch (TransformerException te) {
			logger.error("Error in XSLT transformation",te);
			te.printStackTrace();
		}

        return output;

     }

}
