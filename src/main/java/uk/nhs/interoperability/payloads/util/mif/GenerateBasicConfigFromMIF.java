/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.mif;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.util.TransformManager;

public class GenerateBasicConfigFromMIF {
	private static final Logger logger = LoggerFactory.getLogger(GenerateBasicConfigFromMIF.class);
	
	/**
	 * @param args Command line arguments (ignored)
	 */
	public static void main(String[] args) {
		String directory = PropertyReader.getProperty("mifDirectory");
		String pattern = PropertyReader.getProperty("mifFilenamePattern");
		
		File[] files = getFiles(directory, pattern);
		for (File file: files) {
			if (!file.exists()) {
				logger.info("Cannot find file: {}", file.getAbsolutePath());
			} else {
				processMif(file);
			}
		}
	}
	
	private static File[] getFiles(final String directory, final String pattern) {
		File dir = new File(directory);
		File[] files = dir.listFiles(
			new FilenameFilter() {
				@Override
			    public boolean accept(File dir, String name) {
			        if (name.equals(pattern))
			        	return true;
					return name.matches(pattern);
			    }
			});
		return files;
	}
	
	private static void processMif(File f) {
		String outputPath = PropertyReader.getProperty("generatedConfigPath");
		String templatePath = PropertyReader.getProperty("mifTransformTemplate");
		
		try {
			FileInputStream templateInput = new FileInputStream(templatePath);
			FileInputStream mifInput = new FileInputStream(f);
			String output = TransformManager.doTransform(templateInput, mifInput, null);
			
			FileWriter.writeFile(outputPath + "/" + changeExtension(f.getName()), output.getBytes());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private static String changeExtension(String input) {
		String output = input;
		int idx = output.indexOf(".");
		if (idx>-1) {
			output = output.substring(0, idx) + ".xml";
		}
		return output;
	}

}
