/**
 * Utility classes - mainly for use by itk-payload framework
 */
package uk.nhs.interoperability.payloads.util;