/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Code;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.CompositionalStatement;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.CompositionalGrammarHandler;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

/**
 * This handler will handle compositional grammar fields.
 * @author Adam Hatherly
 */
public class CompositionalGrammarItemHandler extends CodedItemHandler implements DOMFieldHandler {

	@Override
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent) {
		// We will start by parsing it as if it is just a single coded item
		CodedValue multiCodedValue = (CodedValue)super.parse(field, item, namespaces, p, xmldoc, parent);
		
		// Do some basic sanity checks on the value
		String expectedExpressionCode = field.getExpectedExpressionCode();
		CompositionalGrammarHandler.sanityCheck(multiCodedValue, expectedExpressionCode);
		
		// Parse it into the constituent parts
		CompositionalStatement cs = CompositionalGrammarHandler.parse(multiCodedValue);
		return cs;
	}
	
	@Override
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object itemObject, String xpath, Element parent, Document xmldoc, Field field, Payload payload) {
		CompositionalStatement item = (CompositionalStatement)itemObject;
		// Serialise the compositional statement into a single coded item
		Code newCV = CompositionalGrammarHandler.serialise(item);
		// Now call the superclass to stick them into the XML the same as any other coded value 
		return super.preProcessSerialise(namespaces, newCV, xpath, parent, xmldoc, field, payload);
	}
}
