/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import uk.nhs.interoperability.payloads.FieldType;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.DOMFieldHandler;

public class PayloadSerialiser {
	private static final Logger logger = LoggerFactory.getLogger(PayloadSerialiser.class);
	
	//private static final XMLNamespaceContext NS_CONTEXT = new XMLNamespaceContext();
	private static boolean indent = Boolean.parseBoolean(PropertyReader.getProperty("indentXML"));
	
	/**
	 * Factory to create core XML objects (DocumentBuilder, Transformer, etc)
	 */
	private static final XmlFactory XML_FACTORY;
	static {
		final String reuseXmlFactories = PropertyReader.getProperty("reuseXmlFactories");
		if (reuseXmlFactories == null || Boolean.parseBoolean(reuseXmlFactories)) {
			XML_FACTORY = new CachingXmlFactory();
		} else {
			XML_FACTORY = new XmlFactory();
		}
	}
	
	/**
	 * Factory to create core XML objects (DocumentBuilder, Transformer, etc)
	 * <p>
	 * By default, new instances are returned on each call
	 */
	private static class XmlFactory {
		public Transformer getSerializer() throws TransformerConfigurationException {
			TransformerFactory tf = TransformerFactory.newInstance();
			try {
				if (indent) tf.setAttribute("indent-number", 3);
			} catch (IllegalArgumentException ex) {
				// Skip this - not supported in all DOM libraries
			}
			Transformer serializer = tf.newTransformer();
			//serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
			//serializer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,"users.dtd");
			serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
			if (indent) serializer.setOutputProperty(OutputKeys.INDENT,"yes");
			return serializer;
		}
		
		public DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			return factory.newDocumentBuilder();
		}
		
		public DOMImplementation getDomImplementation() throws ParserConfigurationException {
			return getDocumentBuilder().getDOMImplementation();
		}
	}
	
	/**
	 * Factory which creates a single set of core XML objects per thread.
	 * <p>
	 * The same instance is reused for each call made by a thread
	 * <p>
	 * This class provides a significant performance increase when serialising payloads.
	 * However care should be taken when using this class is an environment where thread-pools
	 * continue to live beyond the lifetime of the application (e.g. in an Application Server).
	 */
	private static class CachingXmlFactory extends XmlFactory {
		private final ThreadLocal<Transformer> serializer = new ThreadLocal<Transformer>();
		private final ThreadLocal<DocumentBuilder> documentBuilder = new ThreadLocal<DocumentBuilder>();
		
		@Override
		public Transformer getSerializer() throws TransformerConfigurationException {
			Transformer serializer = this.serializer.get();
			if (serializer == null) {
				serializer = super.getSerializer();
				this.serializer.set(serializer);
			}
			return serializer;
		}
		
		@Override
		public DocumentBuilder getDocumentBuilder()
				throws ParserConfigurationException {
			DocumentBuilder documentBuilder = this.documentBuilder.get();
			if (documentBuilder == null) {
				documentBuilder = super.getDocumentBuilder();
				this.documentBuilder.set(documentBuilder);
			} else {
				documentBuilder.reset();
			}
			return documentBuilder;
		}
	}
			
	public static String serialise(Payload p, String name) {
		String result = serialise(p, name, null);
		return result;
	}
	
	public static String serialise(Payload p, String name, XMLNamespaceContext parentNamespaces) {
		// Combine namespaces defined in this payload class with any inherited from parent
		
		XMLNamespaceContext namespaces = p.getNamespaceContext();
		if (parentNamespaces != null) {
			namespaces.inheritNamespaces(parentNamespaces);
		}
		
		Document xmldoc = createEmptyDOM(namespaces, name);
		// Root element.
		Element root = xmldoc.getDocumentElement();
		
		addNamespaceDeclarations(namespaces, root);
		
		serialiseFieldsInPayload(namespaces, p, root, xmldoc);

		// Now serialise
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		serialiseDOMtoStream(root, bos);
		
		return bos.toString();
	}
	
	public static Document createEmptyDOM(XMLNamespaceContext namespaces, String name) {
		Document xmldoc = null;
		try {
			DOMImplementation impl = XML_FACTORY.getDomImplementation();
			// Document
			if (namespaces == null) {
				xmldoc = impl.createDocument(null, name, null);
			} else {
				xmldoc = impl.createDocument(namespaces.getDocumentNamespace(),
				namespaces.getDocumentNamespacePrefix() + ":" + name, null);
			}
		} catch (ParserConfigurationException e) {
			logger.error("	Unable to create XML DOM", e);
		} catch (DOMException e) {
			logger.error("	Unable to create XML DOM - Field name: {}", name, e);
		}
		return xmldoc;
	}
	
	public static void serialiseDOMtoStream(Node xmldoc, OutputStream out) {
		PrintWriter pw = new PrintWriter(out);
		try {
			
			// Serialisation through Tranform.
			DOMSource domSource = new DOMSource(xmldoc);
			StreamResult streamResult = new StreamResult(pw);
			
			Transformer serializer = XML_FACTORY.getSerializer();
			
			serializer.transform(domSource, streamResult);
		} catch (TransformerException e1) {
			logger.error("	Error generating simple XML: ", e1);
		}
	}
	
	public static void serialiseFieldsInPayload(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc) {
		
		Map<String, Field> fieldDefinitions = p.getFieldDefinitions();
		for (String fieldName : fieldDefinitions.keySet()) {
			Field field = fieldDefinitions.get(fieldName);
			FieldType fieldType = field.getTypeEnum();
			logger.trace("===== Processing field: {} - type specified as: {}", field.getName(), fieldType.name());

			DOMFieldHandler handler = fieldType.getHandler();
			
			String xpath = field.getXpath();
			String strValue = null;
			Object fieldValue = p.getValue(field.getName());
			
			// First, see if the field should be included in the output
			boolean outputField = handler.outputField(field, p);
			
			if (outputField) {
				if (field.getMaxOccurs() > 1) {
					// We are expecting an ArrayList with one or more items
					ArrayList list = (ArrayList)fieldValue;
					if (list != null) {
						for (Object fieldListItem : list) {
							
							// Do some pre-processing on the field if required
							boolean continueProcessing = 
									handler.preProcessSerialise(namespaces, fieldListItem, xpath, parent, xmldoc, field, p);

							// Sometimes the pre-processing may handle the node creation itself (e.g. for coded items),
							// in which case we don't need to continue here
							if (continueProcessing) {
								// If the field is not blank, create a new node or nodes for it to go into
								if (!handler.isItemBlank(field, fieldListItem, true)) {
									createNode(namespaces, xpath, parent, xmldoc, fieldListItem, handler, field, p);
								}
							}
						}
					}
				} else {
					// Do some pre-processing on the field if required
					boolean continueProcessing =
							handler.preProcessSerialise(namespaces, fieldValue, xpath, parent, xmldoc, field, p);
					
					// Sometimes the pre-processing may handle the node creation itself (e.g. for coded items),
					// in which case we don't need to continue here
					if (continueProcessing) {
						// If the field is not blank, create a new node or nodes for it to go into
						if (!handler.isItemBlank(field, fieldValue, false)) {
							createNode(namespaces, xpath, parent, xmldoc, fieldValue, handler, field, p);
						}
					}
				}
			}
		}
	}
	
	public static Element createNode(XMLNamespaceContext namespaces, String xpath, Element parent, Document xmldoc, Object objValue, DOMFieldHandler handler, Field definition, Payload p) {
		//Logger.trace("	Create called for field: " + definition.getName() + " XPATH PROVIDED IS: " + xpath + " and parent node is " + parent.getNodeName());
		/*if (definition.getName().equals("ODSOrgID")) {
			ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
			serialiseDOMtoStream(xmldoc, bos);
			Logger.trace(bos.toString());
		}*/
		if (XPaths.isXPathAHierarchy(xpath)) {
			// We have been passed a hierarchy, so create/find the top element and then recurse
			return createParentNode(namespaces, xpath, parent, xmldoc, objValue, handler, definition, p);
		} else {
			return createLeafNode(namespaces, xpath, parent, xmldoc, objValue, handler, definition, p);
		}
	}
	
	/**
	 * Pattern which matches simple XPath expressions of the form prefix:tag
	 */
	private static final Pattern OPTIMISED_XPATH_PATTERN = Pattern.compile("(\\w+):(\\w+)");
	
	/**
	 * Optimised version of {@link XPaths#findNodeFromXPath(XMLNamespaceContext, String, Element)}.
	 * <p>
	 * For simple searches it can be much faster to traverse the DOM manually than to use the XPath engine.
	 * If <code>firstElement</code> matches {@link #OPTIMISED_XPATH_PATTERN} a manual search of the DOM is
	 * performed, otherwise the standard {@link XPaths} method is used.
	 * 
	 * @see #OPTIMISED_XPATH_PATTERN
	 */
	private static Element findNodeFromXPath(final XMLNamespaceContext namespaces, final String firstElement, final Element parent) {
		// Try a search through the DOM
		// In simple cases this is many times faster than using XPath
		final Matcher matcher = OPTIMISED_XPATH_PATTERN.matcher(firstElement);
		if (matcher.matches()) {
			final String prefix = matcher.group(1);
			final String namespaceURI = namespaces.getNamespaceURI(prefix);
			final String nodeName = matcher.group(2);
			
			// only optimise if the namespaceUri is known
			if (namespaceURI != null) {				
				final NodeList nodes = parent.getChildNodes();
				for (int index = 0; index < nodes.getLength(); index++) {
					final Element child = (Element)nodes.item(index);
					if (nodeName.equals(child.getNodeName()) && namespaceURI.equals(child.getNamespaceURI())) {
						return child;
					} else if (firstElement.equals(child.getNodeName())) {
						// Fall-back in case the element was created as prefix:name
						// without the namespace
						return child;
					}
				}
				
				// No matching element
				return null;
			}
		}
		
		// Fallback to XPath engine
		return XPaths.findNodeFromXPath(namespaces, firstElement, parent);
	}
	
	public static Element createParentNode(XMLNamespaceContext namespaces, String xpath, Element parent, Document xmldoc, Object objValue, DOMFieldHandler handler, Field definition, Payload p) {
		// We have been passed a hierarchy, so create/find the top element and then recurse
		//Logger.trace("	=> Complex path found: " + xpath);
		String firstElement = XPaths.getFirstElementOnXPath(xpath);
		String elementName = XPaths.getElementNameWithoutCondition(firstElement);
		String remainder = xpath.substring(firstElement.length()+1);
		
		//Logger.trace("Splitting XPath - first part '" + firstElement + "' , remainder '" + remainder + "' , element name '" + elementName + "'");
		Element existingNode = findNodeFromXPath(namespaces, firstElement, parent);		
		
		if (existingNode == null) {
			// Create this element, then recurse
			//Logger.trace("No existing element, so create a new " + elementName + " element");
			Element e = createNewElement(namespaces, elementName, xmldoc);
			// In a few rare cases, we might want to specifically add this node as the first
			// child rather than the next child in sequence 
			if (definition.isAddAtStart()) {
				parent.insertBefore(e, parent.getFirstChild());
			} else {
				parent.appendChild(e);
			}
			return createNode(namespaces, remainder, e, xmldoc, objValue, handler, definition, p);
		} else {
			// Element already exists, get it and recurse
			logger.trace("Using existing {} element", elementName);
			return createNode(namespaces, remainder, existingNode, xmldoc, objValue, handler, definition, p);
		}
	}
	
	public static Element createLeafNode(XMLNamespaceContext namespaces, String xpath, Element parent, Document xmldoc, Object objValue, DOMFieldHandler handler, Field definition, Payload p) {
		String value = handler.stringValue(definition, p, objValue, namespaces, parent, xmldoc);
		
		logger.trace("	Creating leaf node for field {} with value: {}", definition.getName(), value);
		xpath = XPaths.getElementNameWithoutCondition(xpath);
		if (xpath.startsWith("@")) {
			// We have been passed an attribute to set
			if (xpath.contains(":")) {
				// Attribute name has a namespace prefix
				String[] nameParts = xpath.substring(1).split(":");
				String uri = namespaces.getNamespaceURI(nameParts[0]);
				//Logger.trace("Attempting to add attribute: '" + xpath.substring(1) + "' with namespace prefix: '" + nameParts[0] + "' - using URI: " + uri + ", with value: " + value + ", adding to parent: " + parent.getNodeName());
				parent.setAttributeNS(uri, xpath.substring(1), value);
			} else {
				parent.setAttributeNS(null, xpath.substring(1), value);
			}
			return parent;
		} else if(xpath.equals(".")) {
			// The field want to set the parent node's text value
			if (value != null) {
				parent.setTextContent(value);
			}
			return parent;
		} else {
			// We have been passed an element to create and set a text value for
			Element e = createNewElement(namespaces, xpath, xmldoc);
			if (value != null) {
				Node n = xmldoc.createTextNode(value);
				e.appendChild(n);
			}
			parent.appendChild(e);
			handler.postProcessSerialise(objValue, namespaces, e, xmldoc);
			return e;
		}
	}
	
	public static Element createNewElement(XMLNamespaceContext namespaces, String name, Document xmldoc) {
		logger.trace("CREATE NODE: {}", name);
		Element e = null;
		try {
			if (name.contains(":")) {
				// Element name has a namespace prefix
				String[] nameParts = name.split(":");
				String uri = namespaces.getNamespaceURI(nameParts[0]);
				e = xmldoc.createElementNS(uri, name);
			} else {
				
				try {
					e = xmldoc.createElement(name);
				} catch (Exception ex) {
					logger.error("Error creating element: {}", name, ex);
				}
			}
			return e;
		} catch (DOMException ex) {
			logger.error("	There was an issue with namespaces when attempting to create an element - the name supplied was: {}", name, ex);
			return null;
		}
	}
	
	public static void addNamespaceDeclarations(XMLNamespaceContext namespaces, Element root) {
		Map<String, String> namespacelist = namespaces.getAllNamespaces();
		for (String namespacePrefix : namespacelist.keySet()) {
			if (namespacePrefix.equals("")) {
				root.setAttribute("xmlns", namespacelist.get(namespacePrefix));
			} else {
				root.setAttribute("xmlns:" + namespacePrefix, namespacelist.get(namespacePrefix));
			}
		}
	}
}
