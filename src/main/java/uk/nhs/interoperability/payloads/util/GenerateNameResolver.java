/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util;

import java.util.ArrayList;

import uk.nhs.interoperability.payloads.metadata.PayloadClassInfo;

public class GenerateNameResolver implements GlobalConstants {
	protected static void generateRootNodeNameResolver(ArrayList<PayloadClassInfo> classInfoList, String srcPath) {
		srcPath = srcPath + DOMAINOBJECTFACTORY + "/RootNodeNameResolver.java";
		String content =
			"/**\n" +
			" * Note: This class is generated automatically when GenerateDomainObjects is run\n" +
			" * @author Adam Hatherly\n" +
			" */\n" +
			"package uk.nhs.interoperability.payloads;\n\n" +
			"public class RootNodeNameResolver {\n";
		
		// Generate the factory method
		content = content +
			"	public static String getRootNodeName(String versionedName, String packg) {\n" +
			"   	if (packg.endsWith(\".\")) {\n" +
			"			packg = packg.substring(0, packg.length()-1);\n" +
			"   	};";

		for (PayloadClassInfo info : classInfoList) {
			String packg = info.getPackageName();
			String rootNode = info.getRootNode();
			String vn = info.getVersionedName(); 
			
			if (rootNode != null && vn != null) {
				content = content +
				"		if (versionedName.equals(\"" + vn + "\") && packg.equals(\"" + packg + "\")) {\n" +
				"			return \"" + rootNode + "\";\n" +
				"		}\n";
			}
		}

		content = content +
			"		return null;\n" +
			"	}\n";
		
		content = content +
			"}";
		FileWriter.writeFile(srcPath, content.getBytes());
	}
}
