/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.PropertyReader;

public class SchemaValidator {
	private static final Logger logger = LoggerFactory.getLogger(SchemaValidator.class);
	
	public static boolean validate(String xml, File xsd) throws SAXException, IOException {
		//URL schemaFile = new URL("http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd");
		Source xmlFile = new StreamSource(new ByteArrayInputStream(xml.getBytes()));
		SchemaFactory schemaFactory = SchemaFactory
		    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(xsd);
		Validator validator = schema.newValidator();
		try {
		  validator.validate(xmlFile);
		  return true;
		} catch (SAXException e) {
		  throw e;
		}
	}
	
	public static boolean testAgainstTemplatedSchema(String xmlToTest, File xsd, File xsl) throws SAXException, IOException, InterruptedException {
		// HACK: The tansform from on-the-wire to templated CDA provided by the
		// messaging team requires an XSLT 2.0 processor (Saxon to be specific). As this
		// would break all the other transforms used in this library, we are forking
		// a separate java command line process to do this transform. This means there
		// has to be a writable directory to put the file into, and read the transformed
		// file back from. By default we will use the working directory as this is only
		// a unit test dependency. This should be the root project directory.
		File baseDir = new File(System.getProperty("java.io.tmpdir"));
		String tempPath = baseDir.getAbsolutePath() + "/";
		String saxonPath = PropertyReader.getProperty("saxonJarPath");
		String xslPath = xsl.getAbsolutePath();
		
		FileWriter.writeFile(tempPath+"temp.xml", xmlToTest.getBytes());
		
		String command = "java -jar " + saxonPath + " -s:"+tempPath+"temp.xml -xsl:" + xslPath + " -o:"+tempPath+"templated.xml";
		logger.info(command);
		
		Runtime rt = Runtime.getRuntime();
		Process pr;
		try {
			pr = rt.exec(command);
			// Wait until the process completes
			pr.waitFor();
			if (pr.exitValue() != 0) {
				logger.error("Error running XSLT transformation using SAXON");
			}
		} catch (IOException e) {
			logger.error("Error when trying to transform from on-the-wire to templated format", e);
			throw e;
		} catch (InterruptedException e) {
			logger.error("Error when trying to transform from on-the-wire to templated format", e);
			throw e;
		}
		
		String templatedXML = FileLoader.loadFile(tempPath+"templated.xml");
		return validate(templatedXML, xsd);
	}
}
