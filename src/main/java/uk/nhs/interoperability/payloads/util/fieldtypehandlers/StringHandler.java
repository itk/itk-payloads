/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.documentation.XPathReportCreator;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

public class StringHandler extends AbstractFieldHandler implements DOMFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(StringHandler.class);
	
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object item, String xpath, Element parent, Document xmldoc, Field field, Payload payload) {
		// Do nothing
		return true;
	}
	
	@Override
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		// Do nothing
		return true;
	}

	@Override
	public String stringValue(Field definition, Payload p, Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) {
		if (val == null) {
			return null;
		} else {
			return val.toString();
		}
	}
	
	@Override
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent) {
		logger.trace("In Parse method for String");
		
		String val = null;
		if (field.getMaxOccurs() > 1) {
			// If we are in a list we can't be an attribute, so we can just use getTextContent
			val = item.getTextContent();
		} else {
			// If we are not in a list, we can look up the value directly using the xpath
			val = XPaths.findStringFromXPath(field.getCompiledXpath(namespaces), parent, field.getXpath());
		}
		logger.trace("String value: {}", val);
		// If enabled, output an entry in an XPath Report
		XPathReportCreator.addField(field, p, val, null);
		return val;
	}
}
