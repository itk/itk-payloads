/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.AbstractPayload;
import uk.nhs.interoperability.payloads.Code;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.CompositionalStatement;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;

/**
 * Compositional statements are in this format (without carriage returns):
 * 	code code=”810301000000103
 * 				:810311000000101
 * 				=373942005
 * 				,810321000000107
 * 				=394586005” 
 * 			codeSystem=”2.16.840.1.113883.2.1.3.2.4.15.1”
 * 			displayName=”810301000000103|Clinical document descriptor|
 * 						 :810311000000101|Type of clinical document|
 * 						 =373942005|Discharge summary|
 * 						 ,810321000000107|Care setting of clinical document|
 * 						 =394586005|Gynaecology|”
 * 
 * Codes are parsed as follows:
 * expression:attributeCode=valueCode[,attributeCode=valueCode]*
 * 
 * Display names are parsed as follows:
 * expression|displayName|:attributeCode|displayName|=valueCode|displayName|[,attributeCode|displayName|=valueCode|displayName|]*
 * 
 * @author Adam Hatherly
 *
 */
public class CompositionalGrammarHandler {
	private static final Logger logger = LoggerFactory.getLogger(CompositionalGrammarHandler.class);
	private static final String EXPRESSION_SEPARATOR = ",";
	private static final String REFINEMENT_SEPARATOR = ":";
	private static final String DISPLAY_NAME_SEPARATOR = "\\|";
	private static final String DISPLAY_NAME_SEPARATOR_NOESCAPE = "|";
	private static final String NAME_VALUE_SEPARATOR = "=";
	
	public static CompositionalStatement parse(Code multiCodedValue) {
		String multiCode = multiCodedValue.getCode();
		String multiDisplayName = multiCodedValue.getDisplayName();
		
		// Create a compositional statement to put it all in
		CompositionalStatement compositionalStatement = new CompositionalStatement();
		
		// Now, we will split the code into the individual coded items
		
		// Focus Code
		String[] codeRefinements = multiCode.split(REFINEMENT_SEPARATOR);
		String[] displayNameRefinements = multiDisplayName.split(REFINEMENT_SEPARATOR);
		
		if (codeRefinements.length != 2 || displayNameRefinements.length != 2) {
			logger.error("Compositional statement codes and display names appear to be invalid");
			return null;
		}
		
		Code focusCode = new CodedValue(codeRefinements[0],
										getDisplayName(displayNameRefinements[0]),
										CompositionalStatement.OID);
		compositionalStatement.setFocusExpression(focusCode);
		
		// Refinement
		String[] refinementCodes = codeRefinements[1].split(EXPRESSION_SEPARATOR);
		String[] refinementDisplayNames = displayNameRefinements[1].split(EXPRESSION_SEPARATOR);
		
		if (refinementCodes.length != refinementDisplayNames.length) {
			logger.error("Compositional statement codes and display names appear to be invalid - the number of codes differs from the number of display names");
			return null;
		}
		
		// Attributes
		for (int attributeNumber=0; attributeNumber < refinementCodes.length; attributeNumber++) {
			String attribute = refinementCodes[attributeNumber];
			String attributeDisplay = refinementDisplayNames[attributeNumber];
			
			// Split into name and value
			String[] attributeParts = attribute.split(NAME_VALUE_SEPARATOR);
			String[] attributeDisplayParts = attributeDisplay.split(NAME_VALUE_SEPARATOR);
			
			if (attributeParts.length != 2 || attributeDisplayParts.length != 2) {
				logger.error("Compositional statement appears to be invalid - not all attributes have name and value codes");
				return null;
			}
			
			Code attributeNameCode = new CodedValue(attributeParts[0],
													getDisplayName(attributeDisplayParts[0]),
													CompositionalStatement.OID);
			Code attributeValueCode = new CodedValue(attributeParts[1],
													 getDisplayName(attributeDisplayParts[1]),
													 CompositionalStatement.OID);
			compositionalStatement.addAttributeCodeAndValueCode(attributeNameCode, attributeValueCode);
		}
		
		return compositionalStatement;
	}
	
	/**
	 * Returns a coded value representing the full compositional statement 
	 * @param compositionalStatement Compositional statement to serialise
	 * @return Code representing the full compositional statement
	 */
	public static Code serialise(CompositionalStatement compositionalStatement) {
		// expression:attributeCode=valueCode[,attributeCode=valueCode]*
		// expression|displayName|:attributeCode|displayName|=valueCode|displayName|[,attributeCode|displayName|=valueCode|displayName|]*
		StringBuilder code = new StringBuilder();
		StringBuilder displayName = new StringBuilder();
		String oid = CompositionalStatement.COMPOSITIONAL_OID;
		
		// Focus expression
		code.append(compositionalStatement.getFocusExpression().getCode());
		displayName
			.append(compositionalStatement.getFocusExpression().getCode()).append(DISPLAY_NAME_SEPARATOR_NOESCAPE)
			.append(compositionalStatement.getFocusExpression().getDisplayName()).append(DISPLAY_NAME_SEPARATOR_NOESCAPE);
	
		// Refinements
		boolean first = true;
		for (Code attributeCode : compositionalStatement.getRefinementCodes().keySet()) {
			Code attributeValue = compositionalStatement.getRefinementCodes().get(attributeCode);
			if (first) {
				code.append(REFINEMENT_SEPARATOR);
				displayName.append(REFINEMENT_SEPARATOR);
				first = false;
			} else {
				code.append(EXPRESSION_SEPARATOR);
				displayName.append(EXPRESSION_SEPARATOR);
			}
			// Code
			code.append(attributeCode.getCode()).append(NAME_VALUE_SEPARATOR).append(attributeValue.getCode());
			// Display Name
			displayName
					.append(attributeCode.getCode()).append(DISPLAY_NAME_SEPARATOR_NOESCAPE)
					.append(attributeCode.getDisplayName()).append(DISPLAY_NAME_SEPARATOR_NOESCAPE)
					.append(NAME_VALUE_SEPARATOR)
					.append(attributeValue.getCode()).append(DISPLAY_NAME_SEPARATOR_NOESCAPE)
					.append(attributeValue.getDisplayName()).append(DISPLAY_NAME_SEPARATOR_NOESCAPE);
		}
		
		CodedValue result = new CodedValue(code.toString(), displayName.toString(), oid);
		return result;
	}
	
	/**
	 * Takes a value and display name in the form:
	 * 373942005|Discharge summary|
	 * And returns just the display name
	 * @param val
	 * @return display name
	 */
	protected static String getDisplayName(String val) {
		String[] valueParts = val.split(DISPLAY_NAME_SEPARATOR);
		if (valueParts.length != 2) {
			logger.error("The display name for part of this compositional statement appear to be invalid");
			return null;
		} else {
			return valueParts[1];
		}
	}
	
	/**
	 * Do some basic checks on the compositional statement to make sure it looks valid
	 * before we try to parse it
	 * @param multiCodedValue Coded value contains the compositional statement codes
	 * @param expectedExpressionCode Expected focus code (from field config)
	 */
	public static boolean sanityCheck(Code multiCodedValue, String expectedExpressionCode) {
		// First, lets do some checks to make sure our compositional statement is valid
		String multiCode = multiCodedValue.getCode();
		if (multiCode == null) {
			logger.error("Invalid SNOMED CT compositional statement - the code value appears to be null");
			return false;
		}
		if (expectedExpressionCode != null) {
			if (!multiCode.startsWith(expectedExpressionCode)) {
				logger.error("Invalid SNOMED CT compositional statement - the value for this field must begin with {}", expectedExpressionCode);
				return false;
			}
		}
		if (!multiCodedValue.getOID().equals(CompositionalStatement.COMPOSITIONAL_OID)) {
			logger.error("Invalid SNOMED CT compositional statement - the OID must be {}", CompositionalStatement.COMPOSITIONAL_OID);
			return false;
		}
		return true;
	}
}
