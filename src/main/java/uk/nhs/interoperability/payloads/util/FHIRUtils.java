/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util;

import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.UUID;

/**
 * Utility class for generating UUIDs.
 * IMPORTANT NOTE: The HL7 specifications specify that DCE (Type 2) UUIDs should be used. Type 2
 * UUIDs are not generally used in any modern systems, and have not been for some time. This
 * library will therefore use the standard Type 4 (Random) UUIDs that are supported in almost
 * all systems. All message examples provided by the UK messaging team use Type 4 UUIDs.
 * 
 * @author Adam Hatherly
 */
public class FHIRUtils {
	public static String stripOIDPrefix(String in) {
		if (in == null) {
			return null;
		}
		if (in.startsWith("urn:oid:")) {
			return in.substring(8);
		} else {
			return in;
		}
	}
	
	public static String addOIDPrefix(String in) {
		if (in.startsWith("urn:oid:")) {
			return in;
		} else {
			return "urn:oid:"+in;
		}
	}
}
