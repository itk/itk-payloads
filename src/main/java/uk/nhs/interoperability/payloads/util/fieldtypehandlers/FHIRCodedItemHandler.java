/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.Code;
import uk.nhs.interoperability.payloads.CodedValue;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.FHIRUtils;
import uk.nhs.interoperability.payloads.util.xml.PayloadSerialiser;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

/**
 * Experimental. Assumes OIDs are used to specify code system.
 * @author Adam Hatherly
 */
public class FHIRCodedItemHandler extends AbstractFieldHandler implements DOMFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(FHIRCodedItemHandler.class);
	
	@Override
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent) {
		
		if (item == null) {
			return null;
		}
		XPathExpression codeXpath = field.getCompiledXpathCode(namespaces);
		XPathExpression displayNameXpath = field.getCompiledXpathDisplayName(namespaces);
		XPathExpression codeSystemXpath = field.getCompiledXpathCodeSystem(namespaces);
		//XPathExpression referenceXpath = field.getCompiledXpathReference(namespaces);
		
		// Find the three attributes of the coded item
		String code = XPaths.findStringFromXPath(codeXpath, item, "x:value/@value");
		String displayName = XPaths.findStringFromXPath(displayNameXpath, item, "x:label/@value");
		//String reference = XPaths.findStringFromXPath(referenceXpath, item, "x:originalText/x:reference/@value");
		String codeSystem = null;
		//if (field.getDeriveOIDFrom() == null) {
			codeSystem = XPaths.findStringFromXPath(codeSystemXpath, item, "x:system/@value");
		//} else {
			// The OID is derived from another coded field - try to populate it
		//	String fieldToDeriveFrom = field.getDeriveOIDFrom();
		//	Object val = p.getValue(fieldToDeriveFrom);
		//	if (val != null) {
		//		Code codeVal = (Code)val;
		//		codeSystem = codeVal.getOID();
		//	}
		//}
		
		// Strip the oid prefix
		code = FHIRUtils.stripOIDPrefix(code);
			
		logger.trace("  Processed FHIR coded item: {}, found value: {}", field.getName(), code);
		if (code != null) {
			//p.setValue(field.getName(), new CodedValue(code, displayName, codeSystem));
			CodedValue newCV = new CodedValue(code, displayName, codeSystem);
			//if (reference != null) {
			//	newCV.setReference(reference);
			//}
			return newCV;
		}
		return null;
	}
	
	@Override
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object itemObject, String xpath, Element parent, Document xmldoc, Field field, Payload payload) {
		Code item = (Code)itemObject;
		logger.trace("	Serialising FHIR coded item - code = {}", item.getCode());
		boolean suppressCodeSystem = field.isSuppressCodeSystem();
		// First create the @code node:  
		Field stringValueField = new Field();
		stringValueField.setType("String");
		stringValueField.setName("Coded Item Value");
		
		PayloadSerialiser.createNode(namespaces, xpath + "/x:value/@value", parent, xmldoc, item.getCode(), new StringHandler(), stringValueField, payload);
		String displayName = item.getDisplayName();
		//String reference = item.getReference();
		if (displayName != null) {
			PayloadSerialiser.createNode(namespaces, xpath + "/x:label/@value", parent, xmldoc, displayName, new StringHandler(), stringValueField, payload);
		}
		//if (reference != null) {
		//	PayloadSerialiser.createNode(namespaces, xpath + "/x:originalText/x:reference/@value", parent, xmldoc, reference, new StringHandler(), stringValueField, payload);
		//}
		
		String oid = item.getOID();
		if (oid != null) {
			// Add oid prefix
			oid = FHIRUtils.addOIDPrefix(oid);
			if (!suppressCodeSystem) {
				PayloadSerialiser.createNode(namespaces, xpath + "/x:system/@value", parent, xmldoc, oid, new StringHandler(), stringValueField, payload);
			}
		} else {
			oid = field.getLocalCodeOID();
			if (oid != null) {
				// Add oid prefix
				oid = FHIRUtils.addOIDPrefix(oid);
				if (!suppressCodeSystem) {
					PayloadSerialiser.createNode(namespaces, xpath + "/x:system/@value", parent, xmldoc, oid, new StringHandler(), stringValueField, payload);
				}
			}
		}
		return false;
	}

	@Override
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		// Do nothing
		return true;
	}
	
	@Override
	public String stringValue(Field definition, Payload p, Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) {
		return null;
	}

	@Override
	public Element getElement(XPathExpression xpath, Element parent, String strXpath) {
		return XPaths.findNodeFromXPath(xpath, parent, strXpath);
	}
	
}
