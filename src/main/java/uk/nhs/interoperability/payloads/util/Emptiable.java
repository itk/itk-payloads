package uk.nhs.interoperability.payloads.util;

/**
 * Interface for classes where non-null instances can be empty
 * <p>
 * This is similar in concept to {@link java.util.Collection#isEmpty()} or {@link java.lang.String#isEmpty()}.
 */
public interface Emptiable {
	/**
	 * Tests if this object is empty
	 */
	boolean isEmpty();
}
