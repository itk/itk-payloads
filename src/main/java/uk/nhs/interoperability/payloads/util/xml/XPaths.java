/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import uk.nhs.interoperability.payloads.Payload;

/**
 * Utility class for dealing with XPaths
 * @author Adam Hatherly
 */
public class XPaths {
	private static final Logger logger = LoggerFactory.getLogger(XPaths.class);
	
	private static XPathFactory xPathfactory = XPathFactory.newInstance();
	//private static final XMLNamespaceContext NS_CONTEXT = new XMLNamespaceContext();

	
	/**
	 * Finds an element using an XPath string, starting from the element provided
	 * @param namespaces Namespaces context for XPath evaluation
	 * @param strXpath XPath to search for
	 * @param parent Element to search from
	 * @return Element matching xpath, or null
	 */
	public static Element findNodeFromXPath(XMLNamespaceContext namespaces, String strXpath, Element parent) {
		try {
			//Logger.trace("   => Attempting to evaluate xpath: " + strXpath + " against parent node: " + parent.getLocalName() + " with namespace "+ parent.getNamespaceURI());
			XPath xpath = xPathfactory.newXPath();
			xpath.setNamespaceContext(namespaces);
			
			XPathExpression expr = xpath.compile(strXpath);
			Element result = findNodeFromXPath(expr, parent, strXpath);
			return result;
		} catch (XPathExpressionException e) {
			logger.error("	Error evaluating XPath: {}", strXpath, e);
		}
		return null;
	}
	
	/**
	 * Finds an element using a compiled XPath, starting from the element provided
	 * @param xpath XPath to search for
	 * @param parent Element to search from
	 * @param strXpath String representation of XPath
	 * @return Element matching xpath, or null
	 */
	public static Element findNodeFromXPath(XPathExpression xpath, Element parent, String strXpath) {
		try {
			//Logger.trace("   => Attempting to evaluate xpath: " + strXpath + " against parent node: " + parent.getLocalName() + " with namespace "+ parent.getNamespaceURI());
			Element result = (Element)xpath.evaluate(parent, XPathConstants.NODE);
			return result;
		} catch (XPathExpressionException e) {
			logger.error("	Error evaluating compiled xpath", e);
		}
		return null;
	}

	
	/**
	 * Finds an element using an XPath string, starting from the element provided
	 * @param namespaces Namespace context to evaluate XPath
	 * @param strXpath XPath to search for
	 * @param parent Element to search from
	 * @return Element matching xpath, or null
	 */
	public static String findStringFromXPath(XMLNamespaceContext namespaces, String strXpath, Element parent) {
		if (strXpath == null) {
			return null;
		}
		try {
			XPath xpath = xPathfactory.newXPath();
			xpath.setNamespaceContext(namespaces);
			
			XPathExpression expr = xpath.compile(strXpath);
			String result = findStringFromXPath(expr, parent, strXpath);
			if (result.length()==0) {
				logger.trace("	Found EMPTY VALUE!! (xpath was {})", strXpath);
			} else {
				logger.trace("	Found value '{}'", result);
			}
			return result;
		} catch (XPathExpressionException e) {
			logger.error("	Error evaluating XPath: {}", strXpath, e);
		}
		return null;
	}
	
	/**
	 * Finds an element using a compiled xpath, starting from the element provided
	 * @param xpath Compiled XPath to search for
	 * @param parent Element to search from
	 * @param strXpath String version of XPath
	 * @return Element matching xpath, or null
	 */
	public static String findStringFromXPath(XPathExpression xpath, Element parent, String strXpath) {
		try {
			//Logger.trace("   => Attempting to evaluate xpath: " + strXpath);
			if (parent == null) {
				logger.trace("	NULL PARENT! ");
			}/* else {
				Logger.trace("      against parent node: " + parent.getLocalName());
				Logger.trace("      with namespace "+ parent.getNamespaceURI());
			}*/
			String result = (String)xpath.evaluate(parent, XPathConstants.STRING);
			if (result.length()==0) {
				logger.trace("	Found EMPTY VALUE!! (xpath was {})", strXpath);
				return null;
			} else {
				//Logger.trace("      Found value '" + result + "'");
			}
			return result;
		} catch (XPathExpressionException e) {
			logger.error("	Error evaluating compiled xpath", e);
		}
		return null;
	}
	
	/**
	 * Finds a list of elements using an XPath string, starting from the element provided
	 * @param namespaces Namespace context to evaluate XPath
	 * @param strXpath XPath to search for
	 * @param parent Element to search from
	 * @return List of element values matching xpath, or null
	 */
	public static ArrayList<Element> findListFromXPath(XMLNamespaceContext namespaces, String strXpath, Element parent) {
		try {
			XPath xpath = xPathfactory.newXPath();
			xpath.setNamespaceContext(namespaces);
			XPathExpression expr;
			expr = xpath.compile(strXpath);
			return findListFromXPath(expr, parent, strXpath);
		} catch (XPathExpressionException e) {
			logger.error("	Error evaluating xpath: {}", strXpath, e);
			return null;
		}
	}
	
	/**
	 * Finds a list of elements using a compiled xpath, starting from the element provided
	 * @param xpath Compiled XPath to search for
	 * @param parent Element to search from
	 * @param strXpath String version of XPath
	 * @return List of element values matching xpath, or null
	 */
	public static ArrayList<Element> findListFromXPath(XPathExpression xpath, Element parent, String strXpath) {
		try {
			//Logger.trace("   => Attempting to evaluate xpath: " + strXpath + " against parent node: " + parent.getLocalName() + " with namespace "+ parent.getNamespaceURI());
			ArrayList<Element> out = new ArrayList<Element>();
			NodeList list = (NodeList)xpath.evaluate(parent, XPathConstants.NODESET);
			for (int n=0; n<list.getLength(); n++) {
				Element e = (Element)list.item(n); 
				out.add(e);
			}
			return out;
		} catch (XPathExpressionException e) {
			logger.error("	Error evaluating compiled xpath", e);
		}
		return null;
	}
	
	/**
	 * When an XPath string contains multiple elements (e.g. element[1]/child[1]/leaf), this method returns the first element
	 * @param element XPath to split
	 * @return First element (including any conditions)
	 */
	public static String getFirstElementOnXPath(String element) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int n=0;
		boolean inCondition = false;
		while (n<element.length()) {
			char c = element.charAt(n);
			if (c == '[') {
				inCondition = true;
			} else if (c == ']') {
				inCondition = false;
			}
			if ((!inCondition) && (c == '/')) {
				return out.toString();
			} else {
				out.write(c);
			}
			n++;
		}
		return out.toString();
	}
	
	/**
	 * Strips any namespace prefixes from XPaths to make parsing the document easier
	 * @param in XPath expression
	 * @param prefix Prefix to remove
	 * @return XPath expression with the specified namespac prefixes removed
	 * @deprecated
	 */
	public static String stripNamespacePrefix(String in, String prefix) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayOutputStream elementName = new ByteArrayOutputStream();		
		int n=0;
		try {
			while (n<in.length()) {
				char c = in.charAt(n);
				if ((c == '/') || (c == '[')) {
					elementName.writeTo(out);
					elementName.reset();
					out.write(c);
				} else if (c == ':') {
					// Out element name thus far has been a prefix. If it is the specifed prefix, discard it
					if (prefix != null) {
						if (elementName.toString().equals(prefix)) {
							elementName.reset();
						}
					} else {
						// If no prefix specified, discard any namespace found
						elementName.reset();
					}
				} else {
					elementName.write(c);
				}
				n++;
			}
			elementName.writeTo(out);
		} catch (IOException e) { }
		return out.toString();
		
	}
	
	/**
	 * When an element has conditions (e.g. element[child/@name='fred']), this strips the conditions and
	 * returns just the element name.
	 * @param in element to remove conditions from
	 * @return element name
	 */
	public static String getElementNameWithoutCondition(String in) {
		int startOfCondition = in.indexOf('['); 
		if (startOfCondition>0) {
			return in.substring(0, in.indexOf('['));
		}
		return in;
	}
	
	/**
	 * When an XPath has conditions (e.g. element[child/@name='fred']/anotherElement), this strips the conditions and
	 * returns just the XPath without them.
	 * @param in xpath to remove conditions from
	 * @return xpath without conditions
	 */
	public static String getXPathWithoutConditions(String in) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		boolean inCondition = false;
		for (int n=0; n<in.length(); n++) {
			char c = in.charAt(n);
			if (c == '[') {
				inCondition = true;
			} else if (c == ']') {
				inCondition = false;
			} else {
				if (!inCondition) {
					bos.write(c);
				}
			}
		}
		return bos.toString();
	}
	
	/**
	 * This will strip out conditions from the XPath, but will also truncate
	 * the XPath so it stops at the end of the last conditional statement, so:
	 * element[child/@name='fred']/anotherElement[secondChild/@age='12']/finalElement
	 * would become: element/anotherElement
	 * @param in
	 * @return Stripped XPath
	 */
	public static String getXPathUpToFinalConditionalStatement(String in) {
		int lastConditional = in.lastIndexOf("]");
		return getXPathWithoutConditions(in.substring(0, lastConditional));		
	}

	/**
	 * When an XPath has conditions (e.g. element[child/@name='fred']/anotherElement), this strips the conditions and
	 * returns just the XPath without them.
	 * @param in xpath to remove conditions from
	 * @return xpath without conditions
	 */
	public static boolean isXPathAHierarchy(String in) {
		boolean inCondition = false;
		for (int n=0; n<in.length(); n++) {
			char c = in.charAt(n);
			if (c == '[') {
				inCondition = true;
			} else if (c == ']') {
				inCondition = false;
			} else if (c == '/') {
				if (!inCondition) {
					return true;
				}
			}
		}
		return false;
	}

	
	/**
	 * Takes a String XPath value and compiles it into a XPathExpression
	 * @param namespaces Namespace context for evaluating XPath
	 * @param val String containing the XPath
	 * @return Compiled XPath
	 */
	public static XPathExpression compileXpath(XMLNamespaceContext namespaces, String val) {
		if (val == null) {
			return null;
		}
		try {
			XPath xpath = xPathfactory.newXPath();
			xpath.setNamespaceContext(namespaces);
			return xpath.compile(val);
		} catch (XPathExpressionException e) {
			logger.error("	Cannot compile XPath: {}", val, e);
		}
		return null;
	}
	
	public static String replaceVariableInXPath(Payload p, String xpath) {
		String rootNodeName = p.getRootNode();
		return replaceVariableInXPath(rootNodeName, xpath);
	}
	public static String replaceVariableInXPath(String newValue, String xpath) {
		if (newValue != null) {
			if (xpath.contains("{childRootNodeName}")) {
				xpath = xpath.replaceAll("\\{childRootNodeName\\}", newValue);
			}
		} else {
			logger.error("Replacement value for field was null!");
		}
		return xpath;
	}
}
