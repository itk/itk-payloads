/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.fieldtypehandlers;

import java.util.ArrayList;

import javax.xml.xpath.XPathExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.nhs.interoperability.payloads.DomainObjectFactory;
import uk.nhs.interoperability.payloads.Payload;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.documentation.XPathReportCreator;
import uk.nhs.interoperability.payloads.util.xml.PayloadParser;
import uk.nhs.interoperability.payloads.util.xml.PayloadSerialiser;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.util.xml.XPaths;

public class PayloadFieldHandler extends AbstractFieldHandler implements DOMFieldHandler {
	private static final Logger logger = LoggerFactory.getLogger(PayloadFieldHandler.class);
	
	@Override
	public boolean isItemBlank(Field defn, Object o, boolean singleItem) {
		// Assume blank until a value is found
		boolean isBlank = true;
		if (o != null) {
			// Now, check if this is an empty payload, which is equivalent to "blank"..
			logger.trace("  CHECKING FOR EMPTY CHILD PAYLOAD");
			
			// List of payloads
			if ((defn.getMaxOccurs()>1) && (!singleItem)) {
				ArrayList<Payload> list = (ArrayList<Payload>)o;
				for (Payload item : list) {
					//Logger.info("Checking for empty payload: " + item.getClassName() + " within list field " + defn.getName() + " - Has Data = " + item.hasData());
					if (item.hasData()) {
						// Found a non-blank value in the list
						isBlank = false;
					}
				}
			} else {
				// Single payload
				//Logger.info("Checking for empty payload: " + ((Payload)o).getClassName() + " within field " + defn.getName() + " - Has Data = " + ((Payload)o).hasData());
				if (((Payload)o).hasData()) {
					// Payload has a non-blank value
					isBlank = false;
				}
			}
		}
		return isBlank;
	}

	@Override
	public boolean preProcessSerialise(XMLNamespaceContext namespaces, Object item,
			String xpath, Element parent, Document xmldoc, Field field, Payload payload) {

		Payload p = (Payload)item;
		
		// See if we have an xpath with a variable in that needs replacing
		//String xpath = definition.getXpath();
		xpath = XPaths.replaceVariableInXPath(p, xpath);
		
		Element e = PayloadSerialiser.createNode(namespaces, xpath, parent, xmldoc, null, this, field, payload);
		
		// If there are some namespaces defined within this subclass, output them now and add them to the
		// collection of namespaces passed into the child payload object
		PayloadSerialiser.addNamespaceDeclarations(payload.getNamespaceContext(), e);
		namespaces.inheritNamespaces(p.getNamespaceContext());
		
		PayloadSerialiser.serialiseFieldsInPayload(namespaces, p, e, xmldoc);
		return false;
	}
	
	@Override
	public boolean preProcessParse(XMLNamespaceContext namespaces, Payload p, Element parent, Document xmldoc, String name, Field field) {
		// Do nothing
		return true;
	}

	@Override
	public String stringValue(Field definition, Payload payload, Object val, XMLNamespaceContext namespaces, Element parent, Document xmldoc) {
		/*try {
			throw new ConfigurationException("ARGH!");
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}*/
		return null;
	}

	@Override
	public boolean isPayload() {
		return true;
	}

	@Override
	public Object convertValueToObject(XMLNamespaceContext namespaces,
			Payload p, Field definition, String val, String packg,
			String versionedName) {
			
			// If this payload has a specific package specified, use it
			// in preference to the current package
			if (definition.getTypePackage() != null) {
				if (definition.getTypePackage().length()>0) {
					packg = definition.getTypePackage();
				}
			}
			
			//Logger.info("Calling factory: name=" + definition.getTypeName() + " package=" + packg);
			
			Payload newPayload = DomainObjectFactory.getDomainObject(definition.getTypeName(), packg);
			if (newPayload == null) {
				logger.trace(" We appear to have a null payload, assume it is an interface and try to select an implementation. The field being processed is: {}", definition.getName());
				logger.trace(" Calling factory with versionedName:'{}' in package:'{}'", versionedName, packg);
				newPayload = DomainObjectFactory.getDomainObjectWithVersion(versionedName, packg, definition.getTypeName());
				/*if (newPayload == null) {
					Logger.info(" NULL returned from factory!");
				}*/
			}
			return newPayload;
		//}
	}
	
	@Override
	public Object parse(Field field, Element item, XMLNamespaceContext namespaces, Payload p, Document xmldoc, Element parent) {
		logger.trace("Field: {}, payload: {}", field.getName(), p.getClassName());
		XPathExpression xpath = field.getCompiledXpath(namespaces);
		String strXpath = field.getXpath();
		String val = null;
		
		if (field.getMaxOccurs() > 1) {
			// If we are in a list we can't be an attribute, so we can just use getTextContent
			val = item.getTextContent();
		} else {
			// If we are not in a list, we can look up the value directly using the xpath
			val = XPaths.findStringFromXPath(field.getCompiledXpath(namespaces), parent, field.getXpath());
		}
		Object elementValueObject = convertValueToObject(namespaces, p, field, val, p.getPackage(), null);
		
		if (item == null) {
			if (val != null) {
				logger.trace(" Treating this as an unstructured element for field {} with XPath {}", field.getName(), strXpath);
				if (val.length()>0) {
					((Payload)elementValueObject).setTextValue(val);
				} else {
					// Empty child object - don't add this
					//p.removeValue(field.getName());
					return null;
				}
			} else {
				logger.trace(" Null item with null value for field {} with XPath {}", field.getName(), strXpath);
				return null;
			}
		} else {
			if (elementValueObject != null) {
				// Combine our interited namespaces with any additional ones defined in child payload object
				namespaces.inheritNamespaces(((Payload)elementValueObject).getNamespaceContext());
				
				// Add the parent xpath and object names up to this object - for reporting purposes
				XPathReportCreator.addInheritedXPaths(p, strXpath, (Payload)elementValueObject, field, null);
				XPathReportCreator.addField(field, strXpath, p, "", null);
				
				// Now recurse
				PayloadParser.parsePayload(namespaces, (Payload)elementValueObject, item, xmldoc, field.getTypeName());
				if (!((Payload)elementValueObject).hasData()) {
					logger.trace("Our new {} payload appears to be empty, so we will ditch it.", field.getTypeName());
					return null;
				} else {
					logger.trace("Our new {} payload has data so we will add it.", field.getTypeName());
				}
			}
		}
		return elementValueObject;
	}
	
	@Override
	public Element getElement(XPathExpression xpath, Element parent, String strXpath) {
		return XPaths.findNodeFromXPath(xpath, parent, strXpath);
	}
}
