/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.xml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XML2HTML {
	private static final Logger logger = LoggerFactory.getLogger(XML2HTML.class);
	public static String reformat(String xml, String header) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int prevChar = 0;
		int currentChar = 0;
		int size = xml.length();
		boolean inLeadingSpaces = true;
		boolean inValue = false;
		boolean inTag = false;
		try {
			out.write("<!DOCTYPE html>\n<html><head><style>body { font-family: \"Courier New\", Courier, monospace; font-size: 14px; } .tag { color:RoyalBlue; } .value { color:DarkViolet }</style></head><body>".getBytes());
			out.write((header + "<br/><br/>").getBytes());
			prevChar = xml.charAt(0);
			for (int n=1; n<size; n++) {
				currentChar = xml.charAt(n);
				if (prevChar == '<') {
					inLeadingSpaces = false;
					inTag = true;
					out.write("<span class='tag'>&lt;".getBytes());
				} else if (prevChar == '>') {
					inLeadingSpaces = false;
					inTag = false;
					out.write("&gt;</span>".getBytes());
				} else if (prevChar == '"') {
					inLeadingSpaces = false;
					if (inTag) {
						if (inValue) {
							inValue = false;
							out.write("\"</span>".getBytes());
						} else {
							inValue = true;
							out.write("<span class='value'>\"".getBytes());
						}
					} else {
						out.write('"');
					}
				} else if (prevChar == 13) {
					inLeadingSpaces = true;
				} else if (prevChar == 10) {
					inLeadingSpaces = true;
					out.write("<br/>\n".getBytes());
				} else if (prevChar == 32) {
					if (inLeadingSpaces) {
						out.write("&nbsp;".getBytes());
					} else {
						out.write(prevChar);
					}
				} else if (prevChar == 9) {
					if (inLeadingSpaces) {
						out.write("&nbsp;&nbsp;&nbsp;".getBytes());
					} else {
						out.write(prevChar);
					}
				} else {
					inLeadingSpaces = false;
					out.write(prevChar);
				}
				prevChar=currentChar;
			}
			out.write(currentChar);
			out.write("</body></html>".getBytes());
		} catch (IOException e) {
			logger.error("Unable to convert XML to formatted HTML", e);
			return xml;
		}
		return out.toString();
	}
}
