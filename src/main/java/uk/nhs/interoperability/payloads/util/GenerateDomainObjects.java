/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.exceptions.ConfigurationException;
import uk.nhs.interoperability.payloads.metadata.PayloadClassInfo;
import uk.nhs.interoperability.payloads.util.xml.ParseUtils;

public class GenerateDomainObjects implements GlobalConstants {
	private static final Logger logger = LoggerFactory.getLogger(GenerateDomainObjects.class);
	
	/**
	 * This can be run from the command line to generate the classes that represent the contents of
	 * the itk-payloads. XML content can be parsed into these objects, or once populated they can be
	 * serialised out into XML content.
	 * @param args Takes one parameter --test : if that parameter is present, additional test payloads will also be generated for the purposed of unit testing
	 * @throws ConfigurationException If configuration files are invalid
	 */
	public static void main(String[] args) throws ConfigurationException {
		
		String srcPath = PropertyReader.getProperty("srcPath");
		
		// First, do the live ones
		String payloadList = PropertyReader.getProperty("payloadsToGenerate");
		// Now, add the test ones
		if (args.length>0) {
			if (args[0].equalsIgnoreCase("--test")) {
				logger.info("Including test payloads");
				payloadList = payloadList + "," + PropertyReader.getProperty("testPayloads");
			}
		}
		// And generate
		execute(payloadList, srcPath);
		
	}
	
	private static void execute(String payloadList, String srcPath) throws ConfigurationException {
		String[] payloads = payloadList.split(",");
		ArrayList<PayloadClassInfo> classInfoList = new ArrayList<PayloadClassInfo>(); 
		
		for (int n=0; n<payloads.length; n++) {
			String packg = PropertyReader.getProperty(payloads[n] + "Package");
			String configFile = PropertyReader.getProperty(payloads[n] + "FieldConfig");
			String payloadSrcPath = srcPath + packg.replace(".", "/");

			// Clean up old classes before regenerating
			//String srcPath = PropertyReader.getProperty("srcPath") + packg.replace(".", "/");
			FileUtils.createDirectory(payloadSrcPath);
			FileUtils.deleteFilesInDir(payloadSrcPath);

			// Generate classes
			ArrayList<PayloadClassInfo> classInfoListPart = 
					GenerateDomainObjects.generateDomainObjects(
							packg,
							configFile,
							payloads[n] + "FieldConfig",
							"codegeneratortransforms/domainObjectsCodeGenerationTransform.xsl",
							true,
							payloadSrcPath);
			
			// Generate Interfaces
			GenerateDomainObjects.generateDomainObjects(
					packg,
					configFile,
					payloads[n] + "FieldConfig",
					"codegeneratortransforms/domainConstraintsCodeGenerationTransform.xsl",
					false,
					payloadSrcPath);
			
			classInfoList.addAll(classInfoListPart);
		}
		
		// Now generate a factory class that we can use to get instances of these classes
		GenerateFactory.generateDomainObjectFactory(classInfoList, srcPath);
		
		// And finally, generate a simple class to allow us to look up the root node name for a template
		GenerateNameResolver.generateRootNodeNameResolver(classInfoList, srcPath);
		
		logger.info("Domain object generation complete.");
	}
	
	private static ArrayList<PayloadClassInfo> generateDomainObjects(String packg, String configFileName,
											String configFileKey, String templatePath, boolean rejectDuplicates, String payloadSrcPath) throws ConfigurationException {
		ArrayList<PayloadClassInfo> classInfoList = new ArrayList<PayloadClassInfo>();
		HashMap params = new HashMap();
		params.put("package",packg);
		params.put("configFileKey",configFileKey);
		
		String xml = getConfigXML(configFileName);
		
		String code = TransformManager.doTransform(
					templatePath,
					xml, params);
		
		String[] classes = code.split("@@@@");
		
		int n=1;
		String className = "";
		String versionedName = null;
		String rootNode = null;
		String[] interfaces = null;
		//String srcPath = PropertyReader.getProperty("srcPath") + packg.replace(".", "/");

		final int fieldCount = 5;
		
		while (n<classes.length) {
			if (n%fieldCount == 1) {
				// Class name
				className = classes[n];
			} else if (n%fieldCount == 2) {
				// Full template name (if applicable)
				if (classes[n].length()>3) {
					versionedName = classes[n];
				} else {
					versionedName = null;	
				}
			} else if (n%fieldCount == 3) {
				// Root node name (if applicable)
				if (classes[n].length()>3) {
					rootNode = classes[n];
				} else {
					rootNode = null;
				}
			} else if (n%fieldCount == 4) {
				// Interfaces (if applicable)
				if (classes[n].length()>3) {
					interfaces = classes[n].split(",");
				} else {
					interfaces = null;
				}
			} else {
				String path = payloadSrcPath + "/" + className;
				logger.trace("Writing generated class/interface to: {}", path);
				if (rejectDuplicates) {
					File f = new File(path);
					if(f.exists()) { 
						throw new ConfigurationException("There appears to be a duplicate class name - duplicate file: " + path);
					}
				}
				try {
					FileWriter.writeFile(path, classes[n].getBytes("UTF-8"));
				} catch (UnsupportedEncodingException e) {
					logger.error("Unable to output generated code using UTF-8", e);
				}

				// Add this to the class info list
				classInfoList.add(new PayloadClassInfo(packg, className.substring(0, className.length()-5), versionedName, rootNode, interfaces));
			}			
			n++;
		}
		return classInfoList;
	}
	
	protected static String getConfigXML(String fileName) {
		String xml = FileLoader.loadFileOnClasspath("/config/" + fileName);
		xml = ParseUtils.addIncludes(xml);
		return xml;
	}
}
