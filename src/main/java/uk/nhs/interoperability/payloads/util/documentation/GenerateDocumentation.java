/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.util.documentation;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.exceptions.ConfigurationException;
import uk.nhs.interoperability.payloads.util.FileLoader;
import uk.nhs.interoperability.payloads.util.FileUtils;
import uk.nhs.interoperability.payloads.util.FileWriter;
import uk.nhs.interoperability.payloads.util.GlobalConstants;
import uk.nhs.interoperability.payloads.util.PropertyReader;
import uk.nhs.interoperability.payloads.util.TransformManager;
import uk.nhs.interoperability.payloads.util.xml.ParseUtils;

public class GenerateDocumentation implements GlobalConstants {
	private static final Logger logger = LoggerFactory.getLogger(GenerateDocumentation.class);
	/**
	 * This can be run from the command line to generate the classes that represent the contents of
	 * the itk-payloads. XML content can be parsed into these objects, or once populated they can be
	 * serialised out into XML content.
	 * @param args Takes one parameter --test : if that parameter is present, additional test payloads will also be generated for the purposed of unit testing
	 * @throws ConfigurationException If the XML configuration files are invalid
	 */
	public static void main(String[] args) throws ConfigurationException {
		
		String srcPath = PropertyReader.getProperty("srcPath");
		
		// First, do the live ones
		String payloadList = PropertyReader.getProperty("payloadsToGenerate");
		// Now, add the test ones
		if (args.length>0) {
			if (args[0].equalsIgnoreCase("--test")) {
				logger.info("Including test payloads");
				payloadList = payloadList + "," + PropertyReader.getProperty("testPayloads");
			}
		}
		// And generate
		execute(payloadList, srcPath);
		
	}
	
	private static void execute(String payloadList, String srcPath) throws ConfigurationException {
		String[] payloads = payloadList.split(",");
		
		String reportPath = PropertyReader.getProperty("reportPath");
		FileUtils.createDirectory(reportPath);
		FileUtils.deleteFilesInDir(reportPath);
		
		for (int n=0; n<payloads.length; n++) {
			String packg = PropertyReader.getProperty(payloads[n] + "Package");
			String configFile = PropertyReader.getProperty(payloads[n] + "FieldConfig");

			// Generate documentation
			GenerateDocumentation.generateHTMLDocument(
				packg,
				configFile,
				payloads[n],
				"documentationTransforms/GenerateHTMLDocumentationFromConfig.xsl",
				reportPath);
		}
		
		logger.info("Documentation generation complete.");
	}
	
	private static void generateHTMLDocument(String packg, String configFileName, String payloadName,
							String templatePath, String reportPath) throws ConfigurationException {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("package",packg);
		params.put("configFileKey",payloadName);
		
		String xml = getConfigXML(configFileName);
		
		String reportContent = TransformManager.doTransform(
					templatePath,
					xml, params);
		
		String path = reportPath + "/" + payloadName + ".html";
		logger.info("Writing HTML documentation to: {}", path);
		FileWriter.writeFile(path, reportContent.getBytes());
	}
	
	private static String getConfigXML(String fileName) {
		String xml = FileLoader.loadFileOnClasspath("/config/" + fileName);
		xml = ParseUtils.addIncludes(xml);
		return xml;
	}
}
