package uk.nhs.interoperability.payloads.util;

public class GenerateFullPayloadConfigFile {

	public static void main(String[] args) {
		
		String payloadName = "templates";
		String configFile = PropertyReader.getProperty(payloadName + "FieldConfig");
		String xml = GenerateDomainObjects.getConfigXML(configFile);
		FileWriter.writeFile("fullConfigFile.xml", xml.getBytes());
	}

}
