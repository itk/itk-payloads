/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.util.Emptiable;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public interface Payload extends Emptiable {
	public String serialise();
	public Object getValue(String fieldName);
	public void setValue(String key, Object value);
	public void setFields(LinkedHashMap<String, Object> fields);
	public Map<String, Field> getFieldDefinitions();
	public String getClassName();
	public String getPackage();
	public void addMultivalue(String key, Object value);
	public void addMultivalue(String key, Object value, boolean reverseOrder);
	public void setTextValue(String value);
	public void removeValue(String key);
	public String getVersionedName();
	public XMLNamespaceContext getNamespaceContext();
	public boolean hasData();
	public String getRootNode();
	public Payload coerceTo(String packg, String name);
	public String getParentObjectXPath();
	public void setParentObjectXPath(String parentObjectXPath);
	public ArrayList<String> getParentObjectNames();
	public void setParentObjectNames(ArrayList<String> parentObjectNames);
}
