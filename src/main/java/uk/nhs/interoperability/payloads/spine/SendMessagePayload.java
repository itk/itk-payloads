/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SendMessagePayload object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} CreationTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} HL7VersionCode</li>
 * <li>String InteractionID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ProcessingCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ProcessingModeCode</li>
 * <li>String ReceiverASID</li>
 * <li>String ReceiverOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ReceiverOrganisationRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID OrgOrWorkgroupID} ReceiverOrganisationOrWorkgroupSDSID</li>
 * <li>String ReceiverUserRoleProfileID</li>
 * <li>String ReceiverUUID</li>
 * <li>String SenderASID</li>
 * <li>String SenderOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} SenderOrganisationRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID OrgOrWorkgroupID} SenderOrganisationOrWorkgroupSDSID</li>
 * <li>String SenderUserRoleProfileID</li>
 * <li>String SenderUUID</li>
 * <li>{@link ControlActWrapper ControlActWrapper} ControlActWrapper</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SendMessagePayload extends AbstractPayload implements Payload , TransmissionWrapper {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "SendMessagePayload";
		protected static final String shortName = "";
		protected static final String rootNode = "Message";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ID ID
		 * @param CreationTime CreationTime
		 * @param HL7VersionCode HL7VersionCode
		 * @param InteractionID InteractionID
		 * @param ProcessingCode ProcessingCode
		 * @param ProcessingModeCode ProcessingModeCode
		 * @param ReceiverASID ReceiverASID
		 * @param ReceiverOrganisationID ReceiverOrganisationID
		 * @param ReceiverOrganisationRole ReceiverOrganisationRole
		 * @param ReceiverOrganisationOrWorkgroupSDSID ReceiverOrganisationOrWorkgroupSDSID
		 * @param ReceiverUserRoleProfileID ReceiverUserRoleProfileID
		 * @param ReceiverUUID ReceiverUUID
		 * @param SenderASID SenderASID
		 * @param SenderOrganisationID SenderOrganisationID
		 * @param SenderOrganisationRole SenderOrganisationRole
		 * @param SenderOrganisationOrWorkgroupSDSID SenderOrganisationOrWorkgroupSDSID
		 * @param SenderUserRoleProfileID SenderUserRoleProfileID
		 * @param SenderUUID SenderUUID
		 * @param ControlActWrapper ControlActWrapper
		 */
	    public SendMessagePayload(String ID, HL7Date CreationTime, CodedValue HL7VersionCode, String InteractionID, CodedValue ProcessingCode, CodedValue ProcessingModeCode, String ReceiverASID, String ReceiverOrganisationID, CodedValue ReceiverOrganisationRole, uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID ReceiverOrganisationOrWorkgroupSDSID, String ReceiverUserRoleProfileID, String ReceiverUUID, String SenderASID, String SenderOrganisationID, CodedValue SenderOrganisationRole, uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID SenderOrganisationOrWorkgroupSDSID, String SenderUserRoleProfileID, String SenderUUID, ControlActWrapper ControlActWrapper) {
			fields = new LinkedHashMap<String, Object>();
			
			setID(ID);
			setCreationTime(CreationTime);
			setHL7VersionCode(HL7VersionCode);
			setInteractionID(InteractionID);
			setProcessingCode(ProcessingCode);
			setProcessingModeCode(ProcessingModeCode);
			setReceiverASID(ReceiverASID);
			setReceiverOrganisationID(ReceiverOrganisationID);
			setReceiverOrganisationRole(ReceiverOrganisationRole);
			setReceiverOrganisationOrWorkgroupSDSID(ReceiverOrganisationOrWorkgroupSDSID);
			setReceiverUserRoleProfileID(ReceiverUserRoleProfileID);
			setReceiverUUID(ReceiverUUID);
			setSenderASID(SenderASID);
			setSenderOrganisationID(SenderOrganisationID);
			setSenderOrganisationRole(SenderOrganisationRole);
			setSenderOrganisationOrWorkgroupSDSID(SenderOrganisationOrWorkgroupSDSID);
			setSenderUserRoleProfileID(SenderUserRoleProfileID);
			setSenderUUID(SenderUUID);
			setControlActWrapper(ControlActWrapper);
		}
	
		/**
		 * A DCE UUID to uniquely identify this message
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * A DCE UUID to uniquely identify this message
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * The date and time that the sending system created the message
		 * @return HL7Date object
		 */	
		public HL7Date getCreationTime() {
			return (HL7Date)getValue("CreationTime");
		}
		
		
		
		
		/**
		 * The date and time that the sending system created the message
		 * @param CreationTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setCreationTime(HL7Date CreationTime) {
			setValue("CreationTime", CreationTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @return CodedValue object
		 */	
		public CodedValue getHL7VersionCode() {
			return (CodedValue)getValue("HL7VersionCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @return HL7StandardVersionCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode getHL7VersionCodeEnum() {
			CodedValue cv = (CodedValue)getValue("HL7VersionCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("HL7StandardVersionCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @param HL7VersionCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setHL7VersionCode(CodedValue HL7VersionCode) {
			setValue("HL7VersionCode", HL7VersionCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param HL7VersionCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setHL7VersionCode(VocabularyEntry HL7VersionCode) {
			Code c = new CodedValue(HL7VersionCode);
			setValue("HL7VersionCode", c);
			return this;
		}
		
		/**
		 * The ID of the interaction being performed
		 * @return String object
		 */	
		public String getInteractionID() {
			return (String)getValue("InteractionID");
		}
		
		
		
		
		/**
		 * The ID of the interaction being performed
		 * @param InteractionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setInteractionID(String InteractionID) {
			setValue("InteractionID", InteractionID);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @return CodedValue object
		 */	
		public CodedValue getProcessingCode() {
			return (CodedValue)getValue("ProcessingCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @return ProcessingID enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID getProcessingCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ProcessingCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ProcessingID", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @param ProcessingCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setProcessingCode(CodedValue ProcessingCode) {
			setValue("ProcessingCode", ProcessingCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ProcessingCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setProcessingCode(VocabularyEntry ProcessingCode) {
			Code c = new CodedValue(ProcessingCode);
			setValue("ProcessingCode", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @return CodedValue object
		 */	
		public CodedValue getProcessingModeCode() {
			return (CodedValue)getValue("ProcessingModeCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @return ProcessingMode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode getProcessingModeCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ProcessingModeCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ProcessingMode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @param ProcessingModeCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setProcessingModeCode(CodedValue ProcessingModeCode) {
			setValue("ProcessingModeCode", ProcessingModeCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ProcessingModeCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setProcessingModeCode(VocabularyEntry ProcessingModeCode) {
			Code c = new CodedValue(ProcessingModeCode);
			setValue("ProcessingModeCode", c);
			return this;
		}
		
		/**
		 * 
		 * @return String object
		 */	
		public String getReceiverASID() {
			return (String)getValue("ReceiverASID");
		}
		
		
		
		
		/**
		 * 
		 * @param ReceiverASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverASID(String ReceiverASID) {
			setValue("ReceiverASID", ReceiverASID);
			return this;
		}
		
		
		/**
		 * SDS Organisation ID
		 * @return String object
		 */	
		public String getReceiverOrganisationID() {
			return (String)getValue("ReceiverOrganisationID");
		}
		
		
		
		
		/**
		 * SDS Organisation ID
		 * @param ReceiverOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverOrganisationID(String ReceiverOrganisationID) {
			setValue("ReceiverOrganisationID", ReceiverOrganisationID);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getReceiverOrganisationRole() {
			return (CodedValue)getValue("ReceiverOrganisationRole");
		}
		
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getReceiverOrganisationRoleEnum() {
			CodedValue cv = (CodedValue)getValue("ReceiverOrganisationRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param ReceiverOrganisationRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverOrganisationRole(CodedValue ReceiverOrganisationRole) {
			setValue("ReceiverOrganisationRole", ReceiverOrganisationRole);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ReceiverOrganisationRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverOrganisationRole(VocabularyEntry ReceiverOrganisationRole) {
			Code c = new CodedValue(ReceiverOrganisationRole);
			setValue("ReceiverOrganisationRole", c);
			return this;
		}
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID getReceiverOrganisationOrWorkgroupSDSID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID)getValue("ReceiverOrganisationOrWorkgroupSDSID");
		}
		
		
		
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @param ReceiverOrganisationOrWorkgroupSDSID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverOrganisationOrWorkgroupSDSID(uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID ReceiverOrganisationOrWorkgroupSDSID) {
			setValue("ReceiverOrganisationOrWorkgroupSDSID", ReceiverOrganisationOrWorkgroupSDSID);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getReceiverUserRoleProfileID() {
			return (String)getValue("ReceiverUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param ReceiverUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverUserRoleProfileID(String ReceiverUserRoleProfileID) {
			setValue("ReceiverUserRoleProfileID", ReceiverUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getReceiverUUID() {
			return (String)getValue("ReceiverUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param ReceiverUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setReceiverUUID(String ReceiverUUID) {
			setValue("ReceiverUUID", ReceiverUUID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getSenderASID() {
			return (String)getValue("SenderASID");
		}
		
		
		
		
		/**
		 * 
		 * @param SenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderASID(String SenderASID) {
			setValue("SenderASID", SenderASID);
			return this;
		}
		
		
		/**
		 * SDS Organisation ID
		 * @return String object
		 */	
		public String getSenderOrganisationID() {
			return (String)getValue("SenderOrganisationID");
		}
		
		
		
		
		/**
		 * SDS Organisation ID
		 * @param SenderOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderOrganisationID(String SenderOrganisationID) {
			setValue("SenderOrganisationID", SenderOrganisationID);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getSenderOrganisationRole() {
			return (CodedValue)getValue("SenderOrganisationRole");
		}
		
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getSenderOrganisationRoleEnum() {
			CodedValue cv = (CodedValue)getValue("SenderOrganisationRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param SenderOrganisationRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderOrganisationRole(CodedValue SenderOrganisationRole) {
			setValue("SenderOrganisationRole", SenderOrganisationRole);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param SenderOrganisationRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderOrganisationRole(VocabularyEntry SenderOrganisationRole) {
			Code c = new CodedValue(SenderOrganisationRole);
			setValue("SenderOrganisationRole", c);
			return this;
		}
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID getSenderOrganisationOrWorkgroupSDSID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID)getValue("SenderOrganisationOrWorkgroupSDSID");
		}
		
		
		
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @param SenderOrganisationOrWorkgroupSDSID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderOrganisationOrWorkgroupSDSID(uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID SenderOrganisationOrWorkgroupSDSID) {
			setValue("SenderOrganisationOrWorkgroupSDSID", SenderOrganisationOrWorkgroupSDSID);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getSenderUserRoleProfileID() {
			return (String)getValue("SenderUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param SenderUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderUserRoleProfileID(String SenderUserRoleProfileID) {
			setValue("SenderUserRoleProfileID", SenderUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getSenderUUID() {
			return (String)getValue("SenderUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param SenderUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setSenderUUID(String SenderUUID) {
			setValue("SenderUUID", SenderUUID);
			return this;
		}
		
		
		/**
		 * The control act wrapper
		 * @return ControlActWrapper object
		 */	
		public ControlActWrapper getControlActWrapper() {
			return (ControlActWrapper)getValue("ControlActWrapper");
		}
		
		
		
		
		/**
		 * The control act wrapper
		 * @param ControlActWrapper value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SendMessagePayload setControlActWrapper(ControlActWrapper ControlActWrapper) {
			setValue("ControlActWrapper", ControlActWrapper);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ID", new Field(
												"ID",
												"x:id/@root",
												"A DCE UUID to uniquely identify this message",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CreationTime", new Field(
												"CreationTime",
												"x:creationTime/@value",
												"The date and time that the sending system created the message",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("HL7VersionCode", new Field(
												"HL7VersionCode",
												"x:versionCode",
												"",
												"",
												"",
												"HL7StandardVersionCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("InteractionID", new Field(
												"InteractionID",
												"x:interactionId/@extension",
												"The ID of the interaction being performed",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InteractionIDOID", new Field(
												"InteractionIDOID",
												"x:interactionId/@root",
												"2.16.840.1.113883.2.1.3.2.4.12",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ProcessingCode", new Field(
												"ProcessingCode",
												"x:processingCode",
												"",
												"",
												"",
												"ProcessingID",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("ProcessingModeCode", new Field(
												"ProcessingModeCode",
												"x:processingModeCode",
												"",
												"",
												"",
												"ProcessingMode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("AcceptAck", new Field(
												"AcceptAck",
												"x:acceptAckCode/@code",
												"NE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverTypeCode", new Field(
												"ReceiverTypeCode",
												"x:communicationFunctionRcv/@typeCode",
												"RCV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverDeviceClassCode", new Field(
												"ReceiverDeviceClassCode",
												"x:communicationFunctionRcv/x:device/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverDeviceDeterminerCode", new Field(
												"ReceiverDeviceDeterminerCode",
												"x:communicationFunctionRcv/x:device/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverASID", new Field(
												"ReceiverASID",
												"x:communicationFunctionRcv/x:device/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverASIDOID", new Field(
												"ReceiverASIDOID",
												"x:communicationFunctionRcv/x:device/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverAgentClassCode", new Field(
												"ReceiverAgentClassCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/@classCode",
												"AGNT",
												"ReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrgClassCode", new Field(
												"ReceiverOrgClassCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/@classCode",
												"ORG",
												"ReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrgDeterminerCode", new Field(
												"ReceiverOrgDeterminerCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"ReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrganisationID", new Field(
												"ReceiverOrganisationID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:id/@extension",
												"SDS Organisation ID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrgIDOID", new Field(
												"ReceiverOrgIDOID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:id/@root",
												"1.2.826.0.1285.0.1.10",
												"ReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverAgentOrgClassCode", new Field(
												"ReceiverAgentOrgClassCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/@classCode",
												"AGNT",
												"ReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrganisationRole", new Field(
												"ReceiverOrganisationRole",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:code",
												"A code identifying the type of role that the agent is playing",
												"",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrganisationClassCode", new Field(
												"ReceiverOrganisationClassCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@classCode",
												"ORG",
												"ReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrganisationDeterminerCode", new Field(
												"ReceiverOrganisationDeterminerCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@determinerCode",
												"INSTANCE",
												"ReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverOrganisationOrWorkgroupSDSID", new Field(
												"ReceiverOrganisationOrWorkgroupSDSID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/x:id",
												"A valid SDS identifier that uniquely identifies the Organization or Workgroup",
												"",
												"",
												"",
												"OrgOrWorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverAgentPersonClassCode", new Field(
												"ReceiverAgentPersonClassCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/@classCode",
												"AGNT",
												"ReceiverUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverUserRoleProfileID", new Field(
												"ReceiverUserRoleProfileID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverURPIDOID", new Field(
												"ReceiverURPIDOID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"ReceiverUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverPersonClassCode", new Field(
												"ReceiverPersonClassCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"ReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverPersonDeterminerCode", new Field(
												"ReceiverPersonDeterminerCode",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"ReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverUUID", new Field(
												"ReceiverUUID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverUUIDOID", new Field(
												"ReceiverUUIDOID",
												"x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"ReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderTypeCode", new Field(
												"SenderTypeCode",
												"x:communicationFunctionSnd/@typeCode",
												"SND",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderDeviceClassCode", new Field(
												"SenderDeviceClassCode",
												"x:communicationFunctionSnd/x:device/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderDeviceDeterminerCode", new Field(
												"SenderDeviceDeterminerCode",
												"x:communicationFunctionSnd/x:device/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASID", new Field(
												"SenderASID",
												"x:communicationFunctionSnd/x:device/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDOID", new Field(
												"SenderASIDOID",
												"x:communicationFunctionSnd/x:device/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderAgentClassCode", new Field(
												"SenderAgentClassCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/@classCode",
												"AGNT",
												"SenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrgClassCode", new Field(
												"SenderOrgClassCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/@classCode",
												"ORG",
												"SenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrgDeterminerCode", new Field(
												"SenderOrgDeterminerCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"SenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrganisationID", new Field(
												"SenderOrganisationID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:id/@extension",
												"SDS Organisation ID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrgIDOID", new Field(
												"SenderOrgIDOID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:id/@root",
												"1.2.826.0.1285.0.1.10",
												"SenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderAgentOrgClassCode", new Field(
												"SenderAgentOrgClassCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/@classCode",
												"AGNT",
												"SenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrganisationRole", new Field(
												"SenderOrganisationRole",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:code",
												"A code identifying the type of role that the agent is playing",
												"",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrganisationClassCode", new Field(
												"SenderOrganisationClassCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@classCode",
												"ORG",
												"SenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrganisationDeterminerCode", new Field(
												"SenderOrganisationDeterminerCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@determinerCode",
												"INSTANCE",
												"SenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderOrganisationOrWorkgroupSDSID", new Field(
												"SenderOrganisationOrWorkgroupSDSID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/x:id",
												"A valid SDS identifier that uniquely identifies the Organization or Workgroup",
												"",
												"",
												"",
												"OrgOrWorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderAgentPersonClassCode", new Field(
												"SenderAgentPersonClassCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/@classCode",
												"AGNT",
												"SenderUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderUserRoleProfileID", new Field(
												"SenderUserRoleProfileID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderURPIDOID", new Field(
												"SenderURPIDOID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"SenderUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderPersonClassCode", new Field(
												"SenderPersonClassCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"SenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderPersonDeterminerCode", new Field(
												"SenderPersonDeterminerCode",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"SenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderUUID", new Field(
												"SenderUUID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderUUIDOID", new Field(
												"SenderUUIDOID",
												"x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"SenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWrapper", new Field(
												"ControlActWrapper",
												"x:ControlActEvent",
												"The control act wrapper",
												"",
												"",
												"",
												"ControlActWrapper",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SendMessagePayload() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SendMessagePayload(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	