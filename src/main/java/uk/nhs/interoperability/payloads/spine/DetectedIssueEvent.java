/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the DetectedIssueEvent object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ErrorCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ErrorCodeQualifier</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class DetectedIssueEvent extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "DetectedIssueEvent";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param ErrorCode ErrorCode
		 * @param ErrorCodeQualifier ErrorCodeQualifier
		 */
	    public DetectedIssueEvent(CodedValue ErrorCode, CodedValue ErrorCodeQualifier) {
			fields = new LinkedHashMap<String, Object>();
			
			setErrorCode(ErrorCode);
			setErrorCodeQualifier(ErrorCodeQualifier);
		}
	
		/**
		 * A code identifying the type of error that has occurred
		 * @return CodedValue object
		 */	
		public CodedValue getErrorCode() {
			return (CodedValue)getValue("ErrorCode");
		}
		
		
		
		
		/**
		 * A code identifying the type of error that has occurred
		 * @param ErrorCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DetectedIssueEvent setErrorCode(CodedValue ErrorCode) {
			setValue("ErrorCode", ErrorCode);
			return this;
		}
		
		
		/**
		 * A code identifying the type of error that has occurred
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ErrorCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DetectedIssueEvent setErrorCode(VocabularyEntry ErrorCode) {
			Code c = new CodedValue(ErrorCode);
			setValue("ErrorCode", c);
			return this;
		}
		
		/**
		 * A code to further qualify the nature of the error that has occurred
  		 * <br>NOTE: This field should be populated using the "DetectedIssueQualifier" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier
		 * @return CodedValue object
		 */	
		public CodedValue getErrorCodeQualifier() {
			return (CodedValue)getValue("ErrorCodeQualifier");
		}
		
		
		
		/**
		 * A code to further qualify the nature of the error that has occurred
  		 * <br>NOTE: This field should be populated using the "DetectedIssueQualifier" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier
		 * @return DetectedIssueQualifier enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier getErrorCodeQualifierEnum() {
			CodedValue cv = (CodedValue)getValue("ErrorCodeQualifier");
			VocabularyEntry entry = VocabularyFactory.getVocab("DetectedIssueQualifier", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier)entry;
		}
		
		
		/**
		 * A code to further qualify the nature of the error that has occurred
  		 * <br>NOTE: This field should be populated using the "DetectedIssueQualifier" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier
		 * @param ErrorCodeQualifier value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DetectedIssueEvent setErrorCodeQualifier(CodedValue ErrorCodeQualifier) {
			setValue("ErrorCodeQualifier", ErrorCodeQualifier);
			return this;
		}
		
		
		/**
		 * A code to further qualify the nature of the error that has occurred
  		 * <br>NOTE: This field should be populated using the "DetectedIssueQualifier" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DetectedIssueQualifier
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ErrorCodeQualifier value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DetectedIssueEvent setErrorCodeQualifier(VocabularyEntry ErrorCodeQualifier) {
			Code c = new CodedValue(ErrorCodeQualifier);
			setValue("ErrorCodeQualifier", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"ALRT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ErrorCode", new Field(
												"ErrorCode",
												"x:code",
												"A code identifying the type of error that has occurred",
												"",
												"",
												"",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ErrorCodeQualifier", new Field(
												"ErrorCodeQualifier",
												"x:code/x:qualifier/x:value",
												"A code to further qualify the nature of the error that has occurred",
												"",
												"",
												"DetectedIssueQualifier",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public DetectedIssueEvent() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public DetectedIssueEvent(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	
	
	