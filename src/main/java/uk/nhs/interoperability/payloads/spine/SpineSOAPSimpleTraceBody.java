/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SpineSOAPSimpleTraceBody object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String TransmissionID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TransmissionCreationTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionHL7VersionCode</li>
 * <li>String TransmissionInteractionID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionProcessingCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionProcessingModeCode</li>
 * <li>String TransmissionReceiverASID</li>
 * <li>String TransmissionReceiverOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionReceiverOrganisationRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID OrgOrWorkgroupID} TransmissionReceiverOrganisationOrWorkgroupSDSID</li>
 * <li>String TransmissionReceiverUserRoleProfileID</li>
 * <li>String TransmissionReceiverUUID</li>
 * <li>String TransmissionSenderASID</li>
 * <li>String TransmissionSenderOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionSenderOrganisationRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID OrgOrWorkgroupID} TransmissionSenderOrganisationOrWorkgroupSDSID</li>
 * <li>String TransmissionSenderUserRoleProfileID</li>
 * <li>String TransmissionSenderUUID</li>
 * <li>{@link ControlActWrapper ControlActWrapper} TransmissionControlActWrapper</li>
 * <li>String ControlActAuthorUserRoleProfileID</li>
 * <li>String ControlActAuthorUUID</li>
 * <li>String ControlActAuthorRoleID</li>
 * <li>String ControlActSenderASIDBLAH</li>
 * <li>String ControlActSenderASIDBLAH2</li>
 * <li>String ControlActSenderASID</li>
 * <li>String ControlActWorkstationIDOID</li>
 * <li>String ControlActWorkstationIDBLAH</li>
 * <li>String ControlActWorkstationIDBLAH2</li>
 * <li>String ControlActWorkstationID</li>
 * <li>{@link SpinePayload SpinePayload} ControlActQueryPayload</li>
 * <li>{@link String String} Postcode</li>
 * <li>String AddressUse</li>
 * <li>{@link String String} Gender</li>
 * <li>{@link HL7Date HL7Date} DateOfBirth</li>
 * <li>{@link HL7Date HL7Date} DateOfDeath</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SMSPPersonName SMSPPersonName} Name</li>
 * <li>String NameType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SpineSOAPSimpleTraceBody extends AbstractPayload implements Payload , SpineSOAPBody {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "SpineSOAPSimpleTraceBody";
		protected static final String shortName = "QUPA_IN000005UK01";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param TransmissionID TransmissionID
		 * @param TransmissionCreationTime TransmissionCreationTime
		 * @param TransmissionHL7VersionCode TransmissionHL7VersionCode
		 * @param TransmissionInteractionID TransmissionInteractionID
		 * @param TransmissionProcessingCode TransmissionProcessingCode
		 * @param TransmissionProcessingModeCode TransmissionProcessingModeCode
		 * @param TransmissionReceiverASID TransmissionReceiverASID
		 * @param TransmissionReceiverOrganisationID TransmissionReceiverOrganisationID
		 * @param TransmissionReceiverOrganisationRole TransmissionReceiverOrganisationRole
		 * @param TransmissionReceiverOrganisationOrWorkgroupSDSID TransmissionReceiverOrganisationOrWorkgroupSDSID
		 * @param TransmissionReceiverUserRoleProfileID TransmissionReceiverUserRoleProfileID
		 * @param TransmissionReceiverUUID TransmissionReceiverUUID
		 * @param TransmissionSenderASID TransmissionSenderASID
		 * @param TransmissionSenderOrganisationID TransmissionSenderOrganisationID
		 * @param TransmissionSenderOrganisationRole TransmissionSenderOrganisationRole
		 * @param TransmissionSenderOrganisationOrWorkgroupSDSID TransmissionSenderOrganisationOrWorkgroupSDSID
		 * @param TransmissionSenderUserRoleProfileID TransmissionSenderUserRoleProfileID
		 * @param TransmissionSenderUUID TransmissionSenderUUID
		 * @param TransmissionControlActWrapper TransmissionControlActWrapper
		 * @param ControlActAuthorUserRoleProfileID ControlActAuthorUserRoleProfileID
		 * @param ControlActAuthorUUID ControlActAuthorUUID
		 * @param ControlActAuthorRoleID ControlActAuthorRoleID
		 * @param ControlActSenderASIDBLAH ControlActSenderASIDBLAH
		 * @param ControlActSenderASIDBLAH2 ControlActSenderASIDBLAH2
		 * @param ControlActSenderASID ControlActSenderASID
		 * @param ControlActWorkstationIDOID ControlActWorkstationIDOID
		 * @param ControlActWorkstationIDBLAH ControlActWorkstationIDBLAH
		 * @param ControlActWorkstationIDBLAH2 ControlActWorkstationIDBLAH2
		 * @param ControlActWorkstationID ControlActWorkstationID
		 * @param ControlActQueryPayload ControlActQueryPayload
		 * @param Postcode Postcode
		 * @param AddressUse AddressUse
		 * @param Gender Gender
		 * @param DateOfBirth DateOfBirth
		 * @param DateOfDeath DateOfDeath
		 * @param Name Name
		 * @param NameType NameType
		 */
	    public SpineSOAPSimpleTraceBody(String TransmissionID, HL7Date TransmissionCreationTime, CodedValue TransmissionHL7VersionCode, String TransmissionInteractionID, CodedValue TransmissionProcessingCode, CodedValue TransmissionProcessingModeCode, String TransmissionReceiverASID, String TransmissionReceiverOrganisationID, CodedValue TransmissionReceiverOrganisationRole, uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionReceiverOrganisationOrWorkgroupSDSID, String TransmissionReceiverUserRoleProfileID, String TransmissionReceiverUUID, String TransmissionSenderASID, String TransmissionSenderOrganisationID, CodedValue TransmissionSenderOrganisationRole, uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionSenderOrganisationOrWorkgroupSDSID, String TransmissionSenderUserRoleProfileID, String TransmissionSenderUUID, ControlActWrapper TransmissionControlActWrapper, String ControlActAuthorUserRoleProfileID, String ControlActAuthorUUID, String ControlActAuthorRoleID, String ControlActSenderASIDBLAH, String ControlActSenderASIDBLAH2, String ControlActSenderASID, String ControlActWorkstationIDOID, String ControlActWorkstationIDBLAH, String ControlActWorkstationIDBLAH2, String ControlActWorkstationID, SpinePayload ControlActQueryPayload, String Postcode, String AddressUse, String Gender, HL7Date DateOfBirth, HL7Date DateOfDeath, uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name, String NameType) {
			fields = new LinkedHashMap<String, Object>();
			
			setTransmissionID(TransmissionID);
			setTransmissionCreationTime(TransmissionCreationTime);
			setTransmissionHL7VersionCode(TransmissionHL7VersionCode);
			setTransmissionInteractionID(TransmissionInteractionID);
			setTransmissionProcessingCode(TransmissionProcessingCode);
			setTransmissionProcessingModeCode(TransmissionProcessingModeCode);
			setTransmissionReceiverASID(TransmissionReceiverASID);
			setTransmissionReceiverOrganisationID(TransmissionReceiverOrganisationID);
			setTransmissionReceiverOrganisationRole(TransmissionReceiverOrganisationRole);
			setTransmissionReceiverOrganisationOrWorkgroupSDSID(TransmissionReceiverOrganisationOrWorkgroupSDSID);
			setTransmissionReceiverUserRoleProfileID(TransmissionReceiverUserRoleProfileID);
			setTransmissionReceiverUUID(TransmissionReceiverUUID);
			setTransmissionSenderASID(TransmissionSenderASID);
			setTransmissionSenderOrganisationID(TransmissionSenderOrganisationID);
			setTransmissionSenderOrganisationRole(TransmissionSenderOrganisationRole);
			setTransmissionSenderOrganisationOrWorkgroupSDSID(TransmissionSenderOrganisationOrWorkgroupSDSID);
			setTransmissionSenderUserRoleProfileID(TransmissionSenderUserRoleProfileID);
			setTransmissionSenderUUID(TransmissionSenderUUID);
			setTransmissionControlActWrapper(TransmissionControlActWrapper);
			setControlActAuthorUserRoleProfileID(ControlActAuthorUserRoleProfileID);
			setControlActAuthorUUID(ControlActAuthorUUID);
			setControlActAuthorRoleID(ControlActAuthorRoleID);
			setControlActSenderASIDBLAH(ControlActSenderASIDBLAH);
			setControlActSenderASIDBLAH2(ControlActSenderASIDBLAH2);
			setControlActSenderASID(ControlActSenderASID);
			setControlActWorkstationIDOID(ControlActWorkstationIDOID);
			setControlActWorkstationIDBLAH(ControlActWorkstationIDBLAH);
			setControlActWorkstationIDBLAH2(ControlActWorkstationIDBLAH2);
			setControlActWorkstationID(ControlActWorkstationID);
			setControlActQueryPayload(ControlActQueryPayload);
			setPostcode(Postcode);
			setAddressUse(AddressUse);
			setGender(Gender);
			setDateOfBirth(DateOfBirth);
			setDateOfDeath(DateOfDeath);
			setName(Name);
			setNameType(NameType);
		}
	
		/**
		 * A DCE UUID to uniquely identify this message
		 * @return String object
		 */	
		public String getTransmissionID() {
			return (String)getValue("TransmissionID");
		}
		
		
		
		
		/**
		 * A DCE UUID to uniquely identify this message
		 * @param TransmissionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionID(String TransmissionID) {
			setValue("TransmissionID", TransmissionID);
			return this;
		}
		
		
		/**
		 * The date and time that the sending system created the message
		 * @return HL7Date object
		 */	
		public HL7Date getTransmissionCreationTime() {
			return (HL7Date)getValue("TransmissionCreationTime");
		}
		
		
		
		
		/**
		 * The date and time that the sending system created the message
		 * @param TransmissionCreationTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionCreationTime(HL7Date TransmissionCreationTime) {
			setValue("TransmissionCreationTime", TransmissionCreationTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionHL7VersionCode() {
			return (CodedValue)getValue("TransmissionHL7VersionCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @return HL7StandardVersionCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode getTransmissionHL7VersionCodeEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionHL7VersionCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("HL7StandardVersionCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @param TransmissionHL7VersionCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionHL7VersionCode(CodedValue TransmissionHL7VersionCode) {
			setValue("TransmissionHL7VersionCode", TransmissionHL7VersionCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionHL7VersionCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionHL7VersionCode(VocabularyEntry TransmissionHL7VersionCode) {
			Code c = new CodedValue(TransmissionHL7VersionCode);
			setValue("TransmissionHL7VersionCode", c);
			return this;
		}
		
		/**
		 * The ID of the interaction being performed
		 * @return String object
		 */	
		public String getTransmissionInteractionID() {
			return (String)getValue("TransmissionInteractionID");
		}
		
		
		
		
		/**
		 * The ID of the interaction being performed
		 * @param TransmissionInteractionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionInteractionID(String TransmissionInteractionID) {
			setValue("TransmissionInteractionID", TransmissionInteractionID);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionProcessingCode() {
			return (CodedValue)getValue("TransmissionProcessingCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @return ProcessingID enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID getTransmissionProcessingCodeEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionProcessingCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ProcessingID", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @param TransmissionProcessingCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionProcessingCode(CodedValue TransmissionProcessingCode) {
			setValue("TransmissionProcessingCode", TransmissionProcessingCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionProcessingCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionProcessingCode(VocabularyEntry TransmissionProcessingCode) {
			Code c = new CodedValue(TransmissionProcessingCode);
			setValue("TransmissionProcessingCode", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionProcessingModeCode() {
			return (CodedValue)getValue("TransmissionProcessingModeCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @return ProcessingMode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode getTransmissionProcessingModeCodeEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionProcessingModeCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ProcessingMode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @param TransmissionProcessingModeCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionProcessingModeCode(CodedValue TransmissionProcessingModeCode) {
			setValue("TransmissionProcessingModeCode", TransmissionProcessingModeCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionProcessingModeCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionProcessingModeCode(VocabularyEntry TransmissionProcessingModeCode) {
			Code c = new CodedValue(TransmissionProcessingModeCode);
			setValue("TransmissionProcessingModeCode", c);
			return this;
		}
		
		/**
		 * 
		 * @return String object
		 */	
		public String getTransmissionReceiverASID() {
			return (String)getValue("TransmissionReceiverASID");
		}
		
		
		
		
		/**
		 * 
		 * @param TransmissionReceiverASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverASID(String TransmissionReceiverASID) {
			setValue("TransmissionReceiverASID", TransmissionReceiverASID);
			return this;
		}
		
		
		/**
		 * SDS Organisation ID
		 * @return String object
		 */	
		public String getTransmissionReceiverOrganisationID() {
			return (String)getValue("TransmissionReceiverOrganisationID");
		}
		
		
		
		
		/**
		 * SDS Organisation ID
		 * @param TransmissionReceiverOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverOrganisationID(String TransmissionReceiverOrganisationID) {
			setValue("TransmissionReceiverOrganisationID", TransmissionReceiverOrganisationID);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionReceiverOrganisationRole() {
			return (CodedValue)getValue("TransmissionReceiverOrganisationRole");
		}
		
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getTransmissionReceiverOrganisationRoleEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionReceiverOrganisationRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param TransmissionReceiverOrganisationRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverOrganisationRole(CodedValue TransmissionReceiverOrganisationRole) {
			setValue("TransmissionReceiverOrganisationRole", TransmissionReceiverOrganisationRole);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionReceiverOrganisationRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverOrganisationRole(VocabularyEntry TransmissionReceiverOrganisationRole) {
			Code c = new CodedValue(TransmissionReceiverOrganisationRole);
			setValue("TransmissionReceiverOrganisationRole", c);
			return this;
		}
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID getTransmissionReceiverOrganisationOrWorkgroupSDSID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID)getValue("TransmissionReceiverOrganisationOrWorkgroupSDSID");
		}
		
		
		
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @param TransmissionReceiverOrganisationOrWorkgroupSDSID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverOrganisationOrWorkgroupSDSID(uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionReceiverOrganisationOrWorkgroupSDSID) {
			setValue("TransmissionReceiverOrganisationOrWorkgroupSDSID", TransmissionReceiverOrganisationOrWorkgroupSDSID);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getTransmissionReceiverUserRoleProfileID() {
			return (String)getValue("TransmissionReceiverUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param TransmissionReceiverUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverUserRoleProfileID(String TransmissionReceiverUserRoleProfileID) {
			setValue("TransmissionReceiverUserRoleProfileID", TransmissionReceiverUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getTransmissionReceiverUUID() {
			return (String)getValue("TransmissionReceiverUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param TransmissionReceiverUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionReceiverUUID(String TransmissionReceiverUUID) {
			setValue("TransmissionReceiverUUID", TransmissionReceiverUUID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getTransmissionSenderASID() {
			return (String)getValue("TransmissionSenderASID");
		}
		
		
		
		
		/**
		 * 
		 * @param TransmissionSenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderASID(String TransmissionSenderASID) {
			setValue("TransmissionSenderASID", TransmissionSenderASID);
			return this;
		}
		
		
		/**
		 * SDS Organisation ID
		 * @return String object
		 */	
		public String getTransmissionSenderOrganisationID() {
			return (String)getValue("TransmissionSenderOrganisationID");
		}
		
		
		
		
		/**
		 * SDS Organisation ID
		 * @param TransmissionSenderOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderOrganisationID(String TransmissionSenderOrganisationID) {
			setValue("TransmissionSenderOrganisationID", TransmissionSenderOrganisationID);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionSenderOrganisationRole() {
			return (CodedValue)getValue("TransmissionSenderOrganisationRole");
		}
		
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getTransmissionSenderOrganisationRoleEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionSenderOrganisationRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param TransmissionSenderOrganisationRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderOrganisationRole(CodedValue TransmissionSenderOrganisationRole) {
			setValue("TransmissionSenderOrganisationRole", TransmissionSenderOrganisationRole);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionSenderOrganisationRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderOrganisationRole(VocabularyEntry TransmissionSenderOrganisationRole) {
			Code c = new CodedValue(TransmissionSenderOrganisationRole);
			setValue("TransmissionSenderOrganisationRole", c);
			return this;
		}
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID getTransmissionSenderOrganisationOrWorkgroupSDSID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID)getValue("TransmissionSenderOrganisationOrWorkgroupSDSID");
		}
		
		
		
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @param TransmissionSenderOrganisationOrWorkgroupSDSID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderOrganisationOrWorkgroupSDSID(uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionSenderOrganisationOrWorkgroupSDSID) {
			setValue("TransmissionSenderOrganisationOrWorkgroupSDSID", TransmissionSenderOrganisationOrWorkgroupSDSID);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getTransmissionSenderUserRoleProfileID() {
			return (String)getValue("TransmissionSenderUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param TransmissionSenderUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderUserRoleProfileID(String TransmissionSenderUserRoleProfileID) {
			setValue("TransmissionSenderUserRoleProfileID", TransmissionSenderUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getTransmissionSenderUUID() {
			return (String)getValue("TransmissionSenderUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param TransmissionSenderUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionSenderUUID(String TransmissionSenderUUID) {
			setValue("TransmissionSenderUUID", TransmissionSenderUUID);
			return this;
		}
		
		
		/**
		 * The control act wrapper
		 * @return ControlActWrapper object
		 */	
		public ControlActWrapper getTransmissionControlActWrapper() {
			return (ControlActWrapper)getValue("TransmissionControlActWrapper");
		}
		
		
		
		
		/**
		 * The control act wrapper
		 * @param TransmissionControlActWrapper value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setTransmissionControlActWrapper(ControlActWrapper TransmissionControlActWrapper) {
			setValue("TransmissionControlActWrapper", TransmissionControlActWrapper);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getControlActAuthorUserRoleProfileID() {
			return (String)getValue("ControlActAuthorUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param ControlActAuthorUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActAuthorUserRoleProfileID(String ControlActAuthorUserRoleProfileID) {
			setValue("ControlActAuthorUserRoleProfileID", ControlActAuthorUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getControlActAuthorUUID() {
			return (String)getValue("ControlActAuthorUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param ControlActAuthorUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActAuthorUUID(String ControlActAuthorUUID) {
			setValue("ControlActAuthorUUID", ControlActAuthorUUID);
			return this;
		}
		
		
		/**
		 * SDS Role Identifier
		 * @return String object
		 */	
		public String getControlActAuthorRoleID() {
			return (String)getValue("ControlActAuthorRoleID");
		}
		
		
		
		
		/**
		 * SDS Role Identifier
		 * @param ControlActAuthorRoleID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActAuthorRoleID(String ControlActAuthorRoleID) {
			setValue("ControlActAuthorRoleID", ControlActAuthorRoleID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActSenderASIDBLAH() {
			return (String)getValue("ControlActSenderASIDBLAH");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActSenderASIDBLAH value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActSenderASIDBLAH(String ControlActSenderASIDBLAH) {
			setValue("ControlActSenderASIDBLAH", ControlActSenderASIDBLAH);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActSenderASIDBLAH2() {
			return (String)getValue("ControlActSenderASIDBLAH2");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActSenderASIDBLAH2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActSenderASIDBLAH2(String ControlActSenderASIDBLAH2) {
			setValue("ControlActSenderASIDBLAH2", ControlActSenderASIDBLAH2);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActSenderASID() {
			return (String)getValue("ControlActSenderASID");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActSenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActSenderASID(String ControlActSenderASID) {
			setValue("ControlActSenderASID", ControlActSenderASID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationIDOID() {
			return (String)getValue("ControlActWorkstationIDOID");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationIDOID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActWorkstationIDOID(String ControlActWorkstationIDOID) {
			setValue("ControlActWorkstationIDOID", ControlActWorkstationIDOID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationIDBLAH() {
			return (String)getValue("ControlActWorkstationIDBLAH");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationIDBLAH value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActWorkstationIDBLAH(String ControlActWorkstationIDBLAH) {
			setValue("ControlActWorkstationIDBLAH", ControlActWorkstationIDBLAH);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationIDBLAH2() {
			return (String)getValue("ControlActWorkstationIDBLAH2");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationIDBLAH2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActWorkstationIDBLAH2(String ControlActWorkstationIDBLAH2) {
			setValue("ControlActWorkstationIDBLAH2", ControlActWorkstationIDBLAH2);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationID() {
			return (String)getValue("ControlActWorkstationID");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActWorkstationID(String ControlActWorkstationID) {
			setValue("ControlActWorkstationID", ControlActWorkstationID);
			return this;
		}
		
		
		/**
		 * The spine query message payload
		 * @return SpinePayload object
		 */	
		public SpinePayload getControlActQueryPayload() {
			return (SpinePayload)getValue("ControlActQueryPayload");
		}
		
		
		
		
		/**
		 * The spine query message payload
		 * @param ControlActQueryPayload value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setControlActQueryPayload(SpinePayload ControlActQueryPayload) {
			setValue("ControlActQueryPayload", ControlActQueryPayload);
			return this;
		}
		
		
		/**
		 * Postcode Query Parameter
		 * @return String object
		 */	
		public String getPostcode() {
			return (String)getValue("Postcode");
		}
		
		
		
		
		/**
		 * Postcode Query Parameter
		 * @param Postcode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setPostcode(String Postcode) {
			setValue("Postcode", Postcode);
			return this;
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return String object
		 */	
		public String getAddressUse() {
			return (String)getValue("AddressUse");
		}
		
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return AddressType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AddressType getAddressUseEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AddressType.getByCode((String)getValue("AddressUse"));
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @param AddressUse value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setAddressUse(String AddressUse) {
			setValue("AddressUse", AddressUse);
			return this;
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return String object
		 */	
		public String getGender() {
			return (String)getValue("Gender");
		}
		
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getGenderEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Sex.getByCode((String)getValue("Gender"));
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Gender value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setGender(String Gender) {
			setValue("Gender", Gender);
			return this;
		}
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * DateOfDeath Query Parameter
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfDeath() {
			return (HL7Date)getValue("DateOfDeath");
		}
		
		
		
		
		/**
		 * DateOfDeath Query Parameter
		 * @param DateOfDeath value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setDateOfDeath(HL7Date DateOfDeath) {
			setValue("DateOfDeath", DateOfDeath);
			return this;
		}
		
		
		/**
		 * Name Query Parameter
		 * @return uk.nhs.interoperability.payloads.commontypes.SMSPPersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SMSPPersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.SMSPPersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Name Query Parameter
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setName(uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @return String object
		 */	
		public String getNameType() {
			return (String)getValue("NameType");
		}
		
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @return PersonNameType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType getNameTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType.getByCode((String)getValue("NameType"));
		}
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @param NameType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceBody setNameType(String NameType) {
			setValue("NameType", NameType);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TransmissionID", new Field(
												"TransmissionID",
												"x:QUPA_IN000005UK01/x:id/@root",
												"A DCE UUID to uniquely identify this message",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionCreationTime", new Field(
												"TransmissionCreationTime",
												"x:QUPA_IN000005UK01/x:creationTime/@value",
												"The date and time that the sending system created the message",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionHL7VersionCode", new Field(
												"TransmissionHL7VersionCode",
												"x:QUPA_IN000005UK01/x:versionCode",
												"",
												"",
												"",
												"HL7StandardVersionCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("TransmissionInteractionID", new Field(
												"TransmissionInteractionID",
												"x:QUPA_IN000005UK01/x:interactionId/@extension",
												"The ID of the interaction being performed",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionInteractionIDOID", new Field(
												"TransmissionInteractionIDOID",
												"x:QUPA_IN000005UK01/x:interactionId/@root",
												"2.16.840.1.113883.2.1.3.2.4.12",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionProcessingCode", new Field(
												"TransmissionProcessingCode",
												"x:QUPA_IN000005UK01/x:processingCode",
												"",
												"",
												"",
												"ProcessingID",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("TransmissionProcessingModeCode", new Field(
												"TransmissionProcessingModeCode",
												"x:QUPA_IN000005UK01/x:processingModeCode",
												"",
												"",
												"",
												"ProcessingMode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("TransmissionAcceptAck", new Field(
												"TransmissionAcceptAck",
												"x:QUPA_IN000005UK01/x:acceptAckCode/@code",
												"NE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverTypeCode", new Field(
												"TransmissionReceiverTypeCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/@typeCode",
												"RCV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverDeviceClassCode", new Field(
												"TransmissionReceiverDeviceClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverDeviceDeterminerCode", new Field(
												"TransmissionReceiverDeviceDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverASID", new Field(
												"TransmissionReceiverASID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverASIDOID", new Field(
												"TransmissionReceiverASIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverAgentClassCode", new Field(
												"TransmissionReceiverAgentClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/@classCode",
												"AGNT",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrgClassCode", new Field(
												"TransmissionReceiverOrgClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/@classCode",
												"ORG",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrgDeterminerCode", new Field(
												"TransmissionReceiverOrgDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationID", new Field(
												"TransmissionReceiverOrganisationID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:id/@extension",
												"SDS Organisation ID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrgIDOID", new Field(
												"TransmissionReceiverOrgIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:id/@root",
												"1.2.826.0.1285.0.1.10",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverAgentOrgClassCode", new Field(
												"TransmissionReceiverAgentOrgClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/@classCode",
												"AGNT",
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationRole", new Field(
												"TransmissionReceiverOrganisationRole",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:code",
												"A code identifying the type of role that the agent is playing",
												"",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationClassCode", new Field(
												"TransmissionReceiverOrganisationClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@classCode",
												"ORG",
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationDeterminerCode", new Field(
												"TransmissionReceiverOrganisationDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@determinerCode",
												"INSTANCE",
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationOrWorkgroupSDSID", new Field(
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/x:id",
												"A valid SDS identifier that uniquely identifies the Organization or Workgroup",
												"",
												"",
												"",
												"OrgOrWorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverAgentPersonClassCode", new Field(
												"TransmissionReceiverAgentPersonClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/@classCode",
												"AGNT",
												"TransmissionReceiverUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverUserRoleProfileID", new Field(
												"TransmissionReceiverUserRoleProfileID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverURPIDOID", new Field(
												"TransmissionReceiverURPIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"TransmissionReceiverUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverPersonClassCode", new Field(
												"TransmissionReceiverPersonClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"TransmissionReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverPersonDeterminerCode", new Field(
												"TransmissionReceiverPersonDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"TransmissionReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverUUID", new Field(
												"TransmissionReceiverUUID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverUUIDOID", new Field(
												"TransmissionReceiverUUIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"TransmissionReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderTypeCode", new Field(
												"TransmissionSenderTypeCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/@typeCode",
												"SND",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderDeviceClassCode", new Field(
												"TransmissionSenderDeviceClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderDeviceDeterminerCode", new Field(
												"TransmissionSenderDeviceDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderASID", new Field(
												"TransmissionSenderASID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderASIDOID", new Field(
												"TransmissionSenderASIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderAgentClassCode", new Field(
												"TransmissionSenderAgentClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/@classCode",
												"AGNT",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrgClassCode", new Field(
												"TransmissionSenderOrgClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/@classCode",
												"ORG",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrgDeterminerCode", new Field(
												"TransmissionSenderOrgDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationID", new Field(
												"TransmissionSenderOrganisationID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:id/@extension",
												"SDS Organisation ID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrgIDOID", new Field(
												"TransmissionSenderOrgIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:id/@root",
												"1.2.826.0.1285.0.1.10",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderAgentOrgClassCode", new Field(
												"TransmissionSenderAgentOrgClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/@classCode",
												"AGNT",
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationRole", new Field(
												"TransmissionSenderOrganisationRole",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:code",
												"A code identifying the type of role that the agent is playing",
												"",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationClassCode", new Field(
												"TransmissionSenderOrganisationClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@classCode",
												"ORG",
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationDeterminerCode", new Field(
												"TransmissionSenderOrganisationDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@determinerCode",
												"INSTANCE",
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationOrWorkgroupSDSID", new Field(
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/x:id",
												"A valid SDS identifier that uniquely identifies the Organization or Workgroup",
												"",
												"",
												"",
												"OrgOrWorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderAgentPersonClassCode", new Field(
												"TransmissionSenderAgentPersonClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/@classCode",
												"AGNT",
												"TransmissionSenderUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderUserRoleProfileID", new Field(
												"TransmissionSenderUserRoleProfileID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderURPIDOID", new Field(
												"TransmissionSenderURPIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"TransmissionSenderUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderPersonClassCode", new Field(
												"TransmissionSenderPersonClassCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"TransmissionSenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderPersonDeterminerCode", new Field(
												"TransmissionSenderPersonDeterminerCode",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"TransmissionSenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderUUID", new Field(
												"TransmissionSenderUUID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderUUIDOID", new Field(
												"TransmissionSenderUUIDOID",
												"x:QUPA_IN000005UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"TransmissionSenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionControlActWrapper", new Field(
												"TransmissionControlActWrapper",
												"x:QUPA_IN000005UK01/x:ControlActEvent",
												"The control act wrapper",
												"",
												"",
												"",
												"ControlActWrapper",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActClassCode", new Field(
												"ControlActClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActMoodCode", new Field(
												"ControlActMoodCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorTypeCode", new Field(
												"ControlActAuthorTypeCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/@typeCode",
												"AUT",
												"ControlActAuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorAgentPersonClassCode", new Field(
												"ControlActAuthorAgentPersonClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/@classCode",
												"AGNT",
												"ControlActAuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorUserRoleProfileID", new Field(
												"ControlActAuthorUserRoleProfileID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorURPIDOID", new Field(
												"ControlActAuthorURPIDOID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"ControlActAuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorPersonClassCode", new Field(
												"ControlActAuthorPersonClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"ControlActAuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorPersonDeterminerCode", new Field(
												"ControlActAuthorPersonDeterminerCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"ControlActAuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorUUID", new Field(
												"ControlActAuthorUUID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorUUIDOID", new Field(
												"ControlActAuthorUUIDOID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"ControlActAuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorPartTypeCode", new Field(
												"ControlActAuthorPartTypeCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/@typeCode",
												"PART",
												"ControlActAuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorRoleClassCode", new Field(
												"ControlActAuthorRoleClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/x:partSDSRole/@classCode",
												"ROL",
												"ControlActAuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorRoleID", new Field(
												"ControlActAuthorRoleID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/x:partSDSRole/x:id/@extension",
												"SDS Role Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorRoleIDOID", new Field(
												"ControlActAuthorRoleIDOID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/x:partSDSRole/x:id/@root",
												"1.2.826.0.1285.0.2.1.104",
												"ControlActAuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDASIDOID", new Field(
												"ControlActSenderASIDASIDOID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDTypeCode", new Field(
												"ControlActSenderASIDTypeCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/@typeCode",
												"AUT",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDClassCode", new Field(
												"ControlActSenderASIDClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/@classCode",
												"AGNT",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDSystemClassCode", new Field(
												"ControlActSenderASIDSystemClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@classCode",
												"DEV",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDSystemDeterminerCode", new Field(
												"ControlActSenderASIDSystemDeterminerCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@determinerCode",
												"INSTANCE",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDBLAH", new Field(
												"ControlActSenderASIDBLAH",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDBLAH2", new Field(
												"ControlActSenderASIDBLAH2",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam2",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASID", new Field(
												"ControlActSenderASID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDOID", new Field(
												"ControlActWorkstationIDOID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@root",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDTypeCode", new Field(
												"ControlActWorkstationIDTypeCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/@typeCode",
												"AUT",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDClassCode", new Field(
												"ControlActWorkstationIDClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/@classCode",
												"AGNT",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDSystemClassCode", new Field(
												"ControlActWorkstationIDSystemClassCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@classCode",
												"DEV",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDSystemDeterminerCode", new Field(
												"ControlActWorkstationIDSystemDeterminerCode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@determinerCode",
												"INSTANCE",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDBLAH", new Field(
												"ControlActWorkstationIDBLAH",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDBLAH2", new Field(
												"ControlActWorkstationIDBLAH2",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam2",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationID", new Field(
												"ControlActWorkstationID",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActQueryPayload", new Field(
												"ControlActQueryPayload",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query",
												"The spine query message payload",
												"",
												"",
												"",
												"SpinePayload",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PostcodeSemanticsText", new Field(
												"PostcodeSemanticsText",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.address/x:semanticsText",
												"Person.address",
												"Postcode",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Postcode", new Field(
												"Postcode",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.address/x:value/x:postalCode",
												"Postcode Query Parameter",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressUse", new Field(
												"AddressUse",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.address/x:value/@use",
												"Optional address 'use' that may be used to specify the purpose of the address",
												"false",
												"",
												"AddressType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GenderSemanticsText", new Field(
												"GenderSemanticsText",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.administrativeGenderCode/x:semanticsText",
												"Person.administrativeGenderCode",
												"Gender",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Gender", new Field(
												"Gender",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.administrativeGenderCode/x:value/@code",
												"Gender Query Parameter",
												"",
												"",
												"Sex",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirthSemanticsText", new Field(
												"DateOfBirthSemanticsText",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.birthTime/x:semanticsText",
												"Person.birthTime",
												"DateOfBirth",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.birthTime/x:value/@value",
												"DateOfBirth Query Parameter",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfDeathSemanticsText", new Field(
												"DateOfDeathSemanticsText",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.deceasedTime/x:semanticsText",
												"Person.deceasedTime",
												"DateOfDeath",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfDeath", new Field(
												"DateOfDeath",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.deceasedTime/x:value/@value",
												"DateOfDeath Query Parameter",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameSemanticsText", new Field(
												"NameSemanticsText",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.name/x:semanticsText",
												"Person.name",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.name/x:value",
												"Name Query Parameter",
												"",
												"",
												"",
												"SMSPPersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameType", new Field(
												"NameType",
												"x:QUPA_IN000005UK01/x:ControlActEvent/x:query/x:person.name/x:value/@use",
												"Name use code",
												"false",
												"",
												"PersonNameType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SpineSOAPSimpleTraceBody() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SpineSOAPSimpleTraceBody(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	