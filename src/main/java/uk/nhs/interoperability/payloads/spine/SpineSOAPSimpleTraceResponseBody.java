/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SpineSOAPSimpleTraceResponseBody object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String TransmissionID</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TransmissionCreationTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionHL7VersionCode</li>
 * <li>String TransmissionInteractionID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionProcessingCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionProcessingModeCode</li>
 * <li>String TransmissionAcknowledgementType</li>
 * <li>String TransmissionAcknowledgementDetailType</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionErrorCode</li>
 * <li>String TransmissionMessageRef</li>
 * <li>String TransmissionReceiverASID</li>
 * <li>String TransmissionReceiverOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionReceiverOrganisationRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID OrgOrWorkgroupID} TransmissionReceiverOrganisationOrWorkgroupSDSID</li>
 * <li>String TransmissionReceiverUserRoleProfileID</li>
 * <li>String TransmissionReceiverUUID</li>
 * <li>String TransmissionSenderASID</li>
 * <li>String TransmissionSenderOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} TransmissionSenderOrganisationRole</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID OrgOrWorkgroupID} TransmissionSenderOrganisationOrWorkgroupSDSID</li>
 * <li>String TransmissionSenderUserRoleProfileID</li>
 * <li>String TransmissionSenderUUID</li>
 * <li>{@link ControlActWrapper ControlActWrapper} TransmissionControlActWrapper</li>
 * <li>String ControlActAuthorUserRoleProfileID</li>
 * <li>String ControlActAuthorUUID</li>
 * <li>String ControlActAuthorRoleID</li>
 * <li>String ControlActSenderASIDBLAH</li>
 * <li>String ControlActSenderASIDBLAH2</li>
 * <li>String ControlActSenderASID</li>
 * <li>String ControlActWorkstationIDOID</li>
 * <li>String ControlActWorkstationIDBLAH</li>
 * <li>String ControlActWorkstationIDBLAH2</li>
 * <li>String ControlActWorkstationID</li>
 * <li>List&lt;{@link DetectedIssueEvent DetectedIssueEvent}&gt; ControlActErrorCodes</li>
 * <li>{@link SpinePayload SpinePayload} ControlActResponsePayload</li>
 * <li>String ControlActQueryResponse</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>{@link PDSPatientID PDSPatientID} NHSNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; Telecom</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} InformationSensitivity</li>
 * <li>String AdministrativeGender</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DateOfBirth</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DeceasedTime</li>
 * <li>String MultipleBirthOrderNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; PatientName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.DateRange DateRange} EffectiveTime</li>
 * <li>String RegisteredGPID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PreviousNhsContact</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} DeathNotification</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SpineSOAPSimpleTraceResponseBody extends AbstractPayload implements Payload , SpineResponseBody {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "SpineSOAPSimpleTraceResponseBody";
		protected static final String shortName = "QUPA_IN000007UK01";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param TransmissionID TransmissionID
		 * @param TransmissionCreationTime TransmissionCreationTime
		 * @param TransmissionHL7VersionCode TransmissionHL7VersionCode
		 * @param TransmissionInteractionID TransmissionInteractionID
		 * @param TransmissionProcessingCode TransmissionProcessingCode
		 * @param TransmissionProcessingModeCode TransmissionProcessingModeCode
		 * @param TransmissionAcknowledgementType TransmissionAcknowledgementType
		 * @param TransmissionAcknowledgementDetailType TransmissionAcknowledgementDetailType
		 * @param TransmissionErrorCode TransmissionErrorCode
		 * @param TransmissionMessageRef TransmissionMessageRef
		 * @param TransmissionReceiverASID TransmissionReceiverASID
		 * @param TransmissionReceiverOrganisationID TransmissionReceiverOrganisationID
		 * @param TransmissionReceiverOrganisationRole TransmissionReceiverOrganisationRole
		 * @param TransmissionReceiverOrganisationOrWorkgroupSDSID TransmissionReceiverOrganisationOrWorkgroupSDSID
		 * @param TransmissionReceiverUserRoleProfileID TransmissionReceiverUserRoleProfileID
		 * @param TransmissionReceiverUUID TransmissionReceiverUUID
		 * @param TransmissionSenderASID TransmissionSenderASID
		 * @param TransmissionSenderOrganisationID TransmissionSenderOrganisationID
		 * @param TransmissionSenderOrganisationRole TransmissionSenderOrganisationRole
		 * @param TransmissionSenderOrganisationOrWorkgroupSDSID TransmissionSenderOrganisationOrWorkgroupSDSID
		 * @param TransmissionSenderUserRoleProfileID TransmissionSenderUserRoleProfileID
		 * @param TransmissionSenderUUID TransmissionSenderUUID
		 * @param TransmissionControlActWrapper TransmissionControlActWrapper
		 * @param ControlActAuthorUserRoleProfileID ControlActAuthorUserRoleProfileID
		 * @param ControlActAuthorUUID ControlActAuthorUUID
		 * @param ControlActAuthorRoleID ControlActAuthorRoleID
		 * @param ControlActSenderASIDBLAH ControlActSenderASIDBLAH
		 * @param ControlActSenderASIDBLAH2 ControlActSenderASIDBLAH2
		 * @param ControlActSenderASID ControlActSenderASID
		 * @param ControlActWorkstationIDOID ControlActWorkstationIDOID
		 * @param ControlActWorkstationIDBLAH ControlActWorkstationIDBLAH
		 * @param ControlActWorkstationIDBLAH2 ControlActWorkstationIDBLAH2
		 * @param ControlActWorkstationID ControlActWorkstationID
		 * @param ControlActErrorCodes ControlActErrorCodes
		 * @param ControlActResponsePayload ControlActResponsePayload
		 * @param ControlActQueryResponse ControlActQueryResponse
		 * @param Address Address
		 * @param NHSNumber NHSNumber
		 * @param Telecom Telecom
		 * @param InformationSensitivity InformationSensitivity
		 * @param AdministrativeGender AdministrativeGender
		 * @param DateOfBirth DateOfBirth
		 * @param DeceasedTime DeceasedTime
		 * @param MultipleBirthOrderNumber MultipleBirthOrderNumber
		 * @param PatientName PatientName
		 * @param EffectiveTime EffectiveTime
		 * @param RegisteredGPID RegisteredGPID
		 * @param PreviousNhsContact PreviousNhsContact
		 * @param DeathNotification DeathNotification
		 */
	    public SpineSOAPSimpleTraceResponseBody(String TransmissionID, HL7Date TransmissionCreationTime, CodedValue TransmissionHL7VersionCode, String TransmissionInteractionID, CodedValue TransmissionProcessingCode, CodedValue TransmissionProcessingModeCode, String TransmissionAcknowledgementType, String TransmissionAcknowledgementDetailType, CodedValue TransmissionErrorCode, String TransmissionMessageRef, String TransmissionReceiverASID, String TransmissionReceiverOrganisationID, CodedValue TransmissionReceiverOrganisationRole, uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionReceiverOrganisationOrWorkgroupSDSID, String TransmissionReceiverUserRoleProfileID, String TransmissionReceiverUUID, String TransmissionSenderASID, String TransmissionSenderOrganisationID, CodedValue TransmissionSenderOrganisationRole, uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionSenderOrganisationOrWorkgroupSDSID, String TransmissionSenderUserRoleProfileID, String TransmissionSenderUUID, ControlActWrapper TransmissionControlActWrapper, String ControlActAuthorUserRoleProfileID, String ControlActAuthorUUID, String ControlActAuthorRoleID, String ControlActSenderASIDBLAH, String ControlActSenderASIDBLAH2, String ControlActSenderASID, String ControlActWorkstationIDOID, String ControlActWorkstationIDBLAH, String ControlActWorkstationIDBLAH2, String ControlActWorkstationID, 
		List<DetectedIssueEvent> ControlActErrorCodes, SpinePayload ControlActResponsePayload, String ControlActQueryResponse, 
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, PDSPatientID NHSNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> Telecom, CodedValue InformationSensitivity, String AdministrativeGender, HL7Date DateOfBirth, HL7Date DeceasedTime, String MultipleBirthOrderNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> PatientName, uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime, String RegisteredGPID, CodedValue PreviousNhsContact, CodedValue DeathNotification) {
			fields = new LinkedHashMap<String, Object>();
			
			setTransmissionID(TransmissionID);
			setTransmissionCreationTime(TransmissionCreationTime);
			setTransmissionHL7VersionCode(TransmissionHL7VersionCode);
			setTransmissionInteractionID(TransmissionInteractionID);
			setTransmissionProcessingCode(TransmissionProcessingCode);
			setTransmissionProcessingModeCode(TransmissionProcessingModeCode);
			setTransmissionAcknowledgementType(TransmissionAcknowledgementType);
			setTransmissionAcknowledgementDetailType(TransmissionAcknowledgementDetailType);
			setTransmissionErrorCode(TransmissionErrorCode);
			setTransmissionMessageRef(TransmissionMessageRef);
			setTransmissionReceiverASID(TransmissionReceiverASID);
			setTransmissionReceiverOrganisationID(TransmissionReceiverOrganisationID);
			setTransmissionReceiverOrganisationRole(TransmissionReceiverOrganisationRole);
			setTransmissionReceiverOrganisationOrWorkgroupSDSID(TransmissionReceiverOrganisationOrWorkgroupSDSID);
			setTransmissionReceiverUserRoleProfileID(TransmissionReceiverUserRoleProfileID);
			setTransmissionReceiverUUID(TransmissionReceiverUUID);
			setTransmissionSenderASID(TransmissionSenderASID);
			setTransmissionSenderOrganisationID(TransmissionSenderOrganisationID);
			setTransmissionSenderOrganisationRole(TransmissionSenderOrganisationRole);
			setTransmissionSenderOrganisationOrWorkgroupSDSID(TransmissionSenderOrganisationOrWorkgroupSDSID);
			setTransmissionSenderUserRoleProfileID(TransmissionSenderUserRoleProfileID);
			setTransmissionSenderUUID(TransmissionSenderUUID);
			setTransmissionControlActWrapper(TransmissionControlActWrapper);
			setControlActAuthorUserRoleProfileID(ControlActAuthorUserRoleProfileID);
			setControlActAuthorUUID(ControlActAuthorUUID);
			setControlActAuthorRoleID(ControlActAuthorRoleID);
			setControlActSenderASIDBLAH(ControlActSenderASIDBLAH);
			setControlActSenderASIDBLAH2(ControlActSenderASIDBLAH2);
			setControlActSenderASID(ControlActSenderASID);
			setControlActWorkstationIDOID(ControlActWorkstationIDOID);
			setControlActWorkstationIDBLAH(ControlActWorkstationIDBLAH);
			setControlActWorkstationIDBLAH2(ControlActWorkstationIDBLAH2);
			setControlActWorkstationID(ControlActWorkstationID);
			setControlActErrorCodes(ControlActErrorCodes);
			setControlActResponsePayload(ControlActResponsePayload);
			setControlActQueryResponse(ControlActQueryResponse);
			setAddress(Address);
			setNHSNumber(NHSNumber);
			setTelecom(Telecom);
			setInformationSensitivity(InformationSensitivity);
			setAdministrativeGender(AdministrativeGender);
			setDateOfBirth(DateOfBirth);
			setDeceasedTime(DeceasedTime);
			setMultipleBirthOrderNumber(MultipleBirthOrderNumber);
			setPatientName(PatientName);
			setEffectiveTime(EffectiveTime);
			setRegisteredGPID(RegisteredGPID);
			setPreviousNhsContact(PreviousNhsContact);
			setDeathNotification(DeathNotification);
		}
	
		/**
		 * A DCE UUID to uniquely identify this message
		 * @return String object
		 */	
		public String getTransmissionID() {
			return (String)getValue("TransmissionID");
		}
		
		
		
		
		/**
		 * A DCE UUID to uniquely identify this message
		 * @param TransmissionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionID(String TransmissionID) {
			setValue("TransmissionID", TransmissionID);
			return this;
		}
		
		
		/**
		 * The date and time that the sending system created the message
		 * @return HL7Date object
		 */	
		public HL7Date getTransmissionCreationTime() {
			return (HL7Date)getValue("TransmissionCreationTime");
		}
		
		
		
		
		/**
		 * The date and time that the sending system created the message
		 * @param TransmissionCreationTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionCreationTime(HL7Date TransmissionCreationTime) {
			setValue("TransmissionCreationTime", TransmissionCreationTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionHL7VersionCode() {
			return (CodedValue)getValue("TransmissionHL7VersionCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @return HL7StandardVersionCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode getTransmissionHL7VersionCodeEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionHL7VersionCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("HL7StandardVersionCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * @param TransmissionHL7VersionCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionHL7VersionCode(CodedValue TransmissionHL7VersionCode) {
			setValue("TransmissionHL7VersionCode", TransmissionHL7VersionCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "HL7StandardVersionCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.HL7StandardVersionCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionHL7VersionCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionHL7VersionCode(VocabularyEntry TransmissionHL7VersionCode) {
			Code c = new CodedValue(TransmissionHL7VersionCode);
			setValue("TransmissionHL7VersionCode", c);
			return this;
		}
		
		/**
		 * The ID of the interaction being performed
		 * @return String object
		 */	
		public String getTransmissionInteractionID() {
			return (String)getValue("TransmissionInteractionID");
		}
		
		
		
		
		/**
		 * The ID of the interaction being performed
		 * @param TransmissionInteractionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionInteractionID(String TransmissionInteractionID) {
			setValue("TransmissionInteractionID", TransmissionInteractionID);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionProcessingCode() {
			return (CodedValue)getValue("TransmissionProcessingCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @return ProcessingID enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID getTransmissionProcessingCodeEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionProcessingCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ProcessingID", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * @param TransmissionProcessingCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionProcessingCode(CodedValue TransmissionProcessingCode) {
			setValue("TransmissionProcessingCode", TransmissionProcessingCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingID" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingID
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionProcessingCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionProcessingCode(VocabularyEntry TransmissionProcessingCode) {
			Code c = new CodedValue(TransmissionProcessingCode);
			setValue("TransmissionProcessingCode", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionProcessingModeCode() {
			return (CodedValue)getValue("TransmissionProcessingModeCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @return ProcessingMode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode getTransmissionProcessingModeCodeEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionProcessingModeCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ProcessingMode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * @param TransmissionProcessingModeCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionProcessingModeCode(CodedValue TransmissionProcessingModeCode) {
			setValue("TransmissionProcessingModeCode", TransmissionProcessingModeCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "ProcessingMode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ProcessingMode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionProcessingModeCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionProcessingModeCode(VocabularyEntry TransmissionProcessingModeCode) {
			Code c = new CodedValue(TransmissionProcessingModeCode);
			setValue("TransmissionProcessingModeCode", c);
			return this;
		}
		
		/**
		 * A code describing the action taken on the message by the receiving application
  		 * <br>NOTE: This field should be populated using the "AcknowledgementType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType
		 * @return String object
		 */	
		public String getTransmissionAcknowledgementType() {
			return (String)getValue("TransmissionAcknowledgementType");
		}
		
		
		
		/**
		 * A code describing the action taken on the message by the receiving application
  		 * <br>NOTE: This field should be populated using the "AcknowledgementType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType
		 * @return AcknowledgementType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType getTransmissionAcknowledgementTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType.getByCode((String)getValue("TransmissionAcknowledgementType"));
		}
		
		
		/**
		 * A code describing the action taken on the message by the receiving application
  		 * <br>NOTE: This field should be populated using the "AcknowledgementType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementType
		 * @param TransmissionAcknowledgementType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionAcknowledgementType(String TransmissionAcknowledgementType) {
			setValue("TransmissionAcknowledgementType", TransmissionAcknowledgementType);
			return this;
		}
		
		
		/**
		 * A code classifying the severity of the issue
  		 * <br>NOTE: This field should be populated using the "AcknowledgementDetailType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType
		 * @return String object
		 */	
		public String getTransmissionAcknowledgementDetailType() {
			return (String)getValue("TransmissionAcknowledgementDetailType");
		}
		
		
		
		/**
		 * A code classifying the severity of the issue
  		 * <br>NOTE: This field should be populated using the "AcknowledgementDetailType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType
		 * @return AcknowledgementDetailType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType getTransmissionAcknowledgementDetailTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType.getByCode((String)getValue("TransmissionAcknowledgementDetailType"));
		}
		
		
		/**
		 * A code classifying the severity of the issue
  		 * <br>NOTE: This field should be populated using the "AcknowledgementDetailType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.AcknowledgementDetailType
		 * @param TransmissionAcknowledgementDetailType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionAcknowledgementDetailType(String TransmissionAcknowledgementDetailType) {
			setValue("TransmissionAcknowledgementDetailType", TransmissionAcknowledgementDetailType);
			return this;
		}
		
		
		/**
		 * A code specifying the type of error that has occurred
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionErrorCode() {
			return (CodedValue)getValue("TransmissionErrorCode");
		}
		
		
		
		
		/**
		 * A code specifying the type of error that has occurred
		 * @param TransmissionErrorCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionErrorCode(CodedValue TransmissionErrorCode) {
			setValue("TransmissionErrorCode", TransmissionErrorCode);
			return this;
		}
		
		
		/**
		 * A code specifying the type of error that has occurred
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionErrorCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionErrorCode(VocabularyEntry TransmissionErrorCode) {
			Code c = new CodedValue(TransmissionErrorCode);
			setValue("TransmissionErrorCode", c);
			return this;
		}
		
		/**
		 * The unique identifier of the message that is being acknowledged
		 * @return String object
		 */	
		public String getTransmissionMessageRef() {
			return (String)getValue("TransmissionMessageRef");
		}
		
		
		
		
		/**
		 * The unique identifier of the message that is being acknowledged
		 * @param TransmissionMessageRef value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionMessageRef(String TransmissionMessageRef) {
			setValue("TransmissionMessageRef", TransmissionMessageRef);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getTransmissionReceiverASID() {
			return (String)getValue("TransmissionReceiverASID");
		}
		
		
		
		
		/**
		 * 
		 * @param TransmissionReceiverASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverASID(String TransmissionReceiverASID) {
			setValue("TransmissionReceiverASID", TransmissionReceiverASID);
			return this;
		}
		
		
		/**
		 * SDS Organisation ID
		 * @return String object
		 */	
		public String getTransmissionReceiverOrganisationID() {
			return (String)getValue("TransmissionReceiverOrganisationID");
		}
		
		
		
		
		/**
		 * SDS Organisation ID
		 * @param TransmissionReceiverOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverOrganisationID(String TransmissionReceiverOrganisationID) {
			setValue("TransmissionReceiverOrganisationID", TransmissionReceiverOrganisationID);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionReceiverOrganisationRole() {
			return (CodedValue)getValue("TransmissionReceiverOrganisationRole");
		}
		
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getTransmissionReceiverOrganisationRoleEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionReceiverOrganisationRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param TransmissionReceiverOrganisationRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverOrganisationRole(CodedValue TransmissionReceiverOrganisationRole) {
			setValue("TransmissionReceiverOrganisationRole", TransmissionReceiverOrganisationRole);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionReceiverOrganisationRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverOrganisationRole(VocabularyEntry TransmissionReceiverOrganisationRole) {
			Code c = new CodedValue(TransmissionReceiverOrganisationRole);
			setValue("TransmissionReceiverOrganisationRole", c);
			return this;
		}
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID getTransmissionReceiverOrganisationOrWorkgroupSDSID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID)getValue("TransmissionReceiverOrganisationOrWorkgroupSDSID");
		}
		
		
		
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @param TransmissionReceiverOrganisationOrWorkgroupSDSID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverOrganisationOrWorkgroupSDSID(uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionReceiverOrganisationOrWorkgroupSDSID) {
			setValue("TransmissionReceiverOrganisationOrWorkgroupSDSID", TransmissionReceiverOrganisationOrWorkgroupSDSID);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getTransmissionReceiverUserRoleProfileID() {
			return (String)getValue("TransmissionReceiverUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param TransmissionReceiverUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverUserRoleProfileID(String TransmissionReceiverUserRoleProfileID) {
			setValue("TransmissionReceiverUserRoleProfileID", TransmissionReceiverUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getTransmissionReceiverUUID() {
			return (String)getValue("TransmissionReceiverUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param TransmissionReceiverUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionReceiverUUID(String TransmissionReceiverUUID) {
			setValue("TransmissionReceiverUUID", TransmissionReceiverUUID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getTransmissionSenderASID() {
			return (String)getValue("TransmissionSenderASID");
		}
		
		
		
		
		/**
		 * 
		 * @param TransmissionSenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderASID(String TransmissionSenderASID) {
			setValue("TransmissionSenderASID", TransmissionSenderASID);
			return this;
		}
		
		
		/**
		 * SDS Organisation ID
		 * @return String object
		 */	
		public String getTransmissionSenderOrganisationID() {
			return (String)getValue("TransmissionSenderOrganisationID");
		}
		
		
		
		
		/**
		 * SDS Organisation ID
		 * @param TransmissionSenderOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderOrganisationID(String TransmissionSenderOrganisationID) {
			setValue("TransmissionSenderOrganisationID", TransmissionSenderOrganisationID);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return CodedValue object
		 */	
		public CodedValue getTransmissionSenderOrganisationRole() {
			return (CodedValue)getValue("TransmissionSenderOrganisationRole");
		}
		
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @return RoleCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode getTransmissionSenderOrganisationRoleEnum() {
			CodedValue cv = (CodedValue)getValue("TransmissionSenderOrganisationRole");
			VocabularyEntry entry = VocabularyFactory.getVocab("RoleCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode)entry;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * @param TransmissionSenderOrganisationRole value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderOrganisationRole(CodedValue TransmissionSenderOrganisationRole) {
			setValue("TransmissionSenderOrganisationRole", TransmissionSenderOrganisationRole);
			return this;
		}
		
		
		/**
		 * A code identifying the type of role that the agent is playing
  		 * <br>NOTE: This field should be populated using the "RoleCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.RoleCode
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param TransmissionSenderOrganisationRole value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderOrganisationRole(VocabularyEntry TransmissionSenderOrganisationRole) {
			Code c = new CodedValue(TransmissionSenderOrganisationRole);
			setValue("TransmissionSenderOrganisationRole", c);
			return this;
		}
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID getTransmissionSenderOrganisationOrWorkgroupSDSID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID)getValue("TransmissionSenderOrganisationOrWorkgroupSDSID");
		}
		
		
		
		
		/**
		 * A valid SDS identifier that uniquely identifies the Organization or Workgroup
		 * @param TransmissionSenderOrganisationOrWorkgroupSDSID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderOrganisationOrWorkgroupSDSID(uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID TransmissionSenderOrganisationOrWorkgroupSDSID) {
			setValue("TransmissionSenderOrganisationOrWorkgroupSDSID", TransmissionSenderOrganisationOrWorkgroupSDSID);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getTransmissionSenderUserRoleProfileID() {
			return (String)getValue("TransmissionSenderUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param TransmissionSenderUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderUserRoleProfileID(String TransmissionSenderUserRoleProfileID) {
			setValue("TransmissionSenderUserRoleProfileID", TransmissionSenderUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getTransmissionSenderUUID() {
			return (String)getValue("TransmissionSenderUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param TransmissionSenderUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionSenderUUID(String TransmissionSenderUUID) {
			setValue("TransmissionSenderUUID", TransmissionSenderUUID);
			return this;
		}
		
		
		/**
		 * The control act wrapper
		 * @return ControlActWrapper object
		 */	
		public ControlActWrapper getTransmissionControlActWrapper() {
			return (ControlActWrapper)getValue("TransmissionControlActWrapper");
		}
		
		
		
		
		/**
		 * The control act wrapper
		 * @param TransmissionControlActWrapper value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTransmissionControlActWrapper(ControlActWrapper TransmissionControlActWrapper) {
			setValue("TransmissionControlActWrapper", TransmissionControlActWrapper);
			return this;
		}
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getControlActAuthorUserRoleProfileID() {
			return (String)getValue("ControlActAuthorUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param ControlActAuthorUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActAuthorUserRoleProfileID(String ControlActAuthorUserRoleProfileID) {
			setValue("ControlActAuthorUserRoleProfileID", ControlActAuthorUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getControlActAuthorUUID() {
			return (String)getValue("ControlActAuthorUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param ControlActAuthorUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActAuthorUUID(String ControlActAuthorUUID) {
			setValue("ControlActAuthorUUID", ControlActAuthorUUID);
			return this;
		}
		
		
		/**
		 * SDS Role Identifier
		 * @return String object
		 */	
		public String getControlActAuthorRoleID() {
			return (String)getValue("ControlActAuthorRoleID");
		}
		
		
		
		
		/**
		 * SDS Role Identifier
		 * @param ControlActAuthorRoleID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActAuthorRoleID(String ControlActAuthorRoleID) {
			setValue("ControlActAuthorRoleID", ControlActAuthorRoleID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActSenderASIDBLAH() {
			return (String)getValue("ControlActSenderASIDBLAH");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActSenderASIDBLAH value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActSenderASIDBLAH(String ControlActSenderASIDBLAH) {
			setValue("ControlActSenderASIDBLAH", ControlActSenderASIDBLAH);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActSenderASIDBLAH2() {
			return (String)getValue("ControlActSenderASIDBLAH2");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActSenderASIDBLAH2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActSenderASIDBLAH2(String ControlActSenderASIDBLAH2) {
			setValue("ControlActSenderASIDBLAH2", ControlActSenderASIDBLAH2);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActSenderASID() {
			return (String)getValue("ControlActSenderASID");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActSenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActSenderASID(String ControlActSenderASID) {
			setValue("ControlActSenderASID", ControlActSenderASID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationIDOID() {
			return (String)getValue("ControlActWorkstationIDOID");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationIDOID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActWorkstationIDOID(String ControlActWorkstationIDOID) {
			setValue("ControlActWorkstationIDOID", ControlActWorkstationIDOID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationIDBLAH() {
			return (String)getValue("ControlActWorkstationIDBLAH");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationIDBLAH value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActWorkstationIDBLAH(String ControlActWorkstationIDBLAH) {
			setValue("ControlActWorkstationIDBLAH", ControlActWorkstationIDBLAH);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationIDBLAH2() {
			return (String)getValue("ControlActWorkstationIDBLAH2");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationIDBLAH2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActWorkstationIDBLAH2(String ControlActWorkstationIDBLAH2) {
			setValue("ControlActWorkstationIDBLAH2", ControlActWorkstationIDBLAH2);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getControlActWorkstationID() {
			return (String)getValue("ControlActWorkstationID");
		}
		
		
		
		
		/**
		 * 
		 * @param ControlActWorkstationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActWorkstationID(String ControlActWorkstationID) {
			setValue("ControlActWorkstationID", ControlActWorkstationID);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of DetectedIssueEvent objects
		 */
		public List<DetectedIssueEvent> getControlActErrorCodes() {
			return (List<DetectedIssueEvent>)getValue("ControlActErrorCodes");
		}
		
		/**
		 * 
		 * @param ControlActErrorCodes value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActErrorCodes(List ControlActErrorCodes) {
			setValue("ControlActErrorCodes", ControlActErrorCodes);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a DetectedIssueEvent object, but this method can be called
		 * multiple times to add additional DetectedIssueEvent objects.
		 * @param ControlActErrorCodes value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody addControlActErrorCodes(DetectedIssueEvent ControlActErrorCodes) {
			addMultivalue("ControlActErrorCodes", ControlActErrorCodes);
			return this;
		}
		
		
		/**
		 * The spine query response payload
		 * @return SpinePayload object
		 */	
		public SpinePayload getControlActResponsePayload() {
			return (SpinePayload)getValue("ControlActResponsePayload");
		}
		
		
		
		
		/**
		 * The spine query response payload
		 * @param ControlActResponsePayload value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActResponsePayload(SpinePayload ControlActResponsePayload) {
			setValue("ControlActResponsePayload", ControlActResponsePayload);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "QueryResponse" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse
		 * @return String object
		 */	
		public String getControlActQueryResponse() {
			return (String)getValue("ControlActQueryResponse");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "QueryResponse" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse
		 * @return QueryResponse enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse getControlActQueryResponseEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse.getByCode((String)getValue("ControlActQueryResponse"));
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "QueryResponse" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse
		 * @param ControlActQueryResponse value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setControlActQueryResponse(String ControlActQueryResponse) {
			setValue("ControlActQueryResponse", ControlActQueryResponse);
			return this;
		}
		
		
		/**
		 * Patient Address
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * Patient Address
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * Patient Address
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return PDSPatientID object
		 */	
		public PDSPatientID getNHSNumber() {
			return (PDSPatientID)getValue("NHSNumber");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param NHSNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setNHSNumber(PDSPatientID NHSNumber) {
			setValue("NHSNumber", NHSNumber);
			return this;
		}
		
		
		/**
		 * Patient Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelecom() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("Telecom");
		}
		
		/**
		 * Patient Telephone Number
		 * @param Telecom value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setTelecom(List Telecom) {
			setValue("Telecom", Telecom);
			return this;
		}
		
		/**
		 * Patient Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param Telecom value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody addTelecom(uk.nhs.interoperability.payloads.commontypes.Telecom Telecom) {
			addMultivalue("Telecom", Telecom);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * @return CodedValue object
		 */	
		public CodedValue getInformationSensitivity() {
			return (CodedValue)getValue("InformationSensitivity");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * @return InformationSensitivity enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity getInformationSensitivityEnum() {
			CodedValue cv = (CodedValue)getValue("InformationSensitivity");
			VocabularyEntry entry = VocabularyFactory.getVocab("InformationSensitivity", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * @param InformationSensitivity value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setInformationSensitivity(CodedValue InformationSensitivity) {
			setValue("InformationSensitivity", InformationSensitivity);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param InformationSensitivity value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setInformationSensitivity(VocabularyEntry InformationSensitivity) {
			Code c = new CodedValue(InformationSensitivity);
			setValue("InformationSensitivity", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return String object
		 */	
		public String getAdministrativeGender() {
			return (String)getValue("AdministrativeGender");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getAdministrativeGenderEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Sex.getByCode((String)getValue("AdministrativeGender"));
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param AdministrativeGender value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setAdministrativeGender(String AdministrativeGender) {
			setValue("AdministrativeGender", AdministrativeGender);
			return this;
		}
		
		
		/**
		 * Date
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * Date
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * Date
		 * @return HL7Date object
		 */	
		public HL7Date getDeceasedTime() {
			return (HL7Date)getValue("DeceasedTime");
		}
		
		
		
		
		/**
		 * Date
		 * @param DeceasedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setDeceasedTime(HL7Date DeceasedTime) {
			setValue("DeceasedTime", DeceasedTime);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getMultipleBirthOrderNumber() {
			return (String)getValue("MultipleBirthOrderNumber");
		}
		
		
		
		
		/**
		 * 
		 * @param MultipleBirthOrderNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setMultipleBirthOrderNumber(String MultipleBirthOrderNumber) {
			setValue("MultipleBirthOrderNumber", MultipleBirthOrderNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getPatientName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("PatientName");
		}
		
		/**
		 * Person name
		 * @param PatientName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setPatientName(List PatientName) {
			setValue("PatientName", PatientName);
			return this;
		}
		
		/**
		 * Person name
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param PatientName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody addPatientName(uk.nhs.interoperability.payloads.commontypes.PersonName PatientName) {
			addMultivalue("PatientName", PatientName);
			return this;
		}
		
		
		/**
		 * A date interval during which the patient care provision was effective
		 * @return uk.nhs.interoperability.payloads.commontypes.DateRange object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.DateRange getEffectiveTime() {
			return (uk.nhs.interoperability.payloads.commontypes.DateRange)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * A date interval during which the patient care provision was effective
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setEffectiveTime(uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getRegisteredGPID() {
			return (String)getValue("RegisteredGPID");
		}
		
		
		
		
		/**
		 * 
		 * @param RegisteredGPID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setRegisteredGPID(String RegisteredGPID) {
			setValue("RegisteredGPID", RegisteredGPID);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * @return CodedValue object
		 */	
		public CodedValue getPreviousNhsContact() {
			return (CodedValue)getValue("PreviousNhsContact");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * @return PreviousNhsContact enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact getPreviousNhsContactEnum() {
			CodedValue cv = (CodedValue)getValue("PreviousNhsContact");
			VocabularyEntry entry = VocabularyFactory.getVocab("PreviousNhsContact", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * @param PreviousNhsContact value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setPreviousNhsContact(CodedValue PreviousNhsContact) {
			setValue("PreviousNhsContact", PreviousNhsContact);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PreviousNhsContact value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setPreviousNhsContact(VocabularyEntry PreviousNhsContact) {
			Code c = new CodedValue(PreviousNhsContact);
			setValue("PreviousNhsContact", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * @return CodedValue object
		 */	
		public CodedValue getDeathNotification() {
			return (CodedValue)getValue("DeathNotification");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * @return DeathNotification enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification getDeathNotificationEnum() {
			CodedValue cv = (CodedValue)getValue("DeathNotification");
			VocabularyEntry entry = VocabularyFactory.getVocab("DeathNotification", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * @param DeathNotification value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setDeathNotification(CodedValue DeathNotification) {
			setValue("DeathNotification", DeathNotification);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param DeathNotification value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAPSimpleTraceResponseBody setDeathNotification(VocabularyEntry DeathNotification) {
			Code c = new CodedValue(DeathNotification);
			setValue("DeathNotification", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("TransmissionID", new Field(
												"TransmissionID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:id/@root",
												"A DCE UUID to uniquely identify this message",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionCreationTime", new Field(
												"TransmissionCreationTime",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:creationTime/@value",
												"The date and time that the sending system created the message",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionHL7VersionCode", new Field(
												"TransmissionHL7VersionCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:versionCode",
												"",
												"",
												"",
												"HL7StandardVersionCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("TransmissionInteractionID", new Field(
												"TransmissionInteractionID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:interactionId/@extension",
												"The ID of the interaction being performed",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionInteractionIDOID", new Field(
												"TransmissionInteractionIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:interactionId/@root",
												"2.16.840.1.113883.2.1.3.2.4.12",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionProcessingCode", new Field(
												"TransmissionProcessingCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:processingCode",
												"",
												"",
												"",
												"ProcessingID",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("TransmissionProcessingModeCode", new Field(
												"TransmissionProcessingModeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:processingModeCode",
												"",
												"",
												"",
												"ProcessingMode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"true",
												"",
												""
												));
	
		put("TransmissionAcceptAck", new Field(
												"TransmissionAcceptAck",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:acceptAckCode/@code",
												"NE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionAcknowledgementType", new Field(
												"TransmissionAcknowledgementType",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:acknowledgement/@typeCode",
												"A code describing the action taken on the message by the receiving application",
												"",
												"",
												"AcknowledgementType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionAcknowledgementDetailType", new Field(
												"TransmissionAcknowledgementDetailType",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:acknowledgement/x:acknowledgementDetail/@typeCode",
												"A code classifying the severity of the issue",
												"",
												"",
												"AcknowledgementDetailType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionErrorCode", new Field(
												"TransmissionErrorCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:acknowledgement/x:acknowledgementDetail/x:code",
												"A code specifying the type of error that has occurred",
												"",
												"",
												"",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionMessageRef", new Field(
												"TransmissionMessageRef",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:acknowledgement/x:messageRef/x:id/@root",
												"The unique identifier of the message that is being acknowledged",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverTypeCode", new Field(
												"TransmissionReceiverTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/@typeCode",
												"RCV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverDeviceClassCode", new Field(
												"TransmissionReceiverDeviceClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverDeviceDeterminerCode", new Field(
												"TransmissionReceiverDeviceDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverASID", new Field(
												"TransmissionReceiverASID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverASIDOID", new Field(
												"TransmissionReceiverASIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverAgentClassCode", new Field(
												"TransmissionReceiverAgentClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/@classCode",
												"AGNT",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrgClassCode", new Field(
												"TransmissionReceiverOrgClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/@classCode",
												"ORG",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrgDeterminerCode", new Field(
												"TransmissionReceiverOrgDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationID", new Field(
												"TransmissionReceiverOrganisationID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:id/@extension",
												"SDS Organisation ID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrgIDOID", new Field(
												"TransmissionReceiverOrgIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:id/@root",
												"1.2.826.0.1285.0.1.10",
												"TransmissionReceiverOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverAgentOrgClassCode", new Field(
												"TransmissionReceiverAgentOrgClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/@classCode",
												"AGNT",
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationRole", new Field(
												"TransmissionReceiverOrganisationRole",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:code",
												"A code identifying the type of role that the agent is playing",
												"",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationClassCode", new Field(
												"TransmissionReceiverOrganisationClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@classCode",
												"ORG",
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationDeterminerCode", new Field(
												"TransmissionReceiverOrganisationDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@determinerCode",
												"INSTANCE",
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverOrganisationOrWorkgroupSDSID", new Field(
												"TransmissionReceiverOrganisationOrWorkgroupSDSID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/x:id",
												"A valid SDS identifier that uniquely identifies the Organization or Workgroup",
												"",
												"",
												"",
												"OrgOrWorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverAgentPersonClassCode", new Field(
												"TransmissionReceiverAgentPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/@classCode",
												"AGNT",
												"TransmissionReceiverUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverUserRoleProfileID", new Field(
												"TransmissionReceiverUserRoleProfileID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverURPIDOID", new Field(
												"TransmissionReceiverURPIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"TransmissionReceiverUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverPersonClassCode", new Field(
												"TransmissionReceiverPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"TransmissionReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverPersonDeterminerCode", new Field(
												"TransmissionReceiverPersonDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"TransmissionReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverUUID", new Field(
												"TransmissionReceiverUUID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionReceiverUUIDOID", new Field(
												"TransmissionReceiverUUIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionRcv/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"TransmissionReceiverUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderTypeCode", new Field(
												"TransmissionSenderTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/@typeCode",
												"SND",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderDeviceClassCode", new Field(
												"TransmissionSenderDeviceClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/@classCode",
												"DEV",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderDeviceDeterminerCode", new Field(
												"TransmissionSenderDeviceDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/@determinerCode",
												"INSTANCE",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderASID", new Field(
												"TransmissionSenderASID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderASIDOID", new Field(
												"TransmissionSenderASIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderAgentClassCode", new Field(
												"TransmissionSenderAgentClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/@classCode",
												"AGNT",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrgClassCode", new Field(
												"TransmissionSenderOrgClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/@classCode",
												"ORG",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrgDeterminerCode", new Field(
												"TransmissionSenderOrgDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/@determinerCode",
												"INSTANCE",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationID", new Field(
												"TransmissionSenderOrganisationID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:id/@extension",
												"SDS Organisation ID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrgIDOID", new Field(
												"TransmissionSenderOrgIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:id/@root",
												"1.2.826.0.1285.0.1.10",
												"TransmissionSenderOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderAgentOrgClassCode", new Field(
												"TransmissionSenderAgentOrgClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/@classCode",
												"AGNT",
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationRole", new Field(
												"TransmissionSenderOrganisationRole",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:code",
												"A code identifying the type of role that the agent is playing",
												"",
												"",
												"RoleCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationClassCode", new Field(
												"TransmissionSenderOrganisationClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@classCode",
												"ORG",
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationDeterminerCode", new Field(
												"TransmissionSenderOrganisationDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/@determinerCode",
												"INSTANCE",
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderOrganisationOrWorkgroupSDSID", new Field(
												"TransmissionSenderOrganisationOrWorkgroupSDSID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentOrgSDS/x:agentOrganizationSDS/x:id",
												"A valid SDS identifier that uniquely identifies the Organization or Workgroup",
												"",
												"",
												"",
												"OrgOrWorkgroupID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderAgentPersonClassCode", new Field(
												"TransmissionSenderAgentPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/@classCode",
												"AGNT",
												"TransmissionSenderUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderUserRoleProfileID", new Field(
												"TransmissionSenderUserRoleProfileID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderURPIDOID", new Field(
												"TransmissionSenderURPIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"TransmissionSenderUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderPersonClassCode", new Field(
												"TransmissionSenderPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"TransmissionSenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderPersonDeterminerCode", new Field(
												"TransmissionSenderPersonDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"TransmissionSenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderUUID", new Field(
												"TransmissionSenderUUID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionSenderUUIDOID", new Field(
												"TransmissionSenderUUIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:communicationFunctionSnd/x:device/x:agencyFor/x:representedOrganization/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"TransmissionSenderUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TransmissionControlActWrapper", new Field(
												"TransmissionControlActWrapper",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent",
												"The control act wrapper",
												"",
												"",
												"",
												"ControlActWrapper",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActClassCode", new Field(
												"ControlActClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActMoodCode", new Field(
												"ControlActMoodCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorTypeCode", new Field(
												"ControlActAuthorTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/@typeCode",
												"AUT",
												"ControlActAuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorAgentPersonClassCode", new Field(
												"ControlActAuthorAgentPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/@classCode",
												"AGNT",
												"ControlActAuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorUserRoleProfileID", new Field(
												"ControlActAuthorUserRoleProfileID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorURPIDOID", new Field(
												"ControlActAuthorURPIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"ControlActAuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorPersonClassCode", new Field(
												"ControlActAuthorPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"ControlActAuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorPersonDeterminerCode", new Field(
												"ControlActAuthorPersonDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"ControlActAuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorUUID", new Field(
												"ControlActAuthorUUID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorUUIDOID", new Field(
												"ControlActAuthorUUIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"ControlActAuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorPartTypeCode", new Field(
												"ControlActAuthorPartTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/@typeCode",
												"PART",
												"ControlActAuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorRoleClassCode", new Field(
												"ControlActAuthorRoleClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/x:partSDSRole/@classCode",
												"ROL",
												"ControlActAuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorRoleID", new Field(
												"ControlActAuthorRoleID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/x:partSDSRole/x:id/@extension",
												"SDS Role Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActAuthorRoleIDOID", new Field(
												"ControlActAuthorRoleIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author/x:AgentPersonSDS/x:part/x:partSDSRole/x:id/@root",
												"1.2.826.0.1285.0.2.1.104",
												"ControlActAuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDASIDOID", new Field(
												"ControlActSenderASIDASIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDTypeCode", new Field(
												"ControlActSenderASIDTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/@typeCode",
												"AUT",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDClassCode", new Field(
												"ControlActSenderASIDClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/@classCode",
												"AGNT",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDSystemClassCode", new Field(
												"ControlActSenderASIDSystemClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@classCode",
												"DEV",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDSystemDeterminerCode", new Field(
												"ControlActSenderASIDSystemDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@determinerCode",
												"INSTANCE",
												"ControlActSenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDBLAH", new Field(
												"ControlActSenderASIDBLAH",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASIDBLAH2", new Field(
												"ControlActSenderASIDBLAH2",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam2",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActSenderASID", new Field(
												"ControlActSenderASID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDOID", new Field(
												"ControlActWorkstationIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@root",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDTypeCode", new Field(
												"ControlActWorkstationIDTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/@typeCode",
												"AUT",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDClassCode", new Field(
												"ControlActWorkstationIDClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/@classCode",
												"AGNT",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDSystemClassCode", new Field(
												"ControlActWorkstationIDSystemClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@classCode",
												"DEV",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDSystemDeterminerCode", new Field(
												"ControlActWorkstationIDSystemDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@determinerCode",
												"INSTANCE",
												"ControlActWorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDBLAH", new Field(
												"ControlActWorkstationIDBLAH",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationIDBLAH2", new Field(
												"ControlActWorkstationIDBLAH2",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam2",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActWorkstationID", new Field(
												"ControlActWorkstationID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActReasonTypeCode", new Field(
												"ControlActReasonTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:reason/@typeCode",
												"RSON",
												"ControlActErrorCodes",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActErrorCodes", new Field(
												"ControlActErrorCodes",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:reason/x:justifyingDetectedIssueEvent",
												"",
												"",
												"",
												"",
												"DetectedIssueEvent",
												"",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActResponsePayload", new Field(
												"ControlActResponsePayload",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject",
												"The spine query response payload",
												"",
												"",
												"",
												"SpinePayload",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActResponsePayloadTypeCode", new Field(
												"ControlActResponsePayloadTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/@typeCode",
												"SUBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActQueryAckType", new Field(
												"ControlActQueryAckType",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:queryAck/@type",
												"QueryAck",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ControlActQueryResponse", new Field(
												"ControlActQueryResponse",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:queryAck/x:queryResponseCode/@code",
												"",
												"",
												"",
												"QueryResponse",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassCode", new Field(
												"ClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTypeCode", new Field(
												"SubjectTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientRoleClassCode", new Field(
												"PatientRoleClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/@classCode",
												"PAT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:addr",
												"Patient Address",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumber", new Field(
												"NHSNumber",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:id",
												"",
												"true",
												"",
												"",
												"PDSPatientID",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Telecom", new Field(
												"Telecom",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:telecom",
												"Patient Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformationSensitivity", new Field(
												"InformationSensitivity",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:confidentialityCode",
												"",
												"false",
												"",
												"InformationSensitivity",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonClassCode", new Field(
												"PatientPersonClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/@classCode",
												"PSN",
												"PatientName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonDeterminerCode", new Field(
												"PatientPersonDeterminerCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/@determinerCode",
												"INSTANCE",
												"PatientName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AdministrativeGender", new Field(
												"AdministrativeGender",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:administrativeGenderCode/@code",
												"",
												"false",
												"",
												"Sex",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:birthTime/@value",
												"Date",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeceasedTime", new Field(
												"DeceasedTime",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:deceasedTime/@value",
												"Date",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MultipleBirthOrderNumber", new Field(
												"MultipleBirthOrderNumber",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:multipleBirthOrderNumber/@value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientName", new Field(
												"PatientName",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:name",
												"Person name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OtherProviderPatientClassCode", new Field(
												"OtherProviderPatientClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/@classCode",
												"PAT",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OtherProviderPatientSubjectOfTypeCode", new Field(
												"OtherProviderPatientSubjectOfTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/@typeCode",
												"SBJ",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionClassCode", new Field(
												"PatientCareProvisionClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/@classCode",
												"PCPR",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionMoodCode", new Field(
												"PatientCareProvisionMoodCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/@moodCode",
												"EVN",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionCodeFixedCodedValue", new Field(
												"PatientCareProvisionCodeFixedCodedValue",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:code/@code",
												"1",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionCodeFixedCodedValueCodeSystem", new Field(
												"PatientCareProvisionCodeFixedCodedValueCodeSystem",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.37",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:effectiveTime",
												"A date interval during which the patient care provision was effective",
												"false",
												"",
												"",
												"DateRange",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTypeCode", new Field(
												"PerformerTypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/@typeCode",
												"PRF",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedOrganizationClassCode", new Field(
												"AssignedOrganizationClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/x:assignedOrganization/@classCode",
												"ASSIGNED",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPID", new Field(
												"RegisteredGPID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/x:assignedOrganization/x:id/@extension",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPIDOID", new Field(
												"RegisteredGPIDOID",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/x:assignedOrganization/x:id/@root",
												"2.16.840.1.113883.2.1.4.3",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeCode", new Field(
												"TypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf2/@typeCode",
												"SBJ",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactClassCode", new Field(
												"previousNhsContactClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/@classCode",
												"OBS",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactMoodCode", new Field(
												"previousNhsContactMoodCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/@moodCode",
												"EVN",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactCodeFixedCodedValue", new Field(
												"previousNhsContactCodeFixedCodedValue",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/x:code/@code",
												"17",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactCodeFixedCodedValueCodeSystem", new Field(
												"previousNhsContactCodeFixedCodedValueCodeSystem",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.35",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousNhsContact", new Field(
												"PreviousNhsContact",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/x:value",
												"",
												"false",
												"",
												"PreviousNhsContact",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectOf3TypeCode", new Field(
												"SubjectOf3TypeCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf3/@typeCode",
												"SBJ",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationClassCode", new Field(
												"DeathNotificationClassCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf3/x:deathNotification/@classCode",
												"OBS",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationMoodCode", new Field(
												"DeathNotificationMoodCode",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf3/x:deathNotification/@moodCode",
												"EVN",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationCodeFixedCodedValue", new Field(
												"DeathNotificationCodeFixedCodedValue",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf3/x:deathNotification/x:code/@code",
												"3",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationCodeFixedCodedValueCodeSystem", new Field(
												"DeathNotificationCodeFixedCodedValueCodeSystem",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf3/x:deathNotification/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.35",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotification", new Field(
												"DeathNotification",
												"x:traceQueryResponse/x:QUPA_IN000007UK01/x:ControlActEvent/x:subject/x:PdsTraceMatch/x:subject/x:patientRole/x:subjectOf3/x:deathNotification/x:value",
												"",
												"false",
												"",
												"DeathNotification",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SpineSOAPSimpleTraceResponseBody() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SpineSOAPSimpleTraceResponseBody(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	
	
		
	