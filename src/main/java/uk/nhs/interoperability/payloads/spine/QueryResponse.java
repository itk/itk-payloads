/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the QueryResponse object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String AuthorUserRoleProfileID</li>
 * <li>String AuthorUUID</li>
 * <li>String AuthorRoleID</li>
 * <li>String SenderASIDBLAH</li>
 * <li>String SenderASIDBLAH2</li>
 * <li>String SenderASID</li>
 * <li>String WorkstationIDOID</li>
 * <li>String WorkstationIDBLAH</li>
 * <li>String WorkstationIDBLAH2</li>
 * <li>String WorkstationID</li>
 * <li>List&lt;{@link DetectedIssueEvent DetectedIssueEvent}&gt; ErrorCodes</li>
 * <li>{@link SpinePayload SpinePayload} ResponsePayload</li>
 * <li>String QueryResponse</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class QueryResponse extends AbstractPayload implements Payload , ControlActWrapper {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "QueryResponse";
		protected static final String shortName = "";
		protected static final String rootNode = "ControlActEvent";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param AuthorUserRoleProfileID AuthorUserRoleProfileID
		 * @param AuthorUUID AuthorUUID
		 * @param AuthorRoleID AuthorRoleID
		 * @param SenderASIDBLAH SenderASIDBLAH
		 * @param SenderASIDBLAH2 SenderASIDBLAH2
		 * @param SenderASID SenderASID
		 * @param WorkstationIDOID WorkstationIDOID
		 * @param WorkstationIDBLAH WorkstationIDBLAH
		 * @param WorkstationIDBLAH2 WorkstationIDBLAH2
		 * @param WorkstationID WorkstationID
		 * @param ErrorCodes ErrorCodes
		 * @param ResponsePayload ResponsePayload
		 * @param QueryResponse QueryResponse
		 */
	    public QueryResponse(String AuthorUserRoleProfileID, String AuthorUUID, String AuthorRoleID, String SenderASIDBLAH, String SenderASIDBLAH2, String SenderASID, String WorkstationIDOID, String WorkstationIDBLAH, String WorkstationIDBLAH2, String WorkstationID, 
		List<DetectedIssueEvent> ErrorCodes, SpinePayload ResponsePayload, String QueryResponse) {
			fields = new LinkedHashMap<String, Object>();
			
			setAuthorUserRoleProfileID(AuthorUserRoleProfileID);
			setAuthorUUID(AuthorUUID);
			setAuthorRoleID(AuthorRoleID);
			setSenderASIDBLAH(SenderASIDBLAH);
			setSenderASIDBLAH2(SenderASIDBLAH2);
			setSenderASID(SenderASID);
			setWorkstationIDOID(WorkstationIDOID);
			setWorkstationIDBLAH(WorkstationIDBLAH);
			setWorkstationIDBLAH2(WorkstationIDBLAH2);
			setWorkstationID(WorkstationID);
			setErrorCodes(ErrorCodes);
			setResponsePayload(ResponsePayload);
			setQueryResponse(QueryResponse);
		}
	
		/**
		 * SDS User Role Profile Identifier
		 * @return String object
		 */	
		public String getAuthorUserRoleProfileID() {
			return (String)getValue("AuthorUserRoleProfileID");
		}
		
		
		
		
		/**
		 * SDS User Role Profile Identifier
		 * @param AuthorUserRoleProfileID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setAuthorUserRoleProfileID(String AuthorUserRoleProfileID) {
			setValue("AuthorUserRoleProfileID", AuthorUserRoleProfileID);
			return this;
		}
		
		
		/**
		 * SDS User UUID
		 * @return String object
		 */	
		public String getAuthorUUID() {
			return (String)getValue("AuthorUUID");
		}
		
		
		
		
		/**
		 * SDS User UUID
		 * @param AuthorUUID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setAuthorUUID(String AuthorUUID) {
			setValue("AuthorUUID", AuthorUUID);
			return this;
		}
		
		
		/**
		 * SDS Role Identifier
		 * @return String object
		 */	
		public String getAuthorRoleID() {
			return (String)getValue("AuthorRoleID");
		}
		
		
		
		
		/**
		 * SDS Role Identifier
		 * @param AuthorRoleID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setAuthorRoleID(String AuthorRoleID) {
			setValue("AuthorRoleID", AuthorRoleID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getSenderASIDBLAH() {
			return (String)getValue("SenderASIDBLAH");
		}
		
		
		
		
		/**
		 * 
		 * @param SenderASIDBLAH value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setSenderASIDBLAH(String SenderASIDBLAH) {
			setValue("SenderASIDBLAH", SenderASIDBLAH);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getSenderASIDBLAH2() {
			return (String)getValue("SenderASIDBLAH2");
		}
		
		
		
		
		/**
		 * 
		 * @param SenderASIDBLAH2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setSenderASIDBLAH2(String SenderASIDBLAH2) {
			setValue("SenderASIDBLAH2", SenderASIDBLAH2);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getSenderASID() {
			return (String)getValue("SenderASID");
		}
		
		
		
		
		/**
		 * 
		 * @param SenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setSenderASID(String SenderASID) {
			setValue("SenderASID", SenderASID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getWorkstationIDOID() {
			return (String)getValue("WorkstationIDOID");
		}
		
		
		
		
		/**
		 * 
		 * @param WorkstationIDOID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setWorkstationIDOID(String WorkstationIDOID) {
			setValue("WorkstationIDOID", WorkstationIDOID);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getWorkstationIDBLAH() {
			return (String)getValue("WorkstationIDBLAH");
		}
		
		
		
		
		/**
		 * 
		 * @param WorkstationIDBLAH value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setWorkstationIDBLAH(String WorkstationIDBLAH) {
			setValue("WorkstationIDBLAH", WorkstationIDBLAH);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getWorkstationIDBLAH2() {
			return (String)getValue("WorkstationIDBLAH2");
		}
		
		
		
		
		/**
		 * 
		 * @param WorkstationIDBLAH2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setWorkstationIDBLAH2(String WorkstationIDBLAH2) {
			setValue("WorkstationIDBLAH2", WorkstationIDBLAH2);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getWorkstationID() {
			return (String)getValue("WorkstationID");
		}
		
		
		
		
		/**
		 * 
		 * @param WorkstationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setWorkstationID(String WorkstationID) {
			setValue("WorkstationID", WorkstationID);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of DetectedIssueEvent objects
		 */
		public List<DetectedIssueEvent> getErrorCodes() {
			return (List<DetectedIssueEvent>)getValue("ErrorCodes");
		}
		
		/**
		 * 
		 * @param ErrorCodes value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setErrorCodes(List ErrorCodes) {
			setValue("ErrorCodes", ErrorCodes);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a DetectedIssueEvent object, but this method can be called
		 * multiple times to add additional DetectedIssueEvent objects.
		 * @param ErrorCodes value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse addErrorCodes(DetectedIssueEvent ErrorCodes) {
			addMultivalue("ErrorCodes", ErrorCodes);
			return this;
		}
		
		
		/**
		 * The spine query response payload
		 * @return SpinePayload object
		 */	
		public SpinePayload getResponsePayload() {
			return (SpinePayload)getValue("ResponsePayload");
		}
		
		
		
		
		/**
		 * The spine query response payload
		 * @param ResponsePayload value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setResponsePayload(SpinePayload ResponsePayload) {
			setValue("ResponsePayload", ResponsePayload);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "QueryResponse" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse
		 * @return String object
		 */	
		public String getQueryResponse() {
			return (String)getValue("QueryResponse");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "QueryResponse" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse
		 * @return QueryResponse enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse getQueryResponseEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse.getByCode((String)getValue("QueryResponse"));
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "QueryResponse" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.QueryResponse
		 * @param QueryResponse value to set
		 * @return this To allow the use of the builder pattern
		 */
		public QueryResponse setQueryResponse(String QueryResponse) {
			setValue("QueryResponse", QueryResponse);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"AuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorAgentPersonClassCode", new Field(
												"AuthorAgentPersonClassCode",
												"x:author/x:AgentPersonSDS/@classCode",
												"AGNT",
												"AuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorUserRoleProfileID", new Field(
												"AuthorUserRoleProfileID",
												"x:author/x:AgentPersonSDS/x:id/@extension",
												"SDS User Role Profile Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorURPIDOID", new Field(
												"AuthorURPIDOID",
												"x:author/x:AgentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.67",
												"AuthorUserRoleProfileID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonClassCode", new Field(
												"AuthorPersonClassCode",
												"x:author/x:AgentPersonSDS/x:agentPersonSDS/@classCode",
												"PSN",
												"AuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPersonDeterminerCode", new Field(
												"AuthorPersonDeterminerCode",
												"x:author/x:AgentPersonSDS/x:agentPersonSDS/@determinerCode",
												"INSTANCE",
												"AuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorUUID", new Field(
												"AuthorUUID",
												"x:author/x:AgentPersonSDS/x:agentPersonSDS/x:id/@extension",
												"SDS User UUID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorUUIDOID", new Field(
												"AuthorUUIDOID",
												"x:author/x:AgentPersonSDS/x:agentPersonSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.65",
												"AuthorUUID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorPartTypeCode", new Field(
												"AuthorPartTypeCode",
												"x:author/x:AgentPersonSDS/x:part/@typeCode",
												"PART",
												"AuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorRoleClassCode", new Field(
												"AuthorRoleClassCode",
												"x:author/x:AgentPersonSDS/x:part/x:partSDSRole/@classCode",
												"ROL",
												"AuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorRoleID", new Field(
												"AuthorRoleID",
												"x:author/x:AgentPersonSDS/x:part/x:partSDSRole/x:id/@extension",
												"SDS Role Identifier",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorRoleIDOID", new Field(
												"AuthorRoleIDOID",
												"x:author/x:AgentPersonSDS/x:part/x:partSDSRole/x:id/@root",
												"1.2.826.0.1285.0.2.1.104",
												"AuthorRoleID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDASIDOID", new Field(
												"SenderASIDASIDOID",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDTypeCode", new Field(
												"SenderASIDTypeCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/@typeCode",
												"AUT",
												"SenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDClassCode", new Field(
												"SenderASIDClassCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/@classCode",
												"AGNT",
												"SenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDSystemClassCode", new Field(
												"SenderASIDSystemClassCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@classCode",
												"DEV",
												"SenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDSystemDeterminerCode", new Field(
												"SenderASIDSystemDeterminerCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@determinerCode",
												"INSTANCE",
												"SenderASID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDBLAH", new Field(
												"SenderASIDBLAH",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDBLAH2", new Field(
												"SenderASIDBLAH2",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam2",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASID", new Field(
												"SenderASID",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDOID", new Field(
												"WorkstationIDOID",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@root",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDTypeCode", new Field(
												"WorkstationIDTypeCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/@typeCode",
												"AUT",
												"WorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDClassCode", new Field(
												"WorkstationIDClassCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/@classCode",
												"AGNT",
												"WorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDSystemClassCode", new Field(
												"WorkstationIDSystemClassCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@classCode",
												"DEV",
												"WorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDSystemDeterminerCode", new Field(
												"WorkstationIDSystemDeterminerCode",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/@determinerCode",
												"INSTANCE",
												"WorkstationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDBLAH", new Field(
												"WorkstationIDBLAH",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationIDBLAH2", new Field(
												"WorkstationIDBLAH2",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:adam2",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("WorkstationID", new Field(
												"WorkstationID",
												"x:author1[x:AgentSystemSDS/x:agentSystemSDS/x:id/@root!='1.2.826.0.1285.0.2.0.107']/x:AgentSystemSDS/x:agentSystemSDS/x:id/@extension",
												"",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReasonTypeCode", new Field(
												"ReasonTypeCode",
												"x:reason/@typeCode",
												"RSON",
												"ErrorCodes",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ErrorCodes", new Field(
												"ErrorCodes",
												"x:reason/x:justifyingDetectedIssueEvent",
												"",
												"",
												"",
												"",
												"DetectedIssueEvent",
												"",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsePayload", new Field(
												"ResponsePayload",
												"x:subject",
												"The spine query response payload",
												"",
												"",
												"",
												"SpinePayload",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponsePayloadTypeCode", new Field(
												"ResponsePayloadTypeCode",
												"x:subject/@typeCode",
												"SUBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("QueryAckType", new Field(
												"QueryAckType",
												"x:queryAck/@type",
												"QueryAck",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("QueryResponse", new Field(
												"QueryResponse",
												"x:queryAck/x:queryResponseCode/@code",
												"",
												"",
												"",
												"QueryResponse",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public QueryResponse() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public QueryResponse(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	