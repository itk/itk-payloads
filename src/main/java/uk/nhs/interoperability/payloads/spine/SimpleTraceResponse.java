/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SimpleTraceResponse object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Address Address}&gt; Address</li>
 * <li>{@link PDSPatientID PDSPatientID} NHSNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; Telecom</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} InformationSensitivity</li>
 * <li>String AdministrativeGender</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DateOfBirth</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DeceasedTime</li>
 * <li>String MultipleBirthOrderNumber</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName}&gt; PatientName</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.DateRange DateRange} EffectiveTime</li>
 * <li>String RegisteredGPID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PreviousNhsContact</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} DeathNotification</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SimpleTraceResponse extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "SimpleTraceResponse";
		protected static final String shortName = "";
		protected static final String rootNode = "PdsTraceMatch";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Address Address
		 * @param NHSNumber NHSNumber
		 * @param Telecom Telecom
		 * @param InformationSensitivity InformationSensitivity
		 * @param AdministrativeGender AdministrativeGender
		 * @param DateOfBirth DateOfBirth
		 * @param DeceasedTime DeceasedTime
		 * @param MultipleBirthOrderNumber MultipleBirthOrderNumber
		 * @param PatientName PatientName
		 * @param EffectiveTime EffectiveTime
		 * @param RegisteredGPID RegisteredGPID
		 * @param PreviousNhsContact PreviousNhsContact
		 * @param DeathNotification DeathNotification
		 */
	    public SimpleTraceResponse(
		List<uk.nhs.interoperability.payloads.commontypes.Address> Address, PDSPatientID NHSNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> Telecom, CodedValue InformationSensitivity, String AdministrativeGender, HL7Date DateOfBirth, HL7Date DeceasedTime, String MultipleBirthOrderNumber, 
		List<uk.nhs.interoperability.payloads.commontypes.PersonName> PatientName, uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime, String RegisteredGPID, CodedValue PreviousNhsContact, CodedValue DeathNotification) {
			fields = new LinkedHashMap<String, Object>();
			
			setAddress(Address);
			setNHSNumber(NHSNumber);
			setTelecom(Telecom);
			setInformationSensitivity(InformationSensitivity);
			setAdministrativeGender(AdministrativeGender);
			setDateOfBirth(DateOfBirth);
			setDeceasedTime(DeceasedTime);
			setMultipleBirthOrderNumber(MultipleBirthOrderNumber);
			setPatientName(PatientName);
			setEffectiveTime(EffectiveTime);
			setRegisteredGPID(RegisteredGPID);
			setPreviousNhsContact(PreviousNhsContact);
			setDeathNotification(DeathNotification);
		}
	
		/**
		 * Patient Address
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Address objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Address> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Address>)getValue("Address");
		}
		
		/**
		 * Patient Address
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * Patient Address
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Address object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Address objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse addAddress(uk.nhs.interoperability.payloads.commontypes.Address Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @return PDSPatientID object
		 */	
		public PDSPatientID getNHSNumber() {
			return (PDSPatientID)getValue("NHSNumber");
		}
		
		
		
		
		/**
		 * 
		 * <br><br>This field is MANDATORY
		 * @param NHSNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setNHSNumber(PDSPatientID NHSNumber) {
			setValue("NHSNumber", NHSNumber);
			return this;
		}
		
		
		/**
		 * Patient Telephone Number
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelecom() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("Telecom");
		}
		
		/**
		 * Patient Telephone Number
		 * @param Telecom value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setTelecom(List Telecom) {
			setValue("Telecom", Telecom);
			return this;
		}
		
		/**
		 * Patient Telephone Number
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param Telecom value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse addTelecom(uk.nhs.interoperability.payloads.commontypes.Telecom Telecom) {
			addMultivalue("Telecom", Telecom);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * @return CodedValue object
		 */	
		public CodedValue getInformationSensitivity() {
			return (CodedValue)getValue("InformationSensitivity");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * @return InformationSensitivity enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity getInformationSensitivityEnum() {
			CodedValue cv = (CodedValue)getValue("InformationSensitivity");
			VocabularyEntry entry = VocabularyFactory.getVocab("InformationSensitivity", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * @param InformationSensitivity value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setInformationSensitivity(CodedValue InformationSensitivity) {
			setValue("InformationSensitivity", InformationSensitivity);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "InformationSensitivity" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.InformationSensitivity
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param InformationSensitivity value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setInformationSensitivity(VocabularyEntry InformationSensitivity) {
			Code c = new CodedValue(InformationSensitivity);
			setValue("InformationSensitivity", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return String object
		 */	
		public String getAdministrativeGender() {
			return (String)getValue("AdministrativeGender");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getAdministrativeGenderEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Sex.getByCode((String)getValue("AdministrativeGender"));
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param AdministrativeGender value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setAdministrativeGender(String AdministrativeGender) {
			setValue("AdministrativeGender", AdministrativeGender);
			return this;
		}
		
		
		/**
		 * Date
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * Date
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * Date
		 * @return HL7Date object
		 */	
		public HL7Date getDeceasedTime() {
			return (HL7Date)getValue("DeceasedTime");
		}
		
		
		
		
		/**
		 * Date
		 * @param DeceasedTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setDeceasedTime(HL7Date DeceasedTime) {
			setValue("DeceasedTime", DeceasedTime);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getMultipleBirthOrderNumber() {
			return (String)getValue("MultipleBirthOrderNumber");
		}
		
		
		
		
		/**
		 * 
		 * @param MultipleBirthOrderNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setMultipleBirthOrderNumber(String MultipleBirthOrderNumber) {
			setValue("MultipleBirthOrderNumber", MultipleBirthOrderNumber);
			return this;
		}
		
		
		/**
		 * Person name
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PersonName objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PersonName> getPatientName() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PersonName>)getValue("PatientName");
		}
		
		/**
		 * Person name
		 * @param PatientName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setPatientName(List PatientName) {
			setValue("PatientName", PatientName);
			return this;
		}
		
		/**
		 * Person name
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PersonName object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PersonName objects.
		 * @param PatientName value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse addPatientName(uk.nhs.interoperability.payloads.commontypes.PersonName PatientName) {
			addMultivalue("PatientName", PatientName);
			return this;
		}
		
		
		/**
		 * A date interval during which the patient care provision was effective
		 * @return uk.nhs.interoperability.payloads.commontypes.DateRange object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.DateRange getEffectiveTime() {
			return (uk.nhs.interoperability.payloads.commontypes.DateRange)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * A date interval during which the patient care provision was effective
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setEffectiveTime(uk.nhs.interoperability.payloads.commontypes.DateRange EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
		 * @return String object
		 */	
		public String getRegisteredGPID() {
			return (String)getValue("RegisteredGPID");
		}
		
		
		
		
		/**
		 * 
		 * @param RegisteredGPID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setRegisteredGPID(String RegisteredGPID) {
			setValue("RegisteredGPID", RegisteredGPID);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * @return CodedValue object
		 */	
		public CodedValue getPreviousNhsContact() {
			return (CodedValue)getValue("PreviousNhsContact");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * @return PreviousNhsContact enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact getPreviousNhsContactEnum() {
			CodedValue cv = (CodedValue)getValue("PreviousNhsContact");
			VocabularyEntry entry = VocabularyFactory.getVocab("PreviousNhsContact", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * @param PreviousNhsContact value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setPreviousNhsContact(CodedValue PreviousNhsContact) {
			setValue("PreviousNhsContact", PreviousNhsContact);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "PreviousNhsContact" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PreviousNhsContact
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PreviousNhsContact value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setPreviousNhsContact(VocabularyEntry PreviousNhsContact) {
			Code c = new CodedValue(PreviousNhsContact);
			setValue("PreviousNhsContact", c);
			return this;
		}
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * @return CodedValue object
		 */	
		public CodedValue getDeathNotification() {
			return (CodedValue)getValue("DeathNotification");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * @return DeathNotification enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification getDeathNotificationEnum() {
			CodedValue cv = (CodedValue)getValue("DeathNotification");
			VocabularyEntry entry = VocabularyFactory.getVocab("DeathNotification", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * @param DeathNotification value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setDeathNotification(CodedValue DeathNotification) {
			setValue("DeathNotification", DeathNotification);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "DeathNotification" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.DeathNotification
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param DeathNotification value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public SimpleTraceResponse setDeathNotification(VocabularyEntry DeathNotification) {
			Code c = new CodedValue(DeathNotification);
			setValue("DeathNotification", c);
			return this;
		}
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectTypeCode", new Field(
												"SubjectTypeCode",
												"x:subject/@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientRoleClassCode", new Field(
												"PatientRoleClassCode",
												"x:subject/x:patientRole/@classCode",
												"PAT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:subject/x:patientRole/x:addr",
												"Patient Address",
												"",
												"",
												"",
												"Address",
												"uk.nhs.interoperability.payloads.commontypes.",
												"10",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumber", new Field(
												"NHSNumber",
												"x:subject/x:patientRole/x:id",
												"",
												"true",
												"",
												"",
												"PDSPatientID",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Telecom", new Field(
												"Telecom",
												"x:subject/x:patientRole/x:telecom",
												"Patient Telephone Number",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformationSensitivity", new Field(
												"InformationSensitivity",
												"x:subject/x:patientRole/x:confidentialityCode",
												"",
												"false",
												"",
												"InformationSensitivity",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonClassCode", new Field(
												"PatientPersonClassCode",
												"x:subject/x:patientRole/x:patientPerson/@classCode",
												"PSN",
												"PatientName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientPersonDeterminerCode", new Field(
												"PatientPersonDeterminerCode",
												"x:subject/x:patientRole/x:patientPerson/@determinerCode",
												"INSTANCE",
												"PatientName",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AdministrativeGender", new Field(
												"AdministrativeGender",
												"x:subject/x:patientRole/x:patientPerson/x:administrativeGenderCode/@code",
												"",
												"false",
												"",
												"Sex",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:subject/x:patientRole/x:patientPerson/x:birthTime/@value",
												"Date",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeceasedTime", new Field(
												"DeceasedTime",
												"x:subject/x:patientRole/x:patientPerson/x:deceasedTime/@value",
												"Date",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MultipleBirthOrderNumber", new Field(
												"MultipleBirthOrderNumber",
												"x:subject/x:patientRole/x:patientPerson/x:multipleBirthOrderNumber/@value",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientName", new Field(
												"PatientName",
												"x:subject/x:patientRole/x:patientPerson/x:name",
												"Person name",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OtherProviderPatientClassCode", new Field(
												"OtherProviderPatientClassCode",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/@classCode",
												"PAT",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OtherProviderPatientSubjectOfTypeCode", new Field(
												"OtherProviderPatientSubjectOfTypeCode",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/@typeCode",
												"SBJ",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionClassCode", new Field(
												"PatientCareProvisionClassCode",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/@classCode",
												"PCPR",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionMoodCode", new Field(
												"PatientCareProvisionMoodCode",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/@moodCode",
												"EVN",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionCodeFixedCodedValue", new Field(
												"PatientCareProvisionCodeFixedCodedValue",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:code/@code",
												"1",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientCareProvisionCodeFixedCodedValueCodeSystem", new Field(
												"PatientCareProvisionCodeFixedCodedValueCodeSystem",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.37",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:effectiveTime",
												"A date interval during which the patient care provision was effective",
												"false",
												"",
												"",
												"DateRange",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PerformerTypeCode", new Field(
												"PerformerTypeCode",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/@typeCode",
												"PRF",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssignedOrganizationClassCode", new Field(
												"AssignedOrganizationClassCode",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/x:assignedOrganization/@classCode",
												"ASSIGNED",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPID", new Field(
												"RegisteredGPID",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/x:assignedOrganization/x:id/@extension",
												"",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RegisteredGPIDOID", new Field(
												"RegisteredGPIDOID",
												"x:subject/x:patientRole/x:patientPerson/x:playedOtherProviderPatient/x:subjectOf/x:patientCareProvision/x:performer/x:assignedOrganization/x:id/@root",
												"2.16.840.1.113883.2.1.4.3",
												"RegisteredGPID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeCode", new Field(
												"TypeCode",
												"x:subject/x:patientRole/x:subjectOf2/@typeCode",
												"SBJ",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactClassCode", new Field(
												"previousNhsContactClassCode",
												"x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/@classCode",
												"OBS",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactMoodCode", new Field(
												"previousNhsContactMoodCode",
												"x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/@moodCode",
												"EVN",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactCodeFixedCodedValue", new Field(
												"previousNhsContactCodeFixedCodedValue",
												"x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/x:code/@code",
												"17",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("previousNhsContactCodeFixedCodedValueCodeSystem", new Field(
												"previousNhsContactCodeFixedCodedValueCodeSystem",
												"x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.35",
												"PreviousNhsContact",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousNhsContact", new Field(
												"PreviousNhsContact",
												"x:subject/x:patientRole/x:subjectOf2/x:previousNhsContact/x:value",
												"",
												"false",
												"",
												"PreviousNhsContact",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SubjectOf3TypeCode", new Field(
												"SubjectOf3TypeCode",
												"x:subject/x:patientRole/x:subjectOf3/@typeCode",
												"SBJ",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationClassCode", new Field(
												"DeathNotificationClassCode",
												"x:subject/x:patientRole/x:subjectOf3/x:deathNotification/@classCode",
												"OBS",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationMoodCode", new Field(
												"DeathNotificationMoodCode",
												"x:subject/x:patientRole/x:subjectOf3/x:deathNotification/@moodCode",
												"EVN",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationCodeFixedCodedValue", new Field(
												"DeathNotificationCodeFixedCodedValue",
												"x:subject/x:patientRole/x:subjectOf3/x:deathNotification/x:code/@code",
												"3",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotificationCodeFixedCodedValueCodeSystem", new Field(
												"DeathNotificationCodeFixedCodedValueCodeSystem",
												"x:subject/x:patientRole/x:subjectOf3/x:deathNotification/x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.35",
												"DeathNotification",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DeathNotification", new Field(
												"DeathNotification",
												"x:subject/x:patientRole/x:subjectOf3/x:deathNotification/x:value",
												"",
												"false",
												"",
												"DeathNotification",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SimpleTraceResponse() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SimpleTraceResponse(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	