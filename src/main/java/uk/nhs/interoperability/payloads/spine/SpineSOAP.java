/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SpineSOAP object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String MessageID</li>
 * <li>String Action</li>
 * <li>String To</li>
 * <li>String From</li>
 * <li>String ReceiverASID</li>
 * <li>String SenderASID</li>
 * <li>String ReplyAddress</li>
 * <li>{@link SpineSOAPBody SpineSOAPBody} Payload</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SpineSOAP extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "SpineSOAP";
		protected static final String shortName = "";
		protected static final String rootNode = "Envelope";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param MessageID MessageID
		 * @param Action Action
		 * @param To To
		 * @param From From
		 * @param ReceiverASID ReceiverASID
		 * @param SenderASID SenderASID
		 * @param ReplyAddress ReplyAddress
		 * @param Payload Payload
		 */
	    public SpineSOAP(String MessageID, String Action, String To, String From, String ReceiverASID, String SenderASID, String ReplyAddress, SpineSOAPBody Payload) {
			fields = new LinkedHashMap<String, Object>();
			
			setMessageID(MessageID);
			setAction(Action);
			setTo(To);
			setFrom(From);
			setReceiverASID(ReceiverASID);
			setSenderASID(SenderASID);
			setReplyAddress(ReplyAddress);
			setPayload(Payload);
		}
	
		/**
		 * UUID of SOAP message (e.g. uuid:1E31730D-9EF9-11E4-B608-01335BA1706F)
		 * @return String object
		 */	
		public String getMessageID() {
			return (String)getValue("MessageID");
		}
		
		
		
		
		/**
		 * UUID of SOAP message (e.g. uuid:1E31730D-9EF9-11E4-B608-01335BA1706F)
		 * @param MessageID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setMessageID(String MessageID) {
			setValue("MessageID", MessageID);
			return this;
		}
		
		
		/**
		 * The SOAP Action (e.g. urn:nhs:names:services:pdsquery/QUPA_IN000005UK01)
		 * @return String object
		 */	
		public String getAction() {
			return (String)getValue("Action");
		}
		
		
		
		
		/**
		 * The SOAP Action (e.g. urn:nhs:names:services:pdsquery/QUPA_IN000005UK01)
		 * @param Action value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setAction(String Action) {
			setValue("Action", Action);
			return this;
		}
		
		
		/**
		 * The SOAP To Address (e.g. https://pds-sync.nis1.national.ncrs.nhs.uk/syncservice-pds/pds)
		 * @return String object
		 */	
		public String getTo() {
			return (String)getValue("To");
		}
		
		
		
		
		/**
		 * The SOAP To Address (e.g. https://pds-sync.nis1.national.ncrs.nhs.uk/syncservice-pds/pds)
		 * @param To value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setTo(String To) {
			setValue("To", To);
			return this;
		}
		
		
		/**
		 * The SOAP From Address
		 * @return String object
		 */	
		public String getFrom() {
			return (String)getValue("From");
		}
		
		
		
		
		/**
		 * The SOAP From Address
		 * @param From value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setFrom(String From) {
			setValue("From", From);
			return this;
		}
		
		
		/**
		 * The Receiver ASID
		 * @return String object
		 */	
		public String getReceiverASID() {
			return (String)getValue("ReceiverASID");
		}
		
		
		
		
		/**
		 * The Receiver ASID
		 * @param ReceiverASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setReceiverASID(String ReceiverASID) {
			setValue("ReceiverASID", ReceiverASID);
			return this;
		}
		
		
		/**
		 * The Receiver ASID
		 * @return String object
		 */	
		public String getSenderASID() {
			return (String)getValue("SenderASID");
		}
		
		
		
		
		/**
		 * The Receiver ASID
		 * @param SenderASID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setSenderASID(String SenderASID) {
			setValue("SenderASID", SenderASID);
			return this;
		}
		
		
		/**
		 * The SOAP ReplyTo Address
		 * @return String object
		 */	
		public String getReplyAddress() {
			return (String)getValue("ReplyAddress");
		}
		
		
		
		
		/**
		 * The SOAP ReplyTo Address
		 * @param ReplyAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setReplyAddress(String ReplyAddress) {
			setValue("ReplyAddress", ReplyAddress);
			return this;
		}
		
		
		/**
		 * The message payload
		 * @return SpineSOAPBody object
		 */	
		public SpineSOAPBody getPayload() {
			return (SpineSOAPBody)getValue("Payload");
		}
		
		
		
		
		/**
		 * The message payload
		 * @param Payload value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SpineSOAP setPayload(SpineSOAPBody Payload) {
			setValue("Payload", Payload);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("MessageID", new Field(
												"MessageID",
												"SOAP-ENV:Header/wsa:MessageID",
												"UUID of SOAP message (e.g. uuid:1E31730D-9EF9-11E4-B608-01335BA1706F)",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Action", new Field(
												"Action",
												"SOAP-ENV:Header/wsa:Action",
												"The SOAP Action (e.g. urn:nhs:names:services:pdsquery/QUPA_IN000005UK01)",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("To", new Field(
												"To",
												"SOAP-ENV:Header/wsa:To",
												"The SOAP To Address (e.g. https://pds-sync.nis1.national.ncrs.nhs.uk/syncservice-pds/pds)",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("From", new Field(
												"From",
												"SOAP-ENV:Header/wsa:From/wsa:Address",
												"The SOAP From Address",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverASID", new Field(
												"ReceiverASID",
												"SOAP-ENV:Header/hl7:communicationFunctionRcv/hl7:device/hl7:id/@extension",
												"The Receiver ASID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReceiverASIDOID", new Field(
												"ReceiverASIDOID",
												"SOAP-ENV:Header/hl7:communicationFunctionRcv/hl7:device/hl7:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASID", new Field(
												"SenderASID",
												"SOAP-ENV:Header/hl7:communicationFunctionSnd/hl7:device/hl7:id/@extension",
												"The Receiver ASID",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SenderASIDOID", new Field(
												"SenderASIDOID",
												"SOAP-ENV:Header/hl7:communicationFunctionSnd/hl7:device/hl7:id/@root",
												"1.2.826.0.1285.0.2.0.107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ReplyAddress", new Field(
												"ReplyAddress",
												"SOAP-ENV:Header/wsa:ReplyTo/wsa:Address",
												"The SOAP ReplyTo Address",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Payload", new Field(
												"Payload",
												"SOAP-ENV:Body",
												"The message payload",
												"",
												"",
												"",
												"SpineSOAPBody",
												"",
												"",
												"",
												"",
												"SOAP-ENV:Body/*/x:interactionId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/", true);
			namespaces.addNamespace("wsa", "http://schemas.xmlsoap.org/ws/2004/08/addressing", false);
			namespaces.addNamespace("hl7", "urn:hl7-org:v3", false);
			namespaces.addNamespace("x", "urn:hl7-org:v3", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SpineSOAP() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SpineSOAP(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	