/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PdsTraceQuery object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link String String} Postcode</li>
 * <li>String AddressUse</li>
 * <li>{@link String String} Gender</li>
 * <li>{@link HL7Date HL7Date} DateOfBirth</li>
 * <li>{@link HL7Date HL7Date} DateOfDeath</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SMSPPersonName SMSPPersonName} Name</li>
 * <li>String NameType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PdsTraceQuery extends AbstractPayload implements Payload , SpinePayload {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "PdsTraceQuery";
		protected static final String shortName = "";
		protected static final String rootNode = "query";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Postcode Postcode
		 * @param AddressUse AddressUse
		 * @param Gender Gender
		 * @param DateOfBirth DateOfBirth
		 * @param DateOfDeath DateOfDeath
		 * @param Name Name
		 * @param NameType NameType
		 */
	    public PdsTraceQuery(String Postcode, String AddressUse, String Gender, HL7Date DateOfBirth, HL7Date DateOfDeath, uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name, String NameType) {
			fields = new LinkedHashMap<String, Object>();
			
			setPostcode(Postcode);
			setAddressUse(AddressUse);
			setGender(Gender);
			setDateOfBirth(DateOfBirth);
			setDateOfDeath(DateOfDeath);
			setName(Name);
			setNameType(NameType);
		}
	
		/**
		 * Postcode Query Parameter
		 * @return String object
		 */	
		public String getPostcode() {
			return (String)getValue("Postcode");
		}
		
		
		
		
		/**
		 * Postcode Query Parameter
		 * @param Postcode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setPostcode(String Postcode) {
			setValue("Postcode", Postcode);
			return this;
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return String object
		 */	
		public String getAddressUse() {
			return (String)getValue("AddressUse");
		}
		
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return AddressType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AddressType getAddressUseEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AddressType.getByCode((String)getValue("AddressUse"));
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @param AddressUse value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setAddressUse(String AddressUse) {
			setValue("AddressUse", AddressUse);
			return this;
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return String object
		 */	
		public String getGender() {
			return (String)getValue("Gender");
		}
		
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getGenderEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.Sex.getByCode((String)getValue("Gender"));
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Gender value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setGender(String Gender) {
			setValue("Gender", Gender);
			return this;
		}
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * DateOfDeath Query Parameter
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfDeath() {
			return (HL7Date)getValue("DateOfDeath");
		}
		
		
		
		
		/**
		 * DateOfDeath Query Parameter
		 * @param DateOfDeath value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setDateOfDeath(HL7Date DateOfDeath) {
			setValue("DateOfDeath", DateOfDeath);
			return this;
		}
		
		
		/**
		 * Name Query Parameter
		 * @return uk.nhs.interoperability.payloads.commontypes.SMSPPersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SMSPPersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.SMSPPersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Name Query Parameter
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setName(uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @return String object
		 */	
		public String getNameType() {
			return (String)getValue("NameType");
		}
		
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @return PersonNameType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType getNameTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType.getByCode((String)getValue("NameType"));
		}
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @param NameType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PdsTraceQuery setNameType(String NameType) {
			setValue("NameType", NameType);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("PostcodeSemanticsText", new Field(
												"PostcodeSemanticsText",
												"x:person.address/x:semanticsText",
												"Person.address",
												"Postcode",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Postcode", new Field(
												"Postcode",
												"x:person.address/x:value/x:postalCode",
												"Postcode Query Parameter",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressUse", new Field(
												"AddressUse",
												"x:person.address/x:value/@use",
												"Optional address 'use' that may be used to specify the purpose of the address",
												"false",
												"",
												"AddressType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GenderSemanticsText", new Field(
												"GenderSemanticsText",
												"x:person.administrativeGenderCode/x:semanticsText",
												"Person.administrativeGenderCode",
												"Gender",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Gender", new Field(
												"Gender",
												"x:person.administrativeGenderCode/x:value/@code",
												"Gender Query Parameter",
												"",
												"",
												"Sex",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirthSemanticsText", new Field(
												"DateOfBirthSemanticsText",
												"x:person.birthTime/x:semanticsText",
												"Person.birthTime",
												"DateOfBirth",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:person.birthTime/x:value/@value",
												"DateOfBirth Query Parameter",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfDeathSemanticsText", new Field(
												"DateOfDeathSemanticsText",
												"x:person.deceasedTime/x:semanticsText",
												"Person.deceasedTime",
												"DateOfDeath",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfDeath", new Field(
												"DateOfDeath",
												"x:person.deceasedTime/x:value/@value",
												"DateOfDeath Query Parameter",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameSemanticsText", new Field(
												"NameSemanticsText",
												"x:person.name/x:semanticsText",
												"Person.name",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:person.name/x:value",
												"Name Query Parameter",
												"",
												"",
												"",
												"SMSPPersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameType", new Field(
												"NameType",
												"x:person.name/x:value/@use",
												"Name use code",
												"false",
												"",
												"PersonNameType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PdsTraceQuery() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PdsTraceQuery(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	
	
	