/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.spine;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the PDSPatientID object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String PatientIDType</li>
 * <li>String PatientID</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class PDSPatientID extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "spineFieldConfig";
		protected static final String name = "PDSPatientID";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.spine";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param PatientIDType String
		 * @param PatientID String
		 */
		public PDSPatientID(String PatientIDType, String PatientID) {
			fields = new LinkedHashMap<String, Object>();
			
			setPatientIDType(PatientIDType);
			setPatientID(PatientID);
		}

		/**
		 * Specifies whether this is an NHS Number, an Old Format NHS Number, or a Temporary NHS Number (issued by an NHAIS registration authority)
  		 * <br>NOTE: This field should be populated using the "PDSPatientIDType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getPatientIDType() {
			return (String)getValue("PatientIDType");
		}
		
		
		
		/**
		 * Specifies whether this is an NHS Number, an Old Format NHS Number, or a Temporary NHS Number (issued by an NHAIS registration authority)
  		 * <br>NOTE: This field should be populated using the "PDSPatientIDType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType
		 * <br><br>This field is MANDATORY
		 * @return PDSPatientIDType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType getPatientIDTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType.getByCode((String)getValue("PatientIDType"));
		}
		
		
		/**
		 * Specifies whether this is an NHS Number, an Old Format NHS Number, or a Temporary NHS Number (issued by an NHAIS registration authority)
  		 * <br>NOTE: This field should be populated using the "PDSPatientIDType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PDSPatientIDType
		 * <br><br>This field is MANDATORY
		 * @param PatientIDType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PDSPatientID setPatientIDType(String PatientIDType) {
			setValue("PatientIDType", PatientIDType);
			return this;
		}
		
		
		/**
		 * This is the patient's NHS Number
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getPatientID() {
			return (String)getValue("PatientID");
		}
		
		
		
		
		/**
		 * This is the patient's NHS Number
		 * <br><br>This field is MANDATORY
		 * @param PatientID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public PDSPatientID setPatientID(String PatientID) {
			setValue("PatientID", PatientID);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("PatientIDType", new Field(
												"PatientIDType",
												"",
												"Specifies whether this is an NHS Number, an Old Format NHS Number, or a Temporary NHS Number (issued by an NHAIS registration authority)",
												"true",
												"",
												"PDSPatientIDType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientID", new Field(
												"PatientID",
												"@extension",
												"This is the patient's NHS Number",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumberRoot", new Field(
												"NHSNumberRoot",
												"@root",
												"2.16.840.1.113883.2.1.4.1",
												"",
												"",
												"PatientIDType",
												"NHSNo",
												"",
												""
												));
	
		put("OldFormatNHSNumberRoot", new Field(
												"OldFormatNHSNumberRoot",
												"@root",
												"2.16.840.1.113883.2.1.3.2.4.3",
												"",
												"",
												"PatientIDType",
												"OldNHSNo",
												"",
												""
												));
	
		put("TempNHSNumberRoot", new Field(
												"TempNHSNumberRoot",
												"@root",
												"2.16.840.1.113883.2.1.3.2.4.3",
												"",
												"",
												"PatientIDType",
												"TempNHSNo",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public PDSPatientID() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public PDSPatientID(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}



	
