/**
 * Transfer of Care (eDischarge) Payload - NOTE: This aligns with Draft B of the spec, but
 * does not currently include the coded entries that were added in Draft B.  
 */
package uk.nhs.interoperability.payloads.toc_edischarge_draftB;