/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.toc_edischarge_draftB;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the DocumentParticipant object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String ParticipantTypeCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} FunctionCode</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Participant Participant} Participant</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class DocumentParticipant extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "tocFieldConfig";
		protected static final String name = "DocumentParticipant";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.toc_edischarge_draftB";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param Participant Participant
		 * @param FunctionCode CodedValue
		 */
		public DocumentParticipant(uk.nhs.interoperability.payloads.templates.Participant Participant, CodedValue FunctionCode) {
			fields = new LinkedHashMap<String, Object>();
			
			setParticipant(Participant);
			setFunctionCode(FunctionCode);
		}

		/**
		 * A code from the HL7 vocabulary ParticipationType to identify the type of participation
  		 * <br>NOTE: This field should be populated using the "ParticipationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType
		 * @return String object
		 */	
		public String getParticipantTypeCode() {
			return (String)getValue("ParticipantTypeCode");
		}
		
		
		
		/**
		 * A code from the HL7 vocabulary ParticipationType to identify the type of participation
  		 * <br>NOTE: This field should be populated using the "ParticipationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType
		 * @return ParticipationType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType getParticipantTypeCodeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType.getByCode((String)getValue("ParticipantTypeCode"));
		}
		
		
		/**
		 * A code from the HL7 vocabulary ParticipationType to identify the type of participation
  		 * <br>NOTE: This field should be populated using the "ParticipationType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationType
		 * @param ParticipantTypeCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipant setParticipantTypeCode(String ParticipantTypeCode) {
			setValue("ParticipantTypeCode", ParticipantTypeCode);
			return this;
		}
		
		
		/**
		 * A code from the HL7 vocabulary ParticipationFunction to describe the function of the participant
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * @return CodedValue object
		 */	
		public CodedValue getFunctionCode() {
			return (CodedValue)getValue("FunctionCode");
		}
		
		
		
		/**
		 * A code from the HL7 vocabulary ParticipationFunction to describe the function of the participant
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * @return ParticipationFunction enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction getFunctionCodeEnum() {
			CodedValue cv = (CodedValue)getValue("FunctionCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("ParticipationFunction", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction)entry;
		}
		
		
		/**
		 * A code from the HL7 vocabulary ParticipationFunction to describe the function of the participant
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * @param FunctionCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipant setFunctionCode(CodedValue FunctionCode) {
			setValue("FunctionCode", FunctionCode);
			return this;
		}
		
		
		/**
		 * A code from the HL7 vocabulary ParticipationFunction to describe the function of the participant
  		 * <br>NOTE: This field should be populated using the "ParticipationFunction" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.ParticipationFunction
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param FunctionCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipant setFunctionCode(VocabularyEntry FunctionCode) {
			Code c = new CodedValue(FunctionCode);
			setValue("FunctionCode", c);
			return this;
		}
		
		/**
		 * Participant Details
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.templates.Participant object
		 */	
		public uk.nhs.interoperability.payloads.templates.Participant getParticipant() {
			return (uk.nhs.interoperability.payloads.templates.Participant)getValue("Participant");
		}
		
		
		
		
		/**
		 * Participant Details
		 * <br><br>This field is MANDATORY
		 * @param Participant value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DocumentParticipant setParticipant(uk.nhs.interoperability.payloads.templates.Participant Participant) {
			setValue("Participant", Participant);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ParticipantTypeCode", new Field(
												"ParticipantTypeCode",
												"@typeCode",
												"A code from the HL7 vocabulary ParticipationType to identify the type of participation",
												"",
												"",
												"ParticipationType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ParticipantContextControlCode", new Field(
												"ParticipantContextControlCode",
												"@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ParticipantContentId", new Field(
												"ParticipantContentId",
												"npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"Participant",
												""
												));
	
		put("ParticipantContentIdRoot", new Field(
												"ParticipantContentIdRoot",
												"npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("FunctionCode", new Field(
												"FunctionCode",
												"x:functionCode",
												"A code from the HL7 vocabulary ParticipationFunction to describe the function of the participant",
												"",
												"",
												"ParticipationFunction",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Participant", new Field(
												"Participant",
												"x:associatedEntity",
												"Participant Details",
												"true",
												"",
												"",
												"Participant",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:associatedEntity/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public DocumentParticipant() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public DocumentParticipant(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	