/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.childscreeningv2;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ClinicalDocument object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String DocumentId</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ConfidentialityCode</li>
 * <li>String DocumentSetId</li>
 * <li>String DocumentVersionNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.ChildPatientUniversal ChildPatientUniversal} ChildPatient</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor ChildScreeningAuthor} Author</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal CustodianOrganizationUniversal} CustodianOrganisation</li>
 * <li>List&lt;{@link PrimaryRecipient PrimaryRecipient}&gt; PrimaryRecipients</li>
 * <li>List&lt;{@link InformationOnlyRecipient InformationOnlyRecipient}&gt; InformationOnlyRecipients</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Consent Consent} AuthorizingConsent</li>
 * <li>String MainDocumentSectionID</li>
 * <li>List&lt;{@link CodedSections CodedSections}&gt; CodedSections</li>
 * <li>List&lt;{@link TextSections TextSections}&gt; TextSections</li>
 * <li>String PreviousDocumentVersionID</li>
 * <li>String PreviousDocumentVersionSetId</li>
 * <li>String PreviousDocumentVersionNumber</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ClinicalDocument extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "childscreeningFieldConfig";
		protected static final String name = "ClinicalDocument";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.childscreeningv2";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param DocumentId DocumentId
		 * @param EffectiveTime EffectiveTime
		 * @param ConfidentialityCode ConfidentialityCode
		 * @param DocumentSetId DocumentSetId
		 * @param DocumentVersionNumber DocumentVersionNumber
		 * @param ChildPatient ChildPatient
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param CustodianOrganisation CustodianOrganisation
		 * @param PrimaryRecipients PrimaryRecipients
		 * @param InformationOnlyRecipients InformationOnlyRecipients
		 * @param AuthorizingConsent AuthorizingConsent
		 * @param MainDocumentSectionID MainDocumentSectionID
		 * @param CodedSections CodedSections
		 * @param TextSections TextSections
		 * @param PreviousDocumentVersionID PreviousDocumentVersionID
		 * @param PreviousDocumentVersionSetId PreviousDocumentVersionSetId
		 * @param PreviousDocumentVersionNumber PreviousDocumentVersionNumber
		 */
	    public ClinicalDocument(String DocumentId, HL7Date EffectiveTime, CodedValue ConfidentialityCode, String DocumentSetId, String DocumentVersionNumber, uk.nhs.interoperability.payloads.templates.ChildPatientUniversal ChildPatient, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author, uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal CustodianOrganisation, 
		List<PrimaryRecipient> PrimaryRecipients, 
		List<InformationOnlyRecipient> InformationOnlyRecipients, uk.nhs.interoperability.payloads.templates.Consent AuthorizingConsent, String MainDocumentSectionID, 
		List<CodedSections> CodedSections, 
		List<TextSections> TextSections, String PreviousDocumentVersionID, String PreviousDocumentVersionSetId, String PreviousDocumentVersionNumber) {
			fields = new LinkedHashMap<String, Object>();
			
			setDocumentId(DocumentId);
			setEffectiveTime(EffectiveTime);
			setConfidentialityCode(ConfidentialityCode);
			setDocumentSetId(DocumentSetId);
			setDocumentVersionNumber(DocumentVersionNumber);
			setChildPatient(ChildPatient);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setCustodianOrganisation(CustodianOrganisation);
			setPrimaryRecipients(PrimaryRecipients);
			setInformationOnlyRecipients(InformationOnlyRecipients);
			setAuthorizingConsent(AuthorizingConsent);
			setMainDocumentSectionID(MainDocumentSectionID);
			setCodedSections(CodedSections);
			setTextSections(TextSections);
			setPreviousDocumentVersionID(PreviousDocumentVersionID);
			setPreviousDocumentVersionSetId(PreviousDocumentVersionSetId);
			setPreviousDocumentVersionNumber(PreviousDocumentVersionNumber);
		}
	
		/**
		 * A DCE UUID to identify this specific document and version
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentId() {
			return (String)getValue("DocumentId");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this specific document and version
		 * <br><br>This field is MANDATORY
		 * @param DocumentId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentId(String DocumentId) {
			setValue("DocumentId", DocumentId);
			return this;
		}
		
		
		/**
		 * The creation time of the CDA document
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * The creation time of the CDA document
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getConfidentialityCode() {
			return (CodedValue)getValue("ConfidentialityCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * @return x_BasicConfidentialityKind enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind getConfidentialityCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ConfidentialityCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("x_BasicConfidentialityKind", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * @param ConfidentialityCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setConfidentialityCode(CodedValue ConfidentialityCode) {
			setValue("ConfidentialityCode", ConfidentialityCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ConfidentialityCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setConfidentialityCode(VocabularyEntry ConfidentialityCode) {
			Code c = new CodedValue(ConfidentialityCode);
			setValue("ConfidentialityCode", c);
			return this;
		}
		
		/**
		 * A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentSetId() {
			return (String)getValue("DocumentSetId");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)
		 * <br><br>This field is MANDATORY
		 * @param DocumentSetId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentSetId(String DocumentSetId) {
			setValue("DocumentSetId", DocumentSetId);
			return this;
		}
		
		
		/**
		 * The version number of the document as an integer value
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentVersionNumber() {
			return (String)getValue("DocumentVersionNumber");
		}
		
		
		
		
		/**
		 * The version number of the document as an integer value
		 * <br><br>This field is MANDATORY
		 * @param DocumentVersionNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentVersionNumber(String DocumentVersionNumber) {
			setValue("DocumentVersionNumber", DocumentVersionNumber);
			return this;
		}
		
		
		/**
		 * Patient Details
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.templates.ChildPatientUniversal object
		 */	
		public uk.nhs.interoperability.payloads.templates.ChildPatientUniversal getChildPatient() {
			return (uk.nhs.interoperability.payloads.templates.ChildPatientUniversal)getValue("ChildPatient");
		}
		
		
		
		
		/**
		 * Patient Details
		 * <br><br>This field is MANDATORY
		 * @param ChildPatient value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setChildPatient(uk.nhs.interoperability.payloads.templates.ChildPatientUniversal ChildPatient) {
			setValue("ChildPatient", ChildPatient);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor object
		 */	
		public uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setAuthor(uk.nhs.interoperability.payloads.templates.ChildScreeningAuthor Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * The organisation responsible for maintaining the information in the CDA document
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal object
		 */	
		public uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal getCustodianOrganisation() {
			return (uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal)getValue("CustodianOrganisation");
		}
		
		
		
		
		/**
		 * The organisation responsible for maintaining the information in the CDA document
		 * <br><br>This field is MANDATORY
		 * @param CustodianOrganisation value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setCustodianOrganisation(uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal CustodianOrganisation) {
			setValue("CustodianOrganisation", CustodianOrganisation);
			return this;
		}
		
		
		/**
		 * Primary recipients
		 * @return List of PrimaryRecipient objects
		 */
		public List<PrimaryRecipient> getPrimaryRecipients() {
			return (List<PrimaryRecipient>)getValue("PrimaryRecipients");
		}
		
		/**
		 * Primary recipients
		 * @param PrimaryRecipients value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPrimaryRecipients(List PrimaryRecipients) {
			setValue("PrimaryRecipients", PrimaryRecipients);
			return this;
		}
		
		/**
		 * Primary recipients
		 * <br>Note: This adds a PrimaryRecipient object, but this method can be called
		 * multiple times to add additional PrimaryRecipient objects.
		 * @param PrimaryRecipients value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addPrimaryRecipients(PrimaryRecipient PrimaryRecipients) {
			addMultivalue("PrimaryRecipients", PrimaryRecipients);
			return this;
		}
		
		
		/**
		 * Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.
		 * @return List of InformationOnlyRecipient objects
		 */
		public List<InformationOnlyRecipient> getInformationOnlyRecipients() {
			return (List<InformationOnlyRecipient>)getValue("InformationOnlyRecipients");
		}
		
		/**
		 * Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.
		 * @param InformationOnlyRecipients value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setInformationOnlyRecipients(List InformationOnlyRecipients) {
			setValue("InformationOnlyRecipients", InformationOnlyRecipients);
			return this;
		}
		
		/**
		 * Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.
		 * <br>Note: This adds a InformationOnlyRecipient object, but this method can be called
		 * multiple times to add additional InformationOnlyRecipient objects.
		 * @param InformationOnlyRecipients value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addInformationOnlyRecipients(InformationOnlyRecipient InformationOnlyRecipients) {
			addMultivalue("InformationOnlyRecipients", InformationOnlyRecipients);
			return this;
		}
		
		
		/**
		 * Details of consent/authorisation for the creation/sharing of the document
		 * @return uk.nhs.interoperability.payloads.templates.Consent object
		 */	
		public uk.nhs.interoperability.payloads.templates.Consent getAuthorizingConsent() {
			return (uk.nhs.interoperability.payloads.templates.Consent)getValue("AuthorizingConsent");
		}
		
		
		
		
		/**
		 * Details of consent/authorisation for the creation/sharing of the document
		 * @param AuthorizingConsent value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setAuthorizingConsent(uk.nhs.interoperability.payloads.templates.Consent AuthorizingConsent) {
			setValue("AuthorizingConsent", AuthorizingConsent);
			return this;
		}
		
		
		/**
		 * A DCE UUID to identify the main document section
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getMainDocumentSectionID() {
			return (String)getValue("MainDocumentSectionID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify the main document section
		 * <br><br>This field is MANDATORY
		 * @param MainDocumentSectionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setMainDocumentSectionID(String MainDocumentSectionID) {
			setValue("MainDocumentSectionID", MainDocumentSectionID);
			return this;
		}
		
		
		/**
		 * A list of the coded sections making up the document
		 * @return List of CodedSections objects
		 */
		public List<CodedSections> getCodedSections() {
			return (List<CodedSections>)getValue("CodedSections");
		}
		
		/**
		 * A list of the coded sections making up the document
		 * @param CodedSections value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setCodedSections(List CodedSections) {
			setValue("CodedSections", CodedSections);
			return this;
		}
		
		/**
		 * A list of the coded sections making up the document
		 * <br>Note: This adds a CodedSections object, but this method can be called
		 * multiple times to add additional CodedSections objects.
		 * @param CodedSections value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addCodedSections(CodedSections CodedSections) {
			addMultivalue("CodedSections", CodedSections);
			return this;
		}
		
		
		/**
		 * A list of the text sections making up the document
		 * <br><br>This field is MANDATORY
		 * @return List of TextSections objects
		 */
		public List<TextSections> getTextSections() {
			return (List<TextSections>)getValue("TextSections");
		}
		
		/**
		 * A list of the text sections making up the document
		 * <br><br>This field is MANDATORY
		 * @param TextSections value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setTextSections(List TextSections) {
			setValue("TextSections", TextSections);
			return this;
		}
		
		/**
		 * A list of the text sections making up the document
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a TextSections object, but this method can be called
		 * multiple times to add additional TextSections objects.
		 * @param TextSections value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addTextSections(TextSections TextSections) {
			addMultivalue("TextSections", TextSections);
			return this;
		}
		
		
		/**
		 * A DCE UUID that identifies the specific document and version that this document replaces
		 * @return String object
		 */	
		public String getPreviousDocumentVersionID() {
			return (String)getValue("PreviousDocumentVersionID");
		}
		
		
		
		
		/**
		 * A DCE UUID that identifies the specific document and version that this document replaces
		 * @param PreviousDocumentVersionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentVersionID(String PreviousDocumentVersionID) {
			setValue("PreviousDocumentVersionID", PreviousDocumentVersionID);
			return this;
		}
		
		
		/**
		 * A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)
		 * @return String object
		 */	
		public String getPreviousDocumentVersionSetId() {
			return (String)getValue("PreviousDocumentVersionSetId");
		}
		
		
		
		
		/**
		 * A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)
		 * @param PreviousDocumentVersionSetId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentVersionSetId(String PreviousDocumentVersionSetId) {
			setValue("PreviousDocumentVersionSetId", PreviousDocumentVersionSetId);
			return this;
		}
		
		
		/**
		 * An integer value that identifies the version number of the document that this document replaces
		 * @return String object
		 */	
		public String getPreviousDocumentVersionNumber() {
			return (String)getValue("PreviousDocumentVersionNumber");
		}
		
		
		
		
		/**
		 * An integer value that identifies the version number of the document that this document replaces
		 * @param PreviousDocumentVersionNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentVersionNumber(String PreviousDocumentVersionNumber) {
			setValue("PreviousDocumentVersionNumber", PreviousDocumentVersionNumber);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"DOCCLIN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeIdRoot", new Field(
												"TypeIdRoot",
												"x:typeId/@root",
												"2.16.840.1.113883.1.3",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeId", new Field(
												"TypeId",
												"x:typeId/@extension",
												"POCD_HD000040",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageTypeRoot", new Field(
												"MessageTypeRoot",
												"npfitlc:messageType/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.17",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageType", new Field(
												"MessageType",
												"npfitlc:messageType/@extension",
												"POCD_MT010000GB01",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentId", new Field(
												"DocumentId",
												"x:id/@root",
												"A DCE UUID to identify this specific document and version",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTypeFixedCodedValue", new Field(
												"DocumentTypeFixedCodedValue",
												"x:code/@code",
												"866371000000107",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTypeFixedCodedValueCodeSystem", new Field(
												"DocumentTypeFixedCodedValueCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.15",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTypeFixedCodedValueDisplayName", new Field(
												"DocumentTypeFixedCodedValueDisplayName",
												"x:code/@displayName",
												"Child Screening Report",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Title", new Field(
												"Title",
												"x:title",
												"Child Screening Report",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"The creation time of the CDA document",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConfidentialityCode", new Field(
												"ConfidentialityCode",
												"x:confidentialityCode",
												"",
												"true",
												"",
												"x_BasicConfidentialityKind",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.415",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentSetId", new Field(
												"DocumentSetId",
												"x:setId/@root",
												"A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentVersionNumber", new Field(
												"DocumentVersionNumber",
												"x:versionNumber/@value",
												"The version number of the document as an integer value",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTypeCode", new Field(
												"PatientTypeCode",
												"x:recordTarget/@typeCode",
												"RCT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientContextControlCode", new Field(
												"PatientContextControlCode",
												"x:recordTarget/@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientContentId", new Field(
												"PatientContentId",
												"x:recordTarget/npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"ChildPatient",
												""
												));
	
		put("PatientContentIdRoot", new Field(
												"PatientContentIdRoot",
												"x:recordTarget/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ChildPatient", new Field(
												"ChildPatient",
												"x:recordTarget/x:patientRole",
												"Patient Details",
												"true",
												"",
												"",
												"ChildPatientUniversal",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"ChildScreeningAuthor",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CustodianTypeCode", new Field(
												"CustodianTypeCode",
												"x:custodian/@typeCode",
												"CST",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CustodianContentId", new Field(
												"CustodianContentId",
												"x:custodian/npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"CustodianOrganisation",
												""
												));
	
		put("CustodianContentIdRoot", new Field(
												"CustodianContentIdRoot",
												"x:custodian/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CustodianOrganisation", new Field(
												"CustodianOrganisation",
												"x:custodian/x:assignedCustodian",
												"The organisation responsible for maintaining the information in the CDA document",
												"true",
												"",
												"",
												"CustodianOrganizationUniversal",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:custodian/x:assignedCustodian/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PrimaryRecipients", new Field(
												"PrimaryRecipients",
												"x:informationRecipient[@typeCode='PRCP']",
												"Primary recipients",
												"false",
												"",
												"",
												"PrimaryRecipient",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformationOnlyRecipients", new Field(
												"InformationOnlyRecipients",
												"x:informationRecipient[@typeCode='TRC']",
												"Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.",
												"false",
												"",
												"",
												"InformationOnlyRecipient",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConsentTypeCode", new Field(
												"ConsentTypeCode",
												"x:authorization/@typeCode",
												"AUTH",
												"AuthorizingConsent",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConsentContentId", new Field(
												"ConsentContentId",
												"x:authorization/npfitlc:contentId/@extension",
												"...",
												"AuthorizingConsent",
												"",
												"",
												"",
												"AuthorizingConsent",
												""
												));
	
		put("ConsentContentIdRoot", new Field(
												"ConsentContentIdRoot",
												"x:authorization/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"AuthorizingConsent",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorizingConsent", new Field(
												"AuthorizingConsent",
												"x:authorization/x:consent",
												"Details of consent/authorisation for the creation/sharing of the document",
												"false",
												"",
												"",
												"Consent",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:authorization/x:consent/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ComponentTypeCode", new Field(
												"ComponentTypeCode",
												"x:component/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ComponentContextConductionInd ", new Field(
												"ComponentContextConductionInd ",
												"x:component/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BodyClassCode", new Field(
												"BodyClassCode",
												"x:component/x:structuredBody/@classCode",
												"DOCBODY",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BodyMoodCode", new Field(
												"BodyMoodCode",
												"x:component/x:structuredBody/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BodyComponentTypeCode", new Field(
												"BodyComponentTypeCode",
												"x:component/x:structuredBody/x:component/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BodyComponentContextConductionInd", new Field(
												"BodyComponentContextConductionInd",
												"x:component/x:structuredBody/x:component/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CSClassCode", new Field(
												"CSClassCode",
												"x:component/x:structuredBody/x:component/x:section/@classCode",
												"DOCSECT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CSMoodCode", new Field(
												"CSMoodCode",
												"x:component/x:structuredBody/x:component/x:section/@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MainDocumentSectionID", new Field(
												"MainDocumentSectionID",
												"x:component/x:structuredBody/x:component/x:section/x:id/@root",
												"A DCE UUID to identify the main document section",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodedSections", new Field(
												"CodedSections",
												"x:component/x:structuredBody/x:component/x:section/x:entry",
												"A list of the coded sections making up the document",
												"false",
												"",
												"",
												"CodedSections",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TextSections", new Field(
												"TextSections",
												"x:component/x:structuredBody/x:component/x:section/x:component",
												"A list of the text sections making up the document",
												"true",
												"",
												"",
												"TextSections",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentClassCode", new Field(
												"PreviousDocumentClassCode",
												"x:relatedDocument/x:priorParentDocument/@classCode",
												"DOCCLIN",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentMoodCode", new Field(
												"PreviousDocumentMoodCode",
												"x:relatedDocument/x:priorParentDocument/@moodCode",
												"EVN",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RDTypeCode", new Field(
												"RDTypeCode",
												"x:relatedDocument/@typeCode",
												"RPLC",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentType", new Field(
												"PreviousDocumentType",
												"x:relatedDocument/x:priorParentDocument/x:code/@code",
												"866371000000107",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentTypeCodeSystem", new Field(
												"PreviousDocumentTypeCodeSystem",
												"x:relatedDocument/x:priorParentDocument/x:code/@codeSystem",
												"866371000000107",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentTypeDisplayName", new Field(
												"PreviousDocumentTypeDisplayName",
												"x:relatedDocument/x:priorParentDocument/x:code/@displayName",
												"Child Screening Report",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentVersionID", new Field(
												"PreviousDocumentVersionID",
												"x:relatedDocument/x:priorParentDocument/x:id",
												"A DCE UUID that identifies the specific document and version that this document replaces",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentVersionSetId", new Field(
												"PreviousDocumentVersionSetId",
												"x:relatedDocument/x:priorParentDocument/x:setId",
												"A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentVersionNumber", new Field(
												"PreviousDocumentVersionNumber",
												"x:relatedDocument/x:priorParentDocument/x:versionNumber",
												"An integer value that identifies the version number of the document that this document replaces",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ClinicalDocument() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ClinicalDocument(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

		