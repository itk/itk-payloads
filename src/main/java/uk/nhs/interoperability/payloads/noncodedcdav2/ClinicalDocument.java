/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.noncodedcdav2;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the ClinicalDocument object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String DocumentId</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} DocumentType</li>
 * <li>String DocumentTitle</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} EffectiveTime</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ConfidentialityCode</li>
 * <li>String DocumentSetId</li>
 * <li>String DocumentVersionNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.PatientUniversal PatientUniversal} Patient</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthored</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Author Author} Author</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.PersonUniversal PersonUniversal} DataEnterer</li>
 * <li>List&lt;{@link DocumentInformant DocumentInformant}&gt; Informant</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal CustodianOrganizationUniversal} CustodianOrganisation</li>
 * <li>List&lt;{@link PrimaryRecipient PrimaryRecipient}&gt; PrimaryRecipients</li>
 * <li>List&lt;{@link InformationOnlyRecipient InformationOnlyRecipient}&gt; InformationOnlyRecipients</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} TimeAuthenticated</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.PersonUniversal PersonUniversal} Authenticator</li>
 * <li>List&lt;{@link DocumentParticipant DocumentParticipant}&gt; Participant</li>
 * <li>List&lt;{@link DocumentationOf DocumentationOf}&gt; DocumentationOf</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.Consent Consent} AuthorizingConsent</li>
 * <li>{@link uk.nhs.interoperability.payloads.templates.EncompassingEncounter EncompassingEncounter} EncompassingEncounter</li>
 * <li>String MainStructuredBodyID</li>
 * <li>List&lt;{@link CodedSections CodedSections}&gt; CodedSections</li>
 * <li>List&lt;{@link TextSections TextSections}&gt; TextSections</li>
 * <li>String NonXMLBodyText</li>
 * <li>String NonXMLBodyMediaType</li>
 * <li>String NonXMLBodyType</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} PreviousDocumentType</li>
 * <li>String PreviousDocumentVersionID</li>
 * <li>String PreviousDocumentVersionSetId</li>
 * <li>String PreviousDocumentVersionNumber</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class ClinicalDocument extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "noncodedcdaFieldConfig";
		protected static final String name = "ClinicalDocument";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.noncodedcdav2";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param DocumentId DocumentId
		 * @param DocumentType DocumentType
		 * @param DocumentTitle DocumentTitle
		 * @param EffectiveTime EffectiveTime
		 * @param ConfidentialityCode ConfidentialityCode
		 * @param DocumentSetId DocumentSetId
		 * @param DocumentVersionNumber DocumentVersionNumber
		 * @param Patient Patient
		 * @param TimeAuthored TimeAuthored
		 * @param Author Author
		 * @param DataEnterer DataEnterer
		 * @param Informant Informant
		 * @param CustodianOrganisation CustodianOrganisation
		 * @param PrimaryRecipients PrimaryRecipients
		 * @param InformationOnlyRecipients InformationOnlyRecipients
		 * @param TimeAuthenticated TimeAuthenticated
		 * @param Authenticator Authenticator
		 * @param Participant Participant
		 * @param DocumentationOf DocumentationOf
		 * @param AuthorizingConsent AuthorizingConsent
		 * @param EncompassingEncounter EncompassingEncounter
		 * @param MainStructuredBodyID MainStructuredBodyID
		 * @param CodedSections CodedSections
		 * @param TextSections TextSections
		 * @param NonXMLBodyText NonXMLBodyText
		 * @param NonXMLBodyMediaType NonXMLBodyMediaType
		 * @param NonXMLBodyType NonXMLBodyType
		 * @param PreviousDocumentType PreviousDocumentType
		 * @param PreviousDocumentVersionID PreviousDocumentVersionID
		 * @param PreviousDocumentVersionSetId PreviousDocumentVersionSetId
		 * @param PreviousDocumentVersionNumber PreviousDocumentVersionNumber
		 */
	    public ClinicalDocument(String DocumentId, CodedValue DocumentType, String DocumentTitle, HL7Date EffectiveTime, CodedValue ConfidentialityCode, String DocumentSetId, String DocumentVersionNumber, uk.nhs.interoperability.payloads.templates.PatientUniversal Patient, HL7Date TimeAuthored, uk.nhs.interoperability.payloads.templates.Author Author, uk.nhs.interoperability.payloads.templates.PersonUniversal DataEnterer, 
		List<DocumentInformant> Informant, uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal CustodianOrganisation, 
		List<PrimaryRecipient> PrimaryRecipients, 
		List<InformationOnlyRecipient> InformationOnlyRecipients, HL7Date TimeAuthenticated, uk.nhs.interoperability.payloads.templates.PersonUniversal Authenticator, 
		List<DocumentParticipant> Participant, 
		List<DocumentationOf> DocumentationOf, uk.nhs.interoperability.payloads.templates.Consent AuthorizingConsent, uk.nhs.interoperability.payloads.templates.EncompassingEncounter EncompassingEncounter, String MainStructuredBodyID, 
		List<CodedSections> CodedSections, 
		List<TextSections> TextSections, String NonXMLBodyText, String NonXMLBodyMediaType, String NonXMLBodyType, CodedValue PreviousDocumentType, String PreviousDocumentVersionID, String PreviousDocumentVersionSetId, String PreviousDocumentVersionNumber) {
			fields = new LinkedHashMap<String, Object>();
			
			setDocumentId(DocumentId);
			setDocumentType(DocumentType);
			setDocumentTitle(DocumentTitle);
			setEffectiveTime(EffectiveTime);
			setConfidentialityCode(ConfidentialityCode);
			setDocumentSetId(DocumentSetId);
			setDocumentVersionNumber(DocumentVersionNumber);
			setPatient(Patient);
			setTimeAuthored(TimeAuthored);
			setAuthor(Author);
			setDataEnterer(DataEnterer);
			setInformant(Informant);
			setCustodianOrganisation(CustodianOrganisation);
			setPrimaryRecipients(PrimaryRecipients);
			setInformationOnlyRecipients(InformationOnlyRecipients);
			setTimeAuthenticated(TimeAuthenticated);
			setAuthenticator(Authenticator);
			setParticipant(Participant);
			setDocumentationOf(DocumentationOf);
			setAuthorizingConsent(AuthorizingConsent);
			setEncompassingEncounter(EncompassingEncounter);
			setMainStructuredBodyID(MainStructuredBodyID);
			setCodedSections(CodedSections);
			setTextSections(TextSections);
			setNonXMLBodyText(NonXMLBodyText);
			setNonXMLBodyMediaType(NonXMLBodyMediaType);
			setNonXMLBodyType(NonXMLBodyType);
			setPreviousDocumentType(PreviousDocumentType);
			setPreviousDocumentVersionID(PreviousDocumentVersionID);
			setPreviousDocumentVersionSetId(PreviousDocumentVersionSetId);
			setPreviousDocumentVersionNumber(PreviousDocumentVersionNumber);
		}
	
		/**
		 * A DCE UUID to identify this specific document and version
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentId() {
			return (String)getValue("DocumentId");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this specific document and version
		 * <br><br>This field is MANDATORY
		 * @param DocumentId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentId(String DocumentId) {
			setValue("DocumentId", DocumentId);
			return this;
		}
		
		
		/**
		 * A code from the SNOMED CT Document Type subset, or any other vocabulary to describe the type of CDA document
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getDocumentType() {
			return (CodedValue)getValue("DocumentType");
		}
		
		
		
		/**
		 * A code from the SNOMED CT Document Type subset, or any other vocabulary to describe the type of CDA document
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * @return Documenttype enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype getDocumentTypeEnum() {
			CodedValue cv = (CodedValue)getValue("DocumentType");
			VocabularyEntry entry = VocabularyFactory.getVocab("Documenttype", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype)entry;
		}
		
		
		/**
		 * A code from the SNOMED CT Document Type subset, or any other vocabulary to describe the type of CDA document
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * @param DocumentType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentType(CodedValue DocumentType) {
			setValue("DocumentType", DocumentType);
			return this;
		}
		
		
		/**
		 * A code from the SNOMED CT Document Type subset, or any other vocabulary to describe the type of CDA document
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param DocumentType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentType(VocabularyEntry DocumentType) {
			Code c = new CodedValue(DocumentType);
			setValue("DocumentType", c);
			return this;
		}
		
		/**
		 * A string which is rendered as a human readable title
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentTitle() {
			return (String)getValue("DocumentTitle");
		}
		
		
		
		
		/**
		 * A string which is rendered as a human readable title
		 * <br><br>This field is MANDATORY
		 * @param DocumentTitle value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentTitle(String DocumentTitle) {
			setValue("DocumentTitle", DocumentTitle);
			return this;
		}
		
		
		/**
		 * The creation time of the CDA document
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getEffectiveTime() {
			return (HL7Date)getValue("EffectiveTime");
		}
		
		
		
		
		/**
		 * The creation time of the CDA document
		 * <br><br>This field is MANDATORY
		 * @param EffectiveTime value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setEffectiveTime(HL7Date EffectiveTime) {
			setValue("EffectiveTime", EffectiveTime);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getConfidentialityCode() {
			return (CodedValue)getValue("ConfidentialityCode");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * @return x_BasicConfidentialityKind enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind getConfidentialityCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ConfidentialityCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("x_BasicConfidentialityKind", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * @param ConfidentialityCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setConfidentialityCode(CodedValue ConfidentialityCode) {
			setValue("ConfidentialityCode", ConfidentialityCode);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "x_BasicConfidentialityKind" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.x_BasicConfidentialityKind
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ConfidentialityCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setConfidentialityCode(VocabularyEntry ConfidentialityCode) {
			Code c = new CodedValue(ConfidentialityCode);
			setValue("ConfidentialityCode", c);
			return this;
		}
		
		/**
		 * A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentSetId() {
			return (String)getValue("DocumentSetId");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)
		 * <br><br>This field is MANDATORY
		 * @param DocumentSetId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentSetId(String DocumentSetId) {
			setValue("DocumentSetId", DocumentSetId);
			return this;
		}
		
		
		/**
		 * The version number of the document as an integer value
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getDocumentVersionNumber() {
			return (String)getValue("DocumentVersionNumber");
		}
		
		
		
		
		/**
		 * The version number of the document as an integer value
		 * <br><br>This field is MANDATORY
		 * @param DocumentVersionNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentVersionNumber(String DocumentVersionNumber) {
			setValue("DocumentVersionNumber", DocumentVersionNumber);
			return this;
		}
		
		
		/**
		 * Patient Details
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.templates.PatientUniversal object
		 */	
		public uk.nhs.interoperability.payloads.templates.PatientUniversal getPatient() {
			return (uk.nhs.interoperability.payloads.templates.PatientUniversal)getValue("Patient");
		}
		
		
		
		
		/**
		 * Patient Details
		 * <br><br>This field is MANDATORY
		 * @param Patient value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPatient(uk.nhs.interoperability.payloads.templates.PatientUniversal Patient) {
			setValue("Patient", Patient);
			return this;
		}
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthored() {
			return (HL7Date)getValue("TimeAuthored");
		}
		
		
		
		
		/**
		 * The time the author originally recorded the End of life information
		 * @param TimeAuthored value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setTimeAuthored(HL7Date TimeAuthored) {
			setValue("TimeAuthored", TimeAuthored);
			return this;
		}
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @return uk.nhs.interoperability.payloads.templates.Author object
		 */	
		public uk.nhs.interoperability.payloads.templates.Author getAuthor() {
			return (uk.nhs.interoperability.payloads.templates.Author)getValue("Author");
		}
		
		
		
		
		/**
		 * The author of the End of Life care plan is the person who originally recorded the information on the End of Life system
		 * @param Author value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setAuthor(uk.nhs.interoperability.payloads.templates.Author Author) {
			setValue("Author", Author);
			return this;
		}
		
		
		/**
		 * A data enterer is the person who entered the information contained in the document
		 * @return uk.nhs.interoperability.payloads.templates.PersonUniversal object
		 */	
		public uk.nhs.interoperability.payloads.templates.PersonUniversal getDataEnterer() {
			return (uk.nhs.interoperability.payloads.templates.PersonUniversal)getValue("DataEnterer");
		}
		
		
		
		
		/**
		 * A data enterer is the person who entered the information contained in the document
		 * @param DataEnterer value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDataEnterer(uk.nhs.interoperability.payloads.templates.PersonUniversal DataEnterer) {
			setValue("DataEnterer", DataEnterer);
			return this;
		}
		
		
		/**
		 * A person who informed the author about information contained in the CDA document
		 * @return List of DocumentInformant objects
		 */
		public List<DocumentInformant> getInformant() {
			return (List<DocumentInformant>)getValue("Informant");
		}
		
		/**
		 * A person who informed the author about information contained in the CDA document
		 * @param Informant value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setInformant(List Informant) {
			setValue("Informant", Informant);
			return this;
		}
		
		/**
		 * A person who informed the author about information contained in the CDA document
		 * <br>Note: This adds a DocumentInformant object, but this method can be called
		 * multiple times to add additional DocumentInformant objects.
		 * @param Informant value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addInformant(DocumentInformant Informant) {
			addMultivalue("Informant", Informant);
			return this;
		}
		
		
		/**
		 * The organisation responsible for maintaining the information in the CDA document
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal object
		 */	
		public uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal getCustodianOrganisation() {
			return (uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal)getValue("CustodianOrganisation");
		}
		
		
		
		
		/**
		 * The organisation responsible for maintaining the information in the CDA document
		 * <br><br>This field is MANDATORY
		 * @param CustodianOrganisation value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setCustodianOrganisation(uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal CustodianOrganisation) {
			setValue("CustodianOrganisation", CustodianOrganisation);
			return this;
		}
		
		
		/**
		 * Primary recipients
		 * @return List of PrimaryRecipient objects
		 */
		public List<PrimaryRecipient> getPrimaryRecipients() {
			return (List<PrimaryRecipient>)getValue("PrimaryRecipients");
		}
		
		/**
		 * Primary recipients
		 * @param PrimaryRecipients value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPrimaryRecipients(List PrimaryRecipients) {
			setValue("PrimaryRecipients", PrimaryRecipients);
			return this;
		}
		
		/**
		 * Primary recipients
		 * <br>Note: This adds a PrimaryRecipient object, but this method can be called
		 * multiple times to add additional PrimaryRecipient objects.
		 * @param PrimaryRecipients value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addPrimaryRecipients(PrimaryRecipient PrimaryRecipients) {
			addMultivalue("PrimaryRecipients", PrimaryRecipients);
			return this;
		}
		
		
		/**
		 * Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.
		 * @return List of InformationOnlyRecipient objects
		 */
		public List<InformationOnlyRecipient> getInformationOnlyRecipients() {
			return (List<InformationOnlyRecipient>)getValue("InformationOnlyRecipients");
		}
		
		/**
		 * Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.
		 * @param InformationOnlyRecipients value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setInformationOnlyRecipients(List InformationOnlyRecipients) {
			setValue("InformationOnlyRecipients", InformationOnlyRecipients);
			return this;
		}
		
		/**
		 * Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.
		 * <br>Note: This adds a InformationOnlyRecipient object, but this method can be called
		 * multiple times to add additional InformationOnlyRecipient objects.
		 * @param InformationOnlyRecipients value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addInformationOnlyRecipients(InformationOnlyRecipient InformationOnlyRecipients) {
			addMultivalue("InformationOnlyRecipients", InformationOnlyRecipients);
			return this;
		}
		
		
		/**
		 * When the person authenticated the CDA document
		 * @return HL7Date object
		 */	
		public HL7Date getTimeAuthenticated() {
			return (HL7Date)getValue("TimeAuthenticated");
		}
		
		
		
		
		/**
		 * When the person authenticated the CDA document
		 * @param TimeAuthenticated value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setTimeAuthenticated(HL7Date TimeAuthenticated) {
			setValue("TimeAuthenticated", TimeAuthenticated);
			return this;
		}
		
		
		/**
		 * This is used where the CDA document needs to be authenticated. This means that the person who authenticated will have overall responsibility for the document not the author. For example a supervisor.
		 * <br><br>This field is MANDATORY
		 * @return uk.nhs.interoperability.payloads.templates.PersonUniversal object
		 */	
		public uk.nhs.interoperability.payloads.templates.PersonUniversal getAuthenticator() {
			return (uk.nhs.interoperability.payloads.templates.PersonUniversal)getValue("Authenticator");
		}
		
		
		
		
		/**
		 * This is used where the CDA document needs to be authenticated. This means that the person who authenticated will have overall responsibility for the document not the author. For example a supervisor.
		 * <br><br>This field is MANDATORY
		 * @param Authenticator value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setAuthenticator(uk.nhs.interoperability.payloads.templates.PersonUniversal Authenticator) {
			setValue("Authenticator", Authenticator);
			return this;
		}
		
		
		/**
		 * Participants are people, organisations or devices that are involved in some way with the CDA document.
		 * @return List of DocumentParticipant objects
		 */
		public List<DocumentParticipant> getParticipant() {
			return (List<DocumentParticipant>)getValue("Participant");
		}
		
		/**
		 * Participants are people, organisations or devices that are involved in some way with the CDA document.
		 * @param Participant value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setParticipant(List Participant) {
			setValue("Participant", Participant);
			return this;
		}
		
		/**
		 * Participants are people, organisations or devices that are involved in some way with the CDA document.
		 * <br>Note: This adds a DocumentParticipant object, but this method can be called
		 * multiple times to add additional DocumentParticipant objects.
		 * @param Participant value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addParticipant(DocumentParticipant Participant) {
			addMultivalue("Participant", Participant);
			return this;
		}
		
		
		/**
		 * This links the document header to the Service Event being documented in the CDA document
		 * @return List of DocumentationOf objects
		 */
		public List<DocumentationOf> getDocumentationOf() {
			return (List<DocumentationOf>)getValue("DocumentationOf");
		}
		
		/**
		 * This links the document header to the Service Event being documented in the CDA document
		 * @param DocumentationOf value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setDocumentationOf(List DocumentationOf) {
			setValue("DocumentationOf", DocumentationOf);
			return this;
		}
		
		/**
		 * This links the document header to the Service Event being documented in the CDA document
		 * <br>Note: This adds a DocumentationOf object, but this method can be called
		 * multiple times to add additional DocumentationOf objects.
		 * @param DocumentationOf value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addDocumentationOf(DocumentationOf DocumentationOf) {
			addMultivalue("DocumentationOf", DocumentationOf);
			return this;
		}
		
		
		/**
		 * Details of consent/authorisation for the creation/sharing of the document
		 * @return uk.nhs.interoperability.payloads.templates.Consent object
		 */	
		public uk.nhs.interoperability.payloads.templates.Consent getAuthorizingConsent() {
			return (uk.nhs.interoperability.payloads.templates.Consent)getValue("AuthorizingConsent");
		}
		
		
		
		
		/**
		 * Details of consent/authorisation for the creation/sharing of the document
		 * @param AuthorizingConsent value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setAuthorizingConsent(uk.nhs.interoperability.payloads.templates.Consent AuthorizingConsent) {
			setValue("AuthorizingConsent", AuthorizingConsent);
			return this;
		}
		
		
		/**
		 * Used to link the document header to the EncompassingEncounter that the CDA document is documenting
		 * @return uk.nhs.interoperability.payloads.templates.EncompassingEncounter object
		 */	
		public uk.nhs.interoperability.payloads.templates.EncompassingEncounter getEncompassingEncounter() {
			return (uk.nhs.interoperability.payloads.templates.EncompassingEncounter)getValue("EncompassingEncounter");
		}
		
		
		
		
		/**
		 * Used to link the document header to the EncompassingEncounter that the CDA document is documenting
		 * @param EncompassingEncounter value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setEncompassingEncounter(uk.nhs.interoperability.payloads.templates.EncompassingEncounter EncompassingEncounter) {
			setValue("EncompassingEncounter", EncompassingEncounter);
			return this;
		}
		
		
		/**
		 * A DCE UUID to identify the main document section
		 * @return String object
		 */	
		public String getMainStructuredBodyID() {
			return (String)getValue("MainStructuredBodyID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify the main document section
		 * @param MainStructuredBodyID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setMainStructuredBodyID(String MainStructuredBodyID) {
			setValue("MainStructuredBodyID", MainStructuredBodyID);
			return this;
		}
		
		
		/**
		 * A list of the coded sections making up the document
		 * @return List of CodedSections objects
		 */
		public List<CodedSections> getCodedSections() {
			return (List<CodedSections>)getValue("CodedSections");
		}
		
		/**
		 * A list of the coded sections making up the document
		 * @param CodedSections value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setCodedSections(List CodedSections) {
			setValue("CodedSections", CodedSections);
			return this;
		}
		
		/**
		 * A list of the coded sections making up the document
		 * <br>Note: This adds a CodedSections object, but this method can be called
		 * multiple times to add additional CodedSections objects.
		 * @param CodedSections value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addCodedSections(CodedSections CodedSections) {
			addMultivalue("CodedSections", CodedSections);
			return this;
		}
		
		
		/**
		 * A list of the text sections making up the document
		 * @return List of TextSections objects
		 */
		public List<TextSections> getTextSections() {
			return (List<TextSections>)getValue("TextSections");
		}
		
		/**
		 * A list of the text sections making up the document
		 * @param TextSections value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setTextSections(List TextSections) {
			setValue("TextSections", TextSections);
			return this;
		}
		
		/**
		 * A list of the text sections making up the document
		 * <br>Note: This adds a TextSections object, but this method can be called
		 * multiple times to add additional TextSections objects.
		 * @param TextSections value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument addTextSections(TextSections TextSections) {
			addMultivalue("TextSections", TextSections);
			return this;
		}
		
		
		/**
		 * The information which makes up the document body when the standard CDA structure (mark-up) is not used.
		 * @return String object
		 */	
		public String getNonXMLBodyText() {
			return (String)getValue("NonXMLBodyText");
		}
		
		
		
		
		/**
		 * The information which makes up the document body when the standard CDA structure (mark-up) is not used.
		 * @param NonXMLBodyText value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setNonXMLBodyText(String NonXMLBodyText) {
			setValue("NonXMLBodyText", NonXMLBodyText);
			return this;
		}
		
		
		/**
		 * The mime type of the non-CDA body.
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getNonXMLBodyMediaType() {
			return (String)getValue("NonXMLBodyMediaType");
		}
		
		
		
		
		/**
		 * The mime type of the non-CDA body.
		 * <br><br>This field is MANDATORY
		 * @param NonXMLBodyMediaType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setNonXMLBodyMediaType(String NonXMLBodyMediaType) {
			setValue("NonXMLBodyMediaType", NonXMLBodyMediaType);
			return this;
		}
		
		
		/**
		 * Encoding of non-CDA body - must be either TXT or B64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * @return String object
		 */	
		public String getNonXMLBodyType() {
			return (String)getValue("NonXMLBodyType");
		}
		
		
		
		/**
		 * Encoding of non-CDA body - must be either TXT or B64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * @return AttachmentType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType getNonXMLBodyTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType.getByCode((String)getValue("NonXMLBodyType"));
		}
		
		
		/**
		 * Encoding of non-CDA body - must be either TXT or B64. Please use the AttachmentType internal vocab.
  		 * <br>NOTE: This field should be populated using the "AttachmentType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AttachmentType
		 * @param NonXMLBodyType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setNonXMLBodyType(String NonXMLBodyType) {
			setValue("NonXMLBodyType", NonXMLBodyType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getPreviousDocumentType() {
			return (CodedValue)getValue("PreviousDocumentType");
		}
		
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * @return Documenttype enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype getPreviousDocumentTypeEnum() {
			CodedValue cv = (CodedValue)getValue("PreviousDocumentType");
			VocabularyEntry entry = VocabularyFactory.getVocab("Documenttype", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype)entry;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * @param PreviousDocumentType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentType(CodedValue PreviousDocumentType) {
			setValue("PreviousDocumentType", PreviousDocumentType);
			return this;
		}
		
		
		/**
		 * 
  		 * <br>NOTE: This field should be populated using the "Documenttype" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Documenttype
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param PreviousDocumentType value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentType(VocabularyEntry PreviousDocumentType) {
			Code c = new CodedValue(PreviousDocumentType);
			setValue("PreviousDocumentType", c);
			return this;
		}
		
		/**
		 * A DCE UUID that identifies the specific document and version that this document replaces
		 * @return String object
		 */	
		public String getPreviousDocumentVersionID() {
			return (String)getValue("PreviousDocumentVersionID");
		}
		
		
		
		
		/**
		 * A DCE UUID that identifies the specific document and version that this document replaces
		 * @param PreviousDocumentVersionID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentVersionID(String PreviousDocumentVersionID) {
			setValue("PreviousDocumentVersionID", PreviousDocumentVersionID);
			return this;
		}
		
		
		/**
		 * A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)
		 * @return String object
		 */	
		public String getPreviousDocumentVersionSetId() {
			return (String)getValue("PreviousDocumentVersionSetId");
		}
		
		
		
		
		/**
		 * A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)
		 * @param PreviousDocumentVersionSetId value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentVersionSetId(String PreviousDocumentVersionSetId) {
			setValue("PreviousDocumentVersionSetId", PreviousDocumentVersionSetId);
			return this;
		}
		
		
		/**
		 * An integer value that identifies the version number of the document that this document replaces
		 * @return String object
		 */	
		public String getPreviousDocumentVersionNumber() {
			return (String)getValue("PreviousDocumentVersionNumber");
		}
		
		
		
		
		/**
		 * An integer value that identifies the version number of the document that this document replaces
		 * @param PreviousDocumentVersionNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public ClinicalDocument setPreviousDocumentVersionNumber(String PreviousDocumentVersionNumber) {
			setValue("PreviousDocumentVersionNumber", PreviousDocumentVersionNumber);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"DOCCLIN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeIdRoot", new Field(
												"TypeIdRoot",
												"x:typeId/@root",
												"2.16.840.1.113883.1.3",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TypeId", new Field(
												"TypeId",
												"x:typeId/@extension",
												"POCD_HD000040",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageTypeRoot", new Field(
												"MessageTypeRoot",
												"npfitlc:messageType/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.17",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageType", new Field(
												"MessageType",
												"npfitlc:messageType/@extension",
												"POCD_MT010011GB02",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentId", new Field(
												"DocumentId",
												"x:id/@root",
												"A DCE UUID to identify this specific document and version",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentType", new Field(
												"DocumentType",
												"x:code",
												"A code from the SNOMED CT Document Type subset, or any other vocabulary to describe the type of CDA document",
												"true",
												"",
												"Documenttype",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.337",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentTitle", new Field(
												"DocumentTitle",
												"x:title",
												"A string which is rendered as a human readable title",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EffectiveTime", new Field(
												"EffectiveTime",
												"x:effectiveTime/@value",
												"The creation time of the CDA document",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConfidentialityCode", new Field(
												"ConfidentialityCode",
												"x:confidentialityCode",
												"",
												"true",
												"",
												"x_BasicConfidentialityKind",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.415",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentSetId", new Field(
												"DocumentSetId",
												"x:setId/@root",
												"A DCE UUID to identify all documents that are part of a set of documents (i.e. previous versions of this document)",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentVersionNumber", new Field(
												"DocumentVersionNumber",
												"x:versionNumber/@value",
												"The version number of the document as an integer value",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientTypeCode", new Field(
												"PatientTypeCode",
												"x:recordTarget/@typeCode",
												"RCT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientContextControlCode", new Field(
												"PatientContextControlCode",
												"x:recordTarget/@contextControlCode",
												"OP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientContentId", new Field(
												"PatientContentId",
												"x:recordTarget/npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"Patient",
												""
												));
	
		put("PatientContentIdRoot", new Field(
												"PatientContentIdRoot",
												"x:recordTarget/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Patient", new Field(
												"Patient",
												"x:recordTarget/x:patientRole",
												"Patient Details",
												"true",
												"",
												"",
												"PatientUniversal",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeValue", new Field(
												"AuthorFunctionCodeValue",
												"x:author/x:functionCode/@code",
												"OA",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorTypeCode", new Field(
												"AuthorTypeCode",
												"x:author/@typeCode",
												"AUT",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContextControlCode", new Field(
												"AuthorContextControlCode",
												"x:author/@contextControlCode",
												"OP",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorContentId", new Field(
												"AuthorContentId",
												"x:author/npfitlc:contentId/@extension",
												"...",
												"Author",
												"",
												"",
												"",
												"Author",
												"true"
												));
	
		put("AuthorContentIdRoot", new Field(
												"AuthorContentIdRoot",
												"x:author/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Author",
												"",
												"",
												"",
												"",
												"true"
												));
	
		put("TimeAuthored", new Field(
												"TimeAuthored",
												"x:author/x:time/@value",
												"The time the author originally recorded the End of life information",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Author", new Field(
												"Author",
												"x:author/x:assignedAuthor",
												"The author of the End of Life care plan is the person who originally recorded the information on the End of Life system",
												"false",
												"",
												"",
												"Author",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:author/x:assignedAuthor/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeSystem", new Field(
												"AuthorFunctionCodeSystem",
												"x:author/x:functionCode/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.178",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorFunctionCodeDisplayName", new Field(
												"AuthorFunctionCodeDisplayName",
												"x:author/x:functionCode/@displayName",
												"Originating Author",
												"Author",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DataEntererTypeCode", new Field(
												"DataEntererTypeCode",
												"x:dataEnterer/@typeCode",
												"ENT",
												"DataEnterer",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DataEntererContextControlCode", new Field(
												"DataEntererContextControlCode",
												"x:dataEnterer/@contextControlCode",
												"OP",
												"DataEnterer",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DataEntererContentId", new Field(
												"DataEntererContentId",
												"x:dataEnterer/npfitlc:contentId/@extension",
												"...",
												"DataEnterer",
												"",
												"",
												"",
												"DataEnterer",
												""
												));
	
		put("DataEntererContentIdRoot", new Field(
												"DataEntererContentIdRoot",
												"x:dataEnterer/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"DataEnterer",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DataEnterer", new Field(
												"DataEnterer",
												"x:dataEnterer/x:assignedEntity",
												"A data enterer is the person who entered the information contained in the document",
												"false",
												"",
												"",
												"PersonUniversal",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:dataEnterer/x:assignedEntity/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Informant", new Field(
												"Informant",
												"x:informant",
												"A person who informed the author about information contained in the CDA document",
												"false",
												"",
												"",
												"DocumentInformant",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CustodianTypeCode", new Field(
												"CustodianTypeCode",
												"x:custodian/@typeCode",
												"CST",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CustodianContentId", new Field(
												"CustodianContentId",
												"x:custodian/npfitlc:contentId/@extension",
												"...",
												"",
												"",
												"",
												"",
												"CustodianOrganisation",
												""
												));
	
		put("CustodianContentIdRoot", new Field(
												"CustodianContentIdRoot",
												"x:custodian/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CustodianOrganisation", new Field(
												"CustodianOrganisation",
												"x:custodian/x:assignedCustodian",
												"The organisation responsible for maintaining the information in the CDA document",
												"true",
												"",
												"",
												"CustodianOrganizationUniversal",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:custodian/x:assignedCustodian/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PrimaryRecipients", new Field(
												"PrimaryRecipients",
												"x:informationRecipient[@typeCode='PRCP']",
												"Primary recipients",
												"false",
												"",
												"",
												"PrimaryRecipient",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("InformationOnlyRecipients", new Field(
												"InformationOnlyRecipients",
												"x:informationRecipient[@typeCode='TRC']",
												"Recipients who are sent a copy of the CDA document for information only. They are not normally required to carry out any action on receipt of the CDA document.",
												"false",
												"",
												"",
												"InformationOnlyRecipient",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthenticatorTypeCode", new Field(
												"AuthenticatorTypeCode",
												"x:authenticator/@typeCode",
												"AUTHEN",
												"Authenticator",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthenticatorContentId", new Field(
												"AuthenticatorContentId",
												"x:authenticator/npfitlc:contentId/@extension",
												"...",
												"Authenticator",
												"",
												"",
												"",
												"Authenticator",
												""
												));
	
		put("AuthenticatorContentIdRoot", new Field(
												"AuthenticatorContentIdRoot",
												"x:authenticator/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"Authenticator",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TimeAuthenticated", new Field(
												"TimeAuthenticated",
												"x:authenticator/x:time/@value",
												"When the person authenticated the CDA document",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthenticatorSignatureCode", new Field(
												"AuthenticatorSignatureCode",
												"x:authenticator/x:signatureCode/@nullFlavor",
												"NA",
												"Authenticator",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Authenticator", new Field(
												"Authenticator",
												"x:authenticator/x:assignedEntity",
												"This is used where the CDA document needs to be authenticated. This means that the person who authenticated will have overall responsibility for the document not the author. For example a supervisor.",
												"true",
												"",
												"",
												"PersonUniversal",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:authenticator/x:assignedEntity/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Participant", new Field(
												"Participant",
												"x:participant",
												"Participants are people, organisations or devices that are involved in some way with the CDA document.",
												"false",
												"",
												"",
												"DocumentParticipant",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DocumentationOf", new Field(
												"DocumentationOf",
												"x:documentationOf",
												"This links the document header to the Service Event being documented in the CDA document",
												"false",
												"",
												"",
												"DocumentationOf",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConsentTypeCode", new Field(
												"ConsentTypeCode",
												"x:authorization/@typeCode",
												"AUTH",
												"AuthorizingConsent",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ConsentContentId", new Field(
												"ConsentContentId",
												"x:authorization/npfitlc:contentId/@extension",
												"...",
												"AuthorizingConsent",
												"",
												"",
												"",
												"AuthorizingConsent",
												""
												));
	
		put("ConsentContentIdRoot", new Field(
												"ConsentContentIdRoot",
												"x:authorization/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"AuthorizingConsent",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AuthorizingConsent", new Field(
												"AuthorizingConsent",
												"x:authorization/x:consent",
												"Details of consent/authorisation for the creation/sharing of the document",
												"false",
												"",
												"",
												"Consent",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:authorization/x:consent/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ComponentOfTypeCode", new Field(
												"ComponentOfTypeCode",
												"x:componentOf/@typeCode",
												"COMP",
												"EncompassingEncounter",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ComponentOfContentId", new Field(
												"ComponentOfContentId",
												"x:componentOf/npfitlc:contentId/@extension",
												"...",
												"EncompassingEncounter",
												"",
												"",
												"",
												"EncompassingEncounter",
												""
												));
	
		put("ComponentOfContentIdRoot", new Field(
												"ComponentOfContentIdRoot",
												"x:componentOf/npfitlc:contentId/@root",
												"2.16.840.1.113883.2.1.3.2.4.18.16",
												"EncompassingEncounter",
												"",
												"",
												"",
												"",
												""
												));
	
		put("EncompassingEncounter", new Field(
												"EncompassingEncounter",
												"x:componentOf/x:encompassingEncounter",
												"Used to link the document header to the EncompassingEncounter that the CDA document is documenting",
												"false",
												"",
												"",
												"EncompassingEncounter",
												"uk.nhs.interoperability.payloads.templates.",
												"",
												"",
												"",
												"x:componentOf/x:encompassingEncounter/x:templateId/@extension",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BodyTypeCode", new Field(
												"BodyTypeCode",
												"x:component/@typeCode",
												"COMP",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("BodyContextConductionInd", new Field(
												"BodyContextConductionInd",
												"x:component/@contextConductionInd",
												"true",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StructuredBodyClassCode", new Field(
												"StructuredBodyClassCode",
												"x:component/x:structuredBody/@classCode",
												"DOCBODY",
												"MainStructuredBodyID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("StructuredBodyMoodCode", new Field(
												"StructuredBodyMoodCode",
												"x:component/x:structuredBody/@moodCode",
												"EVN",
												"MainStructuredBodyID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassificationSectionClassCode", new Field(
												"ClassificationSectionClassCode",
												"x:component/x:structuredBody/x:component/@typeCode",
												"COMP",
												"MainStructuredBodyID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassificationSectionMoodCode", new Field(
												"ClassificationSectionMoodCode",
												"x:component/x:structuredBody/x:component/@contextConductionInd",
												"true",
												"MainStructuredBodyID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassificationSectionClassCode2", new Field(
												"ClassificationSectionClassCode2",
												"x:component/x:structuredBody/x:component/x:section/@classCode",
												"DOCSECT",
												"MainStructuredBodyID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ClassificationSectionMoodCode2", new Field(
												"ClassificationSectionMoodCode2",
												"x:component/x:structuredBody/x:component/x:section/@moodCode",
												"EVN",
												"MainStructuredBodyID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MainStructuredBodyID", new Field(
												"MainStructuredBodyID",
												"x:component/x:structuredBody/x:component/x:section/x:id/@root",
												"A DCE UUID to identify the main document section",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodedSections", new Field(
												"CodedSections",
												"x:component/x:structuredBody/x:component/x:section/x:entry",
												"A list of the coded sections making up the document",
												"false",
												"",
												"",
												"CodedSections",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TextSections", new Field(
												"TextSections",
												"x:component/x:structuredBody/x:component/x:section/x:component",
												"A list of the text sections making up the document",
												"false",
												"",
												"",
												"TextSections",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NonXMLBodyClassCode", new Field(
												"NonXMLBodyClassCode",
												"x:component/x:nonXMLBody/@classCode",
												"DOCBODY",
												"NonXMLBodyText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NonXMLBodyMoodCode", new Field(
												"NonXMLBodyMoodCode",
												"x:component/x:nonXMLBody/@moodCode",
												"EVN",
												"NonXMLBodyText",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NonXMLBodyText", new Field(
												"NonXMLBodyText",
												"x:component/x:nonXMLBody/x:text",
												"The information which makes up the document body when the standard CDA structure (mark-up) is not used.",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NonXMLBodyMediaType", new Field(
												"NonXMLBodyMediaType",
												"x:component/x:nonXMLBody/x:text/@mediaType",
												"The mime type of the non-CDA body.",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NonXMLBodyType", new Field(
												"NonXMLBodyType",
												"",
												"Encoding of non-CDA body - must be either TXT or B64. Please use the AttachmentType internal vocab.",
												"false",
												"",
												"AttachmentType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AttachmentTypeTXT", new Field(
												"AttachmentTypeTXT",
												"x:component/x:nonXMLBody/x:text/@representation",
												"TXT",
												"NonXMLBodyText",
												"",
												"NonXMLBodyType",
												"Text",
												"",
												""
												));
	
		put("AttachmentTypeB64", new Field(
												"AttachmentTypeB64",
												"x:component/x:nonXMLBody/x:text/@representation",
												"B64",
												"NonXMLBodyText",
												"",
												"NonXMLBodyType",
												"Base64",
												"",
												""
												));
	
		put("PreviousDocumentClassCode", new Field(
												"PreviousDocumentClassCode",
												"x:relatedDocument/x:priorParentDocument/@classCode",
												"DOCCLIN",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentMoodCode", new Field(
												"PreviousDocumentMoodCode",
												"x:relatedDocument/x:priorParentDocument/@moodCode",
												"EVN",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("RDTypeCode", new Field(
												"RDTypeCode",
												"x:relatedDocument/@typeCode",
												"RPLC",
												"PreviousDocumentVersionID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentType", new Field(
												"PreviousDocumentType",
												"x:relatedDocument/x:priorParentDocument/x:code",
												"",
												"true",
												"",
												"Documenttype",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.337",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentVersionID", new Field(
												"PreviousDocumentVersionID",
												"x:relatedDocument/x:priorParentDocument/x:id",
												"A DCE UUID that identifies the specific document and version that this document replaces",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentVersionSetId", new Field(
												"PreviousDocumentVersionSetId",
												"x:relatedDocument/x:priorParentDocument/x:setId",
												"A DCE UUID that identifies the document set of the document that this document replaces (should always match the document set of this document)",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PreviousDocumentVersionNumber", new Field(
												"PreviousDocumentVersionNumber",
												"x:relatedDocument/x:priorParentDocument/x:versionNumber",
												"An integer value that identifies the version number of the document that this document replaces",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public ClinicalDocument() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public ClinicalDocument(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	