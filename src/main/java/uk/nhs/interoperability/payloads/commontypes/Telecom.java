/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.commontypes;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Telecom object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Telecom</li>
 * <li>String TelecomType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Telecom extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "commonTypesFieldConfig";
		protected static final String name = "Telecom";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.commontypes";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param Telecom String
		 */
		public Telecom(String Telecom) {
			fields = new LinkedHashMap<String, Object>();
			
			setTelecom(Telecom);
		}

		/**
		 * @param Telecom String
		 * @param TelecomType String
		 */
		public Telecom(String Telecom, String TelecomType) {
			fields = new LinkedHashMap<String, Object>();
			
			setTelecom(Telecom);
			setTelecomType(TelecomType);
		}

		/**
		 * Telephone or Fax Number (with tel: or fax: prefix)
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getTelecom() {
			return (String)getValue("Telecom");
		}
		
		
		
		
		/**
		 * Telephone or Fax Number (with tel: or fax: prefix)
		 * <br><br>This field is MANDATORY
		 * @param Telecom value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Telecom setTelecom(String Telecom) {
			setValue("Telecom", Telecom);
			return this;
		}
		
		
		/**
		 * Telecom use code
  		 * <br>NOTE: This field should be populated using the "TelecomUseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType
		 * @return String object
		 */	
		public String getTelecomType() {
			return (String)getValue("TelecomType");
		}
		
		
		
		/**
		 * Telecom use code
  		 * <br>NOTE: This field should be populated using the "TelecomUseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType
		 * @return TelecomUseType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType getTelecomTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType.getByCode((String)getValue("TelecomType"));
		}
		
		
		/**
		 * Telecom use code
  		 * <br>NOTE: This field should be populated using the "TelecomUseType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.TelecomUseType
		 * @param TelecomType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Telecom setTelecomType(String TelecomType) {
			setValue("TelecomType", TelecomType);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("Telecom", new Field(
												"Telecom",
												"@value",
												"Telephone or Fax Number (with tel: or fax: prefix)",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelecomType", new Field(
												"TelecomType",
												"@use",
												"Telecom use code",
												"false",
												"",
												"TelecomUseType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Telecom() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Telecom(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	