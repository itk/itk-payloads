/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.commontypes;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SMSPAddress object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Postcode</li>
 * <li>List&lt;String&gt; AddressLine</li>
 * <li>String AddressUse</li>
 * <li>String NullFlavour</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SMSPAddress extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "commonTypesFieldConfig";
		protected static final String name = "SMSPAddress";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.commontypes";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param AddressLine String
		 * @param Postcode String
		 */
		public SMSPAddress(List<String> AddressLine, String Postcode) {
			fields = new LinkedHashMap<String, Object>();
			
			setAddressLine(AddressLine);
			setPostcode(Postcode);
		}

		/**
		 * Address postcode
		 * @return String object
		 */	
		public String getPostcode() {
			return (String)getValue("Postcode");
		}
		
		
		
		
		/**
		 * Address postcode
		 * @param Postcode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPAddress setPostcode(String Postcode) {
			setValue("Postcode", Postcode);
			return this;
		}
		
		
		/**
		 * Address lines - exactly 5 lines must be provided for SMSP (pass an empty string for lines that are not needed)
		 * @return List of String objects
		 */
		public List<String> getAddressLine() {
			return (List<String>)getValue("AddressLine");
		}
		
		/**
		 * Address lines - exactly 5 lines must be provided for SMSP (pass an empty string for lines that are not needed)
		 * @param AddressLine value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPAddress setAddressLine(List AddressLine) {
			setValue("AddressLine", AddressLine);
			return this;
		}
		
		/**
		 * Address lines - exactly 5 lines must be provided for SMSP (pass an empty string for lines that are not needed)
		 * <br>Note: This adds a String object, but this method can be called
		 * multiple times to add additional String objects.
		 * @param AddressLine value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPAddress addAddressLine(String AddressLine) {
			addMultivalue("AddressLine", AddressLine);
			return this;
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return String object
		 */	
		public String getAddressUse() {
			return (String)getValue("AddressUse");
		}
		
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return AddressType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AddressType getAddressUseEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AddressType.getByCode((String)getValue("AddressUse"));
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @param AddressUse value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPAddress setAddressUse(String AddressUse) {
			setValue("AddressUse", AddressUse);
			return this;
		}
		
		
		/**
		 * Only populate when no address details present
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getNullFlavour() {
			return (String)getValue("NullFlavour");
		}
		
		
		
		/**
		 * Only populate when no address details present
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("NullFlavour"));
		}
		
		
		/**
		 * Only populate when no address details present
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param NullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPAddress setNullFlavour(String NullFlavour) {
			setValue("NullFlavour", NullFlavour);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("Postcode", new Field(
												"Postcode",
												"x:postalCode",
												"Address postcode",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressLine", new Field(
												"AddressLine",
												"x:streetAddressLine",
												"Address lines - exactly 5 lines must be provided for SMSP (pass an empty string for lines that are not needed)",
												"false",
												"",
												"",
												"String",
												"",
												"5",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressUse", new Field(
												"AddressUse",
												"@use",
												"Optional address 'use' that may be used to specify the purpose of the address",
												"false",
												"",
												"AddressType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NullFlavour", new Field(
												"NullFlavour",
												"@nullFlavor",
												"Only populate when no address details present",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SMSPAddress() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SMSPAddress(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
		
	