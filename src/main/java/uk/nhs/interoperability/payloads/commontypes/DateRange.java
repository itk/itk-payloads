/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.commontypes;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the DateRange object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} Low</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} High</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} Center</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class DateRange extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "commonTypesFieldConfig";
		protected static final String name = "DateRange";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.commontypes";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param Low HL7Date
		 * @param High HL7Date
		 */
		public DateRange(HL7Date Low, HL7Date High) {
			fields = new LinkedHashMap<String, Object>();
			
			setLow(Low);
			setHigh(High);
		}

		/**
		 * The start date of the person's performance in the service event
		 * @return HL7Date object
		 */	
		public HL7Date getLow() {
			return (HL7Date)getValue("Low");
		}
		
		
		
		
		/**
		 * The start date of the person's performance in the service event
		 * @param Low value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DateRange setLow(HL7Date Low) {
			setValue("Low", Low);
			return this;
		}
		
		
		/**
		 * The end date of the person's performance in the service event
		 * @return HL7Date object
		 */	
		public HL7Date getHigh() {
			return (HL7Date)getValue("High");
		}
		
		
		
		
		/**
		 * The end date of the person's performance in the service event
		 * @param High value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DateRange setHigh(HL7Date High) {
			setValue("High", High);
			return this;
		}
		
		
		/**
		 * A single point in time (only for use when low and high are not used). This is used when the time when or during which the person performed the service event is not fully known.
		 * @return HL7Date object
		 */	
		public HL7Date getCenter() {
			return (HL7Date)getValue("Center");
		}
		
		
		
		
		/**
		 * A single point in time (only for use when low and high are not used). This is used when the time when or during which the person performed the service event is not fully known.
		 * @param Center value to set
		 * @return this To allow the use of the builder pattern
		 */
		public DateRange setCenter(HL7Date Center) {
			setValue("Center", Center);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("Low", new Field(
												"Low",
												"x:low/@value",
												"The start date of the person's performance in the service event",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("High", new Field(
												"High",
												"x:high/@value",
												"The end date of the person's performance in the service event",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Center", new Field(
												"Center",
												"x:center/@value",
												"A single point in time (only for use when low and high are not used). This is used when the time when or during which the person performed the service event is not fully known.",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public DateRange() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public DateRange(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	