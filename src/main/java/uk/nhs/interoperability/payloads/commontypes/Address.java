/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.commontypes;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Address object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>List&lt;String&gt; AddressLine</li>
 * <li>String City</li>
 * <li>String Postcode</li>
 * <li>String AddressKey</li>
 * <li>String FullAddress</li>
 * <li>String AddressUse</li>
 * <li>String NullFlavour</li>
 * <li>String Description</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.DateRange DateRange} UseablePeriod</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Address extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "commonTypesFieldConfig";
		protected static final String name = "Address";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.commontypes";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param AddressLine String
		 * @param City String
		 * @param Postcode String
		 */
		public Address(List<String> AddressLine, String City, String Postcode) {
			fields = new LinkedHashMap<String, Object>();
			
			setAddressLine(AddressLine);
			setCity(City);
			setPostcode(Postcode);
		}

		/**
		 * @param AddressLine String
		 * @param Postcode String
		 */
		public Address(List<String> AddressLine, String Postcode) {
			fields = new LinkedHashMap<String, Object>();
			
			setAddressLine(AddressLine);
			setPostcode(Postcode);
		}

		/**
		 * @param FullAddress String
		 */
		public Address(String FullAddress) {
			fields = new LinkedHashMap<String, Object>();
			
			setFullAddress(FullAddress);
		}

		/**
		 * Address lines
		 * @return List of String objects
		 */
		public List<String> getAddressLine() {
			return (List<String>)getValue("AddressLine");
		}
		
		/**
		 * Address lines
		 * @param AddressLine value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setAddressLine(List AddressLine) {
			setValue("AddressLine", AddressLine);
			return this;
		}
		
		/**
		 * Address lines
		 * <br>Note: This adds a String object, but this method can be called
		 * multiple times to add additional String objects.
		 * @param AddressLine value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Address addAddressLine(String AddressLine) {
			addMultivalue("AddressLine", AddressLine);
			return this;
		}
		
		
		/**
		 * Address city
		 * @return String object
		 */	
		public String getCity() {
			return (String)getValue("City");
		}
		
		
		
		
		/**
		 * Address city
		 * @param City value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setCity(String City) {
			setValue("City", City);
			return this;
		}
		
		
		/**
		 * Address postcode
		 * @return String object
		 */	
		public String getPostcode() {
			return (String)getValue("Postcode");
		}
		
		
		
		
		/**
		 * Address postcode
		 * @param Postcode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setPostcode(String Postcode) {
			setValue("Postcode", Postcode);
			return this;
		}
		
		
		/**
		 * Used for PDS addresses to return the PAF key
		 * @return String object
		 */	
		public String getAddressKey() {
			return (String)getValue("AddressKey");
		}
		
		
		
		
		/**
		 * Used for PDS addresses to return the PAF key
		 * @param AddressKey value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setAddressKey(String AddressKey) {
			setValue("AddressKey", AddressKey);
			return this;
		}
		
		
		/**
		 * Unstructured address
		 * @return String object
		 */	
		public String getFullAddress() {
			return (String)getValue("FullAddress");
		}
		
		
		
		
		/**
		 * Unstructured address
		 * @param FullAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setFullAddress(String FullAddress) {
			setValue("FullAddress", FullAddress);
			return this;
		}
		
		
		/**
		 * For internal use within the parser
		 * @param val value to set
		 */
		public void setTextValue(String val) {
			setValue("FullAddress", val);
		}
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return String object
		 */	
		public String getAddressUse() {
			return (String)getValue("AddressUse");
		}
		
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @return AddressType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.AddressType getAddressUseEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.AddressType.getByCode((String)getValue("AddressUse"));
		}
		
		
		/**
		 * Optional address 'use' that may be used to specify the purpose of the address
  		 * <br>NOTE: This field should be populated using the "AddressType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.AddressType
		 * @param AddressUse value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setAddressUse(String AddressUse) {
			setValue("AddressUse", AddressUse);
			return this;
		}
		
		
		/**
		 * Only populate when no address details present
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return String object
		 */	
		public String getNullFlavour() {
			return (String)getValue("NullFlavour");
		}
		
		
		
		/**
		 * Only populate when no address details present
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @return NullFlavour enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour getNullFlavourEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour.getByCode((String)getValue("NullFlavour"));
		}
		
		
		/**
		 * Only populate when no address details present
  		 * <br>NOTE: This field should be populated using the "NullFlavour" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.NullFlavour
		 * @param NullFlavour value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setNullFlavour(String NullFlavour) {
			setValue("NullFlavour", NullFlavour);
			return this;
		}
		
		
		/**
		 * A description of the address; this may be used for things like directions for the address. Only populate for addresses with use 'PHYS'
		 * @return String object
		 */	
		public String getDescription() {
			return (String)getValue("Description");
		}
		
		
		
		
		/**
		 * A description of the address; this may be used for things like directions for the address. Only populate for addresses with use 'PHYS'
		 * @param Description value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setDescription(String Description) {
			setValue("Description", Description);
			return this;
		}
		
		
		/**
		 * Used to indicate some of the following types of valid times for an address:A point in time when the contact address details are validA start and end time (Just including hours and minutes to indicate opening hours of an office for a work address, Using dates for indicating a contact address is only for a certain period of time)A start time only to indicate when the contact address is valid from.An end time only to indicate that the contact address is only valid until a certain date.
		 * @return uk.nhs.interoperability.payloads.commontypes.DateRange object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.DateRange getUseablePeriod() {
			return (uk.nhs.interoperability.payloads.commontypes.DateRange)getValue("UseablePeriod");
		}
		
		
		
		
		/**
		 * Used to indicate some of the following types of valid times for an address:A point in time when the contact address details are validA start and end time (Just including hours and minutes to indicate opening hours of an office for a work address, Using dates for indicating a contact address is only for a certain period of time)A start time only to indicate when the contact address is valid from.An end time only to indicate that the contact address is only valid until a certain date.
		 * @param UseablePeriod value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Address setUseablePeriod(uk.nhs.interoperability.payloads.commontypes.DateRange UseablePeriod) {
			setValue("UseablePeriod", UseablePeriod);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("AddressLine", new Field(
												"AddressLine",
												"x:streetAddressLine",
												"Address lines",
												"false",
												"",
												"",
												"String",
												"",
												"3",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("City", new Field(
												"City",
												"x:city",
												"Address city",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Postcode", new Field(
												"Postcode",
												"x:postalCode",
												"Address postcode",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressKey", new Field(
												"AddressKey",
												"x:addressKey",
												"Used for PDS addresses to return the PAF key",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("FullAddress", new Field(
												"FullAddress",
												".",
												"Unstructured address",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AddressUse", new Field(
												"AddressUse",
												"@use",
												"Optional address 'use' that may be used to specify the purpose of the address",
												"false",
												"",
												"AddressType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NullFlavour", new Field(
												"NullFlavour",
												"@nullFlavor",
												"Only populate when no address details present",
												"false",
												"",
												"NullFlavour",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Description", new Field(
												"Description",
												"x:desc",
												"A description of the address; this may be used for things like directions for the address. Only populate for addresses with use 'PHYS'",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("UseablePeriod", new Field(
												"UseablePeriod",
												"x:useablePeriod",
												"Used to indicate some of the following types of valid times for an address:A point in time when the contact address details are validA start and end time (Just including hours and minutes to indicate opening hours of an office for a work address, Using dates for indicating a contact address is only for a certain period of time)A start time only to indicate when the contact address is valid from.An end time only to indicate that the contact address is only valid until a certain date.",
												"false",
												"",
												"",
												"DateRange",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Address() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Address(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	