/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.commontypes;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SMSPPersonName object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String GivenName</li>
 * <li>String FamilyName</li>
 * <li>String NameType</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SMSPPersonName extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "commonTypesFieldConfig";
		protected static final String name = "SMSPPersonName";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.commontypes";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param GivenName String
		 * @param FamilyName String
		 * @param NameType String
		 */
		public SMSPPersonName(String GivenName, String FamilyName, String NameType) {
			fields = new LinkedHashMap<String, Object>();
			
			setGivenName(GivenName);
			setFamilyName(FamilyName);
			setNameType(NameType);
		}

		/**
		 * @param GivenName String
		 * @param FamilyName String
		 */
		public SMSPPersonName(String GivenName, String FamilyName) {
			fields = new LinkedHashMap<String, Object>();
			
			setGivenName(GivenName);
			setFamilyName(FamilyName);
		}

		/**
		 * Given name (forename)
		 * @return String object
		 */	
		public String getGivenName() {
			return (String)getValue("GivenName");
		}
		
		
		
		
		/**
		 * Given name (forename)
		 * @param GivenName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPPersonName setGivenName(String GivenName) {
			setValue("GivenName", GivenName);
			return this;
		}
		
		
		/**
		 * Family name (surname)
		 * @return String object
		 */	
		public String getFamilyName() {
			return (String)getValue("FamilyName");
		}
		
		
		
		
		/**
		 * Family name (surname)
		 * @param FamilyName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPPersonName setFamilyName(String FamilyName) {
			setValue("FamilyName", FamilyName);
			return this;
		}
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @return String object
		 */	
		public String getNameType() {
			return (String)getValue("NameType");
		}
		
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @return PersonNameType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType getNameTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType.getByCode((String)getValue("NameType"));
		}
		
		
		/**
		 * Name use code
  		 * <br>NOTE: This field should be populated using the "PersonNameType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.PersonNameType
		 * @param NameType value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SMSPPersonName setNameType(String NameType) {
			setValue("NameType", NameType);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("GivenName", new Field(
												"GivenName",
												"x:given",
												"Given name (forename)",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("FamilyName", new Field(
												"FamilyName",
												"x:family",
												"Family name (surname)",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameType", new Field(
												"NameType",
												"@use",
												"Name use code",
												"false",
												"",
												"PersonNameType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SMSPPersonName() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SMSPPersonName(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	
	