/**
 * Some common types used in various payload classes - mainly identifiers, but also addresses
 */
package uk.nhs.interoperability.payloads.commontypes;