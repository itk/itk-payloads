/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the GetNHSNumberResponse object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String MessageID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ResponseCode</li>
 * <li>String NHSNumber</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class GetNHSNumberResponse extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "pdsminiFieldConfig";
		protected static final String name = "GetNHSNumberResponse";
		protected static final String shortName = "";
		protected static final String rootNode = "getNHSNumberResponse-v1-0";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.pdsminiservicesv1_1";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param MessageID MessageID
		 * @param ResponseCode ResponseCode
		 * @param NHSNumber NHSNumber
		 */
	    public GetNHSNumberResponse(String MessageID, CodedValue ResponseCode, String NHSNumber) {
			fields = new LinkedHashMap<String, Object>();
			
			setMessageID(MessageID);
			setResponseCode(ResponseCode);
			setNHSNumber(NHSNumber);
		}
	
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getMessageID() {
			return (String)getValue("MessageID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @param MessageID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberResponse setMessageID(String MessageID) {
			setValue("MessageID", MessageID);
			return this;
		}
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getResponseCode() {
			return (CodedValue)getValue("ResponseCode");
		}
		
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * @return PDSMiniServiceResponseCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode getResponseCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ResponseCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("PDSMiniServiceResponseCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode)entry;
		}
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * @param ResponseCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberResponse setResponseCode(CodedValue ResponseCode) {
			setValue("ResponseCode", ResponseCode);
			return this;
		}
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ResponseCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberResponse setResponseCode(VocabularyEntry ResponseCode) {
			Code c = new CodedValue(ResponseCode);
			setValue("ResponseCode", c);
			return this;
		}
		
		/**
		 * The NHS Number
		 * @return String object
		 */	
		public String getNHSNumber() {
			return (String)getValue("NHSNumber");
		}
		
		
		
		
		/**
		 * The NHS Number
		 * @param NHSNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberResponse setNHSNumber(String NHSNumber) {
			setValue("NHSNumber", NHSNumber);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageID", new Field(
												"MessageID",
												"x:id/@root",
												"A DCE UUID to identify this message",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCode", new Field(
												"MessageCode",
												"x:code/@code",
												"getNHSNumberResponse-v1-0",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCodeSystem", new Field(
												"MessageCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.284",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseCode", new Field(
												"ResponseCode",
												"x:value",
												"A code which indicates the type of response",
												"true",
												"",
												"PDSMiniServiceResponseCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
		put("SubjectTypeCode", new Field(
												"SubjectTypeCode",
												"x:subject/@typeCode",
												"SBJ",
												"NHSNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientClassCode", new Field(
												"PatientClassCode",
												"x:subject/x:patient/@classCode",
												"PAT",
												"NHSNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumberRoot", new Field(
												"NHSNumberRoot",
												"x:subject/x:patient/x:id/@root",
												"2.16.840.1.113883.2.1.4.1",
												"NHSNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumber", new Field(
												"NHSNumber",
												"x:subject/x:patient/x:id/@extension",
												"The NHS Number",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public GetNHSNumberResponse() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public GetNHSNumberResponse(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}


	