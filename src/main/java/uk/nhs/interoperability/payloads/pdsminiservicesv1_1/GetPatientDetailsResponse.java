/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the GetPatientDetailsResponse object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String MessageID</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} ResponseCode</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.PatientID PatientID}&gt; NHSNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonName PersonName} PatientName</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.SMSPAddress SMSPAddress}&gt; Address</li>
 * <li>List&lt;{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom}&gt; TelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Gender</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DateOfBirth</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} DateOfDeath</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SMSPAddress SMSPAddress} GPAddress</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.Telecom Telecom} GPTelephoneNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgID OrgID} GPOrganisationID</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.OrgName OrgName} GPPracticeName</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class GetPatientDetailsResponse extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "pdsminiFieldConfig";
		protected static final String name = "GetPatientDetailsResponse";
		protected static final String shortName = "";
		protected static final String rootNode = "getPatientDetailsResponse-v1-0";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.pdsminiservicesv1_1";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param MessageID MessageID
		 * @param ResponseCode ResponseCode
		 * @param NHSNumber NHSNumber
		 * @param PatientName PatientName
		 * @param Address Address
		 * @param TelephoneNumber TelephoneNumber
		 * @param Gender Gender
		 * @param DateOfBirth DateOfBirth
		 * @param DateOfDeath DateOfDeath
		 * @param GPAddress GPAddress
		 * @param GPTelephoneNumber GPTelephoneNumber
		 * @param GPOrganisationID GPOrganisationID
		 * @param GPPracticeName GPPracticeName
		 */
	    public GetPatientDetailsResponse(String MessageID, CodedValue ResponseCode, 
		List<uk.nhs.interoperability.payloads.commontypes.PatientID> NHSNumber, uk.nhs.interoperability.payloads.commontypes.PersonName PatientName, 
		List<uk.nhs.interoperability.payloads.commontypes.SMSPAddress> Address, 
		List<uk.nhs.interoperability.payloads.commontypes.Telecom> TelephoneNumber, CodedValue Gender, HL7Date DateOfBirth, HL7Date DateOfDeath, uk.nhs.interoperability.payloads.commontypes.SMSPAddress GPAddress, uk.nhs.interoperability.payloads.commontypes.Telecom GPTelephoneNumber, uk.nhs.interoperability.payloads.commontypes.OrgID GPOrganisationID, uk.nhs.interoperability.payloads.commontypes.OrgName GPPracticeName) {
			fields = new LinkedHashMap<String, Object>();
			
			setMessageID(MessageID);
			setResponseCode(ResponseCode);
			setNHSNumber(NHSNumber);
			setPatientName(PatientName);
			setAddress(Address);
			setTelephoneNumber(TelephoneNumber);
			setGender(Gender);
			setDateOfBirth(DateOfBirth);
			setDateOfDeath(DateOfDeath);
			setGPAddress(GPAddress);
			setGPTelephoneNumber(GPTelephoneNumber);
			setGPOrganisationID(GPOrganisationID);
			setGPPracticeName(GPPracticeName);
		}
	
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getMessageID() {
			return (String)getValue("MessageID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @param MessageID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setMessageID(String MessageID) {
			setValue("MessageID", MessageID);
			return this;
		}
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getResponseCode() {
			return (CodedValue)getValue("ResponseCode");
		}
		
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * @return PDSMiniServiceResponseCode enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode getResponseCodeEnum() {
			CodedValue cv = (CodedValue)getValue("ResponseCode");
			VocabularyEntry entry = VocabularyFactory.getVocab("PDSMiniServiceResponseCode", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode)entry;
		}
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * @param ResponseCode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setResponseCode(CodedValue ResponseCode) {
			setValue("ResponseCode", ResponseCode);
			return this;
		}
		
		
		/**
		 * A code which indicates the type of response
  		 * <br>NOTE: This field should be populated using the "PDSMiniServiceResponseCode" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.PDSMiniServiceResponseCode
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param ResponseCode value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setResponseCode(VocabularyEntry ResponseCode) {
			Code c = new CodedValue(ResponseCode);
			setValue("ResponseCode", c);
			return this;
		}
		
		/**
		 * The NHS Number for the patient. If a local identifier also exists, a second ID element will be used to hold it.
		 * @return List of uk.nhs.interoperability.payloads.commontypes.PatientID objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.PatientID> getNHSNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.PatientID>)getValue("NHSNumber");
		}
		
		/**
		 * The NHS Number for the patient. If a local identifier also exists, a second ID element will be used to hold it.
		 * @param NHSNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setNHSNumber(List NHSNumber) {
			setValue("NHSNumber", NHSNumber);
			return this;
		}
		
		/**
		 * The NHS Number for the patient. If a local identifier also exists, a second ID element will be used to hold it.
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.PatientID object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.PatientID objects.
		 * @param NHSNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse addNHSNumber(uk.nhs.interoperability.payloads.commontypes.PatientID NHSNumber) {
			addMultivalue("NHSNumber", NHSNumber);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonName getPatientName() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonName)getValue("PatientName");
		}
		
		
		
		
		/**
		 * 
		 * @param PatientName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setPatientName(uk.nhs.interoperability.payloads.commontypes.PersonName PatientName) {
			setValue("PatientName", PatientName);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.SMSPAddress objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.SMSPAddress> getAddress() {
			return (List<uk.nhs.interoperability.payloads.commontypes.SMSPAddress>)getValue("Address");
		}
		
		/**
		 * 
		 * @param Address value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setAddress(List Address) {
			setValue("Address", Address);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.SMSPAddress object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.SMSPAddress objects.
		 * @param Address value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse addAddress(uk.nhs.interoperability.payloads.commontypes.SMSPAddress Address) {
			addMultivalue("Address", Address);
			return this;
		}
		
		
		/**
		 * 
		 * @return List of uk.nhs.interoperability.payloads.commontypes.Telecom objects
		 */
		public List<uk.nhs.interoperability.payloads.commontypes.Telecom> getTelephoneNumber() {
			return (List<uk.nhs.interoperability.payloads.commontypes.Telecom>)getValue("TelephoneNumber");
		}
		
		/**
		 * 
		 * @param TelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setTelephoneNumber(List TelephoneNumber) {
			setValue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		/**
		 * 
		 * <br>Note: This adds a uk.nhs.interoperability.payloads.commontypes.Telecom object, but this method can be called
		 * multiple times to add additional uk.nhs.interoperability.payloads.commontypes.Telecom objects.
		 * @param TelephoneNumber value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse addTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom TelephoneNumber) {
			addMultivalue("TelephoneNumber", TelephoneNumber);
			return this;
		}
		
		
		/**
		 * A code which indicates the gender.
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return CodedValue object
		 */	
		public CodedValue getGender() {
			return (CodedValue)getValue("Gender");
		}
		
		
		
		/**
		 * A code which indicates the gender.
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getGenderEnum() {
			CodedValue cv = (CodedValue)getValue("Gender");
			VocabularyEntry entry = VocabularyFactory.getVocab("Sex", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Sex)entry;
		}
		
		
		/**
		 * A code which indicates the gender.
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Gender value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setGender(CodedValue Gender) {
			setValue("Gender", Gender);
			return this;
		}
		
		
		/**
		 * A code which indicates the gender.
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Gender value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setGender(VocabularyEntry Gender) {
			Code c = new CodedValue(Gender);
			setValue("Gender", c);
			return this;
		}
		
		/**
		 * The date of birth for the patient
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * The date of birth for the patient
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * The date of deat of the patient
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfDeath() {
			return (HL7Date)getValue("DateOfDeath");
		}
		
		
		
		
		/**
		 * The date of deat of the patient
		 * @param DateOfDeath value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setDateOfDeath(HL7Date DateOfDeath) {
			setValue("DateOfDeath", DateOfDeath);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.SMSPAddress object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SMSPAddress getGPAddress() {
			return (uk.nhs.interoperability.payloads.commontypes.SMSPAddress)getValue("GPAddress");
		}
		
		
		
		
		/**
		 * 
		 * @param GPAddress value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setGPAddress(uk.nhs.interoperability.payloads.commontypes.SMSPAddress GPAddress) {
			setValue("GPAddress", GPAddress);
			return this;
		}
		
		
		/**
		 * 
		 * @return uk.nhs.interoperability.payloads.commontypes.Telecom object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.Telecom getGPTelephoneNumber() {
			return (uk.nhs.interoperability.payloads.commontypes.Telecom)getValue("GPTelephoneNumber");
		}
		
		
		
		
		/**
		 * 
		 * @param GPTelephoneNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setGPTelephoneNumber(uk.nhs.interoperability.payloads.commontypes.Telecom GPTelephoneNumber) {
			setValue("GPTelephoneNumber", GPTelephoneNumber);
			return this;
		}
		
		
		/**
		 * The identifier for the GP practice organisation.
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgID getGPOrganisationID() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgID)getValue("GPOrganisationID");
		}
		
		
		
		
		/**
		 * The identifier for the GP practice organisation.
		 * @param GPOrganisationID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setGPOrganisationID(uk.nhs.interoperability.payloads.commontypes.OrgID GPOrganisationID) {
			setValue("GPOrganisationID", GPOrganisationID);
			return this;
		}
		
		
		/**
		 * The name of the GP practice.
		 * @return uk.nhs.interoperability.payloads.commontypes.OrgName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.OrgName getGPPracticeName() {
			return (uk.nhs.interoperability.payloads.commontypes.OrgName)getValue("GPPracticeName");
		}
		
		
		
		
		/**
		 * The name of the GP practice.
		 * @param GPPracticeName value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetPatientDetailsResponse setGPPracticeName(uk.nhs.interoperability.payloads.commontypes.OrgName GPPracticeName) {
			setValue("GPPracticeName", GPPracticeName);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"OBS",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageID", new Field(
												"MessageID",
												"x:id/@root",
												"A DCE UUID to identify this message",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCode", new Field(
												"MessageCode",
												"x:code/@code",
												"getPatientDetailsResponse-v1-0",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCodeSystem", new Field(
												"MessageCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.284",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ResponseCode", new Field(
												"ResponseCode",
												"x:value",
												"A code which indicates the type of response",
												"true",
												"",
												"PDSMiniServiceResponseCode",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
		put("TypeCode", new Field(
												"TypeCode",
												"x:subject/@typeCode",
												"SBJ",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientClassCode", new Field(
												"PatientClassCode",
												"x:subject/x:patient/@classCode",
												"PAT",
												"NHSNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumber", new Field(
												"NHSNumber",
												"x:subject/x:patient/x:id",
												"The NHS Number for the patient. If a local identifier also exists, a second ID element will be used to hold it.",
												"false",
												"",
												"",
												"PatientID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"2",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PatientName", new Field(
												"PatientName",
												"x:subject/x:patient/x:name",
												"",
												"false",
												"",
												"",
												"PersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Address", new Field(
												"Address",
												"x:subject/x:patient/x:addr",
												"",
												"false",
												"",
												"",
												"SMSPAddress",
												"uk.nhs.interoperability.payloads.commontypes.",
												"3",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TelephoneNumber", new Field(
												"TelephoneNumber",
												"x:subject/x:patient/x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonClassCode", new Field(
												"PersonClassCode",
												"x:subject/x:patient/x:patientPerson/@classCode",
												"PSN",
												"Gender",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PersonDeterminerCode", new Field(
												"PersonDeterminerCode",
												"x:subject/x:patient/x:patientPerson/@determinerCode",
												"INSTANCE",
												"Gender",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Gender", new Field(
												"Gender",
												"x:subject/x:patient/x:patientPerson/x:administrativeGenderCode",
												"A code which indicates the gender.",
												"false",
												"",
												"Sex",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:subject/x:patient/x:patientPerson/x:birthTime/@value",
												"The date of birth for the patient",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfDeath", new Field(
												"DateOfDeath",
												"x:subject/x:patient/x:patientPerson/x:deceasedTime",
												"The date of deat of the patient",
												"false",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GPClassCode", new Field(
												"GPClassCode",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/@classCode",
												"SDLOC",
												"GPAddress",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GPAddress", new Field(
												"GPAddress",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/x:addr",
												"",
												"false",
												"",
												"",
												"SMSPAddress",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GPTelephoneNumber", new Field(
												"GPTelephoneNumber",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/x:telecom",
												"",
												"false",
												"",
												"",
												"Telecom",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgClassCode", new Field(
												"OrgClassCode",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/x:locationOrganization/@classCode",
												"ORG",
												"GPOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OrgDeterminerCode", new Field(
												"OrgDeterminerCode",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/x:locationOrganization/@determinerCode",
												"INSTANCE",
												"GPOrganisationID",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GPOrganisationID", new Field(
												"GPOrganisationID",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/x:locationOrganization/x:id",
												"The identifier for the GP practice organisation.",
												"false",
												"",
												"",
												"OrgID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("GPPracticeName", new Field(
												"GPPracticeName",
												"x:subject/x:patient/x:patientPerson/x:gPPractice/x:locationOrganization/x:name",
												"The name of the GP practice.",
												"false",
												"",
												"",
												"OrgName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public GetPatientDetailsResponse() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public GetPatientDetailsResponse(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	