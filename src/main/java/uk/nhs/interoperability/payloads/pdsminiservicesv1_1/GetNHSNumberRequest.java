/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the GetNHSNumberRequest object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String MessageID</li>
 * <li>{@link HL7Date HL7Date} DateOfBirth</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} Gender</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.PersonID PersonID} LocalIdentifier</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SMSPPersonName SMSPPersonName} Name</li>
 * <li>{@link String String} Postcode</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class GetNHSNumberRequest extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "pdsminiFieldConfig";
		protected static final String name = "GetNHSNumberRequest";
		protected static final String shortName = "";
		protected static final String rootNode = "getNHSNumberRequest-v1-0";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.pdsminiservicesv1_1";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param MessageID MessageID
		 * @param DateOfBirth DateOfBirth
		 * @param Gender Gender
		 * @param LocalIdentifier LocalIdentifier
		 * @param Name Name
		 * @param Postcode Postcode
		 */
	    public GetNHSNumberRequest(String MessageID, HL7Date DateOfBirth, CodedValue Gender, uk.nhs.interoperability.payloads.commontypes.PersonID LocalIdentifier, uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name, String Postcode) {
			fields = new LinkedHashMap<String, Object>();
			
			setMessageID(MessageID);
			setDateOfBirth(DateOfBirth);
			setGender(Gender);
			setLocalIdentifier(LocalIdentifier);
			setName(Name);
			setPostcode(Postcode);
		}
	
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getMessageID() {
			return (String)getValue("MessageID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @param MessageID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setMessageID(String MessageID) {
			setValue("MessageID", MessageID);
			return this;
		}
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return CodedValue object
		 */	
		public CodedValue getGender() {
			return (CodedValue)getValue("Gender");
		}
		
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @return Sex enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.Sex getGenderEnum() {
			CodedValue cv = (CodedValue)getValue("Gender");
			VocabularyEntry entry = VocabularyFactory.getVocab("Sex", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.Sex)entry;
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * @param Gender value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setGender(CodedValue Gender) {
			setValue("Gender", Gender);
			return this;
		}
		
		
		/**
		 * Gender Query Parameter
  		 * <br>NOTE: This field should be populated using the "Sex" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.Sex
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param Gender value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setGender(VocabularyEntry Gender) {
			Code c = new CodedValue(Gender);
			setValue("Gender", c);
			return this;
		}
		
		/**
		 * LocalIdentifier Query Parameter
		 * @return uk.nhs.interoperability.payloads.commontypes.PersonID object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.PersonID getLocalIdentifier() {
			return (uk.nhs.interoperability.payloads.commontypes.PersonID)getValue("LocalIdentifier");
		}
		
		
		
		
		/**
		 * LocalIdentifier Query Parameter
		 * @param LocalIdentifier value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setLocalIdentifier(uk.nhs.interoperability.payloads.commontypes.PersonID LocalIdentifier) {
			setValue("LocalIdentifier", LocalIdentifier);
			return this;
		}
		
		
		/**
		 * Name Query Parameter
		 * @return uk.nhs.interoperability.payloads.commontypes.SMSPPersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SMSPPersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.SMSPPersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Name Query Parameter
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setName(uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		
		/**
		 * Postcode Query Parameter
		 * @return String object
		 */	
		public String getPostcode() {
			return (String)getValue("Postcode");
		}
		
		
		
		
		/**
		 * Postcode Query Parameter
		 * @param Postcode value to set
		 * @return this To allow the use of the builder pattern
		 */
		public GetNHSNumberRequest setPostcode(String Postcode) {
			setValue("Postcode", Postcode);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageID", new Field(
												"MessageID",
												"x:id/@root",
												"A DCE UUID to identify this message",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCode", new Field(
												"MessageCode",
												"x:code/@code",
												"getNHSNumberRequest-v1-0",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCodeSystem", new Field(
												"MessageCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.284",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:queryEvent/x:Person.DateOfBirth/x:value/@value",
												"DateOfBirth Query Parameter",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirthSemanticsText", new Field(
												"DateOfBirthSemanticsText",
												"x:queryEvent/x:Person.DateOfBirth/x:semanticsText",
												"Person.DateOfBirth",
												"DateOfBirth",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Gender", new Field(
												"Gender",
												"x:queryEvent/x:Person.Gender/x:value",
												"Gender Query Parameter",
												"",
												"",
												"Sex",
												"CodedValue",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"true",
												"",
												""
												));
	
		put("GenderSemanticsText", new Field(
												"GenderSemanticsText",
												"x:queryEvent/x:Person.Gender/x:semanticsText",
												"Person.Gender",
												"Gender",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LocalIdentifier", new Field(
												"LocalIdentifier",
												"x:queryEvent/x:Person.LocalIdentifier/x:value/@extension",
												"LocalIdentifier Query Parameter",
												"",
												"",
												"",
												"PersonID",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LocalIdentifierSemanticsText", new Field(
												"LocalIdentifierSemanticsText",
												"x:queryEvent/x:Person.LocalIdentifier/x:semanticsText",
												"Person.LocalIdentifier",
												"LocalIdentifier",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:queryEvent/x:Person.Name/x:value",
												"Name Query Parameter",
												"",
												"",
												"",
												"SMSPPersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameSemanticsText", new Field(
												"NameSemanticsText",
												"x:queryEvent/x:Person.Name/x:semanticsText",
												"Person.Name",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Postcode", new Field(
												"Postcode",
												"x:queryEvent/x:Person.Postcode/x:value",
												"Postcode Query Parameter",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("PostcodeSemanticsText", new Field(
												"PostcodeSemanticsText",
												"x:queryEvent/x:Person.Postcode/x:semanticsText",
												"Person.Postcode",
												"Postcode",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public GetNHSNumberRequest() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public GetNHSNumberRequest(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	