/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.pdsminiservicesv1_1;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the VerifyNHSNumberRequest object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String MessageID</li>
 * <li>{@link HL7Date HL7Date} DateOfBirth</li>
 * <li>String NHSNumber</li>
 * <li>{@link uk.nhs.interoperability.payloads.commontypes.SMSPPersonName SMSPPersonName} Name</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class VerifyNHSNumberRequest extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "pdsminiFieldConfig";
		protected static final String name = "VerifyNHSNumberRequest";
		protected static final String shortName = "";
		protected static final String rootNode = "verifyNHSNumberRequest-v1-0";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.pdsminiservicesv1_1";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param MessageID MessageID
		 * @param DateOfBirth DateOfBirth
		 * @param NHSNumber NHSNumber
		 * @param Name Name
		 */
	    public VerifyNHSNumberRequest(String MessageID, HL7Date DateOfBirth, String NHSNumber, uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name) {
			fields = new LinkedHashMap<String, Object>();
			
			setMessageID(MessageID);
			setDateOfBirth(DateOfBirth);
			setNHSNumber(NHSNumber);
			setName(Name);
		}
	
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getMessageID() {
			return (String)getValue("MessageID");
		}
		
		
		
		
		/**
		 * A DCE UUID to identify this message
		 * <br><br>This field is MANDATORY
		 * @param MessageID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public VerifyNHSNumberRequest setMessageID(String MessageID) {
			setValue("MessageID", MessageID);
			return this;
		}
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @return HL7Date object
		 */	
		public HL7Date getDateOfBirth() {
			return (HL7Date)getValue("DateOfBirth");
		}
		
		
		
		
		/**
		 * DateOfBirth Query Parameter
		 * @param DateOfBirth value to set
		 * @return this To allow the use of the builder pattern
		 */
		public VerifyNHSNumberRequest setDateOfBirth(HL7Date DateOfBirth) {
			setValue("DateOfBirth", DateOfBirth);
			return this;
		}
		
		
		/**
		 * NHSNumber Query Parameter
		 * @return String object
		 */	
		public String getNHSNumber() {
			return (String)getValue("NHSNumber");
		}
		
		
		
		
		/**
		 * NHSNumber Query Parameter
		 * @param NHSNumber value to set
		 * @return this To allow the use of the builder pattern
		 */
		public VerifyNHSNumberRequest setNHSNumber(String NHSNumber) {
			setValue("NHSNumber", NHSNumber);
			return this;
		}
		
		
		/**
		 * Name Query Parameter
		 * @return uk.nhs.interoperability.payloads.commontypes.SMSPPersonName object
		 */	
		public uk.nhs.interoperability.payloads.commontypes.SMSPPersonName getName() {
			return (uk.nhs.interoperability.payloads.commontypes.SMSPPersonName)getValue("Name");
		}
		
		
		
		
		/**
		 * Name Query Parameter
		 * @param Name value to set
		 * @return this To allow the use of the builder pattern
		 */
		public VerifyNHSNumberRequest setName(uk.nhs.interoperability.payloads.commontypes.SMSPPersonName Name) {
			setValue("Name", Name);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("ClassCode", new Field(
												"ClassCode",
												"@classCode",
												"CACT",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MoodCode", new Field(
												"MoodCode",
												"@moodCode",
												"EVN",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageID", new Field(
												"MessageID",
												"x:id/@root",
												"A DCE UUID to identify this message",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCode", new Field(
												"MessageCode",
												"x:code/@code",
												"verifyNHSNumberRequest-v1-0",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("MessageCodeSystem", new Field(
												"MessageCodeSystem",
												"x:code/@codeSystem",
												"2.16.840.1.113883.2.1.3.2.4.17.284",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirth", new Field(
												"DateOfBirth",
												"x:queryEvent/x:Person.DateOfBirth/x:value/@value",
												"DateOfBirth Query Parameter",
												"",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("DateOfBirthSemanticsText", new Field(
												"DateOfBirthSemanticsText",
												"x:queryEvent/x:Person.DateOfBirth/x:semanticsText",
												"Person.DateOfBirth",
												"DateOfBirth",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumber", new Field(
												"NHSNumber",
												"x:queryEvent/x:Person.NHSNumber/x:value/@extension",
												"NHSNumber Query Parameter",
												"",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumberOID", new Field(
												"NHSNumberOID",
												"x:queryEvent/x:Person.NHSNumber/x:value/@root",
												"2.16.840.1.113883.2.1.4.1",
												"NHSNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NHSNumberSemanticsText", new Field(
												"NHSNumberSemanticsText",
												"x:queryEvent/x:Person.NHSNumber/x:semanticsText",
												"Person.NHSNumber",
												"NHSNumber",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Name", new Field(
												"Name",
												"x:queryEvent/x:Person.Name/x:value",
												"Name Query Parameter",
												"",
												"",
												"",
												"SMSPPersonName",
												"uk.nhs.interoperability.payloads.commontypes.",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("NameSemanticsText", new Field(
												"NameSemanticsText",
												"x:queryEvent/x:Person.Name/x:semanticsText",
												"Person.Name",
												"Name",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("", "urn:hl7-org:v3", false);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			namespaces.addNamespace("npfitlc", "NPFIT:HL7:Localisation", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public VerifyNHSNumberRequest() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public VerifyNHSNumberRequest(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	
	