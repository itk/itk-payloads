/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import uk.nhs.interoperability.payloads.util.Emptiable;
import uk.nhs.interoperability.payloads.util.Emptiables;
import uk.nhs.interoperability.payloads.util.xml.PayloadParser;
import uk.nhs.interoperability.payloads.util.xml.PayloadSerialiser;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;

public abstract class AbstractPayload implements Payload {
	private static final Logger logger = LoggerFactory.getLogger(AbstractPayload.class);
	protected LinkedHashMap<String, Object> fields;
	
	protected HashMap<String, String> unresolvedFieldValues;
	protected HashMap<String, String> unresolvedFieldNames;
	
	private String parentObjectXPath = "";
	private ArrayList parentObjectNames = new ArrayList();
	
	public ArrayList getParentObjectNames() {
		return parentObjectNames;
	}

	public void setParentObjectNames(ArrayList parentObjectNames) {
		this.parentObjectNames = parentObjectNames;
	}

	public String getParentObjectXPath() {
		return parentObjectXPath;
	}

	public void setParentObjectXPath(String parentObjectXPath) {
		this.parentObjectXPath = parentObjectXPath;
	}

	public void setFields(LinkedHashMap<String, Object> fields) {
		this.fields = fields;
	}
	
	public String serialise(Payload p) {
		String rootElementName = p.getClassName();
		if (p.getRootNode() != null) {
			if (p.getRootNode().length()>0) {
				rootElementName = p.getRootNode();
			}
		}
		return serialise(p, rootElementName);
	}
	
	public String serialise(Payload p, String name) {
		long startTime = System.currentTimeMillis();
		String result = PayloadSerialiser.serialise(this, name);
		logger.info("TIMER-Serialise: {} ms", (System.currentTimeMillis() - startTime));
		return result;
	}
	
	public String serialise(Payload p, String name, XMLNamespaceContext parentNamespaces) {
		long startTime = System.currentTimeMillis();
		String result = PayloadSerialiser.serialise(this, name, parentNamespaces);
		logger.info("TIMER-Serialise: {} ms", (System.currentTimeMillis() - startTime));
		return result;
	}
	
	public void parse(String xml, Payload p, String name) {
		long startTime = System.currentTimeMillis();
		PayloadParser.parse(xml, p, name);
		logger.info("TIMER-Parse: {} ms", (System.currentTimeMillis() - startTime));
	}
	
	public void parse(String xml, Payload p, String name, XMLNamespaceContext parentNamespaces) {
		long startTime = System.currentTimeMillis();
		PayloadParser.parse(xml, p, name, parentNamespaces);
		logger.info("TIMER-Parse: {} ms", (System.currentTimeMillis() - startTime));
	}
	
	public void parse(InputStream xml, Payload p, String name) {
		long startTime = System.currentTimeMillis();
		PayloadParser.parse(xml, p, name, null);
		logger.info("TIMER-Parse: {} ms", (System.currentTimeMillis() - startTime));
	}
	
	public void parse(InputStream xml, Payload p, String name, XMLNamespaceContext parentNamespaces) {
		long startTime = System.currentTimeMillis();
		PayloadParser.parse(xml, p, name, parentNamespaces);
		logger.info("TIMER-Parse: {} ms", (System.currentTimeMillis() - startTime));
	}
	
	public static void parse(Document xmldoc, Payload p, String name) {
		long startTime = System.currentTimeMillis();
		PayloadParser.parse(xmldoc, p, name, null);
		logger.info("TIMER-Parse: {} ms", (System.currentTimeMillis() - startTime));
	}
	
	public static void parse(Document xmldoc, Payload p, String name, XMLNamespaceContext parentNamespaces) {
		long startTime = System.currentTimeMillis();
		PayloadParser.parse(xmldoc, p, name, parentNamespaces);
		logger.info("TIMER-Parse: {} ms", (System.currentTimeMillis() - startTime));
	}
	
	public Object getValue(String fieldName) {
		return fields.get(fieldName);
	}
	
	public boolean hasData() {
		return (fields.size()>0);
	}

	public void setValue(String key, Object value) {
		if ((value != null) && (key != null)) {
			fields.put(key, value);
		}
	}
	
	public void addMultivalue(String key, Object value) {
		addMultivalue(key, value, false);
	}
	
	public void addMultivalue(String key, Object value, boolean reverseOrder) {
		ArrayList a = (ArrayList)fields.get(key);
		if (a == null) {
			a = new ArrayList();
			fields.put(key, a);
		}
		//if (reverseOrder) {
		//	a.add(0, value);
		//} else {
			a.add(value);
		//}
	}
	
	public void setTextValue(String val) {
	}
	
	public void removeValue(String key) {
		fields.remove(key);
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Payload (Class=").append(this.getClassName()).append(") - Values [");
		Iterator<String> i = fields.keySet().iterator();
		while (i.hasNext()) {
			String key = i.next();
			Object val = fields.get(key);
			sb.append("\n    ").append(key).append(":").append(val).append(", ");
		}
		sb.append("\n]");
		return sb.toString();
	}
	
	public Payload coerceTo(String packg, String name) {
		Payload newPayload = DomainObjectFactory.getDomainObject(name, packg);
		// Copy the actual data into the new object
		newPayload.setFields(this.fields);
		return newPayload;
	}

	public void addUnresolvedField(String unresolvedXPath, List<String> fieldNames, String value) {
		if (this.unresolvedFieldValues == null) {
			this.unresolvedFieldValues = new HashMap<String, String>();
		}
		if (this.unresolvedFieldNames == null) {
			this.unresolvedFieldNames = new HashMap<String, String>();
		}

		for (String fieldName : fieldNames) {
			this.unresolvedFieldNames.put(unresolvedXPath, fieldName);
		}
		this.unresolvedFieldValues.put(unresolvedXPath, value);
	}
	
	public void resolveField(String xpath, String fieldName) {
		//String val = this.unresolvedFieldValues.get(xpath);
		//this.unresolvedFieldValues.remove(key)
	}
	
	
	/**
	 * {@inheritDoc}
	 * <p>
	 * Default behaviour checks any Emptiable or collection instances
	 * for emptiness.
	 */
	@Override // Emptiable interface
	public boolean isEmpty() {
		if (Emptiables.isNullOrEmpty(fields)) {
			return true;
		}
		
		for (final Object fieldValue: fields.values()) {
			final boolean isEmpty;
			
			if (fieldValue instanceof Emptiable) {
				isEmpty = ((Emptiable)fieldValue).isEmpty();
			} else if (fieldValue instanceof Collection) {
				isEmpty = ((Collection<?>)fieldValue).isEmpty();
			} else {
				isEmpty = fieldValue == null;
			}
			
			if (!isEmpty) {
				return false;
			}
		}
		
		return true;
	}
}
