/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import static uk.nhs.interoperability.payloads.util.Emptiables.isNullOrEmpty;

/**
 * This class represents a coded value in HL7. This will always include
 * a code, will usually also habe a display name (i.e. the "rubric"
 * associated with the code), will often include an OID (although the
 * itk-payloads code can often guess this for you), and may also include a
 * reference to link it with the relevant text in the text section of a
 * CDA document.  
 * @author adha3
 */
public class CodedValue implements Code {
	private String code;
	private String displayName;
	private String oid;
	private String reference;
	
	public CodedValue(String code, String displayName) {
		this.code = code;
		this.displayName = displayName;
	}
	
	public CodedValue(String code, String displayName, String oid) {
		this.code = code;
		this.displayName = displayName;
		this.oid = oid;
	}
	
	public CodedValue(VocabularyEntry vocabularyEntry) {
		this.code = vocabularyEntry.getCode();
		this.displayName = vocabularyEntry.getDisplayName();
		this.oid = vocabularyEntry.getOID();
	}
	
	public CodedValue(VocabularyEntry vocabularyEntry, String reference) {
		this.code = vocabularyEntry.getCode();
		this.displayName = vocabularyEntry.getDisplayName();
		this.oid = vocabularyEntry.getOID();
		this.reference = reference;
	}
	
	public String getCode() {
		return code;
	}
	public Code setCode(String code) {
		this.code = code;
		return this;
	}
	public String getDisplayName() {
		return displayName;
	}
	public Code setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}
	public String getOID() {
		return oid;
	}
	public Code setOID(String oid) {
		this.oid = oid;
		return this;
	}
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}
	
	@Override
	public boolean isEmpty() {
		return isNullOrEmpty(code) &&
				isNullOrEmpty(displayName) &&
				isNullOrEmpty(oid) &&
				isNullOrEmpty(reference);
	}
	
	public boolean sameAs(CodedValue other) {
		return (other.getOID().equals(oid) && other.getCode().equals(code));
	}
	
	public boolean sameAs(VocabularyEntry other) {
		return (other.getOID().equals(oid) && other.getCode().equals(code));
	}
	
	@Override
	public String toString() {
		return "{Code:" + code + " OID:" + oid + "}";
	}
}
