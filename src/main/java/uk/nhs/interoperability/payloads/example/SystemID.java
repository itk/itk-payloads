/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.example;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the SystemID object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Type</li>
 * <li>String ID</li>
 * <li>String OID</li>
 * <li>String AssigningOrganisation</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class SystemID extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "exampleFieldConfig";
		protected static final String name = "SystemID";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.example";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * @param Type String
		 * @param ID String
		 * @param OID String
		 * @param AssigningOrganisation String
		 */
		public SystemID(String Type, String ID, String OID, String AssigningOrganisation) {
			fields = new LinkedHashMap<String, Object>();
			
			setType(Type);
			setID(ID);
			setOID(OID);
			setAssigningOrganisation(AssigningOrganisation);
		}

		/**
		 * Type of ID. Note: this does not get output in the document, but determines the OIDs used in the message.
  		 * <br>NOTE: This field should be populated using the "SystemIDType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType
		 * @return String object
		 */	
		public String getType() {
			return (String)getValue("Type");
		}
		
		
		
		/**
		 * Type of ID. Note: this does not get output in the document, but determines the OIDs used in the message.
  		 * <br>NOTE: This field should be populated using the "SystemIDType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType
		 * @return SystemIDType enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType getTypeEnum() {
			return uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType.getByCode((String)getValue("Type"));
		}
		
		
		/**
		 * Type of ID. Note: this does not get output in the document, but determines the OIDs used in the message.
  		 * <br>NOTE: This field should be populated using the "SystemIDType" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.internal.SystemIDType
		 * @param Type value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SystemID setType(String Type) {
			setValue("Type", Type);
			return this;
		}
		
		
		/**
		 * ID
		 * @return String object
		 */	
		public String getID() {
			return (String)getValue("ID");
		}
		
		
		
		
		/**
		 * ID
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SystemID setID(String ID) {
			setValue("ID", ID);
			return this;
		}
		
		
		/**
		 * OID (Optional)
		 * @return String object
		 */	
		public String getOID() {
			return (String)getValue("OID");
		}
		
		
		
		
		/**
		 * OID (Optional)
		 * @param OID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SystemID setOID(String OID) {
			setValue("OID", OID);
			return this;
		}
		
		
		/**
		 * The ID of the organisation that assigned the local ID (use when there is no OID that can be used).Should be in the form: ODSCode:ODSName, for example 'RA8:St Elsewhere's Hospital'
		 * @return String object
		 */	
		public String getAssigningOrganisation() {
			return (String)getValue("AssigningOrganisation");
		}
		
		
		
		
		/**
		 * The ID of the organisation that assigned the local ID (use when there is no OID that can be used).Should be in the form: ODSCode:ODSName, for example 'RA8:St Elsewhere's Hospital'
		 * @param AssigningOrganisation value to set
		 * @return this To allow the use of the builder pattern
		 */
		public SystemID setAssigningOrganisation(String AssigningOrganisation) {
			setValue("AssigningOrganisation", AssigningOrganisation);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("Type", new Field(
												"Type",
												"",
												"Type of ID. Note: this does not get output in the document, but determines the OIDs used in the message.",
												"false",
												"",
												"SystemIDType",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"@extension",
												"ID",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("OID", new Field(
												"OID",
												"@root",
												"OID (Optional)",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("AssigningOrganisation", new Field(
												"AssigningOrganisation",
												"@assigningAuthorityName",
												"The ID of the organisation that assigned the local ID (use when there is no OID that can be used).Should be in the form: ODSCode:ODSName, for example 'RA8:St Elsewhere's Hospital'",
												"false",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("SDSASID", new Field(
												"SDSASID",
												"@root",
												"1.2.826.0.1285.0.2.0.107",
												"ID",
												"",
												"Type",
												"SDSASID",
												"",
												""
												));
	
		put("LocalSystemOID", new Field(
												"LocalSystemOID",
												"@root",
												"2.16.840.1.113883.2.1.3.2.4.18.36",
												"ID",
												"OID",
												"Type",
												"LocalSystemID",
												"",
												""
												));
	
    	}};
	
	

		static {
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public SystemID() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public SystemID(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	