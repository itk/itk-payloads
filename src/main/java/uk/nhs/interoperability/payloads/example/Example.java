/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.example;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.LinkedHashMap;
import java.io.InputStream;
import org.w3c.dom.Document;
import uk.nhs.interoperability.payloads.*;
import uk.nhs.interoperability.payloads.commontypes.*;
import uk.nhs.interoperability.payloads.templates.*;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyEntry;
import uk.nhs.interoperability.payloads.util.xml.XMLNamespaceContext;
import uk.nhs.interoperability.payloads.metadata.Field;
import uk.nhs.interoperability.payloads.vocabularies.VocabularyFactory;


/**
 * This is the Example object
 * <br>This class is generated using the message config in resources/config
 * <br>
 * <br>
 * <br>The fields that can be set for this payload are:
 * <ul>
 	
 * <li>String Text</li>
 * <li>String Attribute</li>
 * <li>{@link uk.nhs.interoperability.payloads.CodedValue CodedValue} CodedValue</li>
 * <li>{@link uk.nhs.interoperability.payloads.HL7Date HL7Date} Time</li>
 * <li>List&lt;String&gt; ListItem</li>
 * <li>String TextIncl</li>
 * <li>String TextIncl2</li>
 * <li>List&lt;{@link PatientID PatientID}&gt; ID</li>
 * <li>{@link Child Child} Child</li>
 * <li>{@link Template Template} TemplatedField</li>
 * <li>String LongXPathItem</li>
 * </ul>
 * @author Adam Hatherly
 * 
 */

public class Example extends AbstractPayload implements Payload  {
    	
		protected static final String configFileKey = "exampleFieldConfig";
		protected static final String name = "Example";
		protected static final String shortName = "";
		protected static final String rootNode = "";
		protected static final String version = "";
		private static final String packg = "uk.nhs.interoperability.payloads.example";
		protected static XMLNamespaceContext namespaces = new XMLNamespaceContext();

		
		/**
		 * Constructor for payload object
		 * @param Text Text
		 * @param Attribute Attribute
		 * @param CodedValue CodedValue
		 * @param Time Time
		 * @param ListItem ListItem
		 * @param TextIncl TextIncl
		 * @param TextIncl2 TextIncl2
		 * @param ID ID
		 * @param Child Child
		 * @param TemplatedField TemplatedField
		 * @param LongXPathItem LongXPathItem
		 */
	    public Example(String Text, String Attribute, CodedValue CodedValue, HL7Date Time, 
		List<String> ListItem, String TextIncl, String TextIncl2, 
		List<PatientID> ID, Child Child, Template TemplatedField, String LongXPathItem) {
			fields = new LinkedHashMap<String, Object>();
			
			setText(Text);
			setAttribute(Attribute);
			setCodedValue(CodedValue);
			setTime(Time);
			setListItem(ListItem);
			setTextIncl(TextIncl);
			setTextIncl2(TextIncl2);
			setID(ID);
			setChild(Child);
			setTemplatedField(TemplatedField);
			setLongXPathItem(LongXPathItem);
		}
	
		/**
		 * An example of a simple text field
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getText() {
			return (String)getValue("Text");
		}
		
		
		
		
		/**
		 * An example of a simple text field
		 * <br><br>This field is MANDATORY
		 * @param Text value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setText(String Text) {
			setValue("Text", Text);
			return this;
		}
		
		
		/**
		 * An example of a simple text attribute
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getAttribute() {
			return (String)getValue("Attribute");
		}
		
		
		
		
		/**
		 * An example of a simple text attribute
		 * <br><br>This field is MANDATORY
		 * @param Attribute value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setAttribute(String Attribute) {
			setValue("Attribute", Attribute);
			return this;
		}
		
		
		/**
		 * This is an test of a coded value
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return CodedValue object
		 */	
		public CodedValue getCodedValue() {
			return (CodedValue)getValue("CodedValue");
		}
		
		
		
		/**
		 * This is an test of a coded value
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @return JobRoleName enumeration entry
		 */	
		public uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName getCodedValueEnum() {
			CodedValue cv = (CodedValue)getValue("CodedValue");
			VocabularyEntry entry = VocabularyFactory.getVocab("JobRoleName", null, cv.getCode());
			return (uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName)entry;
		}
		
		
		/**
		 * This is an test of a coded value
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * @param CodedValue value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setCodedValue(CodedValue CodedValue) {
			setValue("CodedValue", CodedValue);
			return this;
		}
		
		
		/**
		 * This is an test of a coded value
  		 * <br>NOTE: This field should be populated using the "JobRoleName" vocabulary.
         * @see uk.nhs.interoperability.payloads.vocabularies.generated.JobRoleName
		 * <br><br>This field is MANDATORY
		 * <br>Note: This method allows you to set a coded value by passing in a vocabulary entry directly.
		 * @param CodedValue value from enumeration
		 * @return this To allow the use of the builder pattern
		 */
		public Example setCodedValue(VocabularyEntry CodedValue) {
			Code c = new CodedValue(CodedValue);
			setValue("CodedValue", c);
			return this;
		}
		
		/**
		 * An example of a sime Date/Time field
		 * <br><br>This field is MANDATORY
		 * @return HL7Date object
		 */	
		public HL7Date getTime() {
			return (HL7Date)getValue("Time");
		}
		
		
		
		
		/**
		 * An example of a sime Date/Time field
		 * <br><br>This field is MANDATORY
		 * @param Time value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setTime(HL7Date Time) {
			setValue("Time", Time);
			return this;
		}
		
		
		/**
		 * An example of list of items
		 * <br><br>This field is MANDATORY
		 * @return List of String objects
		 */
		public List<String> getListItem() {
			return (List<String>)getValue("ListItem");
		}
		
		/**
		 * An example of list of items
		 * <br><br>This field is MANDATORY
		 * @param ListItem value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setListItem(List ListItem) {
			setValue("ListItem", ListItem);
			return this;
		}
		
		/**
		 * An example of list of items
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a String object, but this method can be called
		 * multiple times to add additional String objects.
		 * @param ListItem value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Example addListItem(String ListItem) {
			addMultivalue("ListItem", ListItem);
			return this;
		}
		
		
		/**
		 * An example of a field included from a separate config file
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getTextIncl() {
			return (String)getValue("TextIncl");
		}
		
		
		
		
		/**
		 * An example of a field included from a separate config file
		 * <br><br>This field is MANDATORY
		 * @param TextIncl value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setTextIncl(String TextIncl) {
			setValue("TextIncl", TextIncl);
			return this;
		}
		
		
		/**
		 * An example of a field included from a separate config file
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getTextIncl2() {
			return (String)getValue("TextIncl2");
		}
		
		
		
		
		/**
		 * An example of a field included from a separate config file
		 * <br><br>This field is MANDATORY
		 * @param TextIncl2 value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setTextIncl2(String TextIncl2) {
			setValue("TextIncl2", TextIncl2);
			return this;
		}
		
		
		/**
		 * Test of included IDs
		 * <br><br>This field is MANDATORY
		 * @return List of PatientID objects
		 */
		public List<PatientID> getID() {
			return (List<PatientID>)getValue("ID");
		}
		
		/**
		 * Test of included IDs
		 * <br><br>This field is MANDATORY
		 * @param ID value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setID(List ID) {
			setValue("ID", ID);
			return this;
		}
		
		/**
		 * Test of included IDs
		 * <br><br>This field is MANDATORY
		 * <br>Note: This adds a PatientID object, but this method can be called
		 * multiple times to add additional PatientID objects.
		 * @param ID value to add to this list
		 * @return this To allow the use of the builder pattern
		 */
		public Example addID(PatientID ID) {
			addMultivalue("ID", ID);
			return this;
		}
		
		
		/**
		 * An example of a subclass
		 * <br><br>This field is MANDATORY
		 * @return Child object
		 */	
		public Child getChild() {
			return (Child)getValue("Child");
		}
		
		
		
		
		/**
		 * An example of a subclass
		 * <br><br>This field is MANDATORY
		 * @param Child value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setChild(Child Child) {
			setValue("Child", Child);
			return this;
		}
		
		
		/**
		 * An example of a templated field which could have different element named depending on which implementation is used
		 * <br><br>This field is MANDATORY
		 * @return Template object
		 */	
		public Template getTemplatedField() {
			return (Template)getValue("TemplatedField");
		}
		
		
		
		
		/**
		 * An example of a templated field which could have different element named depending on which implementation is used
		 * <br><br>This field is MANDATORY
		 * @param TemplatedField value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setTemplatedField(Template TemplatedField) {
			setValue("TemplatedField", TemplatedField);
			return this;
		}
		
		
		/**
		 * An example of an XPath nesting the field several layers deep in the XML
		 * <br><br>This field is MANDATORY
		 * @return String object
		 */	
		public String getLongXPathItem() {
			return (String)getValue("LongXPathItem");
		}
		
		
		
		
		/**
		 * An example of an XPath nesting the field several layers deep in the XML
		 * <br><br>This field is MANDATORY
		 * @param LongXPathItem value to set
		 * @return this To allow the use of the builder pattern
		 */
		public Example setLongXPathItem(String LongXPathItem) {
			setValue("LongXPathItem", LongXPathItem);
			return this;
		}
		
		

			protected static Map<String, Field> fieldDefinitions = new LinkedHashMap<String, Field>() {{
			
		put("Text", new Field(
												"Text",
												"x:text",
												"An example of a simple text field",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Attribute", new Field(
												"Attribute",
												"x:element/@attribute",
												"An example of a simple text attribute",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("CodedValue", new Field(
												"CodedValue",
												"x:code",
												"This is an test of a coded value",
												"true",
												"",
												"JobRoleName",
												"CodedValue",
												"",
												"",
												"2.16.840.1.113883.2.1.3.2.4.17.339",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Time", new Field(
												"Time",
												"x:time",
												"An example of a sime Date/Time field",
												"true",
												"",
												"",
												"HL7Date",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ListItem", new Field(
												"ListItem",
												"x:list/x:item",
												"An example of list of items",
												"true",
												"",
												"",
												"String",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Fixed1", new Field(
												"Fixed1",
												"x:fixed",
												"Example of a fixed value",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TextIncl", new Field(
												"TextIncl",
												"x:incl",
												"An example of a field included from a separate config file",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Fixed", new Field(
												"Fixed",
												"x:inclFixed",
												"Fixed value in included file",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TextIncl2", new Field(
												"TextIncl2",
												"x:incl2",
												"An example of a field included from a separate config file",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("ID", new Field(
												"ID",
												"x:id",
												"Test of included IDs",
												"true",
												"",
												"",
												"PatientID",
												"",
												"100",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Child", new Field(
												"Child",
												"x:subclass",
												"An example of a subclass",
												"true",
												"",
												"",
												"Child",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("TemplatedField", new Field(
												"TemplatedField",
												"{childRootNodeName}",
												"An example of a templated field which could have different element named depending on which implementation is used",
												"true",
												"",
												"",
												"Template",
												"",
												"",
												"",
												"",
												"(x:typeone/x:templateId/@extension|x:typetwo/x:templateId/@extension)[1]",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Fixed2", new Field(
												"Fixed2",
												"x:title/x:subtitle[x:paragraph/@name='Introduction']/x:paragraph/@name",
												"Introduction",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("LongXPathItem", new Field(
												"LongXPathItem",
												"x:title/x:subtitle[x:paragraph/@name='Introduction']/x:paragraph/x:sentance",
												"An example of an XPath nesting the field several layers deep in the XML",
												"true",
												"",
												"",
												"String",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Fixed3", new Field(
												"Fixed3",
												"x:title/x:subtitle[x:paragraph/@name='Introduction']/x:paragraph/@number",
												"1",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Fixed4", new Field(
												"Fixed4",
												"x:title/x:subtitle[x:paragraph/@name='Background']/x:paragraph/@name",
												"Background",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
		put("Fixed5", new Field(
												"Fixed5",
												"x:title/x:subtitle[x:paragraph/@name='Background']/x:paragraph/@number",
												"2",
												"",
												"",
												"",
												"",
												"",
												""
												));
	
    	}};
	
	

		static {
			namespaces.addNamespace("x", "urn:hl7-org:v3", true);
			namespaces.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance", false);
			//fieldDefinitions = init(namespaces, name, configFileKey);
			Field.setDependentFixedFields(fieldDefinitions);
		}
		
		public Map<String, Field> getFieldDefinitions() {
			return fieldDefinitions;
		}
		
		public String getClassName() {
			return name;
		}
		public String getRootNode() {
			return rootNode;
		}
		public String getVersionedName() {
			if (version == null) {
				return shortName;
			} else if (shortName == null) {
				return version;
			} else {
				return version + "#" + shortName;
			}
		}
		public String getPackage() {
			return packg;
		}
		public XMLNamespaceContext getNamespaceContext() {
			return namespaces;
		}
		
		public Example() {
			fields = new LinkedHashMap<String, Object>();
		}
		
		public void parse(String xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		public void parse(String xml, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name, parentNamespaces);
		}
		public void parse(Document xmlDocument) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name);
		}
		public void parse(Document xmlDocument, XMLNamespaceContext parentNamespaces) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xmlDocument, this, name, parentNamespaces);
		}
		public Example(InputStream xml) {
			fields = new LinkedHashMap<String, Object>();
			super.parse(xml, this, name);
		}
		/**
		 * Serialise the object into an XML message
		 */
		public String serialise() {
			return super.serialise(this);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * @param rootElementName Name of root element in serialised message
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName) {
			return super.serialise(this, rootElementName);
		}
		/**
		 * Serialise the object into an XML message with the specified root element name
		 * and the specified inherited namespaces
		 * @param rootElementName Name of root element in serialised message
		 * @param parentNamespaces Namespaces to use
		 * @return Serialised XML content
		 */
		public String serialise(String rootElementName, XMLNamespaceContext parentNamespaces) {
			return super.serialise(this, rootElementName, parentNamespaces);
		}
}

	