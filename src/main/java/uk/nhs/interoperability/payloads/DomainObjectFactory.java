/**
 * Note: This class is generated automatically when GenerateDomainObjects is run
 * @author Adam Hatherly
 */
package uk.nhs.interoperability.payloads;

public class DomainObjectFactory {
	public static Payload getDomainObject(String name, String packg) {
   	if (packg.endsWith(".")) {
			packg = packg.substring(0, packg.length()-1);
   	};		if (name.equals("EventNotification") && packg.equals("uk.nhs.interoperability.payloads.notification")) {
			return new uk.nhs.interoperability.payloads.notification.EventNotification();
		}
		if (name.equals("Patient") && packg.equals("uk.nhs.interoperability.payloads.notification")) {
			return new uk.nhs.interoperability.payloads.notification.Patient();
		}
		if (name.equals("Recipient") && packg.equals("uk.nhs.interoperability.payloads.notification")) {
			return new uk.nhs.interoperability.payloads.notification.Recipient();
		}
		if (name.equals("OriginatingSystem") && packg.equals("uk.nhs.interoperability.payloads.notification")) {
			return new uk.nhs.interoperability.payloads.notification.OriginatingSystem();
		}
		if (name.equals("Event") && packg.equals("uk.nhs.interoperability.payloads.notification")) {
			return new uk.nhs.interoperability.payloads.notification.Event();
		}
		if (name.equals("ContactPerson") && packg.equals("uk.nhs.interoperability.payloads.notification")) {
			return new uk.nhs.interoperability.payloads.notification.ContactPerson();
		}
		if (name.equals("ClinicalDocument") && packg.equals("uk.nhs.interoperability.payloads.endoflifecarev1")) {
			return new uk.nhs.interoperability.payloads.endoflifecarev1.ClinicalDocument();
		}
		if (name.equals("PrimaryRecipient") && packg.equals("uk.nhs.interoperability.payloads.endoflifecarev1")) {
			return new uk.nhs.interoperability.payloads.endoflifecarev1.PrimaryRecipient();
		}
		if (name.equals("InformationOnlyRecipient") && packg.equals("uk.nhs.interoperability.payloads.endoflifecarev1")) {
			return new uk.nhs.interoperability.payloads.endoflifecarev1.InformationOnlyRecipient();
		}
		if (name.equals("TextSections") && packg.equals("uk.nhs.interoperability.payloads.endoflifecarev1")) {
			return new uk.nhs.interoperability.payloads.endoflifecarev1.TextSections();
		}
		if (name.equals("CodedSections") && packg.equals("uk.nhs.interoperability.payloads.endoflifecarev1")) {
			return new uk.nhs.interoperability.payloads.endoflifecarev1.CodedSections();
		}
		if (name.equals("ClinicalDocument") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.ClinicalDocument();
		}
		if (name.equals("PrimaryRecipient") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.PrimaryRecipient();
		}
		if (name.equals("InformationOnlyRecipient") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.InformationOnlyRecipient();
		}
		if (name.equals("DocumentParticipant") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.DocumentParticipant();
		}
		if (name.equals("DocumentInformant") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.DocumentInformant();
		}
		if (name.equals("TextSections") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.TextSections();
		}
		if (name.equals("CodedSections") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.CodedSections();
		}
		if (name.equals("DocumentationOf") && packg.equals("uk.nhs.interoperability.payloads.noncodedcdav2")) {
			return new uk.nhs.interoperability.payloads.noncodedcdav2.DocumentationOf();
		}
		if (name.equals("CustodianOrganizationUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal();
		}
		if (name.equals("AuthorPersonUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal();
		}
		if (name.equals("PatientUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientUniversal();
		}
		if (name.equals("LanguageCommunication") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.LanguageCommunication();
		}
		if (name.equals("RecipientPersonUniversalv1") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RecipientPersonUniversalv1();
		}
		if (name.equals("RecipientPersonUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal();
		}
		if (name.equals("RecipientOrganizationUniversalv2") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversalv2();
		}
		if (name.equals("RecipientOrganizationUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversal();
		}
		if (name.equals("RecipientWorkgroupUniversalv2") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RecipientWorkgroupUniversalv2();
		}
		if (name.equals("RecipientWorkgroupUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RecipientWorkgroupUniversal();
		}
		if (name.equals("AuthorDeviceUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AuthorDeviceUniversal();
		}
		if (name.equals("AuthorNonNamedPersonUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AuthorNonNamedPersonUniversal();
		}
		if (name.equals("PersonWithOrganizationUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal();
		}
		if (name.equals("WorkgroupUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.WorkgroupUniversal();
		}
		if (name.equals("PatientRelationshipParticipantRole") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientRelationshipParticipantRole();
		}
		if (name.equals("Consent") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Consent();
		}
		if (name.equals("TextSection") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.TextSection();
		}
		if (name.equals("Section2") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Section2();
		}
		if (name.equals("Section3") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Section3();
		}
		if (name.equals("Section4") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Section4();
		}
		if (name.equals("Section5") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Section5();
		}
		if (name.equals("Section6") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Section6();
		}
		if (name.equals("PatientRelationships") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientRelationships();
		}
		if (name.equals("RelatedPersonDetails") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RelatedPersonDetails();
		}
		if (name.equals("RelatedEntity") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RelatedEntity();
		}
		if (name.equals("RelatedEntityParticipant") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RelatedEntityParticipant();
		}
		if (name.equals("PersonUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PersonUniversal();
		}
		if (name.equals("HealthCareFacilityUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.HealthCareFacilityUniversal();
		}
		if (name.equals("DocumentParticipantUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.DocumentParticipantUniversal();
		}
		if (name.equals("RelatedSubject") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.RelatedSubject();
		}
		if (name.equals("Attachment") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Attachment();
		}
		if (name.equals("AttachmentSubject") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AttachmentSubject();
		}
		if (name.equals("ServiceEvent") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ServiceEvent();
		}
		if (name.equals("ServiceEventPerformer") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ServiceEventPerformer();
		}
		if (name.equals("EncompassingEncounter") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.EncompassingEncounter();
		}
		if (name.equals("EncompassingEncounterParticipant") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.EncompassingEncounterParticipant();
		}
		if (name.equals("ReferenceURL") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ReferenceURL();
		}
		if (name.equals("PrognosisAwareness") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PrognosisAwareness();
		}
		if (name.equals("AuthoritytoLastingPowerofAttorney") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AuthoritytoLastingPowerofAttorney();
		}
		if (name.equals("AnticipatoryMedicineBoxIssueProcedure") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AnticipatoryMedicineBoxIssueProcedure();
		}
		if (name.equals("EoLCarePlan") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.EoLCarePlan();
		}
		if (name.equals("EOLPatientRelationships") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.EOLPatientRelationships();
		}
		if (name.equals("AdvanceDecisionToRefuseTreatment") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.AdvanceDecisionToRefuseTreatment();
		}
		if (name.equals("DNACPRDecisionbySeniorResponsibleClinician") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.DNACPRDecisionbySeniorResponsibleClinician();
		}
		if (name.equals("NewBornBirthDetails") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.NewBornBirthDetails();
		}
		if (name.equals("ParticipantOrganizationUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ParticipantOrganizationUniversal();
		}
		if (name.equals("ChildPatientUniversal") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientUniversal();
		}
		if (name.equals("ChildPatientOrganisation") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientOrganisation();
		}
		if (name.equals("ChildPatientOrganisationPartOf") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientOrganisationPartOf();
		}
		if (name.equals("ChildPatientGuardian") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardian();
		}
		if (name.equals("ChildPatientGuardianOrganization") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardianOrganization();
		}
		if (name.equals("ChildPatientGuardianPerson") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardianPerson();
		}
		if (name.equals("NewBornHearingScreening") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.NewBornHearingScreening();
		}
		if (name.equals("BloodSpotScreening") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.BloodSpotScreening();
		}
		if (name.equals("NewBornPhysicalExamination") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.NewBornPhysicalExamination();
		}
		if (name.equals("PatientUniversalv2") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientUniversalv2();
		}
		if (name.equals("Patientv2LanguageCommunication") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.Patientv2LanguageCommunication();
		}
		if (name.equals("PatientGuardian") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardian();
		}
		if (name.equals("PatientGuardianOrganization") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardianOrganization();
		}
		if (name.equals("PatientGuardianPerson") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardianPerson();
		}
		if (name.equals("PatientOrganisation") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientOrganisation();
		}
		if (name.equals("PatientOrganisationPartOf") && packg.equals("uk.nhs.interoperability.payloads.templates")) {
			return new uk.nhs.interoperability.payloads.templates.PatientOrganisationPartOf();
		}
		if (name.equals("Address") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.Address();
		}
		if (name.equals("SMSPAddress") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.SMSPAddress();
		}
		if (name.equals("PatientID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.PatientID();
		}
		if (name.equals("PatientIDWithTraceStatuses") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.PatientIDWithTraceStatuses();
		}
		if (name.equals("SystemID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.SystemID();
		}
		if (name.equals("PersonID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.PersonID();
		}
		if (name.equals("WorkgroupID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.WorkgroupID();
		}
		if (name.equals("OrgID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.OrgID();
		}
		if (name.equals("OrgOrWorkgroupID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.OrgOrWorkgroupID();
		}
		if (name.equals("RoleID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.RoleID();
		}
		if (name.equals("AuthorRoleID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.AuthorRoleID();
		}
		if (name.equals("DeviceID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.DeviceID();
		}
		if (name.equals("ConsentID") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.ConsentID();
		}
		if (name.equals("PersonName") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.PersonName();
		}
		if (name.equals("SMSPPersonName") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.SMSPPersonName();
		}
		if (name.equals("Telecom") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.Telecom();
		}
		if (name.equals("DateRange") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.DateRange();
		}
		if (name.equals("OrgName") && packg.equals("uk.nhs.interoperability.payloads.commontypes")) {
			return new uk.nhs.interoperability.payloads.commontypes.OrgName();
		}
		if (name.equals("GetDocumentQuery") && packg.equals("uk.nhs.interoperability.payloads.documentretrieval")) {
			return new uk.nhs.interoperability.payloads.documentretrieval.GetDocumentQuery();
		}
		if (name.equals("GetDocumentResponse") && packg.equals("uk.nhs.interoperability.payloads.documentretrieval")) {
			return new uk.nhs.interoperability.payloads.documentretrieval.GetDocumentResponse();
		}
		if (name.equals("ClinicalDocument") && packg.equals("uk.nhs.interoperability.payloads.childscreeningv2")) {
			return new uk.nhs.interoperability.payloads.childscreeningv2.ClinicalDocument();
		}
		if (name.equals("PrimaryRecipient") && packg.equals("uk.nhs.interoperability.payloads.childscreeningv2")) {
			return new uk.nhs.interoperability.payloads.childscreeningv2.PrimaryRecipient();
		}
		if (name.equals("InformationOnlyRecipient") && packg.equals("uk.nhs.interoperability.payloads.childscreeningv2")) {
			return new uk.nhs.interoperability.payloads.childscreeningv2.InformationOnlyRecipient();
		}
		if (name.equals("TextSections") && packg.equals("uk.nhs.interoperability.payloads.childscreeningv2")) {
			return new uk.nhs.interoperability.payloads.childscreeningv2.TextSections();
		}
		if (name.equals("CodedSections") && packg.equals("uk.nhs.interoperability.payloads.childscreeningv2")) {
			return new uk.nhs.interoperability.payloads.childscreeningv2.CodedSections();
		}
		if (name.equals("GetPatientDetailsResponse") && packg.equals("uk.nhs.interoperability.payloads.pdsminiservicesv1_1")) {
			return new uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetPatientDetailsResponse();
		}
		if (name.equals("VerifyNHSNumberRequest") && packg.equals("uk.nhs.interoperability.payloads.pdsminiservicesv1_1")) {
			return new uk.nhs.interoperability.payloads.pdsminiservicesv1_1.VerifyNHSNumberRequest();
		}
		if (name.equals("VerifyNHSNumberResponse") && packg.equals("uk.nhs.interoperability.payloads.pdsminiservicesv1_1")) {
			return new uk.nhs.interoperability.payloads.pdsminiservicesv1_1.VerifyNHSNumberResponse();
		}
		if (name.equals("GetNHSNumberRequest") && packg.equals("uk.nhs.interoperability.payloads.pdsminiservicesv1_1")) {
			return new uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetNHSNumberRequest();
		}
		if (name.equals("GetNHSNumberResponse") && packg.equals("uk.nhs.interoperability.payloads.pdsminiservicesv1_1")) {
			return new uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetNHSNumberResponse();
		}
		if (name.equals("GetPatientDetailsByNHSNumberRequest") && packg.equals("uk.nhs.interoperability.payloads.pdsminiservicesv1_1")) {
			return new uk.nhs.interoperability.payloads.pdsminiservicesv1_1.GetPatientDetailsByNHSNumberRequest();
		}
		if (name.equals("SimpleTraceResponse") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.SimpleTraceResponse();
		}
		if (name.equals("PdsTraceQuery") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.PdsTraceQuery();
		}
		if (name.equals("QueryControlAct") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.QueryControlAct();
		}
		if (name.equals("QueryResponse") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.QueryResponse();
		}
		if (name.equals("DetectedIssueEvent") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.DetectedIssueEvent();
		}
		if (name.equals("SendMessagePayload") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.SendMessagePayload();
		}
		if (name.equals("ApplicationAcknowledgement") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.ApplicationAcknowledgement();
		}
		if (name.equals("SpineSOAP") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAP();
		}
		if (name.equals("SpineSOAPResponse") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPResponse();
		}
		if (name.equals("SpineSOAPSimpleTraceBody") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPSimpleTraceBody();
		}
		if (name.equals("SpineSOAPSimpleTraceResponseBody") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPSimpleTraceResponseBody();
		}
		if (name.equals("PDSPatientID") && packg.equals("uk.nhs.interoperability.payloads.spine")) {
			return new uk.nhs.interoperability.payloads.spine.PDSPatientID();
		}
		if (name.equals("AdmissionDetailsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.AdmissionDetailsSection();
		}
		if (name.equals("AllergiesAndAdverseReactionsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.AllergiesAndAdverseReactionsSection();
		}
		if (name.equals("AssessmentScalesSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.AssessmentScalesSection();
		}
		if (name.equals("ClinicalSummarySection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.ClinicalSummarySection();
		}
		if (name.equals("DiagnosesSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.DiagnosesSection();
		}
		if (name.equals("DischargeDetailsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.DischargeDetailsSection();
		}
		if (name.equals("InformationGivenSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.InformationGivenSection();
		}
		if (name.equals("InvestigationsAndProceduresRequestedSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.InvestigationsAndProceduresRequestedSection();
		}
		if (name.equals("LegalInformationSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.LegalInformationSection();
		}
		if (name.equals("MedicationsAndMedicalDevicesSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.MedicationsAndMedicalDevicesSection();
		}
		if (name.equals("ParticipationInResearchSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.ParticipationInResearchSection();
		}
		if (name.equals("PatientAndCarerConcernsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.PatientAndCarerConcernsSection();
		}
		if (name.equals("PersonCompletingRecordSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.PersonCompletingRecordSection();
		}
		if (name.equals("PlanAndRequestedActionsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.PlanAndRequestedActionsSection();
		}
		if (name.equals("ProceduresSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.ProceduresSection();
		}
		if (name.equals("SafetyAlertsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.SafetyAlertsSection();
		}
		if (name.equals("SocialContextSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.SocialContextSection();
		}
		if (name.equals("SpecialRequirementsSection") && packg.equals("uk.nhs.interoperability.payloads.prsbsections")) {
			return new uk.nhs.interoperability.payloads.prsbsections.SpecialRequirementsSection();
		}
		if (name.equals("ClinicalDocument") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.ClinicalDocument();
		}
		if (name.equals("PrimaryRecipient") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.PrimaryRecipient();
		}
		if (name.equals("InformationOnlyRecipient") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.InformationOnlyRecipient();
		}
		if (name.equals("DocumentParticipant") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentParticipant();
		}
		if (name.equals("DocumentInformant") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentInformant();
		}
		if (name.equals("DocumentSection") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentSection();
		}
		if (name.equals("DocumentationOf") && packg.equals("uk.nhs.interoperability.payloads.toc_edischarge_draftB")) {
			return new uk.nhs.interoperability.payloads.toc_edischarge_draftB.DocumentationOf();
		}
		if (name.equals("Example") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.Example();
		}
		if (name.equals("Child") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.Child();
		}
		if (name.equals("Template1") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.Template1();
		}
		if (name.equals("Template2") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.Template2();
		}
		if (name.equals("Template3") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.Template3();
		}
		if (name.equals("Template4") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.Template4();
		}
		if (name.equals("PatientID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.PatientID();
		}
		if (name.equals("PatientIDWithTraceStatuses") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.PatientIDWithTraceStatuses();
		}
		if (name.equals("SystemID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.SystemID();
		}
		if (name.equals("PersonID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.PersonID();
		}
		if (name.equals("WorkgroupID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.WorkgroupID();
		}
		if (name.equals("OrgID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.OrgID();
		}
		if (name.equals("OrgOrWorkgroupID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.OrgOrWorkgroupID();
		}
		if (name.equals("RoleID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.RoleID();
		}
		if (name.equals("AuthorRoleID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.AuthorRoleID();
		}
		if (name.equals("DeviceID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.DeviceID();
		}
		if (name.equals("ConsentID") && packg.equals("uk.nhs.interoperability.payloads.example")) {
			return new uk.nhs.interoperability.payloads.example.ConsentID();
		}
		return null;
	}
	public static Payload getDomainObjectWithVersion(String versionedName, String packg, String constraint) {
       if (versionedName == null) { return null; };
   	if (packg.endsWith(".")) {
			packg = packg.substring(0, packg.length()-1);
   	};
		if (versionedName.equals("COCD_TP145018UK03#AssignedCustodian") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.CustodianOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145200GB01#AssignedAuthor") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145200GB01#AssignedAuthor") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Author") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145200GB01#AssignedAuthor") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildScreeningAuthor") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145201GB01#PatientRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientUniversal();
		}
		if (versionedName.equals("COCD_TP145202GB01#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientPersonUniversalv1();
		}
		if (versionedName.equals("COCD_TP145202GB01#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Recipient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientPersonUniversalv1();
		}
		if (versionedName.equals("COCD_TP145202GB02#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145202GB02#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Recipient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145203GB02#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversalv2();
		}
		if (versionedName.equals("COCD_TP145203GB02#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Recipient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversalv2();
		}
		if (versionedName.equals("COCD_TP145203GB03#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145203GB03#IntendedRecipient") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Recipient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145204GB02#RecipientWorkgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientWorkgroupUniversalv2();
		}
		if (versionedName.equals("COCD_TP145204GB02#RecipientWorkgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Recipient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientWorkgroupUniversalv2();
		}
		if (versionedName.equals("COCD_TP145204GB03#RecipientWorkgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientWorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145204GB03#RecipientWorkgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Recipient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RecipientWorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145207GB01#AssignedAuthorDevice") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorDeviceUniversal();
		}
		if (versionedName.equals("COCD_TP145207GB01#AssignedAuthorDevice") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Author") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorDeviceUniversal();
		}
		if (versionedName.equals("COCD_TP145208GB01#AssignedAuthor") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorNonNamedPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145208GB01#AssignedAuthor") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Author") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorNonNamedPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145208GB01#AssignedAuthor") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildScreeningAuthor") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthorNonNamedPersonUniversal();
		}
		if (versionedName.equals("COCD_TP145210GB01#AssignedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145210GB01#AssignedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Performer") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145210GB01#AssignedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Informant") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145210GB01#AssignedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("EncounterParticipant") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145210GB01#AssignedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ResponsibleParty") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PersonWithOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145212GB02#Workgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.WorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145212GB02#Workgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Author") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.WorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145212GB02#Workgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Performer") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.WorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145212GB02#Workgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("EncounterParticipant") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.WorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145212GB02#Workgroup") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ResponsibleParty") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.WorkgroupUniversal();
		}
		if (versionedName.equals("COCD_TP145226GB01#ParticipantRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientRelationshipParticipantRole();
		}
		if (versionedName.equals("COCD_TP146226GB02#Consent") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.Consent();
		}
		if (versionedName.equals("COCD_TP146229GB01#Section1") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.TextSection();
		}
		if (versionedName.equals("COCD_TP146416GB01#PatientRelationships") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientRelationships();
		}
		if (versionedName.equals("COCD_TP145007UK03#RelatedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RelatedEntity();
		}
		if (versionedName.equals("COCD_TP145007UK03#RelatedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Informant") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RelatedEntity();
		}
		if (versionedName.equals("COCD_TP145007UK03#RelatedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RelatedEntityParticipant();
		}
		if (versionedName.equals("COCD_TP145007UK03#RelatedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Participant") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RelatedEntityParticipant();
		}
		if (versionedName.equals("COCD_TP145205GB01#AssignedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PersonUniversal();
		}
		if (versionedName.equals("COCD_TP145211GB01#HealthCareFacility") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.HealthCareFacilityUniversal();
		}
		if (versionedName.equals("COCD_TP145214GB01#AssociatedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.DocumentParticipantUniversal();
		}
		if (versionedName.equals("COCD_TP145214GB01#AssociatedEntity") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("Participant") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.DocumentParticipantUniversal();
		}
		if (versionedName.equals("COCD_TP145213GB01#RelatedSubject") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.RelatedSubject();
		}
		if (versionedName.equals("COCD_TP146224GB02#ObservationMedia") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.Attachment();
		}
		if (versionedName.equals("COCD_TP146227GB02#ServiceEvent") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ServiceEvent();
		}
		if (versionedName.equals("COCD_TP146228GB01#EncompassingEncounter") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.EncompassingEncounter();
		}
		if (versionedName.equals("COCD_TP146248GB01#ReferenceURL") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ReferenceURL();
		}
		if (versionedName.equals("COCD_TP146248GB01#ReferenceURL") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ReferenceURL();
		}
		if (versionedName.equals("COCD_TP146414GB01#PrognosisAwareness") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PrognosisAwareness();
		}
		if (versionedName.equals("COCD_TP146414GB01#PrognosisAwareness") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PrognosisAwareness();
		}
		if (versionedName.equals("COCD_TP146417GB01#AuthorityToLPA") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthoritytoLastingPowerofAttorney();
		}
		if (versionedName.equals("COCD_TP146417GB01#AuthorityToLPA") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AuthoritytoLastingPowerofAttorney();
		}
		if (versionedName.equals("COCD_TP146418GB01#AMBoxIssueProcedure") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AnticipatoryMedicineBoxIssueProcedure();
		}
		if (versionedName.equals("COCD_TP146418GB01#AMBoxIssueProcedure") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AnticipatoryMedicineBoxIssueProcedure();
		}
		if (versionedName.equals("COCD_TP146419GB01#EoLCarePlan") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.EoLCarePlan();
		}
		if (versionedName.equals("COCD_TP146419GB01#EoLCarePlan") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.EoLCarePlan();
		}
		if (versionedName.equals("COCD_TP146420GB01#PatientADRT") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AdvanceDecisionToRefuseTreatment();
		}
		if (versionedName.equals("COCD_TP146420GB01#PatientADRT") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.AdvanceDecisionToRefuseTreatment();
		}
		if (versionedName.equals("COCD_TP146422GB01#DNACPRbySRClinician") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.DNACPRDecisionbySeniorResponsibleClinician();
		}
		if (versionedName.equals("COCD_TP146422GB01#DNACPRbySRClinician") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("CodedSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.DNACPRDecisionbySeniorResponsibleClinician();
		}
		if (versionedName.equals("COCD_TP000028GB01#NewBornBirthDetails") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.NewBornBirthDetails();
		}
		if (versionedName.equals("COCD_TP000028GB01#NewBornBirthDetails") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildScreeningCodedSections") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.NewBornBirthDetails();
		}
		if (versionedName.equals("COCD_TP000029GB01#ParticipantRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ParticipantOrganizationUniversal();
		}
		if (versionedName.equals("COCD_TP145230GB02#PatientRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientUniversal();
		}
		if (versionedName.equals("COCD_TP145230GB02#PatientRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("TOCPatient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientUniversal();
		}
		if (versionedName.equals("COCD_TP145230GB02#guardianOrganization") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardianOrganization();
		}
		if (versionedName.equals("COCD_TP145230GB02#guardianOrganization") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildPatientGuardianChoice") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardianOrganization();
		}
		if (versionedName.equals("COCD_TP145230GB02#guardianPerson") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardianPerson();
		}
		if (versionedName.equals("COCD_TP145230GB02#guardianPerson") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildPatientGuardianChoice") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.ChildPatientGuardianPerson();
		}
		if (versionedName.equals("COCD_TP146003GB01#NewBornHearingScreening") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.NewBornHearingScreening();
		}
		if (versionedName.equals("COCD_TP146003GB01#NewBornHearingScreening") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildScreeningCodedSections") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.NewBornHearingScreening();
		}
		if (versionedName.equals("COCD_TP146005GB02#BloodSpotScreening") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.BloodSpotScreening();
		}
		if (versionedName.equals("COCD_TP146005GB02#BloodSpotScreening") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildScreeningCodedSections") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.BloodSpotScreening();
		}
		if (versionedName.equals("COCD_TP146004GB01#NewBornPhysicalExamination") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.NewBornPhysicalExamination();
		}
		if (versionedName.equals("COCD_TP146004GB01#NewBornPhysicalExamination") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("ChildScreeningCodedSections") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.NewBornPhysicalExamination();
		}
		if (versionedName.equals("COCD_TP145201GB02#PatientRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientUniversalv2();
		}
		if (versionedName.equals("COCD_TP145201GB02#PatientRole") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("TOCPatient") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientUniversalv2();
		}
		if (versionedName.equals("COCD_TP145201GB02#guardianOrganization") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardianOrganization();
		}
		if (versionedName.equals("COCD_TP145201GB02#guardianOrganization") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("PatientGuardianChoice") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardianOrganization();
		}
		if (versionedName.equals("COCD_TP145201GB02#guardianPerson") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardianPerson();
		}
		if (versionedName.equals("COCD_TP145201GB02#guardianPerson") 
				&& packg.equals("uk.nhs.interoperability.payloads.templates") 
				&& constraint.equals("PatientGuardianChoice") 
		   ) {
			return new uk.nhs.interoperability.payloads.templates.PatientGuardianPerson();
		}
		if (versionedName.equals("QUPA_IN000005UK01") 
				&& packg.equals("uk.nhs.interoperability.payloads.spine") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPSimpleTraceBody();
		}
		if (versionedName.equals("QUPA_IN000005UK01") 
				&& packg.equals("uk.nhs.interoperability.payloads.spine") 
				&& constraint.equals("SpineSOAPBody") 
		   ) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPSimpleTraceBody();
		}
		if (versionedName.equals("QUPA_IN000007UK01") 
				&& packg.equals("uk.nhs.interoperability.payloads.spine") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPSimpleTraceResponseBody();
		}
		if (versionedName.equals("QUPA_IN000007UK01") 
				&& packg.equals("uk.nhs.interoperability.payloads.spine") 
				&& constraint.equals("SpineResponseBody") 
		   ) {
			return new uk.nhs.interoperability.payloads.spine.SpineSOAPSimpleTraceResponseBody();
		}
		if (versionedName.equals("COCD_TP000034GB01#AdmissionDetailsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.AdmissionDetailsSection();
		}
		if (versionedName.equals("COCD_TP000034GB01#AdmissionDetailsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.AdmissionDetailsSection();
		}
		if (versionedName.equals("COCD_TP000035GB01#AllergiesAndAdverseReactionsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.AllergiesAndAdverseReactionsSection();
		}
		if (versionedName.equals("COCD_TP000035GB01#AllergiesAndAdverseReactionsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.AllergiesAndAdverseReactionsSection();
		}
		if (versionedName.equals("COCD_TP000008GB01#AssessmentScalesSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.AssessmentScalesSection();
		}
		if (versionedName.equals("COCD_TP000008GB01#AssessmentScalesSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.AssessmentScalesSection();
		}
		if (versionedName.equals("COCD_TP000033GB01#ClinicalSummarySection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.ClinicalSummarySection();
		}
		if (versionedName.equals("COCD_TP000033GB01#ClinicalSummarySection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.ClinicalSummarySection();
		}
		if (versionedName.equals("COCD_TP000037GB01#DiagnosesSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.DiagnosesSection();
		}
		if (versionedName.equals("COCD_TP000037GB01#DiagnosesSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.DiagnosesSection();
		}
		if (versionedName.equals("COCD_TP000036GB01#DischargeDetailsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.DischargeDetailsSection();
		}
		if (versionedName.equals("COCD_TP000036GB01#DischargeDetailsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.DischargeDetailsSection();
		}
		if (versionedName.equals("COCD_TP000015GB01#InformationGivenSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.InformationGivenSection();
		}
		if (versionedName.equals("COCD_TP000015GB01#InformationGivenSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.InformationGivenSection();
		}
		if (versionedName.equals("COCD_TP000041GB01#InvestigationsAndProceduresRequestedSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.InvestigationsAndProceduresRequestedSection();
		}
		if (versionedName.equals("COCD_TP000041GB01#InvestigationsAndProceduresRequestedSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.InvestigationsAndProceduresRequestedSection();
		}
		if (versionedName.equals("COCD_TP000044GB01#LegalInformationSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.LegalInformationSection();
		}
		if (versionedName.equals("COCD_TP000044GB01#LegalInformationSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.LegalInformationSection();
		}
		if (versionedName.equals("COCD_TP000040GB01#MedicationsAndMedicalDevicesSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.MedicationsAndMedicalDevicesSection();
		}
		if (versionedName.equals("COCD_TP000040GB01#MedicationsAndMedicalDevicesSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.MedicationsAndMedicalDevicesSection();
		}
		if (versionedName.equals("COCD_TP000001GB01#ParticipationInResearchSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.ParticipationInResearchSection();
		}
		if (versionedName.equals("COCD_TP000001GB01#ParticipationInResearchSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.ParticipationInResearchSection();
		}
		if (versionedName.equals("COCD_TP000010GB01#PatientAndCarerConcernsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.PatientAndCarerConcernsSection();
		}
		if (versionedName.equals("COCD_TP000010GB01#PatientAndCarerConcernsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.PatientAndCarerConcernsSection();
		}
		if (versionedName.equals("COCD_TP000046GB01#PersonCompletingRecordSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.PersonCompletingRecordSection();
		}
		if (versionedName.equals("COCD_TP000046GB01#PersonCompletingRecordSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.PersonCompletingRecordSection();
		}
		if (versionedName.equals("COCD_TP000014GB01#PlanAndRequestedActionsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.PlanAndRequestedActionsSection();
		}
		if (versionedName.equals("COCD_TP000014GB01#PlanAndRequestedActionsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.PlanAndRequestedActionsSection();
		}
		if (versionedName.equals("COCD_TP000038GB01#ProceduresSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.ProceduresSection();
		}
		if (versionedName.equals("COCD_TP000038GB01#ProceduresSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.ProceduresSection();
		}
		if (versionedName.equals("COCD_TP000039GB01#SafetyAlertsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.SafetyAlertsSection();
		}
		if (versionedName.equals("COCD_TP000039GB01#SafetyAlertsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.SafetyAlertsSection();
		}
		if (versionedName.equals("COCD_TP000043GB01#SocialContextSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.SocialContextSection();
		}
		if (versionedName.equals("COCD_TP000043GB01#SocialContextSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.SocialContextSection();
		}
		if (versionedName.equals("COCD_TP000042GB01#SpecialRequirementsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.SpecialRequirementsSection();
		}
		if (versionedName.equals("COCD_TP000042GB01#SpecialRequirementsSection") 
				&& packg.equals("uk.nhs.interoperability.payloads.prsbsections") 
				&& constraint.equals("PRSBSection") 
		   ) {
			return new uk.nhs.interoperability.payloads.prsbsections.SpecialRequirementsSection();
		}
		if (versionedName.equals("Template1#Template1") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template1();
		}
		if (versionedName.equals("Template1#Template1") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("Template") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template1();
		}
		if (versionedName.equals("Template2#Template2") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template2();
		}
		if (versionedName.equals("Template2#Template2") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("Template") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template2();
		}
		if (versionedName.equals("Template3#Template3") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template3();
		}
		if (versionedName.equals("Template3#Template3") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("SimpleTemplate") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template3();
		}
		if (versionedName.equals("Template4#Template4") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template4();
		}
		if (versionedName.equals("Template4#Template4") 
				&& packg.equals("uk.nhs.interoperability.payloads.example") 
				&& constraint.equals("SimpleTemplate") 
		   ) {
			return new uk.nhs.interoperability.payloads.example.Template4();
		}
		return null;
	}
}