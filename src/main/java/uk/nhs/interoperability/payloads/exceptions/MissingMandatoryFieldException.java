/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads.exceptions;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

public class MissingMandatoryFieldException extends Exception {
	private static final long serialVersionUID = 1L;
	private HashMap<String, String> missingFields = new HashMap<String, String>();
	
	public MissingMandatoryFieldException() {
		super("Missing mandatory fields");
	}
	
	public MissingMandatoryFieldException(MissingMandatoryFieldException missingFields) {
		super("Missing mandatory fields");
		this.missingFields.putAll(missingFields.getMissingFields());
	}
	
	public void addMissingField(String fieldName, String errorMessage) {
		missingFields.put(fieldName, errorMessage);
	}
	
	public void addMissingFields(MissingMandatoryFieldException missingFields) {
		this.missingFields.putAll(missingFields.getMissingFields());
	}
	
	public HashMap<String, String> getMissingFields() {
		return missingFields;
	}
	
	public boolean hasEntries() {
		return (missingFields.size()>0);
	}
	
	@Override
	public String toString() {
		String out = "Missing mandatory fields (" + missingFields.size() + "):\n";
		for (String name : missingFields.keySet()) {
			out = out + " Field name: " + name + ", Message: " + missingFields.get(name) + "\n";
		}
		return out;
	}
}
