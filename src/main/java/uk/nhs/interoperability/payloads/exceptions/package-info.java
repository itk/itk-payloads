/**
 * Exception types that can be generated from the itk-payloads code
 */
package uk.nhs.interoperability.payloads.exceptions;