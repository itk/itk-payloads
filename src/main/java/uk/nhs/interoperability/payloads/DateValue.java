/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package uk.nhs.interoperability.payloads;

import java.util.Date;

import uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision;

import static uk.nhs.interoperability.payloads.util.Emptiables.isNullOrEmpty;

/**
 * This class represents a date/time which can then be serialised in the way
 * HL7 requires. We therefore need to habe both the date/time and a precision
 * to allow the value to be serialised. The object can either be constructed
 * with a string (as per the HL7 format), or as a java date and associated
 * precision. See the {@link uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision DatePrecision}
 * class for details.
 * @see uk.nhs.interoperability.payloads.vocabularies.internal.DatePrecision
 * @author adha3
 *
 */
public class DateValue implements HL7Date {
	private String value = null;
	private DatePrecision precision = null;
	
	public DatePrecision getPrecision() {
		return precision;
	}

	public DateValue(String value) {
		this.value = value;
		this.precision = DatePrecision.getPrecision(value);
	}
	
	public DateValue(Date dateVal, DatePrecision precision) {
		this.precision = precision;
		this.value = this.precision.makeString(dateVal);
	}
	
	public Date getDate() {
		return precision.makeDate(value);
	}
	
	public String getDisplayString() {
		return precision.makeDisplayString(getDate());
	}
	
	public String asString() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
	@Override
	public boolean isEmpty() {
		return isNullOrEmpty(value);
	}
}
