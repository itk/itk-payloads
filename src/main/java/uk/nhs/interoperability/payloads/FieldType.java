/*
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package uk.nhs.interoperability.payloads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.nhs.interoperability.payloads.util.FieldHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.CodedItemHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.CompositionalGrammarItemHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.DOMFieldHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.FHIRCodedItemHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.FixedFieldHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.HL7DateHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.PayloadFieldHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.StringHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.TemplatedFieldHandler;
import uk.nhs.interoperability.payloads.util.fieldtypehandlers.XMLFieldHandler;

/**
 * This is an enumeration of the types of fields defined in the payload config.
 */
public enum FieldType {
	String(StringHandler.class),
	XML(XMLFieldHandler.class),
	HL7Date(HL7DateHandler.class),
	CodedValue(CodedItemHandler.class),
	FHIRCodedValue(FHIRCodedItemHandler.class),
	Fixed(FixedFieldHandler.class),
	Templated(TemplatedFieldHandler.class),
	Other(PayloadFieldHandler.class),
	CompositionalStatement(CompositionalGrammarItemHandler.class);

	private static final Logger logger = LoggerFactory.getLogger(FieldType.class);
	private Class handlerClass = null;
	
	private FieldType(Class cls) {
		this.handlerClass = cls;
	}
	
	public DOMFieldHandler getHandler() {
		try {
			return (DOMFieldHandler)handlerClass.newInstance();
		} catch (InstantiationException e) {
			logger.error("Unable to instantiate field handler for field type: {}", this.name(), e);
		} catch (IllegalAccessException e) {
			logger.error("Unable to instantiate field handler for field type: {}", this.name(), e);
		}
		return null;
	}
}